#include "rfacInterpolatorContainer.hh"

ClassImp(rfacInterpolatorContainer)

rfacInterpolatorContainer::rfacInterpolatorContainer()
{
  
}

rfacInterpolatorContainer::rfacInterpolatorContainer(const QuantumNumbers & k, vector<rfacSpecificLevelData> & values, int fieldGridSize, VerbosePrinter * toUse)
 :key(k)
{
  if(values.size() < 2)
    throw rfacException("Too little data to be able to do a linear fit.");
  ///construct the interpolators.
  
  map<QuantumNumbers, vector<pair<Double_t, Double_t> > > dRate;

  ///map<QuantumNumbers, rfacFieldGridContainer> containing; ///used to speed up point assurance (later).

  for(vector<rfacSpecificLevelData>::iterator it = values.begin(); it!=values.end(); ++it)
    {
      ///Loop order change in progress here.
      for(map<QuantumNumbers, Double_t>::iterator ip = it->Level.DecayRates.begin(); ip!=it->Level.DecayRates.end(); ++ip)
	{
	  dRate[ip->first].push_back(make_pair(it->BField,ip->second));
	}
    }



  for(map<QuantumNumbers, vector<pair<Double_t, Double_t> > >::iterator it = dRate.begin(); it!=dRate.end(); ++it)
    {
      if(it->second.size() < 2) ///VERY bad, interpolation is going to fail. Zero everything out instead.
	{
	  if(toUse)
		toUse->Print(1, "WARNING: not enough decay rates (%d), this decay channel (%s ===>>> %s) will now be discarded!\n", 
		 it->second.size(), key.ToString().c_str(),it->first.ToString().c_str());
	}
      else
	{
	  decayRates.insert(make_pair(it->first,rfacLinearInterpolation(it->second)));
	}
	  
    }
}


rfacInterpolatorContainer::~rfacInterpolatorContainer()
{

}
