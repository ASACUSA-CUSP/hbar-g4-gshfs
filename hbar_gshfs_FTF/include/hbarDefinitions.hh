#ifndef definitions_hh
#define definitions_hh 1
#define maxSN    20
#define maxScN   20
#define maxVeto  20 // Added by Clemens
#define maxCBF  3 // Added by Chloe
#define maxHDF  2 // Added by Chloe
#define maxHodoscope  20 // Added by Clemens

#define maxBPN  100
#define maxLSN  100
#define maxCF100Cross 20
#define maxGV 10 //Gate Valves, added by Stefan
#define maxFI 3  //Field Ionizer, added by Stefan
#define maxBGO2014 3 //BGO 2014 dimensions, added by Stefan




#define maxNDcover 3

#define maxMLSD 8
#define maxMLDuctP 9
#define maxMLEle 4
#define maxMLEleSub 2
#define maxSxPart 15

#define BEAMLINE_HEIGHT 1.2 ///In meters.


#endif
