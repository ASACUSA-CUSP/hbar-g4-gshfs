#ifndef hbarAntiHydrogen_h
#define hbarAntiHydrogen_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "hbarHydrogenLike.hh"

//! The Anti-Hydrogen class with all properties of the Anti-Atom
/*!
  This class contains all properties of the Atom, including quantum numbers
  and magnetic moment.

  This class is designed to be a \b singelton! Keep this in mind when changeing properties!
 */
class hbarAntiHydrogen : public hbarHydrogenLike
{
private:
    //! A pointer to the constructed particle, this pointer is NO class member!
  static hbarAntiHydrogen* theInstance;
  //Constructor.
  hbarAntiHydrogen(const G4String& name /*! The name of the particle. Unique. */);

public:
  //! Definition. Basically returns the object if it can find it, or otherwise creates a new one.
  static hbarAntiHydrogen* Definition();
};

#endif
