#ifndef hbarDetectorMessenger_h
#define hbarDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarDetConst.hh"


class hbarDetConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

//! The detector messenger class
class hbarDetMess: public G4UImessenger
{
public:
  /*!
    The constructor defines the whole macro structor the detector and beamline.
  */
  hbarDetMess(hbarDetConst* );
  ~hbarDetMess();

  void SetNewValue(G4UIcommand*, G4String);

private:
  hbarDetConst*              hbarDetector;          //!< The whole geometry

  G4UIdirectory*             hbardetDir;            //!< /setup/


  G4UIcmdWithAnInteger*      UseCUSPCmd;            //!< setUseCUSP


  G4UIcmdWithADoubleAndUnit* WorldSizeXYCmd;        //!< setWorldSizeXY
  G4UIcmdWithADoubleAndUnit* WorldSizeZCmd;         //!< setWorldSizeZ
  G4UIcmdWithAnInteger*      UseNagataDetectorCmd;

  G4UIcmdWithAnInteger*      UseCAVITYCmd;

  G4UIcmdWithoutParameter*   UpdateCmd;             //!< update

  G4UIcmdWithAnInteger*      WriteGDMLCmd; /// Write G4-geometrie to GDML

  G4UIcmdWithAnInteger*      HbarUseCmd;            //!< setUseHbar

  G4UIcmdWithAnInteger*      HbarUse2014Cmd; /// 2014 Beamline Setup

  G4UIcmdWithAnInteger*      UseCPTCmd; /// use charged Pion Tracker

};

#endif

