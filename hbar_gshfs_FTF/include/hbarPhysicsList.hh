// Skeletton is taken from example TestEM7

#ifndef HbarPhysicsList_h
#define HbarPhysicsList_h 1

#include "G4VModularPhysicsList.hh"
#include "hbarPhysicsListMess.hh"
#include "globals.hh"
//#include "hbarHydrogenLikeDecayAtRest.hh"

#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" 
#include "hbarAntiProtonAnnihilationAtRest.hh"
#include "hbarLowEnergyIonisation.hh" 

#include "hbarAntiHydrogenDecayAtRest.hh"
#include "hbarHydrogenDecayAtRest.hh"


#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4eMultipleScattering.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuMultipleScattering.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hMultipleScattering.hh"
#include "G4hIonisation.hh"
//#include "G4hEnergyLoss.hh"
#include "G4ionIonisation.hh"

#include "hbarLowEnergyIonisation.hh"
#include "G4hIonisation.hh"
#include "G4Transportation.hh"
#include "G4IonTable.hh"

#include "G4StepLimiter.hh"

#include "hbarAntiHydrogenDecayAtRest.hh"

#include "G4UserPhysicsListMessenger.hh"

#include "G4PhysicsListHelper.hh"

// EM physics
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4EmLivermorePolarizedPhysics.hh"
#include "G4LowEPComptonModel.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmDNAPhysics.hh"

#include "G4DecayPhysics.hh"

#include "G4HadronElasticPhysics.hh"
#include "G4HadronDElasticPhysics.hh"
#include "G4HadronHElasticPhysics.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4IonPhysics.hh"

#include "G4OpticalPhysics.hh"

  #include "G4Cerenkov.hh"         
  #include "G4Scintillation.hh"   
  #include "G4OpAbsorption.hh"
  #include  "G4OpRayleigh.hh"
  #include "G4OpMieHG.hh"
  #include "G4OpBoundaryProcess.hh"

#include "G4LossTableManager.hh"
#include "G4EmConfigurator.hh"
#include "G4UnitsTable.hh"

#include "G4ProcessManager.hh"
#include "G4Decay.hh"

#include "G4IonFluctuations.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4UniversalFluctuation.hh"

#include "G4BraggIonGasModel.hh"
#include "G4BetheBlochIonGasModel.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4HadronPhysicsQGSP_BIC.hh"
#include "G4HadronPhysicsQGSP_BERT.hh"
#include "G4HadronPhysicsQGSP_FTFP_BERT.hh"
#include "G4HadronPhysicsFTF_BIC.hh"
// Fritof
#include "G4HadronPhysicsFTFP_BERT_TRV.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4AntiProtonAbsorptionFritiof.hh"

// local includes
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh"
#include "hbarAntiProtonAnnihilationAtRest.hh"
#include "hbarAntiHydrogenDecayAtRest.hh"
#include "hbarLowEnergyIonisation.hh"
#include "hbarHydrogenDecayAtRest.hh"
#include "G4StepLimiter.hh"

#include "G4Transportation.hh"


#include "G4ProcessManager.hh"

#include "globals.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4UserSpecialCuts.hh"
#include "G4ios.hh"
#include <iomanip>

//#include "G4FastSimulationManagerProcess.hh"


class G4VPhysicsConstructor;
class G4UserPhysicsListMessenger;

//! List of physical processes that are used in the simulation
/*!
  This is the physics list class which tells GEANT4 which physical processes
  to use for which particle (and also creates the particles themselves). 
  Since this simulation is at low energy (maximum 2 GeV), no exotic leptonic
  processes are required: mostly ionisation, bremsstrahlung, pair production 
  and multiple scattering are used for every particle. The notable exceptions 
  are antiproton (annihilation is added) and antihydrogen (decay at rest is 
  added). For short-lived hadrons (pions, kaons, lambdas, etc.) the standard 
  GEANT4 elastic and inelastic processes are used.
  
  This class also sets the default "cut value" in range (now 0.5 mm). After 
  every step, if the remaining range of a particle is less than this cut value
  , then the particle is killed on the spot and its remaining energy is 
  deposited on the spot. So this cut value can be thought of as a "spatial 
  accuracy" of the simulation. (Not to be confused with the trajectory step 
  size, which was 0.1 mm!)
*/
class hbarPhysicsList: public G4VModularPhysicsList
{
public:
  
  //! constructor
  /*!
    Sets up a working physics list, the standard configuartion is as follows:
    - EM physics:              StandardPhysics_option4
    - Hadronic ineleastic:     FTFP_BERT_TRV
    - Antiproton annihilation: SMI code
    - Hadronic eleastic:       G4HadronElasticPhysics (no other choice)
    - general particle decay:  G4DecayPhysics (no other choice)
   */
  hbarPhysicsList();

  //! destructor
 ~hbarPhysicsList();

  //! creates the particle definition singletons
  virtual void ConstructParticle();

  //! actually activates the processes for every particle
  virtual void ConstructProcess();  

  //! sets some cut values
  virtual void SetCuts();

  void SetConstructOptPhotons(G4String);

  void SetVerboseOpt(G4int);

  //! sets cutvalue for Gammas
  void SetCutForGamma        (G4double);

  //! sets cutvalue for e-
  void SetCutForElectron     (G4double);

  //! sets cutvalue for e+
  void SetCutForPositron     (G4double);

  //! allows to choose the physics list for electromagnetic processes
  /*!
    Those are the lists to choose from:
    - emstandard_opt4     should be the most accurate one (standard choice)
    - livermore
    - livermore_polarized (with polarized photons)
    - penelope
    - dnaphysics

    for more information on this lists visit: 
    https://twiki.cern.ch/twiki/bin/view/Geant4/LowePhysicsLists
   */
  void SetElectromagneticList(G4String newValue);

  //! allows to choose the physics list for inelastic hadronic processes
  /*!
    Those are the lists to choose from:
    - FTFP_BERT_TRV  Fritof with Bertini cascade for particles below 3 GeV
    - FTFP_BERT      Fritof with Bertini cascade for particles below 5 GeV
    - FTF_BIC        Fritof with binary cascade
    - QGSP_FTFP_BERT quark gluon string model with fritof and  Bertini cascade
    - QGSP_BERT      quark gluon string model with Bertini cascade
    - QGSP_BIC       quark gluon string model with binary cascade

    FTFP_BERT_TRV ist the default choice
   */
  void SetHadronicList       (G4String newValue);

  //! Select the Antiproton annihilation code
  /*!
    - If set to true the SMI antiproton annihilation code is used.
    - If set to false the G4AntiProtonAbsorptionFritiof is used. In Geant4.10 
    this is the only choice
   */
  void SetUseSMICode         (G4String newValue);

private:

  //! Adds models for low energetic particles in gases
  void AddIonGasModels();

  //! constructs the Geant4 step limiter process
  void AddStepLimiter();       

  //! Add processes for Hydrogen, Antihydrogen and  choose between pbar codes
  void AddHydrogenAntihydrogenProcess();

  //! Adds the transportationn processes
  /*!
    in case of Hydrogen and Antihydrogen this function swiches on the
    EnableUseMagneticMoment flag which allows the equation of motion to
    track neutral particles with a magnetic moment.
   */
  void AddModTransportation();

  G4double                    fCutForGamma;       //!< cut value for gammas
  G4double                    fCutForElectron;    //!< cut value for e-
  G4double                    fCutForPositron;    //!< cut value for e+
    
  G4VPhysicsConstructor*      fEmPhysicsList;     //!< EM physics list
  G4VPhysicsConstructor*      fDecPhysicsList;    //!< decay physics list
  G4VPhysicsConstructor*      fHadronPhysInElast; //!< inelastic hadronic list
  G4VPhysicsConstructor*      fHadronPhysElst;    //!< elastic hadronic list
    
  G4Cerenkov*          theCerenkovProcess;
  G4Scintillation*     theScintillationProcess;
  G4OpAbsorption*      theAbsorptionProcess;
  G4OpRayleigh*        theRayleighScatteringProcess;
  G4OpMieHG*           theMieHGScatteringProcess;
  G4OpBoundaryProcess* theBoundaryProcess;

  G4UserPhysicsListMessenger* fMessenger;         //!< standard G4 messanger 
  hbarPhysicsListMess*        hbarMessenger;      //!< hbar messanger

  G4bool                      useSMIModel;        //!< use SMI pbar code
  G4bool					  UseOptPhotons;
};


#endif

