#ifndef hbarPrimaryGeneratorAction_h
#define hbarPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "TH1.h" // Added by Clemens

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "G4DataVector.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleGun.hh"
#include "Randomize.hh"

//Added by Clemens -- start
#include "CRYSetup.h"
#include "CRYGenerator.h"
#include "CRYParticle.h"
#include "CRYUtils.h"
#include "vector"
#include "RNGWrapper.hh"
#include "hbarHydrogenLike.hh"
#include "hbarException.hh"
#include "RLException.hh"

#include <vector>
//Added by Clemens -- stop

class G4ParticleGun;
class G4Event;
class hbarDetConst;
class hbarPrimGenMess;

//! generate the primary particles
/*!
  This class generates the primary particles as Hbars, Pions etc. 
  According to the set limitations (set by macros)
 */
class hbarPrimGenAction : public G4VUserPrimaryGeneratorAction
{
public:
  hbarPrimGenAction(hbarDetConst*);
  ~hbarPrimGenAction();

  void PrepareGroundState(hbarHydrogenLike * particle ///The particle to prepare.
						  ); ///Prepares the ground state, based on the macro used.

  //! Generate primary particles
  /*!
    When using background simulations, this functions calculates the 
    ordering of the background particles and dispaches them for generation
    by GenerateSinglePrimaries.

    E.g:

    If we simulate 5 primary hbars, together with 3 antiprotons per Hbar and 
    100 cosmics per hbar the order in which the particles are generated is 
    randomly distributed.
   */
  void GeneratePrimaries(G4Event*);
  //! Generate single primary particles
  void GenerateSinglePrimaries(G4Event* anEvent); // Changed by Clemens (old GeneratePrimaries)
  // Added by Clemens -- start
  //! Apply specific CRY command to the simulation (refere to CRY library for this)
  void UpdateCRY(std::string* MessInput);
  //! Read a CRY configuration from a file
  void CRYFromFile(G4String newValue);
  // Added by Clemens -- stop

  void SetRndmEnergyFlag(G4String val) { rndmFlagEnergy = val;}
  void SetRndmStateFlag(G4String val) { rndmFlagState = val;}
  void SetRndmRydbergFlag(G4bool val) { rndmRydbergState = val;}

  void SetRndmDirectionFlag(G4String val)  {rndmFlagDir = val;}

  void SetQuantumTransitionFlag(G4String val) { quantumTransitionFlag = val;}
  void SetPreloadQuantumStatesFlag(G4String val) { preloadQuantumStates = (val == "on");}
  void SetEnableFieldRegimeTranslations(G4bool val) { enableFieldRegimeTranslations = val; }
  void SetUseStrongInitialState(G4bool val) { useStrongInitialState = val; }
  void SetQuantumEnergyForceFlag(G4String val) { quantumEnergyForceFlag = val;}
  void SetQuantumStateFile(G4String val) { quantumStateFile = val;}

  void SetSourceStateN(G4int val) { StateN = val; }
  void SetSourceStateL(G4int val) { StateL = val; }
  void SetSourceStateMaxN(G4int val) { MaxStateN = val; }
  void SetSourceStateMaxL(G4int val) { MaxStateL = val; }
  void SetSourceStateTwoMJ(G4int val) { StateTwoMJ = val; }
  void SetSourceStateTwoJ(G4int val) { StateTwoJ = val; }
  void SetSourceStateML(G4int val) { StateML = val; }
  void SetSourceStateTwoMS(G4int val) { StateTwoMS = val; }

  void SetSourceCenter(G4double val)  { SourceCenter = val;}
  void SetBeamKinE(G4double val)  { BeamKinE = val;}
  void SetSourceTemperature(G4double val)  {SourceTemp = val;}
  void SetRndmSourceVelocity(G4double val)  {rndmSourceVel = val;}
  void SetSliceFlag(G4String val) { sliceFlag = val;}
  void SetSourceFWHM_X(G4double val)  { SourceFWHM_X = val;}
  void SetSourceFWHM_Y(G4double val)  { SourceFWHM_Y = val;}
  void SetSourceFWHM_Z(G4double val)  { SourceFWHM_Z = val;}
  void SetSourceFWHM(G4double val)  { SourceFWHM_X = SourceFWHM_Y = SourceFWHM_Z = val;}
  void SetSourceOffsetX(G4double val)  { SourceOffsetX = val;}
  void SetSourceOffsetY(G4double val)  { SourceOffsetY = val;}
  void SetSourceMaximumRadius(G4double val)  { SourceMaxR = val;}
  void SetSourceOpeningAngle(G4double val)  { openAngle = val;}
  void SetProductionRate(G4double val)  { prodRate = val;}
  void SetSourceStateF(G4int val)  { StateF = val;}
  void SetSourceStateM(G4int val)  { StateM = val;}
  void SetSourceStateARatio(G4double val)  { StateARatio = val;}
  void SetSourceStateBRatio(G4double val)  { StateBRatio = val;}
  void SetSourceStateCRatio(G4double val)  { StateCRatio = val;}
  void SetSourceStateDRatio(G4double val)  { StateDRatio = val;}
  void SetBeamDirection(G4ThreeVector val)  { BeamDir = val;}
  void SetIsResetTimer(G4bool val)  { timeToReset = val;}


  void SetLowerHysteresis(G4double val){ lowerHysteresis = val; }
  void SetUpperHysteresis(G4double val){ upperHysteresis = val; }

  //    void SetBeamSizeH(G4double val)  { BeamSizeH = val;}
  //    void SetBeamSizeV(G4double val)  { BeamSizeV = val;}
  //    void SetBeamDivH(G4double val)  { BeamDivH = val;}
  //    void SetBeamDivV(G4double val)  { BeamDivV = val;}
  void SetParticle(G4String);

  void ResetNumberOfParticles() {nPart = 0;};
  void ResetTimer() {prodTime = 0.0*ns;};


  G4int GetNumberOfParticles() {return nPart;};
  G4String GetParticleName() {return particleName;};
  G4String GetRndmEnergyFlag() {return rndmFlagEnergy;};
  G4double GetRndmSourceVelocity() {return rndmSourceVel;};
  G4String GetRndmStateFlag() {return rndmFlagState;};
  G4bool GetRndmRydbergFlat() { return rndmRydbergState; }

  G4String GetRndmDirectionFlag() {return rndmFlagDir;};


  G4String GetQuantumTransitionFlag() { return quantumTransitionFlag; }
  G4bool   GetPreloadQuantumStatesFlag() { return preloadQuantumStates; }
  G4bool GetEnableFieldRegimeTranslations() { return enableFieldRegimeTranslations;}
  G4bool GetUseStrongInitialState() { return useStrongInitialState; }
  G4String GetQuantumEnergyForceFlag() { return quantumEnergyForceFlag; }
  G4String GetQuantumStateFile() { return quantumStateFile; }

  G4String GetSliceFlag() {return sliceFlag;};
  G4double GetSourceCenter() {return SourceCenter;};
  G4double GetSourceTemperature() {return SourceTemp;};
  G4double GetBeamKineticEnergy() {return BeamKinE;};
  G4double GetSourceFWHM_X() {return SourceFWHM_X;};
  G4double GetSourceFWHM_Y() {return SourceFWHM_Y;};
  G4double GetSourceFWHM_Z() {return SourceFWHM_Z;};
  G4double GetSourceOffsetX() {return SourceOffsetX;};
  G4double GetSourceOffsetY() {return SourceOffsetY;};
  G4double GetSourceMaximumRadius() {return SourceMaxR;};
  G4double GetSourceOpeningAngle() {return openAngle;};
  G4double GetProductionRate() {return prodRate;};
  G4double GetProductionTime() {return prodTime;};
  G4int GetSourceStateF() {return StateF;};
  G4int GetSourceStateM() {return StateM;};
  G4int GetSourceStateN() { return StateN; }
  G4int GetSourceStateTwoJ() { return StateTwoJ; }
  G4int GetSourceStateTwoMJ() { return StateTwoMJ; }
  G4int GetSourceStateML() { return StateML; }
  G4int GetSourceStateTwoMS(){ return StateTwoMS; }
  G4int GetSourceStateL() { return StateL; }


  G4double GetLowerHysteresis(){ return lowerHysteresis; }
  G4double GetUpperHysteresis() { return upperHysteresis; }



  G4ThreeVector GetBeamDirection() {return BeamDir;};
  G4bool IsResetTimer() {return timeToReset;};

  //! Set to use the CRY library
  void SetUseCRY(G4bool val) {useCRY = val;} 
  //! Set the CRY emittance region displacement from (0,0,0)
  void SetCRYDisplacement(G4ThreeVector val) {CRYDisplacement = val;}

  //! Use a Z velocity distribution as provided by AEgIS
  void SetuseAEgIS(G4bool val); 
  //! Set the filename of the AEgIS spectrum
  void SetSpectrumFilename(G4String filename);
  //! Open the AEgIS spectrum root file 
  G4bool open_root_spectrum(G4String filename);

  //! Use a particle mixture for macground simulations
  void SetuseParticleMixture(G4bool val){useParticleMixture = val;}
  //! Add a background particle to the simulation
  void addBGParticle(G4String partname);
//! Add a background particle as number, to be used in conjunction with addBGParticle
  void addBGParticleNum(G4int num);

private:
  G4ParticleGun*                particleGun;	//!< pointer a to G4 service class
  hbarDetConst*                 hbarDetector;   //!< pointer to the geometry
  G4String                      particleName;   //!< name of the current particle

  hbarPrimGenMess*              gunMessenger;   //!< messenger of this class

  G4String                      rndmFlagEnergy; //!< flag for a random start energy
  G4String                      rndmFlagState;  //!< flag for a random start state (F,M)
  G4bool                        rndmRydbergState; //!< Random rydberg state.
  G4String                      rndmFlagDir;    //!< flag for shooting only towards the magnet

  G4String                      quantumStateFile; ///Flag for setting file containing quantum states (produced by RFAC).
  G4String                      quantumEnergyForceFlag; ///Flag for setting quantum energy force.
  G4String                      quantumTransitionFlag; ///Flag for using quantum transitions.
  G4bool                        preloadQuantumStates; ///Flag for preload quantum states.
  G4bool                        enableFieldRegimeTranslations; ///Flag to disable field regime translations.
  G4bool                        useStrongInitialState;


  G4double                      SourceCenter;   //!< Position of the source anlong beam axis
  G4double                      BeamKinE;       //!< Kinetic energy of the beam
  G4double                      SourceTemp;     //!< Temperature of the Source (in Kelvin)
  G4double                      rndmSourceVel;  //!< intermediate storage variable for the velocity
  G4String                      sliceFlag;      //!< ?
  G4double                      SourceFWHM_X;   //!< FWHM of the Hbar source in X (beam) direction
  G4double                      SourceFWHM_Y;   //!< FWHM of the Hbar source in Y direction
  G4double                      SourceFWHM_Z;   //!< FWHM of the Hbar source in Z direction
  G4double                      SourceOffsetX;  //!< Offset of the source in X direction
  G4double                      SourceOffsetY;  //!< Offset of the source in Y direction
  G4double                      SourceMaxR;     //!< cutoff radius around the source
  G4double                      openAngle;      //!< opening angle of the beam in degrees
  G4double                      prodRate;       //!< production rate of Hbar (Hz)
  G4double                      prodTime;       //!< time when the next Hbar is produced
  G4double                      StateARatio;    //!< production ratio F=0 M=0
  G4double                      StateBRatio;    //!< production ratio F=1 M=1
  G4double                      StateCRatio;    //!< production ratio F=1 M=0
  G4double                      StateDRatio;    //!< production ratio F=1 M=-1
  G4int                         StateF;         //!< Quantum number F
  G4int                         StateM;         //!< Quantum number M
  G4int                         StateN;         //!< Quantum number N
  G4int                         StateL;         //!< Quantum number L
  G4int                         MaxStateN;      /// Maximum N (for randomizing).
  G4int                         MaxStateL;      /// Maximum L (for randomizing).
  G4int                         StateTwoMJ;        //! Quantum number MJ
  G4int                         StateTwoJ;         //!< Quantum number J
  G4int                         StateML;        /// Quantum number ML.
  G4int                         StateTwoMS;     ///Quantum number MS.


  G4double                      lowerHysteresis;
  G4double                      upperHysteresis;

  G4int                         nPart;          //!< number of particles shooted
  G4ThreeVector                 BeamDir;        //!< direction of the beam
  G4bool                        timeToReset;    //!< whether to reset particle production time

  // in the beginning of each run

  // Added by Clemens -- start
  std::vector<CRYParticle*>    *vect;           //!< vector of generated particles
  CRYGenerator*                 gen;            //!< the CRY particle generator
  G4bool                        useCRY;         //!< flag to use CRY
  G4String                      cryfilename;    //!< filename of a CRY config file 
  G4ThreeVector                 CRYDisplacement;//!< position of the CRY emittance area
  
  TH1D                          velspectr;      //!< Velocity spectrum in Z direction for AeGiS simulations 
  G4String                      spectrumfilename;//!< Filename if the velocity distrivution spectrum (a ROOT file)
  G4bool                        useAEgISData;   //!< Flag to use the velocity spectrum for particle generation
  G4bool                        fileisopen;     //!< Flag if the velocity spectrum file is already open
  
  G4bool                        useParticleMixture; //!< Flag to simulate a mixture of particles
  std::vector<G4String>         particleList;   //!< The list of particles to simulate
  std::vector<G4double>         particleRatio;  //!< The ratio of background particles with respect to the primary particle
  // Added by Clemens -- stop

  const G4double                k_B;            //!< Boltzmann's constant in J/K
  const G4double                eVJ;            //!< 1 eV = evJ joule
  const G4double                clight;         //!< c in m/s

  void set_quantum_state(hbarHydrogenLike* particle);
};

#endif


