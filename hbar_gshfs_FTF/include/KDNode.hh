#include <iostream>
#include <math.h>
#include <algorithm>
#include <cstdlib> 
#include <fstream>
#include <tr1/memory>

#define DIM 3      //!< space dimensions
#define columns 6  //!< number of columns e.g. 6 for x,y,z,Bx,By,Bz

/////////////////////////////////////////////////////////////////////////////////////////
//!<  With the constructor KDNode(double[][columns] Matrix, int length, int depth) 
//!<	a KD-Tree is built out of the data stored in Matrix. 
//!<  A Kd-Tree is is a data structure (special binary tree) which speeds up the 
//!<  search for the nearest neighbour. Each Node of this tree contains:                                                                
//!<  - a variable 'Axis' which describes a splitting hyperplane which is parallel to an 
//!<    axis (x, y or z) and will be swapped for every level 'depth' of the tree         
//!<    according to: depth%DIM (cycling).											   
//!<    e.g. nodes from the first level of the tree are splited to left and right	  	   
//!<    with respect to the first dimension (x), the second level with respect	 	    
//!<    to to the second dimension (y) and so on.                                        
//!<                                                                                     
//!<  - The RightChild of the Node will contain only Points 'larger' (in front) of the   
//!<    plane an the LeftChild only points 'smaller' (behind) the plane. The position    
//!<    of the plane is chosen by bisectioning all the children.						   
//!<																					   
//!<  - The Point at which the splitting plane is set.                                   																			       
/////////////////////////////////////////////////////////////////////////////////////////

class KDNode :  public std::tr1::enable_shared_from_this<KDNode> {
	private:
		int Depth;      //!< Depth of Tree
		int Axis;       //!< split plane dimension (x,y,z)
		bool checked;	//!< if the node has already been found to be nearest neighbour, 
						//!< checked is set to 1 (in case der number 'neighboursnumber' in hbarCavityField is larger than 1)
		double Point[columns];  //!< x, y, z, Bx, By, Bz

		std::tr1::shared_ptr<KDNode> LeftChild;  //!< Pointer to the left child of the current node
		std::tr1::shared_ptr<KDNode> RightChild; //!< Pointer to the right child of the current node

	public:
		KDNode();            	     //!< Default constructor (just one node is built)
		KDNode(double**,int,int);    //!< builds the Tree
		~KDNode();
		
		void SetLeftChild(std::tr1::shared_ptr<KDNode>);  //!< Setter und Getter
		void SetRightChild(std::tr1::shared_ptr<KDNode>);
		void Setchecked(bool);
		bool Getchecked(); 
		double* GetPoint();
		double GetPoint(int);
		int GetAxis();
		std::tr1::shared_ptr<KDNode> GetLeftChild();
		std::tr1::shared_ptr<KDNode> GetRightChild();
		int GetDepth();

		KDNode& operator=(KDNode const&); //!< overloaded assignment operator		
};

//void MergeSort(double[][columns], int, int); //!< Merge-Sort algorithm (descending), used in the constructor which builds the tree
void MergeSort(double**, int, int); //!< Merge-Sort algorithm (descending), used in the constructor which builds the tree
//std::tr1::shared_ptr<KDNode> Approx_Neighbour(std::tr1::shared_ptr<KDNode>,double*);
//std::tr1::shared_ptr<KDNode> Check_Sphere(std::tr1::shared_ptr<KDNode>, std::tr1::shared_ptr<KDNode>, double*); // checks sphere arround approx. neighbour
//double Distance(double*, double*);                // calculates distance between two points in DIM dimensions
//std::tr1::shared_ptr<KDNode> Nearest_Neighbour(std::tr1::shared_ptr<KDNode>,double*);       // determines nearest neighbours
