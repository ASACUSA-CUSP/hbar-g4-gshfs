#ifndef hbarAntiProtonAnnihilationAtRest_h
#define hbarAntiProtonAnnihilationAtRest_h 1
 
// Class Description
// Process for annihilation of p-bar at rest; 
// to be used in your physics list in case you need this physics.
// Class Description - End

#include <vector>

#include "globals.hh"
#include "Randomize.hh" 
#include "G4VRestProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleDefinition.hh"
#include "G4GHEKinematicsVector.hh"
#include "G4String.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

class hbarGeneralPhaseSpaceDecay;
class hbarAntiProtonMess;

//! This class describes the decay of antiprotons
/*!
  The decay of antiprotons is faulty in standard geant4 for low energy
  antiprotons, therefore Berti created this class to define the correct
  decay channels and branching ratios, accroding to:  

  *M. Hori, K. Yamashita, R.S. Hayano, T. Yamazaki, Nucl. Instrum. Methods A 496 (2003) 192.*

  Quote Berti:

  The kaon channels, however, are not used, only the pion channels!

  In case of materials which are made of multiple elements, the capture 
  (and thus annihilation) probability on a nucleus scales as:
  
  \f$ \sigma \approx Z^{(2/3)} \f$.
  
  taken from here (see the last line on page 1793):
  
  *J.S. Cohen, Rep. Prog. Phys. 67 (2004) 1769-1819.*

  After the annihilation pions are created, they can be absorbed or (inelastically) scattered within the nucleus on which the antiproton annihilated. I implemented a very rough quasi-free scattering model which does not take into account angular deflection, but I believe for this simulation program the pion angular scattering is much less important than the energy loss and absorption. The scattering model is based on the following papers:
  
  *J. H�fner, M. Thies, PRC 20 (1979) 273*

  *J. Cugnon, J. Vandermeulen, NPA 445 (1985) 717*

  *J. Cugnon et al., NPA 470 (1987) 558*

  *J. Cugnon et al., PRC 63 (2001) 027301*
  */
class hbarAntiProtonAnnihilationAtRest : public G4VRestProcess
 
{ 
private:
  //! maximum number of secondary particles produced, this is a CUT!
  static const int MAX_SECONDARIES = 100;

  std::vector<hbarGeneralPhaseSpaceDecay*> channelsProton;
  std::vector<hbarGeneralPhaseSpaceDecay*> channelsNeutron;
  G4double sumOfProbabilitiesProton;
  G4double sumOfProbabilitiesNeutron;

  // hide assignment operator as private 
  hbarAntiProtonAnnihilationAtRest& operator=(const hbarAntiProtonAnnihilationAtRest &right);

  
  hbarAntiProtonAnnihilationAtRest(const hbarAntiProtonAnnihilationAtRest& );

  //! parameter of the nuclear absorption of pions
  G4double pionAbsorptionLength;
  //! parameter of the nuclear absorption of pions
  G4double pionMomentumDegradation;

public:
 
  hbarAntiProtonAnnihilationAtRest(const G4String& processName ="hbarAntiProtonAnnihilationAtRest");
  ~hbarAntiProtonAnnihilationAtRest();

  //! true if particle is AntiProton
  G4bool IsApplicable(const G4ParticleDefinition&);

  //! null physics table
  void BuildPhysicsTable(const G4ParticleDefinition&){}

  G4double AtRestGetPhysicalInteractionLength(const G4Track&,
					      G4ForceCondition*);

  //! zero mean lifetime
  G4double GetMeanLifeTime(const G4Track& ,
			   G4ForceCondition* ) {return 0.0;}

  //! perform the Antiproton decay
  /*!
    This is the main function in this class, if checks under what conditions
    the article interacts and defines what to do then.
   */
  G4VParticleChange* AtRestDoIt(const G4Track&, const G4Step&); 

  //! return number of secondaries produced
  G4int GetNumberOfSecondaries();

  // pointer to array containg kinematics of secondaries
  //    G4GHEKinematicsVector* GetSecondaryKinematics();

  //! flag Pion absorption
  void SetApplyPionAbsorption(G4bool value) {ApplyPionAbsorption = value;};
  //! Set Iion absorption
  void SetApplyPionAbsorption(G4String value);
  void SetDebugFileName(G4String value) {DebugFileName = value;};

private:

  void GenerateSecondaries();
  void Normal( G4float* ); //!< normal distribution
  //G4double ExNu( G4float );
  G4int NFac( G4int );     //!< factorial of n

private:

  //! global time-of-flight of stopped AntiProton
  G4float  globalTime;

  // atomic mass of target nucleus
  //    G4float  targetAtomicMass;

  //! atomic number of target nucleus = number of protons
  G4float  targetZ;
    
  //! number of nucleons (i.e. mass number) in target nucleus (averaged over all isotopes)
  G4float  targetA;
    
  //! number of neutrons in target nucleus (averaged over all isotopes)
  G4float  targetN;
    
  //    G4GHEKinematicsVector secondaryKinematics[MAX_SECONDARIES];
  G4DecayProducts* secondaryProducts; //!< pointer to decay products
    
  //    G4float  evapEnergy1;
  //    G4float  evapEnergy3;
  
  //! number of secondary particles produced
  G4int    nofSecondaries;
    
  /***
      G4float  massPionMinus;
      G4float  massProton;
      G4float  massPionZero;
      G4float  massAntiProton;
      G4float  massPionPlus;
      G4float  massGamma;
  ***/

  G4ParticleDefinition* pdefAntiProton; //!< pointer to definition of Anti Proton
  G4ParticleDefinition* pdefNeutron;    //!< pointer to definition of Neutron
  /****    
	   G4ParticleDefinition* pdefGamma;
	   G4ParticleDefinition* pdefPionPlus;
	   G4ParticleDefinition* pdefPionZero;
	   G4ParticleDefinition* pdefPionMinus;
	   G4ParticleDefinition* pdefProton;
	   G4ParticleDefinition* pdefDeuteron;
	   G4ParticleDefinition* pdefTriton;
	   G4ParticleDefinition* pdefAlpha;
  ****/

  hbarAntiProtonMess *pbarMess;         //!< pointer to hbarAntiProtonMess class

  //! flag to apply pion absorption
  static G4bool   ApplyPionAbsorption;
  //! provide debug filename
  static G4String DebugFileName;
};

#endif
 
