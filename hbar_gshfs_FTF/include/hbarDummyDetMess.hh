#ifndef hbarDummyDetMess_hh
#define hbarDummyDetMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarDummyDetectorConst.hh"


class hbarDummyDetectorConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class hbarDummyDetMess : public G4UImessenger
{
public:
  hbarDummyDetMess(hbarDummyDetectorConst* );
  ~hbarDummyDetMess();
  
  void SetNewValue(G4UIcommand*, G4String);
private:
  G4UIcmdWithADoubleAndUnit* DetLengthCmd;          //!< setDetectorLength (dummy detector)
  G4UIcmdWithADoubleAndUnit* DetDiamCmd;            //!< setDetectorDiameter (dummy detector)
  G4UIcmdWithADoubleAndUnit* DetCenterCmd;          //!< setDetectorCenter (dummy detector)
  hbarDummyDetectorConst * hbarDummyDet;
};


#endif
