#ifndef hbarCavityMess_hh
#define hbarCavityMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarCavityConst.hh"


class hbarCavityConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class hbarCavityMess : public G4UImessenger
{
public:
  hbarCavityMess(hbarCavityConst* );
  ~hbarCavityMess();
  
  void SetNewValue(G4UIcommand*, G4String);

  G4UIcmdWithABool* GetUseCavityFieldMap() {return UseCavityFieldMapCmd;};


private:
  hbarCavityConst *               hbarCavity; ///Pointer to the cavity part of the detector.


  G4UIcmdWithAString*        CavityFieldFileCmd;  
  G4UIcmdWithABool*          UseCavityFieldMapCmd;

  G4UIcmdWithADoubleAndUnit* CavityLengthCmd;       //!< setCavityLength
  G4UIcmdWithADoubleAndUnit* CavityInnerDiamCmd;    //!< setCavityInnerDiameter
  G4UIcmdWithADoubleAndUnit* CavityOuterDiamCmd;    //!< setCavityOuterDiameter
  G4UIcmdWithADoubleAndUnit* CavityCenterCmd;       //!< setCavityCenter
  G4UIcmdWithADoubleAndUnit* CavityResThickCmd;     //!< setResonatorThickness
  G4UIcmdWithADoubleAndUnit* CavityResSepCmd;       //!< setResonatorSeparation
  G4UIcmdWithADoubleAndUnit* CavityResWidthCmd;     //!< setResonatorWidth
  
  G4UIcmdWithADoubleAndUnit* HelmholtzCoilsInnerDiamCmd;
  G4UIcmdWithADoubleAndUnit* HelmholtzCoilsOuterDiamCmd;
  G4UIcmdWithADoubleAndUnit* HelmholtzCoilThicknessCmd;
  G4UIcmdWithADoubleAndUnit* HelmholtzCoilDistanceCmd;
  G4UIcmdWithADoubleAndUnit* SupportRingHHOuterDiamCmd;
  G4UIcmdWithADoubleAndUnit* SupportRingHHOuterThickCmd;
  
  G4UIcmdWithADoubleAndUnit* ShieldingThicknessCmd;
  G4UIcmdWithADoubleAndUnit* InnerLayerShieldLengthCmd;
  G4UIcmdWithADoubleAndUnit* InnerLayerShieldHightCmd;
  G4UIcmdWithADoubleAndUnit* MiddleLayerShieldLengthCmd;
  G4UIcmdWithADoubleAndUnit* MiddleLayerShieldHightCmd;
  G4UIcmdWithADoubleAndUnit* ShieldHoleDiamCmd;
  
  G4UIcmdWithABool*			 UseCavityInnerShieldingCmd;
  G4UIcmdWithABool*			 UseCavityMiddleShieldingCmd;
  G4UIcmdWithABool*			 UseHelmholtzCoilsCmd;
  G4UIcmdWithABool*			 UseSupportRingsHHCmd;

};


#endif
