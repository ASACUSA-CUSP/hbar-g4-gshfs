
#ifndef hbarSiPM_SD_h
#define hbarSiPM_SD_h 1

#include <algorithm> 
// USER //
//#include "SimpleHit.hh"
#include "hbarDetectorHit.hh"
#include "TH1.h"

// GEANT4 //
#include "G4VSensitiveDetector.hh"
#include "G4Track.hh"
#include "G4SystemOfUnits.hh"
#include "G4Step.hh"
#include "G4SteppingManager.hh"
#include "Randomize.hh"

#include "G4HCofThisEvent.hh"

#include "G4HCtable.hh"
#include "G4SDManager.hh"

#include "TCanvas.h"
#include "TError.h"
#include "TGraph.h"
#include "G4Run.hh"
#include "G4RunManager.hh"

#include "hbarCPTConst.hh"



// ROOT //
#include "TFile.h"
//#include "G4THitsCollection.hh"

class G4HCofThisEvent;
class hbarCPTConst;
//class hbarDetConst;
class hbarRunAction;



// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0

class hbarSiPM_SD : public G4VSensitiveDetector
{
public:
  hbarSiPM_SD(G4String SDname, hbarCPTConst* cptconst); //!< constructor
  ~hbarSiPM_SD(); //!< destructor
  G4bool ProcessHits(G4Step* , G4TouchableHistory * ROhist); //!<
  void Initialize(G4HCofThisEvent* ); //!<
  void EndOfEvent(G4HCofThisEvent* ); //!<
  //// Getter:
  double* Get_mult_fft_rl();
  double* GetHodorWaveform();
  std::vector<double> Get_response_time();
  G4String GetSiPMName() {return SiPMName;};
  //// Setter:
  void Set_drawhistos(G4bool useflag) {drawhistos = useflag;};
  void Set_write_to_dat_files(G4bool useflag) {write_to_dat_files = useflag;};

private:
  hbarDetectorHitsCollection* hitCollection; //!< currently unused hitcollection object
  G4Track * track; //!< current track
  G4int hitCounter; //!< counts photon hits in sipms
  G4String volName; //!< name of volume
  G4String SiPMName;
  std::vector<G4double> SiPM_up_timeData; //!< save arrival times of photons for upstream sipms
  std::vector<G4double> SiPM_down_timeData; //!< save arrival times of photons for downstream sipms
  std::vector<G4double> SiPM_up_energyData; //!< save energies of photons for upstream sipms
  std::vector<G4double> SiPM_down_energyData; //!< save energies of photons for downstream sipms
  
  G4bool drawhistos;
  G4bool write_to_dat_files;
  
  std::vector<double> response_time; //!< x values of response function
  std::vector<double> response_sig; //!< y values of response function
  double mult_fft_rl[4096];
  double mult_fft_img[4096];
  double HodorWaveform[1024];
  TH1D* histo_resp;  //!< histogram of response function
  TGraph* HodorWaveformgr;
	
  hbarCPTConst* localcptconst;

  bool CalculatePDE(double wl); //!< pass wavelength of photon to function; calculates wether photon gets detected (true) or not (false)
};


#endif

