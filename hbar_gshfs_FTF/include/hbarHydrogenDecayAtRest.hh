#ifndef hbarHydrogenDecayAtRest_h
#define hbarHydrogenDecayAtRest_h 1
 
#include "globals.hh"
#include "Randomize.hh" 
#include "G4VRestProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleDefinition.hh"
#include "G4GHEKinematicsVector.hh"
#include "hbarHydrogenLikeDecayAtRest.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

//! Process of atomic hydrogen recombination to H2 molecules
class hbarHydrogenDecayAtRest : public hbarHydrogenLikeDecayAtRest
 
{ 
private:
  // hide assignment operator as private 
  hbarHydrogenDecayAtRest& operator=(const hbarHydrogenDecayAtRest &right);

  //! copy constructor
  hbarHydrogenDecayAtRest(const hbarHydrogenDecayAtRest& );


protected:
  //! Name of particle.
  virtual G4String GetThisProcessParticleName() {return "hydrogen";}
  //! Number of secondary particles created.
  virtual G4int GetInitialNumberOfSecondaries(){ return 0;}
public:
   //! Constructor, initializes all variables. Currently does nothing.
  hbarHydrogenDecayAtRest();
   //! Destructor frees memory after particle is killed. Currently does nothing.
  ~hbarHydrogenDecayAtRest();
};
#endif
