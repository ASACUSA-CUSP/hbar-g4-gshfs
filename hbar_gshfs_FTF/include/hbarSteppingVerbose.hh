class hbarSteppingVerbose;

#ifndef hbarSteppingVerbose_h
#define hbarSteppingVerbose_h 1

#include "G4SteppingVerbose.hh"
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"

//! helper class to collect information about secondary particles
class hbarSteppingVerbose:public G4SteppingVerbose
{
public:
  //! constructor
  hbarSteppingVerbose();
  //! destructor
  ~hbarSteppingVerbose();
  //! collects the step information
  /*!
    Fills a root TTree with information on secondary particles
   */
  void StepInfo();

  // not used
  void TrackingStarted();
    
private:
  TTree *SPTree;           //!< TTree to fold the date
  TFile *SPfile;           //!< TFile for creating a ROOT file
  Char_t Secondaries[20];  //!< name of the secondary particle
};
#endif
