#ifndef hbarEventActionMessenger_h
#define hbarEventActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarEventAction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;

//! The Event Action messenger class
/*!
  This a messenger class which defines user commands for the events, e.g. 
  what information to print and which particle trajectories to draw. 

  \b ATTENTION! It now also defines some commands which seem to set 
  various sensitive detector trigger levels and coincidences, but actually 
  these commands don't do anything. All the sensitive detector hits are
  written into the output ROOT file, and selecting them via trigger levels 
  and coincidence is done in the analysis ROOT macro.

  The macros defined in this class:
  - /event/drawTracks
  - /event/printModulo
  - /event/printPosInfo
  - /event/printEventInfo
  - /trigger/setCoincidenceMultiplicity
  - /trigger/setAntiCoincidenceMultiplicity
  - /trigger/setTriggerLevel
*/
class hbarEventActionMess: public G4UImessenger
{
public:
  hbarEventActionMess(hbarEventAction*);
  ~hbarEventActionMess();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  hbarEventAction*           eventAction;      
  G4UIcmdWithAString*        DrawCmd;          //!< drawTracks
  G4UIcmdWithAnInteger*      PrintCmd;         //!< printModulo
  G4UIcmdWithAString*        PosInfoCmd;       //!< printPosInfo
  G4UIcmdWithAString*        EventInfoCmd;     //!< printEventInfo
  
  G4UIdirectory*             triggerDir;       //!< /trigger/
  G4UIcmdWithAnInteger*      CoinMultiCmd;     //!< setCoincidenceMultiplicity
  G4UIcmdWithAnInteger*      AntiCoinMultiCmd; //!< setAntiCoincidenceMultiplicity
  G4UIcmdWithADoubleAndUnit* TriggerLevelCmd;  //!< setTriggerLevel
};

#endif
