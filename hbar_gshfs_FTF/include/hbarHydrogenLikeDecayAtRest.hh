#ifndef hbarHydrogenLikeDecayAtRest_h
#define hbarHydrogenLikeDecayAtRest_h 1

#define MAX_SECONDARIES 100

#include <string.h>
#include <math.h>
#include <stdio.h>

#include "globals.hh"
#include "Randomize.hh" 
#include "G4VRestProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleDefinition.hh"
#include "G4GHEKinematicsVector.hh"

#include "G4DynamicParticle.hh"
#include "G4ParticleTypes.hh"
#include "Randomize.hh" 


//! Process of atomic hydrogen/antihydrogen/similar recombination to H2 molecules. Base class to centralize identical methods.
//! T must be a class extending this template class, as is assured by the BOOST_STATIC_ASSERT.
class hbarHydrogenLikeDecayAtRest : public G4VRestProcess
{ 
private:
  //! hide assignment operator as private 
  hbarHydrogenLikeDecayAtRest& operator=(const hbarHydrogenLikeDecayAtRest &right);
  //! copy constructor
  hbarHydrogenLikeDecayAtRest(const hbarHydrogenLikeDecayAtRest& );
  
protected:
  //! Name of particle, for example hydrogen.
   virtual G4String GetThisProcessParticleName() = 0;
  //! Number of secondary particles created by this class. Example: zero
  virtual G4int GetInitialNumberOfSecondaries() = 0;
  
  //! If the specific particle requires some special stuff done at rest, override this. The function body here is to avoid compiler warnings.
  virtual void AtRestDoItAdditional(const G4Track& track)  {(void)track;};
  
  //! Constructor, insitilaizes all variables. We should have no public constructors.
  hbarHydrogenLikeDecayAtRest(const G4String& processName);
  
  //! Destructor frees memory after particle is killed
  ~hbarHydrogenLikeDecayAtRest();
  
public:
  //! test if particle is HydrogenLike
  /*!
    checks the particle name
    /param G4ParticleDefinition& definition of a particle
    /return true if particle is Hydrogen
  */
  G4bool IsApplicable(const G4ParticleDefinition&);
  
  //! null physics table
  void BuildPhysicsTable(const G4ParticleDefinition&){}
  
  
  //! calculates the interaction length of a perticle 
  G4double AtRestGetPhysicalInteractionLength(const G4Track&,
						G4ForceCondition*);
  
  //! zero mean lifetime
  G4double GetMeanLifeTime(const G4Track& ,
			     G4ForceCondition* ) {return 0.0;}
  
  //! perform the annihilation of the particle when hitting something
  /*!
    This function spawns one positron and one antiproton as 
    secondary particle and kills the original antihydrogen
  */
  G4VParticleChange* AtRestDoIt(const G4Track&, const G4Step&); 
  
  //! return number of secondaries produced
  G4int GetNumberOfSecondaries();
  
  //! pointer to array containg kinematics of secondaries
  G4GHEKinematicsVector* GetSecondaryKinematics();
  
private:
  //! Generate the socondary particles after annihilation
  void GenerateSecondaries();
  //! Calculate the Poisson distribution
  void Poisso( G4float, G4int* );
  //! Calculate the Normal distribution
  void Normal( G4float* );
  //! Description of the Antiproton annihilation
  void AntiProtonAnnihilation( G4int* );
  //! Nuclear evaportaion
  G4double ExNu( G4float );
  //! Calculate factorials
  G4int NFac( G4int );

protected:
    G4float                globalTime;     //!< global time-of-flight of stopped AntiProton
  
private:
  

  G4float                targetAtomicMass; //!< atomic mass of target nucleus
  G4float                targetCharge;   //!< charge of target nucleus
  
  
  G4GHEKinematicsVector* pv;             //!< Particle vector of secondaries
  G4GHEKinematicsVector* eve;            //!< Particle vector of secondaries
  G4GHEKinematicsVector* gkin;           //!< Particle vector of secondaries
  
  G4float                evapEnergy1;    //!< evaporation energy
  G4float                evapEnergy3;    //!< evaporation energy
  
  G4int                  ngkine;         //!< the total number of secondary particles
  
  G4int                  ntot;           //!< the number of secondary particles 
  G4GHEKinematicsVector  result;         //!< kinematic defintion of the new particle 
  
  G4float                massPionMinus;  //!< the Pi- mass
  G4float                massProton;     //!< the Proton mass
  G4float                massPionZero;   //!< the Pi0 mass
  G4float                massAntiProton; //!< the Antiproton mass
  G4float                massPionPlus;   //!< the Pi+ mass
  G4float                massGamma;      //!< the Photon mass
  
  G4ParticleDefinition*  pdefGamma;      //!< Particle definition of photons
  G4ParticleDefinition*  pdefPionPlus;   //!< Particle definition of  pi+
  G4ParticleDefinition*  pdefPionZero;   //!< Particle definition of pi0
  G4ParticleDefinition*  pdefPionMinus;  //!< Particle definition of pi-
  G4ParticleDefinition*  pdefProton;     //!< Particle definition of prontons
  G4ParticleDefinition*  pdefAntiProton; //!< Particle definition of antiprotons
  G4ParticleDefinition*  pdefNeutron;    //!< Particle definition of neutrons
  G4ParticleDefinition*  pdefDeuteron;   //!< Particle definition of deuterium
  G4ParticleDefinition*  pdefTriton;     //!< Particle definition of tritium
  G4ParticleDefinition*  pdefAlpha;      //!< Particle definition of helium4 cores

};

#endif
 
