#ifndef hbarBGOMess_hh
#define hbarBGOMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarBGOConst.hh"

/** Redefinitions (erase this comment after a while):

/setup/setNdBGODimension -> /setup/BGO/dimensionRinRoutLen
/setup/setNDflangeDimension -> /setup/BGO/flangeRinRoutLen
/setup/setNDCover -> /setup/BGO/coverNumber
/setup/setNDcoverDimension -> /setup/BGO/coverRinRoutLen
/setup/setNDplsci(something) -> /setup/BGO/scintillatorNumber
/setup/setNDplsciBoxDimension -> /setup/BGO/scintillatorBoxDimensionsXYZ
/setup/setNDplsciTrDim(something) ->
/setup/setNDplsciTrdYLen1 -> 
/setup/setNDplsciTrdYLen2 ->
                              /setup/BGO/scintillatorTrapX1
							  /setup/BGO/scintillatorTrapX2
							  /setup/BGO/scintillatorTrapY1
							  /setup/BGO/scintillatorTrapZ

/setup/setUseNDPLASCIN -> /setup/BGO/enableScintillator
/setup/sensNDPLASCIN -> /setup/BGO/enableSensitiveScintillator
/setup/setNDsupBox -> /setup/BGO/supportBoxNumber
/setup/setNDsupBoxDimension -> /setup/BGO/supportBoxDimensionXYZ
/setup/setUseNDSUPBOX -> /setup/BGO/useSupportbox
/setup/setNDsupBoxDBbar -> /setup/BGO/supportBoxBarDistance
/setup/NagataDetHydroTarget -> /setup/BGO/hydrogenTargetInsteadOfNormal


//Rikard
 */
class hbarBGOConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class hbarBGOMess : public G4UImessenger
{
public:
  hbarBGOMess(hbarBGOConst* );
  ~hbarBGOMess();

  
  void SetNewValue(G4UIcommand*, G4String);
private:
  hbarBGOConst *                hbarBGO; ///Pointer to the CUSP detector.

  G4UIdirectory *               hbarBGODir;

  G4UIcmdWith3VectorAndUnit*	TargetRinRoutLenCmd; 
  G4UIcmdWith3VectorAndUnit*	FlangeRinRoutLenCmd; 

  G4UIcmdWithAnInteger*		    CoverNumberCmd;
  G4UIcmdWith3VectorAndUnit*	CoverRinRoutLenCmd; 

  G4UIcmdWithAnInteger*		    ScintillatorNumberCmd;
  G4UIcmdWithADoubleAndUnit*	ScintillatorTrapezoidX1Cmd; 
  G4UIcmdWithADoubleAndUnit*	ScintillatorTrapezoidX2Cmd; 
  G4UIcmdWithADoubleAndUnit*	ScintillatorTrapezoidY1Cmd; 
  G4UIcmdWithADoubleAndUnit*	ScintillatorTrapezoidY2Cmd; 
  G4UIcmdWithADoubleAndUnit*	ScintillatorTrapezoidZCmd; 
  G4UIcmdWith3VectorAndUnit*	ScintillatorBoxDimensionsXYZ; 
  G4UIcmdWithAnInteger*         EnableScintillatorCmd;
  G4UIcmdWithAnInteger*         EnableSensitiveScintillatorCmd;



  G4UIcmdWithAnInteger*		    SupportBoxNumberCmd;
  G4UIcmdWith3VectorAndUnit*	SupportBoxDimensionXYZCmd; 
  G4UIcmdWith3VectorAndUnit*	SupportBoxBarDistance; 
  G4UIcmdWithAnInteger*         UseSupportBoxCmd;
  G4UIcmdWithAnInteger*         HydroTargetInsteadOfNormalCmd;

};


#endif
