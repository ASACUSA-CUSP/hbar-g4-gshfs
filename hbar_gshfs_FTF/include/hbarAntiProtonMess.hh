//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
// $Id: ExN06PhysicsListMessenger.hh,v 1.1 2003/01/23 15:34:23 maire Exp $
// GEANT4 tag $Name: geant4-05-02-patch-01 $
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef hbarAntiProtonMessenger_h
#define hbarAntiProtonMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarAntiProtonAnnihilationAtRest;
class G4UIdirectory;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//! This is the AntiProton messenger class, for macro definitions
/*!
  The AntiProton messenger defines the following macros:
  - /process/pbar/setPionDebugFileName
  - /process/pbar/setApplyPionAbsorption
 */
class hbarAntiProtonMess: public G4UImessenger
{
public:  
  hbarAntiProtonMess(hbarAntiProtonAnnihilationAtRest* );
  ~hbarAntiProtonMess();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:  
  hbarAntiProtonAnnihilationAtRest*      pbarAnn;    //!< pointer to physics process
  
  G4UIdirectory*         pbarDir;                    //!< /process/pbar/
  G4UIcmdWithAString*    pbarDebugFileNameCmd;       //!< setPionDebugFileName
  G4UIcmdWithAString*    pbarApplyPionAbsorptionCmd; //!< setApplyPionAbsorption
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

