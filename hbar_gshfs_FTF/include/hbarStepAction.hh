#ifndef hbarSteppingAction_h
#define hbarSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "hbarField.hh"
#include "hbarUserTrackInformation.hh"
#include "hbarException.hh"
#include "hbarTrackDictionaryEntry.hh"


class hbarDetConst;
class hbarRunAction;

//!  This class controls what to do at every particle step:
/*!
  When an Hbar atom enters the cavity, start a stopwatch
  When it exits the cavity, stop the stopwatch, and call 
  hbarMWSetup::MakeMWTransition() with the cavity transit time to do 
  the RF spin-flip
  In order to do the Majorana spin-flip transitions, log the magnetic 
  field line angle at the exit of a sextupole magnet and at the entrance 
  of the next sextupole magnet, calculate the difference, and call 
  hbarFieldSetup::MakeMajoranaTransition() with this angle to do the 
  Majorana spin-flips
  
  Besides, there is a bug in GEANT4: sometimes a particle can get stuck 
  in the tracking through a magnetic field. To circumvent this, at every 
  step the position of the particle is checked, and if it is found to be 
  the same as 10 steps ago, then the particle is killed.
 */
class hbarStepAction : public G4UserSteppingAction
{
public:
  
  //! constructor
  hbarStepAction(hbarRunAction*);
  //! destructor
  ~hbarStepAction();
  
  //! checks track conditions at every step
  /*!
    in case of Hydrogen or Antihydrogen this function calles 
    @ref ParticleDependendUserStepAction
  */
  void UserSteppingAction(const G4Step*);

protected:
  

  
private:
  
  hbarRunAction  *runAction; //!< pointer to the hbarRunAction
  hbarDetConst   *detConst;  //!< pointer to the detector setup
  
  hbarField      *Field;     //!< access to field configuration

  //! calculations needed for hydrogen and antihydrogen only
  /*!
    this function:
    - logs entrance and exit time of the microwave cavity
    - logs velocity inside the cavity and just before the dummy detector
    - logs angle to the field lines at the entrace of the sextupole magnet
    - calls the majorana spinflip functions
    - calculates the particles magnetic moment and its gradient
   */

  void ParticleDependentUserStepAction(const G4Step* aStep);
};

#endif
