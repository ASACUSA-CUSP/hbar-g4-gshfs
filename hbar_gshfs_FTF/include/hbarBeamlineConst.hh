#ifndef hbarBeamlineConst_hh
#define hbarBeamlineConst_hh 1

#include "hbarDefinitions.hh"

#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarExtFIField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"

#include "G4ChordFinder.hh"

#include "hbarBeamlineMess.hh"

#include "hbarException.hh"

#include "hbarDefinitions.hh"


class hbarBeamlineMess;

class hbarBeamlineConst
{
public:
  hbarBeamlineConst(); ///Constructor
  ~hbarBeamlineConst(); ///Destructor.
private:
  void ConstructConcreteBlock(G4VPhysicalVolume*);
public:

  //! create beampipes etc.
  void MakeBeamline(G4VPhysicalVolume*);

  void SetBeamPipeStart(G4int, G4double);
  void SetBeamPipeStart(G4double val) {BPStart[BPpt] = val;};
  void SetBeamPipeEnd(G4int, G4double);
  void SetBeamPipeEnd(G4double val) {BPEnd[BPpt] = val;};

  void SetBeamPipeFrontInnerDiameter(G4int num, G4double val);
  void SetBeamPipeRearInnerDiameter(G4int num, G4double val);
  void SetBeamPipeFrontOuterDiameter(G4int num, G4double val);
  void SetBeamPipeRearOuterDiameter(G4int num, G4double val);
  void SetBeamPipeInnerDiameter(G4int num, G4double val) {
    SetBeamPipeFrontInnerDiameter(num, val);
    SetBeamPipeRearInnerDiameter(num, val);
  };
  void SetBeamPipeOuterDiameter(G4int num, G4double val) {
    SetBeamPipeFrontOuterDiameter(num, val);
    SetBeamPipeRearOuterDiameter(num, val);
  };

  void SetBeamPipeFrontInnerDiameter(G4double val) {BPFrontInnerDiam[BPpt] = val;};
  void SetBeamPipeRearInnerDiameter(G4double val)  {BPRearInnerDiam[BPpt] = val;};
  void SetBeamPipeFrontOuterDiameter(G4double val) {BPFrontOuterDiam[BPpt] = val;};
  void SetBeamPipeRearOuterDiameter(G4double val)  {BPRearOuterDiam[BPpt] = val;};
  void SetBeamPipeInnerDiameter(G4double val) {
    SetBeamPipeFrontInnerDiameter(val);
    SetBeamPipeRearInnerDiameter(val);
  };
  void SetBeamPipeOuterDiameter(G4double val) {
    SetBeamPipeFrontOuterDiameter(val);
    SetBeamPipeRearOuterDiameter(val);
  };
  void SetBeamPipeVisible(G4int, G4bool);
  void SetBeamPipeVisible(G4bool val) {visBP[BPpt] = val;};

  void AddBeamPipe();
  void UseBeamPipe(G4int);

  void SetLeadShieldStart(G4int, G4double);
  void SetLeadShieldStart(G4double val) {LSStart[LSpt] = val;};
  void SetLeadShieldEnd(G4int, G4double);
  void SetLeadShieldEnd(G4double val) {LSEnd[LSpt] = val;};


  void SetLeadShieldFrontInnerDiameter(G4int num, G4double val);
  void SetLeadShieldRearInnerDiameter(G4int num, G4double val);
  void SetLeadShieldFrontOuterDiameter(G4int num, G4double val);
  void SetLeadShieldRearOuterDiameter(G4int num, G4double val);
  void SetLeadShieldInnerDiameter(G4int num, G4double val) {
    SetLeadShieldFrontInnerDiameter(num, val);
    SetLeadShieldRearInnerDiameter(num, val);
  };
  void SetLeadShieldOuterDiameter(G4int num, G4double val) {
    SetLeadShieldFrontOuterDiameter(num, val);
    SetLeadShieldRearOuterDiameter(num, val);
  };
  void SetLeadShieldFrontInnerDiameter(G4double val) {LSFrontInnerDiam[LSpt] = val;};
  void SetLeadShieldRearInnerDiameter(G4double val)  {LSRearInnerDiam[LSpt] = val;};
  void SetLeadShieldFrontOuterDiameter(G4double val) {LSFrontOuterDiam[LSpt] = val;};
  void SetLeadShieldRearOuterDiameter(G4double val)  {LSRearOuterDiam[LSpt] = val;};
  void SetLeadShieldInnerDiameter(G4double val) {
    SetLeadShieldFrontInnerDiameter(val);
    SetLeadShieldRearInnerDiameter(val);
  };
  void SetLeadShieldOuterDiameter(G4double val) {
    SetLeadShieldFrontOuterDiameter(val);
    SetLeadShieldRearOuterDiameter(val);
  };

  void AddLeadShield();
  void UseLeadShield(G4int);

  void SetUseConcreteBlock(G4bool useflag) {useConcreteBlock = useflag;};

  // CF100 vacuum parts
  void SetUseCF100Cross(G4int val){if(val<=maxCF100Cross) currentCF100Cross = val-1;
    useCF100Cross[val-1] = true;};
  void SetCF100CrossPlacement(G4ThreeVector val){CF100CrossPlacement[currentCF100Cross] = val;};
  void SetCF100CrossRotation(G4ThreeVector val){CF100CrossRotation[currentCF100Cross] =
      G4RotationMatrix(val.x(), val.y(), val.z());};
  //void SetCrossFlangeExtUse(G4int, G4bool);
  void SetCrossFlangeExtUse(G4bool val){CFExt = val;};

  void SetNumberOfLeadShieldSides(G4int, G4int);
  void SetNumberOfLeadShieldSides(G4int val) {SetNumberOfLeadShieldSides(LSpt+1, val);};

  // Gate Valve parts

  void UseGateValve(G4int);

  void SetGateValveStart(G4int, G4double);
  void SetGateValveStart(G4double val) {GVStart[GVpt] = val;};
  void SetGateValveEnd(G4int, G4double);
  void SetGateValveEnd(G4double val) {GVEnd[GVpt] = val;};

  void SetGateValveInnerDiameter(G4int num, G4double val);
  void SetGateValveInnerDiameter(G4double val) {GVInnerDim[GVpt] = val;};
  void SetGateValveOuterDiameter(G4int num, G4double val);
  void SetGateValveOuterDiameter(G4double val) {GVOuterDim[GVpt] = val;};

  void SetBonnetHight(G4int num, G4double val);
  void SetBonnetHight(G4double val) {GVBonnetHeight[GVpt] = val;};

  void SetTopBoxLength(G4int num, G4double val);
  void SetTopBoxLength(G4double val) {GVTopBoxLength[GVpt] = val;};

  void SetTopBoxWidth(G4int num, G4double val);
  void SetTopBoxWidth(G4double val) {GVTopBoxWidth[GVpt] = val;};

  void SetTopBoxHeight(G4int num, G4double val);
  void SetTopBoxHeight(G4double val) {GVTopBoxHeight[GVpt] = val;};

  void AddGateValve();

  void SetFieldIonizerStart(G4int, G4double);
  void SetFieldIonizerStart(G4double val) {FIStart[FIpt] = val;};
  G4double GetFieldIonizerStart() {return FIStart[FIpt];};

  void AddFieldIonizer();
  void UseFieldIonizer(G4int);


protected:
  G4double     BPFrontOuterDiam[maxBPN];//!< front outer diameter of lead beampipe
  G4double     BPRearOuterDiam[maxBPN]; //!< rear outer diameter of lead beampipe
  G4double     BPFrontInnerDiam[maxBPN];//!< front inner diameter of lead beampipe
  G4double     BPRearInnerDiam[maxBPN]; //!< rear inner diameter of lead beampipe
  G4double     BPStart[maxBPN];       //!< start position of beampipe
  G4double     BPEnd[maxBPN];         //!< end position of beampipe
  G4bool       useBP[maxBPN];         //!< whether the beam pipe element is in use
  G4bool       visBP[maxBPN];         //!< whether the beam pipe element is visible
  G4int        BPpt;                  //!< number of current beam pipe element


  G4double     LSFrontOuterDiam[maxLSN];
  G4double     LSRearOuterDiam[maxLSN];
  G4double     LSFrontInnerDiam[maxLSN];
  G4double     LSRearInnerDiam[maxLSN];
  G4double     LSStart[maxLSN];
  G4double     LSEnd[maxLSN];
  G4bool       useLS[maxLSN];         //!< whether the lead shield element is in use
  G4int        NumOfLSSides[maxLSN];
  G4int        LSpt;                  //!< number of current lead shield element


  G4Cons*            BPSol[maxBPN];     //!< beam pipes
  G4LogicalVolume*   BPLog[maxBPN];
  G4VPhysicalVolume* BPPhy[maxBPN];

  G4VSolid*          LSSol[maxLSN];     //!< lead shields
  G4LogicalVolume*   LSLog[maxLSN];
  G4VPhysicalVolume* LSPhy[maxLSN];


  G4bool             useConcreteBlock;

  // CF 100 vacuum parts
  G4VPhysicalVolume* CF100CrossBody[maxCF100Cross];
  G4VPhysicalVolume* CF100CrossFl1[maxCF100Cross];
  G4VPhysicalVolume* CF100CrossFl2[maxCF100Cross];
  G4VPhysicalVolume* CF100CrossFl3[maxCF100Cross];
  G4VPhysicalVolume* CF100CrossFl4[maxCF100Cross];
  G4bool             useCF100Cross[maxCF100Cross];  //!< if the ch100 vacuum cross is in use
  G4ThreeVector      CF100CrossPlacement[maxCF100Cross];
  G4RotationMatrix   CF100CrossRotation[maxCF100Cross];
  G4int              currentCF100Cross;
  G4bool             CFExt; // if external Cross Flange Parts are used

  //GateValve parts
  G4double     GVOuterDim[maxGV];     //!> Gate Valve Base Pipe Outer Diameter
  G4double     GVInnerDim[maxGV];     //!> Gate Valve Base Pipe Inner Diameter
  G4double     GVStart[maxGV];        //!> Gate Valve Start Point in z-Axis
  G4double     GVEnd[maxGV];          //!> Gate Valve End Point in z-Axis
  G4double     GVBonnetHeight[maxGV]; //!> Gate Valve Height of the Bonnet
  G4double     GVTopBoxLength[maxGV]; //!> Gate Valve Top Box Length
  G4double     GVTopBoxWidth[maxGV];  //!> Gate Valve Top Box Width
  G4double     GVTopBoxHeight[maxGV]; //!> Gate Valve Top Box Height

  G4bool       useGV[maxGV];        //!> whether the gate valve is in use
  G4int        GVpt;                //!> number of current gate valve element

  G4LogicalVolume*   GVPipeLog[maxGV];
  G4VPhysicalVolume* GVPipePhy[maxGV];
  G4LogicalVolume*   GVTopLog[maxGV];
  G4VPhysicalVolume* GVTopPhy[maxGV];

  G4double     FIStart[maxFI];      //!> Field Ionizer Start Point (very upstream) in z-Axis
  G4bool       useFI[maxFI];        //!> whether the field ionizer is in use
  G4int        FIpt;                //!> number of current field ionizer
  
  //hbarExtFIField* ExtFIField;
  //G4bool      useExtFIField;

  G4LogicalVolume*   FILeftLog[maxFI];
  G4VPhysicalVolume* FILeftPhy[maxFI];
  G4LogicalVolume*   FIRightLog[maxFI];
  G4VPhysicalVolume* FIRightPhy[maxFI];
  G4LogicalVolume*   FIMidZylLog[maxFI];
  G4VPhysicalVolume* FIMidZylPhy[maxFI];
  G4LogicalVolume*   FITopLog[maxFI];
  G4VPhysicalVolume* FITopPhy[maxFI];
  G4LogicalVolume*   FIFlLog[maxFI];
  G4VPhysicalVolume* FIFlPhy[maxFI];
  G4LogicalVolume*   FIFlClLog[maxFI];
  G4VPhysicalVolume* FIFlClPhy[maxFI];

  G4PVPlacement*     CFClosurePhy;
  G4PVPlacement*     CFClosure2Phy;
  G4PVPlacement*     CFVacPumpPhy;


  G4Box*             ConcreteSol;
  G4LogicalVolume*   ConcreteLog;
  G4VPhysicalVolume* ConcretePhy;


  hbarBeamlineMess *    beamlineMessenger;
};



#endif
