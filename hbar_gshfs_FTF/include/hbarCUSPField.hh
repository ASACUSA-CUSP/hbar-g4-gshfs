#ifndef hbarCUSPField_h
#define hbarCUSPField_h 1

#include "globals.hh"
#include "G4ElectroMagneticField.hh"

#include "matrix.hh"
#include <vector>

class G4FieldManager;
class G4EqEMFieldWithSpin;
class hbarEqSextupoleField;
class G4MagIntegratorStepper;
class G4ChordFinder;
class G4PropagatorInField;

//! field configuration for the CUSP trap
/*!
  This class handles the generation of the CUSP field. The firld configuration
  is loaded from two text files, one containing the eletric fieldmap and
  one containing the magnetic fieldmap. This class assumes an ideal field as
  it is produced by a simulation. Therefore only r,z axis informtion is stored.
 */
class hbarCUSPField : public G4ElectroMagneticField
{

public:

  //! constructor
  hbarCUSPField();
  //! destructor
  ~hbarCUSPField();

  //! DoesFieldChangeEnergy(),returns true.
  G4bool DoesFieldChangeEnergy() const { return true; };

  //! GetFieldValue() returns the field value at a given point[].
  /*! 
    field is really field[6]: Bx,By,Bz,Ex,Ey,Ez.
    point[] is in global coordinates: x,y,z.
  */
  void GetFieldValue( const G4double Point[3],  G4double* Bfield ) const;

  //! get offset in beam direction
  G4double GetZOffset(){return fZAxisOffset;}

  //! set offset in beam direction
  void SetZOffset(G4double offset){fZAxisOffset = offset;}

  //! initialize field with fieldmaps
  /*!
    @param Elfilename name of the electrical fieldmap
    @param Magfilename name of the magnetic fieldmap
   */
  void Initialize(const char * Elfilename, const char * Magfilename);

  //! returns maximum B field in beam direction
  inline G4double GetMaxZ(){return fBz_max*1000;};

  //! calculated the gradient for B and E field at Point
  void CaluculateGradient(const G4double Point[3], G4double *grad);

private:
  //! read electric fieldmap
  void ReadElectricFieldASCII(const char * filename);

  //! read magnetic fieldmap
  void ReadMagneticFieldASCII(const char * filename);

  //! get electric field
  /*!
    @param _x X-axis coordinate
    @param _y Y-axis coordinate
    @param _z Z-axis coordinate
    @return vector with the electric field components
   */
  std::vector<double> getElectr(const double& _x,const double& _y,const double& _z) const;

  //! get magnetic field
  /*!
    @param _x X-axis coordinate
    @param _y Y-axis coordinate
    @param _z Z-axis coordinate
    @return vector with the magnetic field components
   */
  std::vector<double> getMagn(const double& _x,const double& _y,const double& _z) const;

  //! bilinear interpolation
  std::vector<double> interpolateBili(std::pair<double,double>& Q11, std::pair<double,double>& Q12, std::pair<double,double>& Q21, std::pair<double,double>& Q22, double& r1, double& r2, double& z1, double& z2, double& r, const double& z,const double& x, const double& y) const;

  //! implementation of an itoa function
  char * itoa(int x);
private:

  G4double                            fZAxisOffset;    //!< in m

  hbarEqSextupoleField*               fEquation;       //!< pointer to equation of motion
  G4MagIntegratorStepper*             fStepper;        //!< pointer to the Geant4 stepper
  G4ChordFinder*                      fChordFinder;    //!< pointer to the Seant4 Chord finder
  G4PropagatorInField*                fieldPropagator; //!< pointer to the field properagtor
  Matrix<std::pair<double, double> >* fEmatrix;        //!< (Ez,Er)
  Matrix<std::pair<double, double> >* fBmatrix;        //!< (Bz,Br)
  std::vector<double>*                felectric_field; //!< electric fieldmap
  std::vector<double>*                fmagnetic_field; //!< magnetic fieldmap
  double fBr_min,fBr_max,fBr_step,fBz_min,fBz_max,fBz_step;            
  double fEr_min,fEr_max,fEr_step,fEz_min,fEz_max,fEz_step;

public:
  G4FieldManager* fieldMgr;                            //!< pointer tot he fieldmanager
  
};

#endif
