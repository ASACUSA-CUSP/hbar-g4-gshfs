#ifndef hbarBeamlineMess_hh
#define hbarBeamlineMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarBeamlineConst.hh"

class hbarBeamlineConst;

class hbarBeamlineMess : public G4UImessenger
{
public:
  hbarBeamlineMess(hbarBeamlineConst* );
  ~hbarBeamlineMess();

  void SetNewValue(G4UIcommand*, G4String);
private:
  hbarBeamlineConst *        hbarBeamline; ///Pointer to the CUSP detector.,

  G4UIcmdWithAnInteger*      UseConcreBlockCmd;

  G4UIcmdWithAnInteger*      BPUseCmd;              //!< setUseBeamPipe
  G4UIcmdWithADoubleAndUnit* BPStartCmd;            //!< setBeamPipeStart
  G4UIcmdWithADoubleAndUnit* BPEndCmd;              //!< setBeamPipeEnd
  G4UIcmdWithADoubleAndUnit* BPInnerDiamCmd;        //!< setBeamPipeInnerDiameter
  G4UIcmdWithADoubleAndUnit* BPFrontInnerDiamCmd;   //!< setBeamPipeFrontInnerDiameter
  G4UIcmdWithADoubleAndUnit* BPRearInnerDiamCmd;    //!< setBeamPipeRearInnerDiameter
  G4UIcmdWithADoubleAndUnit* BPOuterDiamCmd;        //!< setBeamPipeOuterDiameter
  G4UIcmdWithADoubleAndUnit* BPFrontOuterDiamCmd;   //!< setBeamPipeFrontOuterDiameter
  G4UIcmdWithADoubleAndUnit* BPRearOuterDiamCmd;    //!< setBeamPipeRearOuterDiameter
  G4UIcmdWithABool*          BPVisCmd;              //!< setBeamPipeVisible

  G4UIcmdWithAnInteger*      CF100CrossUseCmd;      //!< if the CF100 cross is in use
  G4UIcmdWith3VectorAndUnit* CF100CrossPlaceCmd;    //!< position of the CF100 cross
  G4UIcmdWith3VectorAndUnit* CF100CrossRotCmd;      //!< rotation of the CF100 cross
  G4UIcmdWithABool*          CFExtUseCmd;           //!< whether cross flange externals are used

  G4UIcmdWithAnInteger*      LSUseCmd;              //!< setUseLeadShield
  G4UIcmdWithADoubleAndUnit* LSStartCmd;            //!< setLeadShieldStart
  G4UIcmdWithADoubleAndUnit* LSEndCmd;              //!< setLeadShieldEnd
  G4UIcmdWithADoubleAndUnit* LSInnerDiamCmd;        //!< setLeadShieldInnerDiameter
  G4UIcmdWithADoubleAndUnit* LSFrontInnerDiamCmd;   //!< setLeadShieldFrontInnerDiameter
  G4UIcmdWithADoubleAndUnit* LSRearInnerDiamCmd;    //!< setLeadShieldRearInnerDiameter
  G4UIcmdWithADoubleAndUnit* LSOuterDiamCmd;        //!< setLeadShieldOuterDiameter
  G4UIcmdWithADoubleAndUnit* LSFrontOuterDiamCmd;   //!< setLeadShieldFrontOuterDiameter
  G4UIcmdWithADoubleAndUnit* LSRearOuterDiamCmd;    //!< setLeadShieldRearOuterDiameter
  G4UIcmdWithAnInteger*      LSSidesNumCmd;         //!< setNumberOfLeadShieldSides

  G4UIcmdWithAnInteger*      GVUseCmd;          //!< setUseGateValve
  G4UIcmdWithADoubleAndUnit* GVOuterDiamCmd;    //!< setGateValveOuterDiameter
  G4UIcmdWithADoubleAndUnit* GVInnerDiamCmd;    //!< setGateValveInnerDiameter
  G4UIcmdWithADoubleAndUnit* GVStartCmd;        //!< setGateValveStart
  G4UIcmdWithADoubleAndUnit* GVEndCmd;          //!< setGateValveEnd
  G4UIcmdWithADoubleAndUnit* GVBonnetHightCmd;   //!< setGateValveBonnetHight
  G4UIcmdWithADoubleAndUnit* GVTopBoxLengthCmd;  //!< setTopBoxLength
  G4UIcmdWithADoubleAndUnit* GVTopBoxWidthCmd;   //!< GVTopBoxWidth
  G4UIcmdWithADoubleAndUnit* GVTopBoxHeightCmd;  //!< GVTopBoxHeight

  G4UIcmdWithAnInteger*      FIUseCmd;           //!< setUseFieldIonizer
  G4UIcmdWithADoubleAndUnit* FIStartCmd;         //!< setFieldIonizerStart

};


#endif
