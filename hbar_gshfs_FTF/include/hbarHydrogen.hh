
#ifndef hbarHydrogen_h
#define hbarHydrogen_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "hbarHydrogenLike.hh"

//! The Hydrogen Class
/*!
  The full definition of a Hydrogen atom, warning, this class is designed to produce a
  \b singleton!
 */
class hbarHydrogen : public hbarHydrogenLike
{
private:
  //! this non member vaiable holds the pointer to the singelton instance
  static hbarHydrogen* theInstance;

  //Constructor.
  hbarHydrogen(const G4String& name /*! The name of the particle. Unique. */);

public:
  //! Definition. Basically returns the object if it can find it, or otherwise creates a new one.
  static hbarHydrogen* Definition();
};

#endif
