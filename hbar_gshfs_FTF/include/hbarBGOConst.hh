#ifndef hbarBGOConst_hh
#define hbarBGOConst_hh 1
#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"

#include "G4ChordFinder.hh"

#include "hbarBGOMess.hh"

#define MAX_SCINTILLATORS 6

#define maxNDplsci 7
#define maxNDplsciBox 6
#define maxNDplsciTrd 5
#define maxNDsupSum 33
#define maxNDsupPlate 2

//---------------------Nagata Detector---------------------//

class hbarBGOMess;


class hbarBGOConst 
{
public:
  hbarBGOConst();
  ~hbarBGOConst();
  void MakeBGO(G4VPhysicalVolume *, G4double , hbarDetectorSD *);

  void SetUseHydrogenTargetInsteadOfNormal(G4bool useflag)  {UseHydrogenTarget = useflag;};
  void SetTargetRinRoutLen(G4ThreeVector vec){TargetRinRoutLen = vec;};
  void SetFlangeRinRoutLen(G4ThreeVector vec){ FlangeRinRoutLen = vec;}

  void UseCoverNumber(G4int num) { CoverNumber = num; }
  void SetCoverRinRoutLen(G4ThreeVector vec){ CoverRinRoutLen[CoverNumber] = vec;}

  void UseScintillatorNumber(G4int num) {ScintillatorNumber = num; }
  void SetScintillatorBoxDimensionsXYZ(G4ThreeVector vec){ ScintillatorBoxDimensionsXYZ[ScintillatorNumber] = vec; }
  void SetScintillatorTrapezoidX1(G4double val) { ScintillatorTrapezoidX1[ScintillatorNumber] = val; }
  void SetScintillatorTrapezoidX2(G4double val) { ScintillatorTrapezoidX2[ScintillatorNumber] = val; }
  void SetScintillatorTrapezoidY1(G4double val) { ScintillatorTrapezoidY1[ScintillatorNumber] = val; }
  void SetScintillatorTrapezoidY2(G4double val) { ScintillatorTrapezoidY2[ScintillatorNumber] = val; }
  void SetScintillatorTrapezoidZ(G4double val) { ScintillatorTrapezoidZ[ScintillatorNumber] = val; }
  

  void EnableScintillator(G4int num)          {useScintillator[num] = true;};
  void EnableSensitiveScintillator(G4int num)         {useSensitiveScintillator[num] = true;};
  void EnableSupportBox(G4bool val) {useSupportBox = val;}

  void UseSupportBoxNumber(G4int num) {SupportBoxNumber = num; }
  void SetSupportBoxDimensionXYZ(G4ThreeVector vec) {SupportBoxDimensionXYZ[SupportBoxNumber] = vec; }
  void SetSupportBoxDistanceBetweenBars(G4ThreeVector vec) {SupportBoxDistanceBetweenBars = vec;}


private:
  void  MakeTarget(G4VPhysicalVolume*, hbarDetectorSD *);
  void  MakeCover(G4VPhysicalVolume*);
  void  MakeScintillators(G4VPhysicalVolume*, hbarDetectorSD*);
  void  MakeSupportBox(G4VPhysicalVolume*);


  G4int              ScintillatorNumber;
  G4int              SupportBoxNumber; 
  G4int              CoverNumber;

  G4double           CenterOfTheMikiLense;

  G4ThreeVector      SupportBoxDimensionXYZ[maxNDsupSum];
  G4ThreeVector      SupportBoxDistanceBetweenBars;
  G4ThreeVector      TargetRinRoutLen;
  G4ThreeVector      FlangeRinRoutLen;
  G4ThreeVector      CoverRinRoutLen[maxNDcover];
  G4ThreeVector      ScintillatorBoxDimensionsXYZ[maxNDplsciBox];
  G4double           ScintillatorTrapezoidX1[maxNDplsciTrd];
  G4double           ScintillatorTrapezoidX2[maxNDplsciTrd];
  G4double           ScintillatorTrapezoidY1[maxNDplsciTrd];
  G4double           ScintillatorTrapezoidY2[maxNDplsciTrd];
  G4double           ScintillatorTrapezoidZ[maxNDplsciTrd];
  G4bool             useScintillator[maxNDplsciTrd];
  G4bool             useSensitiveScintillator[maxNDplsciTrd];
  G4bool             UseHydrogenTarget;
  G4bool             useSupportBox;

  hbarBGOMess *      BGOMessenger;


  G4VSolid*          ScintillatorBox[maxNDplsciBox]; 
  G4VSolid*          ScintillatorTrapezoid[maxNDplsciTrd]; 
  G4VSolid*          Scintillator[maxNDplsci]; 


  G4LogicalVolume*   NDBGOLog; 
  G4VPhysicalVolume* NDBGOPhy; 
  G4LogicalVolume*   NDcoverLog; 
  G4VPhysicalVolume* NDcoverPhy; 
  G4LogicalVolume*   ScintillatorLog[maxNDplsci]; 
  G4VPhysicalVolume* NDplsciPhy[maxNDplsci]; 
  G4VSolid*          SupportBox[maxNDsupSum]; 
  G4LogicalVolume*   NDsupBoxLog[maxNDsupSum]; 
  G4VPhysicalVolume* NDsupBoxPhy[maxNDsupSum]; 
  G4VSolid*          NDsupPlate[maxNDsupPlate]; 
  G4LogicalVolume*   NDsupPlateLog[maxNDsupPlate]; 
  G4VPhysicalVolume* NDsupPlatePhy[maxNDsupPlate]; 
  G4VPhysicalVolume* ScintillatorPhy[maxNDplsciTrd]; 

};



#endif
