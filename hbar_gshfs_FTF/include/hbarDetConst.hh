#ifndef hbarDetectorConstruction_h
#define hbarDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4RotationMatrix.hh"
#include "G4TwoVector.hh"
#include "G4UserLimits.hh"

#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarCavityField.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"
#include "G4ChordFinder.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "hbarException.hh"

#include "hbarCPTConst.hh"
#include "hbarBeamlineConst.hh"
#include "hbarCUSPConst.hh"
#include "hbarCavityConst.hh"
#include "hbarSextupoleConst.hh"
#include "hbarBGOConst.hh"

#include "hbarDefinitions.hh"

#include "hbarDummyDetectorConst.hh"

#include "hbarException.hh"

//#include "G4GDMLParser.hh"
#include "time.h"


class G4Box;
class G4Tubs;
class G4Cons;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4Sphere;
class G4Polycone;
class G4Polyhedra;
class G4Trd;
class G4SubtractionSolid;
class G4IntersectionSolid;
class G4UnionSolid;
class G4VPVParameterisation;
class G4PVParameterised;
class G4VSolid;

class hbarDetMess;
class hbarDetectorSD;
class hbarFieldSetup;
class hbarCUSPField;
class hbarCavityField;
class hbarMWSetup;
class hbarCPTConst;
class hbarBeamlineConst;
class hbarBeamlinConst;
class hbarCUSPConst;
class hbarCavityConst;
class hbarBGOConst;
class hbarSextupoleConst;
class hbarDummyDetectorConst;


//! Detector construction class
/*!
  Update by Rikard:
  This class is the main detector class, invoking subclasses for the actual detector construction.

 */
class hbarDetConst : public G4VUserDetectorConstruction
{
public:

  hbarDetConst();   //! Constructor
  ~hbarDetConst();   //! Destructor
  G4VPhysicalVolume* ConstructWorld(); ///Constructs the world.

  static G4Material * GetMaterial(const G4String & name ///Name of material to get from database.
								  ); ///Utility function. Should be replaced with something more efficient.


  void SetWorldSizeXY(G4double val) {WorldSizeXY = val;};
  void SetWorldSizeZ(G4double val)  {WorldSizeZ = val;};

  G4int    GetMaximumSextupoleNumber() {return maxSN;};
  G4int    GetMaximumBeamPipeNumber()  {return maxBPN;};

  G4double GetWorldSizeXY()         {return WorldSizeXY;};
  G4double GetWorldSizeZ()          {return WorldSizeZ;};


  hbarFieldSetup* GetMagneticFieldSetup() {return magFieldSetup;};
  G4Box* GetWorldBox() {return WorldSol;};

  hbarCPTConst * GetCPTConst() { return myCPTConst; }
  hbarCavityConst * GetCavityConst() { return myCavityConst; }
  hbarSextupoleConst * GetSextupoleConst() { return mySextupoleConst; }
  hbarDummyDetectorConst * GetDDetConst() { return myDummyDetectorConst; }

  G4VPhysicalVolume* Construct(); // G4VUserDetectorConstruction has same method as virtual -> Polymorph

  void UpdateGeometry();

  void SetUseNagataDetector(G4bool useflag){useNagataDetector = useflag;};
  void SetUseCAVITY(G4bool useflag)        {useCavity = useflag;};
  void SetUseCUSP(G4bool useflag);
  void SetUseHbarDet(G4bool useflag){useHbarCounter = useflag;};
  void SetWriteGDML(G4bool useflag){WriteGDML = useflag;}; // called from hbarDetMess
  void SetUseCPT(G4bool useflag){useCPT = useflag;}; // if charged Pion detector (CPT) is used

  void ConstructGDML(); ///Calls GDML-Parser function

  static G4bool GetUseHbar2014Det() {return useHbar2014;}; //to get access to 2014 Setup for other classes.
  static void SetUseHbar2014Det(G4bool useflag){useHbar2014 = useflag;}; // called from hbarDetMess
  hbarCPTConst* GetmyCPTConst() {return myCPTConst;};
  
  static G4bool GetUseTimepixSetup() {return useTPX;};

private:
  void DefineMaterials(); ///Defines all the materials. Important: this must be called before constructing anything.

  hbarDetMess* detectorMessenger;   //!< pointer to the Messenger class

  hbarFieldSetup*    magFieldSetup; ///Magnetic field setup.
  G4UserLimits*      alim;


  G4Box*             WorldSol;
  G4LogicalVolume*   WorldLog;
  G4VPhysicalVolume* WorldPhy;

  G4double     WorldSizeXY;
  G4double     WorldSizeZ;

  hbarDetectorSD *scintiSD; ///Sensitive detector.

  // Added by SV
  static G4bool      useHbar2014;         /// load Beamline Setup from 2014 including octagonal Hodoscope

  static G4bool     useTPX;
  static G4bool 	UseGDMLHodor;

  ///Todo: investigate where these belongs.
  G4bool                  useHbarCounter;
  G4bool                  useCUSP;
  G4bool                  useNagataDetector;
  G4bool                  useCavity;
  G4bool                  NagataDetTarget;
  G4bool                  WriteGDML;            /// activates GDML-export, set by Messengerclass (Makro) and used in hbarDetConst.cc
  G4bool                  useCPT;               /// if charged Pion detector (CPT) is used (2012 or 2014 depending on UseHbar2014)

  hbarCPTConst *          myCPTConst;           ///Part of the setup.
  hbarBeamlineConst *     myBeamlineConst;      ///Part of the setup.
  hbarBGOConst *          myBGOConst;           ///Part of the setup.
  hbarCUSPConst *         myCUSPConst;          ///Part of the setup.
  hbarCavityConst *       myCavityConst;        ///Part of the setup.
  hbarSextupoleConst *    mySextupoleConst;     ///Part of the setup.
  hbarDummyDetectorConst* myDummyDetectorConst; ///Part of the setup.
};

#endif
