#ifndef hbarStackAction_h
#define hbarStackAction_h 1

#include "G4UserStackingAction.hh"
#include "globals.hh"

class hbarStackActionMess;

//! This class decides what to do with the particle tracks
/*!
  This class decides what to do with the particle tracks: carry on with the 
  tracking of a particle, suspend the tracking temporarily, or kill the 
  particle. The selection is based on what the user sets with the 
  /tracking/processTracks command, which is defined in the 
  hbarStackActionMess class.
 */
class hbarStackAction : public G4UserStackingAction {

public:
  hbarStackAction();
  virtual ~hbarStackAction();
  
  //! this function describes how to treat a certain particle track
  virtual G4ClassificationOfNewTrack 
  ClassifyNewTrack(const G4Track* aTrack);
  
  void SetProcessFlag   (G4String val)  {processFlag = val;};
  
private:
  G4String                    processFlag;
  hbarStackActionMess*        stackMessenger;
};

#endif
