
#ifndef hbarBGO_SD_h
#define hbarBGO_SD_h 1

#include <algorithm> 
#include <list>
// USER //
//#include "SimpleHit.hh"
#include "hbarDetectorHit.hh"
#include "TH1.h"

// GEANT4 //
#include "G4VSensitiveDetector.hh"
#include "G4Track.hh"
#include "G4SystemOfUnits.hh"
#include "G4Step.hh"
#include "G4SteppingManager.hh"
#include "Randomize.hh"

#include "G4HCofThisEvent.hh"

#include "G4HCtable.hh"
#include "G4SDManager.hh"

#include "TCanvas.h"
#include "TError.h"
#include "TGraph.h"
#include "G4Run.hh"
#include "G4RunManager.hh"

#include "hbarCPTConst.hh"

// ROOT //
#include "TFile.h"
//#include "G4THitsCollection.hh"

class G4HCofThisEvent;
class hbarCPTConst;
//class hbarDetConst;
class hbarRunAction;



// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0

class hbarBGO_SD : public G4VSensitiveDetector
{
public:
  hbarBGO_SD(G4String SDname); //!< constructor
  ~hbarBGO_SD(); //!< destructor
  G4bool ProcessHits(G4Step* , G4TouchableHistory * ); //!<
  void Initialize(G4HCofThisEvent* ); //!<
  void EndOfEvent(G4HCofThisEvent* ); //!<
  //// Getter:
  G4String GetSDName() {return BGOName;};
  Double_t* GetBGOedep()  {return &TotalEnergy;};
  G4bool Getbgohit() {return bgohit;};
  G4String GetBGOName() {return BGOName;};
  std::vector<std::vector<double> > GetEdep_vector() {return Edep_vector;};
  
  //// Setter:
 // void Set_drawhistos(G4bool useflag) {drawhistos = useflag;};
 // void Set_write_to_dat_files(G4bool useflag) {write_to_dat_files = useflag;};

private:
  hbarDetectorHitsCollection* BGOhitCollection; //!< hitcollection
  G4Track * track; //!< current track
  G4String volName; //!< name of volume  
  G4String BGOName;
  Double_t TotalEnergy;
  G4bool bgohit;
  std::vector<std::vector<double> > Edep_vector;
  //std::list<double>  Edep_list;


  hbarCPTConst* localcptconst;
};

#endif

