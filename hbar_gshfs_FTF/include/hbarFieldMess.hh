#ifndef hbarFieldMessenger_h
#define hbarFieldMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarFieldSetup;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;

//! Fiel messenger class
/*!
  This a messenger class which defines user commands for the sextupole 
  magnetic field: which sextupole to use, maximum (pole tip) field, whether 
  to use spontaneous Majorana spin-flips for the Hbar atoms, etc.

  This class defines the following macros:
  - /field/setUseSextupole
  - /field/setMaximumValue
  - /field/setZeroFieldValue
  - /field/setAllowMajoranaTransitions
  - /field/setUseAngleDifference
  - /field/setFlip
  - /field/setMagnetHalfPoleNumber
  - /field/setMinimumStepLength  
 */
class hbarFieldMess: public G4UImessenger
{
public:
  hbarFieldMess(hbarFieldSetup* );
  ~hbarFieldMess();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  hbarFieldSetup*            hbarFSetup;           //!< pointer to the field definition
  
  G4UIdirectory*             hbarfieldDir;         //!< /field/
  
  G4UIcmdWithAnInteger*      SextUseCmd;           //!< setUseSextupole
  G4UIcmdWithADoubleAndUnit* MaxValueCmd;          //!< setMaximumValue
  G4UIcmdWithADoubleAndUnit* ZeroValueCmd;         //!< setZeroFieldValue

  /*!
    Whether spontaneous Majorana spin-flips should happen at the entrance of sextupole #2.
    If the Hbar atoms are guided by a >1 G magnetic field between the two sextupoles,
    (or between the cusp trap and the sextupole) then these spin flips should no occur.
  */
  G4UIcmdWithAString*        AllowMajCmd;          //!< setAllowMajoranaTransitions

  /*!
    When an Hbar atoms leaves sextupole #1, its spin is (anti)parallel to the field lines.
    When it enters sextupole #2, its spin can be at an angle to the field lines,
    thus Majorana spin-flips can occur there. The probability of the spin flip
    depends on the angle difference between the spin and the field line.
    This command tells whether to use this angle difference when calculating
    the spin flip probability.
    Setting it to "off" essentially turns off Majorana spin-flips.
  */
  G4UIcmdWithAString*        UseAngleDiffCmd;      //!< setUseAngleDifference
  G4UIcmdWithAString*        FlipCmd;              //!< setFlip
  G4UIcmdWithAnInteger*      MagnetHalfPoleNumCmd; //!< setMagnetHalfPoleNumber
  G4UIcmdWithADoubleAndUnit* MinStepLengthCmd;     //!< setMinimumStepLength
  
};

#endif

