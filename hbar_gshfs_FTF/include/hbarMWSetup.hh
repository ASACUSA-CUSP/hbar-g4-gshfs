#ifndef hbarMWSetup_h
#define hbarMWSetup_h

#include "globals.hh"
#include "hbarMWMess.hh"
#include "TList.h"
#include "G4Step.hh"

#include "hbarCavityConst.hh"
#include "hbarMWMess.hh"
#include "hbarRunAction.hh"
#include "hbarPrimGenAction.hh"
#include "hbarFieldSetup.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" // Added by Clemens
#include "numtostr.hh"
#include "hbarCavityField.hh"

#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TObjString.h"
#include "TMath.h"
#include "TNtuple.h"

#include "G4UniformMagField.hh"
#include "G4MagneticField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4ChordFinder.hh"
#include "G4RunManager.hh"

#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"

#include "CLHEP/Vector/TwoVector.h"

class hbarCavityConst;

class hbarRunAction;
class hbarDetConst;
class hbarMWMess;
class hbarAntiHydrogen;
class hbarHydrogen; // Added by Clemens
//class hbarCavityField;
class hbarCavityField;

class TF1;
class TFile;
class TGraphErrors;
class TGraph;

//! The definition and properties of the microwave field in the Cavity
/*!
  This class defines the microwave field within the Cavity, including
  Q value, transition probabilities, microwave power, modes etc.
  \todo The methods in here needs to be documented. //Rikard
 */
class hbarMWSetup
{

public:

  hbarMWSetup(hbarDetConst*) ;               //!<  A zero field
//  hbarFieldSetup(G4ThreeVector) ;  //  The value of the field
 
  //! Destructor
  ~hbarMWSetup() ;

public:

  G4double GetFixedTransitionProbability() {return fTransProb;};

  G4double GetCavityCenter();
  G4double GetCavityInnerDiameter();
  G4double GetCavityOuterDiameter();
  G4double GetCavityLength();
  G4double GetCavityResonatorSeparation();
  G4String GetUseFixedTransitionProbabilities() {return useFixedProbs;};
  G4double GetMWFrequency() {return nu_used;};
  G4double GetMWPower() {return MW_power;};
  G4String GetMWMode() {return MW_mode;};
  G4double GetCavityQ() {return cavity_Q;};
  G4double GetCavityMagneticField() {return BinCavity;};
  G4double GetCavityMagneticFieldVariation() {return DeltaBinCavity;};
  G4double GetMWFieldAngle() {return fieldAngle;};

  void SetFixedTransitionProbability(G4double val) {fTransProb = val;};

  void SetUseFixedTransitionProbabilities(G4String val) {useFixedProbs = val;};
  void SetUseFixedTransitionProbability(G4String val) {useFixedProbs = val;};
  void SetMWFrequency(G4double val) {nu_used = val;};
  void SetMWPower(G4double val) {MW_power = val;};
  void SetMWMode(G4String val) {MW_mode = val;};
  void SetCavityQ(G4double val) {cavity_Q = val;};
  void SetCavityMagneticField(G4double val) {BinCavity = val;};
  void SetCavityMagneticFieldVariation(G4double val) {DeltaBinCavity = val;};
  void SetMWFieldAngle(G4double val) {fieldAngle = val;};
  void SetRandomParticleNumber(G4bool val) {randPartNum = val;};

  void SetScanStart(G4double val) {scanStart = val;};
  void SetScanEnd(G4double val) {scanEnd = val;};
  void SetScanNumberOfSteps(G4int val) {scanSteps = val;};
  void SetScanFileName(G4String val) {scanFileName = val;};
   
 

  void ConnectRunAction(hbarRunAction *val) {runAction = val;};
  
  //! This function tracks the particle through the Cavity by applying a step adaptive Runge-Kutta solver.
  void MakeMWTransition(hbarHydrogenLike*, const G4Step*, G4double, G4double, G4double);

  void Scan(G4int nevents = 1);
  G4double CalculateOptimumPower();

  //! Set function to turn the Cavity off or on 
  void SetUseCavity(G4bool val){useCavity = val;};
 
private:

  //! Calculates the amplitude of the oscillating magnetic field  at a given point (r, phi) in the cavity
  G4double GetMWFieldB(G4double r, G4double phi);
  //! Calculates the amplitude of the oscillating magnetic field at the center of the cavity
  G4double GetMWFieldB0();
  G4double GetMWFieldB_r(G4double r, G4double phi);
  G4double GetMWFieldB_phi(G4double r, G4double phi);


//  G4double GetTransitionProbability(G4double omega_0, G4double b, G4double T);

private:

  const G4double          nu_0;              //!< the zero-field hyperfine splitting frequency (Hz)
  const G4double          BohrMagneton;      //!< the Bohr Magneton 
  const G4double          NuclearMagneton;   //!< the nuclear Magneton 
  const G4double          GFactorElectron;
  const G4double          GFactorProton;
  hbarDetConst*           hbarDet;

  G4double                fTransProb;        //!< (F,M) = (1,-1) -> (0,0) and (1,1) -> (1,0)


  G4double				  B_field_value;	 //!< mean value of fieldmap
  G4String                useFixedProbs;     //!< use the fixed transition probabilities above
  G4double                nu_used;           //!< the frequency of the used MW field (Hz)
  G4double                cavity_Q;          //!< RF cavity Q
  G4double                MW_power;          //!< MW power
  G4String                MW_mode;           //!< MW mode; TM110 or TM010
  G4double                magn_permeab;      //!< magnetic permeability (mu_0)
  G4double                BinCavity;         //!< static magnetic field in cavity
  G4double                DeltaBinCavity;    //!< variation of the static magnetic field in the cavity
  G4double                P11;               //!< first root of the first Bessel function J_1(x) of the first kind
  G4double                P01;               //!< first root of the zeroth Bessel function J_0(x) of the first kind
  G4double                J1primeP11;        //!< value of the derivative of J_1(x) at P11
  G4double                J1P01;             //!< value of J_1(x) at P01
  G4double                fieldAngle;        //!< angle between beam axis and MW field
  G4bool                  randPartNum;       //!< whether the number of particles shot in a scan should be well-defined or random
  TF1*                    J1;                //!< first Bessel function of the first kind
  TF1*                    J0;                //!< zeroth Bessel function of the first kind

  G4double                scanStart;         //!< scan start frequency
  G4double                scanEnd;           //!< scan end frequency
  G4int                   scanSteps;         //!< number of scan steps
  G4String                scanFileName;      //!< name of the scan file
  TFile*                  scanFile;          //!< ROOT file store the scan results
  TGraphErrors*           scanGraph;         //!< graph with the scan results
  TGraphErrors*           scanInitGraph;     //!< graph with the number of particles shot in each event
  TList*                  scanNtuples;       //!< list of ntuples containing the detector hits for each frequency

  hbarMWMess*             fMWMessenger;      //!< pointer to the MW messenger class
  hbarRunAction*          runAction;         //!< pointer to Genat4 runaction
  G4bool                  useCavity;         //!< switch to turn off the cavity effect, default state is off
};

#endif
