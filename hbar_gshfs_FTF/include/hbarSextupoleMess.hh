#ifndef hbarSextupoleMess_hh
#define hbarSextupoleMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarSextupoleConst.hh"


class hbarDetConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;
class hbarSextupoleConst;

class hbarSextupoleMess : public G4UImessenger
{
public:
  hbarSextupoleMess(hbarSextupoleConst* );
  ~hbarSextupoleMess();
  
  void SetNewValue(G4UIcommand*, G4String);
private:
  hbarSextupoleConst *               hbarSextupole; ///Pointer to the CUSP detector.

  G4UIcmdWithADoubleAndUnit* SextStartCmd;          //!< setSextupoleStart
  G4UIcmdWithADoubleAndUnit* SextEndCmd;            //!< setSextupoleEnd
  G4UIcmdWithADoubleAndUnit* SextOffsetXCmd;        //!< setSextupoleOffsetX
  G4UIcmdWithADoubleAndUnit* SextOffsetYCmd;        //!< setSextupoleOffsetY
  G4UIcmdWithADoubleAndUnit* SextInnerDiamCmd;      //!< setSextupoleInnerDiameter
  G4UIcmdWithADoubleAndUnit* SextFrontInnerDiamCmd; //!< setSextupoleFrontInnerDiameter
  G4UIcmdWithADoubleAndUnit* SextRearInnerDiamCmd;  //!< setSextupoleRearInnerDiameter
  G4UIcmdWithADoubleAndUnit* SextOuterDiamCmd;      //!< setSextupoleOuterDiameter
  G4UIcmdWithADoubleAndUnit* SextFrontOuterDiamCmd; //!< setSextupoleFrontOuterDiameter
  G4UIcmdWithADoubleAndUnit* SextRearOuterDiamCmd;  //!< setSextupoleRearOuterDiameter
  G4UIcmdWithADouble*        SextRotXCmd;           //!< set Sextupole rotation along X axis
  G4UIcmdWithADouble*        SextRotYCmd;           //!< set Sextupole rotation along Y axis
  G4UIcmdWithABool*          SextRotMag;            //!< set rotate magnet geom with field axis
  G4UIcmdWithAnInteger*      SextUseCmd;            //!< setUseSextupole
  G4UIcmdWithAnInteger*      UseSextupolePartsCmd;

  G4UIcmdWithAnInteger*		 SxPartUseCmd;
  G4UIcmdWith3VectorAndUnit* SxPartSetDimenCmd;     

};


#endif
