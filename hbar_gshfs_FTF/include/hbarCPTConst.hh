#ifndef hbarCPTConst_hh
#define hbarCPTConst_hh 1


#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Run.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4OpticalSurface.hh"
#include "G4LogicalSkinSurface.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"

#include "G4ChordFinder.hh"

#include "hbarDefinitions.hh"

#include "hbarCPTMess.hh"

#include "G4AssemblyVolume.hh"

//#include "G4GDMLParser.hh"
#include "hbarSiPM_SD.hh"
#include "hbarBGOPMT_SD.hh"
#include "hbarBGO_SD.hh"
#include "TFile.h"
#include "TTree.h"


#include "G4LogicalBorderSurface.hh"

class hbarCPTMess;
class hbarSiPM_SD;
class hbarBGOPMT_SD;
class hbarBGO_SD;

class hbarCPTConst
{
public:
  hbarCPTConst(); ///Constructor.
  ~hbarCPTConst(); ///Destructor.

public:

  void MakeCPT(G4VPhysicalVolume*, hbarDetectorSD *);  
  void AddCalorimeter();
  void AddVeto();
  void UseCalorimeter(G4int);

  void SetCalorimeterCenter(G4int, G4double, G4double, G4double);
  void SetCalorimeterCenter(G4double xval, G4double yval, G4double zval) {SetCalorimeterCenter(Scintipt+1, xval, yval, zval);};
  void SetCalorimeterCenter(G4ThreeVector val) {SetCalorimeterCenter(Scintipt+1, val.x(), val.y(), val.z());};
  void SetCalorimeterCenter(G4int num, G4ThreeVector val) {SetCalorimeterCenter(num, val.x(), val.y(), val.z());};



  void SetCalorimeterAngle(G4int, G4double, G4double, G4double);
  void SetCalorimeterAngle(G4double phival, G4double thetaval, G4double psival) {SetCalorimeterAngle(Scintipt+1, phival, thetaval, psival);};
  void SetCalorimeterAngle(G4ThreeVector val) {SetCalorimeterAngle(Scintipt+1, val.x(), val.y(), val.z());};
  void SetCalorimeterAngle(G4int num, G4ThreeVector val) {SetCalorimeterAngle(num, val.x(), val.y(), val.z());};
  void SetCalorimeterThickness(G4int, G4double);
  void SetCalorimeterThickness(G4double val) {SetCalorimeterThickness(Scintipt+1, val);};
  void SetCalorimeterLength(G4int, G4double);
  void SetCalorimeterLength(G4double val) {SetCalorimeterLength(Scintipt+1, val);};
  void SetCalorimeterWidth(G4int, G4double);
  void SetCalorimeterWidth(G4double val) {SetCalorimeterWidth(Scintipt+1, val);};
  void SetCalorimeterLargerWidth(G4int, G4double);
  void SetCalorimeterLargerWidth(G4double val) {SetCalorimeterLargerWidth(Scintipt+1, val);};
  void SetCalorimeterCoincidence(G4int, G4int);
  void SetCalorimeterCoincidence(G4int val) {SetCalorimeterCoincidence(Scintipt+1, val);};
  void SetLeadThickness(G4int, G4double);
  void SetLeadThickness(G4double val) {SetLeadThickness(Scintipt+1, val);};
  void SetNumberOfLeadLayers(G4int, G4int);
  void SetNumberOfLeadLayers(G4int val) {SetNumberOfLeadLayers(Scintipt+1, val);};
  void SetNumberOfCalorimeterSides(G4int, G4int);
  void SetNumberOfCalorimeterSides(G4int val) {SetNumberOfCalorimeterSides(Scintipt+1, val);};

  void UseVeto(G4int);

  void AddHodoscope();
  void UseHodoscope(G4int);


  void SetHodoscopeCenter(G4int, G4double, G4double, G4double);
  void SetHodoscopeCenter(G4double xval, G4double yval, G4double zval) {SetHodoscopeCenter(Hodoscopept+1, xval, yval, zval);};
  void SetHodoscopeCenter(G4ThreeVector val) {SetHodoscopeCenter(Hodoscopept+1, val.x(), val.y(), val.z());};
  void SetHodoscopeCenter(G4int num, G4ThreeVector val) {SetHodoscopeCenter(num, val.x(), val.y(), val.z());};
  void SetHodoscopeAngle(G4int, G4double, G4double, G4double);
  void SetHodoscopeAngle1(G4double phival, G4double thetaval, G4double psival) {SetHodoscopeAngle(Hodoscopept+1, phival, thetaval, psival);};
  void SetHodoscopeAngle(G4ThreeVector val) {SetHodoscopeAngle(Hodoscopept+1, val.x(), val.y(), val.z());};
  void SetHodoscopeAngle(G4int num, G4ThreeVector val) {SetHodoscopeAngle(num, val.x(), val.y(), val.z());};
  void SetHodoscopeLength(G4int, G4double);
  void SetHodoscopeLength(G4double val) {SetHodoscopeLength(Hodoscopept+1, val);};
  void SetHodoscopeInRad(G4int, G4double);
  void SetHodoscopeInRad(G4double val) {SetHodoscopeInRad(Hodoscopept+1, val);};
  void SetHodoscopeOutRad(G4int, G4double);
  void SetHodoscopeOutRad(G4double val) {SetHodoscopeOutRad(Hodoscopept+1, val);};
  void SetHodoscopeSegment(G4int num, G4int val);
  void SetHodoscopeSegment(G4int val){SetHodoscopeSegment(Hodoscopept+1, val);};

  void SetHbarDetCenter(G4double xval, G4double yval, G4double zval);
  void SetHbarDetCenter(G4ThreeVector vec){SetHbarDetCenter(vec.x(), vec.y(),
							    vec.z());};
  void SetHbarDetAngle(G4double phi, G4double theta, G4double psi);
  void SetHbarDetAngle(G4ThreeVector vec){SetHbarDetAngle(vec.x(), vec.y(),
							  vec.z());};
  void SetHbarDetSegment(G4int xsegval, G4int ysegval);
  void SetHbarDetSegment(G4ThreeVector vec){SetHbarDetSegment(vec.x(), vec.y());}
  void SetHbarDetDimension(G4double xlen, G4double ylen, G4double zlen);
  void SetHbarDetDimension(G4ThreeVector vec){SetHbarDetDimension(vec.x(),vec.y(),vec.z());};

  void SetHodoScintWidth(G4double val) {SetHodoScintWidth(Hodoscopept+1, val);};
  void SetHodoScintWidth(G4int, G4double);

  void SetHodoScintHeight(G4double val) {SetHodoScintHeight(Hodoscopept+1, val);};
  void SetHodoScintHeight(G4int, G4double);
  
  void SetFoilMaterial(G4String);
  

  void AddBGO2014();
  void UseBGO2014(G4int);

  void SetUseGDMLHodor(G4bool useflag) {UseGDMLHodor = useflag;};
  void SetUseGDMLBGO(G4bool useflag) {UseGDMLBGO = useflag;}; 
  void SetUseSensitiveSiPMs(G4int useflag) {UseSensitiveSiPMs = useflag;};
  void SetUseSensitiveBGOPMTs(G4int useflag) {UseSensitiveBGOPMTs = useflag;};
  void SetSaveWaveformsToDat(G4int useflag) {SaveWaveformsToDat = useflag;};
  void SetWriteWaveformsToPDF(G4int useflag) {WriteWaveformsToPDF = useflag;};

  void SetBGOScint2014Start(G4double val) {SetBGOScint2014Start(BGO2014pt+1, val);};
  void SetBGOScint2014Start(G4int, G4double);
  void SetBGOScint2014End(G4double val) {SetBGOScint2014End(BGO2014pt+1, val);};
  void SetBGOScint2014End(G4int, G4double);
  void SetBGOScint2014Diam(G4double val) {SetBGOScint2014Diam(BGO2014pt+1, val);};
  void SetBGOScint2014Diam(G4int, G4double);

  void SetVetoCenter(G4int, G4double, G4double, G4double);
  void SetVetoCenter(G4double xval, G4double yval, G4double zval) {SetVetoCenter(Vetopt+1, xval, yval, zval);};
  void SetVetoCenter(G4ThreeVector val) {SetVetoCenter(Vetopt+1, val.x(), val.y(), val.z());};
  void SetVetoCenter(G4int num, G4ThreeVector val) {SetVetoCenter(num, val.x(), val.y(), val.z());};
  void SetVetoAngle(G4int, G4double, G4double, G4double);
  void SetVetoAngle1(G4double phival, G4double thetaval, G4double psival) {SetVetoAngle(Vetopt+1, phival, thetaval, psival);};
  void SetVetoAngle(G4ThreeVector val) {SetVetoAngle(Vetopt+1, val.x(), val.y(), val.z());};
  void SetVetoAngle(G4int num, G4ThreeVector val) {SetVetoAngle(num, val.x(), val.y(), val.z());};
  void SetVetoLength(G4int, G4double);
  void SetVetoLength(G4double val) {SetVetoLength(Vetopt+1, val);};
  void SetVetoInRad(G4int, G4double);
  void SetVetoInRad(G4double val) {SetVetoInRad(Vetopt+1, val);};
  void SetVetoOutRad(G4int, G4double);
  void SetVetoOutRad(G4double val) {SetVetoOutRad(Vetopt+1, val);};
  void SetVetoSegment(G4int num, G4int val);
  void SetVetoSegment(G4int val){SetVetoSegment(Vetopt+1, val);};

public:

  G4int    GetCalorimeterCoincidence(G4int var) {return ScintiCoin[var-1];};

  // get helper functions for generation root files
  G4int GetHbarSegemtsX(){return HbarXseg;};
  G4int GetHbarSegemtsY(){return HbarXseg;};
  G4int GetNumberOfHodoscopes(){int num=0; for(int i=0; i<maxHodoscope; i++) useHodoscope[i] ? num++ : num; return num;};
  G4int GetHodoscopeSegments(int num){return Hododiv[num];};
  G4double GetHodoscopeCenter(int num){return HodoscopePos[num].z();};
  G4double GetHodoscopeLength(int num){return HodoscopeLength[num];};
  G4int GetNumberOfVetos(){int num=0;for(int i=0; i<maxVeto; i++) useVeto[i]?num++ : num;return num;};
  G4int GetVetoSegments(int num){return vetodiv[num];};

  hbarSiPM_SD* Get_SiPM_SD_up(int i, int j)   {return SiPM_SD_up[i][j];};
  hbarSiPM_SD* Get_SiPM_SD_down(int i, int j) {return SiPM_SD_down[i][j];};

  hbarBGOPMT_SD* Get_BGOPMT_SD(int i, int j) {return BGOPMT_SD[i][j];};

  //hbarDetectorSD* Get_BGODet_SD() {return BGODet_SD;};

  void ReadResponseFunctionSiPM();
  std::vector<double> Get_responsefkt_x() {return responsefkt_x;};
  std::vector<double> Get_responsefkt_y() {return responsefkt_y;};
  TH1D* Gethisto_responsefkt() {return histo_responsefkt;};

  G4bool GetUseSensitiveSiPMs() {return UseSensitiveSiPMs;};
  G4bool GetUseSensitiveBGOPMTs() {return UseSensitiveBGOPMTs;};
  G4bool GetUseSensitiveBGO() {return UseSensitiveBGO;};

  std::vector<double> Get_photocath_EFF() { return photocath_EFF;};
  std::vector<double> Get_photocath_PP() { return photocath_PP;};
  double Get_PMTGain() {return PMTGain;};
  void Set_MAPMT_SupplyVoltage(double value) {MAPMT_SupplyVoltage = value;};

private:

  void LoadGDMLHodoscope(G4VPhysicalVolume*, hbarDetectorSD *);
  void BuildOctHodoscope(G4VPhysicalVolume*, hbarDetectorSD *);
  void BuildDetailedHodoscope(G4VPhysicalVolume*, hbarDetectorSD *);
  void BuildveryOldHodoscope(G4VPhysicalVolume*, hbarDetectorSD*);
  void BuildTimePix(G4VPhysicalVolume*, hbarDetectorSD *);
  void BuildFibreDetector(G4VPhysicalVolume*, hbarDetectorSD *);
  void BuildGDMLBGO(G4VPhysicalVolume*, hbarDetectorSD*);
  void BuildBGO(G4VPhysicalVolume*, hbarDetectorSD *);
  void SetAssemblyBar();
  
  G4ThreeVector      ScintiPos[maxScN];
  G4RotationMatrix   ScintiRot[maxScN];
  G4double           ScintiThick[maxScN];
  G4double           ScintiWidth[maxScN];
  G4double           ScintiWidthL[maxScN];
  G4double           ScintiLength[maxScN];
  G4bool             useScinti[maxScN];
  G4int              ScintiCoin[maxScN];  // whether the scintillator is

  void DefineMaterialPropertiesHodor();
  void DefineMaterialPropertiesBGO();
  double CalculatePMTGain(double volts);

  // in coincidence (1) or
  // in anticoincidence (-1) or
  // not in any coincidence (0)
  G4int              Scintipt;            //!< number of current scintillator
  G4int              NumOfLeadLayers[maxScN];
  G4double           LeadThick[maxScN];
  G4int              NumOfScintiSides[maxScN];

  // Added by Clemens -- start
  G4ThreeVector      VetoPos[maxVeto];    //!< position of the Veto counter (and hodocope)
  G4RotationMatrix   VetoRot[maxVeto];    //!< rotation of the Veto counter (and hodocope)
  G4double           VetoOutRad[maxVeto]; //!< outer radius of the Veto counter (and hodocope)
  G4double           VetoInRad[maxVeto];  //!< inner radius of the Veto counter (and hodocope)
  G4double           VetoLength[maxVeto]; //!< length of the Veto counter (and hodocope)
  G4bool             useVeto[maxVeto];    //!< use this specific Veto counter (and hodocope)
  G4int              vetodiv[maxVeto];    //!< number of segnets (and hodocope)
  G4int              Vetopt;              //!< number of current Veto counter (and hodocope)

  G4ThreeVector      HodoscopePos[maxHodoscope];    //!< position of the Hodoscope counter (and hodocope)
  G4RotationMatrix   HodoscopeRot[maxHodoscope];    //!< rotation of the Hodoscope counter (and hodocope)
  G4double           HodoscopeOutRad[maxHodoscope]; //!< outer radius of the Hodoscope counter (and hodocope)
  G4double           HodoscopeInRad[maxHodoscope];  //!< inner radius of the Hodoscope counter (and hodocope)
  G4double           HodoscopeLength[maxHodoscope]; //!< length of the Hodoscope counter (and hodocope)
  G4bool             useHodoscope[maxHodoscope];    //!< use this specific Hodoscope counter (and hodocope)
  G4int              Hododiv[maxHodoscope];    //!< number of segnets (and hodocope)
  G4int              Hodoscopept;              //!< number of current Hodoscope counter (and hodocope)

  G4double           HodoScintWidth[maxHodoscope]; //! Parameters for octagonal Hodoscope, only these two needed, the rest is fully automatically
  G4double           HodoScintHeight[maxHodoscope];

  G4double           BGOScint2014Start[maxBGO2014]; //!< Start Point of 2014 BGO Scintillator Plate
  G4double           BGOScint2014End[maxBGO2014];   //!< End Point of 2014 BGO Scintillator Plate
  G4double           BGOScint2014Diam[maxBGO2014];//!< Height of the 2014 BGO Scintillator Plate
  G4bool             useBGO2014[maxBGO2014];    //!< use this specific BGO2014 Scintillator
  G4int              BGO2014pt;              //!< number of current Hodoscope counter (and hodocope)


  G4ThreeVector      HbarPos;             //!< position of the central Hbar detector
  G4RotationMatrix   HbarRot;             //!< rotation of the central Hbar detector
  G4int              HbarXseg;            //!< number of segments in X axis of the Hbar detector
  G4int              HbarYseg;            //!< number of segments in Y axis of the Hbar detector
  G4double           HbarDetX;            //!< X length of the Hbar Detector
  G4double           HbarDetY;            //!< Y length of the Hbar Detector
  G4double           HbarDetZ;            //!< Z length of the Hbar Detector

  G4VSolid*          ScintiSol[maxScN];
  G4LogicalVolume*   ScintiLog[maxScN];
  G4VPhysicalVolume* ScintiPhy[maxScN];

  // Added by Clemens -- start
  // Veto Counter
  G4VSolid*          VetoBaseSol[maxVeto];
  G4LogicalVolume*   VetoBaseLog[maxVeto];
  G4VPhysicalVolume* VetoBasePhy[maxVeto];
  G4VSolid*          VetoSol[maxVeto];
  G4LogicalVolume*   VetoLog[maxVeto];
  G4VPhysicalVolume* VetoPhy[maxVeto];

 // Veto Counter
  G4VSolid*          HodoscopeBaseSol[maxHodoscope];
  G4LogicalVolume*   HodoscopeBaseLog[maxHodoscope];
  G4VPhysicalVolume* HodoscopeBasePhy[maxHodoscope];
  G4VSolid*          HodoscopeSol[maxHodoscope];
  G4LogicalVolume*   HodoscopeLog[maxHodoscope];
  G4VPhysicalVolume* HodoscopePhy[maxHodoscope];
  // Added by Clemens -- end

  G4LogicalVolume* HodoBarLog[maxHodoscope][31]; //!< Logical Volume of Scintillator Bars in the Octagonal Hodoscope
  G4PVPlacement*   HodoBarPhy[maxHodoscope][31]; //!<

  G4LogicalVolume* BGO2014Log[maxBGO2014];  //!< Logical Volume of the BGO2014 Scintillator inside the Octagonal Hodoscope
  G4PVPlacement*   BGO2014Phy[maxBGO2014];

  hbarSiPM_SD* SiPM_SD_up[2][32];
  hbarSiPM_SD* SiPM_SD_down[2][32];

  hbarBGOPMT_SD* BGOPMT_SD[4][64];
  
  G4String FoilMat;
  
  
  G4double BarX[2];
  G4double BarY[2];
  G4double BarZ[2];
  //
  G4double Lightguide_baseX[2];
  G4double Lightguide_baseY[2];
  G4double Lightguide_topX[2];
  G4double Lightguide_topY[2];
  G4double LightguideZ[2];
  //
  G4RotationMatrix* BarRotation;
  G4ThreeVector     BarPosition;
  G4RotationMatrix* LightguideTopRotation;
  G4ThreeVector     LightguideTopPosition[2];
  G4RotationMatrix* LightguideBottomRotation;
  G4ThreeVector     LightguideBottomPosition[2];
  //
  G4double SiPM_SensorAreaSideLength;
  G4double SiPM_SensorThickness;
  G4double SiPM_GapBetweenSensors;
  //
  G4RotationMatrix* SiPMTopRotation;
  G4ThreeVector     SiPMTopPosition[2];
  G4RotationMatrix* SiPMBottomRotation;
  G4ThreeVector     SiPMBottomPosition[2];
  //
  G4double AluminiumFoilThickness;
  G4double WrappingAirGap;
  //
  G4double GlueThickness;

  // Geometry
  G4Box*             scintiBar_sol[2];
  G4LogicalVolume*   scintiBar_log[2];
  G4VPhysicalVolume* scintiBar_phys[2][32];
  //
  G4SubtractionSolid* scintiBarWrapping_sol[2];
  G4LogicalVolume*    scintiBarWrapping_log[2];
  G4VPhysicalVolume*  scintiBarWrapping_phys[2][32];
  //
  G4Box*			 GlueBarLG_sol[2];
  G4LogicalVolume*   GlueBarLG_log[2];
  G4VPhysicalVolume* GlueBarLG_phys[2][32];
  //
  G4Box*			 GlueLGSiPM_sol[2];
  G4LogicalVolume*   GlueLGSiPM_log[2];
  G4VPhysicalVolume* GlueLGSiPM_phys[2][32];
  //
  G4Trd*             Lightguide_sol[2];
  G4LogicalVolume*   Lightguide_log[2];
  G4VPhysicalVolume* TopLightguide_phys[2][32];
  G4VPhysicalVolume* BottomLightguide_phys[2][32];
  //
  G4SubtractionSolid* LightguideWrapping_sol[2];
  G4LogicalVolume*    LightguideWrapping_log[2];
  G4VPhysicalVolume*  TopLightguideWrapping_phys[2][32];
  G4VPhysicalVolume*  BottomLightguideWrapping_phys[2][32];
  //
  G4SubtractionSolid* SiPM_sol;

  G4LogicalVolume*    HodorSiPMUpLog[2][32];
  G4LogicalVolume*    HodorSiPMDownLog[2][32];

  G4VPhysicalVolume*  HodorSiPMUpPhy[2][32];
  G4VPhysicalVolume*  HodorSiPMDownPhy[2][32];
  
  G4AssemblyVolume* Hodor;
  G4AssemblyVolume*	assemblyBar[2];
  
  ///////////////////////////////////////////////////////////////////////////////////////
  // Fibre detector  
  G4double         InnerFibreRadius;
  G4int            InnerFibreChannels;
  G4double         FibreWidth;
  G4double         InnerFibreLength;
  
  G4double         OuterFibreRadius;
  G4int            OuterFibreChannels;
  G4double         OuterFibreLength;
  
  G4VSolid*        InnerFibreSol;
  G4LogicalVolume* InnerFibreLog[63];       //!< logical volume of scintillating fibres
  G4PVPlacement*   InnerFibrePhy[63];   //!< physical volume of scintillating fibres

  G4VSolid*        OuterFibreSol;
  G4LogicalVolume* OuterFibreLog[100];       //!< logical volume of scintillating fibres
  G4PVPlacement*   OuterFibrePhy[100];  //!< physical volume of scintillating fibres
  //hbarDetectorSD* BGODet_SD;
  

  ///////////////////////////////////////////////////////////////////////////////////////
  G4OpticalSurface * OptSurface;
  G4LogicalSurface * BarWrappingSurface0;
  G4LogicalSurface * BarWrappingSurface1;
  G4LogicalSurface * LightguideWrappingSurface0;
  G4LogicalSurface * LightguideWrappingSurface1;
  
    ////////////////////////////////////////////
    //                 TimePix                // 
    ////////////////////////////////////////////
      
    G4double            fFoilThick;
    G4double            fFoilSizeXY;
    
    G4double            fTPixPosZ;
    G4int               fTPixNumPix;
    G4double            fTPixThick;
    G4double            fTPixPixelXY;
    G4double            fTPixdxcross;
    G4double            fTPixdycross;

    G4double            fElectrodeThick;
    G4double            fElectrodeSizeX;
    G4double            fElectrodeSizeY;  
    G4Box*              fSolidFoil;   //pointer to the solid Foil
    G4LogicalVolume*    fLogicFoil;   //pointer to the logical Foil
    G4PVPlacement*      fPhysiFoil;   //pointer to the physical Foil

    G4Box*              fSolidTPix;   //pointer to the solid TPix pixel
    G4LogicalVolume*    fLogicTPix; //[][];   //pointer to the logical TPix pixel
    G4PVPlacement**      fPhysiTPix;   //pointer to the physical TPix pixels

    G4Box*              fSolidElectrode; //pointer to the solid electrode
    G4LogicalVolume*    fLogicElectrode; //pointer to the logical electrode
    G4PVPlacement*      fPhysiElectrode; //pointer to the physical electrode 
    
    G4UnionSolid*       TpxFlangeSolid;
    G4LogicalVolume*    TpxFlangeLog;
    G4PVPlacement*      TpxFlangePhy;
    G4SubtractionSolid* TpxAperatureSol;
    G4LogicalVolume*    TpxAperatureLog;
    G4PVPlacement*      TpxAperaturePhy;
    G4SubtractionSolid* TpxCopperAperatureSol;
    G4LogicalVolume*    TpxCopperAperatureLog;
    G4PVPlacement*      TpxCopperAperaturePhy;
   
    G4VSolid*           TpxBeampipeSol;
    G4LogicalVolume*    TpxBeampipeLog;
    G4PVPlacement*      TpxBeampipePhys;  

    G4VSolid*           HodoscopeVacuumSol;
    G4LogicalVolume*    HodoscopeVacuumLog;
    G4PVPlacement*      HodoscopeVacuumPhys;
    
  ///////////////////////////////////////////////////////////////////////////////////////    
    
  G4LogicalVolume* Hodor_log;
  G4PVPlacement*  Hodor_phy;

  G4LogicalVolume* BGOboxlog;
  G4PVPlacement*   BGOboxphy;
  /*G4LogicalVolume* BGO_log;
  G4LogicalVolume* Coat_log;
  G4PVPlacement* BGO_phys;
  G4PVPlacement* Coat_phys;*/
  G4LogicalBorderSurface* border_surf;
  G4OpticalSurface* coat_bgo_opsurf;
  G4OpticalSurface* photocath_opsurf;
  G4OpticalSurface* borosilicate_opsurf;

  std::vector<double> Scinti_wavelength;
  std::vector<double> Scinti_PP;
  std::vector<double> Scinti_FAST;
  std::vector<double> bgo_PP;
  std::vector<double> bgo_FAST;
  std::vector<double> rindex_galactic;
  std::vector<double> rindex_air;
  std::vector<double> rindex_scinti;
  std::vector<double> rindex_plexi;
  std::vector<double> rindex_glue;
  std::vector<double> rindex_bgo;
  std::vector<double> rindex_tempax;
  std::vector<double> absorption;
  std::vector<double> Al_real_PP;
  std::vector<double> Al_real;
  std::vector<double> Al_imag_PP;
  std::vector<double> Al_imag;
  std::vector<double> abs_length_carboncoat;
  std::vector<double> abs_length_bgo;
  std::vector<double> photocath_EFF;
  std::vector<double> photocath_EFF1;  
  std::vector<double> photocath_PP;
  std::vector<double> photocath_ReR;
  std::vector<double> photocath_ImR;
  std::vector<double> pmtgain_x;
  std::vector<double> pmtgain_y;  
  std::vector<double> refbgo_wl;
  std::vector<double> refbgo_i;
  std::vector<double> refbgo_img;
  std::vector<double> rindex_carboncoat;
  std::vector<double> rindexc_carboncoat;
  std::vector<double> rindex_x_boros;
  std::vector<double> rindex_x_borosc;
  std::vector<double> rindexc_tempax;
  std::vector<double> boro_abslength;
  
  double PMTGain;
  double MAPMT_SupplyVoltage;

  std::vector<double> responsefkt_x;
  std::vector<double> responsefkt_y;
  TH1D* histo_responsefkt;

  // Added by Clemens -- start
  // Hbar Counter
  G4VSolid* HbarBaseSol;
  G4LogicalVolume* HbarBaseLog;
  G4VPhysicalVolume* HbarBasePhy;
  G4VSolid* HbarYSegSol;
  G4LogicalVolume* HbarYSegLog;
  G4VPhysicalVolume* HbarYSegPhy;
  G4VSolid* HbarSol;
  G4LogicalVolume* HbarLog;
  G4VPhysicalVolume* HbarPhy;
  // Added by Clemens -- end

  G4VSolid*          LeadSol[maxScN];
  G4LogicalVolume*   LeadLog[maxScN];
  G4PVParameterised* LeadPhy[maxScN];

  G4VPVParameterisation *LeadParam[maxScN];

  G4bool useHbarCounter;
  G4bool UseGDMLHodor;
  G4bool UseGDMLBGO;
  G4bool UseSensitiveSiPMs;
  G4bool UseSensitiveBGOPMTs;
  G4bool UseSensitiveBGO;
  G4int SaveWaveformsToDat;
  G4int WriteWaveformsToPDF;

  hbarCPTMess * CPTMessenger;


};

#endif
