#ifndef hbarRunAction_h
#define hbarRunAction_h 1

#include "G4UserRunAction.hh"
#include "hbarDetConst.hh"
#include "globals.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TNtuple.h"

#include "hbarRootHit.h"
#include "hbarBuildRealRootTree.hh"
#include "QuantumNumbers.hh"
#include "StrongFieldQuantumNumbers.hh"
#include "hbarBGOPMT_SD.hh"

class G4Run;
class hbarRunMessenger;
class hbarPrimGenAction;
class hbarDetectorHit;

//! This is the "master" class, which governs a GEANT4 run
/*!
  This is the "master" class, which governs a GEANT4 run. It has
  two important functions:

  \li BeginOfRunAction()
  \li EndOfRunAction()
 */
class hbarRunAction : public G4UserRunAction
{
public:
  hbarRunAction(hbarDetConst*);
  ~hbarRunAction();

public:
  //! This function is executed before every run.
  /*!
    Most importantly, it resets the hit counter and the timer, and it creates
  a new output ROOT file which stores the simulation results (detector hits,
  Hbar stopping positions, etc).
   */
  void BeginOfRunAction(const G4Run*);

  //! This function is executed after every run.
  /*!
    It  writes the output ROOT file with the results and all the
    simulation-related parameters to disk.
   */
  void EndOfRunAction(const G4Run*);

  void SetFileName(G4String); //!< sets the output filename

  void SetfilenamedatBGO(G4String); 
 
  void SetHodorWaveformFileName(G4String);
  void SetBGOPMTFileName(G4String);

  void SetUse3D(G4bool useflag){Use3dGrid = useflag;}; ///Use 3D Grid (E,B,Angle)

  void SetVerboseStepping(G4bool val ///Value
			  ); ///Set verbose stepping.

  void SetVerboseTypeTransitions(G4bool val
								 );
  G4bool GetVerboseTypeTransitions();


  G4bool GetVerboseStepping(); ///Getter.


  void SetVerboseDecay(G4bool val ///Value
			  ); ///Set verbose stepping.


  G4bool GetVerboseDecay(); ///Getter.


  hbarDetConst* GetDetector() {return detConst;}; //!< returns the full geometry
  TH1I*  GetAngleDifferenceHistogram() {return angleDiffDist;};
  TH1I*  GetAngleDifferenceHistogramAll() {return angleDiffDistAll;};
  TH1F*  GetVelocityHistogramDet(){return veloDistDet;} // Added by Clemens
  TH1F*  GetAngularDistHistogramDet(){return angleDistDet;} // Added by Clemens
  TH1I*  GetVelocityHistogram() {return veloDist;};

  TH1I* GetCavityEntranceQuantumNumberHistogram(){ return cavityEntranceQuantumNumber; }
  TH2I* GetDetectorCavityQuantumNumberHistogram() { return detectorCavityQuantumNumber; }

  //! returns the exit angle in respect to the beam axis after the cavity
  G4double GetExitAngle() {return exitAngle;};
  //! returns the entrance angle in respect to the beam axis before the cavity
  G4double GetEntranceAngle() {return entranceAngle;};
  //! return the total velocity
  G4double GetVelocity() {return velocity;};
  //! return the velocity in beam axis
  G4double GetVelocityZ() {return velocityZ;};


  // Added by Clemens -- start
  //! return the velocity at the dummy detector
  G4double GetVelocityDet() {return velocitydet;};
  //! return the velocity in beam direction at the dummy detector
  G4double GetVelocityZDet() {return velocityZdet;};

  G4double GetQuantumNumberAtCavity() { return quantumNumberAtCavity; }
  G4double GetQuantumNumberAtDetector() { return quantumNumberAtDetector; }
  //! returns the imact angle at the dummy detector, this is \a NOT working properly
  G4double GetImpactAngleDet() {return impactangledet;};
  // Added by Clemens -- stop

  //! returns the X axis boundry of the cavity
  /*!
    this function is used to determin the time the particle spends inside the
    Cavity in hbarStepAction.
  */
  G4double GetPositionX_Cavity() {return posX_Cavity;};
  //! returns the Y axis boundry of the cavity
  /*!
    this function is used to determin the time the particle spends inside the
    Cavity in hbarStepAction.
  */
  G4double GetPositionY_Cavity() {return posY_Cavity;};
  //! returns the time  when the particle enters the cavity
  G4double GetEntranceTime_Cavity() {return enterTime_Cavity;};
  //! returns the time  when the particle leaves the cavity
  G4double GetExitTime_Cavity() {return exitTime_Cavity;};

  hbarPrimGenAction* GetPrimaryGeneratorAction() {return primGenAction;};

  //! returns the ntupel that buildes the StopTree
  /*!
    The Ntupel returned by this function contains the x,y,z values of
    the annihilation positions for every produced Hbar
   */
  TNtuple*  GetStopNtuple() {return stopNt;};
  TNtuple*  GetDummyDetNtuple() {return dummydetNt;};

  //! returns the TTree object that buildes the HitTree
  /*!
    The TTree returned by this function contains all hits in "real" detectors,
    currently it recordes every particle hit as TTree entry, this has to be
    changed in the near future to macht our measured data sets.

    Current structure:
    - event
      - event.event   - the evenumber of the primary particle
      - event.hit     - true (1) if the primary particle has hit the dummy detector
    - energy
      - energy.Edep   - energy deposited in the dtector
      - energy.Ekin   - kinetic energy of the particle
    - time          - the relative particle time
    - positon
      - position.x    - X position of the hit
      - position.y    - Y position of the hit
      - position.z    - Z position of the hit
    - momentumdir
      - momentumdir.x - Momentum in X direction
      - momentumdir.y - Momentum in Y direction
      - momentumdir.z - Momentum in Z direction
    - particle      - name of the particle
    - detector      - name of the hit detector
   */
  TTree*    GetHitTree()    {return hitTree;};
  TTree*	GetHodorTree()  {return HodorTree;};
  TFile*	GetHodorFile()  {return HodorFile;};
  TTree*	GetBGOPMTTree() {return BGOPMTTree;};
  TFile*	GetBGOPMTFile() {return BGOPMTFile;};

  G4bool     Use3dGrid;

  //TTree*    GetTTree()      {return rootTTree;};

  //! store exit angle at cavity
  void SetExitAngle(G4double val) {exitAngle = val;};
  //! store entrance angle at cavity
  void SetEntranceAngle(G4double val) {entranceAngle = val;};
  //! store total velocity at cavity
  void SetVelocity(G4double val) {velocity = val;};
  //! store total velovity in beam direction at cavity
  void SetVelocityZ(G4double val) {velocityZ = val;};

  void SetQuantumNumberAtCavity(G4int val) { quantumNumberAtCavity = val;}
  void SetQuantumNumberAtDetector(G4int val) { quantumNumberAtDetector = val; }

  // Added by Clemens -- start
  //! store total velocity at the dummy detector
  void SetVelocityDet(G4double val) {velocitydet = val;};
  //! store total velocity in beam direction at the dummy detector
  void SetVelocityZDet(G4double val) {velocityZdet = val;};
  //! store imact angle at the dummy detector
  void SetImpactAngleDet(G4double val) {impactangledet = val;};
  // Added by Clemens -- stop

  void SetPositionX_Cavity(G4double val) {posX_Cavity = val;};
  void SetPositionY_Cavity(G4double val) {posY_Cavity = val;};
  void SetEntranceTime_Cavity(G4double val) {enterTime_Cavity = val;};
  void SetExitTime_Cavity(G4double val) {exitTime_Cavity = val;};

  void ConnectPrimGenAction(hbarPrimGenAction* val) {primGenAction = val;};

  void AddHitCounter(G4int val = 1) {hitCounter += val;};
  void ResetHitCounter() {hitCounter = 0;};
  G4int GetHitCounter() {return hitCounter;};

  //! stores the parameters of a hit in the hbarRootHit structure and fills the TTree
  void FillHit(hbarDetectorHit *aHit);

  //! fills the TTree containing the real hits from the single particle HitTree
  /*!
    This function is called at the end of each run. It loops over the TTree
    containing the single particle hits and calculated the corresponding values
    for a real full event as it  would be produced by the CPT detector.
   */
  void FillRealHitTree();
  //    G4double GetWindow4Curve() {return Win4Curve;};
  //    G4double GetWindow4Radius() {return Win4Radius;};


 // void AddBGOHitCounter(G4int val = 1) {BGOHitCounter += val;};
  //G4int GetBGOHitCounter() {return BGOHitCounter;};

private:

  // histogram to store the difference between the angle of the magnetic field
  // at the exit of sextupole #1 and the angle of the magnetic field
  // at the entrance of sextupole #2
  TH1I            *angleDiffDist;    //!< only those particles which reach the detector
  TH1I            *angleDiffDistAll; //!< all particles that reach sextupole #2
  TH1I            *veloDist;         //!< all particles that reach sextupole #2
  // Added by Clemens -- start
  TH1F            *veloDistDet;      //!< all particles that reach the detector
  TH1F            *angleDistDet;     //!< all particles that reach the detector
  // Added by clemens -- stop
  TNtuple         *stopNt;           //!< (X,Y,Z) coordinates of all stopped pbars
  TNtuple         *dummydetNt;       //!< stopping information of the hbars in the dummy detector
  hbarRootHit     theHit;            //!< structure that contains the single particle hits
  TTree           *hitTree;          //!< contains the hits in the detectors
  //TTree           *rootTTree;        //!< contains the data as it would be produced in a measurement

  TH1I            *cavityEntranceQuantumNumber; //!< Contains the principal quantum number at cavity entrance.
  TH2I            *detectorCavityQuantumNumber; ///Quantum number at the detector.


  ////    TH3D *pbarXYZHe;
  ////    TH2D *pbarEff;
  TFile           *rootfile;

  G4String 		   HodorWaveformFileName;
  TFile			  *HodorFile;
  TTree		      *HodorTree;
  G4String 		   BGOPMTFileName;
  TFile			  *BGOPMTFile;
  TTree		      *BGOPMTTree;
  //G4int			   BGOHitCounter;

  G4String		   filenamedatBGO;


  G4String           fileName;
  hbarRunMessenger*  runMessenger;
  hbarDetConst*      detConst;
  G4int              hitCounter;
  G4double           exitAngle;
  G4double           entranceAngle;
  G4double           velocity;
  G4double           velocityZ;
  G4double           posX_Cavity;
  G4double           posY_Cavity;
  G4double           enterTime_Cavity;
  G4double           exitTime_Cavity;
  G4bool             isVerboseStepping;
  G4bool             isVerboseDecay;
  G4bool             isVerboseTypeTransitions;
  G4int              quantumNumberAtCavity;
  G4int              quantumNumberAtDetector;



  // Added by Clemens -- start
  G4double velocitydet;
  G4double velocityZdet;
  G4double impactangledet;
  // Added by clemens -- stop

  hbarPrimGenAction*   primGenAction;
};

#endif

