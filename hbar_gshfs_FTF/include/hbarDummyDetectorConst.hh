#ifndef hbarDummyDetectorConst_hh
#define hbarDummyDetectorConst_hh 1

#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"

#include "G4ChordFinder.hh"

#include "hbarDefinitions.hh"
#include "hbarDummyDetMess.hh"

class hbarDummyDetMess;


class hbarDummyDetectorConst : public G4VUserDetectorConstruction
{
public:
  hbarDummyDetectorConst(G4UserLimits*);
  ~hbarDummyDetectorConst();
  G4VPhysicalVolume * Construct();

  G4double GetDetectorStart() {return DetCenter - DetLength/2.0;};

  void SetDetectorLength(G4double val) {DetLength = val;};
  void SetDetectorDiameter(G4double val) {DetDiam = val;};
  void SetDetectorCenter(G4double val) {DetCenter = val;};
  G4double GetDetectorLength() {return DetLength;};
  G4double GetDetectorDiameter() {return DetDiam;};
  G4double GetDetectorCenter() {return DetCenter;};


private:
  G4double           DetLength;
  G4double           DetDiam;
  G4double           DetCenter;
  G4Tubs*            DetSol;
  G4LogicalVolume*   DetLog;
  G4VPhysicalVolume* DetPhy;

  hbarDummyDetMess * DummyDetMess;
  G4UserLimits *     alim;

};



#endif
