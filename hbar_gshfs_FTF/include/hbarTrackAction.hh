#ifndef hbarTrackingAction_h
#define hbarTrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "globals.hh"

#include "hbarTrackActionMess.hh"
#include "hbarTrajectory.hh"
#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "G4ThreeVector.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "hbarUserTrackInformation.hh"

class hbarTrackActionMess;

//! This class controls the particle tracks.
/*!
  This class controls the particle tracks. It has a PreUserTrackingAction()
  and a PostUserTrackingAction() function, which are called at the beginning
  and at the end of each track, respectively. In the PreUserTrackingAction(),
  it is decided whether the particle trajectory should be stored or not, but 
  in principle it can be used for other things too. If the trajectory is stored,
  then it is available at the end of the event for e.g. to draw it. The 
  PostUserTrackingAction() doesn't do anything now.
 */
class hbarTrackAction : public G4UserTrackingAction {

public:
  hbarTrackAction();
  ~hbarTrackAction();
   
  //! This function is called before the tracking of a particle begins  
  /*!
    Currently this function does nothing than allowing to set which 
    trajectories are stored for visualisation.
   */
  void PreUserTrackingAction(const G4Track*);
  //! This function is called after the tracking of a particle has ended
  void PostUserTrackingAction(const G4Track*);

  G4double GetTrackLengthInPast() {return trackLength[(trackLengthPt+1)%10];};
  void SetTrackLength(G4double);

  void SetStoreFlag   (G4String val)  {storeFlag = val;};

private:
  G4double                    trackLength[10];     //!< Track length _1-10_steps_ago_
  G4int                       trackLengthPt;
  G4String                    storeFlag;
  hbarTrackActionMess*        trackMessenger;
};

#endif
