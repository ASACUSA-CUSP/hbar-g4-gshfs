#ifndef hbarPhysicsListMessenger_h
#define hbarPhysicsListMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarPhysicsList;
class G4UIdirectory;
class G4UIcmdWithAString;


//! This is the PhysicsList messenger class, for macro definitions
/*!
  The AntiProton messenger defines the following macros:
  - /process/setElectromagneticList
  - /process/setHadronicList
  - /process/setUseSMIPbarAnnihilation
 */
class hbarPhysicsListMess: public G4UImessenger
{
public:  
  hbarPhysicsListMess(hbarPhysicsList* );
  ~hbarPhysicsListMess();
  
  void SetNewValue(G4UIcommand*, G4String);

  
private:  
  hbarPhysicsList*       physList;                     //!< pointer to physics list
  
  G4UIcmdWithAString*    setElectromagneticCmd;        //!< setElectromagneticList
  G4UIcmdWithAString*    setHadronicCmd;               //!< setHadronicList
  G4UIcmdWithAString*    setUseSMIPbarAnnihilationCmd; //!< setUseSMIPbarAnnihilation
  G4UIcmdWithAString*    SetConstructOptPhotonsCmd;
};

#endif

