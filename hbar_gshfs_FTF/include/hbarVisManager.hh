// Alternatively, you can implement an empty function here and just
//   register the systems you want in your main(), e.g.:
//   G4VisManager* myVisManager = new MyVisManager;
//   myVisManager -> RegisterGraphicsSystem (new MyGraphicsSystem);

#ifndef hbarVisManager_h
#define hbarVisManager_h 1

#ifdef G4VIS_USE

#include "G4VisManager.hh"

class hbarVisManager: public G4VisManager {

public:

  hbarVisManager ();

private:

  void RegisterGraphicsSystems ();

};

#endif

#endif
