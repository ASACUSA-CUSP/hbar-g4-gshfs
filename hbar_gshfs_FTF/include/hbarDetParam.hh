
#ifndef hbarDetParam_h
#define hbarDetParam_h 1

#include "globals.hh"
#include "G4VPVParameterisation.hh"

class G4VPhysicalVolume;
class G4Tubs;

// Dummy declarations to get rid of warnings ...
class G4Trd;
class G4Trap;
class G4Cons;
class G4Orb;
class G4Sphere;
class G4Torus;
class G4Para;
class G4Hype;
class G4Box;
class G4Polycone;
class G4Polyhedra;

//! The detector parameterisation class, used if multiple copies of a detector are placed
/*!
  This is a so called detector parameterisation class, which does some 
  computations when one wants to place multiple copies of a detector piece 
  into the geometry. In this simulation, these are the lead layers in between 
  the scintillation layers. The scintillator layers themselves are not placed
  this way. This is because the scintillator layers are defined as a single
  block of scintillator, into which the lead layers are placed, so this way
  the scintillator also becomes layered. When reading out the deposited energy
  in the scintillator, the sum of all layers are read out (because the layers 
  are one entity in GEANT) and written into an hbarDetectorHit.
*/
class hbarDetParam : public G4VPVParameterisation
{
public:

  hbarDetParam(G4double scintiThick, G4int layerNum, G4double leadThick);
  virtual ~hbarDetParam();
  void ComputeTransformation(const G4int copyNo,
			     G4VPhysicalVolume *physVol) const;
  /*
    void ComputeDimensions(      G4Trd &leadLayer,
    const G4int copyNo,
    const G4VPhysicalVolume *physVol) const;
  */

private:  // Dummy declarations to get rid of warnings ...

  void ComputeDimensions (G4Trd&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Tubs&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Trap&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Cons&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Orb&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Sphere&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Torus&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Para&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Hype&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Box&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Polycone&,const G4int,const G4VPhysicalVolume*) const {}
  void ComputeDimensions (G4Polyhedra&,const G4int,const G4VPhysicalVolume*) const {}

private:

  G4int LayerNum;
  G4double LeadThick;
  G4double ScintiThick;
};

#endif
