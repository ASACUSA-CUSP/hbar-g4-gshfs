#ifndef hbarCPTMess_hh
#define hbarCPTMess_hh 1

#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarCPTConst.hh"


class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class hbarCPTConst;


class hbarCPTMess : public G4UImessenger
{
public:
  hbarCPTMess(hbarCPTConst* );
  ~hbarCPTMess();

  void SetNewValue(G4UIcommand*, G4String);
private:
  hbarCPTConst *               hbarCPT; ///Pointer to the CUSP detector.
  G4UIdirectory*             hbardetCalDir;         //!< /setup/detector/

  G4UIcmdWithAnInteger*      ScUseCmd;              //!< setUseCalorimeter
  G4UIcmdWithADoubleAndUnit* ScThickCmd;            //!< setCalorimeterThickness
  G4UIcmdWithADoubleAndUnit* ScLengthCmd;           //!< setCalorimeterLength
  G4UIcmdWithADoubleAndUnit* ScWidthCmd;            //!< setCalorimeterWidth
  G4UIcmdWithADoubleAndUnit* ScWidthLCmd;           //!< setCalorimeterLargerWidth
  G4UIcmdWith3VectorAndUnit* ScCenterCmd;           //!< setCalorimeterCenter
  G4UIcmdWith3VectorAndUnit* ScAngleCmd;            //!< setCalorimeterAngle
  G4UIcmdWithADoubleAndUnit* LeadThickCmd;          //!< setLeadLayerThickness
  G4UIcmdWithAnInteger*      LeadNumCmd;            //!< setNumberOfLeadLayers
  G4UIcmdWithAnInteger*      ScSidesNumCmd;         //!< setNumberOfCalorimeterSides

  // Added by Clemens -- start
  G4UIcmdWithAnInteger*      VetoUseCmd;            //!< setUseVeto
  G4UIcmdWithAnInteger*      VetoSetDivCmd;         //!< setVetoSegmentation
  G4UIcmdWithADoubleAndUnit* VetoLengthCmd;         //!< setVetoLength
  G4UIcmdWithADoubleAndUnit* VetoInRadCmd;          //!< setVetoInRad
  G4UIcmdWithADoubleAndUnit* VetoOutRadCmd;         //!< setVetoOutRad
  G4UIcmdWith3VectorAndUnit* VetoCenterCmd;         //!< setVetoCenter
  G4UIcmdWith3VectorAndUnit* VetoAngleCmd;          //!< setVetoAngle

  G4UIcmdWithAnInteger*      HodoscopeUseCmd;       //!< setUseHodoscope
  G4UIcmdWithAnInteger*      HodoscopeSetDivCmd;    //!< setHodoscopeSegmentation
  G4UIcmdWithADoubleAndUnit* HodoscopeLengthCmd;    //!< setHodoscopeLength
  G4UIcmdWithADoubleAndUnit* HodoscopeInRadCmd;     //!< setHodoscopeInRad
  G4UIcmdWithADoubleAndUnit* HodoscopeOutRadCmd;    //!< setHodoscopeOutRad
  G4UIcmdWith3VectorAndUnit* HodoscopeCenterCmd;    //!< setHodoscopeCenter
  G4UIcmdWith3VectorAndUnit* HodoscopeAngleCmd;     //!< setHodoscopeAngle

  G4UIcmdWith3Vector*        HbarSetDivCmd;         //!< setHbarDiv
  G4UIcmdWith3VectorAndUnit* HbarSetDimenCmd;       //!< setHbarDimensions
  G4UIcmdWith3VectorAndUnit* HbarCenterCmd;         //!< setHbarCenter
  G4UIcmdWith3VectorAndUnit* HbarAngleCmd;          //!< setHbarAngle

  G4UIcmdWithADoubleAndUnit* HodoScintWidthCmd;    //!< setHodoscopeScInnWidth
  G4UIcmdWithADoubleAndUnit* HodoScintHeightCmd;   //!< setHodoscopeScOutWidth

  G4UIcmdWithADoubleAndUnit* BGOScint2014StartCmd; //!< setBGOScint2014StartPoint
  G4UIcmdWithADoubleAndUnit* BGOScint2014EndCmd;   //!< setBGOScint2014EndPoint
  G4UIcmdWithADoubleAndUnit* BGOScint2014DiamCmd;  //!< setBGOScint2014Height
  G4UIcmdWithAnInteger*      BGO2014UseCmd;        //!< setUseBGO2014

  G4UIcmdWithAnInteger*      HbarUseGDMLHodorCmd;
  G4UIcmdWithAnInteger*      HbarUseGDMLBGOCmd;
  G4UIcmdWithAnInteger*      SetUseSensitiveSiPMsCmd;
  G4UIcmdWithAnInteger*      SetUseSensitiveBGOPMTsCmd;
  
  G4UIcmdWithAnInteger*		 SaveWaveformsToDatCmd;
  G4UIcmdWithAnInteger*		 WriteWaveformsToPDFCmd;	
  G4UIcmdWithADouble* 	 	 SetMAPMT_SupplyVoltageCmd;
  
  G4UIcmdWithAString*        FoilMatCmd;
};


#endif
