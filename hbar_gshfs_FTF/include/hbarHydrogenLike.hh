#ifndef hbarHydrogenLike_h
#define hbarHydrogenLike_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "hbarHydrogenLikeAtomQuantumNumbers.hh"
#include "G4ParticleTable.hh"
#include "QuantumNumbers.hh"
#include "Randomize.hh"
#include "TMath.h"

#include "rfacInterpolation.hh"
#include "rfacDecayableObject.hh"
#include "RLException.hh"


#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"


//! The HydrogenLike class is the base class for antihydrogen and hydrogen classes.
/*!
  This class contains all identical properties of the atoms, including quantum numbers
  and magnetic moment.
 */
class hbarHydrogenLike : public G4ParticleDefinition, public hbarHydrogenLikeAtomQuantumNumbers, public rfacDecayableObject
{
protected:


  bool decayEnabled; ///If this particle can decay.
  bool levelSplittingEnabled; ///If energy levels should be used in the force equation for this particle.


  double GetTimeToNextDecay(); ///Returns time to next decay. Also sets the DecayChannel to use for this decay, as well as the total decay rate.
  
protected:

  hbarHydrogenLike(const G4String& aName ///Name.
		   );   //! Constructor. Called by derived class.


  ~hbarHydrogenLike();  //! The dummy destructor

public:

  bool GetSplittingEnabled(); ///Getter for this property.

  void SetSplittingEnabled(bool val ///New value.
			   ); ///Setter for this property.
  
  bool GetDecayEnabled(); ///Getter for this property.

  void SetDecayEnabled(bool val ///New value.
		       ); ///Setter for this property.

  void StepTime(double timeStep //The time step.
		); ///Step time (tick for decays etc.).

  //! returns the magnetic moment
  /*!
    This function return the magnetic moment in respect to the current quantum
    numbers und an external fieldstrength 
    \param Field is a G4double value containing the external fieldstrangth
   */
  G4double GetMu(G4double Field ///The magnetic field.
		 ); ///Returns the mu, given the magnetic field. This is NOT actually MU, but rather the factor that is not the magnetic field strength, in the Zeeman effect term. Unit: MeV.

  G4double GetgFactor() const; //Returns the gJ factor.

protected:
  G4double GetMuPrefix() const; ///Returns the thing multiplied by the Bohr magneton to get the magnetic moment.
  //basically g_J \sqrt{j(j+1)}


private:
  static double GetU01Random() { return CLHEP::HepRandom::getTheEngine()->flat(); }
  
};

#endif
