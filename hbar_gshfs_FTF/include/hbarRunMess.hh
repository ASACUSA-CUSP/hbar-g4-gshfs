#ifndef hbarRunMessenger_h
#define hbarRunMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"
#include "G4ios.hh"

class hbarRunAction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;

//! This messenger class defines the ROOT output file
/*!
  This messenger class defines only one user command: to set the name of the output ROOT file.
  - /file/setFileName
 */
class hbarRunMessenger: public G4UImessenger
{
public:

  hbarRunMessenger(hbarRunAction* );
  ~hbarRunMessenger();
  
  void SetNewValue(G4UIcommand* ,G4String );
  
private:

  hbarRunAction*             runAction;   //!< pointer tu runAction
   
  G4UIdirectory*             fileDir;     //!< /file/

  G4UIcmdWithAString*        FileNameCmd; //!< setFileName

  G4UIcmdWithAString *       VerboseSteppingCmd; ///For verbose stepping.

  G4UIcmdWithAString *       VerboseDecayCmd; ///For verbose stepping.

  G4UIcmdWithAString *       VerboseTypeTransitionsCmd; ///Verbose type transitions.

  G4UIcmdWithAString *       filenamedatBGOCmd;

  G4UIcmdWithAString *       HodorWaveformFileNameCmd;
 
  G4UIcmdWithAString *		 BGOPMTFileNameCmd;

};

#endif

