#ifndef hbarEventAction_h
#define hbarEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "hbarHydrogen.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarException.hh"

class hbarEventActionMess;
class hbarRunAction;


//! The genat4 action class
/*!
  This is the usual GEANT4 event action class. One particle created by 
  the primary generator and all of its secondary particles are one event. 
  The ::BeginOfEventAction() function runs in the beginning of every event. 
  It resets some variables into which various information are collected. 
  The ::EndOfEventAction() function runs at the end of every event. It does 
  the follow things:

    \li Determines whether the Hbar atom hit the dummy Hbar detector (see the hbarDetConst class)
    \li If yes, it declares this event a "hit" and fills the coordinates of the Hbar stopping position into the output "stop ntuple" (see the hbarRunAction class)
    \li It draws the particle trajectories which were requested by the user (see the hbarEventActionMess class)
    \li Reads the hits from the sensitive detectors
    \li Fills these data into the output "hit tree" (see the hbarRunAction class)

 */
class hbarEventAction : public G4UserEventAction
{
public:
  hbarEventAction(hbarRunAction*);
  virtual ~hbarEventAction();

public:
  virtual void   BeginOfEventAction(const G4Event*);
  virtual void   EndOfEventAction(const G4Event*);

  void SetDrawFlag                   (G4String val) {drawFlag = val;};
  void SetPrintModulo                (G4int    val) {printModulo = val;};
  void SetPosFlag                    (G4String val) {posFlag = val;};
  void SetEventFlag                  (G4String val) {eventFlag = val;};
  void SetCoincidenceMultiplicity    (G4int val)    {coinMulti = val;};
  void SetAntiCoincidenceMultiplicity(G4int val)    {anticoinMulti = val;};
  void SetTriggerLevel               (G4double val) {triggerLevel = val;};

private:
  G4String                    drawFlag;
  G4String                    eventFlag;
  G4String                    posFlag;
  G4int                       printModulo;
  G4int                       scintiCollID;
  G4int                       coinMulti;      //!< coincidence multiplicity
  G4int                       anticoinMulti;  //!< anticoincidence multiplicity
  G4double                    triggerLevel ;  //!< trigger level
  // the above 3 are only used for drawing
  hbarEventActionMess*        eventMessenger;
  hbarRunAction*              runAction;
};

#endif

    
