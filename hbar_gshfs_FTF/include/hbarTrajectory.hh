#ifndef hbarTrajectory_h
#define hbarTrajectory_h 1

#include "G4VTrajectory.hh"
#include "G4Allocator.hh"
#include <stdlib.h>                 // Include from 'system'
#include "G4ios.hh"                 // Include from 'system'
#include <vector>                   // G4RWTValOrderedVector
#include "globals.hh"               // Include from 'global'
#include "G4ParticleDefinition.hh"  // Include from 'particle+matter'
#include "G4TrajectoryPoint.hh"     // Include from 'tracking'
#include "G4Track.hh"
#include "G4Step.hh"
#include "hbarUserTrackInformation.hh"
#include "hbarTrackDictionaryEntry.hh"
#include <utility>
#include <map>


class G4Polyline;                   // Forward declaration.

typedef std::vector<std::pair<G4VTrajectoryPoint*, hbarTrackDictionaryEntry> >  hbarTrajectoryPointContainer;

//! properties of trajectories for visualization
/*!
  This class defines all vilual properties on how trajectories are drawen.
 */
class hbarTrajectory : public G4VTrajectory
{
public:

  // Constructor/Destrcutor
  hbarTrajectory();
  
  hbarTrajectory(const G4Track* aTrack);
  hbarTrajectory(hbarTrajectory &);
  virtual ~hbarTrajectory();
  
  // Operators
  inline void* operator new(size_t);
  inline void  operator delete(void*);
  inline int operator == (const hbarTrajectory& right) const
  {return (this==&right);} 
  
  // Get/Set functions 
  inline G4int GetTrackID() const
  { return fTrackID; }
  inline G4int GetParentID() const
  { return fParentID; }
  inline G4String GetParticleName() const
  { return ParticleName; }
  inline G4double GetCharge() const
  { return PDGCharge; }
  inline G4int GetPDGEncoding() const
  { return PDGEncoding; }
  inline G4ThreeVector GetInitialMomentum() const
  { return initialMomentum; }
  
  // Other member functions
  virtual void ShowTrajectory(std::ostream& os=G4cout) const;
  
  //! defines the visual appearance of the trajectories
  /*!
    If i_mode>=0, draws a trajectory as a polyline (blue for
    positive, red for negative, green for neutral, dark green for lowfield
    seekers and dark red for highfield seekers) 
    and, if i_mode!=0,
    adds markers - yellow circles for step points and magenta squares
    for auxiliary points, if any - whose screen size in pixels is
    given by std::abs(i_mode)/1000.  E.g: i_mode = 5000 gives easily
    visible markers.
  */
  virtual void DrawTrajectory(G4int i_mode=0) const;
  //virtual void DrawTrajectory() const {G4VTrajectory::DrawTrajectory();}
  virtual void DrawTrajectory() const {DrawTrajectory(50);}
  virtual void AppendStep(const G4Step* aStep);
  virtual int GetPointEntries() const { return positionRecord->size(); }
  virtual G4VTrajectoryPoint* GetPoint(G4int i) const 
  { return (*positionRecord)[i].first; }
  virtual hbarTrackDictionaryEntry GetPointQNbr(G4int i) const
  { return (*positionRecord)[i].second; }
  virtual void MergeTrajectory(G4VTrajectory* secondTrajectory);
  
  G4ParticleDefinition* GetParticleDefinition();
  
  virtual const std::map<G4String,G4AttDef>* GetAttDefs() const;
  virtual std::vector<G4AttValue>* CreateAttValues() const;
  
  //---------
private:
  //---------
  
  hbarTrajectoryPointContainer* positionRecord;
  G4int                         fTrackID;
  G4int                         fParentID;
  G4int                         PDGEncoding;
  G4double                      PDGCharge;
  G4String                      ParticleName;
  G4ThreeVector                 initialMomentum;
  G4ParticleDefinition         *fpParticleDefinition;
  G4bool                        useColouring;         // Added by Clemens
};

#if defined G4TRACKING_ALLOC_EXPORT
extern G4DLLEXPORT G4Allocator<hbarTrajectory> hbarTrajectoryAllocator;
#else
extern G4DLLIMPORT G4Allocator<hbarTrajectory> hbarTrajectoryAllocator;
#endif

inline void* hbarTrajectory::operator new(size_t)
{
  void* aTrajectory;
  aTrajectory = (void*)hbarTrajectoryAllocator.MallocSingle();
  return aTrajectory;
}

inline void hbarTrajectory::operator delete(void* aTrajectory)
{
  hbarTrajectoryAllocator.FreeSingle((hbarTrajectory*)aTrajectory);
}


#endif
