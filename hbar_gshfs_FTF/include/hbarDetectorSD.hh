#ifndef hbarDetectorSD_h
#define hbarDetectorSD_h 1

#include "G4VSensitiveDetector.hh"
#include "hbarDetectorHit.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

//! This class tells what to do when a particle hits a sensitive detector
/*!
  This class tells what to do when a particle hits a sensitive detector: 
  create a new detector hit (defined in hbarDetectorHit) and fill it with the 
  data (time, detector ID, deposited energy, etc.) which is read out from the 
  detector. In the simulation, the sensitive detectors are the scintillator 
  plates (this is defined in the hbarDetConst class). The deposited energy is 
  not converted to photons inside the scintillators, but it is reasonable to 
  assume that in real life the light output of the scintillator will be 
  proportional to the deposited energy. So by reading the deposited energy 
  of each hit in the simulation, one can, for example, apply an energy 
  threshold (XX eV) just like one would apply a threshold (YY mV) on the d
  iscriminator in the scintillator->PMT->discriminator chain in real life.
*/
class hbarDetectorSD : public G4VSensitiveDetector
{

public:
  hbarDetectorSD(G4String name);
  ~hbarDetectorSD();

  //! initialize hit collection
  void Initialize(G4HCofThisEvent*HCE);
  //! This function creates a hit in a sensitive detector. Also the naming scheme of the detector is don in this function.
  G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
  //! empty function
  void EndOfEvent(G4HCofThisEvent*HCE);
  //! empty function
  void clear();
  //! empty function
  void DrawAll();
  //! empty function
  void PrintAll();
  

private:
  hbarDetectorHitsCollection *scintiCollection; //!< collection of all scintillator hits


public:
};




#endif

