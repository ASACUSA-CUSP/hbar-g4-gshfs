#ifndef hbarCUSPMess_hh
#define hbarCUSPMess_hh 1
#include "globals.hh"
#include "G4UImessenger.hh"
#include <G4UIcmdWith3Vector.hh>
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "hbarCUSPConst.hh"


class hbarCUSPConst;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3VectorAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class hbarCUSPMess : public G4UImessenger
{
public:
  hbarCUSPMess(hbarCUSPConst* );
  ~hbarCUSPMess();
  
  void SetNewValue(G4UIcommand*, G4String);
private:
  hbarCUSPConst *               hbarCUSP; ///Pointer to the CUSP detector.


  // Added by tajima -- start
  G4UIcmdWithADoubleAndUnit*    CenterOfTheCuspCmd;
  G4UIcmdWithADoubleAndUnit*    CenterOfTheMikiLenseCmd;
  G4UIcmdWithADoubleAndUnit*    CenterOfTheMLSDCmd;
  G4UIcmdWithADoubleAndUnit*    CenterOfTheSGVDCmd;
  G4UIcmdWithAnInteger*         UseCUSPprevCmd;
  G4UIcmdWithAnInteger*         UseCUSPMainCmd;
  G4UIcmdWithAnInteger*         UseCUSPMRECmd;
  G4UIcmdWithAnInteger*         UseCUSPMREsupCmd;
  G4UIcmdWithAnInteger*         UseCUSPUHVCmd;
  G4UIcmdWithAnInteger*         UseCUSPCuColCmd;
  G4UIcmdWithAnInteger*         UseCUSPCuStCmd;
  G4UIcmdWithAnInteger*         UseCUSPPolCmd;
  G4UIcmdWithAnInteger*         UseCUSPCuPlateCmd;
  G4UIcmdWithAnInteger*         UseCUSPSUSbandCmd;
  G4UIcmdWithAnInteger*         UseCUSPRadShieldCmd;
  G4UIcmdWithAnInteger*         UseCUSPOVCCmd;
  G4UIcmdWithAnInteger*         UseCUSPMgCoilCmd;
  G4UIcmdWithAnInteger*         UseCUSPMgShieldCmd;
  G4UIcmdWithAnInteger*         UseCUSPTPlsciCmd;
  G4UIcmdWithAnInteger*         sensCuspTPLASCINCmd;
  G4UIcmdWithAnInteger*         UseMLSDUCTCmd;
  G4UIcmdWithAnInteger*         UseSGVDUCTCmd;
  G4UIcmdWithAnInteger*         UseMikiLenseCmd;
  G4UIcmdWithAnInteger*         UseMLDUCTCmd;
  G4UIcmdWithAnInteger*         UseMLELECmd;
  // -- Cusp Main
  G4UIcmdWithAnInteger*		CuspMPartUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMBSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspMTSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspMHSetDimenCmd; 
  // -- MRE
  G4UIcmdWithAnInteger*		CuspMREUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMRESetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspMRESetPlaceCmd; 
  // -- MRE support
  G4UIcmdWithAnInteger*		CuspMREsupUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMREsupSetDimenCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMREsupSetPlaceCmd;
  // -- UHV bore
  G4UIcmdWithAnInteger*		CuspUHVUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspUHVbSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspUHVcnSetDimenCmd; 
  G4UIcmdWithADoubleAndUnit*	CuspUHVcnSetFInDiaCmd; 
  G4UIcmdWithADoubleAndUnit*	CuspUHVcnSetRInDiaCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspUHVSetPlaceCmd; 
  // -- copper collar
  G4UIcmdWith3VectorAndUnit*	CuspCuColSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspCuColSetPlaceCmd; 
  // -- copper strip
  G4UIcmdWithAnInteger*		CuspCuStUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspCuStSetDimenCmd;
  //G4UIcmdWith3VectorAndUnit*	CuspCuStSetPlaceCmd;
  // -- poll
  G4UIcmdWithAnInteger*		CuspPolUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspPolSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspPolSetPlaceCmd; 
  // -- copper plate
  G4UIcmdWithAnInteger*		CuspCuPlateUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspCuPlateSetDimenCmd;
  G4UIcmdWith3VectorAndUnit*	CuspCuPlateSetPlaceCmd;
  // -- SUS band
  G4UIcmdWithAnInteger*		CuspSUSbandUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspSUSbandSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspSUSbandSetPlaceCmd; 
  // -- copper cylinder (radation shield)
  G4UIcmdWith3VectorAndUnit*	CuspRadShieldSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspRadShieldSetPlaceCmd; 
  // -- OVC
  G4UIcmdWithAnInteger*		CuspOVCUseCmd;
  G4UIcmdWithAnInteger*		CuspOVCrUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspOVCrSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspOVCSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspOVCSetPlaceCmd; 
  // -- magnet coil
  G4UIcmdWithAnInteger*		CuspMgCoilUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMgCoilSetDimenCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMgCoilSetPlaceCmd;
  // -- magnetic shield
  G4UIcmdWithAnInteger*		CuspMgShUseCmd;
  G4UIcmdWithAnInteger*		CuspMgShCnUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspMgShSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspMgShCnSetDimenCmd; 
  G4UIcmdWithADoubleAndUnit*	CuspMgShCnSetFInDiaCmd; 
  G4UIcmdWithADoubleAndUnit*	CuspMgShCnSetRInDiaCmd; 
  // -- track detector
  G4UIcmdWithAnInteger*		CuspTPlsciUseCmd;
  G4UIcmdWith3VectorAndUnit*	CuspTPlsciSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	CuspTPlsciSetPlaceCmd; 
  // -- duct between sextupole and GV
  G4UIcmdWith3VectorAndUnit*	SGVD0SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	SGVD1SetDimenCmd; 
  // -- Miki Lense
  G4UIcmdWithAnInteger*		MLDuctPUseCmd;
  G4UIcmdWith3VectorAndUnit*	MLDuctPSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	MLEleSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit*	MLEleSubSetDimenCmd; 
  // -- duct between MLduct and sextupole
  G4UIcmdWithAnInteger*		MLSDUseCmd;
  G4UIcmdWith3VectorAndUnit*	MLSDSetDimenCmd; 

  // Added by Balint -- start
  G4UIcmdWithAString*           CuspElAsciiFileCmd;
  G4UIcmdWithAString*           CuspMagAsciiFileCmd;
  // Added by Balint -- stop

  //    G4UIcmdWithAString*        AbsOneMaterCmd;
  G4UIcmdWithoutParameter*   UpdateCmd;             //!< update



  G4UIcmdWith3VectorAndUnit* CuspBoxSetDimenCmd;      //!< setCuspBoxDimensions
  G4UIcmdWith3VectorAndUnit* CuspT1SetDimenCmd;      //!< setCuspT1Dimensions
  G4UIcmdWith3VectorAndUnit* CuspT2SetDimenCmd;      //!< setCuspT2Dimensions
  G4UIcmdWith3VectorAndUnit* CuspSetPlaceCmd;      //!< setCuspPlacement
  G4UIcmdWithAnInteger*      CBFUseCmd;            //!< setUseCBF
  G4UIcmdWithADoubleAndUnit* CBFSetInRadCmd;          //!< setCBFInRad
  G4UIcmdWithADoubleAndUnit* CBFSetOutRadCmd;         //!< setCBFOutRad
  G4UIcmdWithADoubleAndUnit* CBFSetLengthCmd;         //!< setCBFLength
  G4UIcmdWith3VectorAndUnit* CBFSetPlaceCmd;      //!< setCBFPlace
  G4UIcmdWith3VectorAndUnit* GV1SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* Red0SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* BellSetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* Ch0SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* Ch2SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* GV1SetPlaceCmd;     
  G4UIcmdWith3VectorAndUnit* Red0SetPlaceCmd;     
  G4UIcmdWith3VectorAndUnit* BellSetPlaceCmd;     
  G4UIcmdWith3VectorAndUnit* Ch0SetPlaceCmd;     
  G4UIcmdWith3VectorAndUnit* Ch2SetPlaceCmd;     
  G4UIcmdWithAnInteger*      HDFUseCmd;            //!< setUseHDF
  G4UIcmdWithADoubleAndUnit* HDFSetInRadCmd;          //!< setHDFInRad
  G4UIcmdWithADoubleAndUnit* HDFSetOutRadCmd;         //!< setHDFOutRad
  G4UIcmdWithADoubleAndUnit* HDFSetLengthCmd;         //!< setHDFLength
  G4UIcmdWith3VectorAndUnit* HDFSetPlaceCmd;      //!< setHDFPlace

  G4UIcmdWith3VectorAndUnit* Bm0SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* Bm0SetPlaceCmd; 
  G4UIcmdWith3VectorAndUnit* Bm1SetDimenCmd; 
  G4UIcmdWith3VectorAndUnit* Bm1SetPlaceCmd; 




};


#endif
