#ifndef hbarCavityField_h
#define hbarCavityField_h 1
#include <iostream>
#include <map>
#include <iomanip>
#include <math.h>
#include <algorithm>
#include <cstdlib> 
#include <fstream>
#include "G4ElectroMagneticField.hh"
#include "matrix.hh"
#include <vector>
#include "KDNode.hh"
//#include "hbarEqSextupoleField.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include <tr1/memory>
#include "G4FieldManager.hh"

class G4FieldManager;
class G4MagIntegratorStepper;
class G4ChordFinder;
class G4PropagatorInField;

#define neighboursnumber 1 //!< number of nearest neighbours to use for linear interpolation
#define DIM 3      		   //!< space dimensions
#define columns 6  		   //!< number of columns e.g. 6 for x,y,z,Bx,By,Bz

//! This class determinates the magnetic field at a certain point inside the cavity from a fieldmap

/*!Determination of the nearest neighbour:
After the k-dimensional Tree is built, the Nearest Neighbour is then determined 
in the following way:                      
First, an approximate neighbour is being found. This is done by the function
Approx_Neighbour(KDNode*, double*). It simply runs through the tree, starting at
the top and comparing the requested point (actually: comparing the 'Axis' component
of the point) to the points stored in the nodes and depending on whether it is
larger or smaller, choosing the 'branch' of the right or of the left child.
If the bottom of the tree is reached (leaves), then this leafnode will be returned.

But a closer point could still exist (e.g. the gridpoints are not equidistant):
The distance 'dist' between the Point stored in the node determined by Approx_Neighbour
and the requested point is calculated. Since each node of the tree splits the space
into halfs according to Axis, we need to check whether this splitting planes
intersect with a sphere with radius 'dist' around the approximate point.
This is done by Check_Sphere().  
*/

//OXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX//
class hbarCavityField : public G4ElectroMagneticField {

	public:
		//! Constructor
		hbarCavityField(); 
		~hbarCavityField();
		//! data is read into matrix, numberoflines is calculated, SearchTree and NearestN are initialized
		void Initialize(const char*);
		//! sets the field to interpolated value
		void GetFieldValue(const G4double*,G4double*) const;
		
		//! Calculate weighted mean value (linear interpolation)		
		double* Find_Field(double*,double[][columns],int) const;
		double Get_meanB_val() {return meanB_val;};
		
		//! method of G4ElectroMagneticfield that needs to be overloaded
		G4bool DoesFieldChangeEnergy() const { return true; };

		std::tr1::shared_ptr<KDNode> GetSearchTree();	//!< returns pointer to root node of 'SearchTree'
		std::tr1::shared_ptr<KDNode> GetNearestN();		//!< returns pointer to 'NearestN'	
		int Getnumberoflines();							//!< returns 'numberoflines'
		
		G4FieldManager* fieldMgr; 
		//hbarEqSextupoleField* fEquation; 

	private:	
		G4int fStepperType;
		G4double fMinStep;
		G4double fMinMiss;	
		G4String allowMajorana; 
		G4String useAngleDiff; 
		double meanB_val;
		
		void ReadFileToMatrix(double**, const char*); //!< method which reads the data points into a matrix
		double Distance(G4double*, G4double*) const; //!< calculates distance between two points in DIM dimensions
		std::tr1::shared_ptr<KDNode> Approx_Neighbour(std::tr1::shared_ptr<KDNode>,double*) const; //!< calculates the approximate nearest neighbour
		std::tr1::shared_ptr<KDNode> Check_Sphere(std::tr1::shared_ptr<KDNode>, std::tr1::shared_ptr<KDNode>, double*) const; //!< checks sphere arround approx. neighbour
		std::tr1::shared_ptr<KDNode> Nearest_Neighbour(std::tr1::shared_ptr<KDNode>,double*) const; //!< determines nearest neighbour out of all nodes with checked=false

		int numberoflines; //!< Number of lines of the data file
		double** MagField; //!< 2 dim. matrix which stores x, y, z, Bx, By, Bz
		std::tr1::shared_ptr<KDNode> SearchTree; //!< KD-Tree
		std::tr1::shared_ptr<KDNode> NearestN; //!< Pointer to Node with Nearest Neighbour
};
//OXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX//
#endif
