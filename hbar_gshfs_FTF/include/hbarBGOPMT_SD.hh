
#ifndef hbarBGOPMT_SD_h
#define hbarBGOPMT_SD_h 1

#include <algorithm> 
// USER //
//#include "SimpleHit.hh"
#include "hbarDetectorHit.hh"
#include "TH1.h"

// GEANT4 //
#include "G4VSensitiveDetector.hh"
#include "G4Track.hh"
#include "G4SystemOfUnits.hh"
#include "G4Step.hh"
#include "G4SteppingManager.hh"
#include "Randomize.hh"

#include "G4HCofThisEvent.hh"

#include "G4HCtable.hh"
#include "G4SDManager.hh"

#include "TCanvas.h"
#include "TError.h"
#include "TGraph.h"
#include "G4Run.hh"
#include "G4RunManager.hh"

#include "hbarCPTConst.hh"
#include "G4OpBoundaryProcess.hh"

// ROOT //
#include "TFile.h"
//#include "G4THitsCollection.hh"

class G4HCofThisEvent;
class hbarCPTConst;
//class hbarDetConst;
class hbarRunAction;



// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0

class hbarBGOPMT_SD : public G4VSensitiveDetector
{
public:
  hbarBGOPMT_SD(G4String SDname, hbarCPTConst* cptconst); //!< constructor
  ~hbarBGOPMT_SD(); //!< destructor
  G4bool ProcessHits(G4Step* , G4TouchableHistory * ); //!<
  G4bool ProcessHits_constStep(const G4Step*, G4TouchableHistory*);
  void Initialize(G4HCofThisEvent* ); //!<
  void EndOfEvent(G4HCofThisEvent* ); //!<
  //// Getter:
  int GetNumberofPhotons() {return hitCounter;};
  std::vector<G4double> GetPMT_energyData()  {return PMT_energyData;};
  //std::vector<std::vector<double> > GetEdep_vector() {return Edep_vector;};
  Double_t* GetTotalEnergy()  {return &TotalEnergy;};
  void SetTotalEnergy(double val) {TotalEnergy = val;};
  int ADCNumber;
  std::vector<double> Get_PMT_energyData() {return PMT_energyData;};
/*
  std::vector<double> Get_PMT0_energyData() {return PMT0_energyData;};
  std::vector<double> Get_PMT1_energyData() {return PMT1_energyData;};
  std::vector<double> Get_PMT2_energyData() {return PMT2_energyData;};
  std::vector<double> Get_PMT3_energyData() {return PMT3_energyData;};
*/
  G4String GetBGOPMTName() {return BGOPMTName;};
  //// Setter:
 // void Set_drawhistos(G4bool useflag) {drawhistos = useflag;};
  void Set_write_to_dat_files(G4bool useflag) {write_to_dat_files = useflag;};

private:
  hbarDetectorHitsCollection* hitCollection; //!< currently unused hitcollection object
  G4Track * track; //!< current track
  G4int hitCounter; //!< counts photon hits in pmt
  G4String volName; //!< name of volume
  G4String BGOPMTName; //!< name of pmt
  
  std::vector<G4double> PMT_energyData; //!< save energies of photons
  /*std::vector<G4double> PMT0_energyData;
  std::vector<G4double> PMT1_energyData;
  std::vector<G4double> PMT2_energyData;
  std::vector<G4double> PMT3_energyData;*/
 // std::vector<G4double> PMT_chargeData; //!< save number of generated electrons
  //std::vector<G4double> PMT_timeData; //!< save arrival times of photons
  double TotalCharge; //!< Total Charge in PMT... Sum of q.e. * gain for all photons
  Double_t TotalEnergy;
  //G4bool drawhistos;
  G4bool write_to_dat_files;
  
  //std::vector<double> response_time; //!< x values of response function
  //std::vector<double> response_sig; //!< y values of response function
  //double mult_fft_rl[4096];
  //double mult_fft_img[4096];
  //double HodorWaveform[1024];
  //TH1D* histo_resp;  //!< histogram of response function
  //TGraph* HodorWaveformgr;
	
  hbarCPTConst* localcptconst;

  G4OpBoundaryProcessStatus fExpectedNextStatus;



  double CalculateQE_PMT(double wl); //!< pass wavelength of photon to function; calculates wether photon gets detected (true) or not (false)
};


#endif

