#ifndef hbarSextupoleConst_hh
#define hbarSextupoleConst_hh 1

#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"
#include "G4ChordFinder.hh"

#include "hbarSextupoleMess.hh"

class hbarSextupoleMess;

class hbarSextupoleConst 
{
public:
  hbarSextupoleConst(hbarFieldSetup * fieldToUse);
  ~hbarSextupoleConst();
  void MakeSextupole(G4VPhysicalVolume *, G4UserLimits *);

  void SetSextupoleStart(G4int, G4double);
  void SetSextupoleStart(G4double val) {SextupoleStart[Sextpt] = val;};
  void SetSextupoleEnd(G4int, G4double);
  void SetSextupoleEnd(G4double val) {SextupoleEnd[Sextpt] = val;};
  
  void SetSextupoleOffsetX(G4int, G4double);
  void SetSextupoleOffsetX(G4double val) {SextupoleOffsetX[Sextpt] = val;};
  void SetSextupoleOffsetY(G4int, G4double);
  void SetSextupoleOffsetY(G4double val) {SextupoleOffsetY[Sextpt] = val;};
  void SetSextupoleRotationX(G4double val) {SextupoleRotationX[Sextpt] = val;};
  void SetSextupoleRotationX(G4int num, G4double val);
  void SetSextupoleRotationY(G4double val) {SextupoleRotationY[Sextpt] = val;};
  void SetSextupoleRotationY(G4int num, G4double val);
  void SetSextupoleRotateMagnet(G4bool val) {SextupoleRotateMagnet[Sextpt] = val;};
  
  void SetSextupoleFrontInnerDiameter(G4int num, G4double val);
  void SetSextupoleRearInnerDiameter(G4int num, G4double val);
  void SetSextupoleFrontOuterDiameter(G4int num, G4double val);
  void SetSextupoleRearOuterDiameter(G4int num, G4double val);
  void SetSextupoleInnerDiameter(G4int num, G4double val) {
    SetSextupoleFrontInnerDiameter(num, val);
    SetSextupoleRearInnerDiameter(num, val);
  };
  void SetSextupoleOuterDiameter(G4int num, G4double val) {
    SetSextupoleFrontOuterDiameter(num, val);
    SetSextupoleRearOuterDiameter(num, val);
  };
  
  void SetSextupoleFrontInnerDiameter(G4double val) {SextupoleFrontInnerDiam[Sextpt] = val;};
  void SetSextupoleRearInnerDiameter(G4double val)  {SextupoleRearInnerDiam[Sextpt] = val;};
  void SetSextupoleFrontOuterDiameter(G4double val) {SextupoleFrontOuterDiam[Sextpt] = val;};
  void SetSextupoleRearOuterDiameter(G4double val)  {SextupoleRearOuterDiam[Sextpt] = val;};
  void SetSextupoleInnerDiameter(G4double val) {
    SetSextupoleFrontInnerDiameter(val);
    SetSextupoleRearInnerDiameter(val);
  };
  void SetSextupoleOuterDiameter(G4double val) {
    SetSextupoleFrontOuterDiameter(val);
    SetSextupoleRearOuterDiameter(val);
  };

  void SetUseSextupoleParts(G4bool useflag){useSextupoleParts[Sextpt] = useflag;};

  void AddSextupole();
  void UseSextupole(G4int);

  G4double GetSextupoleLength(G4int var) {return SextupoleEnd[var-1] - SextupoleStart[var-1];};

  G4double GetSextupoleFrontInnerDiameter(G4int var) {return SextupoleFrontInnerDiam[var-1];};
  G4double GetSextupoleRearInnerDiameter(G4int var)  {return SextupoleRearInnerDiam[var-1];};
  G4double GetSextupoleFrontOuterDiameter(G4int var) {return SextupoleFrontOuterDiam[var-1];};
  G4double GetSextupoleRearOuterDiameter(G4int var)  {return SextupoleRearOuterDiam[var-1];};

  G4double GetSextupoleStart(G4int var) {return SextupoleStart[var-1];};
  G4double GetSextupoleEnd(G4int var)   {return SextupoleEnd[var-1];};
  G4double GetSextupoleOffsetX(G4int var) {return SextupoleOffsetX[var-1];};
  G4double GetSextupoleOffsetY(G4int var) {return SextupoleOffsetY[var-1];};
  G4LogicalVolume* GetSextupoleLogicalVolume(G4int var){return SextLog[var-1];};

  G4double GetSextupoleRotationX(G4int var) {return SextupoleRotationX[var-1];};
  G4double GetSextupoleRotationY(G4int var) {return SextupoleRotationY[var-1];};

  G4bool   IsUseSextupole(G4int num) {return useSext[num-1];};

  void UseSxPart(G4int num);
  void AddSxPart();
  void SetSxPartDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetSxPartDimension(G4ThreeVector vec){SetSxPartDimension(SxPartpt,vec.x(),vec.y(),vec.z());};



private:

  G4Cons*            SextSol[maxSN];
  G4LogicalVolume*   SextLog[maxSN];
  G4VPhysicalVolume* SextPhy[maxSN];


  
  G4bool       useSext[maxSN];          //!< whether the sextupole is in use
  G4bool       useSextupoleParts[maxSN];//!< use the big sextupole geometry
  G4int        Sextpt;                  //!< number of current sextupole

  G4double     SextupoleFrontOuterDiam[maxSN];
  G4double     SextupoleRearOuterDiam[maxSN];
  G4double     SextupoleFrontInnerDiam[maxSN];
  G4double     SextupoleRearInnerDiam[maxSN];
  G4double     SextupoleStart[maxSN];
  G4double     SextupoleEnd[maxSN];
  G4double     SextupoleOffsetX[maxSN];
  G4double     SextupoleOffsetY[maxSN];
  G4double     SextupoleRotationX[maxSN];
  G4double     SextupoleRotationY[maxSN];
  G4bool       SextupoleRotateMagnet[maxSN];

  G4int              SxPartpt;
  G4bool             useSxPart[maxSxPart]; 
  G4double           SxPartInRad[maxSxPart],SxPartOutRad[maxSxPart],SxPartHalfLen[maxSxPart];
  G4VSolid*          SxPart[maxSxPart]; 
  G4LogicalVolume*   SxPartLog[3]; 
  G4VPhysicalVolume* SxPartPhy[3]; 


  hbarFieldSetup * magFieldSetup;
  G4bool allLocal;
  G4bool useNagataDetector;

  hbarSextupoleMess * SextupoleMess;

};



#endif
