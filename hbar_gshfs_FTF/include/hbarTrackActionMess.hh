#ifndef hbarTrackActionMessenger_h
#define hbarTrackActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarTrackAction;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;

//! This is the Track Action messenger class
/*!
  This messenger class defines only one user command, with which the user 
  can select which particle trajectories to store: none, primary, pbar, hbar,
  etc. The more trajectories are stored, the slower and more memory-consuming 
  the simulation is. The name of the command (/tracking/storeTracks) is a bit
  misleading, because not the tracks but the trajectories are stored, but 
  GEANT4 already has a built-in /tracking/storeTrajectory command, so I needed 
  another command name.

  To select which particle tracks to store in the simulation
  - none: no particle tracks are stored
  - primary: only the primary particle track is stored
  - pbar: only antiproton tracks are stored
  - hbar: only antihydrogen tracks are stored
  - heavy: only heavy particle tracks are stored, i.e. electron and photon tracks are deleted
  - all: all particle tracks are stored, except for neutrino tracks
  - allwithneutrino: all particle tracks are stored

  The following macros are defined in this class:
  - /tracking/storeTracks
 */
class hbarTrackActionMess: public G4UImessenger
{
public:
  hbarTrackActionMess(hbarTrackAction*);
  ~hbarTrackActionMess();
    
  void SetNewValue(G4UIcommand*, G4String);
    
private:
  hbarTrackAction*       trackAction; //!< pointer to hbarTrackAction  
  G4UIcmdWithAString*    StoreCmd;    //!< storeTracks
};

#endif
