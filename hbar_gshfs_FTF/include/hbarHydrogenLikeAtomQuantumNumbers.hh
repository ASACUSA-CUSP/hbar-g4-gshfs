#ifndef hbarHydrogenLikeAtomQuantumNumbers_h
#define hbarHydrogenLikeAtomQuantumNumbers_h 1

#include "globals.hh"
#include "G4ios.hh"
#include <math.h>

//! Contains a subset of quantum numbers (and some rules for them) for hydrogen like atoms such as hydrogen and antihydrogen.
/*! Basically a container class, but it's also doing some checks to assure that the values entered are sound.
  NOTE: This class only contains the nuclear quantum numbers M and F. The other ones are treated differently by import from rFAC.
  
 */
class hbarHydrogenLikeAtomQuantumNumbers
{
protected:

  //! Current quantum numbers.
  short F;
  short M;
    
public:
  //! Get quantum number F. Some nuclear spin stuff only used in the ground state anyway.
  G4int GetF(){ return F;}

  G4int GetM(){ return M; }

  //! Set the quantum number F. Call refresh() to put it into effect.
  void SetF(G4int val/*!The new value. */);

  void SetM(G4int val); ///Setter.

  hbarHydrogenLikeAtomQuantumNumbers(G4int _F = 0, //! Quantum number F.
									 G4int _M = 1 //! Quantum number M.
									 ); ///Constructor, can also work as a default such.

  hbarHydrogenLikeAtomQuantumNumbers(const hbarHydrogenLikeAtomQuantumNumbers& toCopy ///Object to copy.
				     ); ///Copy constructor.
  
 };


#endif
