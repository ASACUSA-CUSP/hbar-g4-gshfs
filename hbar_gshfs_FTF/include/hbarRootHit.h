#ifndef hbarRootHit_h
#define hbarRootHit_h 1

//! This structure stores the values that were written to the TTree
typedef struct {

  Int_t    Event;        //!< Eventnumber 
  Int_t    Hit;          //!< 1 if there is a H or Hbar hit in the dummy detector 
  Double_t Edep;         //!< energy deposit
  Double_t Ekin;         //!< kinetic energy of incident particle
  Double_t Time;         //!< particle time
  Double_t PosX;         //!< Hit position on X axis
  Double_t PosY;         //!< Hit position on Y axis
  Double_t PosZ;         //!< Hit position on Z axis
  Double_t MomX;         //!< Particle momentum in X direction
  Double_t MomY;         //!< Particle momentum in Y direction
  Double_t MomZ;         //!< Particle momentum in Z direction
  Char_t   Particle[20]; //!< Particle name
  Int_t    PDGcode;
  Char_t   Detector[20]; //!< Detector name


  //tajima
  Int_t    track_id;
  Int_t    parent_id;


} hbarRootHit;

//! This structure stores the Hits for the "real" root hits TTree "T"
typedef struct {
  Double_t HbarDet_Q[128];        //!< Charge of HbarDet element
  Double_t HbarDet_T[128];        //!< Timing of HbarDet element
  Double_t Hodoscope_Q1[20][300]; //!< Charge of Hodoscopes
  Double_t Hodoscope_T1[20][300]; //!< Timing of Hodoscopes
  Double_t Hodoscope_Q2[20][300]; //!< Charge of Hodoscopes
  Double_t Hodoscope_T2[20][300]; //!< Timing of Hodoscopes
  Double_t Veto_Q[20][300];       //!< Charge of Veto
  Double_t Veto_T[20][300];       //!< Timing of Veto
  Double_t BGO_Edep;
  Int_t    isHit;                 //!< 1 if there was an Hbar at the dummy detector
} hbarRealRootHit;

#endif
