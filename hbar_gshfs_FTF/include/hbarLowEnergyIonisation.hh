#include "G4hIonisation.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "Randomize.hh"
#include "G4VContinuousDiscreteProcess.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4Proton.hh"
#include "G4AntiProton.hh"
#include "G4Electron.hh"
#include "G4VParticleChange.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4PhysicsLogVector.hh"
#include "G4PhysicsLinearVector.hh"
#include "G4PhysicalConstants.hh"


#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"


#ifndef hbarLowEnergyIonisation_h
#define hbarLowEnergyIonisation_h 1

class hbarLowEnergyIonisation : public G4hIonisation {
	private:
		//void InitializeMe();
	public:
		hbarLowEnergyIonisation(const G4String&);
		~hbarLowEnergyIonisation();          
		G4bool IsApplicable(const G4ParticleDefinition&);
		
		G4VParticleChange* AlongStepDoIt(const G4Track& trackData,
								  const G4Step& stepData);
	/*	G4double AlongStepGetPhysicalInteractionLength(
                             const G4Track&,G4double,G4double,G4double&,
                             G4GPILSelection*);*/
};

#endif
