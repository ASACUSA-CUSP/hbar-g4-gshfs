#ifndef hbarAntiHydrogenDecayAtRest_h
#define hbarAntiHydrogenDecayAtRest_h 1
 
#include "globals.hh"
#include "Randomize.hh" 
#include "G4VRestProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleDefinition.hh"
#include "G4GHEKinematicsVector.hh"
#include "hbarHydrogenLikeDecayAtRest.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"



//! Process of atomic hydrogen recombination to H2 molecules
class hbarAntiHydrogenDecayAtRest : public hbarHydrogenLikeDecayAtRest
 
{ 
private:
  // hide assignment operator as private 
  hbarAntiHydrogenDecayAtRest& operator=(const hbarAntiHydrogenDecayAtRest &right);

  //! copy constructor
  hbarAntiHydrogenDecayAtRest(const hbarAntiHydrogenDecayAtRest& );

protected:
  //! Name of particle.
  virtual G4String GetThisProcessParticleName() {return "antihydrogen";}
  //! Number of secondary particles created.
  virtual G4int GetInitialNumberOfSecondaries(){ return 2; /*Comment by Rikard: No idea why this is set...? */}

  //! For antihydrogen we need to do some extra stuff. 
  virtual void AtRestDoItAdditional(const G4Track& track);
public:
   //! Constructor, initializes all variables. Currently does nothing.
  hbarAntiHydrogenDecayAtRest();
   //! Destructor frees memory after particle is killed. Currently does nothing.
  ~hbarAntiHydrogenDecayAtRest();
};
#endif
