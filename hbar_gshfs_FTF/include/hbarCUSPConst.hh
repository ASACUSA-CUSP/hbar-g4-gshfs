#ifndef hbarCUSPConst_hh
#define hbarCUSPConst_hh 1

#include "hbarDefinitions.hh"

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4RotationMatrix.hh"
#include "G4TwoVector.hh"
#include "G4UserLimits.hh"

#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh" // Added by Clemens
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarCavityField.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"
#include "G4ClassicalRK4.hh"
#include "G4ChordFinder.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "hbarException.hh"

#include "hbarCUSPMess.hh"

// What about a little documentation ? /// Kind regards, Rikard
#define maxCuspMPart 3
#define maxCuspMain 3
#define maxCuspMRE 31
#define maxCuspMRESeg 6
#define maxCuspMREsup 12
#define maxCuspUHV 5
#define maxCuspUHVb 4  //maxCuspUHV-1
#define maxCuspCuSt 24
#define maxCuspPol 16  
#define maxCuspCuPlate 37
#define maxCuspSUSband 4
#define maxCuspOVCr 10
#define maxCuspOVC 17
#define maxCuspMgCoil 5
#define maxCuspMgSh 3
#define maxCuspMgShCn 4
#define maxCuspMgShield 2
#define maxCuspTPlsci 2

class hbarCUSPMess;

/**This class is used to construct the CUSP detector.
   This should hopefully make the code cleaner(!).
   Some stuff is just copy-pasted from the big class I split this from, 
   don't blame me for lack of documentation here.
   //Rikard.
 */


class hbarCUSPConst 
{
public:
  hbarCUSPConst(hbarFieldSetup *);
  ~hbarCUSPConst();

  void MakeCUSP(G4VPhysicalVolume *, hbarDetectorSD*);

  void SetUseCUSPprev(G4bool useflag)      {useCUSPprev = useflag;};
  void SetUseCUSPMain(G4bool useflag)      {useCUSPMain = useflag;};
  void SetUseCUSPMRE(G4bool useflag)       {useCUSPMRE = useflag;};
  void SetUseCUSPMREsup(G4bool useflag)    {useCUSPMREsup = useflag;};
  void SetUseCUSPUHV(G4bool useflag)       {useCUSPUHV = useflag;};
  void SetUseCUSPCuCol(G4bool useflag)     {useCUSPCuCol = useflag;};
  void SetUseCUSPCuSt(G4bool useflag)      {useCUSPCuSt = useflag;};
  void SetUseCUSPPol(G4bool useflag)       {useCUSPPol = useflag;};
  void SetUseCUSPCuPlate(G4bool useflag)   {useCUSPCuPlate = useflag;};
  void SetUseCUSPSUSband(G4bool useflag)   {useCUSPSUSband = useflag;};
  void SetUseCUSPRadShield(G4bool useflag) {useCUSPRadShield = useflag;};
  void SetUseCUSPOVC(G4bool useflag)       {useCUSPOVC = useflag;};
  void SetUseCUSPMgCoil(G4bool useflag)    {useCUSPMgCoil = useflag;};
  void SetUseCUSPMgShield(G4bool useflag)  {useCUSPMgShield = useflag;};
  void SetUseCUSPTPlsci(G4bool useflag)    {useCUSPTPlsci = useflag;};
  void SetsensCuspTPLASCIN(G4int num)      {sensCuspTPLASCIN[num] = 1;};

  void SetUseSGVDUCT(G4bool useflag)       {useSGVDUCT = useflag;};
  void SetUseMLSDUCT(G4bool useflag)       {useMLSDUCT = useflag;};
  void SetUseMikiLense(G4bool useflag)     {useMikiLense = useflag;};
  void SetUseMLDUCT(G4bool useflag)        {useMLDUCT = useflag;};
  void SetUseMLELE(G4bool useflag)         {useMLELE = useflag;};


  void SetCuspElAsciiFile(G4String Elfilename){AsciiElfilename = Elfilename;};
  void SetCuspMagAsciiFile(G4String Magfilename){AsciiMagfilename = Magfilename;};
  // Added by Balint -- stop
  


  // -- Cusp Main
  void UseCuspMPart(G4int num);  void AddCuspMPart();
  void SetCuspMBDimension(G4int num,G4double xlen, G4double ylen, G4double zlen);
  void SetCuspMBDimension(G4ThreeVector vec){SetCuspMBDimension(CuspMPartpt,vec.x(),vec.y(),vec.z());};
  void SetCuspMTDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspMTDimension(G4ThreeVector vec){SetCuspMTDimension(CuspMPartpt,vec.x(),vec.y(),vec.z());};
  void SetCuspMHDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspMHDimension(G4ThreeVector vec){SetCuspMHDimension(CuspMPartpt,vec.x(),vec.y(),vec.z());};
  // -- MRE
  void UseCuspMRE(G4int num);
  void AddCuspMRE();
  void SetCuspMREDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspMREDimension(G4ThreeVector vec){SetCuspMREDimension(CuspMREpt,vec.x(),vec.y(),vec.z());};
  void SetCuspMREPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspMREPlacement(G4ThreeVector vec){SetCuspMREPlacement(CuspMREpt,vec.x(),vec.y(),vec.z());};
  //--  MRE support
  void UseCuspMREsup(G4int num);
  void AddCuspMREsup();
  void SetCuspMREsupDimension(G4int num,G4double xlen, G4double ylen, G4double zlen);
  void SetCuspMREsupDimension(G4ThreeVector vec){SetCuspMREsupDimension(CuspMREsuppt,vec.x(),vec.y(),vec.z());};
  void SetCuspMREsupPlacement(G4int num,G4double x, G4double y, G4double z);
  void SetCuspMREsupPlacement(G4ThreeVector vec){SetCuspMREsupPlacement(CuspMREsuppt,vec.x(),vec.y(),vec.z());};
  // --  UHV bore
  void UseCuspUHV(G4int num);
  void AddCuspUHV();
  void SetCuspUHVbDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspUHVbDimension(G4ThreeVector vec){SetCuspUHVbDimension(CuspUHVpt,vec.x(),vec.y(),vec.z());};
  void SetCuspUHVcnDimension(G4double foud, G4double roud, G4double len);
  void SetCuspUHVcnDimension(G4ThreeVector vec){SetCuspUHVcnDimension(vec.x(),vec.y(),vec.z());};
  void SetCuspUHVcnFrontInnerRadius(G4double find);
  void SetCuspUHVcnRearInnerRadius(G4double rind);
  void SetCuspUHVPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspUHVPlacement(G4ThreeVector vec){SetCuspUHVPlacement(CuspUHVpt,vec.x(),vec.y(),vec.z());};
  // -- copper collar
  void SetCuspCuColDimension(G4double ind, G4double oud, G4double len);
  void SetCuspCuColDimension(G4ThreeVector vec){SetCuspCuColDimension(vec.x(),vec.y(),vec.z());};
  void SetCuspCuColPlacement(G4double x, G4double y, G4double z);
  void SetCuspCuColPlacement(G4ThreeVector vec){SetCuspCuColPlacement(vec.x(),vec.y(),vec.z());};
  // -- copper strip
  void UseCuspCuSt(G4int num);
  void AddCuspCuSt();
  void SetCuspCuStDimension(G4int num,G4double xlen, G4double ylen, G4double zlen);
  void SetCuspCuStDimension(G4ThreeVector vec){SetCuspCuStDimension(CuspCuStpt,vec.x(),vec.y(),vec.z());};
  void SetCuspCuStPlacement(G4int num,G4double x, G4double y, G4double z);
  void SetCuspCuStPlacement(G4ThreeVector vec){SetCuspCuStPlacement(CuspCuStpt,vec.x(),vec.y(),vec.z());};
  // -- poll
  void UseCuspPol(G4int num);
  void AddCuspPol();
  void SetCuspPolDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspPolDimension(G4ThreeVector vec){SetCuspPolDimension(CuspPolpt,vec.x(),vec.y(),vec.z());};
  void SetCuspPolPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspPolPlacement(G4ThreeVector vec){SetCuspPolPlacement(CuspPolpt,vec.x(),vec.y(),vec.z());};
  // -- copper plate 
  void UseCuspCuPlate(G4int num);
  void AddCuspCuPlate();
  void SetCuspCuPlateDimension(G4int num, G4double xlen, G4double ylen, G4double zlen);
  void SetCuspCuPlateDimension(G4ThreeVector vec){SetCuspCuPlateDimension(CuspCuPlatept,vec.x(),vec.y(),vec.z());};
  void SetCuspCuPlatePlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspCuPlatePlacement(G4ThreeVector vec){SetCuspCuPlatePlacement(CuspCuPlatept,vec.x(),vec.y(),vec.z());};
  // -- SUS band
  void UseCuspSUSband(G4int num);
  void AddCuspSUSband();
  void SetCuspSUSbandDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspSUSbandDimension(G4ThreeVector vec){SetCuspSUSbandDimension(CuspSUSbandpt,vec.x(),vec.y(),vec.z());};
  void SetCuspSUSbandPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspSUSbandPlacement(G4ThreeVector vec){SetCuspSUSbandPlacement(CuspSUSbandpt,vec.x(),vec.y(),vec.z());};
  // -- copper cylinder (radation shield)
  void SetCuspRadShieldDimension(G4double ind, G4double oud, G4double len);
  void SetCuspRadShieldDimension(G4ThreeVector vec) {SetCuspRadShieldDimension(vec.x(),vec.y(),vec.z());};
  void SetCuspRadShieldPlacement(G4double x, G4double y, G4double z);
  void SetCuspRadShieldPlacement(G4ThreeVector vec) {SetCuspRadShieldPlacement(vec.x(),vec.y(),vec.z());};
  // -- OVC
  void UseCuspOVC(G4int num);   
  void AddCuspOVC();
  void UseCuspOVCr(G4int num);   
  void AddCuspOVCr();
  void SetCuspOVCDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspOVCDimension(G4ThreeVector vec){SetCuspOVCDimension(CuspOVCpt,vec.x(),vec.y(),vec.z());};
  void SetCuspOVCrDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspOVCrDimension(G4ThreeVector vec){SetCuspOVCrDimension(CuspOVCrpt,vec.x(),vec.y(),vec.z());};
  void SetCuspOVCPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspOVCPlacement(G4ThreeVector vec){SetCuspOVCPlacement(CuspOVCpt,vec.x(),vec.y(),vec.z());};
  // -- magnet coil
  void UseCuspMgCoil(G4int num);
  void AddCuspMgCoil();
  void SetCuspMgCoilDimension(G4int num, G4double ind, G4double oud, G4double wd);
  void SetCuspMgCoilDimension(G4ThreeVector vec){SetCuspMgCoilDimension(CuspMgCoilpt,vec.x(),vec.y(),vec.z());};
  void SetCuspMgCoilPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspMgCoilPlacement(G4ThreeVector vec){SetCuspMgCoilPlacement(CuspMgCoilpt,vec.x(),vec.y(),vec.z());};
  // -- magnetic shield
  void UseCuspMgSh(G4int num);
  void AddCuspMgSh();
  void SetCuspMgShDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetCuspMgShDimension(G4ThreeVector vec){SetCuspMgShDimension(CuspMgShpt,vec.x(),vec.y(),vec.z());};
  void UseCuspMgShCn(G4int num);
  void AddCuspMgShCn();
  void SetCuspMgShCnDimension(G4int num,G4double foud, G4double roud, G4double len);
  void SetCuspMgShCnDimension(G4ThreeVector vec){SetCuspMgShCnDimension(CuspMgShCnpt,vec.x(),vec.y(),vec.z());};
  void SetCuspMgShCnFrontInnerRadius(G4int,G4double);
  void SetCuspMgShCnFrontInnerRadius(G4double find){SetCuspMgShCnFrontInnerRadius(CuspMgShCnpt, find);};
  void SetCuspMgShCnRearInnerRadius(G4int,G4double);
  void SetCuspMgShCnRearInnerRadius(G4double rind){SetCuspMgShCnRearInnerRadius(CuspMgShCnpt, rind);};
  // -- cusp track detector
  void UseCuspTPlsci(G4int num);
  void AddCuspTPlsci();
  void SetCuspTPlsciDimension(G4int num, G4double xlen, G4double ylen, G4double zlen);
  void SetCuspTPlsciDimension(G4ThreeVector vec){SetCuspTPlsciDimension(CuspTPlscipt,vec.x(),vec.y(),vec.z());};
  void SetCuspTPlsciPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCuspTPlsciPlacement(G4ThreeVector vec){SetCuspTPlsciPlacement(CuspTPlscipt,vec.x(),vec.y(),vec.z());};

  
  void SetBeamline0Dimension(G4double inr, G4double our, G4double hi);
  void SetBeamline0Dimension(G4ThreeVector vec){SetBeamline0Dimension(vec.x(),
								      vec.y(),
								      vec.z());};
  void SetBeamline0Placement(G4double x, G4double y, G4double z);
  void SetBeamline0Placement(G4ThreeVector vec){SetBeamline0Placement(vec.x(),
								      vec.y(),
								      vec.z());};
  void SetBeamline1Dimension(G4double inr, G4double our, G4double hi);
  void SetBeamline1Dimension(G4ThreeVector vec){SetBeamline1Dimension(vec.x(),
								      vec.y(),
								      vec.z());};
  void SetBeamline1Placement(G4double x, G4double y, G4double z);
  void SetBeamline1Placement(G4ThreeVector vec){SetBeamline1Placement(vec.x(),
								      vec.y(),
								      vec.z());};

  void SetCuspBoxDimension(G4double wi, G4double thi, G4double le);
  void SetCuspBoxDimension(G4ThreeVector vec){SetCuspBoxDimension(vec.x(),
								  vec.y(),
								  vec.z());};
 
  void SetCuspT1Dimension(G4double inr, G4double our, G4double hi);
  void SetCuspT1Dimension(G4ThreeVector vec){SetCuspT1Dimension(vec.x(),
								  vec.y(),
								  vec.z());};
  
  void SetCuspT2Dimension(G4double inr, G4double our, G4double hi);
  void SetCuspT2Dimension(G4ThreeVector vec){SetCuspT2Dimension(vec.x(),
								vec.y(),
								vec.z());};
  
  void SetCuspPlacement(G4double x, G4double y, G4double z);
  void SetCuspPlacement(G4ThreeVector vec){SetCuspPlacement(vec.x(),
							    vec.y(),
							    vec.z());};
  
  void SetCBFInRad(G4int, G4double);
  void SetCBFInRad(G4double val) {SetCBFInRad(CBFpt, val);};
  
  void SetCBFOutRad(G4int, G4double);
  void SetCBFOutRad(G4double val) {SetCBFOutRad(CBFpt, val);};
  
  void SetCBFLength(G4int, G4double);
  void SetCBFLength(G4double val) {SetCBFLength(CBFpt, val);};
  
  void SetCBFPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetCBFPlacement(G4ThreeVector vec){SetCBFPlacement(CBFpt, vec.x(),
							  vec.y(),
							  vec.z());};
  void AddCBF();
  void UseCBF(G4int);  
 
  void SetGV1Dimension(G4double inr, G4double our, G4double hi);
  void SetGV1Dimension(G4ThreeVector vec){SetGV1Dimension(vec.x(),
								vec.y(),
								vec.z());};

  void SetRed0Dimension(G4double inr, G4double our, G4double hi);
  void SetRed0Dimension(G4ThreeVector vec){SetRed0Dimension(vec.x(),
							    vec.y(),
							    vec.z());};
  
  void SetBellDimension(G4double inr, G4double our, G4double hi);
  void SetBellDimension(G4ThreeVector vec){SetBellDimension(vec.x(),
								vec.y(),
								vec.z());};
  
  void SetHDFInRad(G4int, G4double);
  void SetHDFInRad(G4double val) {SetHDFInRad(HDFpt, val);};
  
  void SetHDFOutRad(G4int, G4double);
  void SetHDFOutRad(G4double val) {SetHDFOutRad(HDFpt, val);};
  
  void SetHDFLength(G4int, G4double);
  void SetHDFLength(G4double val) {SetHDFLength(HDFpt, val);};
  
  void SetHDFPlacement(G4int num, G4double x, G4double y, G4double z);
  void SetHDFPlacement(G4ThreeVector vec){SetHDFPlacement(HDFpt, vec.x(),
  							  vec.y(),
  							  vec.z());};
  void AddHDF();
  void UseHDF(G4int);  

  void SetCh0Dimension(G4double inr, G4double our, G4double hi);
  void SetCh0Dimension(G4ThreeVector vec){SetCh0Dimension(vec.x(),
								vec.y(),
								vec.z());};
  
  void SetCh2Dimension(G4double inr, G4double our, G4double hi);
  void SetCh2Dimension(G4ThreeVector vec){SetCh2Dimension(vec.x(),
							  vec.y(),
							  vec.z());};
  
  void SetGV1Placement(G4double x, G4double y, G4double z);
  void SetGV1Placement(G4ThreeVector vec){SetGV1Placement(vec.x(),
							    vec.y(),
							    vec.z());};
  
  void SetRed0Placement(G4double x, G4double y, G4double z);
  void SetRed0Placement(G4ThreeVector vec){SetRed0Placement(vec.x(),
							    vec.y(),
							    vec.z());};

  void SetBellPlacement(G4double x, G4double y, G4double z);
  void SetBellPlacement(G4ThreeVector vec){SetBellPlacement(vec.x(),
							    vec.y(),
							    vec.z());};

  void SetCh0Placement(G4double x, G4double y, G4double z);
  void SetCh0Placement(G4ThreeVector vec){SetCh0Placement(vec.x(),
							    vec.y(),
							    vec.z());};

  void SetCh2Placement(G4double x, G4double y, G4double z);
  void SetCh2Placement(G4ThreeVector vec){SetCh2Placement(vec.x(),
							    vec.y(),
							    vec.z());};

  hbarCUSPField*  GetCUSPField() {return cuspField;} // Added by Balint


  G4double GetCenterOfTheCusp()         {return CenterOfTheCusp;}; // Added by tajima
  G4double GetCenterOfTheMikiLense()         {return CenterOfTheMikiLense;}; // Added by tajima

  // Added by tajima -- start
  void SetCenterOfTheCusp(G4double val) {CenterOfTheCusp = val;};


  // -- duct between sextupole and GV
  void SetSGVD0Dimension(G4double ind, G4double oud, G4double len);
  void SetSGVD0Dimension(G4ThreeVector vec){SetSGVD0Dimension(vec.x(),vec.y(),vec.z());};
  void SetSGVD1Dimension(G4double ind, G4double oud, G4double len);
  void SetSGVD1Dimension(G4ThreeVector vec){SetSGVD1Dimension(vec.x(),vec.y(),vec.z());};
  // -- Miki Lense
  void UseMLDuctP(G4int num);
  void AddMLDuctP();
  void SetMLDuctPDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetMLDuctPDimension(G4ThreeVector vec){SetMLDuctPDimension(MLDuctPpt,vec.x(),vec.y(),vec.z());};
  void SetMLEleDimension(G4double ind, G4double oud, G4double len);
  void SetMLEleDimension(G4ThreeVector vec){SetMLEleDimension(vec.x(),vec.y(),vec.z());};
  void SetMLEleSubDimension(G4double ind, G4double oud, G4double len);
  void SetMLEleSubDimension(G4ThreeVector vec){SetMLEleSubDimension(vec.x(),vec.y(),vec.z());};
  // -- duct between MLduct and sextupole


  void UseMLSD(G4int num);
  void AddMLSD();
  void SetMLSDDimension(G4int num, G4double ind, G4double oud, G4double len);
  void SetMLSDDimension(G4ThreeVector vec){SetMLSDDimension(MLSDpt,vec.x(),vec.y(),vec.z());};
  void SetCenterOfTheMikiLense(G4double val);
  void SetCenterOfTheMLSD(G4double val);
  void SetCenterOfTheSGVD(G4double val);



private:
  // Added by tajima -- start

  G4double           CenterOfTheCusp, CenterOfTheMikiLense, CenterOfTheMLSD, CenterOfTheSGVD;
  G4bool             useCUSPprev;
  G4bool             useCUSPMain, useCUSPMRE, useCUSPMREsup, useCUSPUHV, useCUSPCuCol, useCUSPCuSt;
  G4bool             useCUSPPol, useCUSPCuPlate, useCUSPSUSband, useCUSPRadShield, useCUSPOVC;
  G4bool             useCUSPMgCoil, useCUSPMgShield, useCUSPTPlsci, sensCuspTPLASCIN[maxCuspTPlsci];

  // -- Cusp Main
  G4int              CuspMPartpt;
  G4bool             useCuspMPart[maxCuspMPart]; 
  G4double           CuspMBHalfXLen[maxCuspMPart], CuspMBHalfYLen[maxCuspMPart], CuspMBHalfZLen[maxCuspMPart];
  G4double           CuspMTInRad[maxCuspMPart], CuspMTOutRad[maxCuspMPart], CuspMTHalfLen[maxCuspMPart];
  G4double           CuspMHInRad[maxCuspMPart], CuspMHOutRad[maxCuspMPart], CuspMHHalfLen[maxCuspMPart];
  G4double           CuspMtransY[maxCuspMPart], CuspMaintransY[maxCuspMPart]; 
  G4double           CuspMainPosY[maxCuspMain], CuspMainPosZ[maxCuspMain]; 
  G4VSolid*          CuspMBox[maxCuspMPart]; 
  G4VSolid*          CuspMTub[maxCuspMPart]; 
  G4VSolid*          CuspMHole[maxCuspMPart]; 
  G4VSolid*          Cusp_M[maxCuspMPart]; 
  G4VSolid*          CuspMain[maxCuspMain]; 
  G4LogicalVolume*   CuspMainLog[maxCuspMain]; 
  G4VPhysicalVolume* CuspMainPhy[maxCuspMain]; 

  // -- MRE
  G4int              CuspMREpt;
  G4bool             useCuspMRE[maxCuspMRE]; 
  G4double           CuspMREInRad[maxCuspMRE],CuspMREOutRad[maxCuspMRE],CuspMREHalfLen[maxCuspMRE];
  G4double           CuspMREPosZ[maxCuspMRE]; 
  G4ThreeVector      CuspMREPos[maxCuspMRE]; 
  G4VSolid*          CuspMRE[maxCuspMRE]; 
  G4LogicalVolume*   CuspMRELog[maxCuspMRE]; 
  G4VPhysicalVolume* CuspMREPhy[maxCuspMRE]; 
  G4VSolid*          CuspMRESeg[maxCuspMRESeg]; 
  G4VSolid*          CuspMRESegsup[8]; 
  G4LogicalVolume*   CuspMRESegsupLog[2]; 
  G4VPhysicalVolume* CuspMRESegsupPhy[2]; 

  // -- MRE support
  G4int              CuspMREsuppt;
  G4bool             useCuspMREsup[maxCuspMREsup]; 
  G4double           CuspMREsupHalfXLen[maxCuspMREsup],CuspMREsupHalfYLen[maxCuspMREsup],CuspMREsupHalfZLen[maxCuspMREsup];
  G4ThreeVector      CuspMREsupPos[maxCuspMREsup];
  G4RotationMatrix   CuspMREsupRot[maxCuspMREsup];
  G4VSolid*          CuspMREsup[maxCuspMREsup]; 
  G4LogicalVolume*   CuspMREsupLog[maxCuspMREsup]; 
  G4VPhysicalVolume* CuspMREsupPhy[maxCuspMREsup]; 

  // -- UHV bore
  G4int              CuspUHVpt;
  G4bool             useCuspUHV[maxCuspUHV]; 
  G4double           CuspUHVbInRad[maxCuspUHVb],CuspUHVbOutRad[maxCuspUHVb],CuspUHVbHalfLen[maxCuspUHVb];
  G4double           CuspUHVcnFInRad,CuspUHVcnRInRad,CuspUHVcnFOutRad,CuspUHVcnROutRad,CuspUHVcnHalfLen;
  G4double           CuspUHVPosZ[maxCuspUHV];
  G4ThreeVector      CuspUHVPos[maxCuspUHV];
  G4VSolid*          CuspUHV[maxCuspUHV]; 
  G4LogicalVolume*   CuspUHVLog[maxCuspUHV]; 
  G4VPhysicalVolume* CuspUHVPhy[maxCuspUHV]; 

  // -- copper collar
  G4double           CuspCuColInRad,CuspCuColOutRad,CuspCuColHalfLen;
  G4ThreeVector      CuspCuColPos;
  G4LogicalVolume*   CuspCuColLog; 
  G4VPhysicalVolume* CuspCuColPhy; 

  // -- copper strip
  G4int              CuspCuStpt;
  G4bool             useCuspCuSt[maxCuspCuSt]; 
  G4double           CuspCuStHalfXLen[maxCuspCuSt],CuspCuStHalfYLen[maxCuspCuSt],CuspCuStHalfZLen[maxCuspCuSt];
  G4ThreeVector      CuspCuStPos[maxCuspCuSt];
  G4RotationMatrix   CuspCuStRot[maxCuspCuSt];
  G4VSolid*          CuspCuSt[maxCuspCuSt]; 
  G4LogicalVolume*   CuspCuStLog[maxCuspCuSt]; 
  G4VPhysicalVolume* CuspCuStPhy[maxCuspCuSt]; 

  // -- poll
  G4int              CuspPolpt;
  G4bool             useCuspPol[maxCuspPol]; 
  G4double           CuspPolInRad[maxCuspPol],CuspPolOutRad[maxCuspPol],CuspPolHalfLen[maxCuspPol];
  G4double           CuspPolPosY[maxCuspPol],CuspPolPosZ[maxCuspPol]; 
  G4ThreeVector      CuspPolPos[maxCuspPol]; 
  G4RotationMatrix   CuspPolRot[maxCuspPol];
  G4VSolid*          CuspPol[maxCuspPol]; 
  G4LogicalVolume*   CuspPolLog[maxCuspPol]; 
  G4VPhysicalVolume* CuspPolPhy[maxCuspPol]; 
  // -- copper plate
  G4double           CuspCuPlateHalfXLen[maxCuspCuPlate];
  G4double           CuspCuPlateHalfYLen[maxCuspCuPlate];
  G4double           CuspCuPlateHalfZLen[maxCuspCuPlate];
  G4ThreeVector      CuspCuPlatePos[maxCuspCuPlate];
  G4int              CuspCuPlatept;
  G4bool             useCuspCuPlate[maxCuspCuPlate]; 
  G4VSolid*          CuspCuPlate[maxCuspCuPlate]; 
  G4LogicalVolume*   CuspCuPlateLog[maxCuspCuPlate]; 
  G4VPhysicalVolume* CuspCuPlatePhy[maxCuspCuPlate]; 
  // -- SUS band
  G4double           CuspSUSbandInRad[maxCuspSUSband],CuspSUSbandOutRad[maxCuspSUSband];
  G4double           CuspSUSbandHalfLen[maxCuspSUSband];
  G4double           CuspSUSbandPosZ[maxCuspSUSband];
  G4int              CuspSUSbandpt;
  G4bool             useCuspSUSband[maxCuspSUSband]; 
  G4VSolid*          CuspSUSband[maxCuspSUSband]; 
  G4LogicalVolume*   CuspSUSbandLog[maxCuspSUSband]; 
  G4VPhysicalVolume* CuspSUSbandPhy[maxCuspSUSband]; 
  G4ThreeVector      CuspSUSbandPos[maxCuspSUSband];
  // -- copper cylinder (radation shield)
  G4double            CuspRadShieldInRad, CuspRadShieldOutRad,CuspRadShieldHalfLen; 
  G4LogicalVolume*    CuspRadShieldLog; 
  G4VPhysicalVolume*  CuspRadShieldPhy;
  G4ThreeVector       CuspRadShieldPos; 
  // -- OVC
  G4double           CuspOVCInRad[maxCuspOVC], CuspOVCOutRad[maxCuspOVC], CuspOVCHalfLen[maxCuspOVC];
  G4double           CuspOVCrInRad[maxCuspOVCr], CuspOVCrOutRad[maxCuspOVCr], CuspOVCrHalfLen[maxCuspOVCr];
  G4double           CuspOVCPosZ[maxCuspOVC];
  G4int              CuspOVCpt, CuspOVCrpt;
  G4bool             useCuspOVC[maxCuspOVC], useCuspOVCr[maxCuspOVCr];
  G4VSolid*          CuspOVC[maxCuspOVC]; 
  G4VSolid*          CuspOVCr[maxCuspOVCr]; 
  G4LogicalVolume*   CuspOVCLog[maxCuspOVC]; 
  G4VPhysicalVolume* CuspOVCPhy[maxCuspOVC]; 
  G4ThreeVector      CuspOVCPos[maxCuspOVC];
  // -- magnet coil
  G4double           CuspMgCoilInRad[maxCuspMgCoil], CuspMgCoilOutRad[maxCuspMgCoil], CuspMgCoilHalfWidth[maxCuspMgCoil];
  G4int              CuspMgCoilpt;
  G4bool             useCuspMgCoil[maxCuspMgCoil]; 
  G4ThreeVector      CuspMgCoilPos[maxCuspMgCoil];
  G4VSolid*          CuspMgCoil[maxCuspMgCoil]; 
  G4LogicalVolume*   CuspMagCoilLog; 
  G4VPhysicalVolume* CuspMagCoilPhys; 
  // -- magnetic shield
  G4double           CuspMgShInRad[maxCuspMgSh];
  G4double           CuspMgShOutRad[maxCuspMgSh];
  G4double           CuspMgShHalfLen[maxCuspMgSh];
  G4double           CuspMgShCnFInRad[maxCuspMgShCn];
  G4double           CuspMgShCnRInRad[maxCuspMgShCn];
  G4double           CuspMgShCnFOutRad[maxCuspMgShCn];
  G4double           CuspMgShCnROutRad[maxCuspMgShCn];
  G4double           CuspMgShCnHalfLen[maxCuspMgShCn];
  G4int              CuspMgShpt;
  G4int              CuspMgShCnpt;
  G4bool             useCuspMgSh[maxCuspMgSh]; 
  G4bool             useCuspMgShCn[maxCuspMgShCn]; 
  G4VSolid*          CuspMgSh[maxCuspMgSh]; 
  G4VSolid*          CuspMgShCn[maxCuspMgShCn]; 
  G4LogicalVolume*   CuspMgShieldLog; 
  G4VPhysicalVolume* CuspMgShieldPhy; 
  // -- track detector
  G4double           CuspTPlsciHalfXLen[maxCuspTPlsci];
  G4double           CuspTPlsciHalfYLen[maxCuspTPlsci];
  G4double           CuspTPlsciHalfZLen[maxCuspTPlsci];
  G4ThreeVector      CuspTPlsciPos[maxCuspTPlsci];
  G4int              CuspTPlscipt;
  G4bool             useCuspTPlsci[maxCuspTPlsci]; 
  G4VSolid*          CuspTPlsci[maxCuspTPlsci]; 
  G4LogicalVolume*   CuspTPlsciLog[maxCuspTPlsci]; 
  G4VPhysicalVolume* CuspTPlsciPhy[maxCuspTPlsci]; 


  // Added by Balint -- start
  G4String AsciiElfilename;
  G4String AsciiMagfilename;
  // Added by Balint -- stop

  hbarCUSPMess* cuspMessenger;   //!< pointer to the Messenger
  
  // Added by Chloe -- start
  G4double            innerRadiusOfTheKB0; //!< Inner radius of the CUSP beampipe
  G4double            outerRadiusOfTheKB0; //!< Outer radius of the CUSP beampipe
  G4double            hightOfTheKB0; //!< height of the CUSP beampipe
  G4double            beamline0Pos_x ; //!< x position of the center of the CUSP beampipe
  G4double            beamline0Pos_y; //!< y position of the center of the CUSP beampipe
  G4double            beamline0Pos_z; //!< z position of the center of the CUSP beampipe
  G4double            innerRadiusOfTheKB1; //!< Inner radius of the CUSP cold bore
  G4double            outerRadiusOfTheKB1; //!< Outer radius of the CUSP cold bore
  G4double            hightOfTheKB1; //!< height of the CUSP cold bore
  G4double            beamline1Pos_x ; //!< x position of the center of the CUSP cold bore
  G4double            beamline1Pos_y; //!< y position of the center of the CUSP cold bore
  G4double            beamline1Pos_z; //!< z position of the center of the CUSP beampipe
  G4double            innerRadiusOfCuspT1; //!< Inner radius of the CUSP Tube 1 (substraction solid)
  G4double            outerRadiusOfCuspT1; //!< Outer radius of the CUSP Tube 1 (substraction solid)
  G4double            hightOfCuspT1; //!< height of the CUSP Tube 1 (substraction solid)
  G4double            innerRadiusOfCuspT2; //!< Inner radius of the CUSP Tube 2 (substraction solid)
  G4double            outerRadiusOfCuspT2; //!< Outer radius of the CUSP Tube 2 (substraction solid)
  G4double            hightOfCuspT2; //!< height of the CUSP Tube 2 (substraction solid)
  G4double            cuspWidth; //!< Width of the CUSP Box (substraction solid)
  G4double            cuspLength;//!< Length of the CUSP Box (substraction solid)
  G4double            cuspThick;//!< Thickness of the CUSP Box (substraction solid)
  G4double            cuspPos_x ; //!< x position of the center of the CUSP
  G4double            cuspPos_y ; //!< y position of the center of the CUSP
  G4double            cuspPos_z ; //!< z position of the center of the CUSP
  G4double           CBFInRad[maxCBF];  //!< inner radius of the CUSP cold bore flanges
  G4double           CBFOutRad[maxCBF]; //!< outer radius of the CUSP cold bore flanges
  G4double           CBFLength[maxCBF]; //!< length of the CUSP cold bore flanges
  G4double           CBFPos_x[maxCBF]; //!< x Placement of the CUSP cold bore flanges
  G4double           CBFPos_y[maxCBF]; //!< y Placement of the CUSP cold bore flanges
  G4double           CBFPos_z[maxCBF]; //!< z Placement of the CUSP cold bore flanges
  G4int              CBFpt;              //!< number of current CBF
  G4bool             useCBF[maxCBF];    //!< use this specific CBF 
  G4double            innerRadiusOfTheGV1; //!< Inner radius of the Gate valve 1
  G4double            outerRadiusOfTheGV1; //!< Outer radius of the Gate valve 1
  G4double            hightOfTheGV1; //!< height of the GV1
  G4double            innerRadiusOfTheRed0; //!< Inner radius of the reducing flange 0
  G4double            outerRadiusOfTheRed0; //!< Outer radius of the reducing flange 0
  G4double            hightOfTheRed0; //!< height of the reducing flange 0
  G4double            innerRadiusOfTheBell; //!< Inner radius of the bellow
  G4double            outerRadiusOfTheBell; //!< Outer radius of the bellow
  G4double            hightOfTheBell; //!< height of the bellow
  G4double           HDFInRad[maxHDF];  //!< inner radius of  flanges
  G4double           HDFOutRad[maxHDF]; //!< outer radius of  flanges
  G4double           HDFLength[maxHDF]; //!< length of the  flanges
  G4double           HDFPos_x[maxHDF]; //!< x Placement of the flanges
  G4double           HDFPos_y[maxHDF]; //!< y Placement of the  flanges
  G4double           HDFPos_z[maxHDF]; //!< z Placement of the flanges
  G4int              HDFpt;              //!< number of current HDF
  G4bool             useHDF[maxHDF];    //!< use this specific HDF 
  G4double            innerRadiusOfCh0; //!< Inner radius of the chamber 0
  G4double            outerRadiusOfCh0; //!< Outer radius of the chamber 0
  G4double            hightOfCh0; //!< height of the chamber 0
  G4double            innerRadiusOfCh2; //!< Inner radius of the chamber 2
  G4double            outerRadiusOfCh2; //!< Outer radius of the chamber 2
  G4double            hightOfCh2; //!< height of the chamber 2
  G4double            gv1Pos_x ; //!< x position of the center of the Gate valve 1
  G4double            gv1Pos_y ; //!< y position of the center of the Gate valve 1
  G4double            gv1Pos_z ; //!< z position of the center of the  Gate valve 1
  G4double            red0Pos_x ; //!< x position of the center of the reducing flange
  G4double            red0Pos_y ; //!< y position of the center of the reducing flange
  G4double            red0Pos_z ; //!< z position of the center of the  reducing flange
  G4double            bellPos_x ; //!< x position of the center of the bellow
  G4double            bellPos_y ; //!< y position of the center of the bellow
  G4double            bellPos_z ; //!< z position of the center of the  bellow
  G4double            ch0Pos_x ; //!< x position of the center of the chamber 0
  G4double            ch0Pos_y ; //!< y position of the center of the chamber 0
  G4double            ch0Pos_z ; //!< z position of the center of the  chamber 0
  G4double            ch2Pos_x ; //!< x position of the center of the chamber 1
  G4double            ch2Pos_y ; //!< y position of the center of the chamber 1
  G4double            ch2Pos_z ; //!< z position of the center of the  chamber 1

  hbarCUSPField*     cuspField; // Added by Balint
  hbarFieldSetup*    magFieldSetup;

  // -- Miki Lense
  G4double           MLDuctPInRad[maxMLDuctP];
  G4double           MLDuctPOutRad[maxMLDuctP];
  G4double           MLDuctPHalfLen[maxMLDuctP];
  G4int              MLDuctPpt;
  G4bool             useMLDuctP[maxMLDuctP]; 
  G4VSolid*          MLDuctP[maxMLDuctP]; 
  G4LogicalVolume*   MLDuctLog; 
  G4VPhysicalVolume* MLDuctPhy; 
  G4double           MLEleInRad,MLEleOutRad,MLEleHalfLen;
  G4double           MLEleSubInRad,MLEleSubOutRad,MLEleSubHalfLen;
  G4VSolid*          MLEle[maxMLEle]; 
  G4VSolid*          MLEleSub[maxMLEleSub]; 
  G4LogicalVolume*   MLEleLog; 
  G4VPhysicalVolume* MLElePhy; 
  G4LogicalVolume*   MLElesupLog; 
  G4VPhysicalVolume* MLElesupPhy; 



  G4LogicalVolume*  beamline0_log; 
  G4VPhysicalVolume* beamline0_phys;

  G4LogicalVolume*  cuspm0_log; 
  G4VPhysicalVolume* cuspm0_phys;

  G4LogicalVolume*  beamline1_log; 
  G4VPhysicalVolume* beamline1_phys;
  G4VSolid*          CBF[maxCBF]; 
  G4LogicalVolume*   CBFLog[maxCBF];
  G4VPhysicalVolume* CBFPhy[maxCBF]; 
  G4LogicalVolume*  gv1_log;
  G4VPhysicalVolume* gv1_phys;

  G4LogicalVolume*  red0_log;
  G4VPhysicalVolume* red0_phys;

  G4LogicalVolume*  bell_log;
  G4VPhysicalVolume* bell_phys;

  G4VSolid*          HDF[maxHDF]; 
  G4LogicalVolume*   HDFLog[maxHDF]; 
  G4VPhysicalVolume* HDFPhy[maxHDF]; 

  G4LogicalVolume*  chambervac0_log;
  G4VPhysicalVolume* chambervac0_phys;

  G4LogicalVolume*  chamber2_log;
  G4VPhysicalVolume* chamber2_phys;
  //--- added by Chloe --> stop

  // -- duct between sextupole and GV
  G4double           SGVD0InRad,SGVD0OutRad,SGVD0HalfLen;
  G4double           SGVD1InRad,SGVD1OutRad,SGVD1HalfLen;
  G4LogicalVolume*   SGVDLog; 
  G4VPhysicalVolume* SGVDPhy; 

  // -- duct between MLduct and sextupole
  G4double           MLSDInRad[maxMLSD],MLSDOutRad[maxMLSD],MLSDHalfLen[maxMLSD];
  G4int              MLSDpt;
  G4bool             useMLSD[maxMLSD]; 
  G4VSolid*          MLSD[maxMLSD]; 
  G4LogicalVolume*   MLSDLog; 
  G4VPhysicalVolume* MLSDPhy; 
  G4bool             useMLSDUCT, useSGVDUCT, useMikiLense, useMLDUCT, useMLELE;



};


#endif
