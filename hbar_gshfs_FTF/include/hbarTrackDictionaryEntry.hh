#include "G4ios.hh"                 // Include from 'system'

#ifndef hbarTrackDictionaryEntry_hh
#define hbarTrackDictionaryEntry_hh 1
/**
   Contains tracking information for a step. 
   Used for correct colouring of the steps.
 */
class hbarTrackDictionaryEntry
{
public:
  hbarTrackDictionaryEntry(G4int _N=-1000, ///Principal quantum number.
						   G4int _F = -1000, ///F quantum number.
						   G4int _M = -1000 ///M quantum number (note: for GS!).
						   ); ///Constructor.
  ~hbarTrackDictionaryEntry(); ///Destructor.

  bool operator == (const hbarTrackDictionaryEntry &b ///To compare with.
					) const; ///Standard overloaded operator.

  bool operator != (const hbarTrackDictionaryEntry &b ///To compare with.
					) const; ///Standard overloaded operator.


  G4int N; ///N quantum number.
  G4int F; ///F quantum number.
  G4int M; ///M quantum number.
};


#endif
