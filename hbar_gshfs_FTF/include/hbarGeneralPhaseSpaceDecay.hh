//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//
//
//
#ifndef hbarGeneralPhaseSpaceDecay_h
#define hbarGeneralPhaseSpaceDecay_h 1

#include "G4ios.hh"
#include "globals.hh"
#include "G4VDecayChannel.hh"

//! The Genat4 decay handler
/*!
  This class handles the decay of particles, momentumconservation etc.
 */
class hbarGeneralPhaseSpaceDecay : public G4VDecayChannel
{
public:
  //Constructors 
  hbarGeneralPhaseSpaceDecay(G4int Verbose = 1);
  /*****
	hbarGeneralPhaseSpaceDecay(const G4String& theParentName,
	G4double        theBR,
	G4int           theNumberOfDaughters,
	const G4String& theDaughterName1,
	const G4String& theDaughterName2 = "",
	const G4String& theDaughterName3 = "");

	hbarGeneralPhaseSpaceDecay(const G4String& theParentName,
	G4double        theParentMass,
	G4double        theBR,
	G4int           theNumberOfDaughters,
	const G4String& theDaughterName1,
	const G4String& theDaughterName2 = "",
	const G4String& theDaughterName3 = "");
  *******/
  hbarGeneralPhaseSpaceDecay(G4double        theParentMass,
			     G4double        theBR,
			     G4int           theNumberOfDaughters,
			     const G4String& theDaughterName1,
			     const G4String& theDaughterName2 = "",
			     const G4String& theDaughterName3 = "",
			     const G4String& theDaughterName4 = "",
			     const G4String& theDaughterName5 = "",
			     const G4String& theDaughterName6 = "",
			     const G4String& theDaughterName7 = "",
			     const G4String& theDaughterName8 = "",
			     const G4String& theDaughterName9 = "",
			     const G4String& theDaughterName10 = "");

  //  Destructor
  virtual ~hbarGeneralPhaseSpaceDecay();

public:
  G4double GetParentMass() const;
  void SetParentMass(const G4double aParentMass);
  virtual G4DecayProducts* DecayIt(G4double mass=0.0);   
  void ResetDaughters();
  //! calculate momentum of daughter particles in two-body decay
  static G4double Pmx(G4double e, G4double p1, G4double p2);

protected:
  G4DecayProducts* OneBodyDecayIt();
  G4DecayProducts* TwoBodyDecayIt();
  G4DecayProducts* ThreeBodyDecayIt();
  G4DecayProducts* ManyBodyDecayIt();
     
private:
  G4double parentmass;
  G4ParticleDefinition** daughters; // added by bernadette
  G4double* daughters_mass; // added by bernadette
    
};  



inline G4double hbarGeneralPhaseSpaceDecay::GetParentMass() const
{
  return parentmass;
}

inline void hbarGeneralPhaseSpaceDecay::SetParentMass(const G4double aParentMass)
{
  parentmass = aParentMass;
}



inline
G4double hbarGeneralPhaseSpaceDecay::Pmx(G4double e, G4double p1, G4double p2)
{
  // calculate momentum of daughter particles in two-body decay
  G4double ppp = (e+p1+p2)*(e+p1-p2)*(e-p1+p2)*(e-p1-p2)/(4.0*e*e);
  if (ppp>0) return sqrt(ppp);
  else       return -1.;
}

#endif
