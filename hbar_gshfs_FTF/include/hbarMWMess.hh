#ifndef hbarMWMessenger_h
#define hbarMWMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarMWSetup;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

//! This is the Microwave messenger class
/*!
  The defined macros in this class are:
  - /MW/setTransitionProbabilities
  - /MW/setUseFixedTransitionProbabilities
  - /MW/setMWFrequency
  - /MW/setMWPower
  - /MW/setMWMode
  - /MW/setCavityQ
  - /MW/setCavityMagneticField
  - /MW/setCavityMagneticFieldVariation
  - /MW/setMWFieldAngle
  - /MW/setRandomParticleNumber
  - /MW/scan/start
  - /MW/scan/setStartFrequency
  - /MW/scan/setEndFrequency
  - /MW/scan/setNumberOfSteps
  - /MW/scan/setFileName
 */
class hbarMWMess: public G4UImessenger
{
public:
  hbarMWMess(hbarMWSetup* );
  ~hbarMWMess();
  
  void SetNewValue(G4UIcommand*, G4String);
  
private:
  hbarMWSetup*               hbarmwSetup;          //!< pointer to the microwave setup
  
  G4UIdirectory*             hbarMWDir;            //!< /MW/
  G4UIdirectory*             hbarMWScanDir;        //!< /MW/scan/
  
  //G4UIcmdWithADouble*        Pi1TransProbCmd;
  //G4UIcmdWithADouble*        Pi2TransProbCmd;
  //G4UIcmdWithADouble*        Sigma1TransProbCmd;
  G4UIcmdWithADouble*        TransProbCmd;         //!< setTransitionProbabilities
  G4UIcmdWithAString*        UseFixedTransProbCmd; //!< setUseFixedTransitionProbabilities
  G4UIcmdWithADoubleAndUnit* MWFreqCmd;            //!< setMWFrequency
  G4UIcmdWithADoubleAndUnit* MWPowerCmd;           //!< setMWPower
  G4UIcmdWithAString*        MWModeCmd;            //!< setMWMode
  G4UIcmdWithADouble*        CavityQCmd;           //!< setCavityQ
  G4UIcmdWithADoubleAndUnit* CavityMFCmd;          //!< setCavityMagneticField
  G4UIcmdWithADoubleAndUnit* CavityMFDCmd;         //!< setCavityMagneticFieldVariation
  G4UIcmdWithADouble*        MWAngleCmd;           //!< setMWFieldAngle
  G4UIcmdWithABool*          RandomPartNumCmd;     //!< setRandomParticleNumber

  G4UIcmdWithAnInteger*      ScanOnCmd;            //!< start
  G4UIcmdWithADoubleAndUnit* ScanStartCmd;         //!< setStartFrequency
  G4UIcmdWithADoubleAndUnit* ScanEndCmd;           //!< setEndFrequency
  G4UIcmdWithAnInteger*      ScanStepsCmd;         //!< setNumberOfSteps
  G4UIcmdWithAString*        ScanFileNameCmd;      //!< setFileName

  G4UIcmdWithABool*          UseCavityCmd;         //!< setRandomParticleNumber
  
  G4UIcmdWithAnInteger*		 UseCFieldmapCmd;
 
};

#endif

