#ifndef hbarStackActionMessenger_h
#define hbarStackActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class hbarStackAction;
class G4UIcmdWithAString;

//! This messenger class sets which particle tracks to process
/*!
  This messenger class defines only one user command, with which the user
  can select which particle tracks to process: primary, hbar, charged, heavy,
  etc. Any particle which does not fall into the user-defined category is
  killed immediately after creation (and its energy is deposited in the 
  volume in which it is killed). The more tracks are processed, the slower 
  and more memory-consuming the simulation is.

  To select which particles to track in the simulation
  - primary: only the primary particle is tracked; all the secondaries are killed immediately
  - charged: only charged particles are tracked
  - pbar: only antiprotons are tracked
  - hbar: only antihydrogen atoms are tracked
  - heavy: only heavy particles are tracked, i.e. electrons and photons are not
  - fermion: only fermions are tracked
  - allbute+e-: all particles are tracked, except for electrons and positrons
  - all: all particles are tracked, except for neutrinos
  - allwithneutrino: all particles are tracked

  This messenger class defines the following macro:
  - /tracking/processTracks
*/
class hbarStackActionMess: public G4UImessenger
{
public:
  hbarStackActionMess(hbarStackAction*);
  ~hbarStackActionMess();
  
  void SetNewValue(G4UIcommand*, G4String);

private:
  hbarStackAction*       stackAction;  //!< pointer to hbarStackAction
  G4UIcmdWithAString*    ProcessCmd;   //!< processTracks
};

#endif
