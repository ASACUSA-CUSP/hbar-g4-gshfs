#ifndef hbarExtFIField_h
#define hbarExtFIField_h 1

#include "globals.hh"
#include "G4ElectricField.hh"
#include "G4ElectroMagneticField.hh"

#include "TROOT.h"
#include "TGraph.h"
#include "TGraph2D.h"


class hbarDetConst;


class hbarExtFIField : public G4ElectricField
{
public:
  hbarExtFIField();
  ~hbarExtFIField();

  //! this function does the calculation!
  void GetFieldValue( const  double Point[3],
		      double *Efield ) const;
		      
  //void GetFieldValueBasic(const G4double Point[3], G4double Efield[], G4double coeff = 1.0, G4double E_null = 0.0) const;

  //G4double GetFieldMaxValue(G4int num) {return B_r_max[num-1];};
  //    G4double GetCoefficient() {return coeff;};

  G4double GetFieldAngle(G4double x, G4double y);

  //! returns the number of the currently active ExtFI during geometry generation
  void CurrentExtFI(G4int);

  // If DoesFieldChangeEnergy() returns true,
  // the track time is not calculated (it is always zero).
  // This is a bug in GEANT4
  //
  // Update: this is now fixed
  G4bool DoesFieldChangeEnergy() const {return true;};
 // G4bool IsFieldFlipped(G4int num) {return fieldFlip[num-1];};

  void SetFieldMaxValue(G4int, G4double);
  void SetFieldMaxValue(G4double val);

  void SetExtFIcenter(G4double val) {ExtFIcenter = val;};
  G4double  GetExtFIcenter() {return ExtFIcenter;};
  //! not used at the moment
 // void ReadHarmonicsFile();
  //! not used at the moment .. not event fully defined
  //void ReadZFile();
  void Update();

protected:

  //    G4Cons *ExtFIMagFieldSol;
  hbarDetConst *hbarDetector;        //!< pointer to detector construction
  G4double      ExtFIcenter;
  G4double UpstreamVolt;
  G4double DownstreamVolt;
  
  
  /*G4int         Sextpt;              //!< number of currently active sextupole
  G4double      B_r_max[maxSN];      //!< pole tip field of the magnet
  G4double      sextStart[maxSN];    //!< starting position of the sextupolefild
  G4double      sextEnd[maxSN];      //!< ending position of the sextupolefild
  G4double      r_max_front[maxSN];  //!< maximum front field radius (not the pipe)
  G4double      r_max_rear[maxSN];   //!< maximum rear field radius (not the pipe)
  G4double      sextOffsetX[maxSN];  //!< displacement in X direction
  G4double      sextOffsetY[maxSN];  //!< displacement in Y direction
  G4double      sextRotX[maxSN];     //!< rotation along X axis
  G4double      sextRotY[maxSN];     //!< rotation along Y axis
  G4double      halfLength[maxSN];   //!< half length of the sextupole
  G4double      B_zero[maxSN];       //!< constant field in the center of the sextupole
  //    G4double coeff;
  //    G4double r_max;
  G4bool        useSext[maxSN];      //!< whether tp use the sextupole 
  G4bool        fieldFlip[maxSN];    //!< sets if the direction of the B field can be reversed
  G4int         poleNum;             //!< HALF of the multipole order (e.g. 3 for a sextupole)

  G4String      BharmonicsFileName;  //!< path to file that contains harmonics (currently not in use)
  G4String      BzFileName;          //!< path to file that contains a fieldmap of B in Beam direction (currently not in use)
  TGraph       *B_harmonics_A[21];   //!< place to store harmonics
  TGraph       *B_harmonics_B[21];   //!< place to store harmonics
  TGraph2D     *B_z_table[10];       //!< place to store fieldmap in beam (Z) direction */
};

#endif

