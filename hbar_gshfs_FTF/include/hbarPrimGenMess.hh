#ifndef hbarPrimaryGeneratorMessenger_h
#define hbarPrimaryGeneratorMessenger_h 1

#include "G4UImessenger.hh"
#include "globals.hh"

class hbarPrimGenAction;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcmdWithoutParameter;

//! This is the messenger class for generatin primary particles
/*!
  This a messenger class which defines user commands for the primary 
  generator: which particles to shoot, with which energy/temperature, from 
  which position, into which direction, etc.

  Macros defined in this class:
  - /gun/setRandomDirection
  - /gun/setRandomEnergy
  - /gun/setMaximumFlatRandomVelocity
  - /gun/setRandomState
  - /gun/setBeamEnergy
  - /gun/setSourceTemperature
  - /gun/setParticle
  - /gun/setSourceCenter
  - /gun/setSourceFWHM_X
  - /gun/setSourceFWHM_Y
  - /gun/setSourceFWHM_Z
  - /gun/setSourceFWHM
  - /gun/setSourceOpeningAngle
  - /gun/setSlice
  - /gun/setSourceOffsetX
  - /gun/setSourceOffsetY
  - /gun/setSourceMaximumRadius
  - /gun/setStateF
  - /gun/setStateM
  - /gun/setStateARatio
  - /gun/setStateBRatio
  - /gun/setStateCRatio
  - /gun/setStateDRatio
  - /gun/setBeamDirection
  - /gun/setProductionRate
  - /gun/setAlwaysResetProductionTime
  - /gun/resetProductionTime
  - /CRY/file
  - /CRY/input
  - /CRY/update
  - /gun/useCRY
  - /CRY/setDisplacement
  - /gun/useAEgISData
  - /gun/setSpectrumFilename
  - /gun/setUseParticleMixture
  - /gun/addBackgroundParticle
  - /gun/addBackgroundParticleNum
 */
class hbarPrimGenMess: public G4UImessenger
{
public:
  hbarPrimGenMess(hbarPrimGenAction*);
  ~hbarPrimGenMess();

  void SetNewValue(G4UIcommand*, G4String);

private:
  hbarPrimGenAction*           hbarAction;            //!< pointer to PrimGenAction

  G4UIcmdWithAString*          RndmDirCmd;            //!< setRandomDirection
  G4UIcmdWithAString*          RndmEnergyCmd;         //!< setRandomEnergy
  G4UIcmdWithADouble*          RndmVelCmd;            //!< setMaximumFlatRandomVelocity
  G4UIcmdWithAString*          RndmStateCmd;          //!< setRandomState
  G4UIcmdWithAString*          RndmRydbergCmd;          //!< setRandomRydberg
  G4UIcmdWithADoubleAndUnit*   KinECmd;               //!< setBeamEnergy
  G4UIcmdWithADoubleAndUnit*   SourceTempCmd;         //!< setSourceTemperature
  G4UIcmdWithAString*          PartCmd;               //!< setParticle
  G4UIcmdWithADoubleAndUnit*   SourceCenterCmd;       //!< setSourceCenter
  G4UIcmdWithADoubleAndUnit*   SourceSizeXCmd;        //!< setSourceFWHM_X
  G4UIcmdWithADoubleAndUnit*   SourceSizeYCmd;        //!< setSourceFWHM_Y
  G4UIcmdWithADoubleAndUnit*   SourceSizeZCmd;        //!< setSourceFWHM_Z
  G4UIcmdWithADoubleAndUnit*   SourceSizeCmd;         //!< setSourceFWHM
  G4UIcmdWithADouble*          SourceOpenAngleCmd;    //!< setSourceOpeningAngle
  G4UIcmdWithAString*          SliceCmd;              //!< setSlice
  G4UIcmdWithADoubleAndUnit*   SourceOffsetXCmd;      //!< setSourceOffsetX
  G4UIcmdWithADoubleAndUnit*   SourceOffsetYCmd;      //!< setSourceOffsetY
  G4UIcmdWithADoubleAndUnit*   SourceMaxRadCmd;       //!< setSourceMaximumRadius
  G4UIcmdWithAnInteger*        SourceStateFCmd;       //!< setStateF
  G4UIcmdWithAnInteger*        SourceStateMCmd;       //!< setStateM
  G4UIcmdWithADouble*          SourceStateARatioCmd;  //!< setStateARatio
  G4UIcmdWithADouble*          SourceStateBRatioCmd;  //!< setStateBRatio
  G4UIcmdWithADouble*          SourceStateCRatioCmd;  //!< setStateCRatio
  G4UIcmdWithADouble*          SourceStateDRatioCmd;  //!< setStateDRatio
  G4UIcmdWith3Vector*          DirCmd;                //!< setBeamDirection
  G4UIcmdWithADoubleAndUnit*   ProdRateCmd;           //!< setProductionRate
  G4UIcmdWithABool*            AlwaysRTCmd;           //!< setAlwaysResetProductionTime
  G4UIcmdWithoutParameter*     ResetTimeCmd;          //!< resetProductionTime


  ///Added by Rikard -- start
  G4UIcmdWithAString*          PreloadQuantumStatesCmd; 
  G4UIcmdWithAString*          EnableFieldRegimeTranslationsCmd;
  G4UIcmdWithAString*          EnableQuantumTransitionCmd;
  G4UIcmdWithAString*          EnableQuantumEnergyForceCmd;
  G4UIcmdWithAString*          QuantumStateFileCmd;
  G4UIcmdWithAString*          UseStrongInitialStateCmd;
  G4UIcmdWithAnInteger*        SourceStateNCmd;       //!< setStateN
  G4UIcmdWithAnInteger*        SourceStateLCmd;       //!< setStateL
  G4UIcmdWithAnInteger*        SourceStateTwoJCmd;       //!< setStateJ
  G4UIcmdWithAnInteger*        SourceStateTwoMJCmd;       //!< setStateM
  G4UIcmdWithAnInteger*        SourceStateMLCmd;       //!< setStateJ
  G4UIcmdWithAnInteger*        SourceStateTwoMSCmd;       //!< setStateMJ

  G4UIcmdWithAnInteger*        SourceStateMaxNCmd;       //!< setStateMaxN
  G4UIcmdWithAnInteger*        SourceStateMaxLCmd;       //!< setStateMaxL


  G4UIcmdWithADouble*          LowerHysteresisCmd; 
  G4UIcmdWithADouble *         UpperHysteresisCmd;

  ///Added by Rikard -- stop


  // Added by Clemens -- start
  G4UIcmdWithAString*          CRYFileCmd;            //!< file
  G4UIcmdWithAString*          CRYInputCmd;           //!< input
  G4UIcmdWithoutParameter*     CRYUpdateCmd;          //!< update
  G4UIcmdWithABool*            useCRYCmd;             //!< useCRY
  G4UIcmdWith3Vector*          CRYDispacementCmd;     //!< setDisplacement
  std::string*                 MessInput;             //!< temporary storage variable for CRY commands
  
  G4UIcmdWithABool*            useAEgISCmd;           //!< useAEgISData
  G4UIcmdWithAString*          SpectrumCmd;           //!< setSpectrumFilename

  G4UIcmdWithABool*            useParticleMixtureCmd; //!< setUseParticleMixture
  G4UIcmdWithAString*          addBGParticleCmd;      //!< addBackgroundParticle
  G4UIcmdWithAnInteger*        addBGParticleNumCmd;   //!< addBackgroundParticleNum
  // Added by Clemens -- stop
};

#endif

