#ifndef hbarField_h
#define hbarField_h 1

#include "globals.hh"
#include "G4MagneticField.hh"
#include "G4ElectroMagneticField.hh"

//#include "TROOT.h"
//#include "TGraph.h"
//#include "TGraph2D.h"

#include "hbarSextupoleField.hh"
#include "hbarCUSPField.hh"
#include "hbarCavityField.hh"
#include "hbarExtFIField.hh"
//#include "hbarBeamlineConst.hh"

class hbarDetConst;

//class G4Cons;

//! Magnetic and electric field definitions
/*!
  This class defines magnetic and electric fields used in the simulation.
  The actual calculation of the 
  field is done in the GetFieldValue() function. The spatial coordinates 
  are world coordinates i.e. not with respect to the sextupole volume. 
  Because of this, the function first checks whether the point is within a 
  sextupole volume or not, and if yes, then it calculates the magnetic field 
  and its gradient.
  
*/
class hbarField : public hbarSextupoleField
{
public:
  hbarField(hbarDetConst*);
  //    hbarField(hbarDetConst*, G4double);
  ~hbarField();

  //! this function does the calculation!
  /*!
    GetFieldValue takes a point consiting of three coordintes and returns the
    field values at this point by writing them to the Bfield pointer. As with 
    G4.10 the Bfield array can store up to 24 entries. The current usage is as 
    follows:
    - Bfield[0-2]   B field
    - Bfield[3-5]   E field
    - Bfield[6-8]   grad(B)
    - Bfield[9-11]  grad(E)
    - Bfield[12]    particle magnetic moment depending on external B
    - Bfield[13-15] grad(mu) depending on external B

    The magnetic moment and its gradient  gets calculated within 
    @ref hbarStepAction.    
    
    @param Point geometric point in world coordinates
    @param Bfield stores the return values   
   */
  void GetFieldValue( const  double Point[3],
		      double *Bfield ) const;

  //! returns a pointer to the intermediate storage of grad(mu)
  /*!
    The purpose of this function is to provide a link between the field
    and @ref hbarStepAction. 

    @return pointer to the intermediate storage of the gradient of the 
    particles magnetic moment
   */
  G4double* GetGradMu(){return gradmu;}

  //! returns a pointer to the intermediate storage of the magnetic moment
  /*!
    Provides the link to @ref hbarStepAction. Geant4 doens not have a
    facility to calculate the magnetic moment dynamically depending on the
    external fields. So currently (july 2013) the only way to do this is my 
    producing cross links to the StepAction class.

    @return pointer to the intermediate storage of the particles magnetic
    moment
   */
  G4double* GetMu(){return mu;}

  //! updates the sextupole settings with new values taken from macros
  void Update();

  //! provides the link to the CUSP trap magnetic field
  void SetCUSPField(hbarCUSPField *field){cuspfield = field;};
  //! provides the link to the cavity magnetic field
  void SetCavityField(hbarCavityField *field) {cavityfield = field;};

  void SetextFIField(hbarExtFIField *field) {ExtFIField = field;};
private:

#define   maxSN  20
  
  hbarDetConst  *hbarDetector;        //!< pointer to detector construction
  hbarCUSPField *cuspfield;           //!< pointer to the CUSP field definition
  hbarCavityField *cavityfield;		  //!< pointer to the cavity field definition
  hbarExtFIField *ExtFIField;		  //!< pointer to the external field ioniser field definition
  
  G4double *gradmu;                   //!< pointer to grad(mu)
  G4double *mu;                       //!< pointer to the magnetic moment
};

#endif

