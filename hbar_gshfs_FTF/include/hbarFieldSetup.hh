#ifndef hbarFieldSetup_h
#define hbarFieldSetup_h

#include "G4MagneticField.hh"
#include "G4UniformMagField.hh"
#include "hbarField.hh"


class G4MagInt_Driver; 
class G4FieldManager;
class G4ChordFinder;
class G4Mag_UsualEqRhs;
class G4MagIntegratorStepper;


class hbarDetConst;
class hbarEqSextupoleField;
class hbarFieldMess;
class hbarAntiHydrogen;
class hbarHydrogenLike;
class hbarHydrogen; // Added by Clemens

//! Defintion of the cavity
/*!
  This is one of the most important classes, because it handles the RF 
  transitions in the cavity. In the simulation each Hbar atom which enters 
  the RF cavity sees both the static and the oscillating magnetic fields there.
  The magnitude of the static magnetic field determines the hyperfine 
  transition frequency (via the "frequency vs B" Breit-Rabi diagram), 
  while the amplitude of the oscillating magnetic field determines how fast 
  the transitions occur (Rabi oscillation frequency).
  
  The value of the static field is randomly generated for each atom in the 
  range [static field-0.5*maximum field variation, static field+0.5*maximum 
  field variation]. This field then remains constant as the atom flies 
  through the cavity. The RF transition frequencies are calculated as a 
  function of this static field using the Breit-Rabi formula.
  
  The amplitude of the oscillating magnetic field is calculated using the 
  given RF power P and the cavity Q. In case of the double strip line cavity, 
  this is done with the formula:
  
  \f[B = \sqrt(\frac{P*Q*100}{(c*d)})\f],
  
  where c is the speed of light, d is the separation between the two strip 
  lines, and the 100 stands for 100 Ohm. This calculation is not necessarily 
  100% accurate, so use it only as a rough guidance to estimate what power 
  is needed in the cavity. Unlike the magnitude of the static field, the 
  amplitude of the oscillating field does change within the cavity, for 
  example in the double stripling cavity where the amplitude follows a sin(Z) 
  dependence (Z is the beam direction, Z=0 in the center of the cavity).
  
  When an Hbar atom enters the cavity, it is in one of the four possible spin 
  states: (F,M) = (1,1), (1,0), (1,-1), (0,0). As it moves through the cavity, 
  the evolution of its spin state is calculated by numerically solving the
  optical Bloch (differential) equations using the 4th-order Runge-Kutta
  method. This is a step-wise method which calculates the spin state at a 
  given step from the spin state at the previous step. At each step, the
  hyperfine transition frequency is the same, but the Rabi frequency is 
  not (see above). During this process the spin state is complex i.e. a 
  superposition of the 4 spin eigenstates. When the atom reaches the exit 
  of the cavity, then one of the 4 possible spin eigenstates is randomly 
  selected based on the complex spin state of the atom at the last step.

  This class handles the RF scan as well. When the user sets N scan steps,
  in reality N+1 different frequencies are simulated (because of the start + 
  end frequencies). Each frequency is one run in GEANT4 i.e. one output
  ROOT file, thus the output ROOT file contains the result of the last 
  frequency only. Therefore another "scan output ROOT file" is also created,
  which contains the scan-related parameters (start + end frequencies, 
  number of steps, etc.) plus the "detector hit trees" of each frequency 
  (saved in a TList). A TGraphErrors with the dummy detector hits vs. 
  frequency graph is also saved in this file. But please note that this 
  is not the real scan curve. For that the detector hit trees have to be 
  analyzed with the analysis ROOT macro.
  
  For the scan, the user tells how many particles to shoot for a frequency 
  point. This number can be fixed, or it can be random for each frequency. 
  In the latter case, when the user sets N particles, the real particle 
  number is obtained from a Gaussian distribution with mean N and sigma sqrt(N).
*/
class hbarFieldSetup
{

public:

  //  A zero field
  hbarFieldSetup(hbarDetConst*) ;               
  //  hbarFieldSetup(G4ThreeVector) ;  //  The value of the field
  ~hbarFieldSetup() ;
      
  //  G4ThreeVector GetConstantFieldValue();
  //  G4FieldManager*  GetLocalFieldManager() { return fLocalFieldManager ;}
  //! returns the equaton of motion
  hbarEqSextupoleField* GetEquationOfMotion(){return fEquation;}
  //! returns Genat4s field manager
  G4FieldManager*  GetFieldManager() { return fFieldManager ;}
  //  G4FieldManager*  GetS1FieldManager() { return fS1FieldManager ;}
  //  G4FieldManager*  GetS2FieldManager() { return fS2FieldManager ;}

  //! returns the magnetic field
  hbarField*       GetField() {return fMagneticField;}
  //  hbarField*       GetS1Field() {return fS1MagneticField;}
  //  hbarField*       GetS2Field() {return fS2MagneticField;}

  //! returns the state of the majorana transitions
  /*!
    \return is a G4string with values \b off \b weak or \b strong
   */
  G4String         GetAllowMajoranaTransitions() {return allowMajorana;};
  G4String         GetUseFieldLinesAngleDifference() {return useAngleDiff;};
  
  //G4String         IsMagnetFlipped(G4int) ;

  void SetStepperType( G4int i) { fStepperType = i ; }
  void SetStepper();
  //  void SetMinStep(G4double s) { fMinStep = s ; }
  //  void SetFieldValue(G4ThreeVector fieldVector) ;
  void SetMinimumStepLength(G4double);

  void CurrentSextupole(G4int num) {fMagneticField->CurrentSextupole(num);};

  void SetFieldMaxValue(G4double fieldValue) ;
  void SetZeroField(G4double fieldValue) ;
  
  //! sets the majorana transition state
  /*!
    \param val is a G4string with allowed values: \b off \b weak \b straong
   */
  void SetAllowMajoranaTransitions(G4String val) {allowMajorana = val;};
  // allowed values: off, weak, strong
  void SetUseFieldLinesAngleDifference(G4String val) {useAngleDiff = val;};

  //! inverts the magnetoc field of one magnet
  void SetFlipMagnet(G4int, G4String);
  //! inverts the magnetoc field of the currently active magnet
  void SetFlipMagnet(G4String);
  //! Set polenumber/2 for magnet
  void SetFieldHalfPoleNumber(G4int val);

  void UpdateField();
  //! performs the majorana transition for Antihydrogen
  void MakeMajoranaTransition(hbarHydrogenLike* aHbar, G4double anglediff);
  //! performs the majorana transition for Hydrogen
  template<class T>
  void MakeMajoranaTransition_templ(T aHbar, G4double anglediff);
protected:

  hbarDetConst*           hbarDetector;

  //G4FieldManager*         GetFieldManager() ;
  // Returns the Field Manager

  //  G4FieldManager*         fS1FieldManager ;
  //  G4FieldManager*         fS2FieldManager ;
  G4FieldManager*         fFieldManager ;
  G4FieldManager*         globalFieldMgr;

  //  G4ChordFinder*          fS1ChordFinder ;
  //  G4ChordFinder*          fS2ChordFinder ;
  G4ChordFinder*          fChordFinder ;

  //  hbarEqSextupoleField*       fS1Equation ; 
  //  hbarEqSextupoleField*       fS2Equation ; 
  hbarEqSextupoleField*       fEquation ; 
  //  G4Mag_UsualEqRhs*       fEquation ; 
  //  G4Mag_UsualEqRhs*       fLocalEquation ; 

  //  hbarField*              fS1MagneticField ; 
  //  hbarField*              fS2MagneticField ; 
  hbarField*              fMagneticField ; 
  G4UniformMagField*      fGlobalMagneticField;
  ///  G4MagneticField*        fMagneticField ; 

  //  G4MagIntegratorStepper* fS1Stepper ;
  //  G4MagIntegratorStepper* fS2Stepper ;
  G4MagIntegratorStepper* fStepper ;
  //  G4MagIntegratorStepper* fLocalStepper ;
  G4int                   fStepperType ;

  //  G4MagInt_Driver*        fS1IntgrDriver;
  //  G4MagInt_Driver*        fS2IntgrDriver;
  G4MagInt_Driver*        fIntgrDriver;

  G4double                fMinStep ;
  G4double                fMinMiss ;

  hbarFieldMess*          fFieldMessenger;   //!< pointer to the Field Messenger class

  G4String                allowMajorana;     //!< allow Majorana transitions (spin flips)
  G4String                useAngleDiff;      
  /*!< use difference between field line angles
    when calculating the Majorana transition probabilities
  */
};




#endif
