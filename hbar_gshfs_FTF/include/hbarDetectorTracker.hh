#ifndef hbarDetectorTracker_h
#define hbarDetectorTracker_h 1

#include "G4VSensitiveDetector.hh"
#include "hbarDetectorHit.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

//! Class for implementing the tracking detector, currently not used
class hbarDetectorTracker : public G4VSensitiveDetector
{

  public:
      hbarDetectorTracker(G4String name);
      ~hbarDetectorTracker();

      void Initialize(G4HCofThisEvent*HCE);
      G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
      void EndOfEvent(G4HCofThisEvent*HCE);
      void clear();
      void DrawAll();
      void PrintAll();

  private:
      hbarDetectorHitsCollection *trackerCollection;

  public:
};




#endif

