#ifndef hbarDetectorHit_h
#define hbarDetectorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

//! This class defines the structure of a detector hit event
/*!
  This class defines the structure of a detector hit event i.e. what 
  information to store when a particle hits a sensitive detector: time, 
  position, detector ID, kinetic energy, deposited energy, etc.
*/
class hbarDetectorHit : public G4VHit
{
public:

  hbarDetectorHit();
  ~hbarDetectorHit();
  hbarDetectorHit(const hbarDetectorHit &right);
  const hbarDetectorHit& operator=(const hbarDetectorHit &right);
  G4int operator==(const hbarDetectorHit &right) const;

  inline void *operator new(size_t);
  inline void operator delete(void *aHit);

  //! Draws the Hit with colour(1.,0.,0.) as circles in the pVVisManager.
  void Draw();
  //! Empty function
  void Print();

private:
  G4int         Event;       //!< store the event number
  G4int         Hit;         //!< is this a hit in the dummy detector (annihilation plate)
  G4double      Edep;        //!< deposited energy
  G4double      Ekin;        //!< kinetic energy
  G4double      Time;        //!< particle time
  G4ThreeVector Pos;         //!< X,Y and Z value of the hit in the Detector
  G4ThreeVector Momentumdir; //!< X,Y and Z component of the particle momentum
  G4String      Particle;    //!< the name of the particle as string
  G4int         PDGcode;
  G4String      Detector;    //!< the name of the Detector as string

  // tajima
  G4int         track_id;
  G4int         parent_id;

public:
  inline void SetEventNumber(G4int ev)
  { Event = ev; }
  inline G4int GetEventNumber()
  { return Event; }

  inline void SetHitNumber(G4int h)
  { Hit = h; }
  inline G4int GetHitNumber()
  { return Hit; }
  
  inline void SetPDGcode(G4int pdg)
  { 
  //std::cout << pdg << std::endl;
  PDGcode = pdg;
  //std::cout << PDGcode << std::endl;
  }
  
  inline G4int Get_PDGcode()
  { 
  //std::cout << PDGcode << std::endl;
  return PDGcode; 
  }

  inline void SetEnergyDeposit(G4double de)
  { Edep = de; }
  inline G4double GetEnergyDeposit()
  { return Edep; }

  inline void SetKineticEnergy(G4double ke)
  { Ekin = ke; }
  inline G4double GetKineticEnergy()
  { return Ekin; }

  inline void SetTime(G4double t)
  { Time = t; }
  inline G4double GetTime()
  { return Time; }

  inline void SetPosition(G4ThreeVector xyz)
  { Pos = xyz; }
  inline G4ThreeVector GetPosition()
  { return Pos; }

  //Added by Clemens -- start
  inline void SetMomentumdir(G4ThreeVector xyz)
  { Momentumdir = xyz; }
  inline G4ThreeVector GetMomentumdir()
  { return Momentumdir; }
  // Added by Clemens -- stop

  inline void SetParticleName(G4String name)
  { Particle = name; }
  inline G4String GetParticleName()
  { return Particle; }

  inline void SetDetectorName(G4String name)
  { Detector = name; }
  inline G4String GetDetectorName()
  { return Detector; }

  /*inline void SetPDGcode(G4int t_i)
  { PDGcode = t_i; }
  inline G4int GetPDGcode()
  { return PDGcode; }*/
  
  // tajima
  inline void Settrack_id(G4int t_i)
  { track_id = t_i; }
  inline G4int Gettrack_id()
  { return track_id; }

  inline void Setparent_id(G4int p_i)
  { parent_id = p_i; }
  inline G4int Getparent_id()
  { return parent_id; }



};

typedef G4THitsCollection<hbarDetectorHit> hbarDetectorHitsCollection;

extern G4Allocator<hbarDetectorHit> hbarDetectorHitAllocator;

inline void* hbarDetectorHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) hbarDetectorHitAllocator.MallocSingle();
  return aHit;
}

inline void hbarDetectorHit::operator delete(void *aHit)
{
  hbarDetectorHitAllocator.FreeSingle((hbarDetectorHit*) aHit);
}

#endif
