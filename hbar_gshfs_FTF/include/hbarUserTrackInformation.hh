#ifndef hbarUserTrackInformation_h
#define hbarUserTrackInformation_h 1

#include "G4VTrajectory.hh"
#include "G4Allocator.hh"
#include <stdlib.h>                 // Include from 'system'
#include "G4ios.hh"                 // Include from 'system'
#include <vector>                   // G4RWTValOrderedVector
#include "globals.hh"               // Include from 'global'
#include "G4ParticleDefinition.hh"  // Include from 'particle+matter'
#include "G4TrajectoryPoint.hh"     // Include from 'tracking'
#include "G4Track.hh"
#include "G4Step.hh"
#include "hbarTrackDictionaryEntry.hh"
#include <map>

/**
   In order to color the tracks correctly, we need to know properties of the particle in question.
   But the (anti-)hydrogen particle is not static as it should be. 
   Solution: in each step, store the quantum number of it in a dictionary. Ugly but should work.

*/
class hbarUserTrackInformation: public G4VUserTrackInformation
{
public:

  hbarUserTrackInformation(); ///Constructor.

  ~hbarUserTrackInformation(); ///Destructor.
  
  void DictionaryInsert(const G4Step * stepPointer, ///Step. Ownership not passed.
						hbarTrackDictionaryEntry toInsert ///To insert.
						); ///Insert a pointer and a quantum number in the dictionary.

  hbarTrackDictionaryEntry DictionaryLookup(const G4Step * stepPointer ///Pointer to step. Ownership not passed.
						 ) const; ///Look up a pointer in the dictionary.

  G4bool DictionaryHas(const G4Step * stepPointer ///Pointer to a step.
					   ) const; ///Check if it is in the dictionary.

  
  /**An entry in the track dictionary.
	 Self-descriptive.
   */

private:
  std::map<const G4Step *, hbarTrackDictionaryEntry> dictionary; ///Dictionary of step points and quantum numbers. \note the pointer should NEVER be dereferenced, comparision is by pointer only.

};

#endif
