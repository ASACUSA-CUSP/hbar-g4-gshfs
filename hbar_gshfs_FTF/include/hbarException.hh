#include <exception>
#include <string>

#include "G4RunManager.hh"

#ifndef hbarException_hh
#define hbarException_hh 1

using namespace std;

class hbarException : public exception
{
  
private:
  string msg; ///  The reason of the exception to be thrown.

public:
  hbarException(string m="exception!" /// The description of the reason for the exception to be thrown.
		    ); /// Constructor.

  ~hbarException() throw(); /// Destructor.
  const char* what() const throw(); /// Retrieves the reason of the 
};

#endif
