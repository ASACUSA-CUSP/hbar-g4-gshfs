#ifndef numtostr_h
#define numtostr_h

#include <stdlib.h>
#include <string>
#include <cmath>
#include <sstream>

//using namespace std;



std::string numtostr(int number, int decs = 1, char fillc = '0');

std::string numtostr(long number, int decs = 1, char fillc = '0');

std::string numtostr(double number, int decs = 3, bool force = false);

double GetOne(double val);


#endif





