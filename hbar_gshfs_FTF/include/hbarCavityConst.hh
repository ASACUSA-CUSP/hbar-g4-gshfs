#ifndef hbarCavityConst_hh
#define hbarCavityConst_hh 1

#include "hbarDetConst.hh"
#include "hbarDetMess.hh"
#include "hbarDetParam.hh"
#include "hbarDetectorSD.hh"
#include "hbarDetectorTracker.hh"
#include "hbarCUSPField.hh"
#include "hbarFieldSetup.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4RotationMatrix.hh"
#include "G4UserLimits.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include <G4PVDivision.hh>

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4Mag_UsualEqRhs.hh"
#include "G4ChordFinder.hh"

#include "hbarCavityField.hh"
#include "hbarCavityMess.hh"

class hbarCavityMess;

class hbarCavityConst
{
public:
  hbarCavityConst(hbarMWSetup * );
  ~hbarCavityConst();
  void MakeCavity(G4VPhysicalVolume *);

  void SetMWCavityLength(G4double val) {MWCavityLength = val;};
  void SetMWCavityInnerDiameter(G4double val) {MWCavityInnerDiam = val;};
  void SetMWCavityOuterDiameter(G4double val) {MWCavityOuterDiam = val;};
  void SetMWCavityCenter(G4double val) {MWCavityCenter = val;};
  void SetMWCavityResonatorThickness(G4double val) {MWCavityResThick = val;};
  void SetMWCavityResonatorWidth(G4double val) {MWCavityResWidth = val;};
  void SetMWCavityResonatorSeparation(G4double val) {MWCavityResSep = val;};
  
  void SetHelmholtzCoilsInnerDiam(G4double val) {HelmholtzCoilsInnerDiam = val;};
  void SetHelmholtzCoilsOuterDiam(G4double val) {HelmholtzCoilsOuterDiam = val;};
  void SetHelmholtzCoilThickness(G4double val) {HelmholtzCoilThickness = val;};
  void SetHelmholtzCoilDistance(G4double val) {HelmholtzCoilDistance = val;};

  void SetSupportRingHHOuterDiam(G4double val) {SupportRingHHOuterDiam = val;};
  void SetSupportRingHHOuterThick(G4double val) {SupportRingHHOuterThick = val;};
  
  void SetUseCavityInnerShielding(G4bool val) {UseCavityInnerShielding = val;};
  void SetUseCavityMiddleShielding(G4bool val) {UseCavityMiddleShielding = val;};
  void SetUseHelmholtzCoils(G4bool val) {UseHelmholtzCoils = val;};
  void SetUseSupportRingsHH(G4bool val) {UseSupportRingsHH = val;};
  
  void SetShieldingThickness(G4double val) {ShieldingThickness = val;};
  void SetInnerLayerShieldLength(G4double val) {InnerLayerShieldLength = val;};
  void SetInnerLayerShieldHight(G4double val) {InnerLayerShieldHight = val;};
  void SetMiddleLayerShieldLength(G4double val) {MiddleLayerShieldLength = val;};
  void SetMiddleLayerShieldHight(G4double val) {MiddleLayerShieldHight = val;};
  void SetShieldHoleDiam(G4double val) {ShieldHoleDiam = val;};

  G4double GetMWCavityLength() {return MWCavityLength;};
  G4double GetMWCavityInnerDiameter() {return MWCavityInnerDiam;};
  G4double GetMWCavityOuterDiameter() {return MWCavityOuterDiam;};
  G4double GetMWCavityCenter() {return MWCavityCenter;};
  G4double GetMWCavityResonatorWidth() {return MWCavityResWidth;};
  G4double GetMWCavityResonatorThickness() {return MWCavityResThick;};
  G4double GetMWCavityResonatorSeparation() {return MWCavityResSep;};

  void SetCavityFieldFile(G4String filename){cavityFieldfilename = filename;};
  void SetUseCavityFieldMap(G4bool useflag) {useCavityFieldmap = useflag;};
  G4bool GetUseCavityFieldMap() 			{return useCavityFieldmap;};
  hbarCavityField* GetcavityFieldmap()		{return cavityField;};
  hbarMWSetup* GetMWSetup() {return mwSetup;};

private:

  hbarMWSetup*       mwSetup;


  G4Box*				InnerInnerBox;
  G4Box*				InnerOuterBox;
  G4Box*				MiddleInnerBox;
  G4Box*				MiddleOuterBox;
  G4VSolid*				InnerLayerShieldSol;
  G4VSolid*				InnerLayerShieldBox;    
  G4VSolid*				MiddleLayerShieldSol;
  G4VSolid*				MiddleLayerShieldBox; 
    
  G4LogicalVolume*		InnerLayerShieldLog;
  G4LogicalVolume*		MiddleLayerShieldLog;
  G4VPhysicalVolume*	InnerLayerShieldPhys;
  G4VPhysicalVolume*	MiddleLayerShieldPhys;
  
  
  
  G4Box*             Res1Sol;
  G4LogicalVolume*   Res1Log;
  G4VPhysicalVolume* Res1Phy;

  G4Box*             Res2Sol;
  G4LogicalVolume*   Res2Log;
  G4VPhysicalVolume* Res2Phy;




  G4String cavityFieldfilename;
  G4bool useCavityFieldmap; 
  G4VSolid*          CavitySol;
  G4LogicalVolume*   CavityLog;
  G4VPhysicalVolume* CavityPhy;
 

  G4double				CavityWingsLength;
  G4double				CavityWingsWidth;
  G4double				CavityWingsThickness;
  G4Box*				CavityWingsSol;
  G4LogicalVolume*		CavityWingsLog;
  G4VPhysicalVolume*	CavityWingsPhys1;
  G4VPhysicalVolume*	CavityWingsPhys2;

  hbarCavityField* cavityField;


  G4double				ShieldHoleDiam;
  G4Tubs*				ShieldHoleSolid;
  G4LogicalVolume*		ShieldHoleLog;
  G4VPhysicalVolume*	ShieldHolePhys;
  


  hbarCavityMess * cavityMess;

  G4bool				UseCavityInnerShielding;
  G4bool				UseCavityMiddleShielding;
  G4bool				UseHelmholtzCoils;
  G4bool				UseSupportRingsHH;


  G4double     MWCavityLength;      //!< total length of the cavity
  G4double     MWCavityInnerDiam;   //!< inner diameter of the cavity
  G4double     MWCavityOuterDiam;   //!< outer diameter of the cavity
  G4double     MWCavityCenter;      //!< center of the cavity
  G4double     MWCavityResWidth;    //!< cavity resonator strip width
  G4double     MWCavityResSep;      //!< cavity resonator strip separation
  G4double     MWCavityResThick;    //!< cavity resonator strip thickness


  G4double				MWCavityInnerRingLength;
  G4double				MWCavityInnerRingThickness;
  G4double				MWCavityOuterRingThickness;
  
  G4double				MWCavityPlateHoleDiam;
  G4double				MWCavityPlateDiam;
  G4Tubs*				MWCavityPlateSol;
  G4LogicalVolume*		MWCavityPlateLog;
  G4VPhysicalVolume*	MWCavityPlatePhys1;
  G4VPhysicalVolume*	MWCavityPlatePhys2;

  G4double				HelmholtzCoilsInnerDiam;
  G4double				HelmholtzCoilsOuterDiam;
  G4double				HelmholtzCoilThickness;
  G4double				HelmholtzCoilDistance;
    
  G4Tubs*				HelmholtzCoilSol;
  G4LogicalVolume*		HelmholtzCoilLog;
  G4VPhysicalVolume* 	HelmholtzCoilPhys1;
  G4VPhysicalVolume* 	HelmholtzCoilPhys2;
   
  G4double				SupportRingHHInnerDiam;
  G4double				SupportRingHHMidDiam;
  G4double				SupportRingHHOuterDiam;
  G4double				SupportRingHHOuterThick;

  G4Tubs*				SupportRingHHSol;
  G4LogicalVolume*		SupportRingHHLog;
  G4VPhysicalVolume*	SupportRingHHPhys1;
  G4VPhysicalVolume*	SupportRingHHPhys2;
  G4Tubs*				SupportRingHHOuterSol;
  G4LogicalVolume*		SupportRingHHOuterLog;
  G4VPhysicalVolume*	SupportRingHHOuterPhys1;
  G4VPhysicalVolume*	SupportRingHHOuterPhys2;
  G4VPhysicalVolume*	SupportRingHHOuterPhys3;
  G4VPhysicalVolume*	SupportRingHHOuterPhys4;
  
  G4double 				InnerLayerShieldLength;
  G4double				InnerLayerShieldHight;
  G4double 				MiddleLayerShieldLength;
  G4double				MiddleLayerShieldHight;
  G4double				ShieldingThickness;
};


#endif
