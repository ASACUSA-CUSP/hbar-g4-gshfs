#ifndef hbarEqSextupoleField_hh
#define hbarEqSextupoleField_hh

//#include "/home/tajima/hbar_gshfs/include/G4EquationOfMotion.hh"
#include "G4EquationOfMotion.hh"
#include "hbarCUSPField.hh"
#include "hbarSextupoleField.hh"
#include "hbarField.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
class G4ParticleDefinition;   // Added by Berti

//! Calculate the force on a particle traversing the sextupole field
/*!
  This equation of motion class, or more precisely its ::EvaluateRhsGivenB() 
  function, calculates the force acting on a particle. It is based on the 
  G4Mag_UsualEqRhs class. The force which it calculates is not a force in 
  the common sense but it is force*(c/velocity). (I have no idea why GEANT4 
  calculates the force like that.) It calculates the normal Lorentz force 
  acting on a charged particle in a magnetic field, plus in case of 
  antihydrogen the additional force acting on the magnetic moment in an 
  inhomogeneous magnetic field. More explanation can be found in its source 
  code.
 */
class hbarEqSextupoleField : public G4EquationOfMotion
{
public: 

  //! constructor
  hbarEqSextupoleField(G4Field *hbarField )
    : G4EquationOfMotion( hbarField ) {;}
  
  //! destructor
  ~hbarEqSextupoleField() {;} 
  
  //! passes the charge, magnetic moment, magnetic charge and electric moment
  /*!
    G4Chargestats is sine Geant4.10 a way of passing information on charge,
    magnetic moment etc. to the equation of motion. The main problem hereby is
    that this information is taken fom the constants defined in the 
    particle definiton, thus not allowing to have a magnetic moment depending
    on external fields.
   */
  void SetChargeMomentumMass(G4ChargeState particleCharge, // in e+ units
			     G4double MomentumXc,
			     G4double MassXc2);
  
  
  //! calculates the force
  /*!
    Given the value of the electromagnetic field, this function 
    calculates the value of the derivative dydx.

    - y[0-2]       spatial coordinates
    - y[3-5]       momentum * c
    - y[7]         time in lab frame

    - Field[0-2]   B field
    - Field[3-5]   E field
    - Field[6-8]   grad(B)
    - Field[9-11]  grad(E)
    - Field[12]    particle magnetic moment depending on external B
    - Field[13-15] grad(mu) depending on external B

    - dydx[0-2]    (d/ds)X = Vx/V = Px/P
    - dydx[3-5]    force on the particle * (c/velocity)
    - dydx[7]      inverse velocity (actually, c/v)

    Field[12-15] is nessesary because G4ChargeState has no support for
    changing magnetic moments with the external field and also no way to get
    the gradient of the magnetic moment.

    @param y physical information about the particle
    @param Field information on the different fields
    @param dydx derivatives of y
  */
  void EvaluateRhsGivenB(const G4double y[],
			 const G4double Field[],
			 G4double dydx[] ) const;
  
private:
  
  G4double        fMassCof;  //!< square of invariant mass
  G4double        mu;        //!< intermediate storage for magnetic moment
  G4double        charge;    //!< intermediate storage of charge
};

#endif
