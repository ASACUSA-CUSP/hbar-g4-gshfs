#!/usr/bin/python
import sys
import os

def read_cc_file(filename):
    entry = []
    with open(filename,'r') as f:
        command = ""
        bufferstring = ""
        for line in f:
            if "//" in line:
                continue
            if ("G4UIdirectory" in line) and bufferstring != "":
                entry.append(bufferstring)
                bufferstring = ""
            if " new G4UIcmd" in line:
                if bufferstring != "":
                    entry.append(bufferstring)
                    bufferstring = ""
                command = line[line.find('"')+1:line.rfind('"')]
                bufferstring += " \\b " + command + ":\\n"
            if "SetGuidance" in line:
                #bufferstring += "\\a " + "Description:\n\t"
                bufferstring += "\t"
                command = line[line.find('"')+1:line.rfind('"')]
                bufferstring += command + "\\n"
            if "SetDefaultValue" in line:
                bufferstring += " \\a default \\a value: "
                if(line.find('"') != -1):
                    command = line[line.find('"')+1:line.rfind('"')]
                else:
                    command = line[line.find('(')+1:line.find(')')]
                bufferstring += command + "\\n"
            if "SetCandidates" in line:
                bufferstring += " \\a available \\a candidates: "
                command = line[line.find('"')+1:line.rfind('"')]
                bufferstring += command + "\\n"
            if "SetRange" in line:
                bufferstring += " \\a parameter \\a range: "
                command = line[line.find('"')+1:line.rfind('"')]
                bufferstring += command + "\\n"
            if "SetDefaultUnit" in line:
                bufferstring += " \\a default \\a unit: "
                command = line[line.find('"')+1:line.rfind('"')]
                bufferstring += command + "\\n"

        entry.append(bufferstring)
    return entry

if __name__ == "__main__":
    """ This script reads sourcecode files for geant4 messenger classes
    and generates a file called "macro.dox" that contains all the information
    in a doxygen readable form. """
    
    messengerlist = []
    for dirname, dirnames, filenames in os.walk('./src'):
        #for subdirname in dirnames:
           # print os.path.join(dirname, subdirname)
        for filename in filenames:
            #print os.path.join(dirname, filename)
            if ("Mess" in os.path.join(dirname, filename)) \
                    and ("hbar" in os.path.join(dirname, filename)):
                messengerlist.append(os.path.join(dirname, filename))

    entries = []
    for i in messengerlist:
        entry = read_cc_file(i)
        for j in entry:
            entries.append(j)
    entries.sort()

    ofile = open("./doc/macro.dox","w")
    ofile.write("/*!\n")
    ofile.write(" \\mainpage The Macro Documentation\n\\n")
    for i in entries:
        if "/" in i:
            ofile.write(i+"\\n")

    ofile.write("*/\n")
