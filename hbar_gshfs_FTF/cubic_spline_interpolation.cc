#include <iostream>
#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <fstream>
#include <tr1/memory>
#include "include/KDNode.hh"

#define columns2 4
#define huge 1.0e99
#define DIM2 1

using namespace std;
//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\
// program to spline interpolate a fieldmap with the following structure:
//	x	y	z	Bx	By	Bz	0	0	0
// One needs to set the file names of the out- and input file in the main-program below.
// between every two points on the original fieldmap e.g. in z-direction, there will be one interpolated point.
// This 1 dim. interpolation will be done for x,y,z direction
//
// to compile, run: g++ cubic_spline_interpolation.cc ./src/KDNode.cc 
//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\//°\\

//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
double Distance(double* x, double* y) {
	double distance=0;
	for(int i=0; i<DIM2; i++) distance += (x[i]-y[i])*(x[i]-y[i]);
	distance = sqrt(distance);
	return distance;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> Approx_Neighbour2(std::tr1::shared_ptr<KDNode> current, double* x) {
	if(current->GetRightChild().get()==NULL) return current; // if end of tree is reached, return node
	if(current->GetRightChild()->Getchecked() && current->GetLeftChild()->Getchecked()) current->Setchecked(true); // if both children are checked, set node.checked 1

	//if point[Axis] of current node is smaller and the right child is not checked, then search branch of right child:
	if(current->GetPoint(current->GetAxis())<x[current->GetAxis()]&&(!current->GetRightChild()->Getchecked())) return Approx_Neighbour2(current->GetRightChild(),x);//if left child is checked, search branch of right child: 									 		                           
	else if(current->GetLeftChild()->Getchecked()) return Approx_Neighbour2(current->GetRightChild(),x);
	//else, search branch of left child:
	else return Approx_Neighbour2(current->GetLeftChild(),x);
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> Check_Sphere2(std::tr1::shared_ptr<KDNode> node, std::tr1::shared_ptr<KDNode> approx, double* x) {
	double dist = Distance(approx->GetPoint(),x);		 // calculate distance from point saved in approx and x, the point we are looking for

	if(node->GetLeftChild()==NULL)  {
		if(Distance(node->GetPoint(),x)<dist) approx = node;
		return approx;       //return approx at leaf 
	}
	int axis = node->GetAxis();
	if(fabs(node->GetPoint(axis)-x[axis])<dist) {   //check if hyperplane intersects hypersphere
		std::tr1::shared_ptr<KDNode> nright(Check_Sphere2(node->GetRightChild(),approx,x));		// if yes, then search branches for intersection
		std::tr1::shared_ptr<KDNode> nleft(Check_Sphere2(node->GetLeftChild(),approx,x));	

		if(nright->Getchecked()&&nleft->Getchecked()) node->Setchecked(true);

		if(nright->Getchecked()) return nleft;
		if(nleft->Getchecked()) return nright;
		
		if(Distance(nright->GetPoint(),x)<Distance(nleft->GetPoint(),x)) return nright; // if distance between nright and x is smaller
		else return nleft;																
	}
	else {
		// if there is no intersection, check whether component point[Axis] of current node is larger or smaller than x[Axis]...
		if((node->GetRightChild()->Getchecked())&&(node->GetLeftChild()->Getchecked())) node->Setchecked(true);
		if(node->GetPoint(axis)<x[axis]&&!(node->GetRightChild()->Getchecked())) return Check_Sphere2(node->GetRightChild(),approx,x); 
		else if(node->GetLeftChild()->Getchecked()) return Check_Sphere2(node->GetRightChild(),approx,x);
		else return Check_Sphere2(node->GetLeftChild(),approx,x); //... and search corresponding branch
	}
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> Nearest_Neighbour2(std::tr1::shared_ptr<KDNode> tree, double* value) {	
	std::tr1::shared_ptr<KDNode> approx =std::tr1::shared_ptr<KDNode>(new KDNode());
	approx = Approx_Neighbour2(tree,value); // calculate approximate neighbour
	return Check_Sphere2(tree,approx,value); // return node after sphere is checked
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void MergeSort2(double** matrixtosort, int length, int curraxis) {
	if(length>1) {
		
		int elementsInA1 = length/2;
		int elementsInA2 = ((length%2)==1) ? elementsInA1+1 : elementsInA1;
	
		double** arr1 = new double*[elementsInA1];
		for(int i=0; i<elementsInA1; i++) arr1[i] = new double[columns];
		double** arr2 = new double*[elementsInA2];
		for(int i=0; i<elementsInA2; i++) arr2[i] = new double[columns];

		for(int i=0; i<elementsInA1; i++) for(int j=0;j<columns; j++) arr1[i][j] = matrixtosort[i][j];
		for(int i=elementsInA1; i<elementsInA1+elementsInA2; i++) for(int j=0;j<columns; j++) arr2[i-elementsInA1][j] = matrixtosort[i][j];
			
		MergeSort2(arr1,elementsInA1,curraxis);
		MergeSort2(arr2,elementsInA2,curraxis);

		int i = 0, j = 0, k = 0;
		while(elementsInA1!=j && elementsInA2!=k) {		
			if(arr1[j][curraxis] < arr2[k][curraxis]) {			
				for(int l=0;l<columns2; l++) matrixtosort[i][l] = arr1[j][l];
				i++;
				j++;
			}
			else {
				for(int l=0;l<columns2; l++) matrixtosort[i][l] = arr2[k][l];
				i++;
				k++;
			}
		}
		while(elementsInA1 != j) {
			for(int l=0;l<columns2; l++) matrixtosort[i][l] = arr1[j][l];
			i++;
			j++;
		}
		while(elementsInA2 != k) {
			for(int l=0;l<columns2; l++) matrixtosort[i][l] = arr2[k][l];
			i++;
			k++;
		}
		
		for(int i=0; i<elementsInA1; i++) delete [] arr1[i];
		delete [] arr1;
		for(int i=0; i<elementsInA2; i++) delete [] arr2[i];
		delete [] arr2;
		
	}
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
//Find y'' 
//This routine stores an array y2[0..n-1] with second derivatives of the interpolating function at the tabulated points pointed to by xv,
// using function values pointed to by yv.If yp1 and/or ypn are equal to 1 10^99 or larger, the routine is signaled to set the corresponding 
//boundary condition for a natural spline, with zero second derivative on that boundary; otherwise, they are
//the values of the first derivatives at the endpoints... (based on function from numerical recipies)
/*void sety2(double** y2, int numberofy2, const double *xv, const double *yv, double yp1, double ypn) {
	int i,k;
	double p,qn,sig,un;
	int n=numberofy2;
	double u[n-1];
	if(yp1 > 0.99e99) y2[0][0]=u[0]=0.0;
	else {
		y2[0][0] = -0.5;
		u[0]=(3.0/(xv[2*1]-xv[0]))*((yv[2*1]-yv[0])/(xv[2*1]-xv[0])-yp1);
	}
	for(i=1;i<n-1;i++) { // decomposition loop of tridiagonal algorithm
		sig=(xv[2*i]-xv[2*(i-1)])/(xv[2*(i+1)]-xv[2*(i-1)]);
		p=sig*y2[i-1][0]+2.0;
		y2[i][0]=(sig-1.0)/p;
		u[i]=(yv[2*(i+1)]-yv[2*i])/(xv[2*(i+1)]-xv[2*i])-(yv[2*i]-yv[2*(i-1)])/(xv[2*i]-xv[2*(i-1)]);
		u[i]=(6.0*u[i]/(xv[2*(i+1)]-xv[2*(i-1)])-sig*u[i-1])/p;
	}
	if(ypn > 0.99e99) qn=un=0.0;
	else {
		qn=0.5;
		un=(3.0/(xv[2*(n-1)]-xv[2*(n-2)]))*(ypn-(yv[2*(n-1)]-yv[2*(n-2)])/(xv[2*(n-1)]-xv[2*(n-2)]));
	}
	y2[n-1][0]=(un-qn*u[n-2])/(qn*y2[n-2][0]+1.0);
	for(k=n-2;k>=0;k--) y2[k][0]=y2[k][0]*y2[k+1][0]+u[k]; // backsubstitution loop of the tridiag. algorithm
}*/
void sety2(double** y2, int numberofy2, double** yzval2, double yp1, double ypn) {	
	int i,k;
	double p,qn,sig,un;
	int n=numberofy2;
	double u[n-1];
	if(yp1>0.99e99) y2[0][0] = u[0] = 0.0;	
	else {
		y2[0][0] = -0.5;
		u[0]=(3.0/(yzval2[1][0]-yzval2[0][0]))*((yzval2[1][1]-yzval2[0][1])/(yzval2[1][0]-yzval2[0][0])-yp1);
	}
	for(i=1;i<n-1;i++) { // decomposition loop of tridiagonal algorithm
		sig=(yzval2[i][0]-yzval2[i-1][0])/(yzval2[i+1][0]-yzval2[i-1][0]);
		p=sig*y2[i-1][0]+2.0;
		y2[i][0]=(sig-1.0)/p;
		u[i]=(yzval2[i+1][1]-yzval2[i][1])/(yzval2[i+1][0]-yzval2[i][0])-(yzval2[i][1]-yzval2[(i-1)][1])/(yzval2[i][0]-yzval2[i-1][0]);
		u[i]=(6.0*u[i]/(yzval2[i+1][0]-yzval2[i-1][0])-sig*u[i-1])/p;
	}
	if(ypn>0.99e99) qn = un = 0.0;
	else {
		qn=0.5;
		un=(3.0/(yzval2[(n-1)][0]-yzval2[(n-2)][0]))*(ypn-(yzval2[(n-1)][1]-yzval2[(n-2)][1])/(yzval2[(n-1)][0]-yzval2[(n-2)][0]));
	}
	y2[n-1][0]=(un-qn*u[n-2])/(qn*y2[n-2][0]+1.0);
	for(k=n-2;k>=0;k--) y2[k][0]=y2[k][0]*y2[k+1][0]+u[k]; // backsubstitution loop of the tridiag. algorithm
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
// this function returns the cubic spline interpolated value y
double rawinterp(double* lowNN, double* highNN, double x, double** y2, int sizeofy2) {
	double y,h,b,a;
	double y2low=0, y2high=0;
	h=highNN[0]-lowNN[0];
	if(h == 0.0) throw("Bad input in rawinterp");
	a=(highNN[0]-x)/h;
	b=(x-lowNN[0])/h;
	//set y2 for high and low NN
	for(int i=0;i<sizeofy2;i++) {
		if(y2[i][1]==lowNN[0]) y2low = y2[i][0];
		if(y2[i][1]==highNN[0]) y2high = y2[i][0];
	}
	y=a*lowNN[1]+b*highNN[1]+((a*a*a-a)*y2low+(b*b*b-b)*y2high)*(h*h)/6.0;
	return y;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
// two of the three coordinates are fixed, the other one gets spline interpolated
// out File ... output file
// numberoflines ... number of lines in original fieldmap file
// axisx and axisy ... coordinates kept fixed
// axisz ... coordinate points which get spline interpolated
// valuesx ... array with 'countx' entries
// valuesy ... array with 'county' entries
void one_dim_spline(fstream &outFile, int numberoflines, int axisx, int axisy, int axisz, double** MagField, double* valuesx, int countx, double* valuesy, int county) {	
	double yp1=huge; // boundary values of y2 (=huge corresponds to natural spline interpolation, so y2 will be set to zero at the boundaries)
    double ypn=huge;
	int countxval=0; // count values of third coord. with two other coord. fixed
	int k=0;
	double zinter = 0; // coordinate point (1-dim) which is not on the fieldmap, where mag. field gets interpolated
	double *pointzinter;
	double* NN1 = new double[columns]; // Nearest neighbour 1 and 2
	double* NN2 = new double[columns];
	for(int i=0;i<columns;i++) NN1[i]=0;
	for(int i=0;i<columns;i++) NN2[i]=0;
	int currentindex = -2;

	for(int ix=0;ix<countx;ix++) { // loops over 2 coordinates, fixed for every ix/iy
		for(int iy=0;iy<county;iy++) {
			currentindex = -2;
			while(currentindex!=countxval-1) { // while last value to interpolate is not reached
				if(currentindex==-2) currentindex=currentindex+2;		
				countxval=0;
				for(int i=0; i<numberoflines; i++) { // compute 'countxval'
					if(MagField[i][axisx]==valuesx[ix] && MagField[i][axisy]==valuesy[iy]) {
						countxval++;
					}
				}
				if(countxval==0 || countxval==1) break;
				
				double** yzval = new double*[countxval]; // array with values of third coord. and mag.field
				for(int l=0; l<countxval; l++) yzval[l] = new double[columns];
			
				double** y2 = new double*[countxval]; // contains points and their second derivates
				for(int l=0; l<countxval; l++) y2[l] = new double[2];
		
				k=0;
				for(int i=0; i<numberoflines; i++) {
					if(MagField[i][axisx]==valuesx[ix] && MagField[i][axisy]==valuesy[iy]) {
						yzval[k][0] = MagField[i][axisz];
						yzval[k][1] = MagField[i][3];
						k++;		
					}	
				}	
				std::tr1::shared_ptr<KDNode> tree(new KDNode(yzval,countxval,0)); // kd tree with yzval
				std::tr1::shared_ptr<KDNode> nn_node(new KDNode()); // node to temporary store nearest neighbours nodes
				MergeSort2(yzval,countxval,0);	
				zinter = yzval[currentindex][0]+fabs((yzval[currentindex+1][0]-yzval[currentindex][0])/2);
				
				pointzinter = &zinter; // pointer to zinter because method Nearest_Neighbour expects a pointer
				// find two nearest neigbours:
				nn_node = Nearest_Neighbour2(tree, pointzinter);
				nn_node->Setchecked(true);
				NN1 = nn_node.get()->GetPoint();	
				nn_node = Nearest_Neighbour2(tree, pointzinter);
				NN2 = nn_node.get()->GetPoint();
				// set y'':		
				sety2(y2,countxval,yzval,yp1,ypn);
				for(int i=0;i<countxval;i++) y2[i][1] = yzval[i][0];

				// calculate interpolated value:  
				double yinter = rawinterp(NN1,NN2,zinter,y2,countxval);
				
				// file output:
				if(axisx==0) outFile << valuesx[ix] << "	";
				else if(axisy==0) outFile << valuesy[iy] << "	";
				else if(axisz==0) outFile << zinter << "	";
				
				if(axisx==1) outFile << valuesx[ix] << "	";
				else if(axisy==1) outFile << valuesy[iy] << "	";
				else if(axisz==1) outFile << zinter << "	";
				
				if(axisx==2) outFile << valuesx[ix] << "	";
				else if(axisy==2) outFile << valuesy[iy] << "	";
				else if(axisz==2) outFile << zinter << "	";
							
				outFile << yinter << "	" << 0 << "	" << 0 << "	" << 0 << "	" << 0 << "	" << 0 <<endl;				
				currentindex++;
				
				for(int i=0; i<countxval; i++) delete [] yzval[i];
				delete [] yzval;
				
				for(int i=0; i<countxval; i++) delete [] y2[i];
				delete [] y2;			
			}
		}
	}
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
int main() {	
	char filename[] = "simulated_cavity_fieldmap.dat";
	std::ifstream inFile(filename); 
	if (!inFile) std::cout << "Error in opening file: " << filename << std::endl; 
	int numberoflines=0;
	// count number of lines in file
	numberoflines = std::count(std::istreambuf_iterator<char>(inFile), 
	std::istreambuf_iterator<char>(), '\n');
	numberoflines--;
	inFile.clear(); // clear eof flag
	inFile.seekg(0, ios::beg); // go back to firstline	
	
	double** MagField = new double*[numberoflines];
	for(int i=0; i< numberoflines; i++) MagField[i] = new double[4];
	double x=0,y=0,z=0,Bx=0,By=0,Bz=0;
	double B=0;
	int lcount=0;
	double dump=0;  

	fstream outFile; // output file to store new fieldmap
    outFile.open("spline_points.dat", ios::out);

	while(!inFile.eof() && lcount != numberoflines) {
		inFile >> x >> y >> z >> Bx >> By >> Bz >> dump >> dump >> dump;
		MagField[lcount][0] = x;
		MagField[lcount][1] = y;
		MagField[lcount][2] = z;
		B = sqrt(Bx*Bx+By*By+Bz*Bz);
		MagField[lcount][3] = B;
		outFile << x << "	" << y << "	" << z << "	" << B << "	" << 0 << "	" << 0 << "	" << 0 <<  "	" << 0 << "	" << 0 <<endl;
		lcount++; 
	}
    inFile.close();
    
    // count how many different x,y,z values there are on the fieldmap
	int countx = 0;		
    int county = 0;
    int countz = 0;
    MergeSort2(MagField,numberoflines, 0); 
    for(int i=1;i<numberoflines-1;i++) if(MagField[i][0]!=MagField[i-1][0]) countx++;
	countx++;

	MergeSort2(MagField, numberoflines, 1);
	for(int i=1;i<numberoflines-1;i++) if(MagField[i][1]!=MagField[i-1][1]) county++;
	county++;
	
	MergeSort2(MagField, numberoflines, 2);
	for(int i=1;i<numberoflines-1;i++) if(MagField[i][2]!=MagField[i-1][2]) countz++;	
	countz++;
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX
	 double valuesx[countx]; // array with all different values of x-coordiantes
	 int count=1;
	 MergeSort2(MagField, numberoflines, 0);
	 valuesx[0]=MagField[0][0];
	 for(int i=1;i<numberoflines-1;i++) {
		if(MagField[i][0]!=MagField[i-1][0]) {
			valuesx[count] = MagField[i][0];
			count++;
		}
	}
	double valuesy[county];
		 count=1;
		 MergeSort2(MagField, numberoflines, 1);
		 valuesy[0]=MagField[0][1];
		 for(int i=1;i<numberoflines-1;i++) {
			if(MagField[i][1]!=MagField[i-1][1]) {
				valuesy[count] = MagField[i][1];
				count++;
			}
		}	
	double valuesz[countz];
		 count=1;
		 MergeSort2(MagField, numberoflines, 2);
		 valuesz[0]=MagField[0][2];
		 for(int i=1;i<numberoflines-1;i++) {
			if(MagField[i][2] != MagField[i-1][2]) {
				valuesz[count] = MagField[i][2];
				count++;
			}
		}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOX

	one_dim_spline(outFile,numberoflines,0,1,2,MagField,valuesx,countx,valuesy,county);
	one_dim_spline(outFile,numberoflines,0,2,1,MagField,valuesx,countx,valuesz,countz);
	one_dim_spline(outFile,numberoflines,1,2,0,MagField,valuesy,county,valuesz,countz);

	for(int i=0; i<numberoflines; i++) delete [] MagField[i];
	delete [] MagField;
	
	return 0;	
}
