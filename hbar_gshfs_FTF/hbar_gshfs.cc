/**
   \file hbar_gshfs.cc
   \brief Main simulation file
   \author ASACUSA. For information contact Bertalan Juhasz (Bertalan.Juhasz@cern.ch).

 */

#include <ctime>

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
//#include "G4UIXm.hh"
#include "Randomize.hh"
#include "G4ParticleTable.hh"

#include "G4PhysListFactory.hh"
#include "G4VModularPhysicsList.hh"
#include "G4EmStandardPhysicsSS.hh"
#include "G4EmUserPhysics.hh"


#include "hbarDetConst.hh"
#include "hbarPhysicsList.hh"
#include "hbarPrimGenAction.hh"
#include "hbarRunAction.hh"
#include "hbarEventAction.hh"
#include "hbarStepAction.hh"
#include "hbarTrackAction.hh"
#include "hbarStackAction.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarMWSetup.hh"
#include "hbarSteppingVerbose.hh"

#include <iostream>
#include <fstream>

//! Main function. 
int main(int argc, //! # arguments
		 char** argv //! The actual arguments
		 ) 
{

  // get random seed with hardware entropy
  std::ifstream randseedgen;
  unsigned int devrand;
  randseedgen.open("/dev/random", std::ios::binary);
  if(randseedgen.is_open()){
    char *mem = new char [sizeof(int)];
    randseedgen.read(mem,sizeof(int));
    devrand = *reinterpret_cast<int*>(mem);
    delete [] mem;
  }
  else
    devrand = 0;
  randseedgen.close();
  unsigned int seconds = std::time(NULL); // current time in seconds since the epoch

  // choose the Random engine
  //  RanecuEngine theNewEngine;
  // initilaize the engine with the current time as seed
  CLHEP::HepJamesRandom theNewEngine(seconds xor devrand);
  CLHEP::HepRandom::setTheEngine(&theNewEngine);
  //  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  
  // User Verbose output class

  /* Rikard says no.
  G4VSteppingVerbose* verbosity = new hbarSteppingVerbose;
  G4VSteppingVerbose::SetInstance(verbosity);
  */
  //
  
  // Construct the default run manager
  G4RunManager * runManager = new G4RunManager;
  
  
  // set mandatory initialization classes
  hbarDetConst* detector = new hbarDetConst();
  runManager->SetUserInitialization(detector);
  
  
  // normal hbar physics list:
  //hbarPhysicsList* physics = new hbarPhysicsList;
  //runManager->SetUserInitialization(physics);
  
  // Helga Physics List version:
  G4PhysListFactory factory;
  G4VModularPhysicsList* phys = factory.GetReferencePhysList("FTFP_BERT");
  //G4VModularPhysicsList* phys = factory.GetReferencePhysList("FTF_BIC");
  //phys->ReplacePhysics(new G4EmStandardPhysicsSS()); // single scattering
  //phys->RegisterPhysics(new G4EmUserPhysics());  
  runManager->SetUserInitialization(phys);
  
  // set user action classes
  hbarPrimGenAction* aPrimGenAction = new hbarPrimGenAction(detector);
  runManager->SetUserAction(aPrimGenAction);
  hbarRunAction* aRunAct = new hbarRunAction(detector);
  aRunAct->ConnectPrimGenAction(aPrimGenAction);
  runManager->SetUserAction(aRunAct);

  detector->GetCavityConst()->GetMWSetup()->ConnectRunAction(aRunAct);
  hbarEventAction* event = new hbarEventAction(aRunAct);
  runManager->SetUserAction(event);
  hbarStepAction* step = new hbarStepAction(aRunAct);
  runManager->SetUserAction(step);
  hbarTrackAction* track = new hbarTrackAction;
  runManager->SetUserAction(track);
  hbarStackAction* stack = new hbarStackAction;
  runManager->SetUserAction(stack);
  
  //Initialize G4 kernel
  runManager->Initialize();
    

#ifdef G4VIS_USE
  // visualization manager
  G4VisManager* visManager = new G4VisExecutive; // hbarVisManager;
  visManager->Initialize();
#endif


  // get the pointer to the User Interface manager 
  G4UImanager* UImanager = G4UImanager::GetUIpointer(); 

  G4UIExecutive* ui = new G4UIExecutive(argc, argv, "/*t*/csh");

  if (argc > 1)   // there is a batch file to execute
    {
      G4String command = "/control/execute ";
      G4String fileName = argv[1];
      UImanager->ApplyCommand(command+fileName);
    }
  else
	{
	  ui->SessionStart();
	}

  // This command pops up the annoying GUI window!
  //ui->SessionStart();
  
  // job termination
  delete ui;
#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;
  //delete verbosity;

  return 0;
}

