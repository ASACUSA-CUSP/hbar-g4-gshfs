#!/bin/bash

for number in 700 800 900 1000 1500 2000
do
	for ((z=2;z<=10;z++))
	do
		#cat hbar_cusp_2014_scan${number}_35_1.mac | sed -e "s/scan_${number}_1G_1.root/scan_${number}_1G_$z.root/" > hbar_cusp_2014_scan${number}_$z-1.mac
		#cat hbar_cusp_2014_scan${number}_$z-1.mac | sed -e "s/g4_demo_scan${number}_35_1.root/g4_demo_scan${number}_35_$z.root/" > hbar_cusp_2014_scan${number}_35_$z.mac
		cat hbar_cusp_2014_scan${number}_pi_0.16G_30_1.mac | sed -e "s/1420.655/1420.66/" > hbar_cusp_2014_scan${number}_$z-1.mac
		cat hbar_cusp_2014_scan${number}_$z-1.mac | sed -e "s/././" > hbar_cusp_2014_scan${number}_pi_0.16G_30_$z.mac
	done
done

#rm hbar_cusp_2014*-1.mac

	#cat hbar_cusp_2014_scan${number}_35_1.mac | sed -e "s/scan_${number}_1G_1.root/scan_${number}_1G_$z.root/" > hbar_cusp_2014_scan${number}_$z-1.mac
	#cat hbar_cusp_2014_scan${number}_$z-1.mac | sed -e "s/g4_demo_scan${number}_35_1.root/g4_demo_scan${number}_35_$z.root/" > hbar_cusp_2014_scan${number}_35_$z.mac
#	done

