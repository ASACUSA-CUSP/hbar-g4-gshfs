#!/bin/bash

for number in 700 800 900 1000 1500 2000
do
	for ((z=2;z<=10;z++))
	do
		cat hbar_cusp_2014_scan${number}_pi_0.16G_30_$z.mac | sed -e "s/1420.655/1420.66/" > hbar_cusp_2014_scan${number}_pi_0.16G_30_$z-1.mac
	done
done
