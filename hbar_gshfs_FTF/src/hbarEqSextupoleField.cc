#include "hbarEqSextupoleField.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" // Added by Clemens
#include "globals.hh"

void hbarEqSextupoleField::SetChargeMomentumMass(G4ChargeState particleCharge, // in e+ units
						 G4double MomentumXc,
						 G4double MassXc2) 
{
  mu       = particleCharge.GetMagneticDipoleMoment();
  charge   =  particleCharge.GetCharge();
  fMassCof = MassXc2*MassXc2;
  (void)MomentumXc; //Disable unused variable compiler warnings. But this should probably be considered: why is this parameter here? //Rikard

}
//
//----------------------------------changed by Tajima stop


void
hbarEqSextupoleField::EvaluateRhsGivenB(const G4double y[],
			                const G4double Field[],
					G4double dydx[] ) const
{

  // y[0-2]: spatial coordinates
  // y[3-5]: momentum * c
  // y[7]: time in lab frame
  //
  // dydx[0-7]: "derivates of the above with respect to the distance along the curve"
  // (or so I was told)
  // In other words:
  // dydx[0-2]: (d/ds)X = Vx/V = Px/P
  // dydx[3-5]: force on the particle, or to be more precise: force * (c/velocity)
  // dydx[7]: inverse velocity (actually, c/v)
  //
  // Field[0-2]    : magnetic field (defined in hbarField.cc)
  // Field[3-5]    : electric field (defined in hbarField.cc)
  // Field[6-8]    : magnetic field gradient (defined in hbarField.cc)
  // Field[9-11]   : electric field gradient (defined in hbarField.cc)
  // Field[12]     : magnetic moment of particle
  // Field[13-15]  : gradient of magnetic moment of particle

  // in case we have a particle with magnetic moment that is no H or Hbar


  G4double gradmu[3];
  G4double lmu;
  gradmu[0] = Field[13]; 
  gradmu[1] = Field[14]; 
  gradmu[2] = Field[15]; 

  if(Field[12] != 0)
	{
	  lmu = Field[12];
	} 
  else if (mu == 99) 
	{
	  lmu = 0;
	} 
  else 
	{
	  lmu = mu;
	}


  
  G4double fElectroMagCof =  eplus*charge*c_light;
  G4double pSquared = y[3]*y[3] + y[4]*y[4] + y[5]*y[5] ;

  G4double Energy   = std::sqrt( pSquared + fMassCof );

  G4double pModuleInverse  = 1.0/std::sqrt(pSquared);

  //  G4double inverse_velocity = Energy * c_light * pModuleInverse;
  G4double inverse_velocity = Energy * pModuleInverse / c_light;

  G4double cof1     = fElectroMagCof*pModuleInverse;
  G4double cof2     = Energy*pModuleInverse;

  //  G4double vDotE = y[3]*Field[3] + y[4]*Field[4] + y[5]*Field[5] ;

  dydx[0] = y[3]*pModuleInverse;
  dydx[1] = y[4]*pModuleInverse;
  dydx[2] = y[5]*pModuleInverse;

  // Lorentz force - magnetic part
  dydx[3] = cof1*(y[4]*Field[2] - y[5]*Field[1]);
  dydx[4] = cof1*(y[5]*Field[0] - y[3]*Field[2]); 
  dydx[5] = cof1*(y[3]*Field[1] - y[4]*Field[0]);

  // Lorentz force - electric part
  // in case od hbar and h fElectroMagCof is 0
  dydx[3] += fElectroMagCof*inverse_velocity*Field[3]/c_light;
  dydx[4] += fElectroMagCof*inverse_velocity*Field[4]/c_light;
  dydx[5] += fElectroMagCof*inverse_velocity*Field[5]/c_light;

  // Select particle and add additional force (B-Field)
  G4double fieldValue = std::sqrt(Field[0]*Field[0] + Field[1]*Field[1] + Field[2]*Field[2]);
  
  // extra force due to the magnetic moment
  // grad(mu*B) = mu*grad(B) + B*grad(mu)
  dydx[3] -= (cof2*lmu*Field[6] + cof2*gradmu[0]*fieldValue);
  dydx[4] -= (cof2*lmu*Field[7] + cof2*gradmu[1]*fieldValue);
  dydx[5] -= (cof2*lmu*Field[8] + cof2*gradmu[2]*fieldValue);
  
  
  if((dydx[3] != 0 || dydx[4] != 0 || dydx[5] != 0) && y[2] > 0)
	{
	  //std::cout << "ForceT: " << fieldValue << " " << std::sqrt(dydx[3]*dydx[3]+dydx[4]*dydx[4]) << " " << y[2]/cm << " " << lmu << std::endl;
	  //	  std::cout << "X: " << dydx[3]  << std::endl
	  //			<< "Y: " << dydx[4]  << std::endl
	  //			<< "Z: " << dydx[5]  << std::endl;
	}
  
  // Lab Time of flight
  dydx[7] = inverse_velocity;
  return ;
}
