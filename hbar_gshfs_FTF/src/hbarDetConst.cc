#include "hbarDetConst.hh"

hbarDetConst::hbarDetConst()
  :WorldSol(NULL), WorldLog(NULL), WorldPhy(NULL),
   WorldSizeXY(10.0*m),WorldSizeZ(40.0*m), useCavity(true), useNagataDetector(false), useCPT(true),
   useCUSP(false), 
   myCPTConst(NULL), myBeamlineConst(NULL), myBGOConst(NULL),
   myCUSPConst(NULL), myCavityConst(NULL), mySextupoleConst(NULL),
   myDummyDetectorConst(NULL)
{
  DefineMaterials();
  alim = new G4UserLimits();
  alim->SetMaxAllowedStep(1.0*mm);

  magFieldSetup = new hbarFieldSetup(this);

  myCPTConst = new hbarCPTConst();
  myBeamlineConst = new hbarBeamlineConst();
  myBGOConst = new hbarBGOConst();
  myCUSPConst = new hbarCUSPConst(magFieldSetup);
  myCavityConst = new hbarCavityConst(new hbarMWSetup(this));
  mySextupoleConst = new hbarSextupoleConst(magFieldSetup);
  //myDummyDetectorConst = new hbarDummyDetectorConst(alim);

  G4String scintiSDname = "/myDet/scintillator";
  scintiSD = new hbarDetectorSD(scintiSDname);
  G4SDManager *SDman = G4SDManager::GetSDMpointer();
  SDman->AddNewDetector(scintiSD);

  detectorMessenger = new hbarDetMess(this);
}

G4bool hbarDetConst::useHbar2014 = 0; // creating the class attribute, because static!
G4bool hbarDetConst::useTPX = 1; // TODO integrate into messeneger class

G4Material * hbarDetConst::GetMaterial(const G4String & name)
{
  G4MaterialTable * myTable = G4Material::GetMaterialTable();
  for(std::vector<G4Material*>::iterator it = myTable->begin(); it!=myTable->end(); ++it)
	{
	  if((*it)->GetName() == name)
		{
		  return *it;
		}
	}
  char buffer[400];
  sprintf(buffer, "The requested material '%s' was not in the table.", name.c_str());
  throw hbarException(buffer);
}

hbarDetConst::~hbarDetConst()
{
  if (magFieldSetup)
	delete magFieldSetup;
  delete detectorMessenger;
  if (alim)
	delete alim;
  if(myCPTConst)
	delete myCPTConst;
  if(myBeamlineConst)
	delete myBeamlineConst;
  if(myBGOConst)
	delete myBGOConst;
  if(myCUSPConst)
	delete myCUSPConst;
  if(myCavityConst)
	delete myCavityConst;
  if(mySextupoleConst)
	delete mySextupoleConst;
}

G4VPhysicalVolume* hbarDetConst::Construct()
{
  return ConstructWorld();
}

void hbarDetConst::DefineMaterials()
{
  G4Element* H  = new G4Element("Hydrogen","H" , 1., 1.01*g/mole);
  G4Element* C  = new G4Element("Carbon", "C" , 6., 12.01*g/mole);
  G4Element* N  = new G4Element("Nitrogen", "N" , 7., 14.01*g/mole);
  G4Element* O  = new G4Element("Oxygen", "O" , 8., 16.00*g/mole);
  G4Element* Na  = new G4Element("Natrium", "Na" , 11. , 22.99*g/mole);
  G4Element* Hg  = new G4Element("Mercury", "Hg" , 80., 200.59*g/mole);
  G4Element* Al  = new G4Element("Aluminium", "Al" , 13., 26.98*g/mole);
  
  G4Element* elAu  = new G4Element("Gold", "Au" , 79., 196.97*g/mole);
  G4Element* elMo  = new G4Element("Molybdenum", "Mo" , 42., 95.94*g/mole);
  
  G4Element* Si = new G4Element("Silicon", "Si" , 14., 28.09*g/mole);
  G4Element* K  = new G4Element("Potassium", "K", 19. , 39.1*g/mole);
  G4Element* Ca  = new G4Element("Calcium", "Ca", 31. , 69.72*g/mole);
  G4Element* Cr = new G4Element("Chromium", "Cr", 24., 52.00*g/mole);
  G4Element* Fe = new G4Element("Iron", "Fe", 26., 55.85*g/mole);
  G4Element* Ni = new G4Element("Nickel", "Ni", 28., 58.69*g/mole);
  G4Element* elTi = new G4Element("Titanium", "Ti",22.,47.88*g/mole);
  G4Element* elAs = new G4Element("As", "As",33., 74.92*g/mole);
  G4Element* elBi = new G4Element("Bismuth", "Bi",83.,209.0*g/mole);
  G4Element* elGe = new G4Element("Germanium", "Ge",32.,72.63*g/mole);
  G4Element* elBa = new G4Element("Barium", "Ba",56.,137.33*g/mole);
  G4Element* elF = new G4Element("Fluorine", "F",9.,19.*g/mole);
  G4Element* elPb = new G4Element("Lead", "Pb",82.,207.2*g/mole);
  G4Element* elCu = new G4Element("Copper", "Cu",29.,63.546*g/mole);
  G4Element* elNb = new G4Element("Niobium", "Nb",41.,92.906*g/mole);
  G4Element* elZn = new G4Element("Zinc", "Zn",30.,65.38*g/mole);
  G4Element* elMg = new G4Element("Magnesium", "Mg",12.,24.31*g/mole);
	G4Element* Bor = new G4Element("Bor", "B", 5., 10.81*g/mole);
  G4Element* Sb  = new G4Element("Antimony", "Sb" , 51. , 121.75*g/mole);
  G4Element* Cs  = new G4Element("Ceasium", "Cs" , 55. , 132.91*g/mole);
  // A7075_Alloy --  electrode
  G4Material* A7075_Alloy = new G4Material("A7075_Alloy", 3.1*g/cm3, 4);
  A7075_Alloy->AddElement(Al,0.905);
  A7075_Alloy->AddElement(elCu,0.015);
  A7075_Alloy->AddElement(elZn,0.055);
  A7075_Alloy->AddElement(elMg,0.025);

  G4Material* Copper = new G4Material("Copper",8.94*g/cm3, 1);
  Copper->AddElement(elCu,1.0);

  // NbTi
  G4Material* NbTiCable = new G4Material("NbTiCable",7.923*g/cm3,3);
  NbTiCable->AddElement(elNb,0.186);
  NbTiCable->AddElement(elTi,0.214);
  NbTiCable->AddElement(elCu,0.6);

  G4Material* Iro = new G4Material("Iron",7.874*g/cm3, 1);
  Iro->AddElement(Fe,1.0);
  
  G4Material* Sili = new G4Material("Silicon",2.3290*g/cm3, 1);
  Sili->AddElement(Si,1.0);
  
  G4Material* Goldmaterial = new G4Material("Gold", 19.3*g/cm3, 1);
  Goldmaterial->AddElement(elAu,1.0);
  
  G4Material* Molybdenummaterial = new G4Material("Molybdenum", 10.28*g/cm3, 1);
  Molybdenummaterial->AddElement(elMo,1.0);
  
  
  G4Material* Macerite = new G4Material("Macerite", 2.8*g/cm3,6);
  Macerite->AddElement(K,0.091);
  Macerite->AddElement(elMg,0.102);
  Macerite->AddElement(Al,0.085);
  Macerite->AddElement(Si,0.22);
  Macerite->AddElement(O,0.412);
  Macerite->AddElement(elF,0.09);


  G4Material* hydro = new G4Material("hydro", 0.0000899*g/cm3, 1,kStateGas,273.*kelvin,1.*atmosphere);
  hydro->AddElement(H,1.0);

  G4Material * epoxy = new G4Material("Epoxy", 1.15*g/cm3, 3);   //c11h12o3
  epoxy->AddElement(H,0.462);
  epoxy->AddElement(C,0.423);
  epoxy->AddElement(O,0.115);


  G4Material *mumetal = new G4Material("Mu-Metal", 8.7*g/cm3, 2);
  mumetal->AddElement(Fe,0.19);
  mumetal->AddElement(Ni,0.81);

  G4Material* Aluminium = new G4Material("Aluminium",2.7*g/cm3, 1);
  Aluminium->AddElement(Al,100*perCent);

  G4Material* GlassPb = new G4Material("Glass Lead", 3.1*g/cm3, 5);
  GlassPb->AddElement(O,15.6*perCent);
  GlassPb->AddElement(Si,8.0*perCent);
  GlassPb->AddElement(elTi,0.8*perCent);
  GlassPb->AddElement(elAs,0.2*perCent);
  GlassPb->AddElement(elPb,75.4*perCent);

  // BGO
  G4Material* BGO = new G4Material("BGO", 7.13*g/cm3, 3);
  BGO->AddElement(elBi,67.1*perCent);
  BGO->AddElement(elGe,17.5*perCent);
  BGO->AddElement(O,15.4*perCent);

  //BaF2
  G4Material* BaF2 = new G4Material("BaF2", 4.88*g/cm3, 2);
  BaF2->AddElement(elBa,78.0*perCent);
  BaF2->AddElement(elF,22.0*perCent);

  G4Material * LeadMaterial = new G4Material("Lead", 82., 207.19*g/mole, 11.35*g/cm3);
  (void)LeadMaterial;

  //////////////////////////////////////////////////////////////////////
  // Scintillator
  G4Material* ScintiMaterial = new G4Material("Scintillator", 1.032 *g/cm3, 2);
  ScintiMaterial->AddElement(C, 9);
  ScintiMaterial->AddElement(H, 10);

  // Plexiglas
  G4Material* Plexiglas = new G4Material("Plexiglas", 1.19 *g/cm3, 3);
  Plexiglas->AddElement(C, 5);
  Plexiglas->AddElement(O, 2);
  Plexiglas->AddElement(H, 8);

  // SiPM
  G4Material* SiPMMaterial = new G4Material("SiPM", 2.33 *g/cm3, 1);
  SiPMMaterial->AddElement(Si, 1.);

  // Wrapping
  G4Material* WrappingMaterial = new G4Material("Wrapping", 2.7 *g/cm3, 1);
  WrappingMaterial->AddElement(Al, 1.);

  // Glue
  G4Material * epoxy_glue = new G4Material("Epoxy_Glue", 1.15*g/cm3, 3);   //c11h12o3
  epoxy_glue->AddElement(H,0.462);
  epoxy_glue->AddElement(C,0.423);
  epoxy_glue->AddElement(O,0.115); 
  ////////////////////////////////////////////////////////////////////////

// Kovar ?... difference zu tempax?
	G4Material* SiO2 = new G4Material("SiO2", 2.648*g/cm3, 2);
	SiO2->AddElement(Si, 1.0/3.0);
	SiO2->AddElement(O, 2.0/3.0);

	G4Material* B2O3 = new G4Material("B2O3", 2.55*g/cm3, 2);
	B2O3->AddElement(Bor, 0.4);
	B2O3->AddElement(O, 0.6);

	G4Material* Na2O = new G4Material("Na2O", 2.27*g/cm3, 2);
	Na2O->AddElement(Na, 2.0/3.0);
	Na2O->AddElement(O, 1.0/3.0);	

	G4Material* Al2O3 = new G4Material("Al2O3", 4.0*g/cm3, 2);
	Al2O3->AddElement(Al, 0.4);
	Al2O3->AddElement(O, 0.6);	

	G4Material* Borosilicate = new G4Material("Borosilicate", 2.29*g/cm3, 4);
	Borosilicate->AddMaterial(SiO2, 0.81);
	Borosilicate->AddMaterial(B2O3, 0.13);
	Borosilicate->AddMaterial(Na2O, 0.04);
	Borosilicate->AddMaterial(Al2O3, 0.02);

	G4Material* CarbonCoatmaterial = new G4Material("CarbonCoatmaterial", 2.26*g/cm3, 1); 
	CarbonCoatmaterial->AddElement(C, 1.0);

/*G4Material* Borosilicate = new G4Material("Borosilicate glass", density= 2.23*g/cm3, ncomponents=6);
  Borosilicate->AddElement(B, fractionmass=0.040064);
  Borosilicate->AddElement(O, fractionmass=0.539562); 
  Borosilicate->AddElement(Na, fractionmass=0.028191);
  Borosilicate->AddElement(Al, fractionmass=0.011644);
  Borosilicate->AddElement(Si, fractionmass=0.377220);
  Borosilicate->AddElement(K, fractionmass=0.003321);*/

	G4Material* PMTmaterial = new G4Material("PMTmaterial", 4.28*g/cm3,3); // bialkali cathode
	PMTmaterial->AddElement(K, 0.133);
	PMTmaterial->AddElement(Cs, 0.452);
	PMTmaterial->AddElement(Sb, 0.415); 


  ////////////////////////////////////////////////////////////////////////
  

  // Stainless steel
  G4Material* Sts = new G4Material("Sts", 7.90*g/cm3, 3);
  Sts->AddElement(Cr, 0.18);
  Sts->AddElement(Ni, 0.08);
  Sts->AddElement(Fe, 0.74);

  // Concrete

  G4Material * ConcreteMaterial = new G4Material("Concrete", 2.03*g/cm3, 10);
  ConcreteMaterial->AddElement(H  ,  0.01);
  ConcreteMaterial->AddElement(O  ,  0.529);
  ConcreteMaterial->AddElement(Na ,  0.016);
  ConcreteMaterial->AddElement(Hg ,  0.002);
  ConcreteMaterial->AddElement(Al ,  0.034);
  ConcreteMaterial->AddElement(Si ,  0.337);
  ConcreteMaterial->AddElement(K  ,  0.013);
  ConcreteMaterial->AddElement(Ca ,  0.044);
  ConcreteMaterial->AddElement(Fe ,  0.014);
  ConcreteMaterial->AddElement(C  ,  0.001);

  // Air
  G4Material* Air = new G4Material("Air", 1.205*mg/cm3, 2, kStateGas,293.*kelvin,1.*atmosphere);
  Air->AddElement(N, 0.7);
  Air->AddElement(O, 0.3);

  // Vacuum
  G4Material* isolvac = new G4Material("IsolVac", 1.2e-7*mg/cm3, 1, kStateGas,295.*kelvin,1.e-7*bar);
  isolvac->AddMaterial(Air, 1.);
  
  G4Material* isolvac2 = new G4Material("Vac", 1.2e-10*mg/cm3, 1, kStateGas,295.*kelvin,1.e-10*bar);
  isolvac2->AddMaterial(Air, 1.);

  G4Material* Vacuum = new G4Material("Galactic", 1.0, 1.01*g/mole,
		   universe_mean_density,
		   kStateGas, 3.e-18*pascal, 2.73*kelvin);
  //(void)Vacuum;
}

G4VPhysicalVolume* hbarDetConst::ConstructWorld()
{
  if (WorldPhy)
    {
      G4GeometryManager::GetInstance()->OpenGeometry();
      G4PhysicalVolumeStore::GetInstance()->Clean();
      G4LogicalVolumeStore::GetInstance()->Clean();
      G4SolidStore::GetInstance()->Clean();
    }

  // World

  WorldSol = new G4Box("World", WorldSizeXY/2.0, WorldSizeXY/2.0, WorldSizeZ/2.0);

  WorldLog = new G4LogicalVolume(WorldSol,                 // its solid
                                 GetMaterial("Galactic"),  // its material
                                 "World",
                                 0, 0, 0);

  WorldLog->SetUserLimits(alim);

  if (magFieldSetup)
	{
	  WorldLog->SetFieldManager(magFieldSetup->GetFieldManager(), false) ;
	}

  WorldPhy = new G4PVPlacement(0,		//no rotation
			       G4ThreeVector(),	//at (0,0,0)
			       "World",		//its name
			       WorldLog,	//its logical volume
			       NULL,		//its mother  volume
			       false,		//no boolean operation
			       0);		//copy number

  myBeamlineConst->MakeBeamline(WorldPhy);


  if(useNagataDetector && useCPT)
	{
	  throw hbarException("Cannot combine the Nagata detector with some other detectors.");
	}

  if(useCPT)
	{
	  myCPTConst->MakeCPT(WorldPhy, scintiSD);
	}

  if(useNagataDetector)
	{
	  myBGOConst->MakeBGO(WorldPhy, myCUSPConst->GetCenterOfTheMikiLense(), scintiSD);
	}

  if(useCUSP)
	{
	  myCUSPConst->MakeCUSP(WorldPhy, scintiSD);
	}

  if(useCavity)
	{
	  myCavityConst->MakeCavity(WorldPhy);
	}

  mySextupoleConst->MakeSextupole(WorldPhy, alim);

  //G4VPhysicalVolume * dummyVol = myDummyDetectorConst->Construct();
  //dummyVol->SetMotherLogical(WorldLog);
  //WorldLog->AddDaughter(dummyVol);

  WorldLog->SetVisAttributes (G4VisAttributes::Invisible);
  if (magFieldSetup != NULL)
	{
	  magFieldSetup->UpdateField();
	}
	
	

  return WorldPhy;
}


/*void hbarDetConst::ConstructGDML()
{
  time_t _tm =time(NULL );   // creating the actual time when called
  struct tm * curtime = localtime ( &_tm );
  G4String timestr = asctime(curtime);

  if (WriteGDML == 1){
     G4GDMLParser fParser;  // GDMLparser
     G4VPhysicalVolume* fWorldPhysVol;
     fWorldPhysVol = ConstructWorld();
     G4String filename = "GDML_" + timestr + ".gdml"; //preparing the full filename with timestamp
     fParser.Write(filename, fWorldPhysVol); //parsing the gdml
  }
}*/


void hbarDetConst::UpdateGeometry()
{
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructWorld());
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
  if (magFieldSetup != NULL)
	{
	  magFieldSetup->UpdateField();
	}
  else
	{
	  throw hbarException("The magnetic field setup was NULL.");
	}
}




void hbarDetConst::SetUseCUSP(G4bool useflag)
{
  useCUSP = useflag;
}
