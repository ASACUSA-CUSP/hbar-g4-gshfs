#include "hbarTrajectory.hh"
#include "hbarAntiHydrogen.hh" // Added by Clemens
#include "hbarHydrogen.hh"     // Added by Clemens

#include "G4AttDef.hh"
#include "G4AttDefStore.hh"
#include "G4AttValue.hh"
#include "G4UIcommand.hh"
#include "G4UnitsTable.hh"
#include "G4TrajectoryPoint.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4ThreeVector.hh"
#include "G4Polyline.hh"
#include "G4Polymarker.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"

#ifdef G4ATTDEBUG
#include "G4AttCheck.hh"
#endif

G4Allocator<hbarTrajectory> hbarTrajectoryAllocator;

hbarTrajectory::hbarTrajectory()
:  positionRecord(0), fTrackID(0), fParentID(0),
   PDGEncoding( 0 ), PDGCharge(0.0), ParticleName(""),
   initialMomentum( G4ThreeVector() ), useColouring(true) // Added by Clemens
{;}

hbarTrajectory::hbarTrajectory(const G4Track* aTrack)
{
   fpParticleDefinition = aTrack->GetDefinition();
   ParticleName = fpParticleDefinition->GetParticleName();
   PDGCharge = fpParticleDefinition->GetPDGCharge();
   PDGEncoding = fpParticleDefinition->GetPDGEncoding();
   fTrackID = aTrack->GetTrackID();
   fParentID = aTrack->GetParentID();
   initialMomentum = aTrack->GetMomentum();
   positionRecord = new hbarTrajectoryPointContainer();
   // Following is for the first trajectory point
   positionRecord->push_back(std::make_pair(new G4TrajectoryPoint(aTrack->GetPosition()),hbarTrackDictionaryEntry()));
   useColouring = true;
}

hbarTrajectory::hbarTrajectory(hbarTrajectory & right):G4VTrajectory()
{
  ParticleName = right.ParticleName;
  PDGCharge = right.PDGCharge;
  PDGEncoding = right.PDGEncoding;
  fTrackID = right.fTrackID;
  fParentID = right.fParentID;
  initialMomentum = right.initialMomentum;
  positionRecord = new hbarTrajectoryPointContainer();

  for(size_t i=0;i<right.positionRecord->size();i++)
  {
    G4TrajectoryPoint* rightPoint = (G4TrajectoryPoint*)((*(right.positionRecord))[i].first);
	hbarTrackDictionaryEntry rightEnt = (*(right.positionRecord))[i].second;
    positionRecord->push_back(std::make_pair(new G4TrajectoryPoint(*rightPoint), rightEnt));
  }
  useColouring = true;
}

hbarTrajectory::~hbarTrajectory()
{
  //  positionRecord->clearAndDestroy();
  size_t i;
  for(i=0;i<positionRecord->size();i++){
    delete  (*positionRecord)[i].first;
  }
  positionRecord->clear();

  delete positionRecord;
}

void hbarTrajectory::ShowTrajectory(std::ostream& os) const
{
  // Invoke the default implementation in G4VTrajectory...
   G4VTrajectory::ShowTrajectory(os);
   // ... or override with your own code here.
}

void hbarTrajectory::DrawTrajectory(G4int i_mode) const
{
  // If i_mode>=0, draws a trajectory as a polyline (blue for
  // positive, red for negative, green for neutral) and, if i_mode!=0,
  // adds markers - yellow circles for step points and magenta squares
  // for auxiliary points, if any - whose screen size in pixels is
  // given by std::abs(i_mode)/1000.  E.g: i_mode = 5000 gives easily
  // visible markers.

  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if (!pVVisManager) return;

  const G4double markerSize = std::abs(i_mode)/1000;
  G4bool lineRequired (i_mode >= 0);
  G4bool markersRequired (markerSize > 0.);

  std::vector<G4Polyline> trajectoryLine;
  std::vector<hbarTrackDictionaryEntry> segQuantumNumber;
  G4Polymarker stepPoints;
  G4Polymarker auxiliaryPoints;

  hbarTrackDictionaryEntry lastQ(20,-100,-100);

  for (G4int i = 0; i < GetPointEntries() ; i++) 
	{
	  if(GetPointQNbr(i) != lastQ)
		{
		  lastQ = GetPointQNbr(i);
		  trajectoryLine.push_back(G4Polyline());
		  segQuantumNumber.push_back(lastQ);
		  if(i>0)
			{
			  trajectoryLine.back().push_back(G4ThreeVector(GetPoint(i-1)->GetPosition()));
			  //G4cout << "IIIPushing point " << GetPoint(i-1)->GetPosition().z() << " for id " << trajectoryLine.size() << " with qnbr " << segQuantumNumber.back() << G4endl;
			}

		}

	  G4VTrajectoryPoint* aTrajectoryPoint = GetPoint(i);
	  const std::vector<G4ThreeVector>* auxiliaries
		= aTrajectoryPoint->GetAuxiliaryPoints();
	  if (auxiliaries) 
		{
		  for (size_t iAux = 0; iAux < auxiliaries->size(); ++iAux) 
			{
			  const G4ThreeVector pos((*auxiliaries)[iAux]);
			  if (lineRequired) 
				{
				  trajectoryLine.back().push_back(pos);
				  //G4cout << "Pushing point " << pos.z() << " for id " << trajectoryLine.size() << " with qnbr " << segQuantumNumber.back() << G4endl;

				}
			  if (markersRequired) 
				{
				  auxiliaryPoints.push_back(pos);
				}
			}
		}
	  const G4ThreeVector pos(aTrajectoryPoint->GetPosition());
	  if (lineRequired) 
		{
		  trajectoryLine.back().push_back(pos);
		  //		  G4cout << "XPushing point " << pos.z() << " for id " << trajectoryLine.size() << " with qnbr " << segQuantumNumber.back() << G4endl;
		}
	  if (markersRequired) 
		{
		  stepPoints.push_back(pos);
		}
	}
	
  if (lineRequired) 
	{
	  for(G4int i = 0; i<(int)trajectoryLine.size(); ++i)
		{
		  G4VisAttributes trajectoryLineAttribs;
		  G4VisAttributes::LineStyle ls = G4VisAttributes::unbroken;
		  G4Colour colour;
		  const G4double charge = GetCharge();
		  if (charge > 0.) 
			{
			  colour = G4Colour(0.,0.,1.); // Blue = positive
			  if (ParticleName == "e+") 
				{
				  colour = G4Colour(0.0, 1.0, 1.0);  // Cyan = positron
				  ls = G4VisAttributes::dashed;
				} 
			  else if (ParticleName == "mu+") 
				{
				  colour = G4Colour(0.5, 0.0, 1.0);  // Violet = positive muon
				  ls = G4VisAttributes::dashed;
				}
			} 
		  else if (charge < 0.) 
			{
			  colour = G4Colour(1.,0.,0.); // Red = negative
			  if (ParticleName == "e-") 
				{
				  colour = G4Colour(1.0, 1.0, 0.0);  // Yellow = electron
				  ls = G4VisAttributes::dashed;
				} 
			  else if (ParticleName == "mu-") 
				{
				  colour = G4Colour(1.0, 0.0, 0.5);  // Magenta = negative muon
				  ls = G4VisAttributes::dashed;
				}
			} 
		  else  
			{
			  //colour = G4Colour(0.,1.,0.); // Green = neutral
			  // Added by Clemens -- start
			  if( (ParticleName == "antihydrogen" && useColouring ) || (ParticleName == "hydrogen" && useColouring))
				{
				  //std::cout << "ParticleName: " << ParticleName << std::endl;
				  //std::cout << "F: " << static_cast<hbarAntiHydrogen*>(fpParticleDefinition)->GetF() << std::endl;
				  
				  if(segQuantumNumber[i].N > 1)
					{
					  if(segQuantumNumber[i].N > 20)
						{
						  ls = G4VisAttributes::dotted;
						}
					  switch(segQuantumNumber[i].N%4)
						{
						case 0:
						  colour = G4Colour(0.0,1.0,1.0);
						  break;
						case 1:
						  colour = G4Colour(1.0,0.0,1.0);
						  break;
						case 2:
						  colour = G4Colour(1.0,0.6, 0);
						  break;
						case 3:
						  colour = G4Colour(145./255., 44./255.,238/255.);
						  break;
						}
					}
				  else if(segQuantumNumber[i].N == 1)
					{
					  if(segQuantumNumber[i].F == 1 && segQuantumNumber[i].M != 1)
						{
						  colour = G4Colour(0.,0.69,0.); // green
						}
					  else 
						{
						  colour = G4Colour(0.69,0.,0.); // red
						}
					}
				  else ///Mysterious case... should never happen.
					{
					  colour = G4Colour(1.,1.,1.); // white
					}
				} 
			  else 
				{
				  // Any other particle is light Green..
				  colour = G4Colour(0.,1.0,0.); // Green = neutral
				}
			  //colour = G4Colour(0.69,0.,0.); // Green = neutral
			  if (ParticleName == "gamma") 
				{
				  colour = G4Colour(1.0, 1.0, 1.0);  // White = gamma
				  ls = G4VisAttributes::dashed;
				}
			}
		  trajectoryLineAttribs.SetColour(colour);
		  trajectoryLineAttribs.SetLineStyle(ls);
		  trajectoryLine[i].SetVisAttributes(&trajectoryLineAttribs);
		  pVVisManager->Draw(trajectoryLine[i]);
		}
	}
  if (markersRequired) 
	{
	  auxiliaryPoints.SetMarkerType(G4Polymarker::squares);
	  auxiliaryPoints.SetScreenSize(markerSize);
	  auxiliaryPoints.SetFillStyle(G4VMarker::filled);
	  G4VisAttributes auxiliaryPointsAttribs(G4Colour(0.,1.,1.));  // Cyan
	  auxiliaryPoints.SetVisAttributes(&auxiliaryPointsAttribs);
	  pVVisManager->Draw(auxiliaryPoints);
	  
	  stepPoints.SetMarkerType(G4Polymarker::circles);
	  stepPoints.SetScreenSize(markerSize);
	  stepPoints.SetFillStyle(G4VMarker::filled);
	  G4VisAttributes stepPointsAttribs(G4Colour(1.,1.,0.));  // Yellow.
	  stepPoints.SetVisAttributes(&stepPointsAttribs);
	  pVVisManager->Draw(stepPoints);
	}
  
}

const std::map<G4String,G4AttDef>* hbarTrajectory::GetAttDefs() const
{
  G4bool isNew;
  std::map<G4String,G4AttDef>* store
    = G4AttDefStore::GetInstance("hbarTrajectory",isNew);
  if (isNew) {

    G4String ID("ID");
    (*store)[ID] = G4AttDef(ID,"Track ID","Bookkeeping","","G4int");

    G4String PID("PID");
    (*store)[PID] = G4AttDef(PID,"Parent ID","Bookkeeping","","G4int");

    G4String PN("PN");
    (*store)[PN] = G4AttDef(PN,"Particle Name","Bookkeeping","","G4String");

    G4String Ch("Ch");
    (*store)[Ch] = G4AttDef(Ch,"Charge","Physics","e+","G4double");

    G4String PDG("PDG");
    (*store)[PDG] = G4AttDef(PDG,"PDG Encoding","Bookkeeping","","G4int");

    G4String IMom("IMom");
    (*store)[IMom] = G4AttDef(IMom, "Momentum of track at start of trajectory",
                              "Physics","G4BestUnit","G4ThreeVector");

    G4String IMag("IMag");
    (*store)[IMag] = 
      G4AttDef(IMag, "Magnitude of momentum of track at start of trajectory",
               "Physics","G4BestUnit","G4double");

    G4String NTP("NTP");
    (*store)[NTP] = G4AttDef(NTP,"No. of points","Bookkeeping","","G4int");

  }
  return store;
}

std::vector<G4AttValue>* hbarTrajectory::CreateAttValues() const
{
  std::vector<G4AttValue>* values = new std::vector<G4AttValue>;

  values->push_back
    (G4AttValue("ID",G4UIcommand::ConvertToString(fTrackID),""));

  values->push_back
    (G4AttValue("PID",G4UIcommand::ConvertToString(fParentID),""));

  values->push_back(G4AttValue("PN",ParticleName,""));

  values->push_back
    (G4AttValue("Ch",G4UIcommand::ConvertToString(PDGCharge),""));

  values->push_back
    (G4AttValue("PDG",G4UIcommand::ConvertToString(PDGEncoding),""));

  values->push_back
    (G4AttValue("IMom",G4BestUnit(initialMomentum,"Energy"),""));

  values->push_back
    (G4AttValue("IMag",G4BestUnit(initialMomentum.mag(),"Energy"),""));

  values->push_back
    (G4AttValue("NTP",G4UIcommand::ConvertToString(GetPointEntries()),""));

#ifdef G4ATTDEBUG
  G4cout << G4AttCheck(values,GetAttDefs());
#endif

  return values;
}

void hbarTrajectory::AppendStep(const G4Step* aStep)
{
  hbarTrackDictionaryEntry myEnt;
  if(hbarUserTrackInformation * trif = dynamic_cast<hbarUserTrackInformation*>(aStep->GetTrack()->GetUserInformation()))
	{
	  if(trif->DictionaryHas(aStep))
		{
		  myEnt = trif->DictionaryLookup(aStep);
		}
	}
  positionRecord->push_back( std::make_pair(new G4TrajectoryPoint(aStep->GetPostStepPoint()->
																  GetPosition() ),myEnt));
}

G4ParticleDefinition* hbarTrajectory::GetParticleDefinition()
{
   return (G4ParticleTable::GetParticleTable()->FindParticle(ParticleName));
}

void hbarTrajectory::MergeTrajectory(G4VTrajectory* secondTrajectory)
{
  if(!secondTrajectory) return;

  hbarTrajectory* seco = (hbarTrajectory*)secondTrajectory;
  G4int ent = seco->GetPointEntries();
  for(G4int i=1;i<ent;i++) // initial point of the second trajectory should not be merged
  { 
    positionRecord->push_back(make_pair((*(seco->positionRecord))[i].first, (*(seco->positionRecord))[i].second));
    //    positionRecord->push_back(seco->positionRecord->removeAt(1));
  }
  delete (*seco->positionRecord)[0].first;
  seco->positionRecord->clear();
}

