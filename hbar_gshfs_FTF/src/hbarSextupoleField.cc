#include "G4ThreeVector.hh"
#include "G4Cons.hh"

#include "hbarField.hh"
#include "hbarDetConst.hh"

#include "CLHEP/Vector/TwoVector.h"

// local helper function, rotates a vector 
void rotate(G4double Bfield[3], G4double xangle, G4double yangle){
  //std::cout << "BFIELD before:" << std::endl;
  //std::cout << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] << std::endl;

  // Rotate the position to rotate the magnetic field in the simulation
  if((xangle < 10e-6 && xangle >- 10e-6) || (yangle < 10e-6 && yangle >- 10e-6)){
    G4double sx = sin(xangle);
    G4double cx = cos(xangle);
    
    // Rotate along x axis
    G4double tmp1 = Bfield[0];
    G4double tmp2 = Bfield[1]*cx - Bfield[2]*sx;
    Bfield[2] = Bfield[1]*sx + Bfield[2]*cx;
    Bfield[0] = tmp1;
    Bfield[1] = tmp2;
 
    // Rotate along y axis
 
    G4double sy = sin(yangle);
    G4double cy = cos(yangle);
    tmp1 = Bfield[0]*cy + Bfield[2]*sy;
    tmp2 = Bfield[1];
    Bfield[2] = -Bfield[0]*sx + Bfield[2]*cx;
    Bfield[0] = tmp1;
    Bfield[1] = tmp2;
  }
  //std::cout << "BFIELD after:" << std::endl;
  //std::cout << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] << std::endl;
}

hbarSextupoleField::hbarSextupoleField(hbarDetConst* aDet)
:hbarDetector(aDet)
{
  for (G4int i = 0; i < maxSN; i++) {
     B_r_max[i] = 1.0*tesla;
     B_zero[i] = 0.0;
     fieldFlip[i] = false;
  }
  Sextpt = -1;
  poleNum = 3;

  BharmonicsFileName = "fieldmaps/bnl/sx26mar06asa.harmonics.txt";  // not in use
  BzFileName = "fieldmaps/bnl/sx26mar06asa.field_inner.txt";        // not in use
  for (G4int i = 0; i < 21; i++) {
     B_harmonics_A[i] = new TGraph();
     B_harmonics_B[i] = new TGraph();
  }
  for (G4int i = 0; i < 10; i++) {
     B_z_table[i] = new TGraph2D();
  }

//  coeff = 0.0;
//  r_max = 10.0*mm;
////  Update();
//  r_max = hbarDetector->GetSextupoleConst()->GetSextupoleInnerDiameter();
//  coeff = B_r_max/sqr(r_max);
//  std::cout << "coeff: " << coeff << std::endl;
}

/*
hbarField::hbarField(G4Cons* aCons, G4double maxfield)
:sextupoleMagFieldSol(aCons)
{
  B_r_max = maxfield;
//  r_max = hbarDetector->GetSextupoleConst()->GetSextupoleInnerDiameter();
//  coeff = B_r_max/sqr(r_max);
}
*/

hbarSextupoleField::~hbarSextupoleField()
{
}

void hbarSextupoleField::GetFieldValue(const double PointOrig[3], double *Bfield) const
{
// PointOrig: world coordinates (i.e. not with respect to the sextupole volume center)
// Bfield: 0-2: B vector
//         3-5: grad(|B|) vector

  // Sextupole field
  for (G4int i = 0; i < maxSN; i++) {
  	  // check whether the i-th sextupole is in use at all
     if (useSext[i]) {
     	  // check whether the point is within the i-th sextupole
        if ( (PointOrig[2] <= sextEnd[i]) && (PointOrig[2] >= sextStart[i]) ) {
           G4double Point[] = {PointOrig[0] - sextOffsetX[i], PointOrig[1] - sextOffsetY[i], PointOrig[2]};
	   

           G4double r = sqrt(sqr(Point[0]) + sqr(Point[1]));
           G4double r_max = 0.5*(r_max_rear[i]*(Point[2] - sextStart[i]) + r_max_front[i]*(sextEnd[i] - Point[2]))/halfLength[i];
           if (r <= r_max) {
              G4double Bdir = 1.0;
              if (fieldFlip[i]) Bdir = -1.0;

              G4double coeff2 = B_r_max[i]/(pow(r_max, poleNum-1));

              G4double pos[] = {Point[0], Point[1], Point[2]};       // position of particle

              G4double deltapos_up[]   = {Point[0], Point[1], Point[2]};
              G4double deltapos_down[] = {Point[0], Point[1], Point[2]};
              G4double deltaDist = 0.1*mm;
              G4double Bfield_up[3];
              G4double Bfield_down[3];

	      G4double delta[3] = {deltaDist, 0*mm, 0*mm};
	      
	      // rotate offset point for caculating the gradient in x direction
	      rotate(delta, sextRotX[i], sextRotY[i]);

              GetFieldValueBasic(pos, Bfield, Bdir*coeff2, B_zero[i]);

              deltapos_up[0] = Point[0] - delta[0];
              deltapos_up[1] = Point[1] - delta[1];
              deltapos_up[2] = Point[2] - delta[2];
              deltapos_down[0] = Point[0] + delta[0];
              deltapos_down[1] = Point[1] + delta[1];
              deltapos_down[2] = Point[2] + delta[2];

              GetFieldValueBasic(deltapos_up, Bfield_up, Bdir*coeff2, B_zero[i]);
              GetFieldValueBasic(deltapos_down, Bfield_down, Bdir*coeff2, B_zero[i]);

              Bfield[6] = (sqrt(Bfield_up[0]*Bfield_up[0] + Bfield_up[1]*Bfield_up[1] + Bfield_up[2]*Bfield_up[2]) - sqrt(Bfield_down[0]*Bfield_down[0] + Bfield_down[1]*Bfield_down[1] + Bfield_down[2]*Bfield_down[2]))/(2.0*deltaDist);


	      delta[0] = 0*mm;
	      delta[1] = deltaDist;
	      delta[2] = 0*mm;
	      // rotate offset point for caculating the gradient in y direction
	      rotate(delta, sextRotX[i], sextRotY[i]);
	      deltapos_up[0] = Point[0] - delta[0];
              deltapos_up[1] = Point[1] - delta[1];
              deltapos_up[2] = Point[2] - delta[2];
              deltapos_down[0] = Point[0] + delta[0];
              deltapos_down[1] = Point[1] + delta[1];
              deltapos_down[2] = Point[2] + delta[2];

              GetFieldValueBasic(deltapos_up, Bfield_up, Bdir*coeff2, B_zero[i]);
              GetFieldValueBasic(deltapos_down, Bfield_down, Bdir*coeff2, B_zero[i]);

              Bfield[7] = (sqrt(Bfield_up[0]*Bfield_up[0] + Bfield_up[1]*Bfield_up[1] + Bfield_up[2]*Bfield_up[2]) - sqrt(Bfield_down[0]*Bfield_down[0] + Bfield_down[1]*Bfield_down[1] + Bfield_down[2]*Bfield_down[2]))/(2.0*deltaDist);


	      delta[0] = 0*mm;
	      delta[1] = 0*mm;
	      delta[2] = deltaDist;
	      // rotate offset point for caculating the gradient in z direction
	      rotate(delta, sextRotX[i], sextRotY[i]);
	      deltapos_up[0] = Point[0] - delta[0];
              deltapos_up[1] = Point[1] - delta[1];
              deltapos_up[2] = Point[2] - delta[2];
              deltapos_down[0] = Point[0] + delta[0];
              deltapos_down[1] = Point[1] + delta[1];
              deltapos_down[2] = Point[2] + delta[2];

              GetFieldValueBasic(deltapos_up, Bfield_up, Bdir*coeff2, B_zero[i]);
              GetFieldValueBasic(deltapos_down, Bfield_down, Bdir*coeff2, B_zero[i]);

              Bfield[8] = (sqrt(Bfield_up[0]*Bfield_up[0] + Bfield_up[1]*Bfield_up[1] + Bfield_up[2]*Bfield_up[2]) - sqrt(Bfield_down[0]*Bfield_down[0] + Bfield_down[1]*Bfield_down[1] + Bfield_down[2]*Bfield_down[2]))/(2.0*deltaDist);

              //Bfield[1] = Bdir*2.0*coeff*Point[1]*Point[2];
              //Bfield[2] = Bdir*coeff*(sqr(Point[1]) - sqr(Point[2]));
              //Bfield[4] = 2.0*coeff*Point[1];
              //Bfield[5] = 2.0*coeff*Point[2];

	      // now rotate the full bfield
	      rotate(Bfield, sextRotX[i], sextRotY[i]);
              /*std::cout << "Bfield[0]: " <<  Bfield[0]/tesla << std::endl;
              std::cout << "Bfield[1]: " <<  Bfield[1]/tesla << std::endl;
              std::cout << "Bfield[2]: " <<  Bfield[2]/tesla << std::endl;
              std::cout << "Bfield[3]: " <<  Bfield[3] << std::endl;
              std::cout << "Bfield[4]: " <<  Bfield[4] << std::endl;
              std::cout << "Bfield[5]: " <<  Bfield[5] << std::endl;*/
           }
           break;
        }
     }	
  }
}
  
void hbarSextupoleField::GetFieldValueBasic(const G4double Point[3], G4double Bfield[], G4double coeff, G4double B_null) const
{
   G4double B_r, B_phi;
   CLHEP::Hep2Vector vect(Point[0], Point[1]);
   G4double r   = vect.r();
   G4double phi = vect.phi();
   B_r   = pow(r, poleNum-1)*sin(poleNum*phi);
   B_phi = pow(r, poleNum-1)*cos(poleNum*phi);
   //CLHEP::Hep2Vector Bvect(B_r, B_phi);
   //G4double Bangle = Bvect.phi();
   Bfield[2] = B_null;
   Bfield[0] = coeff*(B_r*cos(phi) - B_phi*sin(phi));
   Bfield[1] = coeff*(B_r*sin(phi) + B_phi*cos(phi));
 
}

G4double hbarSextupoleField::GetFieldAngle(G4double x, G4double y)
{
   CLHEP::Hep2Vector vect(x, y);
   G4double r   = vect.r();
   G4double phi = vect.phi();
   G4double B_r   = pow(r, poleNum-1)*sin(poleNum*phi);
   G4double B_phi = pow(r, poleNum-1)*cos(poleNum*phi);
//   Hep2Vector Bvect(B_r*cos(phi) - B_phi*sin(phi), B_r*sin(phi) + B_phi*cos(phi));
   CLHEP::Hep2Vector Bvect2(B_r, B_phi);
//   std::cout << Bvect.phi() << " " << Bvect2.phi() + phi << std::endl;
   return Bvect2.phi() + phi;
   //Bfield[2] = coeff*B_z;
   //Bfield[0] = coeff*(B_r*cos(phi) - B_phi*sin(phi));
   //Bfield[1] = coeff*(B_r*sin(phi) + B_phi*cos(phi));
}

void hbarSextupoleField::CurrentSextupole(G4int num)
{
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      Sextpt = num-1;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
}

void hbarSextupoleField::Update() {
  if (hbarDetector == NULL) {
     G4cout << "NULL detector! Magnetic field cannot be updated." << G4endl;
     return;
  }
  for (G4int i = 0; i < maxSN; i++) {
     useSext[i] = hbarDetector->GetSextupoleConst()->IsUseSextupole(i+1);
     if (useSext[i]) {
        sextStart[i]   = hbarDetector->GetSextupoleConst()->GetSextupoleStart(i+1);
        sextEnd[i]     = hbarDetector->GetSextupoleConst()->GetSextupoleEnd(i+1);
        sextOffsetX[i] = hbarDetector->GetSextupoleConst()->GetSextupoleOffsetX(i+1);
        sextOffsetY[i] = hbarDetector->GetSextupoleConst()->GetSextupoleOffsetY(i+1);
        r_max_front[i] = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleFrontInnerDiameter(i+1);
        r_max_rear[i]  = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleRearInnerDiameter(i+1);
        halfLength[i]  = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleLength(i+1);
	sextRotX[i]    = hbarDetector->GetSextupoleConst()->GetSextupoleRotationX(i+1)*(M_PI/180);
	sextRotY[i]    = hbarDetector->GetSextupoleConst()->GetSextupoleRotationY(i+1)*(M_PI/180);
     }
  }
//  coeff = B_r_max/(r_max*r_max);
}

void hbarSextupoleField::SetFieldMaxValue(G4int num, G4double val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      B_r_max[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
//  Update();
}

void hbarSextupoleField::SetFieldMaxValue(G4double val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      B_r_max[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldMaxValue(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarSextupoleField::SetZeroField(G4int num, G4double val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      B_zero[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
//  Update();
}

void hbarSextupoleField::SetZeroField(G4double val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      B_zero[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldMaxValue(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarSextupoleField::SetFieldFlip(G4int num, G4bool val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      fieldFlip[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
}

void hbarSextupoleField::SetFieldFlip(G4bool val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      fieldFlip[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldFlip(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarSextupoleField::SetFieldHalfPoleNumber(G4int val) {
   if (val > 0) {
      poleNum = val;
   } else {
      G4cout << "The half of the pole number must be a positive integer!" << G4endl;
   }
}

void hbarSextupoleField::ReadHarmonicsFile() {

}
