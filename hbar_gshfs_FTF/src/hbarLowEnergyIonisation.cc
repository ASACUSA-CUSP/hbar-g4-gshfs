#include "hbarLowEnergyIonisation.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
hbarLowEnergyIonisation::hbarLowEnergyIonisation(const G4String& name) 
	: G4hIonisation(name)
{
	//InitializeMe();
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
/*void hbarLowEnergyIonisation::InitializeMe() {
	
}*/
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
hbarLowEnergyIonisation::~hbarLowEnergyIonisation() {
	
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
G4bool hbarLowEnergyIonisation::IsApplicable(const G4ParticleDefinition& aparticle) {	
	return ((aparticle.GetParticleName()=="antihydrogen"||"hydrogen") ? true : false);
}			    
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//					    
G4VParticleChange* hbarLowEnergyIonisation::AlongStepDoIt(const G4Track& trackData, const G4Step& stepData) {				
  (void)stepData; ///To avoid compiler warnings.
	aParticleChange.Initialize(trackData);	
	//G4ForceCondition condition = NotForced;
	//G4double previousStepSize =;
	const G4Material* material = trackData.GetMaterial();
	const G4double steplength = trackData.GetStep()->GetStepLength();
	//const G4double intlength = this->GetCurrentInteractionLength();
	
	G4double materialpartdensity = material->GetTotNbOfAtomsPerVolume();
	G4double d = 1.1*CLHEP::angstrom; // diameter of a hydrogen atom
	G4double mfp = 1/(materialpartdensity*M_PI*d*d); // estimation of mena free path (kinetic gas theory)
	
	//std::cout << " steplength: " << steplength/CLHEP::m << " intlength = " << intlength/CLHEP::m << " mfp: " << mfp/CLHEP::m << " material: " << material->GetName() << std::endl;
	
	if(steplength > mfp) {
	//	std::cout << "hallo!" << std::endl;
		aParticleChange.ProposeTrackStatus(fStopButAlive);
	}
	else {}
	//std::cout << "********************************************************************************** status " << aParticleChange.GetTrackStatus()<< std::endl;
	return &aParticleChange;					    
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
/*G4double hbarLowEnergyIonisation::AlongStepGetPhysicalInteractionLength(
                             const G4Track& trackData,G4double,G4double,G4double&,
                             G4GPILSelection* selection)
{
	const G4Material* material = trackData.GetMaterial();
	G4double materialpartdensity = material->GetTotNbOfAtomsPerVolume();
	//G4double d = trackData.GetDynamicParticle()->GetDefinition()->GetPDGWidth();
	G4double d = 1.1*CLHEP::angstrom;
	//std::cout << "*************************** " << d/CLHEP::m << std::endl;
	G4double mfp = 1/(materialpartdensity*M_PI*d);

 return mfp;
}		   	*/			    
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
/*G4double hbarLowEnergyIonisation::GetMeanFreePath(const G4Track& trackData,
							  G4double, // previousStepSize
							  enum G4ForceCondition* condition)
{
  const G4DynamicParticle* aParticle = trackData.GetDynamicParticle();
  const G4MaterialCutsCouple* couple = trackData.GetMaterialCutsCouple();

  // material is currently not used!
  // const G4Material* material = couple->GetMaterial();

  G4double meanFreePath;
  G4bool isOutRange ;
  G4double charge = -1.0;
  G4double chargeSquare = 1.0;
  *condition = NotForced ;

  G4double kineticEnergy = (aParticle->GetKineticEnergy());
  // Modified by Berti
  charge = -1.0;
  //   charge = aParticle->GetCharge()/eplus;
  chargeSquare = 1.0;
  //   chargeSquare = theIonEffChargeModel->TheValue(aParticle, material);

  if(kineticEnergy < LowestKineticEnergy) meanFreePath = DBL_MAX;

  else {
    if(kineticEnergy > HighestKineticEnergy)
      kineticEnergy = HighestKineticEnergy;
    meanFreePath = (((*theMeanFreePathTable)(couple->GetIndex()))->
                    GetValue(kineticEnergy,isOutRange))/chargeSquare;
  }

  return meanFreePath ;
}
*/
