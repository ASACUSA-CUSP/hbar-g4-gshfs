#include "hbarStackAction.hh"
#include "hbarStackActionMess.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4ios.hh"

hbarStackAction::hbarStackAction()
:processFlag("all")
{
   stackMessenger = new hbarStackActionMess(this);
}

hbarStackAction::~hbarStackAction()
{
   delete stackMessenger;
}

G4ClassificationOfNewTrack hbarStackAction::ClassifyNewTrack(const G4Track * aTrack)
{
  G4ClassificationOfNewTrack classification = fKill;

  // Do not track if the particle is a recoiled heavy ion because it crashes the program
  // These ions have a name in the form "Fe56[0.0]"
  // Is this still true? Let's test it - 11.07.2013 - Clemens
  // Yes it is - 15.08.2013 - Rikard

  if (aTrack->GetDefinition()->GetParticleName().contains("[")) {
  //     G4cout << aTrack->GetDefinition()->GetParticleName() << " killed." << G4endl;
	return classification;
  }
 
  if ((processFlag == "primary") && (aTrack->GetParentID()==0))
  { 
    classification = fUrgent; 
  }

//  if ((processFlag == "secondary") && (aTrack->GetParentID()<=1))
//  { classification = fUrgent; }

//  if ((processFlag == "tertiary") && (aTrack->GetParentID()<=2))
//  { classification = fUrgent; }

  if ((processFlag == "all") && !(aTrack->GetDefinition()->GetParticleName().contains("nu")))
  { 
    classification = fUrgent; 
  }

  if (processFlag == "allwithneutrino")
    { 
      classification = fUrgent; 
    }

  if ((processFlag == "pbar") && (aTrack->GetDefinition()->GetParticleName() == "anti_proton"))
  { 
    classification = fUrgent; 
  }

  if ((processFlag == "hbar") && (aTrack->GetDefinition()->GetParticleName().contains("antihydrogen")))
  { 
    classification = fUrgent; 
  }


  if ((processFlag == "charged") && (aTrack->GetDefinition()->GetPDGCharge() != 0.))
  { 
    classification = fUrgent; 
  }

  if ((processFlag == "heavy") && (aTrack->GetDefinition()->GetParticleName() != "gamma")
              && (aTrack->GetDefinition()->GetParticleName() != "e+")
              && (aTrack->GetDefinition()->GetParticleName() != "e-"))
  { 
    classification = fUrgent; 
  }

  if ((processFlag == "fermion") && (aTrack->GetDefinition()->GetParticleName() != "gamma"))
  { 
    classification = fUrgent; 
  }

  if ((processFlag == "allbute+e-")
              && (aTrack->GetDefinition()->GetParticleName() != "e+")
              && (aTrack->GetDefinition()->GetParticleName() != "e-"))
  { 
    classification = fUrgent; 
  }

  return classification;
}

