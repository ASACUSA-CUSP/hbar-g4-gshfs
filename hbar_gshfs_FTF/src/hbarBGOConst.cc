#include "hbarBGOConst.hh"

hbarBGOConst::hbarBGOConst()
  :UseHydrogenTarget(false), useSupportBox(false)
{
  memset(useScintillator, 0, sizeof(bool)*maxNDplsciTrd);
  memset(useSensitiveScintillator, 0, sizeof(bool)*maxNDplsciTrd);
  BGOMessenger = new hbarBGOMess(this);
}

hbarBGOConst::~hbarBGOConst()
{
  if(BGOMessenger)
	delete BGOMessenger;
}

void hbarBGOConst::MakeBGO(G4VPhysicalVolume * WorldPhy, G4double _CenterOfTheMikiLense, hbarDetectorSD * scintiSD)
{
  CenterOfTheMikiLense = _CenterOfTheMikiLense;
  MakeTarget(WorldPhy, scintiSD);
  MakeCover(WorldPhy);
  MakeScintillators(WorldPhy, scintiSD);
  if(useSupportBox)
	MakeSupportBox(WorldPhy);
}

void hbarBGOConst::MakeTarget(G4VPhysicalVolume * WorldPhy, hbarDetectorSD * scintiSD)
{
  G4String targetMaterial = UseHydrogenTarget ? "hydro" : "BGO";

  G4Tubs* NDBGO = new G4Tubs("NDBGO",TargetRinRoutLen.x()*0.5,TargetRinRoutLen.y()*0.5, TargetRinRoutLen.z()*0.5,0.0,2.0*M_PI);

  NDBGOLog = new G4LogicalVolume(NDBGO,hbarDetConst::GetMaterial(targetMaterial),"NDBGO",0,0,0);

  NDBGOPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheMikiLense+112.0*mm+TargetRinRoutLen.z()*0.5),
							   NDBGOLog,"NDBGO",WorldPhy->GetLogicalVolume(),false,0);

  G4VisAttributes* bluecolor = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
  NDBGOLog->SetVisAttributes(bluecolor);
  NDBGOLog->SetSensitiveDetector(scintiSD);
}

void hbarBGOConst::MakeCover(G4VPhysicalVolume * WorldPhy)
{
  G4Tubs * flangePrimitive[2];
  for(int i = 0; i<2; ++i)
	flangePrimitive[i] = new G4Tubs("NDflange" + numtostr(i),FlangeRinRoutLen.x()*0.5,FlangeRinRoutLen.y()*0.5,FlangeRinRoutLen.z()*0.5,0.0,2.0*M_PI);

  G4UnionSolid* flange = new G4UnionSolid("NDflange",flangePrimitive[0], flangePrimitive[1],0,G4ThreeVector(0.0*mm,0.0*mm,2.0*FlangeRinRoutLen.z()));


  G4Tubs * coverPrimitive[3];
  for(int i = 0; i<3; ++i)
	coverPrimitive[i] = new G4Tubs("NDcover" + numtostr(i) ,CoverRinRoutLen[i].x()*0.5, CoverRinRoutLen[i].y()*0.5,CoverRinRoutLen[i].z()*0.5,0.0,2.0*M_PI);

  G4UnionSolid* coverU1 = new G4UnionSolid("NDcoverU1",coverPrimitive[0],coverPrimitive[1],0,
											 G4ThreeVector(0.0*mm,0.0*mm,(CoverRinRoutLen[0].z() + CoverRinRoutLen[1].z())*0.5));

  G4UnionSolid* coverU2 = new G4UnionSolid("NDcoverU2",coverU1, coverPrimitive[2],0,
											 G4ThreeVector(0.0*mm,0.0*mm,(CoverRinRoutLen[0].z() + CoverRinRoutLen[1].z() + CoverRinRoutLen[2].z())*0.5));

  G4UnionSolid* cover = new G4UnionSolid("NDcover",flange,coverU2,0,G4ThreeVector(0.0*mm,0.0*mm,
																				  (FlangeRinRoutLen.z() + CoverRinRoutLen[0].z())*0.5));

  NDcoverLog = new G4LogicalVolume(cover,hbarDetConst::GetMaterial("Sts"),"NDcover",0,0,0);
  NDcoverPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheMikiLense+100.0*mm+FlangeRinRoutLen.z()),
								 NDcoverLog,"NDcover05",WorldPhy->GetLogicalVolume(),false,0);

}


///This function, I do not fully understand. So it's hard to clean further. //R
void hbarBGOConst::MakeScintillators(G4VPhysicalVolume * WorldPhy, hbarDetectorSD * scintiSD)
{
  G4RotationMatrix* RotateXPi2 = new G4RotationMatrix();
  RotateXPi2 -> rotateX(0.5*M_PI);

  for (G4int i=0; i<5; i++)
	{
	  G4String ScintillatorBoxName = "BGOScintillatorBox"+ numtostr(i);
	  ScintillatorBox[i]=new G4Box(ScintillatorBoxName,
							  0.5*ScintillatorBoxDimensionsXYZ[i].x(),
							  0.5*ScintillatorBoxDimensionsXYZ[i].y(),
							  0.5*ScintillatorBoxDimensionsXYZ[i].z()
							  );

	  G4String TrapezoidName = "BGOScintillatorTrapezoid"+ numtostr(i);
	  ScintillatorTrapezoid[i]=new G4Trd(TrapezoidName,
										 ScintillatorTrapezoidX1[i]*0.5,
										 ScintillatorTrapezoidX2[i]*0.5,
										 ScintillatorTrapezoidY1[i]*0.5,
										 ScintillatorTrapezoidY2[i]*0.5,
										 ScintillatorTrapezoidZ[i]*0.5);
	}

    G4String ScintillatorBoxName = "BGOScintillatorBox"+ numtostr(5);
	  ScintillatorBox[5]=new G4Box(ScintillatorBoxName,
							  0.5*ScintillatorBoxDimensionsXYZ[5].x(),
							  0.5*ScintillatorBoxDimensionsXYZ[5].y(),
							  0.5*ScintillatorBoxDimensionsXYZ[5].z()
							  );

  if(useScintillator[0]) ///Front scintillator.
	{
	  G4VSolid * tmpScintillator  = new G4SubtractionSolid("BGOScintillatorT1",
										  ScintillatorBox[0],
										  ScintillatorBox[1],0,
										  G4ThreeVector(0.0*mm,
														-ScintillatorBoxDimensionsXYZ[0].y()*0.5
														+ScintillatorBoxDimensionsXYZ[1].y()*0.5
														-2.0*mm,
														0.0*mm));

	  Scintillator[0] = new G4UnionSolid("BGOFrontScintillator",
										 tmpScintillator,
										 ScintillatorTrapezoid[0],
										 RotateXPi2,
										 G4ThreeVector(0.0*mm,
													   ScintillatorBoxDimensionsXYZ[0].y()*0.5
													   + ScintillatorTrapezoidZ[0]*0.5,
													   0.0*mm));

	  ScintillatorLog[0] = new G4LogicalVolume(Scintillator[0],hbarDetConst::GetMaterial("Scintillator"),"ScintillatorFront",0,0,0);

	  ScintillatorPhy[0] = new G4PVPlacement(0,G4ThreeVector(0.0*mm,
															 ScintillatorBoxDimensionsXYZ[0].y()*0.5-17.0*mm,
															 CenterOfTheMikiLense+15.0*mm+ScintillatorBoxDimensionsXYZ[0].z()*0.5),
											 ScintillatorLog[0],
											 "ScintillatorFront",
											 WorldPhy->GetLogicalVolume(),
											 false,0);
	}

  if(useScintillator[1]) ///Top scintillator.
	{
	  Scintillator[1] = new G4UnionSolid("BGOTopScintillator",ScintillatorBox[2],ScintillatorTrapezoid[1],0,
									G4ThreeVector(0.0*mm,
												  0.0*mm,
												  0.5*(ScintillatorTrapezoidZ[1] + ScintillatorBoxDimensionsXYZ[1].z()
													   ))
										 );

	  ScintillatorLog[1] = new G4LogicalVolume(Scintillator[1],hbarDetConst::GetMaterial("Scintillator"),"ScintillatorTop",0,0,0);

	  ScintillatorPhy[1] = new G4PVPlacement(0,G4ThreeVector(0.0*mm,
															 .5*ScintillatorBoxDimensionsXYZ[2].y()  +110.0*mm,
															 CenterOfTheMikiLense+40.0*mm+.5*ScintillatorBoxDimensionsXYZ[2].z()),
											 ScintillatorLog[1],"ScintillatorTop",WorldPhy->GetLogicalVolume(),false,0);
	}

  if(useScintillator[2]) ///Left scintillator
	{
	  Scintillator[2] = new G4UnionSolid("BGOLeftScintillator",ScintillatorBox[3],ScintillatorTrapezoid[2],0,
									G4ThreeVector(0.0*mm,
												  0.0*mm,
												  .5*ScintillatorTrapezoidZ[2] + .5*ScintillatorBoxDimensionsXYZ[3].z()
												  ));
	  ScintillatorLog[2] = new G4LogicalVolume(Scintillator[2],hbarDetConst::GetMaterial("Scintillator"),"BGOLeftScintillator",0,0,0);
	  ScintillatorPhy[2] = new G4PVPlacement(0,G4ThreeVector(
															 .5*ScintillatorBoxDimensionsXYZ[3].x() +150.0*mm,
															 -.5*ScintillatorBoxDimensionsXYZ[3].y() +110.0*mm,
															 CenterOfTheMikiLense+25.0*mm+
															 .5*ScintillatorBoxDimensionsXYZ[3].z()
															 ),
											 ScintillatorLog[2],"ScintillatorLeft",WorldPhy->GetLogicalVolume(),false,0);
	}

  if(useScintillator[3]) ///Right scintillator.
	{
	  Scintillator[3] = new G4UnionSolid("BGORightScintillator",ScintillatorBox[4],ScintillatorTrapezoid[3],0,
									G4ThreeVector(0.0*mm,0.0*mm,
												  ScintillatorBoxDimensionsXYZ[3].z()*.5 +
												  ScintillatorTrapezoidZ[3]*.5 +
												  ScintillatorBoxDimensionsXYZ[4].z()*.5)
										 );
	  ScintillatorLog[3] = new G4LogicalVolume(Scintillator[3],hbarDetConst::GetMaterial("Scintillator"),"BGORightScintillator",0,0,0);
	  ScintillatorPhy[3] = new G4PVPlacement(0,G4ThreeVector(-ScintillatorBoxDimensionsXYZ[4].z()*.5-150.0*mm,
															 -ScintillatorBoxDimensionsXYZ[4].y()*.5+110.0*mm,
															 CenterOfTheMikiLense+25.0*mm+ScintillatorBoxDimensionsXYZ[4].z()*.5
															 ),
											 ScintillatorLog[2],"ScintillatorRight",WorldPhy->GetLogicalVolume(),false,0);
	}

  if(useScintillator[4]) ///Top scintillator.
	{
	  G4double NDplsciHoleOutRad = 45.0*mm;
	  G4Tubs* NDplsciHole = new G4Tubs("ScintillatorHole",0.0*mm,NDplsciHoleOutRad,ScintillatorTrapezoidZ[4]*.5+2.0*mm,0.0,2.0*M_PI);
	  G4Box* NDplsciHoleSub = new G4Box("ScintillatorSubHole",50.0*mm,10.0*mm,ScintillatorBoxDimensionsXYZ[5].z()*.5+4.0*mm);
	  G4SubtractionSolid* NDplsciSub = new G4SubtractionSolid("ScintillatorWithHole",NDplsciHole,NDplsciHoleSub,0,
															  G4ThreeVector(0.0*mm,-40.0*mm-10.0*mm,0.0*mm));

	  Scintillator[4] = new G4SubtractionSolid("ScintillatorStrange",ScintillatorBox[5],NDplsciSub,0,
											   G4ThreeVector(0.0*mm,-110.0*mm+ScintillatorBoxDimensionsXYZ[5].y()*.5,0.0*mm));

	  Scintillator[6] = new G4UnionSolid("ScintillatorFunny",Scintillator[4],ScintillatorTrapezoid[4],RotateXPi2,
									G4ThreeVector(0.0*mm,-ScintillatorBoxDimensionsXYZ[5].y()*.5-ScintillatorTrapezoidZ[4]*.5,0.0*mm));

	  ScintillatorLog[4] = new G4LogicalVolume(Scintillator[6],hbarDetConst::GetMaterial("Scintillator"),"ScintillatorBack",0,0,0);

	  ScintillatorPhy[4] = new G4PVPlacement(0,G4ThreeVector(0.0*mm,-ScintillatorBoxDimensionsXYZ[5].y()*.5+110.0*mm,
															 CenterOfTheMikiLense+ScintillatorBoxDimensionsXYZ[5].z()*.5+405.0*mm),
										ScintillatorLog[4],"ScintillatorBack",WorldPhy->GetLogicalVolume(),false,0);
	}

  for(G4int i=0;i<5;i++)
	{
	  if( useScintillator[i] )
		{
		  G4VisAttributes* orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));
		  ScintillatorLog[i]->SetVisAttributes(orangecolor);
		  if(useSensitiveScintillator[i] != 0)
			{
			  ScintillatorLog[i]->SetSensitiveDetector(scintiSD);
			}
		}
	}
}

void hbarBGOConst::MakeSupportBox(G4VPhysicalVolume * WorldPhy)
{
  G4double HeightOfTopSurfaceOfSupportBox = -67.0*mm;

  G4int NDsupBoxpr = 0;
  for(G4int i=0;i<maxNDsupSum;i++)
	{
	  G4String NDsupBoxName = "SupportBox_"+ numtostr(i);
	  NDsupBoxpr = (i>2) + (i>6) + (i>10) + (i>14) + (i>18) + (i>20) + (i>24) + (i>28) + (i>30) - 9*(i>33);
	  SupportBox[i]=new G4Box(NDsupBoxName,
							SupportBoxDimensionXYZ[NDsupBoxpr].x()*0.5,
							SupportBoxDimensionXYZ[NDsupBoxpr].y()*0.5,
							SupportBoxDimensionXYZ[NDsupBoxpr].z()*0.5);
	  NDsupBoxLog[i]=new G4LogicalVolume(SupportBox[i],hbarDetConst::GetMaterial("Aluminium"),NDsupBoxName,0,0,0);
	}

  G4double SupportBoxX = SupportBoxDimensionXYZ[0].x()*0.5+SupportBoxDimensionXYZ[2].x()*0.5;
  G4double SupportBoxZ = -SupportBoxDimensionXYZ[0].z()*0.5+SupportBoxDimensionXYZ[2].z()*0.5;

  G4ThreeVector SupportBoxOrigin = G4ThreeVector(0.0*mm,HeightOfTopSurfaceOfSupportBox,CenterOfTheMikiLense-SupportBoxZ);

  G4double NDsupBoxY[4];

  NDsupBoxY[0] = -SupportBoxDimensionXYZ[0].y();
  for(G4int i=1;i<=3;i++)
	{
	  NDsupBoxY[i]=NDsupBoxY[i-1]-(SupportBoxDimensionXYZ[i-1].y()+SupportBoxDimensionXYZ[i].y())*0.5;
	  switch(i)
		{
		case 1:
		  NDsupBoxY[i]-=SupportBoxDistanceBetweenBars.x();
		  break;
		case 2:
		  NDsupBoxY[i]-=SupportBoxDistanceBetweenBars.y();
		  break;
		case 3:
		  NDsupBoxY[i]-=SupportBoxDistanceBetweenBars.z();
		  break;
		}
	}

  G4ThreeVector NDsupBoxPos[maxNDsupSum];
  NDsupBoxPos[0]=G4ThreeVector(0.0*mm,NDsupBoxY[0],SupportBoxZ);
  for(G4int i=1;i<8;++i)
	{
	  if(i<4)
		{
		  NDsupBoxPos[2*i-1]=G4ThreeVector(0.0*mm,NDsupBoxY[i],-SupportBoxZ);
		  NDsupBoxPos[2*i]=G4ThreeVector(0.0*mm,NDsupBoxY[i],SupportBoxZ);
		}
	  else
		{
		  NDsupBoxPos[2*i-1]=G4ThreeVector(SupportBoxX,NDsupBoxY[i-4],0.0*mm);
		  NDsupBoxPos[2*i]=G4ThreeVector(-SupportBoxX,NDsupBoxY[i-4],0.0*mm);
		}
	}

  NDsupBoxPos[15]=G4ThreeVector(SupportBoxX,NDsupBoxY[0]-SupportBoxDimensionXYZ[0].y()-SupportBoxDimensionXYZ[4].y(),-SupportBoxZ);
  NDsupBoxPos[16]=G4ThreeVector(-SupportBoxX,NDsupBoxY[0]-SupportBoxDimensionXYZ[0].y()-SupportBoxDimensionXYZ[4].y(),-SupportBoxZ);
  NDsupBoxPos[17]=G4ThreeVector(SupportBoxX,NDsupBoxY[0]-SupportBoxDimensionXYZ[0].y()-SupportBoxDimensionXYZ[4].y(),SupportBoxZ);
  NDsupBoxPos[18]=G4ThreeVector(-SupportBoxX,NDsupBoxY[0]-SupportBoxDimensionXYZ[0].y()-SupportBoxDimensionXYZ[4].y(),SupportBoxZ);

  G4double NDsupPlsciX0 = ScintillatorBoxDimensionsXYZ[2].x()+SupportBoxDimensionXYZ[5].x();
  G4double NDsupPlsciX1 = 150.0*mm+2.0*ScintillatorBoxDimensionsXYZ[4].x()-SupportBoxDimensionXYZ[6].x();
  G4double NDsupPlsciX2 = ScintillatorBoxDimensionsXYZ[0].x()+SupportBoxDimensionXYZ[7].x();
  G4double NDsupPlsciY0 = 110.0*mm+SupportBoxDimensionXYZ[5].y();
  G4double NDsupPlsciY1 = 110.0*mm+SupportBoxDimensionXYZ[6].y();
  G4double NDsupPlsciY2 = -50.0*mm-SupportBoxDimensionXYZ[6].y();
  G4double NDsupPlsciY3 = -17.0*mm+SupportBoxDimensionXYZ[7].y();
  G4double NDsupPlsciY4 = 110.0*mm-SupportBoxDimensionXYZ[7].y();
  G4double NDsupPlsciZ0 = CenterOfTheMikiLense+40.0*mm+SupportBoxDimensionXYZ[5].z();
  G4double NDsupPlsciZ1 = CenterOfTheMikiLense+25.0*mm+SupportBoxDimensionXYZ[6].z();
  G4double NDsupPlsciZ2 = CenterOfTheMikiLense+15.0*mm+SupportBoxDimensionXYZ[7].z();
  G4double NDsupPlsciZ3 = CenterOfTheMikiLense+405.0*mm+2.0*ScintillatorBoxDimensionsXYZ[5].z()-SupportBoxDimensionXYZ[7].z();

  NDsupBoxPos[19]=G4ThreeVector(NDsupPlsciX0,NDsupPlsciY0,NDsupPlsciZ0);
  NDsupBoxPos[20]=G4ThreeVector(-NDsupPlsciX0,NDsupPlsciY0,NDsupPlsciZ0);
  NDsupBoxPos[21]=G4ThreeVector(NDsupPlsciX1,NDsupPlsciY1,NDsupPlsciZ1);
  NDsupBoxPos[22]=G4ThreeVector(-NDsupPlsciX1,NDsupPlsciY1,NDsupPlsciZ1);
  NDsupBoxPos[23]=G4ThreeVector(NDsupPlsciX1,NDsupPlsciY2,NDsupPlsciZ1);
  NDsupBoxPos[24]=G4ThreeVector(-NDsupPlsciX1,NDsupPlsciY2,NDsupPlsciZ1);
  NDsupBoxPos[25]=G4ThreeVector(NDsupPlsciX2,NDsupPlsciY3,NDsupPlsciZ2);
  NDsupBoxPos[26]=G4ThreeVector(-NDsupPlsciX2,NDsupPlsciY3,NDsupPlsciZ2);
  NDsupBoxPos[27]=G4ThreeVector(NDsupPlsciX2,NDsupPlsciY4,NDsupPlsciZ3);
  NDsupBoxPos[28]=G4ThreeVector(-NDsupPlsciX2,NDsupPlsciY4,NDsupPlsciZ3);

  NDsupBoxPos[29]=G4ThreeVector(160.0*mm+SupportBoxDimensionXYZ[8].x(),NDsupBoxY[1]+SupportBoxDimensionXYZ[0].y()+SupportBoxDimensionXYZ[8].y(),-SupportBoxZ);
  NDsupBoxPos[30]=G4ThreeVector(-160.0*mm-SupportBoxDimensionXYZ[8].x(),NDsupBoxY[1]+SupportBoxDimensionXYZ[0].y()+SupportBoxDimensionXYZ[8].y(),-SupportBoxZ);
  NDsupBoxPos[31]=G4ThreeVector(160.0*mm+SupportBoxDimensionXYZ[9].x(),SupportBoxDimensionXYZ[9].y(),SupportBoxZ);
  NDsupBoxPos[32]=G4ThreeVector(-160.0*mm-SupportBoxDimensionXYZ[9].x(),SupportBoxDimensionXYZ[9].y(),SupportBoxZ);

  for(G4int i=0;i<maxNDsupSum;i++)
	{
	  G4String NDsupBoxName = "BGOSupportBox_"+ numtostr(i);
	  if(i<19 || i>28)
		{
		  NDsupBoxPos[i] = NDsupBoxPos[i]+SupportBoxOrigin;
		}
	  NDsupBoxPhy[i]=new G4PVPlacement(0,NDsupBoxPos[i],NDsupBoxLog[i],NDsupBoxName,WorldPhy->GetLogicalVolume(),false,0);
	}
  for(G4int i=0;i<maxNDsupPlate;i++)
	{
	  G4String NDsupPlateName = "BGOSupportPlate_"+ numtostr(i);
	  NDsupPlate[i] = new G4Box(NDsupPlateName,SupportBoxDimensionXYZ[0].x(),0.5*5.0*mm,SupportBoxDimensionXYZ[0].x());
	  NDsupPlateLog[i]= new G4LogicalVolume(NDsupPlate[i],hbarDetConst::GetMaterial("Aluminium"),NDsupPlateName,0,0,0);
	  G4ThreeVector temp= SupportBoxOrigin+G4ThreeVector(0.0*mm,NDsupBoxY[2+i]-15.0*mm,0.0*mm);
	  NDsupPlatePhy[i]= new G4PVPlacement(0,temp,NDsupPlateLog[i],NDsupPlateName,WorldPhy->GetLogicalVolume(),false,0);
	}
}
