#include "numtostr.hh"

std::string numtostr(int number, int decs, char fillc) {
  std::stringstream res;
  if (decs < 0) decs = 1;
  double intpartw = double(abs(number));
  int numdigs = 0;
  while (floor(intpartw) > 0.0) {
    intpartw /= 10.0;
    numdigs++;
  }
  if (number == 0) numdigs = 1;
  if (decs > 1) {
    res.width(decs);
    res.fill(fillc);
  }
  //res << number << '\0';
  res << number;
  //const char *buf = res.str().c_str();
  //return std::string(buf);
  return res.str();
}

std::string numtostr(long number, int decs, char fillc) {
  std::stringstream res;
  if (decs < 0) decs = 1;
  double intpartw = double(abs(number));
  int numdigs = 0;
  while (floor(intpartw) > 0.0) {
    intpartw /= 10.0;
    numdigs++;
  }
  if (number == 0) numdigs = 1;
  if (decs > 1) {
    res.width(decs);
    res.fill(fillc);
  }
  //res << number << '\0';
  res << number;
  //const char *buf = res.str().c_str();
  //return std::string(buf);
  return res.str();
}

std::string numtostr(double number, int decs, bool force) {
  std::stringstream res;
  if (decs < 0) decs = 3;
  double intpartw = double(abs(int(number)));
  int numdigs = 0;
  while (floor(intpartw) > 0.0) {
    intpartw /= 10.0;
    numdigs++;
  }
  if (double(number) == 0.0) numdigs = 1;
  res.precision(decs+numdigs);
  if (force) res.setf(std::ios::showpoint);
  else res.unsetf(std::ios::showpoint);
  //res << double(number) << '\0';
  res << double(number);
  //const char *buf = res.str().c_str();
  //return std::string(buf);
  return res.str();
}


double GetOne(double val) {
  val = 1.0;
  return val;
}




