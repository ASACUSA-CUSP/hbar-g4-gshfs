#include "G4ThreeVector.hh"
#include "G4Cons.hh"

#include "hbarField.hh"
#include "hbarSextupoleField.hh"
#include "hbarDetConst.hh"
#include "hbarCavityField.hh"

#include "CLHEP/Vector/TwoVector.h"

hbarField::hbarField(hbarDetConst* aDet)
  :hbarSextupoleField(aDet),hbarDetector(aDet),
  cuspfield(NULL),cavityfield(NULL), ExtFIField(NULL)
{
  gradmu = new G4double[3];
  mu     = new G4double;
  ExtFIField = new hbarExtFIField();
  
}


hbarField::~hbarField()
{
  delete [] gradmu;
  delete mu;
}

void hbarField::GetFieldValue(const double PointOrig[3], double *Bfield) const
{
  // PointOrig: world coordinates (i.e. not with respect to the sextupole volume center)

  Bfield[0]  = 0.0;   // Bx
  Bfield[1]  = 0.0;   // By
  Bfield[2]  = 0.0;   // Bz
  Bfield[3]  = 0.0;   // Ex
  Bfield[4]  = 0.0;   // Ey
  Bfield[5]  = 0.0;   // Ez

  Bfield[6]  = 0.0;   // grad Bx
  Bfield[7]  = 0.0;   // grad By
  Bfield[8]  = 0.0;   // grad Bz
  Bfield[9]  = 0.0;   // grad Ex
  Bfield[0]  = 0.0;   // grad Ey
  Bfield[11] = 0.0;   // grad Ez

  Bfield[12] = *mu;       // magnetic moment of particle
  Bfield[13] = gradmu[0]; // gradient of magnetic moment x
  Bfield[14] = gradmu[1]; // gradient of magnetic moment y
  Bfield[15] = gradmu[2]; // gradient of magnetic moment z

  // Sextupole field
  for (G4int i = 0; i < maxSN; i++) {
  	  // check whether the i-th sextupole is in use at all
     if (useSext[i]) {
     	  // check whether the point is within the i-th sextupole
        if ( (PointOrig[2] <= sextEnd[i]) && (PointOrig[2] >= sextStart[i]) ) {
	  hbarSextupoleField::GetFieldValue( PointOrig,Bfield);
	  break;
        }
     }
  } // Sextupole end

  //double cuspfieldpos = hbarDetector->GetMWCavityCenter()-
  //hbarDetector->GetMWCavityLength()*2;
  //if(cuspfield != NULL && PointOrig[2] < cuspfieldpos ){
  if(cuspfield != NULL ){
    if(PointOrig[2] < cuspfield->GetZOffset()+std::abs(cuspfield->GetMaxZ())){
      cuspfield->GetFieldValue(PointOrig,Bfield);
      cuspfield->CaluculateGradient(PointOrig,Bfield+6);
    }
  } // cuspfield end

 if(1){
    double ficenter = ExtFIField->GetExtFIcenter();
    //std::cout << " not NULL HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << ficenter - 0.1*m << " " <<  PointOrig[2] << std::endl;
    if ( (PointOrig[2] >= ficenter - 0.005*m) && (PointOrig[2] <= ficenter + 0.005*m) ) {
        ExtFIField->GetFieldValue(PointOrig,Bfield);
    //  cuspfield->GetFieldValue(PointOrig,Bfield);
    //  cuspfield->CaluculateGradient(PointOrig,Bfield+6);
    //std::cout << " in " << ficenter - 0.005*m << " " << ficenter + 0.005*m << " " <<  PointOrig[2] << std::endl;
    }
  } // ExtFIfield end


  
  
  Bfield[0]-=4.8e-8; // earth magnetic field! but not in the cavity ()
  
  
  G4double cavitylength = hbarDetector->GetCavityConst()->GetMWCavityLength();
  if(cavityfield != NULL) { // if there exists a cavity fieldmap
	  if((PointOrig[2]>-cavitylength*0.5) && (PointOrig[2]<cavitylength*0.5)) { // check wether particle is in the cavity
		  cavityfield->GetFieldValue(PointOrig,Bfield);	 // set field at this point
		  //Bfield[2]-=4.8e-5*tesla; 
	  }
  }
	//std::cout << "Bfield hbarField.cc : " << Bfield[0] << "	" << Bfield[1] << "	" << Bfield[2] << std::endl;
  //std::cout << "mu2=" << Bfield[12] << std::endl;
  /*for(int i=0; i<16; i++){
    std::cout << Bfield[i] << " " ;
  }
  std::cout << std::endl;*/
}


void hbarField::Update() {
  if (hbarDetector == NULL) {
     G4cout << "NULL detector! Magnetic field cannot be updated." << G4endl;
     return;
  }
  hbarSextupoleField::Update();
}


