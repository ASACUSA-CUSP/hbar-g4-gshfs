#include "G4ThreeVector.hh"
#include "G4Cons.hh"

#include "hbarField.hh"
#include "hbarDetConst.hh"

#include "CLHEP/Vector/TwoVector.h"

// local helper function, rotates a vector 
/*void rotate(G4double Bfield[3], G4double xangle, G4double yangle){
  //std::cout << "BFIELD before:" << std::endl;
  //std::cout << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] << std::endl;

  // Rotate the position to rotate the magnetic field in the simulation
  if((xangle < 10e-6 && xangle >- 10e-6) || (yangle < 10e-6 && yangle >- 10e-6)){
    G4double sx = sin(xangle);
    G4double cx = cos(xangle);
    
    // Rotate along x axis
    G4double tmp1 = Bfield[0];
    G4double tmp2 = Bfield[1]*cx - Bfield[2]*sx;
    Bfield[2] = Bfield[1]*sx + Bfield[2]*cx;
    Bfield[0] = tmp1;
    Bfield[1] = tmp2;
 
    // Rotate along y axis
 
    G4double sy = sin(yangle);
    G4double cy = cos(yangle);
    tmp1 = Bfield[0]*cy + Bfield[2]*sy;
    tmp2 = Bfield[1];
    Bfield[2] = -Bfield[0]*sx + Bfield[2]*cx;
    Bfield[0] = tmp1;
    Bfield[1] = tmp2;
  }
  //std::cout << "BFIELD after:" << std::endl;
  //std::cout << Bfield[0] << " " << Bfield[1] << " " << Bfield[2] << std::endl;
}
*/
hbarExtFIField::hbarExtFIField()
{

ExtFIcenter = -0.408*m;
UpstreamVolt = 5.0*kilovolt;
DownstreamVolt = -5.0*kilovolt;

UpstreamVolt = 0.0*kilovolt;
DownstreamVolt = -4.0*0.001*kilovolt;

}

/*
hbarField::hbarField(G4Cons* aCons, G4double maxfield)
:sextupoleMagFieldSol(aCons)
{
  B_r_max = maxfield;
//  r_max = hbarDetector->GetSextupoleConst()->GetSextupoleInnerDiameter();
//  coeff = B_r_max/sqr(r_max);
}
*/

hbarExtFIField::~hbarExtFIField()
{
}

void hbarExtFIField::GetFieldValue(const double PointOrig[3], double *Bfield) const
{
// PointOrig: world coordinates (i.e. not with respect to the sextupole volume center)
// Bfield: 0-2: B vector
//         3-5: grad(|B|) vector

  //std::cout << " in " << PointOrig << " " << Bfield[3] << " " <<   std::endl;

  Bfield[3]  = 0.0;   // Ex
  Bfield[4]  = 0.0;   // Ey
  
  Bfield[5]  = (UpstreamVolt - DownstreamVolt)/(1.0*cm);   // Ez

}
  
/*void hbarExtFIField::GetFieldValueBasic(const G4double Point[3], G4double Bfield[], G4double coeff, G4double B_null) const
{
   G4double B_r, B_phi;
   CLHEP::Hep2Vector vect(Point[0], Point[1]);
   G4double r   = vect.r();
   G4double phi = vect.phi();
   B_r   = pow(r, poleNum-1)*sin(poleNum*phi);
   B_phi = pow(r, poleNum-1)*cos(poleNum*phi);
   //CLHEP::Hep2Vector Bvect(B_r, B_phi);
   //G4double Bangle = Bvect.phi();
   Bfield[2] = B_null;
   Bfield[0] = coeff*(B_r*cos(phi) - B_phi*sin(phi));
   Bfield[1] = coeff*(B_r*sin(phi) + B_phi*cos(phi));
 
}*/

/*G4double hbarExtFIField::GetFieldAngle(G4double x, G4double y)
{
   CLHEP::Hep2Vector vect(x, y);
   G4double r   = vect.r();
   G4double phi = vect.phi();
   G4double B_r   = pow(r, poleNum-1)*sin(poleNum*phi);
   G4double B_phi = pow(r, poleNum-1)*cos(poleNum*phi);
//   Hep2Vector Bvect(B_r*cos(phi) - B_phi*sin(phi), B_r*sin(phi) + B_phi*cos(phi));
   CLHEP::Hep2Vector Bvect2(B_r, B_phi);
//   std::cout << Bvect.phi() << " " << Bvect2.phi() + phi << std::endl;
   return Bvect2.phi() + phi;
   //Bfield[2] = coeff*B_z;
   //Bfield[0] = coeff*(B_r*cos(phi) - B_phi*sin(phi));
   //Bfield[1] = coeff*(B_r*sin(phi) + B_phi*cos(phi));
}*/


void hbarExtFIField::Update() {
  if (hbarDetector == NULL) {
     G4cout << "NULL detector! Magnetic field cannot be updated." << G4endl;
     return;
  }
  /*for (G4int i = 0; i < maxSN; i++) {
     useSext[i] = hbarDetector->GetSextupoleConst()->IsUseSextupole(i+1);
     if (useSext[i]) {
        sextStart[i]   = hbarDetector->GetSextupoleConst()->GetSextupoleStart(i+1);
        sextEnd[i]     = hbarDetector->GetSextupoleConst()->GetSextupoleEnd(i+1);
        sextOffsetX[i] = hbarDetector->GetSextupoleConst()->GetSextupoleOffsetX(i+1);
        sextOffsetY[i] = hbarDetector->GetSextupoleConst()->GetSextupoleOffsetY(i+1);
        r_max_front[i] = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleFrontInnerDiameter(i+1);
        r_max_rear[i]  = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleRearInnerDiameter(i+1);
        halfLength[i]  = 0.5*hbarDetector->GetSextupoleConst()->GetSextupoleLength(i+1);
	sextRotX[i]    = hbarDetector->GetSextupoleConst()->GetSextupoleRotationX(i+1)*(M_PI/180);
	sextRotY[i]    = hbarDetector->GetSextupoleConst()->GetSextupoleRotationY(i+1)*(M_PI/180);
     }
  }*/
//  coeff = B_r_max/(r_max*r_max);
}

/*void hbarExtFIField::SetFieldMaxValue(G4int num, G4double val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      B_r_max[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
//  Update();
}

void hbarExtFIField::SetFieldMaxValue(G4double val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      B_r_max[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldMaxValue(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarExtFIField::SetZeroField(G4int num, G4double val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      B_zero[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
//  Update();
}

void hbarExtFIField::SetZeroField(G4double val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      B_zero[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldMaxValue(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarExtFIField::SetFieldFlip(G4int num, G4bool val) {
   if ((num-1 >= 0) && (num-1 < maxSN)) {
      fieldFlip[num-1] = val;
   } else {
      G4cout << "Sextupole number " << num << " is out of range!" << G4endl;
   }
}

void hbarExtFIField::SetFieldFlip(G4bool val) {
   if ((Sextpt >= 0) && (Sextpt < maxSN)) {
      fieldFlip[Sextpt] = val;
   } else {
      G4cout << "hbarField::SetFieldFlip(): Don't know which sextupole to set!" << G4endl;
   }
}

void hbarExtFIField::SetFieldHalfPoleNumber(G4int val) {
   if (val > 0) {
      poleNum = val;
   } else {
      G4cout << "The half of the pole number must be a positive integer!" << G4endl;
   }
}

void hbarExtFIField::ReadHarmonicsFile() {

}*/
