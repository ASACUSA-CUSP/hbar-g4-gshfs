#include "hbarPhysicsList.hh"

hbarPhysicsList::hbarPhysicsList() : G4VModularPhysicsList(),
				     fEmPhysicsList(0),
				     fDecPhysicsList(0), 
				     fHadronPhysInElast(0),
				     fHadronPhysElst(0),
				     fMessenger(0),
				     hbarMessenger(0)
{
  G4LossTableManager::Instance();
  defaultCutValue = 0.5*mm;
  fCutForGamma     = defaultCutValue;
  fCutForElectron  = defaultCutValue;
  fCutForPositron  = defaultCutValue;

  fMessenger = new G4UserPhysicsListMessenger(this);
  hbarMessenger = new hbarPhysicsListMess(this);
 
  
  SetVerboseLevel(0);
  
  // EM physics
  //fEmPhysicsList    = new G4EmStandardPhysics_option4(1);
  
  // Deacy physics and all particles
  fDecPhysicsList    = new G4DecayPhysics(0);
  
  // Hadronic models
  //
  fHadronPhysInElast = NULL;
  fHadronPhysElst    = new G4HadronElasticPhysics(0);

  // SMI Annihilation process
  useSMIModel = true;
  
  //UseOptPhotons = true;
}

hbarPhysicsList::~hbarPhysicsList()
{
  delete fMessenger;
  delete hbarMessenger;
  if(fEmPhysicsList != NULL)
    delete fEmPhysicsList;
  delete fDecPhysicsList;
  if(fHadronPhysInElast != NULL)
    delete fHadronPhysInElast;
  delete fHadronPhysElst;
}

void hbarPhysicsList::ConstructParticle()
{
  fDecPhysicsList->ConstructParticle();

  G4OpticalPhoton::OpticalPhotonDefinition();

  // construct Hydrogen and Antihydrogen particles
  hbarAntiHydrogen::Definition();
  hbarHydrogen::Definition();

}

void hbarPhysicsList::ConstructProcess()
{
  // transportation
  // 
  
  AddModTransportation();
  
  // electromagnetic physics list
  // constructed by macro
 
  // decay physics list
  //
  fDecPhysicsList->ConstructProcess();
  
   // hadronic physics lists
  fHadronPhysElst->ConstructProcess();
  // inelastic process constructed by macro

  // step limitation (as a full process)
  AddStepLimiter();
  
  

  //G4EmProcessOptions opt;
  //opt.SetVerbose(verbose);

  // allow optical photon processes
  // ConstructOpt();

  // Hydrogen and Antihydrogen processes
  // constructed by macro
}


void hbarPhysicsList::AddStepLimiter()
{
  // Add the Geant4 step limiter to every particle
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  GetParticleIterator()->reset();
  while ((*GetParticleIterator())()){  
    G4ParticleDefinition* particle = GetParticleIterator()->value();
    ph->RegisterProcess(new G4StepLimiter, particle);     
  }
}


void hbarPhysicsList::SetCuts()
{
  if (verboseLevel >0) 
	{
	  G4cout << "hbarPhysicsList::SetCuts:";
	  G4cout << "CutLength : " << G4BestUnit(defaultCutValue,"Length") << G4endl;
	}

  // set cut values for gamma at first and for e- second and next for e+,
  // because some processes for e+/e- need cut values for gamma
  SetCutValue(fCutForGamma, "gamma");
  SetCutValue(fCutForElectron, "e-");
  SetCutValue(fCutForPositron, "e+");

  if (verboseLevel>0) 
	DumpCutValuesTable();
}


void hbarPhysicsList::SetCutForGamma(G4double cut)
{
  fCutForGamma = cut;
  SetParticleCuts(fCutForGamma, G4Gamma::Gamma());
}


void hbarPhysicsList::SetCutForElectron(G4double cut)
{
  fCutForElectron = cut;
  SetParticleCuts(fCutForElectron, G4Electron::Electron());
}


void hbarPhysicsList::SetCutForPositron(G4double cut)
{
  fCutForPositron = cut;
  SetParticleCuts(fCutForPositron, G4Positron::Positron());
}


void hbarPhysicsList::AddIonGasModels()
{
  G4EmConfigurator* em_config = G4LossTableManager::Instance()->EmConfigurator();
  GetParticleIterator()->reset();
  while ((*GetParticleIterator())())
  {
    G4ParticleDefinition* particle = GetParticleIterator()->value();
    G4String partname = particle->GetParticleName();
    if(partname == "alpha" || partname == "He3" || partname == "GenericIon") 
	  {
      G4BraggIonGasModel* mod1 = new G4BraggIonGasModel();
      G4BetheBlochIonGasModel* mod2 = new G4BetheBlochIonGasModel();
      G4double eth = 2.*MeV*particle->GetPDGMass()/proton_mass_c2;
      em_config->SetExtraEmModel(partname,"ionIoni",mod1,"",0.0,eth,
                                 new G4IonFluctuations());
      em_config->SetExtraEmModel(partname,"ionIoni",mod2,"",eth,100*TeV,
                                 new G4UniversalFluctuation());

    }
  }
}

void hbarPhysicsList::AddHydrogenAntihydrogenProcess(){
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  GetParticleIterator()->reset();
  while( (*GetParticleIterator())() )
	{ // Particle iterator  
	  G4ParticleDefinition* particle = GetParticleIterator()->value();
	  G4String particleName = particle->GetParticleName();
	  
	  if (particleName == ("hydrogen"))
		{
		  ph->RegisterProcess(new hbarLowEnergyIonisation("hbarLowEIonisation"), particle);
		  ph->RegisterProcess(new hbarHydrogenDecayAtRest(), particle);
		} 
	  else if(particleName == ("antihydrogen")) 
		{
		  ph->RegisterProcess(new hbarLowEnergyIonisation("hbarLowEIonisation"), particle);
		  ph->RegisterProcess(new hbarAntiHydrogenDecayAtRest(), particle);
		} 
	  else if(particleName == ("anti_proton")) 
		{
		  if(!useSMIModel)
			ph->RegisterProcess(new G4AntiProtonAbsorptionFritiof, particle);
		  else
			ph->RegisterProcess(new hbarAntiProtonAnnihilationAtRest, particle);
		}
	} // Particle iterator end
}

void hbarPhysicsList::AddModTransportation()
{
  // Add the transportation process with enabeling the use of magnetic moments
  // for Hydrogen and Antihydrogen
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  GetParticleIterator()->reset();
  while ((*GetParticleIterator())())
	{  
	  G4ParticleDefinition* particle = GetParticleIterator()->value();
	  G4String particleName = particle->GetParticleName();
	  G4Transportation* trans = new G4Transportation(0);
	  if(particleName == ("hydrogen") || particleName == ("antihydrogen"))
		{
		  trans->EnableUseMagneticMoment(true);
		} 
	  else 
		{
		  trans->EnableUseMagneticMoment(false);
		}
	  ph->RegisterProcess(trans, particle);     
	}
}

// Macro Commands
void hbarPhysicsList::SetElectromagneticList(G4String newValue){
  if(fEmPhysicsList == NULL)
	{
	  if(newValue == "emstandard_opt4")
		{
		  //fEmPhysicsList = new G4EmStandardPhysics(0);
		  fEmPhysicsList = new G4EmStandardPhysics_option4(0);
		} 
	  else if(newValue == "livermore")
		{
		fEmPhysicsList = new G4EmLivermorePhysics(0);
	  } 
	  else if(newValue == "livermore_polarized")
		{
		  fEmPhysicsList = new G4EmLivermorePolarizedPhysics(0);
	  } 
	  else if(newValue == "penelope")
		{
		  fEmPhysicsList = new G4EmPenelopePhysics(0);
		} 
	  else if(newValue == "dnaphysics")
		{
		  fEmPhysicsList = new G4EmDNAPhysics(0);
		} 
	  else 
		{
		  fEmPhysicsList = new G4EmStandardPhysics_option4(0);
		  //fEmPhysicsList = new G4EmStandardPhysics(0);
		  std::cerr << "Physicslist: " << newValue << " not found!"
					<< "fallback is emstandard_opt4" << std::endl;
		}
	  fEmPhysicsList->ConstructProcess();
	}
}

void hbarPhysicsList::SetHadronicList(G4String newValue){
  if(fHadronPhysInElast == NULL)
	{
	  if(newValue == "FTFP_BERT_TRV")
		{
		  fHadronPhysInElast = new G4HadronPhysicsFTFP_BERT_TRV(0);
		}
	  else if(newValue == "FTFP_BERT")
		{
		  fHadronPhysInElast = new G4HadronPhysicsFTFP_BERT(0);
		}
	  else if(newValue == "FTF_BIC")
		{
		  fHadronPhysInElast = new G4HadronPhysicsFTF_BIC(0);
		}
	  else if(newValue == "QGSP_FTFP_BERT")
		{
		  fHadronPhysInElast = new G4HadronPhysicsQGSP_FTFP_BERT(0);
		}
	  else if(newValue == "QGSP_BERT")
		{
		  fHadronPhysInElast = new G4HadronPhysicsQGSP_BERT(0);
		}
	  else if(newValue == "QGSP_BIC")
		{
		  fHadronPhysInElast = new G4HadronPhysicsQGSP_BIC(0);
		}
	  else
		{
		  fHadronPhysInElast = new G4HadronPhysicsFTFP_BERT_TRV(0);
		  std::cerr << "Physicslist: " << newValue << " not found!"
					<< "fallback is FTFP_BERT_TRV" << std::endl;
		}
	  fHadronPhysInElast->ConstructProcess();
	}
}

void hbarPhysicsList::SetConstructOptPhotons(G4String newValue)
{
	if(newValue == "on") {

		theCerenkovProcess           = new G4Cerenkov("Cerenkov");
		theScintillationProcess      = new G4Scintillation("Scintillation");
		theAbsorptionProcess         = new G4OpAbsorption();
		theRayleighScatteringProcess = new G4OpRayleigh();
		theMieHGScatteringProcess    = new G4OpMieHG();
		theBoundaryProcess           = new G4OpBoundaryProcess();

		SetVerboseOpt(0);
  
		theCerenkovProcess->SetMaxNumPhotonsPerStep(20);
		theCerenkovProcess->SetMaxBetaChangePerStep(10.0);
		theCerenkovProcess->SetTrackSecondariesFirst(true);
  
		theScintillationProcess->SetScintillationYieldFactor(1.);
		theScintillationProcess->SetTrackSecondariesFirst(true);

  // Use Birks Correction in the Scintillation process
		G4EmSaturation* emSaturation = G4LossTableManager::Instance()->EmSaturation();
		theScintillationProcess->AddSaturation(emSaturation);
 
		GetParticleIterator()->reset();

		while( (*GetParticleIterator())() ){
			G4ParticleDefinition* particle = GetParticleIterator()->value();
			G4ProcessManager* pmanager = particle->GetProcessManager();
			G4String particleName = particle->GetParticleName();
			if (theCerenkovProcess->IsApplicable(*particle)) {
				pmanager->AddProcess(theCerenkovProcess);
				pmanager->SetProcessOrdering(theCerenkovProcess,idxPostStep);
    		}
			if (theScintillationProcess->IsApplicable(*particle)) {
				pmanager->AddProcess(theScintillationProcess);
				pmanager->SetProcessOrderingToLast(theScintillationProcess, idxAtRest);
				pmanager->SetProcessOrderingToLast(theScintillationProcess, idxPostStep);
			}
			if (particleName == "opticalphoton") {
				G4cout << " AddDiscreteProcess to OpticalPhoton " << G4endl;
				pmanager->AddDiscreteProcess(theAbsorptionProcess);
				pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
				pmanager->AddDiscreteProcess(theMieHGScatteringProcess);
				pmanager->AddDiscreteProcess(theBoundaryProcess);
			}
		}
	}

	else {};
}

void hbarPhysicsList::SetVerboseOpt(G4int verbose)
{
  theCerenkovProcess->SetVerboseLevel(verbose);
  theScintillationProcess->SetVerboseLevel(verbose);
  theAbsorptionProcess->SetVerboseLevel(verbose);
  theRayleighScatteringProcess->SetVerboseLevel(verbose);
  theMieHGScatteringProcess->SetVerboseLevel(verbose);
  theBoundaryProcess->SetVerboseLevel(verbose);  
}

void hbarPhysicsList::SetUseSMICode(G4String newValue)
{
  if(newValue == "true") 
	useSMIModel = true;
  else 
	useSMIModel = false;
  AddHydrogenAntihydrogenProcess();
}

