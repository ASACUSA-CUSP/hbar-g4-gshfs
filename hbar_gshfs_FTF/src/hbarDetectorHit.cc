#include "hbarDetectorHit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"


G4Allocator<hbarDetectorHit> hbarDetectorHitAllocator;

hbarDetectorHit::hbarDetectorHit() {
  Event = 0;
  Hit = 0;
  Edep = 0.0;
  Ekin = 0.0;
  Time = 0.0;
  Pos = G4ThreeVector(0.0, 0.0, 0.0);
  Momentumdir = G4ThreeVector(0.0, 0.0, 0.0);
  Particle = "";
  Detector = "";
  PDGcode = 0;
  
  // tajima
  track_id = 0;
  parent_id = 0;

}

hbarDetectorHit::~hbarDetectorHit()
{;}

hbarDetectorHit::hbarDetectorHit(const hbarDetectorHit &right)
  : G4VHit()
{
  Event = right.Event;
  Hit = right.Hit;
  Edep = right.Edep;
  Ekin = right.Ekin;
  Time = right.Time;
  Pos = right.Pos;
  Momentumdir = right.Momentumdir;
  Particle = G4String(right.Particle);
  PDGcode = right.PDGcode;
  
  //std::cout << "hbardetector hit class (cc)  1" << Particle << std::endl;
  //std::cout << "hbardetector hit class (cc)  1" << PDG_code << std::endl;
  
  
  Detector = G4String(right.Detector);
  
  // tajima
  track_id = right.track_id;
  parent_id = right.parent_id;
}

const hbarDetectorHit& hbarDetectorHit::operator=(const hbarDetectorHit &right)
{
  Event = right.Event;
  Hit = right.Hit;
  Edep = right.Edep;
  Ekin = right.Ekin;
  Time = right.Time;
  Pos = right.Pos;
  Momentumdir = right.Momentumdir;
  Particle = right.Particle;
  //PDGcode = right.PDGcode;
  Detector = G4String(right.Detector);
  
  //std::cout << "hbardetector hit class (cc)  " << Particle << std::endl;
  //std::cout << "hbardetector hit class (cc)  " << PDG_code << std::endl;

  // tajima
  track_id = right.track_id;
  parent_id = right.parent_id;

  return *this;
}

G4int hbarDetectorHit::operator==(const hbarDetectorHit &right) const
{
  return (this==&right) ? 1 : 0;
}

void hbarDetectorHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(Pos);
    circle.SetScreenSize(0.04);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,0.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

void hbarDetectorHit::Print()
{;}


