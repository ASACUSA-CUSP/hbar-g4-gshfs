// USER //
#include "hbarSiPM_SD.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

//FFT
#include "TH1.h"
#include "TVirtualFFT.h"


// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

hbarSiPM_SD::hbarSiPM_SD(G4String SDname, hbarCPTConst* cptconst)
  : G4VSensitiveDetector(SDname), hitCollection(0)
{
  //G4cout << "Creating SD with name: " << SDname << G4endl;
  //std::string hitcolname = "SiHitCollection";

  SiPMName = SDname;
  drawhistos = false;
  write_to_dat_files = false;
  collectionName.insert(SDname);

  localcptconst = cptconst;
  response_time = cptconst->Get_responsefkt_x();
  response_sig = cptconst->Get_responsefkt_y();

  histo_resp = cptconst->Gethisto_responsefkt();

	/*if(drawhistos==true) {
		G4String histo_name2 = "histo_resp";
		std::string dump;
		TCanvas *c1 = new TCanvas("c1", "c1");
		histo_resp->GetXaxis()->SetTitle("time (ns)");
		histo_resp->GetYaxis()->SetTitle("signal (mV)");
		histo_resp->Draw();

   		mkdir("./data_scintil/hits", 0700);

		dump = "data_scintil/hits/" + histo_name2;
		dump += "_resp.pdf";
		c1->Print(dump.c_str());
		delete c1;
	}*/
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
hbarSiPM_SD::~hbarSiPM_SD()
{
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

void hbarSiPM_SD::Initialize(G4HCofThisEvent* HCE)
{
  //G4cout << "create new HitCollection " << GetName() << " " << collectionName[0] << G4endl;
  hitCollection = new hbarDetectorHitsCollection(GetName(), collectionName[0]);

  static G4int HCID = -1;
  //if (HCID<0) { HCID = GetCollectionID(0); }
  if (HCID<0)
    { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(hitCollection); }
  HCE->AddHitsCollection(HCID, hitCollection);

  hitCounter = 0;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
G4bool hbarSiPM_SD::ProcessHits(G4Step * step, G4TouchableHistory* ROhist)
{
  G4TouchableHandle touchable = step->GetPreStepPoint()->GetTouchableHandle();
	//G4int copyNo = touchable->GetVolume()->GetCopyNo();
	//G4VPhysicalVolume* physical = touchable->GetVolume();
  track = step->GetTrack();
  G4String volume_name = track->GetVolume()->GetName();
  G4String part_name = track->GetParticleDefinition()->GetParticleName();
  double Etot = track->GetTotalEnergy()/CLHEP::eV;
  double wavelength = 1.2398/Etot*1e3; // wavelength in nm

  bool pdebool = CalculatePDE(wavelength);
   // G4cout << track->GetParticleDefinition()->GetParticleName() << G4endl;
  if(pdebool==true) {
	if( step->GetPreStepPoint()->GetStepStatus() == fGeomBoundary && part_name == "opticalphoton") {  

   	 track->SetTrackStatus(fKillTrackAndSecondaries);
   	 hitCounter++;
    
   	 if(volume_name.contains("Up_SiPM"))
   	 {
			SiPM_up_timeData.push_back(step->GetPreStepPoint()->GetGlobalTime()/ns);
			SiPM_up_energyData.push_back(step->GetPreStepPoint()->GetTotalEnergy()/eV);
   	 }
   	 if(volume_name.contains("Down_SiPM"))
   	 {
			SiPM_down_timeData.push_back(step->GetPreStepPoint()->GetGlobalTime()/ns);
			SiPM_down_energyData.push_back(step->GetPreStepPoint()->GetTotalEnergy()/eV);
   	 }
    }
 }
  // G4int copyNo = touchable->GetReplicaNumber();
  // G4int plane = touchable->GetReplicaNumber(1);
  if (step->GetTotalEnergyDeposit() <= 0.) return false;

  return true;
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
void hbarSiPM_SD::EndOfEvent(G4HCofThisEvent* HE)
{
  int evtnumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  std::string strev;
  std::stringstream out;
  out << evtnumber;
  strev = out.str();

  G4cout << "***Event: " << evtnumber << " ... Number of tracks in '" << GetName() << "':	" << hitCounter << G4endl;

  // Write hit data to files
  G4String histo_name;
  G4String volume_name = GetName();
  G4String fileName;
  
  std::vector<G4double> xbins;
  G4double binwidth = 0.2; //0.2 ns,  200 ps
  Int_t xbins_size = 4096;
  std::string dump;
  std::vector<G4double> temp_TimeData;

  // The effect of any linear, shift-invariant system on an arbitrary input signal is obtained by
  //  convolving the input signal (photons in sipms) with the response of the system to a unit impulse (single photon)
  // To do: 
  // noise ganz am ende drauf - nach conv und back fft

	HodorWaveformgr = new TGraph(xbins_size);

	if(volume_name.contains("SiPM_"))
    {
		if(volume_name.contains("_up_")) temp_TimeData = SiPM_up_timeData;
		else if(volume_name.contains("_down_")) temp_TimeData = SiPM_down_timeData;
		
		histo_name = volume_name + "_evno" + strev + "_histo";
		G4String mag_name = volume_name + strev + "mag";
		G4String ph_name = volume_name + strev + "ph";
		G4String re_name = volume_name + strev + "re";
		
		G4double min_val = 0;
		if (temp_TimeData.size()>0) min_val = *std::min_element(temp_TimeData.begin(), temp_TimeData.end()); // if -> otherwise segfault

		TH1D* up_histo = new TH1D(histo_name, histo_name, xbins_size, 0, xbins_size*binwidth);

		if(temp_TimeData.size()>0) {
			 for(size_t i=0; i<temp_TimeData.size(); i++) {
				up_histo->Fill((temp_TimeData[i] - min_val + 5)); //5 ns, so we have a baseline
			} 
		}

		if(drawhistos==true && temp_TimeData.size() > 0) {
			gErrorIgnoreLevel = 2000;
			TCanvas *c1 = new TCanvas("c1", "c1");
			up_histo->Draw();
			//up_histo->GetXaxis()->SetLimits(0,200);
			up_histo->GetXaxis()->SetTitle("time (ns)");
			up_histo->GetYaxis()->SetTitle("counts");
			dump = "data_scintil/hits/" + histo_name;
			dump += ".pdf";
			c1->Print(dump.c_str());
			delete c1;
		}

   TH1 *hm = 0;
   TVirtualFFT::SetTransform(0);
   hm = histo_resp->FFT(hm, mag_name);

   //
   TVirtualFFT *fft_resp = TVirtualFFT::GetCurrentTransform();
   Double_t *re_resp = new Double_t[xbins_size];
   Double_t *im_resp = new Double_t[xbins_size];
   fft_resp->GetPointsComplex(re_resp, im_resp);

	// transformation of the simulated signal
   TH1 *hm1 =0;
   TVirtualFFT::SetTransform(0);
   mag_name+="1";
   hm1 = up_histo->FFT(hm1, mag_name);

   TVirtualFFT *fft_sim = TVirtualFFT::GetCurrentTransform();
   Double_t *re_sim = new Double_t[xbins_size];
   Double_t *im_sim = new Double_t[xbins_size];
   fft_sim->GetPointsComplex(re_sim, im_sim);

	   /// multiply fft of response and fft of simulated signal
	for(int i=0; i<xbins_size; i++) {
		mult_fft_rl[i] = 1.0/sqrt(xbins_size)*(re_resp[i]*re_sim[i] - im_resp[i]*im_sim[i]);
		mult_fft_img[i] = 1.0/sqrt(xbins_size)*(re_resp[i]*im_sim[i] + im_resp[i]*re_sim[i]);
	}

	TVirtualFFT *fft_back = TVirtualFFT::FFT(1, &xbins_size, "C2R EX K");
  	fft_back->SetPointsComplex(mult_fft_rl,mult_fft_img);
   	fft_back->Transform();	 

	fft_back->GetPointsComplex(mult_fft_rl,mult_fft_img);

	for(int i=0; i<xbins_size; i++) {
		mult_fft_rl[i] *= 1.0/sqrt(xbins_size);
		mult_fft_img[i] *= 1.0/sqrt(xbins_size);
   	 }


	if(drawhistos==true && temp_TimeData.size() > 0) {
		gErrorIgnoreLevel = 2000;
		TCanvas *c3 = new TCanvas("c3", "c3");
		HodorWaveformgr = new TGraph(xbins_size, &response_time[0], mult_fft_rl);
		HodorWaveformgr->Draw("al");
		HodorWaveformgr->GetXaxis()->SetTitle("time (ns)");
		HodorWaveformgr->GetYaxis()->SetTitle("signal (V)");
		HodorWaveformgr->GetXaxis()->SetLimits(0,200);
		dump = "data_scintil/hits/" + histo_name;
		dump += "_conv.pdf";
		c3->Print(dump.c_str());
		delete c3;
	}
	delete re_resp;
	delete re_sim;
	delete im_resp;
	delete im_sim;
	}

	for(int j=0; j<1024; j++) HodorWaveform[j] = mult_fft_rl[j];

	if(write_to_dat_files && temp_TimeData.size() > 0) {
		//std::string namedir = G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction()->GetParticleDefinition()->GetParticleName();
		mkdir("./data_scintil/hits", 0700);
		G4String fileName = "data_scintil/hits/" + volume_name + "_" + strev + ".dat";
    	std::ofstream fileOut(fileName);
		for(int i=0; i<1024; i++) fileOut << response_time[i] << "	" << HodorWaveform[i] << std::endl;

		fileName = "data_scintil/hits/" + volume_name + "_" + strev + "_Energy.dat";
    	std::ofstream fileOut1(fileName);
		for(int i=0; i<SiPM_up_energyData.size(); i++) fileOut1 << SiPM_up_energyData[i] << std::endl;

		
	}

	hitCounter = 0; // reset the hit counter after every event
	SiPM_up_timeData.clear();
    SiPM_down_timeData.clear();
    SiPM_up_energyData.clear();
    SiPM_down_energyData.clear();
	
 //G4cout << "EndOfEvent method of SD '" << GetName() << "' called. " << GetNumberOfEvent() << G4endl;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
bool hbarSiPM_SD::CalculatePDE(double wl) {
///read photon detection efficiency  //////////////////////
	ifstream inFile("data_scintil/pde_sipm_final.dat"); // PM3350TS
	
	int numberoflines=122;
	int lcount = 0;			
	double wltemp=0, pdetemp=0;
	std::string dump;

	std::vector<double> pde_x;
	std::vector<double> pde_y;

	while(!inFile.eof() && lcount != numberoflines) {
		inFile >> wltemp >> pdetemp;
		pde_x.push_back(wltemp);
		pde_y.push_back(pdetemp);
		lcount++; 	
	}
	//////////////////////////////////////////
	std::vector<double>::iterator upperwl = std::upper_bound(pde_x.begin(), pde_x.end(), wl);
	std::vector<double>::iterator lowerwl = upperwl - 1;

	double pde_lower = pde_y[(lowerwl - pde_x.begin())];
	double pde_upper = pde_y[(upperwl - pde_x.begin())];

	// linear interpolation
	double ret_pde = pde_lower + (pde_upper - pde_lower)*(wl - *lowerwl)/(*upperwl - *lowerwl);

	double rand_var =  G4UniformRand();   // uniform random number in [0,1]
	//std::cout << rand_var << std::endl;
	bool ret_bool = false;
	if(rand_var < ret_pde) ret_bool = true;
	//std::cout << wl << " lower: " << *lowerwl << "  " << pde_lower << " upper: " << *upperwl << " " << pde_upper << "   ret: " << ret_pde << std::endl;

	return ret_bool;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

double* hbarSiPM_SD::Get_mult_fft_rl() {
	return mult_fft_rl;
}

double* hbarSiPM_SD::GetHodorWaveform() {
	return HodorWaveform;
}

std::vector<double> hbarSiPM_SD::Get_response_time() {
	return response_time;
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...



/*TH1 *hb = 0;
   	//Let's look at the output
  	hb = TH1::TransformHisto(fft_back,hb,"Re");
	Double_t *xbin_val = new Double_t[xbins_size];
	Double_t *bin_content = new Double_t[xbins_size];
	hb->GetXaxis()->GetLowEdge(xbin_val);

	for(int k=0; k<xbins_size; k++) {
		bin_content[k] = hb->GetBinContent(k);
		bin_content[k] *= 1.0/sqrt(xbins_size);
	}
*/
/////////////////////////////////////////////////////
/*if(drawhistos==true && temp_TimeData.size() > 0) {
		TCanvas *c3 = new TCanvas("c3", "c3");	

		HodorWaveformgr = new TGraph(xbins_size, xbin_val, bin_content);
		HodorWaveformgr->Draw("al");
		HodorWaveformgr->GetXaxis()->SetTitle("time (ns)");
		HodorWaveformgr->GetYaxis()->SetTitle("signal (mV)");



		//hb->GetXaxis()->SetLimits(up_histo->GetXaxis()->GetXmin(), up_histo->GetXaxis()->GetXmax());  // rescale x axis
		//hb->Draw();
		//hb->GetXaxis()->SetLimits(0,200);
		dump = "data_scintil/hits/" + histo_name;
		dump += "_conv_test.pdf";
		c3->Print(dump.c_str());
		delete c3;
	}*/
/////////////////////////////////////////////////////


