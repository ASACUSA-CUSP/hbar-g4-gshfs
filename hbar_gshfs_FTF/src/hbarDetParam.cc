
#include "hbarDetParam.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Trd.hh"

hbarDetParam::hbarDetParam(G4double scintiThick, G4int layerNum, G4double leadThick)
{
   ScintiThick = scintiThick;
   LayerNum = layerNum;
   LeadThick = leadThick;
}

hbarDetParam::~hbarDetParam()
{;}

void hbarDetParam::ComputeTransformation
(const G4int copyNo, G4VPhysicalVolume *physVol) const
{
  G4double th = (ScintiThick + LeadThick)/(LayerNum + 1);
  //std::cout << "th: " << th << std::endl;
  G4ThreeVector origin(-ScintiThick/2.0 - LeadThick/2.0 + (copyNo+1)*th, 0.0, 0.0);
  physVol->SetTranslation(origin);
}

