#include "KDNode.hh"
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
KDNode::KDNode() {
	Axis = 0;
	Depth = 0;
	checked = false;
	LeftChild = std::tr1::shared_ptr<KDNode>();
	RightChild = std::tr1::shared_ptr<KDNode>();
	//for(int i=0; i<columns; i++) Point[i]=0.0;			
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
KDNode::~KDNode() {
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void KDNode::SetLeftChild(std::tr1::shared_ptr<KDNode> left) {
	LeftChild = left;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void KDNode::SetRightChild(std::tr1::shared_ptr<KDNode> right) {
	RightChild = right;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> KDNode::GetRightChild() {
	return RightChild;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> KDNode::GetLeftChild() {
	return LeftChild;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
double* KDNode::GetPoint() {
	return Point;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
int KDNode::GetDepth() {
	return Depth;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void KDNode::Setchecked(bool c) {
	checked = c;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
bool KDNode::Getchecked() {
	return checked;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
double KDNode::GetPoint(int where) {
	return Point[where];
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
int KDNode::GetAxis() {
	return Axis;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
KDNode& KDNode::operator=(KDNode const& rhs) { 
	if (this != &rhs) {
		Axis = rhs.Axis;
		Depth = rhs.Depth;
		LeftChild = rhs.LeftChild;
		RightChild = rhs.RightChild;
		checked = rhs.checked;
		for(int i=0;i<columns;i++) Point[i] = rhs.Point[i];	
	}
	return *this;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
KDNode::KDNode(double** Matrix, int length, int depth) { 
	Depth = depth; // set depth of tree
	checked = false; 								
	Axis=depth%DIM;	// Axis (column x, y or z) to sort by
	checked = false;
	//for(int i=0;i<columns;i++) Point[i] = 0.0;

	if(length==1) { // at the end of tree (leaf) length is *1* not *0*
	    for(int i=0; i<columns;i++) Point[i] = Matrix[0][i]; //Save Point of leaf
		LeftChild = std::tr1::shared_ptr<KDNode>();
		RightChild = std::tr1::shared_ptr<KDNode>();
		return;
	}

	MergeSort(Matrix,length,Axis);	// Sort 'Matrix' with 'length' according to current 'Axis'
	int nextlength = length/2;		// set length for next node

	for(int i=0; i<columns; i++) Point[i] = Matrix[nextlength][i]; // save Point at bisection of Matrix

	 LeftChild = std::tr1::shared_ptr<KDNode>(new KDNode(Matrix, nextlength, depth+1));
	 RightChild = std::tr1::shared_ptr<KDNode>(new KDNode(Matrix+nextlength, length-nextlength, depth+1));
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void MergeSort(double** matrixtosort, int length, int curraxis) {
	if(length>1) {
		int elementsInA1 = length/2;
		int elementsInA2 = ((length%2)==1) ? elementsInA1+1 : elementsInA1;
	
		double** arr1 = new double*[elementsInA1];
		for(int i=0; i<elementsInA1; i++) arr1[i] = new double[columns];
		double** arr2 = new double*[elementsInA2];
		for(int i=0; i<elementsInA2; i++) arr2[i] = new double[columns];

		for(int i=0; i<elementsInA1; i++) for(int j=0;j<columns; j++) arr1[i][j] = matrixtosort[i][j];
		for(int i=elementsInA1; i<elementsInA1+elementsInA2; i++) for(int j=0;j<columns; j++) arr2[i-elementsInA1][j] = matrixtosort[i][j];
			
		MergeSort(arr1,elementsInA1,curraxis);
		MergeSort(arr2,elementsInA2,curraxis);

		int i = 0, j = 0, k = 0;
		while(elementsInA1!=j && elementsInA2!=k) {		
			if(arr1[j][curraxis] < arr2[k][curraxis]) {			
				for(int l=0;l<columns; l++) matrixtosort[i][l] = arr1[j][l];
				i++;
				j++;
			}
			else {
				for(int l=0;l<columns; l++) matrixtosort[i][l] = arr2[k][l];
				i++;
				k++;
			}
		}
		while(elementsInA1 != j) {
			for(int l=0;l<columns; l++) matrixtosort[i][l] = arr1[j][l];
			i++;
			j++;
		}
		while(elementsInA2 != k) {
			for(int l=0;l<columns; l++) matrixtosort[i][l] = arr2[k][l];
			i++;
			k++;
		}
		
		for(int i=0; i<elementsInA1; i++) delete [] arr1[i];
		delete [] arr1;
		for(int i=0; i<elementsInA2; i++) delete [] arr2[i];
		delete [] arr2;
		
	}
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
