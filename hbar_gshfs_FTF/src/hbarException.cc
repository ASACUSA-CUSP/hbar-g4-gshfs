#include "hbarException.hh"

hbarException::hbarException(string m)
  :msg(m)
{

}

hbarException::~hbarException() throw()
{

}

const char* hbarException::what() const throw()
{
  return msg.c_str();
}
