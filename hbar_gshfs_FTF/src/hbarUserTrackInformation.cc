#include "hbarUserTrackInformation.hh"

hbarUserTrackInformation::hbarUserTrackInformation()
{

}

hbarUserTrackInformation::~hbarUserTrackInformation()
{

}

void hbarUserTrackInformation::DictionaryInsert(const G4Step * stepPointer, hbarTrackDictionaryEntry toInsert)
{
  dictionary[stepPointer] = toInsert;
}

hbarTrackDictionaryEntry hbarUserTrackInformation::DictionaryLookup(const G4Step * stepPointer) const
{
  return dictionary.at(stepPointer); ///WILL throw exception if it is not in the dictionary...
}

G4bool hbarUserTrackInformation::DictionaryHas(const G4Step * stepPointer) const
{
  return dictionary.find(stepPointer) != dictionary.end();
}
