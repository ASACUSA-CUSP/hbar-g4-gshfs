#include "hbarEventAction.hh"
#include "hbarEventActionMess.hh"
#include "hbarDetectorHit.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4TrajectoryPoint.hh"
#include "G4VVisManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"
#include "Randomize.hh"
#include "G4ThreeVector.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"
#include "G4Point3D.hh"
#include "G4SDManager.hh"

#include <typeinfo>

#include "hbarRunAction.hh"

hbarEventAction::hbarEventAction(hbarRunAction* aRunAct)
:drawFlag("all"),eventFlag("off"),posFlag("off"),
printModulo(50),runAction(aRunAct)
{
  scintiCollID = -1;
  eventMessenger = new hbarEventActionMess(this);
}

hbarEventAction::~hbarEventAction()
{
  delete eventMessenger;
}

void hbarEventAction::BeginOfEventAction(const G4Event* evt)
{
   G4SDManager *SDman = G4SDManager::GetSDMpointer();
   if (scintiCollID < 0) 
     {
		G4String colName;
	  // if(runAction->GetDetector()->GetCPTConst()->GetUseSensitiveBGO()) {
		//scintiCollID = SDman->GetCollectionID(colName="BGODet_SD");
		//}
      // else 
	scintiCollID = SDman->GetCollectionID(colName="scintiCollection");
     }
    

  // Reset variables
  runAction->SetExitAngle(0.0);
  runAction->SetEntranceAngle(0.0);
  runAction->SetVelocity(0.0);
  runAction->SetVelocityZ(0.0);
  runAction->SetVelocityDet(0.0); // Added by Clemens
  runAction->SetVelocityZDet(0.0); // Added by Clemens
  runAction->SetImpactAngleDet(0.0); // Added by Clemens
  runAction->SetPositionX_Cavity(0.0);
  runAction->SetPositionY_Cavity(0.0);
  runAction->SetEntranceTime_Cavity(0.0);
  runAction->SetExitTime_Cavity(0.0);
  runAction->SetQuantumNumberAtCavity(0);
  runAction->SetQuantumNumberAtDetector(0);

  G4int evtNb = evt->GetEventID() +1;
  if (eventFlag == "on") 
	{
	  G4cout << ">>> Event " << evt->GetEventID() << G4endl;
	}
  if (evtNb%printModulo == 0)
	{
	  G4cout << "---> Event: " << evtNb << G4endl;
	}
}

void hbarEventAction::EndOfEventAction(const G4Event* evt)
{
/*  if(runAction->GetDetector()->GetCPTConst()->GetUseSensitiveBGOPMTs()) {
  	double sum = 0.0, sumenergy = 0.0;
	std::vector<double> energy_pmt;
  
	for(int i=0;i<4;i++) {
   		for(int j=0; j<64;j++) { 
    	 energy_pmt = runAction->GetDetector()->GetCPTConst()->Get_BGOPMT_SD(i,j)->Get_PMT_energyData();
		 for(int k=0; k<energy_pmt.size(); k++) sumenergy += energy_pmt[k];
		 runAction->GetDetector()->GetCPTConst()->Get_BGOPMT_SD(i,j)->SetTotalEnergy(sumenergy);
		 sumenergy = 0.0;
	    }
  	}

}*/
	/*for(int i=0;i<4;i++) {
   		for(int j=0; j<64;j++) { 
    	 sum += runAction->GetDetector()->GetCPTConst()->Get_BGOPMT_SD(i,j)->GetNumberofPhotons();
		 sumenergy += *(runAction->GetDetector()->GetCPTConst()->Get_BGOPMT_SD(i,j)->GetTotalEnergy());
	    }
  	}
	//if(sum>0.0) runAction->AddBGOHitCounter();

	//G4cout << "******** Total Number of Photons in BGO: " << sum << "	Total Energy in BGO (eV):	" << sumenergy << " BGO Hit Counter: " << runAction->GetBGOHitCounter() << G4endl;
	
 	/* G4String fileName = "data_scintil/hits/tot_photons_energy.dat";
 	 std::ofstream fileOut(fileName);
 	 for(int i=0; i<energy_pmt.size(); i++) fileOut << energy_pmt[i] << std::endl;

  }
  //if(runAction->GetDetector()->GetCPTConst()->GetUseSensitiveBGO() && (runAction->GetDetector()->GetCPTConst()->Get_BGODet_SD()->Getbgohit())) runAction->AddBGOHitCounter();
*/


  if (eventFlag == "on") 
	{
	  G4cout << "~>>> Event " << evt->GetEventID() << G4endl;
	}
  G4TrajectoryContainer * trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if(trajectoryContainer)
     { 
	   n_trajectories = trajectoryContainer->entries(); 
	 }
  if (eventFlag == "on") 
	{
	  G4cout << "    " << n_trajectories 
			 << " trajectories stored in this event." << G4endl;
	}

  hbarDetectorHitsCollection* SHC = 0;



  //G4bool triggered;
  if (scintiCollID >= 0) 
	{
	  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
	  if (HCE) 
		{
		  SHC = (hbarDetectorHitsCollection*)(HCE->GetHC(scintiCollID));
		}
  }
  

  G4int hitnum = 0;
  G4VVisManager* hbarVVisManager = G4VVisManager::GetConcreteInstance();
  for (G4int i=0; i<n_trajectories; i++) 
	{

	  G4Trajectory* trj = (G4Trajectory *)((*(evt->GetTrajectoryContainer()))[i]);
	  int trjPointNum = trj->GetPointEntries();
	  if (trjPointNum > 0) 
		{
		  G4TrajectoryPoint* trjLastPoint = (G4TrajectoryPoint*)(trj->GetPoint(trjPointNum -1));
		  G4ThreeVector trjLastPointPos = trjLastPoint->GetPosition();
		  G4Point3D trjLastPointPos3D = (G4Point3D)(trjLastPointPos);
		  if (posFlag == "on") 
			{
			  G4cout << "Stopping position: (X,Y,Z) = " << trjLastPointPos3D << G4endl;
			}
			
		  if (trj->GetParticleName().contains("hydrogen") || trj->GetParticleName().contains("anti_proton")) 
			{
			  //if (trj->GetParticleName().contains("hydrogen")) // modified by Clemens
			  //  double pbarX = trjLastPointPos.x() - 500.*mm + 14.3*mm;

			  if(runAction->GetQuantumNumberAtCavity() > 0)
				{
				  runAction->GetCavityEntranceQuantumNumberHistogram()->Fill(runAction->GetQuantumNumberAtCavity());
				}

			  G4double posX = trjLastPointPos.x();
			  G4double posY = trjLastPointPos.y();
			  G4double posZ = trjLastPointPos.z();
			  G4double posR = sqrt(posX*posX + posY*posY);
			
			  if(runAction->GetDetector()->GetDDetConst() != NULL)
				{
				  std::cout << "************************** "<< runAction->GetDetector()->GetDDetConst()->GetDetectorStart()/m << "   " << posZ/m << std::endl;
				  
				  if ((posZ >= runAction->GetDetector()->GetDDetConst()->GetDetectorStart()-1.*mm)
					  && (posZ <= runAction->GetDetector()->GetDDetConst()->GetDetectorStart()+1.*mm)
					  && (posR <= runAction->GetDetector()->GetDDetConst()->GetDetectorDiameter()/2.0)) 
					{
					   if(trj->GetParticleName() == "anti_proton") {
					    runAction->AddHitCounter();
					   }
					   else {
					   
					          if(trj->GetParticleName() == "antihydrogen")
						        runAction->SetQuantumNumberAtDetector(hbarAntiHydrogen::Definition()->GetN());
					          else if(trj->GetParticleName() == "hydrogen")
						        runAction->SetQuantumNumberAtDetector(hbarHydrogen::Definition()->GetN());
					          else
						        throw hbarException("Something is wrong with the particle names.");
					          if(runAction->GetQuantumNumberAtDetector() > 0)
						        {
						          runAction->GetDetectorCavityQuantumNumberHistogram()->Fill(runAction->GetQuantumNumberAtDetector(), runAction->GetQuantumNumberAtCavity());
						        }

					          if(runAction->GetQuantumNumberAtCavity() == 0)
						        {
						          G4cout << "WARNING: Particle hit detector before passing the cavity!." << G4endl;
						        }
						      std::cout << "************************** Add!!!" << std::endl;
					          runAction->AddHitCounter();
					         
					          hitnum = 1;
					          if (drawFlag.contains("hit") && hbarVVisManager) 
						        {
						          trj->DrawTrajectory();
						        }
					          runAction->GetAngleDifferenceHistogram()
						        ->Fill(runAction->GetEntranceAngle() - runAction->GetExitAngle());
					          runAction->GetVelocityHistogram()->Fill(runAction->GetVelocity()/(m/s));
					          runAction->GetVelocityHistogramDet()
						        ->Fill(runAction->GetVelocityDet()/(m/s)); // Added by Clemens
					          runAction->GetAngularDistHistogramDet()
						        ->Fill(runAction->GetImpactAngleDet()); // Added by Clemens
					
						         // Filling the dummydet ntupel
		           			  // G4ThreeVector imvect = runAction->GetMomentumDirection();
		           			  // hbarHydrogenLike* part = reinterpret_cast<hbarHydrogenLike*>(trj->GetParticleDefinition());
		            			  //std::cout <<part->GetTwoMJ()*0.5*(part->GetP()) << " " << part->GetTwoMJ() << std::endl;
		            		  //runAction->GetDummyDetNtuple()->Fill(posX, posY, posZ, runAction->GetVelocityStep()/(m/s),imvect.theta(), imvect.phi(), part->GetN(), part->GetTwoj()/2., part->GetTwoMJ()*0.5*(part->GetP()), part->GetF(), part->GetM());
					      }
					}
				}
			  if (drawFlag.contains("inS2") && hbarVVisManager && (runAction->GetVelocity() > 0.0)) {
				trj->DrawTrajectory();
			  }
			  runAction->GetStopNtuple()->Fill(posX, posY, posZ);
			}

        if (hbarVVisManager) {
          if (drawFlag == "all") {
            trj->DrawTrajectory();
          } else if ((drawFlag == "charged")&&(trj->GetCharge() != 0.)) {
           trj->DrawTrajectory();
          } else if ((drawFlag == "pbar")&&(trj->GetParticleName() == "anti_proton")) {
            trj->DrawTrajectory();
          } else if ((drawFlag == "hbar")&&(trj->GetParticleName().contains("antihydrogen"))) {
            trj->DrawTrajectory();
          } else if ((drawFlag == "hbar")&&(trj->GetParticleName() == "hydrogen")) { // Added by Clemens
           trj->DrawTrajectory();
          } else if (SHC && (SHC->entries() > 0) && drawFlag.contains("realhit")) {
           trj->DrawTrajectory();
          }
        }
      }
    }


   //G4cout << "******** after tr!" << std::endl;


    if (SHC) 
	  {
		int n_hit = SHC->entries();
		G4int evn = evt->GetEventID();
		for (G4int i = 0; i < n_hit; i++) 
		  {
			(*SHC)[i]->SetEventNumber(evn+1);
			//         G4cout << "hitnum3: " << hitnum << std::endl;
			(*SHC)[i]->SetHitNumber(hitnum);

			//std::cout << "SHC: " << (*SHC)[i]->GetPDG_code() << " " << typeid((*SHC)[i]->GetPDG_code()).name() << std::endl;
			runAction->FillHit((*SHC)[i]);
			// Fill the hit data into the hit ntuple
			runAction->GetHitTree()->Fill();
		  }
		/*if (eventFlag == "on") 
		  {
			G4cout << "     " << n_hit
				   << " hits are stored in hbarDetectorHitsCollection." << G4endl;
			G4double totE = 0;
			for (G4int i = 0; i < n_hit; i++) {
			  G4cout << " Particle: " << (*SHC)[i]->GetParticleName()
						<< "  Detector: " << (*SHC)[i]->GetDetectorName()
						<< "  Time: " << std::setprecision(16) << (*SHC)[i]->GetTime()/ns
						<< "  Ekin: " << std::setprecision(6) << (*SHC)[i]->GetKineticEnergy()/MeV
						<< "  Edep: " << (*SHC)[i]->GetEnergyDeposit()/MeV
						<< "  PosZ: " << (*SHC)[i]->GetPosition().z()/m << G4endl;
			  totE += (*SHC)[i]->GetEnergyDeposit();
			}
			G4cout << "     Total energy deposition in scintillator : "
				   << totE / MeV << " (MeV)" << G4endl;
			G4cout << "     Total energy deposition in scintillator : "
					  << totE / MeV << " (MeV)" << G4endl;
		  }*/

        //G4cout << "******** end!" << std::endl;

	  }
///////////////////////////////////////////////////////////////////////////////////////////////////
	
	if(runAction->GetDetector()->GetCPTConst()->GetUseSensitiveSiPMs()) {
		runAction->GetHodorTree()->Fill();	
	}
///////////////////////////////////////////////////////////////////////////////////////////////////
	if(runAction->GetDetector()->GetCPTConst()->GetUseSensitiveBGOPMTs()) {
			//std::cout << "++++++++++++++++++++++++++++++++++ eventaction fill" << std::endl;
		runAction->GetBGOPMTTree()->Fill();	
	}
}

