#include "hbarRunMess.hh"
#include "hbarRunAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "Randomize.hh"

hbarRunMessenger::hbarRunMessenger(hbarRunAction* aRun)
:runAction(aRun)
{   
  fileDir = new G4UIdirectory("/file/");
  fileDir->SetGuidance("Results file control.");
        
  FileNameCmd = new G4UIcmdWithAString("/file/setFileName",this);
  FileNameCmd->SetGuidance("Choose a name for the ROOT file.");
  FileNameCmd->SetParameterName("fileName",true);
  FileNameCmd->SetDefaultValue ("rootfiles/g4.root");
  FileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  VerboseSteppingCmd = new G4UIcmdWithAString("/setup/printStepsVerbose",this);
  VerboseSteppingCmd->SetGuidance("Print every step verbose for the primary particle.");
  VerboseSteppingCmd->SetGuidance("  Choice : on, off(default)");
  VerboseSteppingCmd->SetParameterName("choice",true);
  VerboseSteppingCmd->SetDefaultValue("off");
  VerboseSteppingCmd->SetCandidates("on off");
  VerboseSteppingCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  VerboseDecayCmd = new G4UIcmdWithAString("/setup/printDecayVerbose",this);
  VerboseDecayCmd->SetGuidance("Print every decay verbose for the primary particle.");
  VerboseDecayCmd->SetGuidance("  Choice : on, off(default)");
  VerboseDecayCmd->SetParameterName("choice",true);
  VerboseDecayCmd->SetDefaultValue("off");
  VerboseDecayCmd->SetCandidates("on off");
  VerboseDecayCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  VerboseTypeTransitionsCmd = new G4UIcmdWithAString("/setup/printVerboseTypeTransitions",this);
  VerboseTypeTransitionsCmd->SetGuidance("Print type transitions for quantum number verbosely.");
  VerboseTypeTransitionsCmd->SetGuidance("  Choice : on, off(default)");
  VerboseTypeTransitionsCmd->SetParameterName("choice",true);
  VerboseTypeTransitionsCmd->SetDefaultValue("off");
  VerboseTypeTransitionsCmd->SetCandidates("on off");
  VerboseTypeTransitionsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  filenamedatBGOCmd = new G4UIcmdWithAString("/file/setfilenamedatBGO",this);
  filenamedatBGOCmd->SetGuidance("Choose a name to write BGO energies in dat file.");
  filenamedatBGOCmd->SetParameterName("filenamedatBGO",true);
  filenamedatBGOCmd->SetDefaultValue ("data_scintil/hits/BGOenergies.dat");
  filenamedatBGOCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  HodorWaveformFileNameCmd = new G4UIcmdWithAString("/file/SetHodorWaveformFileName",this);
  HodorWaveformFileNameCmd->SetGuidance("Choose a name for the Waveform ROOT file.");
  HodorWaveformFileNameCmd->SetParameterName("HodorWaveformFileName",true);
  HodorWaveformFileNameCmd->SetDefaultValue ("rootfiles/Hodor_File.root");
  HodorWaveformFileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  BGOPMTFileNameCmd = new G4UIcmdWithAString("/file/SetBGOPMTFileName",this);
  BGOPMTFileNameCmd->SetGuidance("Choose a name for the BGOPMT ROOT file.");
  BGOPMTFileNameCmd->SetParameterName("BGOPMTFileName",true);
  BGOPMTFileNameCmd->SetDefaultValue ("rootfiles/BGOPMT_File.root");
  BGOPMTFileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


}

hbarRunMessenger::~hbarRunMessenger()
{  
  delete FileNameCmd; delete fileDir; delete VerboseSteppingCmd; delete VerboseDecayCmd;
  delete VerboseTypeTransitionsCmd; delete HodorWaveformFileNameCmd; delete filenamedatBGOCmd;
  delete BGOPMTFileNameCmd;
}

void hbarRunMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if (command == FileNameCmd)
    { runAction->SetFileName(newValue); }
  if ( command == VerboseSteppingCmd)
    { runAction->SetVerboseStepping( newValue == "on" ); }

  if ( command == VerboseDecayCmd)
    { runAction->SetVerboseDecay( newValue == "on" ); }

  if ( command == VerboseTypeTransitionsCmd)
    { runAction->SetVerboseTypeTransitions( newValue == "on" ); }

  if (command == filenamedatBGOCmd)
    { runAction->SetfilenamedatBGO(newValue); }

if (command == HodorWaveformFileNameCmd)
    { runAction->SetHodorWaveformFileName(newValue); }

if (command == BGOPMTFileNameCmd)
    { runAction->SetBGOPMTFileName(newValue); }
}


