#include "hbarCPTMess.hh"

hbarCPTMess::hbarCPTMess(hbarCPTConst * _hbarCPT)
  :hbarCPT(_hbarCPT)
{


  hbardetCalDir = new G4UIdirectory("/setup/detector/");
  hbardetCalDir->SetGuidance("Detector properties control.");
  
  FoilMatCmd = new G4UIcmdWithAString("/setup/detector/setFoilMaterial",this);
  FoilMatCmd->SetGuidance("Set Material of Foil for Timepix3 simulation studies.");
  FoilMatCmd->SetParameterName("Count",true,false);
  FoilMatCmd->SetDefaultValue("Carboncoatmaterial");  
  FoilMatCmd->AvailableForStates(G4State_Idle);  


  ScUseCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseCalorimeter",this);
  ScUseCmd->SetGuidance("Switch to calorimeter number X");
  ScUseCmd->SetParameterName("Count",true,false);
  ScUseCmd->SetDefaultValue(0);
  ScUseCmd->SetRange("Count>=0");
  ScUseCmd->AvailableForStates(G4State_Idle);

  ScThickCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setCalorimeterThickness",this);
  ScThickCmd->SetGuidance("Set the total thickness of the current calorimeter (m)");
  ScThickCmd->SetParameterName("Size",false,false);
  ScThickCmd->SetDefaultUnit("m");
  ScThickCmd->SetRange("Size>0.0");
  ScThickCmd->AvailableForStates(G4State_Idle);

  ScLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setCalorimeterLength",this);
  ScLengthCmd->SetGuidance("Set length of the current calorimeter (m)");
  ScLengthCmd->SetParameterName("Size",false,false);
  ScLengthCmd->SetDefaultUnit("m");
  ScLengthCmd->SetRange("Size>0.0");
  ScLengthCmd->AvailableForStates(G4State_Idle);

  ScWidthCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setCalorimeterWidth",this);
  ScWidthCmd->SetGuidance("Set width of the current calorimeter (m)");
  ScWidthCmd->SetParameterName("Size",false,false);
  ScWidthCmd->SetDefaultUnit("m");
  ScWidthCmd->SetRange("Size>0.0");
  ScWidthCmd->AvailableForStates(G4State_Idle);

  ScWidthLCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setCalorimeterLargerWidth",this);
  ScWidthLCmd->SetGuidance("Set width of the longer side of the current calorimeter (m)");
  ScWidthLCmd->SetParameterName("Size",false,false);
  ScWidthLCmd->SetDefaultUnit("m");
  ScWidthLCmd->SetRange("Size>0.0");
  ScWidthLCmd->AvailableForStates(G4State_Idle);

  ScCenterCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setCalorimeterCenter",this);
  ScCenterCmd->SetGuidance("Set position of the center of the current calorimeter (m)");
  ScCenterCmd->SetParameterName("Px", "Py", "Pz",false,false);
  ScCenterCmd->SetDefaultUnit("m");
  ScCenterCmd->AvailableForStates(G4State_Idle);

  ScAngleCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setCalorimeterAngle",this);
  ScAngleCmd->SetGuidance("Set angle the current calorimeter (degree)");
  ScAngleCmd->SetParameterName("Ax", "Ay", "Az",false,false);
  ScAngleCmd->SetDefaultUnit("degree");
  ScAngleCmd->AvailableForStates(G4State_Idle);

  LeadThickCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setLeadLayerThickness",this);
  LeadThickCmd->SetGuidance("Set thickness of one lead layer in the current calorimeter (m)");
  LeadThickCmd->SetParameterName("Size",false,false);
  LeadThickCmd->SetDefaultUnit("m");
  LeadThickCmd->SetRange("Size>0.0");
  LeadThickCmd->AvailableForStates(G4State_Idle);

  LeadNumCmd = new G4UIcmdWithAnInteger("/setup/detector/setNumberOfLeadLayers",this);
  LeadNumCmd->SetGuidance("Set number of lead layers in the detector");
  LeadNumCmd->SetParameterName("Count",true,false);
  LeadNumCmd->SetDefaultValue(0);
  LeadNumCmd->SetRange("Count>=0");
  LeadNumCmd->AvailableForStates(G4State_Idle);

  ScSidesNumCmd = new G4UIcmdWithAnInteger("/setup/detector/setNumberOfCalorimeterSides",this);
  ScSidesNumCmd->SetGuidance("Set number of sides (rectangle, hexagon etc.) in the detector");
  ScSidesNumCmd->SetParameterName("Count",true,false);
  ScSidesNumCmd->SetDefaultValue(4);
  ScSidesNumCmd->SetRange("Count>3");
  ScSidesNumCmd->AvailableForStates(G4State_Idle);

  // Added by Clemens -- start
  // Veto Counter
  VetoUseCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseVeto",this);
  VetoUseCmd->SetGuidance("Switch to veto counter number X");
  VetoUseCmd->SetParameterName("Count",true,false);
  VetoUseCmd->SetDefaultValue(0);
  VetoUseCmd->SetRange("Count>=0");
  VetoUseCmd->AvailableForStates(G4State_Idle);

  VetoSetDivCmd = new G4UIcmdWithAnInteger("/setup/detector/setVetoSegmentation",this);
  VetoSetDivCmd->SetGuidance("Set number of sections X");
  VetoSetDivCmd->SetParameterName("Div",true,false);
  VetoSetDivCmd->SetDefaultValue(4);
  VetoSetDivCmd->SetRange("Div>=0");
  VetoSetDivCmd->AvailableForStates(G4State_Idle);

  VetoLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setVetoLength", this);
  VetoLengthCmd->SetGuidance("Set thickness of veto counter");
  VetoLengthCmd->SetParameterName("Length",false,false);
  VetoLengthCmd->SetDefaultUnit("m");
  VetoLengthCmd->SetRange("Length>0.0");
  VetoLengthCmd->AvailableForStates(G4State_Idle);

  VetoInRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setVetoInRad", this);
  VetoInRadCmd->SetGuidance("Set inner radius of veto counter");
  VetoInRadCmd->SetParameterName("InRad",false,false);
  VetoInRadCmd->SetDefaultUnit("m");
  VetoInRadCmd->SetRange("InRad>=0.0");
  VetoInRadCmd->AvailableForStates(G4State_Idle);

  VetoOutRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setVetoOutRad", this);
  VetoOutRadCmd->SetGuidance("Set outer radius of veto counter");
  VetoOutRadCmd->SetParameterName("OutRad",false,false);
  VetoOutRadCmd->SetDefaultUnit("m");
  VetoOutRadCmd->SetRange("OutRad>0.0");
  VetoOutRadCmd->AvailableForStates(G4State_Idle);

  VetoCenterCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setVetoCenter",this);
  VetoCenterCmd->SetGuidance("Set center of the current veto counter (m)");
  VetoCenterCmd->SetParameterName("x", "y", "z",false,false);
  VetoCenterCmd->SetDefaultUnit("m");
  VetoCenterCmd->AvailableForStates(G4State_Idle);

  VetoAngleCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setVetoAngle",this);
  VetoAngleCmd->SetGuidance("Set angle the current veto counter (degree)");
  VetoAngleCmd->SetParameterName("Ax", "Ay", "Az",false,false);
  VetoAngleCmd->SetDefaultUnit("degree");
  VetoAngleCmd->AvailableForStates(G4State_Idle);

  HodoscopeUseCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseHodoscope",this);
  HodoscopeUseCmd->SetGuidance("Switch to veto counter number X");
  HodoscopeUseCmd->SetParameterName("Count",true,false);
  HodoscopeUseCmd->SetDefaultValue(0);
  HodoscopeUseCmd->SetRange("Count>=0");
  HodoscopeUseCmd->AvailableForStates(G4State_Idle);

  HodoscopeSetDivCmd = new G4UIcmdWithAnInteger("/setup/detector/setHodoscopeSegmentation",this);
  HodoscopeSetDivCmd->SetGuidance("Set number of sections X");
  HodoscopeSetDivCmd->SetParameterName("Div",true,false);
  HodoscopeSetDivCmd->SetDefaultValue(4);
  HodoscopeSetDivCmd->SetRange("Div>=0");
  HodoscopeSetDivCmd->AvailableForStates(G4State_Idle);

  HodoscopeLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setHodoscopeLength", this);
  HodoscopeLengthCmd->SetGuidance("Set thickness of veto counter");
  HodoscopeLengthCmd->SetParameterName("Length",false,false);
  HodoscopeLengthCmd->SetDefaultUnit("m");
  HodoscopeLengthCmd->SetRange("Length>0.0");
  HodoscopeLengthCmd->AvailableForStates(G4State_Idle);

  HodoscopeInRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setHodoscopeInRad", this);
  HodoscopeInRadCmd->SetGuidance("Set inner radius of veto counter");
  HodoscopeInRadCmd->SetParameterName("InRad",false,false);
  HodoscopeInRadCmd->SetDefaultUnit("m");
  HodoscopeInRadCmd->SetRange("InRad>=0.0");
  HodoscopeInRadCmd->AvailableForStates(G4State_Idle);

  HodoscopeOutRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setHodoscopeOutRad", this);
  HodoscopeOutRadCmd->SetGuidance("Set outer radius of veto counter");
  HodoscopeOutRadCmd->SetParameterName("OutRad",false,false);
  HodoscopeOutRadCmd->SetDefaultUnit("m");
  HodoscopeOutRadCmd->SetRange("OutRad>0.0");
  HodoscopeOutRadCmd->AvailableForStates(G4State_Idle);

  HodoscopeCenterCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setHodoscopeCenter",this);
  HodoscopeCenterCmd->SetGuidance("Set center of the current veto counter (m)");
  HodoscopeCenterCmd->SetParameterName("x", "y", "z",false,false);
  HodoscopeCenterCmd->SetDefaultUnit("m");
  HodoscopeCenterCmd->AvailableForStates(G4State_Idle);

  HodoscopeAngleCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setHodoscopeAngle",this);
  HodoscopeAngleCmd->SetGuidance("Set angle the current veto counter (degree)");
  HodoscopeAngleCmd->SetParameterName("Ax", "Ay", "Az",false,false);
  HodoscopeAngleCmd->SetDefaultUnit("degree");
  HodoscopeAngleCmd->AvailableForStates(G4State_Idle);

  HbarSetDivCmd = new G4UIcmdWith3Vector("/setup/detector/setHbarDiv",this);
  HbarSetDivCmd->SetGuidance("Set Hbar segmentation ");
  HbarSetDivCmd->SetParameterName("xsegm", "ysegm", "0" ,false,false);
  HbarSetDivCmd->AvailableForStates(G4State_Idle);

  HbarSetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/detector/setHbarDimensions",this);
  HbarSetDimenCmd->SetGuidance("Set dimensions of the Hbar counter around the center (m)");
  HbarSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  HbarSetDimenCmd->SetDefaultUnit("m");
  HbarSetDimenCmd->AvailableForStates(G4State_Idle);;

  HbarCenterCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setHbarCenter",this);
  HbarCenterCmd->SetGuidance("Set center of the Hbar counter (m)");
  HbarCenterCmd->SetParameterName("x", "y", "z",false,false);
  HbarCenterCmd->SetDefaultUnit("m");
  HbarCenterCmd->AvailableForStates(G4State_Idle);

  HbarAngleCmd = new G4UIcmdWith3VectorAndUnit("/setup/detector/setHbarAngle",this);
  HbarAngleCmd->SetGuidance("Set angle of the Hbar counter (degree)");
  HbarAngleCmd->SetParameterName("Ax", "Ay", "Az",false,false);
  HbarAngleCmd->SetDefaultUnit("degree");
  HbarAngleCmd->AvailableForStates(G4State_Idle);

  HodoScintWidthCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setHodoscopeScintWidth", this);
  HodoScintWidthCmd->SetGuidance("Set the width of one Hodoscope Scintillator unit");
  HodoScintWidthCmd->SetParameterName("HodoScInnWidth",false,false);
  HodoScintWidthCmd->SetDefaultUnit("m");
  HodoScintWidthCmd->AvailableForStates(G4State_Idle);

  HodoScintHeightCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setHodoscopeScintHeight", this);
  HodoScintHeightCmd->SetGuidance("Set the height of one Hodoscope Scintillator unit");
  HodoScintHeightCmd->SetParameterName("HodoScInnWidth",false,false);
  HodoScintHeightCmd->SetDefaultUnit("m");
  HodoScintHeightCmd->AvailableForStates(G4State_Idle);

  BGOScint2014StartCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setBGOScint2014Start", this);
  BGOScint2014StartCmd->SetGuidance("Set the start point for the 2014 BGO Scintillator plate");
  BGOScint2014StartCmd->SetParameterName("BGOScint2014Start",false,false);
  BGOScint2014StartCmd->SetDefaultUnit("m");
  BGOScint2014StartCmd->AvailableForStates(G4State_Idle);

  BGOScint2014EndCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setBGOScint2014End", this);
  BGOScint2014EndCmd->SetGuidance("Set the start point for the 2014 BGO Scintillator plate");
  BGOScint2014EndCmd->SetParameterName("BGOScint2014End",false,false);
  BGOScint2014EndCmd->SetDefaultUnit("m");
  BGOScint2014EndCmd->AvailableForStates(G4State_Idle);

  BGOScint2014DiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/detector/setBGOScint2014Diam", this);
  BGOScint2014DiamCmd->SetGuidance("Set the diameter of the 2014 BGO Scintillator plate");
  BGOScint2014DiamCmd->SetParameterName("BGOScint2014Diam",false,false);
  BGOScint2014DiamCmd->SetDefaultUnit("m");
  BGOScint2014DiamCmd->AvailableForStates(G4State_Idle);

  BGO2014UseCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseBGO2014",this);
  BGO2014UseCmd->SetGuidance("Switch to BGO2014 Scintillator number X");
  BGO2014UseCmd->SetParameterName("Count",true,false);
  BGO2014UseCmd->SetDefaultValue(0);
  BGO2014UseCmd->SetRange("Count>=0");
  BGO2014UseCmd->AvailableForStates(G4State_Idle);

  // added by Berni 
  HbarUseGDMLHodorCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseGDMLHodor",this);
  HbarUseGDMLHodorCmd->SetGuidance("Use the GDML file to load Hodor Geometry");
  HbarUseGDMLHodorCmd->SetParameterName("useflag",true,false);
  HbarUseGDMLHodorCmd->SetDefaultValue(0);
  HbarUseGDMLHodorCmd->SetRange("useflag>=0");
  HbarUseGDMLHodorCmd->AvailableForStates(G4State_Idle);

  HbarUseGDMLBGOCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseGDMLBGO",this);
  HbarUseGDMLBGOCmd->SetGuidance("Use the GDML file to load BGO Geometry");
  HbarUseGDMLBGOCmd->SetParameterName("useflag",true,false);
  HbarUseGDMLBGOCmd->SetDefaultValue(0);
  HbarUseGDMLBGOCmd->SetRange("useflag>=0");
  HbarUseGDMLBGOCmd->AvailableForStates(G4State_Idle);

  SetUseSensitiveSiPMsCmd = new G4UIcmdWithAnInteger("/setup/detector/SetUseSensitiveSiPMs",this);
  SetUseSensitiveSiPMsCmd->SetGuidance("Use sensitive SiPMs.");
  SetUseSensitiveSiPMsCmd->SetParameterName("useflag",true,false);
  SetUseSensitiveSiPMsCmd->SetDefaultValue(0);
  SetUseSensitiveSiPMsCmd->SetRange("useflag>=0");
  SetUseSensitiveSiPMsCmd->AvailableForStates(G4State_Idle);
 
  SetUseSensitiveBGOPMTsCmd = new G4UIcmdWithAnInteger("/setup/detector/SetUseSensitiveBGOPMTs",this);
  SetUseSensitiveBGOPMTsCmd->SetGuidance("Use sensitive BGOPMTs.");
  SetUseSensitiveBGOPMTsCmd->SetParameterName("useflag",true,false);
  SetUseSensitiveBGOPMTsCmd->SetDefaultValue(0);
  SetUseSensitiveBGOPMTsCmd->SetRange("useflag>=0");
  SetUseSensitiveBGOPMTsCmd->AvailableForStates(G4State_Idle);

  SaveWaveformsToDatCmd = new G4UIcmdWithAnInteger("/setup/detector/SetSaveWaveformsToDat",this);
  SaveWaveformsToDatCmd->SetGuidance("Save simulated Waveforms to a .dat file.");
  SaveWaveformsToDatCmd->SetParameterName("useflag",true,false);
  SaveWaveformsToDatCmd->SetDefaultValue(0);
  SaveWaveformsToDatCmd->SetRange("useflag>=0");
  SaveWaveformsToDatCmd->AvailableForStates(G4State_Idle);

  WriteWaveformsToPDFCmd = new G4UIcmdWithAnInteger("/setup/detector/SetWriteWaveformsToPDF",this);
  WriteWaveformsToPDFCmd->SetGuidance("Print Waveforms to pdfs - will be saved in folder /data_scintil/hits.");
  WriteWaveformsToPDFCmd->SetParameterName("useflag",true,false);
  WriteWaveformsToPDFCmd->SetDefaultValue(0);
  WriteWaveformsToPDFCmd->SetRange("useflag>=0");
  WriteWaveformsToPDFCmd->AvailableForStates(G4State_Idle);

  SetMAPMT_SupplyVoltageCmd = new G4UIcmdWithADouble("/setup/detector/SetMAPMT_SupplyVoltage", this);
  SetMAPMT_SupplyVoltageCmd->SetGuidance("Set supply voltage of MAPMT.");
  SetMAPMT_SupplyVoltageCmd->SetParameterName("MAPMT_SupplyVoltage",false,false);
  SetMAPMT_SupplyVoltageCmd->AvailableForStates(G4State_Idle);

}


hbarCPTMess::~hbarCPTMess()
{
  delete ScUseCmd;
  delete ScThickCmd;
  delete ScLengthCmd;
  delete ScWidthCmd;
  delete ScWidthLCmd;
  delete ScCenterCmd;
  delete ScAngleCmd;
  delete VetoUseCmd;
  delete VetoInRadCmd;
  delete VetoOutRadCmd;
  delete VetoCenterCmd;
  delete VetoAngleCmd;
  delete VetoLengthCmd;
  delete VetoSetDivCmd;
  delete HodoscopeUseCmd;
  delete HodoscopeInRadCmd;
  delete HodoscopeOutRadCmd;
  delete HodoscopeCenterCmd;
  delete HodoscopeAngleCmd;
  delete HodoscopeLengthCmd;
  delete HodoscopeSetDivCmd;
  delete HbarSetDivCmd;
  delete HbarSetDimenCmd;
  delete HbarCenterCmd;
  delete HbarAngleCmd;
  delete LeadThickCmd;
  delete LeadNumCmd;
  delete ScSidesNumCmd;
  delete hbardetCalDir;
  delete HodoScintWidthCmd;
  delete HodoScintHeightCmd;
  delete BGOScint2014StartCmd;
  delete BGOScint2014EndCmd;
  delete BGOScint2014DiamCmd;
  delete BGO2014UseCmd;
  delete HbarUseGDMLHodorCmd;
  delete HbarUseGDMLBGOCmd;
  delete SetUseSensitiveSiPMsCmd;
  delete SetUseSensitiveBGOPMTsCmd;
  delete SaveWaveformsToDatCmd;
  delete WriteWaveformsToPDFCmd;
  delete SetMAPMT_SupplyVoltageCmd;
  delete FoilMatCmd;

}

void hbarCPTMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if (command == HbarSetDivCmd)
	{
	  hbarCPT->SetHbarDetSegment(HbarSetDivCmd->GetNew3VectorValue(newValue));
	}
	
  if (command == FoilMatCmd)
	{
	    //std::cout << "------------------------------------------------------------ " << newValue << std::endl;
	  hbarCPT->SetFoilMaterial(newValue);
	}	
	
  if (command == HbarSetDimenCmd)
	{
	  hbarCPT->SetHbarDetDimension(HbarSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == HbarCenterCmd)
	{
	  hbarCPT->SetHbarDetCenter(HbarCenterCmd->GetNew3VectorValue(newValue));
	}
  if (command == HbarAngleCmd)
	{
	  hbarCPT->SetHbarDetAngle(HbarAngleCmd->GetNew3VectorValue(newValue));
	}

  if( command == VetoUseCmd )
	{
	  hbarCPT->UseVeto(VetoUseCmd->GetNewIntValue(newValue));
	}

  if( command == VetoSetDivCmd )
    {
	  hbarCPT->SetVetoSegment(VetoSetDivCmd->GetNewIntValue(newValue));
	}

  if (command == VetoInRadCmd)
    {
	  hbarCPT->SetVetoInRad(VetoInRadCmd->GetNewDoubleValue(newValue));
	}

  if (command == VetoLengthCmd)
    {
	  hbarCPT->SetVetoLength(VetoLengthCmd->GetNewDoubleValue(newValue));
	}

  if (command == VetoOutRadCmd)
    {
	  hbarCPT->SetVetoOutRad(VetoOutRadCmd->GetNewDoubleValue(newValue));
	}

  if (command == VetoCenterCmd)
	{
	  hbarCPT->SetVetoCenter(VetoCenterCmd->GetNew3VectorValue(newValue));
	}

  if (command == VetoAngleCmd)
	{
	  hbarCPT->SetVetoAngle(VetoAngleCmd->GetNew3VectorValue(newValue));
	}

  if( command == HodoscopeUseCmd )
	{
	  hbarCPT->UseHodoscope(HodoscopeUseCmd->GetNewIntValue(newValue));
	}

  if( command == HodoscopeSetDivCmd )
    {
	  hbarCPT->SetHodoscopeSegment(HodoscopeUseCmd->GetNewIntValue(newValue));
	}

  if (command == HodoscopeInRadCmd)
    {
	  hbarCPT->SetHodoscopeInRad(HodoscopeInRadCmd->GetNewDoubleValue(newValue));
	}

  if (command == HodoscopeLengthCmd)
    {
	  hbarCPT->SetHodoscopeLength(HodoscopeLengthCmd->GetNewDoubleValue(newValue));
	}

  if (command == HodoscopeOutRadCmd)
    {
	  hbarCPT->SetHodoscopeOutRad(HodoscopeOutRadCmd->GetNewDoubleValue(newValue));
	}

  if (command == HodoscopeCenterCmd)
	{
	  hbarCPT->SetHodoscopeCenter(HodoscopeCenterCmd->GetNew3VectorValue(newValue));
	}

  if (command == HodoscopeAngleCmd)
	{
	  hbarCPT->SetHodoscopeAngle(HodoscopeAngleCmd->GetNew3VectorValue(newValue));
	}


  if( command == ScUseCmd )
	{
	  hbarCPT->UseCalorimeter(ScUseCmd->GetNewIntValue(newValue));
	}

  if( command == ScThickCmd )
	{
	  hbarCPT->SetCalorimeterThickness(ScThickCmd->GetNewDoubleValue(newValue));
	}

  if( command == ScLengthCmd )
	{
	  hbarCPT->SetCalorimeterLength(ScLengthCmd->GetNewDoubleValue(newValue));
	}

  if( command == ScWidthCmd )
	{
	  hbarCPT->SetCalorimeterWidth(ScWidthCmd->GetNewDoubleValue(newValue));
	}

  if( command == ScWidthLCmd )
	{
	  hbarCPT->SetCalorimeterLargerWidth(ScWidthLCmd->GetNewDoubleValue(newValue));
	}

  if( command == ScCenterCmd )
	{
	  hbarCPT->SetCalorimeterCenter(ScCenterCmd->GetNew3VectorValue(newValue));
	}

  if( command == ScAngleCmd )
	{
	  hbarCPT->SetCalorimeterAngle(ScAngleCmd->GetNew3VectorValue(newValue));
	}

  if( command == LeadThickCmd )
	{
	  hbarCPT->SetLeadThickness(LeadThickCmd->GetNewDoubleValue(newValue));
	}

  if( command == LeadNumCmd )
	{
	  hbarCPT->SetNumberOfLeadLayers(LeadNumCmd->GetNewIntValue(newValue));
	}

  if( command == ScSidesNumCmd )
	{
	  hbarCPT->SetNumberOfCalorimeterSides(ScSidesNumCmd->GetNewIntValue(newValue));
	}

  if( command == HodoScintWidthCmd )
	{
	  hbarCPT->SetHodoScintWidth(HodoScintWidthCmd->GetNewDoubleValue(newValue));
	}

  if( command == HodoScintHeightCmd )
	{
	  hbarCPT->SetHodoScintHeight(HodoScintHeightCmd->GetNewDoubleValue(newValue));
	}

  if( command == BGOScint2014StartCmd )
	{
	  hbarCPT->SetBGOScint2014Start(BGOScint2014StartCmd->GetNewDoubleValue(newValue));
	}

  if( command == BGOScint2014EndCmd )
	{
	  hbarCPT->SetBGOScint2014End(BGOScint2014EndCmd->GetNewDoubleValue(newValue));
	}

  if( command == BGOScint2014DiamCmd )
	{
	  hbarCPT->SetBGOScint2014Diam(BGOScint2014DiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BGO2014UseCmd )
	{
	  hbarCPT->UseBGO2014(BGO2014UseCmd->GetNewIntValue(newValue));
	}

if (command == HbarUseGDMLHodorCmd)
	{
	  hbarCPT->SetUseGDMLHodor(HbarUseGDMLHodorCmd->GetNewIntValue(newValue));
	}

if (command == HbarUseGDMLBGOCmd)
	{
	  hbarCPT->SetUseGDMLBGO(HbarUseGDMLBGOCmd->GetNewIntValue(newValue));
	}

if (command == SetUseSensitiveSiPMsCmd)
	{
	  hbarCPT->SetUseSensitiveSiPMs(SetUseSensitiveSiPMsCmd->GetNewIntValue(newValue));
	}

if (command == SetUseSensitiveBGOPMTsCmd)
	{
	  hbarCPT->SetUseSensitiveBGOPMTs(SetUseSensitiveBGOPMTsCmd->GetNewIntValue(newValue));
	}

if (command == SaveWaveformsToDatCmd)
	{
	  hbarCPT->SetSaveWaveformsToDat(SaveWaveformsToDatCmd->GetNewIntValue(newValue));
	}

if (command == WriteWaveformsToPDFCmd)
	{
	  hbarCPT->SetWriteWaveformsToPDF(WriteWaveformsToPDFCmd->GetNewIntValue(newValue));
	}
if( command == SetMAPMT_SupplyVoltageCmd )
	{
	  hbarCPT->Set_MAPMT_SupplyVoltage(SetMAPMT_SupplyVoltageCmd->GetNewDoubleValue(newValue));
	}

}


