#include "hbarDetectorTracker.hh"
#include "hbarDetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"

hbarDetectorTracker::hbarDetectorTracker(G4String name)
:G4VSensitiveDetector(name)
{
  G4String HCname;
  collectionName.insert(HCname="trackerCollection");
  trackerCollection = NULL;
}

hbarDetectorTracker::~hbarDetectorTracker(){;}

void hbarDetectorTracker::Initialize(G4HCofThisEvent *HCE)
{
  static int HCID = -1;
  trackerCollection = new hbarDetectorHitsCollection
                      (SensitiveDetectorName,collectionName[0]);

  if (HCID < 0) {
     HCID = GetCollectionID(0);
  }
  HCE->AddHitsCollection(HCID, trackerCollection);
}

G4bool hbarDetectorTracker::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.) return false;

  hbarDetectorHit* newHit = new hbarDetectorHit();
  newHit->SetEnergyDeposit( edep );
  newHit->SetTime((aStep->GetPreStepPoint()->GetGlobalTime() + aStep->GetPostStepPoint()->GetGlobalTime())/2.0);
  newHit->SetKineticEnergy(aStep->GetPreStepPoint()->GetKineticEnergy() );
  newHit->SetPosition( aStep->GetPreStepPoint()->GetPosition() );
  newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() );
  newHit->SetParticleName( aStep->GetTrack()->GetDefinition()->GetParticleName() );
  newHit->SetPDGcode(aStep->GetTrack()->GetDefinition()->GetPDGEncoding() );
  //std::cout << aStep->GetTrack()->GetDefinition()->GetParticleName() <<  "detector tracker " << aStep->GetTrack()->GetDefinition()->GetPDGEncoding()  << std::endl;
  
  // tajima
  newHit->Settrack_id(aStep->GetTrack()->GetTrackID() ); 
  newHit->Setparent_id( aStep->GetTrack()->GetParentID() );



  trackerCollection->insert( newHit );

  return true;
}

void hbarDetectorTracker::EndOfEvent(G4HCofThisEvent*) {
}

void hbarDetectorTracker::clear()
{
}

void hbarDetectorTracker::DrawAll()
{
}

void hbarDetectorTracker::PrintAll()
{
}
