#include "hbarPrimGenAction.hh"

#include "hbarDetConst.hh"
#include "hbarPrimGenMess.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" // Added by Clemens

#include "G4Event.hh"
#include "G4Proton.hh"
#include "G4OpticalPhoton.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include "G4Box.hh"

#include "TMath.h"
#include "TF1.h"
#include "TFile.h" // Added by Clemens
#include "TROOT.h" // Added by Clemens
#include "TRint.h" // Added by Clemens

// Cosmic moun momentum spectrum dN/dp
Double_t bogdanova_spec(Double_t* x, Double_t *par) 
{
  if (x[0] > 1.0) 
	{
      return 18.0
		/(x[0]*TMath::Cos(par[0]) + 145.0)
		*TMath::Power((x[0] + 2.7/TMath::Cos(par[0])), -2.7)
		*(x[0] + 5.0)
		/(x[0] + 5.0/TMath::Cos(par[0]))
		;
	} 
  else 
	{
	  return 18.0
		/(1.0*TMath::Cos(par[0]*TMath::DegToRad()) + 145.0)
		*TMath::Power((1.0 + 2.7/TMath::Cos(par[0])), -2.7)
		*(1.0 + 5.0)
		/(1.0 + 5.0/TMath::Cos(par[0]))
		;
	}
}

// Antihydrogen to Hydrogen ?????????? Clemens
hbarPrimGenAction::hbarPrimGenAction(hbarDetConst* hbarDC)
 :hbarDetector(hbarDC),particleName("hydrogen"),
  rndmFlagEnergy("on"),rndmFlagState("follow"),rndmRydbergState(false),rndmFlagDir("on"),
  quantumStateFile(""), quantumEnergyForceFlag("off"), quantumTransitionFlag("off"), preloadQuantumStates(true), enableFieldRegimeTranslations(true), useStrongInitialState(false),
  SourceCenter(-1.0*m),BeamKinE(10.0*keV),SourceTemp(15.0*kelvin),
  rndmSourceVel(0.0*m/s),
  sliceFlag("off"),
  SourceFWHM_X(0.0), SourceFWHM_Y(0.0), SourceFWHM_Z(0.0),
  SourceOffsetX(0.0*m), SourceOffsetY(0.0*m), SourceMaxR(1.0*m),
  openAngle(16.5), prodRate(0.0*hertz), prodTime(0.0*ns),
  StateARatio(0.25), StateBRatio(0.25), StateCRatio(0.25), StateDRatio(0.25),
  StateF(1), StateM(1),
  StateN(1), StateL(0), MaxStateN(3), MaxStateL(2), StateTwoMJ(5), StateTwoJ(5), StateML(0), StateTwoMS(1),
  lowerHysteresis(0.5), upperHysteresis(5),
  nPart(0), BeamDir(0.0, 0.0, 1.0),
  timeToReset(true), useCRY(true),cryfilename(""),  useAEgISData(false), fileisopen(false),
  useParticleMixture(false),         
  k_B(1.38e-23), eVJ(1.6022e-19), clight(2.998e8)

//k_B(8.6174e-5*eV/kelvin)
{
  G4int n_particle = 1;
  particleGun  = new G4ParticleGun(n_particle);

  // Added by Clemens -- start
  // CRY Support
  if(useCRY){
    std::string setupString("");
    std::ifstream inputFile;
    inputFile.open(cryfilename.c_str(),std::ios::in);
    char buffer[1000];
    
    
    if (inputFile.fail()) {
      for(int i = 0; i<20; i++)
      if( cryfilename != "")  //....only complain if a filename was given //Corr by Rikard
      G4cout << "PrimaryGeneratorAction: Failed to open CRY input file= " 
	     << cryfilename << G4endl;
    } else {
    
      while ( !inputFile.getline(buffer,1000).eof()) {
	setupString.append(buffer);
	setupString.append(" "); }
    
      CRYSetup *setup=new CRYSetup(setupString,"./cry_v1.7/data");
      gen = new CRYGenerator(setup);
      RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),
					     &CLHEP::HepRandomEngine::flat);
      setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
    }
    //vect=new std::vector<CRYParticle*>;
  }
  vect = new std::vector<CRYParticle*>;
  // Added by Clemens -- stop

  //create a messenger for this class
  gunMessenger = new hbarPrimGenMess(this);

}

hbarPrimGenAction::~hbarPrimGenAction()
{
  delete particleGun;
  delete gunMessenger;
  if(vect) delete vect;
}

G4bool hbarPrimGenAction::open_root_spectrum(G4String filename){
  if(fileisopen) return true;
  TFile *f = new TFile(filename.c_str(), "READ");
  gROOT->cd();
  if(!f->IsOpen()) {delete f; return false;}
  TH1D *velspectr2;
  f->GetObject("velocity-z", velspectr2);
  // TH1D *velspectr;
  velspectr = TH1D(*velspectr2);
  delete f;
  //if(!velspectr) {return false;}
  //std::cout << "FILE CLOSED" << std::endl;
  fileisopen = true;
  return true;
}

void hbarPrimGenAction::GeneratePrimaries(G4Event* anEvent)
{
  if(!useParticleMixture) 
    GenerateSinglePrimaries(anEvent);
  else {
  std::vector<G4int> shotp;
    // The number of particles from BeamOn correspondes to Hbars!
    G4int partnum=0, shotpart=0;
    G4double oldratio=0, newratio=0, rand=0;
    for(size_t i=0; i<particleRatio.size(); i++) {
      partnum += particleRatio[i]; 
      shotp.push_back(0); }

    for(int i=0; i<partnum; i++){
      oldratio = 0;
      newratio = 0;
      rand = G4UniformRand();
      for(size_t j=0; j<particleRatio.size(); j++) {
	newratio = (particleRatio[j]-shotp[j])/(partnum-shotpart);
	/*std::cout << shotpart << " rand=" << rand << " old= " << oldratio
		  << " new= " << newratio << " pname= " 
		  << particleList[j] << std::endl;*/
	if(rand < oldratio+newratio && rand > oldratio){
	  particleName = particleList[j];
	  GenerateSinglePrimaries(anEvent);
	  shotp[j] += 1;
	  shotpart++; }
	oldratio += newratio; }
    }
    //std::cout << shotp[0] << " " << shotp[1] << " " << partnum <<std::endl;

  }
}

void hbarPrimGenAction::GenerateSinglePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of event
  //
  if(useAEgISData)
    {
      if (!open_root_spectrum(spectrumfilename))
	G4cerr << "Couldn't open Spectrum "<< spectrumfilename << G4endl;
    }
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  if (particleName != "cosmic") 
    {
      
      // primaries are generates the usual way
      G4ParticleDefinition* particle
	= particleTable->FindParticle(particleName);
      particleGun->SetParticleDefinition(particle);

	  if(hbarHydrogenLike * toUse = dynamic_cast<hbarHydrogenLike *>(particle))
		{
		  set_quantum_state(toUse);
		}

      
      G4double vx = 0.0;    // velocity X component
      G4double vy = 0.0;    // velocity Y component
      G4double vz = 0.0;    // velocity Z component
      G4double vr = 0.0;    // velocity transversal component
      G4double v = 0.0;     // velocity
      G4double vold = 0.0;
      G4double kinE = BeamKinE;
      G4double mass = particle->GetPDGMass()/eV*eVJ/sqr(clight);   // in kg
      G4double sigmav = sqrt((k_B*SourceTemp/kelvin)/mass);
      
      // generate velocity vector from the Maxwell-Botzmann velocity distribution
      do 
	{
	  vx = CLHEP::RandGauss::shoot(0.0, sigmav);   // in m/s
	  vy = CLHEP::RandGauss::shoot(0.0, sigmav);   // in m/s
	  vz = CLHEP::RandGauss::shoot(0.0, sigmav);   // in m/s
	  vr = sqrt(vx*vx + vy*vy);
	  v = sqrt(vx*vx + vy*vy + vz*vz);
	  if (rndmSourceVel > 0.0) {
	    // the velocity distribution should be flat
	    // so we generate a new velocity
	    // but keep the old velocity's direction
	    vold = v;
	    v = CLHEP::RandFlat::shoot(0.0, rndmSourceVel);   // in m/s
	    vx *= v/vold;
	    vy *= v/vold;
	    vz *= v/vold;
	    vr = sqrt(vx*vx + vy*vy);
	  }
	  
	  // If AEgIS data is present, use it for z velocity - added by Clemens
	  if(useAEgISData)
	    {
	      vz = velspectr.GetRandom();
	      vr = sqrt(vx*vx + vy*vy);
	      v  = sqrt(vx*vx + vy*vy + vz*vz); 
	    }
	  
	  // only accept the generated value if it is within the source cone (if used)
	  // and within the beam slice (if used)
	} while ( ((rndmFlagDir == "aim") && ((vr/v > std::sin(openAngle*M_PI/360.0)) || (vz < 0.0) ))
              || ((sliceFlag == "on")  && (TMath::Abs(vx) > 1.0)) );
       //while (((rndmFlagDir == "aim") && (vx/vr < SourceSep/(hbarDetector->GetBeamPipe1InnerDiameter()/2.0)))
       //        || ((sliceFlag == "on") && (TMath::Abs(vz) > 1.0)));
      //std::cout << vx << " " << vy << " " << vz << " " << v  << std::endl;
      //std::cout << std::sin(openAngle*M_PI/360.0) << " < " << vr/v << std::endl;
      if (rndmFlagDir == "off") 
	{
	  // The beam direction should _not_ be random
	  // so shoot the particle in the Z direction
	  // but keep its random velocity
	  vx = v*BeamDir.x()/BeamDir.mag();
	  vy = v*BeamDir.y()/BeamDir.mag();
	  vz = v*BeamDir.z()/BeamDir.mag();

	}
      
      if (rndmFlagEnergy == "on") 
	kinE = 0.5*mass*v*v*joule;
      // the beam energy should be random
      // so calculate the energy from the random velocity
      //
      // if it should _not_ be random,
      // then the user-assigned energy is kept
      
      // now the particle velocity direction and energy can be set
      
      //particleGun->SetParticleMomentumDirection(G4ThreeVector(vx, vy, vz));
      
      particleGun->SetParticleMomentumDirection(G4ThreeVector(vx, vy, vz)); // FIXME
      
      
      particleGun->SetParticleEnergy(kinE);
      //G4cout << "Particle momentum direction: " << vx << " " << vy << " " << vz << " Kinetic energy: " << kinE/(eV) << G4endl;
      


      
      // set the particle inital position
      G4double sourcePosX = 0.0;
      G4double sourcePosY = 0.0;
      G4double sourcePosZ = SourceCenter;
      do 
	{
	  if (SourceFWHM_X > 0.0) 
	    {
	      sourcePosX = CLHEP::RandGauss::shoot(0.0, SourceFWHM_X/2.354);
	    }
	  if (SourceFWHM_Y > 0.0) 
	    {
	      // the particle source is not point-like in the Y direction
	      // so generate a random Y position
	      sourcePosY = CLHEP::RandGauss::shoot(0.0, SourceFWHM_Y/2.354);
	    }
	  // the particle source radius should be smaller a user-defined value
	} while ((sqrt(sourcePosX*sourcePosX + sourcePosY*sourcePosY) > SourceMaxR)
		 || ((sliceFlag == "on") && (abs(sourcePosX) > 2.0*mm)));
      if (SourceFWHM_Z > 0.0) 
	{
	  // the particle source is not point-like in the Z direction
	  // so generate a random Z position
	  sourcePosZ = SourceCenter + CLHEP::RandGauss::shoot(0.0, SourceFWHM_Z/2.354);
	}
      
      // offset the particle from the beam axis, if necessary
      sourcePosX += SourceOffsetX;
      sourcePosY += SourceOffsetY;
      
      // finally set the particle's inital energy and position
      particleGun->SetParticlePosition(G4ThreeVector(sourcePosX, sourcePosY, sourcePosZ));
      //G4cout << "Initial position: " << sourcePosX/m << " " << sourcePosY/m << " " << sourcePosZ/m << G4endl;
      
      // Set inital time of the particle
      if (prodRate > 0.0) 
	{
	  // exponential distribution
	  prodTime += - TMath::Log(1 - G4UniformRand())/prodRate;
	}
      particleGun->SetParticleTime(prodTime);
      
      nPart++;
      particleGun->GeneratePrimaryVertex(anEvent);
      
    } 
  else 
    {
      
      // Added by Clemens -- start
      if(useCRY)
	{
	  //std::cout << "TESTTEST" << std::endl;
	  vect->clear();
	  //std::cout << "TESTTEST2" << std::endl;
	  
	  gen->genEvent(vect);
	  G4String cryparticleName;
	  for ( unsigned j=0; j<vect->size(); j++) 
	    {
	      cryparticleName=CRYUtils::partName((*vect)[j]->id());
	      
	      /*std::cout << "  "          << particleName << " "
		<< "charge="      << (*vect)[j]->charge() << " "
		<< std::setprecision(4)
		<< "energy (MeV)=" << (*vect)[j]->ke()*MeV << " "
		<< "pos (m)"
		<< G4ThreeVector((*vect)[j]->x(), 
		(*vect)[j]->y(), (*vect)[j]->z()) << " "
		<< "direction cosines " 
		<< G4ThreeVector((*vect)[j]->u(), 
		(*vect)[j]->v(), (*vect)[j]->w())<< " "
		<< std::endl;
		std::cout << CRYDisplacement.x() << " " << CRYDisplacement.y() << " "
		<< CRYDisplacement.z() << std::endl;*/
	      G4ThreeVector pos = 
		G4ThreeVector((*vect)[j]->y()*m + CRYDisplacement.x()*m, 
			      (*vect)[j]->z()*m + CRYDisplacement.y()*m, 
			      (*vect)[j]->x()*m + CRYDisplacement.z()*m);
	      G4ThreeVector imp = G4ThreeVector((*vect)[j]->v(), (*vect)[j]->w(), (*vect)[j]->u());
	      particleGun->SetParticleDefinition(particleTable->FindParticle((*vect)[j]->PDGid()));
	      particleGun->SetParticleEnergy((*vect)[j]->ke()*MeV);
	      particleGun->SetParticlePosition(pos);
	      particleGun->SetParticleMomentumDirection(imp);
	      particleGun->SetParticleTime((*vect)[j]->t());
	      particleGun->GeneratePrimaryVertex(anEvent);
	      delete (*vect)[j];
	    }
	  
	  // Added by Clemens -- stop
	} 
      else 
	{
	  
	  // The primaries are cosmic muons
	  // Their momentum spectrum is approximated using Eq. (1) of the paper:
	  // Cosmic muon flux at shallow depths underground,
	  // L. N. Bogdanova, M. G. Gavrilov, V. N. Kornoukhov, A. S. Starostin,
	  // arXiv:nucl-ex/0601019
	  
	  G4double totalcmflux = 234./(m*m*s);
	  G4String pname = "mu+";
	  if (G4UniformRand() < 0.5) 
	    {
	      pname = "mu-";
	    }
	  G4ParticleDefinition* particle
	    = particleTable->FindParticle(pname);
	  particleGun->SetParticleDefinition(particle);
	  
	  G4double gunsize = 1.5*m;
	  G4double gunarea = 5.0*gunsize*gunsize;
	  G4double cmrate  = totalcmflux*gunarea;
	  
	  G4double cmphi  = 2.0*TMath::Pi()*G4UniformRand();  // azimuth angle ("compass angle")
	  G4double cmtheta = 0.0;                             // zenith angle (angle above the horizon)
	  G4double yval = 0.0;
	  
	  // The muon intensity angular dependence is ~cos^2(theta)
	  // so randomly generate theta following this distribution
	  // using the acceptance-rejection method
	  do 
	    {
	      cmtheta = G4UniformRand()*TMath::Pi()/2.0;  // zenith angle
	      yval = G4UniformRand();
	    } while (yval > TMath::Power(TMath::Cos(cmtheta), 2.0));
	  
	  if (rndmFlagEnergy == "on") 
	    {
	      // The random zenith angle is now generated,
	      // so now the random muon momentum should be generated too.
	      // Instead of plugging the azimuth angle into the bognadova_spec function, however,
	      // 10 bogdanova_spec functions are defined, with azimuth angles from
	      // 0 to 0.9*Pi in steps of 0.1*Pi.
	      // This is because every time ROOT gets a random number from a function,
	      // it integrates the function over the whole range, if the function parameters
	      // were changed since the last random number was gotten from the function.
	      // Doing this would be very time-consuming, therefore it's better to define
	      // separate functions for a few (10) separate angle ranges and not to touch them afterwards.
	      //
	      // All the following gymnastics with 10 functions
	      // (instead of an array) is necessary because
	      // there seems to be a problem in ROOT or g++.
	      // The compiled code (with an array) crashed for no apparent reason
	      
	      static TF1 *cmspec_func1 = NULL;
	      G4bool funcexists1 = (cmspec_func1 != NULL);
	      cmspec_func1 = new TF1("specfunc1",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists1) 
		{
		  cmspec_func1->SetParameter(0, 0.0*TMath::Pi()/20.0);
		  cmspec_func1->SetNpx(1000);
		}
	      
	      static TF1 *cmspec_func2 = NULL;
	      G4bool funcexists2 = (cmspec_func2 != NULL);
	      cmspec_func2 = new TF1("specfunc2",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists2) 
		{
		  cmspec_func2->SetParameter(0, 1.0*TMath::Pi()/20.0);
		  cmspec_func2->SetNpx(1000);
		}

	      static TF1 *cmspec_func3 = NULL;
	      G4bool funcexists3 = (cmspec_func3 != NULL);
	      cmspec_func3 = new TF1("specfunc3",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists3) 
		{
		  cmspec_func3->SetParameter(0, 2.0*TMath::Pi()/20.0);
		  cmspec_func3->SetNpx(1000);
		}

	      static TF1 *cmspec_func4 = NULL;
	      G4bool funcexists4 = (cmspec_func4 != NULL);
	      cmspec_func4 = new TF1("specfunc4",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists4) 
		{
		  cmspec_func4->SetParameter(0, 3.0*TMath::Pi()/20.0);
		  cmspec_func4->SetNpx(1000);
		}

	      static TF1 *cmspec_func5 = NULL;
	      G4bool funcexists5 = (cmspec_func5 != NULL);
	      cmspec_func5 = new TF1("specfunc5",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists5) 
		{
		  cmspec_func5->SetParameter(0, 4.0*TMath::Pi()/20.0);
		  cmspec_func5->SetNpx(1000);
		}
	      
	      static TF1 *cmspec_func6 = NULL;
	      G4bool funcexists6 = (cmspec_func6 != NULL);
	      cmspec_func6 = new TF1("specfunc6",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists6) 
		{
		  cmspec_func6->SetParameter(0, 5.0*TMath::Pi()/20.0);
		  cmspec_func6->SetNpx(1000);
		}
	      
	      static TF1 *cmspec_func7 = NULL;
	      G4bool funcexists7 = (cmspec_func7 != NULL);
	      cmspec_func7 = new TF1("specfunc7",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists7) 
		{
		  cmspec_func7->SetParameter(0, 6.0*TMath::Pi()/20.0);
		  cmspec_func7->SetNpx(1000);
		}

	      static TF1 *cmspec_func8 = NULL;
	      G4bool funcexists8 = (cmspec_func8 != NULL);
	      cmspec_func8 = new TF1("specfunc8",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists8) 
		{
		  cmspec_func8->SetParameter(0, 7.0*TMath::Pi()/20.0);
		  cmspec_func8->SetNpx(1000);
		}
	      
	      static TF1 *cmspec_func9 = NULL;
	      G4bool funcexists9 = (cmspec_func9 != NULL);
	      cmspec_func9 = new TF1("specfunc9",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists9) 
		{
		  cmspec_func9->SetParameter(0, 8.0*TMath::Pi()/20.0);
		  cmspec_func9->SetNpx(1000);
		}
	      
	      static TF1 *cmspec_func10 = NULL;
	      G4bool funcexists10 = (cmspec_func10 != NULL);
	      cmspec_func10 = new TF1("specfunc10",bogdanova_spec, 0.0, 1000.0, 1); //NULL;
	      if (!funcexists10) 
		{
		  cmspec_func10->SetParameter(0, 9.0*TMath::Pi()/20.0);
		  cmspec_func10->SetNpx(1000);
		}
	      
	      
	      G4int funcnum = TMath::FloorNint(cmtheta*20.0/TMath::Pi());
	      G4double cm_p = 0.0; // cmspec_func[funcnum]->GetRandom()*GeV;
	      
	      switch (funcnum) 
		{
		case 0:
		  cm_p = cmspec_func1->GetRandom()*GeV;
		  break;
		case 1:
		  cm_p = cmspec_func2->GetRandom()*GeV;
		  break;
		case 2:
		  cm_p = cmspec_func3->GetRandom()*GeV;
		  break;
		case 3:
		  cm_p = cmspec_func4->GetRandom()*GeV;
		  break;
		case 4:
		  cm_p = cmspec_func5->GetRandom()*GeV;
		  break;
		case 5:
		  cm_p = cmspec_func6->GetRandom()*GeV;
		  break;
		case 6:
		  cm_p = cmspec_func7->GetRandom()*GeV;
		  break;
		case 7:
		  cm_p = cmspec_func8->GetRandom()*GeV;
		  break;
		case 8:
		  cm_p = cmspec_func9->GetRandom()*GeV;
		  break;
		case 9:
		  cm_p = cmspec_func10->GetRandom()*GeV;
		  break;
		default:
		  std::cout << "Wrong function number in PrimGenAction() !!!" << std::endl;
		}
	      //std::cout << "break 2 " << std::endl;
	      particleGun->SetParticleEnergy(0.0);     // to avoid the stupid warnings
	      //std::cout << "break 3 " << std::endl;
	      particleGun->SetParticleMomentum(cm_p);
	      //std::cout << "break 4 " << std::endl;
	      //std::cout << "muon p: " << cm_p/(GeV) << std::endl;
	      //std::cout << "muon E: " << particleGun->GetParticleEnergy()/GeV << std::endl;
	    } 
	  else 
	    {
	      particleGun->SetParticleEnergy(BeamKinE);
	    }
	  
	  if (rndmFlagDir != "off") 
	    {
	      
	      G4ThreeVector gundirection(TMath::Sin(cmtheta)*TMath::Sin(cmphi), -TMath::Cos(cmtheta), TMath::Sin(cmtheta)*TMath::Cos(cmphi));
	      particleGun->SetParticleMomentumDirection(gundirection);
	      
	      G4ThreeVector guncenter(gunsize*(0.5 - G4UniformRand()),
				      gunsize*(0.5 - G4UniformRand()),
								  gunsize*(0.5 - G4UniformRand()));
	      //G4double sourcePosX = 0.0;
	      //G4double sourcePosY = 0.0;
	      //G4double sourcePosZ = 0.0;
	      G4ThreeVector gunoppdir = -gundirection;
	      G4double gundistance = hbarDetector->GetWorldBox()->DistanceToOut(guncenter, gunoppdir);
	      G4ThreeVector gunstart = guncenter + gundistance*gunoppdir;
	      
	      particleGun->SetParticlePosition(gunstart);
	    } 
	  else 
	    {
	      G4ThreeVector gundirection(0.0, 0.0, 1.0);
	      particleGun->SetParticleMomentumDirection(gundirection);
	      G4ThreeVector gunstart(0.0, 0.0, SourceCenter);
	      particleGun->SetParticlePosition(gunstart);
	    }
	  
	  // Set inital time of the particle
	  //G4double nexttime = 0.0*ns;
	  // exponential distribution
	  prodTime += - TMath::Log(1 - G4UniformRand())/cmrate;
	  particleGun->SetParticleTime(prodTime);
	  //std::cout << "prodTime: " << prodTime << std::endl;
	  
	  nPart++;
	  particleGun->GeneratePrimaryVertex(anEvent);
	}
    }
}

void hbarPrimGenAction::SetParticle(G4String val) {

  if ((val == "p") || (val == "proton")) {
      particleName = "proton";
   } else if ((val == "anti_proton") || (val == "antiproton")
               || (val == "pbar")) {
      particleName = "anti_proton";
   } else if ((val == "anti_neutron") || (val == "antineutron")) {
      particleName = "anti_neutron";
   } else if ((val == "n") || (val == "neutron")) {
      particleName = "neutron";
   } else if ((val == "anti_hydrogen") || (val == "anti_hydrogen")
	      || (val == "hbar") || (val == "Hbar")) {
    particleName = "antihydrogen";
  } else if ((val == "hydrogen") || (val == "hydrogen") // Added by Clemens
	     || (val == "h") || (val == "H")) {
    particleName = "hydrogen";
  } else if ((val == "mu+") || (val == "muon+")) {
      particleName = "mu+";
   } else if ((val == "mu-") || (val == "muon-")) {
      particleName = "mu-";
   } else if ((val == "pi+") || (val == "pion+")) {
      particleName = "pi+";
   } else if ((val == "pi-") || (val == "pion-")) {
      particleName = "pi-";
   } else if ((val == "pi0") || (val == "pion0")) {
      particleName = "pi0";
   } else if (val == "cosmic") {
      particleName = "cosmic";
   } else if ((val == "electron") || (val == "e-")) {
      particleName = "e-";
   } else if ((val == "positron") || (val == "e+")) {
      particleName = "e+";
   }
   else if ((val == "k-")) {
      particleName = "kaon-"; }
   else if ((val == "k+")) {
      particleName = "kaon+"; }
   else if ((val == "k0")) {
      particleName = "kaon0"; }
   else if ((val == "opticalphoton")) {
      particleName = "opticalphoton"; }
}

// Added by Clemens -- start
void hbarPrimGenAction::UpdateCRY(std::string* MessInput)
{
  CRYSetup *setup = new CRYSetup(*MessInput,"./cry_v1.7/data");

  gen = new CRYGenerator(setup);

  // set random number generator
  RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
  setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
}

void hbarPrimGenAction::SetuseAEgIS(G4bool val){
  useAEgISData = val;
}

void hbarPrimGenAction::CRYFromFile(G4String newValue)
{
  // Read the cry input file
  std::ifstream inputFile;
  inputFile.open(newValue,std::ios::in);
  char buffer[1000];

  if (inputFile.fail()) 
	{
	  G4cout << "Failed to open input file " << newValue << G4endl;
	  G4cout << "Make sure to define the cry library on the command line" << G4endl;
	  //InputState=-1;
	}
  else
	{
	  std::string setupString("");
	  while ( !inputFile.getline(buffer,1000).eof()) 
		{
		  setupString.append(buffer);
		  setupString.append(" ");
		}

	  CRYSetup *setup=new CRYSetup(setupString,"./cry_v1.7/data");
	  
	  gen = new CRYGenerator(setup);
	  
	  // set random number generator
	  RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
	  setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
	  //InputState=0;
	}
}

void hbarPrimGenAction::SetSpectrumFilename(G4String filename)
{
  spectrumfilename = filename;
}

void hbarPrimGenAction::addBGParticle(G4String partname)
{
  if(particleList.empty()) 
    particleList.push_back(particleName);
  SetParticle(partname);
  particleList.push_back(particleName);
}

void hbarPrimGenAction::addBGParticleNum(G4int num)
{
  if(particleRatio.empty()) 
	particleRatio.push_back(1);
  particleRatio.push_back(num);
}


void hbarPrimGenAction::PrepareGroundState(hbarHydrogenLike * particle)
{
  ///Randomize the ground state (not same as the quantum numbers...)
  if (rndmFlagState == "on") 
    {
      G4double fm = G4UniformRand()*(StateARatio+StateBRatio+StateCRatio+StateDRatio);
      if (fm < StateARatio) 
		{
		  particle->SetF(0);
		  particle->SetM(0);
		} 
      else if (fm < StateARatio+StateBRatio) 
		{
		  particle->SetF(1);
		  particle->SetM(1);
		} 
      else if (fm < StateARatio+StateBRatio+StateCRatio)
		{
		  particle->SetF(1);
		  particle->SetM(0);
		} 
      else 
		{
		  particle->SetF(1);
		  particle->SetM(-1);
		}
    } 
  else if(rndmFlagState == "follow")
	{
	  bool choice = (G4UniformRand() >= 0.5);
	  bool flipUp = true;

	  if(particle->IsWeakFieldRegime())
		{
		  flipUp = particle->GetTwoMJ() > 0;
		}
	  else
		{
		  flipUp = particle->GetTwoMS() > 0;
		}

	  if(flipUp)
		{
		  if(choice)
			{
			  particle->SetF(0);
			  particle->SetM(0);
			}
		  else
			{
			  particle->SetF(1);
			  particle->SetM(1);
			}
		}
	  else
		{
		  if(choice)
			{
			  particle->SetF(1);
			  particle->SetM(-1);
			}
		  else
			{
			  particle->SetF(1);
			  particle->SetM(0);
			}
		}
	}
  else 
    {
      particle->SetF(StateF);
      particle->SetM(StateM);
    }
}

void hbarPrimGenAction::set_quantum_state(hbarHydrogenLike * particle)
{
  particle->ResetQuantumNumbers(useStrongInitialState);
  particle->SetEnableFieldRegimeTranslations(enableFieldRegimeTranslations);

  if(quantumStateFile.size() > 1)
	particle->SetStateFile(quantumStateFile);
  else
	{
	  if (quantumTransitionFlag == "on" || quantumEnergyForceFlag == "on")
		{
		  throw hbarException("Quantum transitions and/or quantum energy was enabled, but no quantum state database was specified.");
		}
	}
  if(preloadQuantumStates)
    {
      particle->PreloadStates();
    }

  particle->SetLowerHysteresisFactor(lowerHysteresis);
  particle->SetUpperHysteresisFactor(upperHysteresis);

  if (rndmRydbergState) //Just take uniform probability for now.
    {
	  if(particle->IsStrongFieldRegime())
		throw RLException("Can only set quantum state of particle with RandomRydberg in weak field regime, with a field %+13.10e we have strong field regime.", particle->GetField());

      G4int tempN = 1+(G4UniformRand()-1E-20)*(MaxStateN);
      particle->SetN(tempN);
      G4int maxL = (MaxStateL < tempN ) ? MaxStateL : (tempN-1);
      G4int tempL = (G4UniformRand()-1E-20)*(maxL+1);
      particle->SetL(tempL);
      G4int tempJ = G4UniformRand()>=0.5 ? 2*tempL + 1 : 2*tempL - 1;
      while(tempJ < 0) tempJ +=2;
      particle->SetTwoJ(tempJ);
      G4int tempMJ = 2*(G4int)((G4UniformRand()-1E-20)*(double(tempJ)*0.5))+1;
	  tempMJ *= (-1 +2*(G4UniformRand()>=0.5));
      particle->SetTwoMJ(tempMJ);
    }
  else
    {
	  particle->SetN(StateN);
	  particle->SetL(StateL);
	  if(!useStrongInitialState)
		{
		  particle->SetTwoJ(StateTwoJ);
		  particle->SetTwoMJ(StateTwoMJ);
		}
	  else
		{
		  particle->SetML(StateML);
		  particle->SetTwoMS(StateTwoMS);		  
		}
    }
  PrepareGroundState(particle);
  particle->SetDecayEnabled(quantumTransitionFlag=="on");
  particle->SetSplittingEnabled(quantumEnergyForceFlag=="on");
  if (quantumTransitionFlag == "on")
	{
	  particle->InitializeDecay();
	}
}
