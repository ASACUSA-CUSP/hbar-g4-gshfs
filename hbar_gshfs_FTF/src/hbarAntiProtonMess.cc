#include "hbarAntiProtonMess.hh"
#include "hbarAntiProtonAnnihilationAtRest.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarAntiProtonMess::hbarAntiProtonMess(hbarAntiProtonAnnihilationAtRest* pAnn)
:pbarAnn(pAnn)
{
  // Pbar process
  ///////////////
  pbarDir = new G4UIdirectory("/process/pbar/");
  pbarDir->SetGuidance("Pbar annihilation commands");
 
  pbarDebugFileNameCmd = new G4UIcmdWithAString("/process/pbar/setPionDebugFileName",this);
  pbarDebugFileNameCmd->SetGuidance("Set name of the file to dump the properties of the annihilation pions");
  pbarDebugFileNameCmd->SetGuidance("Setting it to \"\" means no dumping ");
  pbarDebugFileNameCmd->SetParameterName("value",true);
  pbarDebugFileNameCmd->SetDefaultValue("");
  pbarDebugFileNameCmd->AvailableForStates(G4State_Idle);

  pbarApplyPionAbsorptionCmd = new G4UIcmdWithAString("/process/pbar/setApplyPionAbsorption",this);
  pbarApplyPionAbsorptionCmd->SetGuidance("Apply absorption and energy loss to the annihilation pions");
  pbarApplyPionAbsorptionCmd->SetGuidance("  Choice : on(default), off");
  pbarApplyPionAbsorptionCmd->SetParameterName("value",true);
  pbarApplyPionAbsorptionCmd->SetDefaultValue("on");
  pbarApplyPionAbsorptionCmd->SetCandidates("on off");
  pbarApplyPionAbsorptionCmd->AvailableForStates(G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarAntiProtonMess::~hbarAntiProtonMess()
{
  delete pbarDebugFileNameCmd;
  delete pbarApplyPionAbsorptionCmd;
  delete pbarDir;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void hbarAntiProtonMess::SetNewValue(G4UIcommand* command,
                                          G4String newValue)
{       
  if( command == pbarDebugFileNameCmd )
   { pbarAnn->SetDebugFileName(newValue);}

  if( command == pbarApplyPionAbsorptionCmd )
   { pbarAnn->SetApplyPionAbsorption(newValue);}

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
