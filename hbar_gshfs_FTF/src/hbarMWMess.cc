#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithoutParameter.hh"

#include "hbarMWMess.hh"
#include "hbarMWSetup.hh"

hbarMWMess::hbarMWMess(hbarMWSetup *hbarMWS)
:hbarmwSetup(hbarMWS)
{ 
  hbarMWDir = new G4UIdirectory("/MW/");
  hbarMWDir->SetGuidance("Microwave properties control.");

  TransProbCmd = new G4UIcmdWithADouble("/MW/setTransitionProbabilities",this);
  TransProbCmd->SetGuidance("Set probability for all transitions");
  TransProbCmd->SetGuidance("  Choice : [0,1]");
  TransProbCmd->SetParameterName("value",true);
  TransProbCmd->SetDefaultValue(0.0);
  TransProbCmd->SetRange("value>=0.0");
  TransProbCmd->AvailableForStates(G4State_Idle);

  UseFixedTransProbCmd = new G4UIcmdWithAString("/MW/setUseFixedTransitionProbabilities",this);
  UseFixedTransProbCmd->SetGuidance("Use fixed transition probabilities");
  UseFixedTransProbCmd->SetGuidance("  Choice : on(default), off");
  UseFixedTransProbCmd->SetParameterName("value",true);
  UseFixedTransProbCmd->SetDefaultValue("on");
  UseFixedTransProbCmd->SetCandidates("on off");
  UseFixedTransProbCmd->AvailableForStates(G4State_Idle);

  MWFreqCmd = new G4UIcmdWithADoubleAndUnit("/MW/setMWFrequency",this);
  MWFreqCmd->SetGuidance("Set frequency of the MW field (megahertz)");
  MWFreqCmd->SetParameterName("Frequency",false);
  MWFreqCmd->SetDefaultUnit("megahertz");
  MWFreqCmd->SetRange("Frequency>0.0");
  MWFreqCmd->AvailableForStates(G4State_Idle);

  MWPowerCmd = new G4UIcmdWithADoubleAndUnit("/MW/setMWPower",this);
  MWPowerCmd->SetGuidance("Set total power of the MW field (watt)");
  MWPowerCmd->SetParameterName("Power",false);
  MWPowerCmd->SetDefaultUnit("watt");
  MWPowerCmd->SetRange("Power>=0.");
  MWPowerCmd->AvailableForStates(G4State_Idle);

  MWModeCmd = new G4UIcmdWithAString("/MW/setMWMode",this);
  MWModeCmd->SetGuidance("Set MW field mode");
  MWModeCmd->SetGuidance("  Choice : TM110(default), TM010, Const, const, Sin, sin, Cos, cos");
  MWModeCmd->SetParameterName("mode",true);
  MWModeCmd->SetDefaultValue("TM110");
  MWModeCmd->SetCandidates("TM110 TM010 Const const Sin sin Cos cos");
  MWModeCmd->AvailableForStates(G4State_Idle);

  CavityQCmd = new G4UIcmdWithADouble("/MW/setCavityQ",this);
  CavityQCmd->SetGuidance("Set Q of the cavity");
//  CavityQCmd->SetGuidance("  Choice : [0,1]");
  CavityQCmd->SetParameterName("value",false);
//  CavityQCmd->SetDefaultValue(100.0);
  CavityQCmd->SetRange("value>=0.0");
  CavityQCmd->AvailableForStates(G4State_Idle);

  CavityMFCmd = new G4UIcmdWithADoubleAndUnit("/MW/setCavityMagneticField",this);
  CavityMFCmd->SetGuidance("Set magnetic field in the cavity (tesla)");
  CavityMFCmd->SetParameterName("Strength",true);
  CavityMFCmd->SetDefaultValue(0.0);
  CavityMFCmd->SetDefaultUnit("tesla");
  CavityMFCmd->SetRange("Strength>=0.");
  CavityMFCmd->AvailableForStates(G4State_Idle);

  CavityMFDCmd = new G4UIcmdWithADoubleAndUnit("/MW/setCavityMagneticFieldVariation",this);
  CavityMFDCmd->SetGuidance("Set magnetic field variation i.e. uncertainty (full width) in the cavity (tesla)");
  CavityMFDCmd->SetParameterName("Strength",true);
  CavityMFDCmd->SetDefaultValue(0.0);
  CavityMFDCmd->SetDefaultUnit("tesla");
  CavityMFDCmd->SetRange("Strength>=0.");
  CavityMFDCmd->AvailableForStates(G4State_Idle);

  MWAngleCmd = new G4UIcmdWithADouble("/MW/setMWFieldAngle",this);
  MWAngleCmd->SetGuidance("Set angle between MW and static magnetic fields (degree)");
  MWAngleCmd->SetParameterName("value",false);
//  MWAngleCmd->SetRange("value>=0.0");
  MWAngleCmd->AvailableForStates(G4State_Idle);

  RandomPartNumCmd = new G4UIcmdWithABool("/MW/setRandomParticleNumber",this);
  RandomPartNumCmd->SetGuidance("Whether the number of particles to be shoot should be well-defined or random");
  RandomPartNumCmd->SetDefaultValue(true);
  RandomPartNumCmd->AvailableForStates(G4State_Idle);

  hbarMWScanDir = new G4UIdirectory("/MW/scan/");
  hbarMWScanDir->SetGuidance("Microwave scan properties control.");

  ScanOnCmd = new G4UIcmdWithAnInteger("/MW/scan/start",this);
  ScanOnCmd->SetGuidance("Start a MW scan");
  ScanOnCmd->SetParameterName("value",true);
  ScanOnCmd->SetDefaultValue(1);
  ScanOnCmd->SetRange("value>0");
  ScanOnCmd->AvailableForStates(G4State_Idle);

  ScanStartCmd = new G4UIcmdWithADoubleAndUnit("/MW/scan/setStartFrequency",this);
  ScanStartCmd->SetGuidance("First frequency of the scan (megahertz)");
  ScanStartCmd->SetParameterName("Frequency",false);
  ScanStartCmd->SetDefaultUnit("megahertz");
  ScanStartCmd->SetRange("Frequency>0.0");
  ScanStartCmd->AvailableForStates(G4State_Idle);

  ScanEndCmd = new G4UIcmdWithADoubleAndUnit("/MW/scan/setEndFrequency",this);
  ScanEndCmd->SetGuidance("Last frequency of the scan (megahertz)");
  ScanEndCmd->SetParameterName("Frequency",false);
  ScanEndCmd->SetDefaultUnit("megahertz");
  ScanEndCmd->SetRange("Frequency>0.0");
  ScanEndCmd->AvailableForStates(G4State_Idle);

  ScanStepsCmd = new G4UIcmdWithAnInteger("/MW/scan/setNumberOfSteps",this);
  ScanStepsCmd->SetGuidance("Number of steps in the scan. (default: 0)");
  ScanStepsCmd->SetGuidance("The number of frequency points is n+1!");
  ScanStepsCmd->SetParameterName("value",true);
  ScanStepsCmd->SetDefaultValue(0);
  ScanStepsCmd->SetRange("value>=0");
  ScanStepsCmd->AvailableForStates(G4State_Idle);

  ScanFileNameCmd = new G4UIcmdWithAString("/MW/scan/setFileName",this);
  ScanFileNameCmd->SetGuidance("Name of the ROOT file to save the scan results");
  ScanFileNameCmd->SetParameterName("fileName",true);
  ScanFileNameCmd->SetDefaultValue("rootfiles/g4_scan.root");
  ScanFileNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  UseCavityCmd = new G4UIcmdWithABool("/MW/setUseCavity",this);
  UseCavityCmd->SetGuidance("Whether to turn on the cavity or not, default is off.");
  UseCavityCmd->SetDefaultValue(false);
  UseCavityCmd->AvailableForStates(G4State_Idle);
}

hbarMWMess::~hbarMWMess()
{
  delete TransProbCmd; delete UseFixedTransProbCmd;
  delete hbarMWDir;
  delete MWFreqCmd;  delete MWPowerCmd; delete MWModeCmd; delete MWAngleCmd;
  delete RandomPartNumCmd;
  delete CavityQCmd; delete CavityMFCmd; delete CavityMFDCmd;
  delete hbarMWScanDir;
  delete ScanOnCmd; delete ScanStepsCmd;
  delete ScanStartCmd; delete ScanEndCmd; delete ScanFileNameCmd;
  delete UseCavityCmd;
}

void hbarMWMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == TransProbCmd ) {
     hbarmwSetup->SetFixedTransitionProbability(TransProbCmd->GetNewDoubleValue(newValue));
  }

  if( command == UseFixedTransProbCmd )
   { hbarmwSetup->SetUseFixedTransitionProbabilities(newValue);}

  if( command == MWFreqCmd )
   { hbarmwSetup->SetMWFrequency( MWFreqCmd->GetNewDoubleValue(newValue));}

  if( command == MWPowerCmd )
   { hbarmwSetup->SetMWPower( MWPowerCmd->GetNewDoubleValue(newValue));}

  if( command == MWModeCmd )
   { hbarmwSetup->SetMWMode(newValue);}

  if( command == CavityQCmd )
   { hbarmwSetup->SetCavityQ( CavityQCmd->GetNewDoubleValue(newValue));}

  if( command == CavityMFCmd )
   { hbarmwSetup->SetCavityMagneticField( CavityMFCmd->GetNewDoubleValue(newValue));}

  if( command == CavityMFDCmd )
   { hbarmwSetup->SetCavityMagneticFieldVariation( CavityMFDCmd->GetNewDoubleValue(newValue));}

  if( command == MWAngleCmd )
   { hbarmwSetup->SetMWFieldAngle( MWAngleCmd->GetNewDoubleValue(newValue));}

  if( command == RandomPartNumCmd )
   { hbarmwSetup->SetRandomParticleNumber( RandomPartNumCmd->GetNewBoolValue(newValue));}

  if( command == ScanOnCmd )
   { hbarmwSetup->Scan( ScanOnCmd->GetNewIntValue(newValue));}

  if( command == ScanStartCmd )
   { hbarmwSetup->SetScanStart( ScanStartCmd->GetNewDoubleValue(newValue));}

  if( command == ScanEndCmd )
   { hbarmwSetup->SetScanEnd( ScanEndCmd->GetNewDoubleValue(newValue));}

  if( command == ScanStepsCmd )
   { hbarmwSetup->SetScanNumberOfSteps( ScanStepsCmd->GetNewIntValue(newValue));}

  if( command == ScanFileNameCmd )
   { hbarmwSetup->SetScanFileName(newValue);}

  if( command == UseCavityCmd )
   { hbarmwSetup->SetUseCavity( RandomPartNumCmd->GetNewBoolValue(newValue));}
}


