#include "hbarTrackActionMess.hh"
#include "hbarTrackAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4ios.hh"

hbarTrackActionMess::hbarTrackActionMess(hbarTrackAction * aTraAct)
:trackAction(aTraAct)
{
  // To select which particle tracks to store in the simulation
  // none: no particle tracks are stored
  // primary: only the primary particle track is stored
  // pbar: only antiproton tracks are stored
  // hbar: only antihydrogen tracks are stored
  // heavy: only heavy particle tracks are stored, i.e. electron and photon tracks are deleted
  // all: all particle tracks are stored, except for neutrino tracks
  // allwithneutrino: all particle tracks are stored
  StoreCmd = new G4UIcmdWithAString("/tracking/storeTracks",this);
  StoreCmd->SetGuidance("Choose which tracks to store.");
  StoreCmd->SetGuidance("  Choice : none, primary(default), pbar, hbar, heavy, all, allwithneutrino");
  StoreCmd->SetParameterName("track",true);
  StoreCmd->SetDefaultValue ("primary");
  StoreCmd->SetCandidates("none primary charged pbar hbar heavy all allwithneutrino");
  StoreCmd->AvailableForStates(G4State_Idle);
}

hbarTrackActionMess::~hbarTrackActionMess()
{
  delete StoreCmd;
}

void hbarTrackActionMess::SetNewValue(G4UIcommand * command,G4String newValue)
{
  if( command==StoreCmd )
  { trackAction->SetStoreFlag(newValue); }
}

