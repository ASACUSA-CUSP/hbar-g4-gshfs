#include "hbarCUSPMess.hh"

hbarCUSPMess::hbarCUSPMess(hbarCUSPConst * _hbarCUSP)
  :hbarCUSP(_hbarCUSP)
{

  Bm0SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBeamline0Dimension",this);
  Bm0SetDimenCmd->SetGuidance("Set dimensions of the beamline0 tube");
  Bm0SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  Bm0SetDimenCmd->SetDefaultUnit("cm");
  Bm0SetDimenCmd->AvailableForStates(G4State_Idle);

  Bm0SetPlaceCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBeamline0Placement",this);
  Bm0SetPlaceCmd->SetGuidance("Set coordinates of the beamline0 tube");
  Bm0SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  Bm0SetPlaceCmd->SetDefaultUnit("m");
  Bm0SetPlaceCmd->AvailableForStates(G4State_Idle);
  
  Bm1SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBeamline1Dimension",this);
  Bm1SetDimenCmd->SetGuidance("Set dimensions of the beamline1 tube");
  Bm1SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  Bm1SetDimenCmd->SetDefaultUnit("cm");
  Bm1SetDimenCmd->AvailableForStates(G4State_Idle);

  Bm1SetPlaceCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBeamline1Placement",this);
  Bm1SetPlaceCmd->SetGuidance("Set coordinates of the beamline1 tube");
  Bm1SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  Bm1SetPlaceCmd->SetDefaultUnit("m");
  Bm1SetPlaceCmd->AvailableForStates(G4State_Idle);

  CenterOfTheCuspCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCenterOfTheCusp",this);
  CenterOfTheCuspCmd->SetGuidance("Set z position of the center of the cusp (mm)");
  CenterOfTheCuspCmd->SetParameterName("pos",false,false);
  CenterOfTheCuspCmd->SetDefaultUnit("mm");
  CenterOfTheCuspCmd->AvailableForStates(G4State_Idle);

  CenterOfTheMLSDCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCenterOfTheMLSD",this);
  CenterOfTheMLSDCmd->SetGuidance("Set z position of the center of the MLSD  (mm)");
  CenterOfTheMLSDCmd->SetParameterName("pos",false,false);
  CenterOfTheMLSDCmd->SetDefaultUnit("mm");
  CenterOfTheMLSDCmd->AvailableForStates(G4State_Idle);

  CenterOfTheSGVDCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCenterOfTheSGVD",this);
  CenterOfTheSGVDCmd->SetGuidance("Set z position of the center of the SGVD  (mm)");
  CenterOfTheSGVDCmd->SetParameterName("pos",false,false);
  CenterOfTheSGVDCmd->SetDefaultUnit("mm");
  CenterOfTheSGVDCmd->AvailableForStates(G4State_Idle);

  CenterOfTheMikiLenseCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCenterOfTheMikiLense",this);
  CenterOfTheMikiLenseCmd->SetGuidance("Set z position of the center of the MikiLense (mm)");
  CenterOfTheMikiLenseCmd->SetParameterName("pos",false,false);
  CenterOfTheMikiLenseCmd->SetDefaultUnit("mm");
  CenterOfTheMikiLenseCmd->AvailableForStates(G4State_Idle);

  UseCUSPprevCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPprev",this);
  UseCUSPprevCmd->SetGuidance("Swich CUSPprev on or off");
  UseCUSPprevCmd->SetParameterName("useflag",true,false);
  UseCUSPprevCmd->SetDefaultValue(0);
  UseCUSPprevCmd->SetRange("useflag>=0");
  UseCUSPprevCmd->AvailableForStates(G4State_Idle);

  UseCUSPMainCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPMain",this);
  UseCUSPMainCmd->SetGuidance("Swich CUSPMain on or off");
  UseCUSPMainCmd->SetParameterName("useflag",true,false);
  UseCUSPMainCmd->SetDefaultValue(0);
  UseCUSPMainCmd->SetRange("useflag>=0");
  UseCUSPMainCmd->AvailableForStates(G4State_Idle);

  UseCUSPMRECmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPMRE",this);
  UseCUSPMRECmd->SetGuidance("Swich CUSPMRE on or off");
  UseCUSPMRECmd->SetParameterName("useflag",true,false);
  UseCUSPMRECmd->SetDefaultValue(0);
  UseCUSPMRECmd->SetRange("useflag>=0");
  UseCUSPMRECmd->AvailableForStates(G4State_Idle);

  UseCUSPMREsupCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPMREsup",this);
  UseCUSPMREsupCmd->SetGuidance("Swich CUSPMREsup on or off");
  UseCUSPMREsupCmd->SetParameterName("useflag",true,false);
  UseCUSPMREsupCmd->SetDefaultValue(0);
  UseCUSPMREsupCmd->SetRange("useflag>=0");
  UseCUSPMREsupCmd->AvailableForStates(G4State_Idle);

  UseCUSPUHVCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPUHV",this);
  UseCUSPUHVCmd->SetGuidance("Swich CUSPUHV on or off");
  UseCUSPUHVCmd->SetParameterName("useflag",true,false);
  UseCUSPUHVCmd->SetDefaultValue(0);
  UseCUSPUHVCmd->SetRange("useflag>=0");
  UseCUSPUHVCmd->AvailableForStates(G4State_Idle);

  UseCUSPCuColCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPCuCol",this);
  UseCUSPCuColCmd->SetGuidance("Swich CUSPCuCol on or off");
  UseCUSPCuColCmd->SetParameterName("useflag",true,false);
  UseCUSPCuColCmd->SetDefaultValue(0);
  UseCUSPCuColCmd->SetRange("useflag>=0");
  UseCUSPCuColCmd->AvailableForStates(G4State_Idle);

  UseCUSPCuStCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPCuSt",this);
  UseCUSPCuStCmd->SetGuidance("Swich CUSPCuSt on or off");
  UseCUSPCuStCmd->SetParameterName("useflag",true,false);
  UseCUSPCuStCmd->SetDefaultValue(0);
  UseCUSPCuStCmd->SetRange("useflag>=0");
  UseCUSPCuStCmd->AvailableForStates(G4State_Idle);

  UseCUSPPolCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPPol",this);
  UseCUSPPolCmd->SetGuidance("Swich CUSPPol on or off");
  UseCUSPPolCmd->SetParameterName("useflag",true,false);
  UseCUSPPolCmd->SetDefaultValue(0);
  UseCUSPPolCmd->SetRange("useflag>=0");
  UseCUSPPolCmd->AvailableForStates(G4State_Idle);

  UseCUSPCuPlateCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPCuPlate",this);
  UseCUSPCuPlateCmd->SetGuidance("Swich CUSPCuPlate on or off");
  UseCUSPCuPlateCmd->SetParameterName("useflag",true,false);
  UseCUSPCuPlateCmd->SetDefaultValue(0);
  UseCUSPCuPlateCmd->SetRange("useflag>=0");
  UseCUSPCuPlateCmd->AvailableForStates(G4State_Idle);

  UseCUSPSUSbandCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPSUSband",this);
  UseCUSPSUSbandCmd->SetGuidance("Swich CUSPSUSband on or off");
  UseCUSPSUSbandCmd->SetParameterName("useflag",true,false);
  UseCUSPSUSbandCmd->SetDefaultValue(0);
  UseCUSPSUSbandCmd->SetRange("useflag>=0");
  UseCUSPSUSbandCmd->AvailableForStates(G4State_Idle);

  UseCUSPRadShieldCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPRadShield",this);
  UseCUSPRadShieldCmd->SetGuidance("Swich CUSPRadShield on or off");
  UseCUSPRadShieldCmd->SetParameterName("useflag",true,false);
  UseCUSPRadShieldCmd->SetDefaultValue(0);
  UseCUSPRadShieldCmd->SetRange("useflag>=0");
  UseCUSPRadShieldCmd->AvailableForStates(G4State_Idle);

  UseCUSPOVCCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPOVC",this);
  UseCUSPOVCCmd->SetGuidance("Swich CUSPOVC on or off");
  UseCUSPOVCCmd->SetParameterName("useflag",true,false);
  UseCUSPOVCCmd->SetDefaultValue(0);
  UseCUSPOVCCmd->SetRange("useflag>=0");
  UseCUSPOVCCmd->AvailableForStates(G4State_Idle);

  UseCUSPMgCoilCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPMgCoil",this);
  UseCUSPMgCoilCmd->SetGuidance("Swich CUSPMgCoil on or off");
  UseCUSPMgCoilCmd->SetParameterName("useflag",true,false);
  UseCUSPMgCoilCmd->SetDefaultValue(0);
  UseCUSPMgCoilCmd->SetRange("useflag>=0");
  UseCUSPMgCoilCmd->AvailableForStates(G4State_Idle);

  UseCUSPMgShieldCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPMgShield",this);
  UseCUSPMgShieldCmd->SetGuidance("Swich CUSPMgShield on or off");
  UseCUSPMgShieldCmd->SetParameterName("useflag",true,false);
  UseCUSPMgShieldCmd->SetDefaultValue(0);
  UseCUSPMgShieldCmd->SetRange("useflag>=0");
  UseCUSPMgShieldCmd->AvailableForStates(G4State_Idle);

  UseCUSPTPlsciCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSPTPlsci",this);
  UseCUSPTPlsciCmd->SetGuidance("Swich CUSPTPlsci on or off");
  UseCUSPTPlsciCmd->SetParameterName("useflag",true,false);
  UseCUSPTPlsciCmd->SetDefaultValue(0);
  UseCUSPTPlsciCmd->SetRange("useflag>=0");
  UseCUSPTPlsciCmd->AvailableForStates(G4State_Idle);
 
  UseMLSDUCTCmd = new G4UIcmdWithAnInteger("/setup/setUseMLSDUCT",this);
  UseMLSDUCTCmd->SetGuidance("Swich MLSDUCT on or off");
  UseMLSDUCTCmd->SetParameterName("useflag",true,false);
  UseMLSDUCTCmd->SetDefaultValue(0);
  UseMLSDUCTCmd->SetRange("useflag>=0");
  UseMLSDUCTCmd->AvailableForStates(G4State_Idle);
 
  UseSGVDUCTCmd = new G4UIcmdWithAnInteger("/setup/setUseSGVDUCT",this);
  UseSGVDUCTCmd->SetGuidance("Swich SGVDUCT on or off");
  UseSGVDUCTCmd->SetParameterName("useflag",true,false);
  UseSGVDUCTCmd->SetDefaultValue(0);
  UseSGVDUCTCmd->SetRange("useflag>=0");
  UseSGVDUCTCmd->AvailableForStates(G4State_Idle);

  UseMikiLenseCmd = new G4UIcmdWithAnInteger("/setup/setUseMikiLense",this);
  UseMikiLenseCmd->SetGuidance("Swich MikiLense on or off");
  UseMikiLenseCmd->SetParameterName("useflag",true,false);
  UseMikiLenseCmd->SetDefaultValue(0);
  UseMikiLenseCmd->SetRange("useflag>=0");
  UseMikiLenseCmd->AvailableForStates(G4State_Idle);
 
  UseMLDUCTCmd = new G4UIcmdWithAnInteger("/setup/setUseMLDUCT",this);
  UseMLDUCTCmd->SetGuidance("Swich MLDUCT on or off");
  UseMLDUCTCmd->SetParameterName("useflag",true,false);
  UseMLDUCTCmd->SetDefaultValue(0);
  UseMLDUCTCmd->SetRange("useflag>=0");
  UseMLDUCTCmd->AvailableForStates(G4State_Idle);
  
  UseMLELECmd = new G4UIcmdWithAnInteger("/setup/setUseMLELE",this);
  UseMLELECmd->SetGuidance("Swich MLELE on or off");
  UseMLELECmd->SetParameterName("useflag",true,false);
  UseMLELECmd->SetDefaultValue(0);
  UseMLELECmd->SetRange("useflag>=0");
  UseMLELECmd->AvailableForStates(G4State_Idle);
 
  // -- Cusp Main
  CuspMPartUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMPart",this);
  CuspMPartUseCmd->SetGuidance("Switch to CuspMPart number X");
  CuspMPartUseCmd->SetParameterName("Count",true,false);
  CuspMPartUseCmd->SetDefaultValue(0);
  CuspMPartUseCmd->SetRange("Count>=0");
  CuspMPartUseCmd->AvailableForStates(G4State_Idle);

  CuspMBSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMBDimension",this);
  CuspMBSetDimenCmd->SetGuidance("Set dimensions of the CuspMB ");
  CuspMBSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  CuspMBSetDimenCmd->SetDefaultUnit("mm");
  CuspMBSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMTSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMTDimension",this);
  CuspMTSetDimenCmd->SetGuidance("Set dimensions of the CuspMT ");
  CuspMTSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspMTSetDimenCmd->SetDefaultUnit("mm");
  CuspMTSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMHSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMHDimension",this);
  CuspMHSetDimenCmd->SetGuidance("Set dimensions of the CuspMH ");
  CuspMHSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspMHSetDimenCmd->SetDefaultUnit("mm");
  CuspMHSetDimenCmd->AvailableForStates(G4State_Idle);
  // -- MRE
  CuspMREUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMRE",this);
  CuspMREUseCmd->SetGuidance("Switch to CuspMRE number X");
  CuspMREUseCmd->SetParameterName("Count",true,false);
  CuspMREUseCmd->SetDefaultValue(0);
  CuspMREUseCmd->SetRange("Count>=0");
  CuspMREUseCmd->AvailableForStates(G4State_Idle);

  CuspMRESetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMREDimension",this);
  CuspMRESetDimenCmd->SetGuidance("Set dimensions of the CuspMRE ");
  CuspMRESetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspMRESetDimenCmd->SetDefaultUnit("mm");
  CuspMRESetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMRESetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMREPlacement",this);
  CuspMRESetPlaceCmd->SetGuidance("Set coordinates of the CuspMRE ");
  CuspMRESetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspMRESetPlaceCmd->SetDefaultUnit("mm");
  CuspMRESetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- MRE sup
  CuspMREsupUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMREsup",this);
  CuspMREsupUseCmd->SetGuidance("Switch to CuspMREsup number X");
  CuspMREsupUseCmd->SetParameterName("Count",true,false);
  CuspMREsupUseCmd->SetDefaultValue(0);
  CuspMREsupUseCmd->SetRange("Count>=0");
  CuspMREsupUseCmd->AvailableForStates(G4State_Idle);
  
  CuspMREsupSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMREsupDimension",this);
  CuspMREsupSetDimenCmd->SetGuidance("Set dimensions of the CuspMREsup");
  CuspMREsupSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  CuspMREsupSetDimenCmd->SetDefaultUnit("mm");
  CuspMREsupSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMREsupSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMREsupPlacement",this);
  CuspMREsupSetPlaceCmd->SetGuidance("Set coordinates of the CuspMREsup");
  CuspMREsupSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspMREsupSetPlaceCmd->SetDefaultUnit("mm");
  CuspMREsupSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- UHV bore      
  CuspUHVUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspUHV",this);
  CuspUHVUseCmd->SetGuidance("Switch to CuspUHV number X");
  CuspUHVUseCmd->SetParameterName("Count",true,false);
  CuspUHVUseCmd->SetDefaultValue(0);
  CuspUHVUseCmd->SetRange("Count>=0");
  CuspUHVUseCmd->AvailableForStates(G4State_Idle);

  CuspUHVbSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspUHVbDimension",this);
  CuspUHVbSetDimenCmd->SetGuidance("Set dimensions of the CuspUHVb ");
  CuspUHVbSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspUHVbSetDimenCmd->SetDefaultUnit("mm");
  CuspUHVbSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspUHVcnSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspUHVcnDimension",this);
  CuspUHVcnSetDimenCmd->SetGuidance("Set dimensions of the CuspUHVcn ");
  CuspUHVcnSetDimenCmd->SetParameterName("fod", "bod", "len",false,false);
  CuspUHVcnSetDimenCmd->SetDefaultUnit("mm");
  CuspUHVcnSetDimenCmd->AvailableForStates(G4State_Idle);
 
  CuspUHVcnSetFInDiaCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCuspUHVcnFInDia",this);
  CuspUHVcnSetFInDiaCmd->SetGuidance("Set dimensions of the CuspUHVcnFInDia");
  CuspUHVcnSetFInDiaCmd->SetParameterName("fid",false,false);
  CuspUHVcnSetFInDiaCmd->SetDefaultUnit("mm");
  CuspUHVcnSetFInDiaCmd->AvailableForStates(G4State_Idle);

  CuspUHVcnSetRInDiaCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCuspUHVcnRInDia",this);
  CuspUHVcnSetRInDiaCmd->SetGuidance("Set dimensions of the CuspUHVcnRInDia");
  CuspUHVcnSetRInDiaCmd->SetParameterName("bid",false,false);
  CuspUHVcnSetRInDiaCmd->SetDefaultUnit("mm");
  CuspUHVcnSetRInDiaCmd->AvailableForStates(G4State_Idle);

  CuspUHVSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspUHVPlacement",this);
  CuspUHVSetPlaceCmd->SetGuidance("Set coordinates of the CuspUHV");
  CuspUHVSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspUHVSetPlaceCmd->SetDefaultUnit("mm");
  CuspUHVSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- copper collar
  CuspCuColSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspCuColDimension",this);
  CuspCuColSetDimenCmd->SetGuidance("Set dimensions of the CuspCuCollar ");
  CuspCuColSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspCuColSetDimenCmd->SetDefaultUnit("mm");
  CuspCuColSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspCuColSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspCuColPlacement",this);
  CuspCuColSetPlaceCmd->SetGuidance("Set coordinates of the CuspCuCollar ");
  CuspCuColSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspCuColSetPlaceCmd->SetDefaultUnit("mm");
  CuspCuColSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- copper strip
  CuspCuStUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspCuSt",this);
  CuspCuStUseCmd->SetGuidance("Switch to CuspCuSt number X");
  CuspCuStUseCmd->SetParameterName("Count",true,false);
  CuspCuStUseCmd->SetDefaultValue(0);
  CuspCuStUseCmd->SetRange("Count>=0");
  CuspCuStUseCmd->AvailableForStates(G4State_Idle);
   
  CuspCuStSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspCuStDimension",this);
  CuspCuStSetDimenCmd->SetGuidance("Set dimensions of the CuspCuSt ");
  CuspCuStSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  CuspCuStSetDimenCmd->SetDefaultUnit("mm");
  CuspCuStSetDimenCmd->AvailableForStates(G4State_Idle);
  // -- poll
  CuspPolUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspPol",this);
  CuspPolUseCmd->SetGuidance("Switch to CuspPol number X");
  CuspPolUseCmd->SetParameterName("Count",true,false);
  CuspPolUseCmd->SetDefaultValue(0);
  CuspPolUseCmd->SetRange("Count>=0");
  CuspPolUseCmd->AvailableForStates(G4State_Idle);

  CuspPolSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspPolDimension",this);
  CuspPolSetDimenCmd->SetGuidance("Set dimensions of the CuspPol ");
  CuspPolSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspPolSetDimenCmd->SetDefaultUnit("mm");
  CuspPolSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspPolSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspPolPlacement",this);
  CuspPolSetPlaceCmd->SetGuidance("Set coordinates of the CuspPol ");
  CuspPolSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspPolSetPlaceCmd->SetDefaultUnit("mm");
  CuspPolSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- copper plate 
  CuspCuPlateUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspCuPlate",this);
  CuspCuPlateUseCmd->SetGuidance("Switch to CuspCuPlate number X");
  CuspCuPlateUseCmd->SetParameterName("Count",true,false);
  CuspCuPlateUseCmd->SetDefaultValue(0);
  CuspCuPlateUseCmd->SetRange("Count>=0");
  CuspCuPlateUseCmd->AvailableForStates(G4State_Idle);
 
  CuspCuPlateSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspCuPlateDimension",this);
  CuspCuPlateSetDimenCmd->SetGuidance("Set dimensions of the CuspCuPlate ");
  CuspCuPlateSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  CuspCuPlateSetDimenCmd->SetDefaultUnit("mm");
  CuspCuPlateSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspCuPlateSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspCuPlatePlacement",this);
  CuspCuPlateSetPlaceCmd->SetGuidance("Set coordinates of the CuspCuPlate  ");
  CuspCuPlateSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspCuPlateSetPlaceCmd->SetDefaultUnit("mm");
  CuspCuPlateSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- SUS band
  CuspSUSbandUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspSUSband",this);
  CuspSUSbandUseCmd->SetGuidance("Switch to CuspSUSband number X");
  CuspSUSbandUseCmd->SetParameterName("Count",true,false);
  CuspSUSbandUseCmd->SetDefaultValue(0);
  CuspSUSbandUseCmd->SetRange("Count>=0");
  CuspSUSbandUseCmd->AvailableForStates(G4State_Idle);

  CuspSUSbandSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspSUSbandDimension",this);
  CuspSUSbandSetDimenCmd->SetGuidance("Set dimensions of the CuspSUSband ");
  CuspSUSbandSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspSUSbandSetDimenCmd->SetDefaultUnit("mm");
  CuspSUSbandSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspSUSbandSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspSUSbandPlacement",this);
  CuspSUSbandSetPlaceCmd->SetGuidance("Set coordinates of the CuspSUSband ");
  CuspSUSbandSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspSUSbandSetPlaceCmd->SetDefaultUnit("mm");
  CuspSUSbandSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- copper cylinder (radiation shield)
  CuspRadShieldSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspRadShieldDimension",this);
  CuspRadShieldSetDimenCmd->SetGuidance("Set dimensions of the CuspRadShield ");
  CuspRadShieldSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspRadShieldSetDimenCmd->SetDefaultUnit("mm");
  CuspRadShieldSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspRadShieldSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspRadShieldPlacement",this);
  CuspRadShieldSetPlaceCmd->SetGuidance("Set coordinates of the CuspRadShield ");
  CuspRadShieldSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspRadShieldSetPlaceCmd->SetDefaultUnit("mm");
  CuspRadShieldSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- OVC
  CuspOVCUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspOVC",this);
  CuspOVCUseCmd->SetGuidance("Switch to CuspOVC number X");
  CuspOVCUseCmd->SetParameterName("Count",true,false);
  CuspOVCUseCmd->SetDefaultValue(0);
  CuspOVCUseCmd->SetRange("Count>=0");
  CuspOVCUseCmd->AvailableForStates(G4State_Idle);

  CuspOVCrUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspOVCr",this);
  CuspOVCrUseCmd->SetGuidance("Switch to CuspOVCr number X");
  CuspOVCrUseCmd->SetParameterName("Count",true,false);
  CuspOVCrUseCmd->SetDefaultValue(0);
  CuspOVCrUseCmd->SetRange("Count>=0");
  CuspOVCrUseCmd->AvailableForStates(G4State_Idle);

  CuspOVCrSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspOVCrDimension",this);
  CuspOVCrSetDimenCmd->SetGuidance("Set dimensions of the CuspOVCr ");
  CuspOVCrSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspOVCrSetDimenCmd->SetDefaultUnit("mm");
  CuspOVCrSetDimenCmd->AvailableForStates(G4State_Idle);
 
  CuspOVCSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspOVCDimension",this);
  CuspOVCSetDimenCmd->SetGuidance("Set dimensions of the CuspOVC ");
  CuspOVCSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspOVCSetDimenCmd->SetDefaultUnit("mm");
  CuspOVCSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspOVCSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspOVCPlacement",this);
  CuspOVCSetPlaceCmd->SetGuidance("Set coordinates of the CuspOVC");
  CuspOVCSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspOVCSetPlaceCmd->SetDefaultUnit("mm");
  CuspOVCSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- magnet coil
  CuspMgCoilUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMgCoil",this);
  CuspMgCoilUseCmd->SetGuidance("Switch to CuspMgCoil number X");
  CuspMgCoilUseCmd->SetParameterName("Count",true,false);
  CuspMgCoilUseCmd->SetDefaultValue(0);
  CuspMgCoilUseCmd->SetRange("Count>=0");
  CuspMgCoilUseCmd->AvailableForStates(G4State_Idle);
  
  CuspMgCoilSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMgCoilDimension",this);
  CuspMgCoilSetDimenCmd->SetGuidance("Set dimensions of the Cusp magnet coil");
  CuspMgCoilSetDimenCmd->SetParameterName("ind", "oud", "wd",false,false);
  CuspMgCoilSetDimenCmd->SetDefaultUnit("mm");
  CuspMgCoilSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMgCoilSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMgCoilPlacement",this);
  CuspMgCoilSetPlaceCmd->SetGuidance("Set coordinates of the Cusp magnet coil ");
  CuspMgCoilSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspMgCoilSetPlaceCmd->SetDefaultUnit("mm");
  CuspMgCoilSetPlaceCmd->AvailableForStates(G4State_Idle);
  // -- magnetic shield
  CuspMgShUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMgSh",this);
  CuspMgShUseCmd->SetGuidance("Switch to CuspMgSh number X");
  CuspMgShUseCmd->SetParameterName("Count",true,false);
  CuspMgShUseCmd->SetDefaultValue(0);
  CuspMgShUseCmd->SetRange("Count>=0");
  CuspMgShUseCmd->AvailableForStates(G4State_Idle);

  CuspMgShCnUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspMgShCn",this);
  CuspMgShCnUseCmd->SetGuidance("Switch to CuspMgShCn number X");
  CuspMgShCnUseCmd->SetParameterName("Count",true,false);
  CuspMgShCnUseCmd->SetDefaultValue(0);
  CuspMgShCnUseCmd->SetRange("Count>=0");
  CuspMgShCnUseCmd->AvailableForStates(G4State_Idle);

  CuspMgShSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMgShDimension",this);
  CuspMgShSetDimenCmd->SetGuidance("Set dimensions of the CuspMgSh ");
  CuspMgShSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  CuspMgShSetDimenCmd->SetDefaultUnit("mm");
  CuspMgShSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMgShCnSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspMgShCnDimension",this);
  CuspMgShCnSetDimenCmd->SetGuidance("Set dimensions of the CuspMgShCn ");
  CuspMgShCnSetDimenCmd->SetParameterName("foud", "boud", "len",false,false);
  CuspMgShCnSetDimenCmd->SetDefaultUnit("mm");
  CuspMgShCnSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspMgShCnSetFInDiaCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCuspMgShCnFInDia",this);
  CuspMgShCnSetFInDiaCmd->SetGuidance("Set dimensions of the CuspMgShCnFInDia");
  CuspMgShCnSetFInDiaCmd->SetParameterName("find",false,false);
  CuspMgShCnSetFInDiaCmd->SetDefaultUnit("mm");
  CuspMgShCnSetFInDiaCmd->AvailableForStates(G4State_Idle);

  CuspMgShCnSetRInDiaCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCuspMgShCnRInDia",this);
  CuspMgShCnSetRInDiaCmd->SetGuidance("Set dimensions of the CuspMgShCnRInDia");
  CuspMgShCnSetRInDiaCmd->SetParameterName("rind",false,false);
  CuspMgShCnSetRInDiaCmd->SetDefaultUnit("mm");
  CuspMgShCnSetRInDiaCmd->AvailableForStates(G4State_Idle);
  // -- track detector 
  CuspTPlsciUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCuspTPlsci",this);
  CuspTPlsciUseCmd->SetGuidance("Switch to CuspTPlsci number X");
  CuspTPlsciUseCmd->SetParameterName("Count",true,false);
  CuspTPlsciUseCmd->SetDefaultValue(0);
  CuspTPlsciUseCmd->SetRange("Count>=0");
  CuspTPlsciUseCmd->AvailableForStates(G4State_Idle);
 
  CuspTPlsciSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspTPlsciDimension",this);
  CuspTPlsciSetDimenCmd->SetGuidance("Set dimensions of the CuspTPlsci ");
  CuspTPlsciSetDimenCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  CuspTPlsciSetDimenCmd->SetDefaultUnit("mm");
  CuspTPlsciSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspTPlsciSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspTPlsciPlacement",this);
  CuspTPlsciSetPlaceCmd->SetGuidance("Set coordinates of the CuspTPlsci  ");
  CuspTPlsciSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspTPlsciSetPlaceCmd->SetDefaultUnit("mm");
  CuspTPlsciSetPlaceCmd->AvailableForStates(G4State_Idle);

  sensCuspTPLASCINCmd = new G4UIcmdWithAnInteger("/setup/sensCuspTPLASCIN",this);
  sensCuspTPLASCINCmd->SetGuidance("Set track detector sensitive");
  sensCuspTPLASCINCmd->SetParameterName("num",true,false);
  sensCuspTPLASCINCmd->SetDefaultValue(0);
  sensCuspTPLASCINCmd->SetRange("num>=0");
  sensCuspTPLASCINCmd->AvailableForStates(G4State_Idle);

  // -- duct between sextupole and GV
  SGVD0SetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setSGVD0Dimension",this);
  SGVD0SetDimenCmd->SetGuidance("Set dimensions of the SGVD0 ");
  SGVD0SetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  SGVD0SetDimenCmd->SetDefaultUnit("mm");
  SGVD0SetDimenCmd->AvailableForStates(G4State_Idle);

  SGVD1SetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setSGVD1Dimension",this);
  SGVD1SetDimenCmd->SetGuidance("Set dimensions of the SGVD1 ");
  SGVD1SetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  SGVD1SetDimenCmd->SetDefaultUnit("mm");
  SGVD1SetDimenCmd->AvailableForStates(G4State_Idle);
  // -- Miki Lense
  MLDuctPUseCmd = new G4UIcmdWithAnInteger("/setup/setMLDuctP",this);
  MLDuctPUseCmd->SetGuidance("Switch to MLDuctP number X");
  MLDuctPUseCmd->SetParameterName("Count",true,false);
  MLDuctPUseCmd->SetDefaultValue(0);
  MLDuctPUseCmd->SetRange("Count>=0");
  MLDuctPUseCmd->AvailableForStates(G4State_Idle);

  MLDuctPSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setMLDuctPDimension",this);
  MLDuctPSetDimenCmd->SetGuidance("Set dimensions of the MLDuctP ");
  MLDuctPSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  MLDuctPSetDimenCmd->SetDefaultUnit("mm");
  MLDuctPSetDimenCmd->AvailableForStates(G4State_Idle);

  MLEleSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setMLEleDimension",this);
  MLEleSetDimenCmd->SetGuidance("Set dimensions of the MLEle ");
  MLEleSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  MLEleSetDimenCmd->SetDefaultUnit("mm");
  MLEleSetDimenCmd->AvailableForStates(G4State_Idle);

  MLEleSubSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setMLEleSubDimension",this);
  MLEleSubSetDimenCmd->SetGuidance("Set dimensions of the MLEleSub ");
  MLEleSubSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  MLEleSubSetDimenCmd->SetDefaultUnit("mm");
  MLEleSubSetDimenCmd->AvailableForStates(G4State_Idle);
 
  // -- duct between MLduct and sextupole
  MLSDUseCmd = new G4UIcmdWithAnInteger("/setup/setMLSD",this);
  MLSDUseCmd->SetGuidance("Switch to MLSD number X");
  MLSDUseCmd->SetParameterName("Count",true,false);
  MLSDUseCmd->SetDefaultValue(0);
  MLSDUseCmd->SetRange("Count>=0");
  MLSDUseCmd->AvailableForStates(G4State_Idle);

  MLSDSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setMLSDDimension",this);
  MLSDSetDimenCmd->SetGuidance("Set dimensions of the MLSD ");
  MLSDSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  MLSDSetDimenCmd->SetDefaultUnit("mm");
  MLSDSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspElAsciiFileCmd = new G4UIcmdWithAString("/setup/cusp/setCuspElAsciiFile",this);
  CuspElAsciiFileCmd->SetGuidance("Set CUSP ASCII Electric Fieldmap");
  CuspElAsciiFileCmd->SetParameterName("CUSPElFieldASCII",false,false);
  CuspElAsciiFileCmd->AvailableForStates(G4State_Idle);

  CuspMagAsciiFileCmd = new G4UIcmdWithAString("/setup/cusp/setCuspMagAsciiFile",this);
  CuspMagAsciiFileCmd->SetGuidance("Set CUSP ASCII Magnetic Fieldmap");
  CuspMagAsciiFileCmd->SetParameterName("CUSPMagFieldASCII",false,false);
  CuspMagAsciiFileCmd->AvailableForStates(G4State_Idle);

  
  CuspBoxSetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspBox",this);
  CuspBoxSetDimenCmd->SetGuidance("Set dimensions of the Cusp box");
  CuspBoxSetDimenCmd->SetParameterName("wi", "thi", "le",false,false);
  CuspBoxSetDimenCmd->SetDefaultUnit("cm");
  CuspBoxSetDimenCmd->AvailableForStates(G4State_Idle);

  CuspT1SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspT1",this);
  CuspT1SetDimenCmd->SetGuidance("Set dimensions of the Cusp 1 tube");
  CuspT1SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  CuspT1SetDimenCmd->SetDefaultUnit("cm");
  CuspT1SetDimenCmd->AvailableForStates(G4State_Idle);

  CuspT2SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspT2",this);
  CuspT2SetDimenCmd->SetGuidance("Set dimensions of the Cusp 2 tube");
  CuspT2SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  CuspT2SetDimenCmd->SetDefaultUnit("cm");
  CuspT2SetDimenCmd->AvailableForStates(G4State_Idle);

  CuspSetPlaceCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCuspPlacement",this);
  CuspSetPlaceCmd->SetGuidance("Set coordinates of the cusp");
  CuspSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CuspSetPlaceCmd->SetDefaultUnit("m");
  CuspSetPlaceCmd->AvailableForStates(G4State_Idle);

  CBFUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setCBFUse",this);
  CBFUseCmd->SetGuidance("Switch to CBF number X");
  CBFUseCmd->SetParameterName("Count",true,false);
  CBFUseCmd->SetDefaultValue(0);
  CBFUseCmd->SetRange("Count>=0");
  CBFUseCmd->AvailableForStates(G4State_Idle);
  
  CBFSetInRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCBFInRad", this);
  CBFSetInRadCmd->SetGuidance("Set inner radius of CBFs");
  CBFSetInRadCmd->SetParameterName("InRad",false,false);
  CBFSetInRadCmd->SetDefaultUnit("cm");
  CBFSetInRadCmd->SetRange("InRad>=0.0");
  CBFSetInRadCmd->AvailableForStates(G4State_Idle);
  
  CBFSetOutRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCBFOutRad", this);
  CBFSetOutRadCmd->SetGuidance("Set outer radius of CBFs");
  CBFSetOutRadCmd->SetParameterName("OutRad",false,false);
  CBFSetOutRadCmd->SetDefaultUnit("cm");
  CBFSetOutRadCmd->SetRange("OutRad>0.0");
  CBFSetOutRadCmd->AvailableForStates(G4State_Idle);
  
  CBFSetLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setCBFLength", this);
  CBFSetLengthCmd->SetGuidance("Set thickness of CBF");
  CBFSetLengthCmd->SetParameterName("Length",false,false);
  CBFSetLengthCmd->SetDefaultUnit("cm");
  CBFSetLengthCmd->SetRange("Length>0.0");
  CBFSetLengthCmd->AvailableForStates(G4State_Idle);
  
  CBFSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCBFPlacement",this);
  CBFSetPlaceCmd->SetGuidance("Set center of the current CBF");
  CBFSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  CBFSetPlaceCmd->SetDefaultUnit("m");
  CBFSetPlaceCmd->AvailableForStates(G4State_Idle);

  GV1SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setGV1Dimension",this);
  GV1SetDimenCmd->SetGuidance("Set dimensions of the first gate valve");
  GV1SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  GV1SetDimenCmd->SetDefaultUnit("cm");
  GV1SetDimenCmd->AvailableForStates(G4State_Idle);

  Red0SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setRed0Dimension",this);
  Red0SetDimenCmd->SetGuidance("Set dimensions of the reducing flange");
  Red0SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  Red0SetDimenCmd->SetDefaultUnit("cm");
  Red0SetDimenCmd->AvailableForStates(G4State_Idle);

  BellSetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBellDimension",this);
  BellSetDimenCmd->SetGuidance("Set dimensions of the bellow");
  BellSetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  BellSetDimenCmd->SetDefaultUnit("cm");
  BellSetDimenCmd->AvailableForStates(G4State_Idle);
  
  HDFUseCmd = new G4UIcmdWithAnInteger("/setup/cusp/setHDFUse",this);
  HDFUseCmd->SetGuidance("Switch to HDF number X");
  HDFUseCmd->SetParameterName("Count",true,false);
  HDFUseCmd->SetDefaultValue(0);
  HDFUseCmd->SetRange("Count>=0");
  HDFUseCmd->AvailableForStates(G4State_Idle);
  
  HDFSetInRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setHDFInRad", this);
  HDFSetInRadCmd->SetGuidance("Set inner radius of HDFs");
  HDFSetInRadCmd->SetParameterName("InRad",false,false);
  HDFSetInRadCmd->SetDefaultUnit("cm");
  HDFSetInRadCmd->SetRange("InRad>=0.0");
  HDFSetInRadCmd->AvailableForStates(G4State_Idle);
  
  HDFSetOutRadCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setHDFOutRad", this);
  HDFSetOutRadCmd->SetGuidance("Set outer radius of HDFs");
  HDFSetOutRadCmd->SetParameterName("OutRad",false,false);
  HDFSetOutRadCmd->SetDefaultUnit("cm");
  HDFSetOutRadCmd->SetRange("OutRad>0.0");
  HDFSetOutRadCmd->AvailableForStates(G4State_Idle);
  
  HDFSetLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/cusp/setHDFLength", this);
  HDFSetLengthCmd->SetGuidance("Set thickness of HDF");
  HDFSetLengthCmd->SetParameterName("Length",false,false);
  HDFSetLengthCmd->SetDefaultUnit("cm");
  HDFSetLengthCmd->SetRange("Length>0.0");
  HDFSetLengthCmd->AvailableForStates(G4State_Idle);
  
  HDFSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setHDFPlacement",this);
  HDFSetPlaceCmd->SetGuidance("Set center of the current HDF");
  HDFSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  HDFSetPlaceCmd->SetDefaultUnit("m");
  HDFSetPlaceCmd->AvailableForStates(G4State_Idle);

  Ch0SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCh0Dimension",this);
  Ch0SetDimenCmd->SetGuidance("Set dimensions of the Chamber 0");
  Ch0SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  Ch0SetDimenCmd->SetDefaultUnit("cm");
  Ch0SetDimenCmd->AvailableForStates(G4State_Idle);

  Ch2SetDimenCmd= new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCh2Dimension",this);
  Ch2SetDimenCmd->SetGuidance("Set dimensions of the Chamber 2");
  Ch2SetDimenCmd->SetParameterName("inr", "our", "hi",false,false);
  Ch2SetDimenCmd->SetDefaultUnit("cm");
  Ch2SetDimenCmd->AvailableForStates(G4State_Idle);

  GV1SetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setGV1Placement",this);
  GV1SetPlaceCmd->SetGuidance("Set center of the first gate valve");
  GV1SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  GV1SetPlaceCmd->SetDefaultUnit("m"); 
  GV1SetPlaceCmd->AvailableForStates(G4State_Idle);
  
  Red0SetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setRed0Placement",this);
  Red0SetPlaceCmd->SetGuidance("Set center of the reducing flange");
  Red0SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  Red0SetPlaceCmd->SetDefaultUnit("m"); 
  Red0SetPlaceCmd->AvailableForStates(G4State_Idle);

  BellSetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setBellPlacement",this);
  BellSetPlaceCmd->SetGuidance("Set center of the bellow");
  BellSetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  BellSetPlaceCmd->SetDefaultUnit("m"); 
  BellSetPlaceCmd->AvailableForStates(G4State_Idle);

  Ch0SetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCh0Placement",this);
  Ch0SetPlaceCmd->SetGuidance("Set center of chamber 0");
  Ch0SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  Ch0SetPlaceCmd->SetDefaultUnit("m"); 
  Ch0SetPlaceCmd->AvailableForStates(G4State_Idle);

  Ch2SetPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/cusp/setCh2Placement",this);
  Ch2SetPlaceCmd->SetGuidance("Set center of chamber 2");
  Ch2SetPlaceCmd->SetParameterName("x", "y", "z",false,false);
  Ch2SetPlaceCmd->SetDefaultUnit("m"); 
  Ch2SetPlaceCmd->AvailableForStates(G4State_Idle);


}


hbarCUSPMess::~hbarCUSPMess()
{
  delete CenterOfTheCuspCmd;  
  delete CenterOfTheMikiLenseCmd;
  delete CenterOfTheSGVDCmd;  
  delete CenterOfTheMLSDCmd;
  delete UseCUSPprevCmd; 
  delete UseCUSPMainCmd; 
  delete UseCUSPMRECmd; 
  delete UseCUSPMREsupCmd; 
  delete UseCUSPUHVCmd;
  delete UseCUSPCuColCmd; 
  delete UseCUSPCuStCmd; 
  delete UseCUSPPolCmd; 
  delete UseCUSPCuPlateCmd;
  delete UseCUSPSUSbandCmd; 
  delete UseCUSPRadShieldCmd; 
  delete UseCUSPOVCCmd; 
  delete UseCUSPMgCoilCmd;
  delete UseCUSPMgShieldCmd; 
  delete UseMikiLenseCmd; 
  delete UseMLDUCTCmd; 
  delete UseMLELECmd;
  delete MLEleSubSetDimenCmd;
  delete UseSGVDUCTCmd; 
  delete UseMLSDUCTCmd;
  delete UseCUSPTPlsciCmd;
  delete CuspMPartUseCmd;
  delete CuspMBSetDimenCmd; 
  delete CuspMTSetDimenCmd; 
  delete CuspMHSetDimenCmd;
  delete CuspMREUseCmd; 
  delete CuspMRESetDimenCmd; 
  delete CuspMRESetPlaceCmd;
  delete CuspMREsupUseCmd; 
  delete CuspMREsupSetDimenCmd; 
  delete CuspMREsupSetPlaceCmd;
  delete CuspUHVUseCmd; 
  delete CuspUHVbSetDimenCmd; 
  delete CuspUHVcnSetDimenCmd;
  delete CuspUHVcnSetFInDiaCmd; 
  delete CuspUHVcnSetRInDiaCmd; 
  delete CuspUHVSetPlaceCmd;
  delete CuspCuColSetDimenCmd; 
  delete CuspCuColSetPlaceCmd;
  delete CuspCuStUseCmd; 
  delete CuspCuStSetDimenCmd;
  delete CuspPolUseCmd; 
  delete CuspPolSetDimenCmd;
  delete CuspPolSetPlaceCmd;
  delete CuspCuPlateUseCmd; 
  delete CuspCuPlateSetDimenCmd; 
  delete CuspCuPlateSetPlaceCmd;
  delete CuspSUSbandUseCmd; 
  delete CuspSUSbandSetDimenCmd; 
  delete CuspSUSbandSetPlaceCmd;
  delete CuspRadShieldSetDimenCmd; 
  delete CuspRadShieldSetPlaceCmd;
  delete CuspOVCUseCmd; 
  delete CuspOVCSetDimenCmd; 
  delete CuspOVCSetPlaceCmd;
  delete CuspOVCrUseCmd; 
  delete CuspOVCrSetDimenCmd;
  delete CuspMgCoilUseCmd; 
  delete CuspMgCoilSetDimenCmd; 
  delete CuspMgCoilSetPlaceCmd;
  delete CuspMgShUseCmd; 
  delete CuspMgShCnUseCmd;
  delete CuspMgShSetDimenCmd; 
  delete CuspMgShCnSetDimenCmd;
  delete CuspMgShCnSetFInDiaCmd; 
  delete CuspMgShCnSetRInDiaCmd;
  delete CuspTPlsciUseCmd; 
  delete CuspTPlsciSetDimenCmd; 
  delete CuspTPlsciSetPlaceCmd; 
  delete SGVD0SetDimenCmd; 
  delete SGVD1SetDimenCmd; 
  delete sensCuspTPLASCINCmd;
  delete MLSDUseCmd; 
  delete MLSDSetDimenCmd;
  delete MLDuctPUseCmd; 
  delete MLDuctPSetDimenCmd; 
  delete MLEleSetDimenCmd;
  delete CuspElAsciiFileCmd; 
  delete CuspMagAsciiFileCmd; 
  delete CuspBoxSetDimenCmd;
  delete CuspT1SetDimenCmd;
  delete CuspT2SetDimenCmd;
  delete CuspSetPlaceCmd;
  delete CBFUseCmd;
  delete CBFSetInRadCmd;
  delete CBFSetOutRadCmd;
  delete CBFSetLengthCmd;
  delete CBFSetPlaceCmd;
  delete GV1SetDimenCmd;
  delete Red0SetDimenCmd;
  delete BellSetDimenCmd;
  delete Ch0SetDimenCmd;
  delete Ch2SetDimenCmd;
  delete GV1SetPlaceCmd;
  delete Red0SetPlaceCmd;
  delete BellSetPlaceCmd;
  delete Ch0SetPlaceCmd;  
  delete Ch2SetPlaceCmd;
  delete HDFUseCmd;
  delete HDFSetInRadCmd;
  delete HDFSetOutRadCmd;
  delete HDFSetLengthCmd;
  delete HDFSetPlaceCmd;
  delete Bm0SetDimenCmd;  
  delete Bm0SetPlaceCmd;  
  delete Bm1SetDimenCmd;  
  delete Bm1SetPlaceCmd; 
  
  
  
}

void hbarCUSPMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if (command == Bm0SetDimenCmd) 
	{
	  hbarCUSP->SetBeamline0Dimension(Bm0SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Bm0SetPlaceCmd) 
	{
	  hbarCUSP->SetBeamline0Placement(Bm0SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Bm1SetDimenCmd) 
	{
	  hbarCUSP->SetBeamline1Dimension(Bm1SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Bm1SetPlaceCmd) 
	{
	  hbarCUSP->SetBeamline1Placement(Bm1SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if( command == CenterOfTheCuspCmd )
	{
	  hbarCUSP->SetCenterOfTheCusp(CenterOfTheCuspCmd->GetNewDoubleValue(newValue));
	}
  if( command == CenterOfTheSGVDCmd )
	{
	  hbarCUSP->SetCenterOfTheSGVD(CenterOfTheSGVDCmd->GetNewDoubleValue(newValue));
	}
  if( command == CenterOfTheMLSDCmd )
	{
	  hbarCUSP->SetCenterOfTheMLSD(CenterOfTheMLSDCmd->GetNewDoubleValue(newValue));
	}
  if( command == CenterOfTheMikiLenseCmd )
	{
	  hbarCUSP->SetCenterOfTheMikiLense(CenterOfTheMikiLenseCmd->GetNewDoubleValue(newValue));
	}
  if( command == UseCUSPprevCmd)
	{
	  hbarCUSP->SetUseCUSPprev(UseCUSPprevCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPMainCmd)
	{
	  hbarCUSP->SetUseCUSPMain(UseCUSPMainCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPMRECmd)
	{
	  hbarCUSP->SetUseCUSPMRE(UseCUSPMRECmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPMREsupCmd)
	{
	  hbarCUSP->SetUseCUSPMREsup(UseCUSPMREsupCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPUHVCmd)
	{
	  hbarCUSP->SetUseCUSPUHV(UseCUSPUHVCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPCuColCmd)
	{
	  hbarCUSP->SetUseCUSPCuCol(UseCUSPCuColCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPCuStCmd)
	{
	  hbarCUSP->SetUseCUSPCuSt(UseCUSPCuStCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPPolCmd)
	{
	  hbarCUSP->SetUseCUSPPol(UseCUSPPolCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPCuPlateCmd)
	{
	  hbarCUSP->SetUseCUSPCuPlate(UseCUSPCuPlateCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPSUSbandCmd)
	{
	  hbarCUSP->SetUseCUSPSUSband(UseCUSPSUSbandCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPRadShieldCmd)
	{
	  hbarCUSP->SetUseCUSPRadShield(UseCUSPRadShieldCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPOVCCmd)
	{
	  hbarCUSP->SetUseCUSPOVC(UseCUSPOVCCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPMgCoilCmd)
	{
	  hbarCUSP->SetUseCUSPMgCoil(UseCUSPMgCoilCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPMgShieldCmd)
	{
	  hbarCUSP->SetUseCUSPMgShield(UseCUSPMgShieldCmd->GetNewIntValue(newValue));
	}
  if( command == UseCUSPTPlsciCmd)
	{
	  hbarCUSP->SetUseCUSPTPlsci(UseCUSPTPlsciCmd->GetNewIntValue(newValue));
	}
  if( command == UseSGVDUCTCmd)
	{
	  hbarCUSP->SetUseSGVDUCT(UseSGVDUCTCmd->GetNewIntValue(newValue));
	}
  if( command == UseMLSDUCTCmd)
	{
	  hbarCUSP->SetUseMLSDUCT(UseMLSDUCTCmd->GetNewIntValue(newValue));
	}
  if( command == UseMikiLenseCmd)
	{
	  hbarCUSP->SetUseMikiLense(UseMikiLenseCmd->GetNewIntValue(newValue));
	}
  if( command == UseMLDUCTCmd)
	{
	  hbarCUSP->SetUseMLDUCT(UseMLDUCTCmd->GetNewIntValue(newValue));
	}
  if( command == UseMLELECmd)
	{
	  hbarCUSP->SetUseMLELE(UseMLELECmd->GetNewIntValue(newValue));
	}
  // -- Cusp Main
  if( command == CuspMPartUseCmd )
	{
	  hbarCUSP->UseCuspMPart(CuspMPartUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspMBSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMBDimension(CuspMBSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMTSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMTDimension(CuspMTSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMHSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMHDimension(CuspMHSetDimenCmd->GetNew3VectorValue(newValue));
	}
  // -- MRE
  if( command == CuspMREUseCmd )
	{
	  hbarCUSP->UseCuspMRE(CuspMREUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspMRESetDimenCmd) 
	{
	  hbarCUSP->SetCuspMREDimension(CuspMRESetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMRESetPlaceCmd) 
	{
	  hbarCUSP->SetCuspMREPlacement(CuspMRESetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- MRE support
  if( command == CuspMREsupUseCmd )
	{
	  hbarCUSP->UseCuspMREsup(CuspMREsupUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspMREsupSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMREsupDimension(CuspMREsupSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMREsupSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspMREsupPlacement(CuspMREsupSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- UHV
  if( command == CuspUHVUseCmd )
	{
	  hbarCUSP->UseCuspUHV(CuspUHVUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspUHVbSetDimenCmd) 
	{
	  hbarCUSP->SetCuspUHVbDimension(CuspUHVbSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspUHVcnSetDimenCmd) 
	{
	  hbarCUSP->SetCuspUHVcnDimension(CuspUHVcnSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspUHVcnSetFInDiaCmd) 
	{
	  hbarCUSP->SetCuspUHVcnFrontInnerRadius(CuspUHVcnSetFInDiaCmd->GetNewDoubleValue(newValue));
	}
  if (command == CuspUHVcnSetRInDiaCmd) 
	{
	  hbarCUSP->SetCuspUHVcnRearInnerRadius(CuspUHVcnSetRInDiaCmd->GetNewDoubleValue(newValue));
	}
  if (command == CuspUHVSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspUHVPlacement(CuspUHVSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- copper collar
  if (command == CuspCuColSetDimenCmd) 
	{
	  hbarCUSP->SetCuspCuColDimension(CuspCuColSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspCuColSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspCuColPlacement(CuspCuColSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- copper strip
  if( command == CuspCuStUseCmd )
	{
	  hbarCUSP->UseCuspCuSt(CuspCuStUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspCuStSetDimenCmd) 
	{
	  hbarCUSP->SetCuspCuStDimension(CuspCuStSetDimenCmd->GetNew3VectorValue(newValue));
	} 
  // -- poll
  if( command == CuspPolUseCmd )
	{
	  hbarCUSP->UseCuspPol(CuspPolUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspPolSetDimenCmd) 
	{
	  hbarCUSP->SetCuspPolDimension(CuspPolSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspPolSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspPolPlacement(CuspPolSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- copper plate
  if( command == CuspCuPlateUseCmd )
	{
	  hbarCUSP->UseCuspCuPlate(CuspCuPlateUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspCuPlateSetDimenCmd) 
	{
	  hbarCUSP->SetCuspCuPlateDimension(CuspCuPlateSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspCuPlateSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspCuPlatePlacement(CuspCuPlateSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- SUS band
  if( command == CuspSUSbandUseCmd )
	{
	  hbarCUSP->UseCuspSUSband(CuspSUSbandUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspSUSbandSetDimenCmd) 
	{
	  hbarCUSP->SetCuspSUSbandDimension(CuspSUSbandSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspSUSbandSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspSUSbandPlacement(CuspSUSbandSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- copper cylinder (radiation shield)
  if (command == CuspRadShieldSetDimenCmd) 
	{
	  hbarCUSP->SetCuspRadShieldDimension(CuspRadShieldSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspRadShieldSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspRadShieldPlacement(CuspRadShieldSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- OVC
  if( command == CuspOVCUseCmd )
	{
	  hbarCUSP->UseCuspOVC(CuspOVCUseCmd->GetNewIntValue(newValue));
	}
  if( command == CuspOVCrUseCmd )
	{
	  hbarCUSP->UseCuspOVCr(CuspOVCrUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspOVCrSetDimenCmd) 
	{
	  hbarCUSP->SetCuspOVCrDimension(CuspOVCrSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspOVCSetDimenCmd) 
	{
	  hbarCUSP->SetCuspOVCDimension(CuspOVCSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspOVCSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspOVCPlacement(CuspOVCSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- magnet coil
  if( command == CuspMgCoilUseCmd )
	{
	  hbarCUSP->UseCuspMgCoil(CuspMgCoilUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspMgCoilSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMgCoilDimension(CuspMgCoilSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMgCoilSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspMgCoilPlacement(CuspMgCoilSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  // -- magnetic shield
  if( command == CuspMgShUseCmd )
	{
	  hbarCUSP->UseCuspMgSh(CuspMgShUseCmd->GetNewIntValue(newValue));
	}
  if( command == CuspMgShCnUseCmd )
	{
	  hbarCUSP->UseCuspMgShCn(CuspMgShCnUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspMgShSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMgShDimension(CuspMgShSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMgShCnSetDimenCmd) 
	{
	  hbarCUSP->SetCuspMgShCnDimension(CuspMgShCnSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspMgShCnSetFInDiaCmd) 
	{
	  hbarCUSP->SetCuspMgShCnFrontInnerRadius(CuspMgShCnSetFInDiaCmd->GetNewDoubleValue(newValue));
	}
  if (command == CuspMgShCnSetRInDiaCmd) 
	{
	  hbarCUSP->SetCuspMgShCnRearInnerRadius(CuspMgShCnSetRInDiaCmd->GetNewDoubleValue(newValue));
	}
  // -- track detector
  if( command == CuspTPlsciUseCmd )
	{
	  hbarCUSP->UseCuspTPlsci(CuspTPlsciUseCmd->GetNewIntValue(newValue));
	}
  if (command == CuspTPlsciSetDimenCmd) 
	{
	  hbarCUSP->SetCuspTPlsciDimension(CuspTPlsciSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspTPlsciSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspTPlsciPlacement(CuspTPlsciSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  if( command == sensCuspTPLASCINCmd)
	{
	  hbarCUSP->SetsensCuspTPLASCIN(sensCuspTPLASCINCmd->GetNewIntValue(newValue));
	}

  if (command == SGVD0SetDimenCmd) 
	{
	  hbarCUSP->SetSGVD0Dimension(SGVD0SetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == SGVD1SetDimenCmd) 
	{
	  hbarCUSP->SetSGVD1Dimension(SGVD1SetDimenCmd->GetNew3VectorValue(newValue));
	}

  if( command == MLSDUseCmd )
	{
	  hbarCUSP->UseMLSD(MLSDUseCmd->GetNewIntValue(newValue));
	}
  if (command == MLSDSetDimenCmd) 
	{
	  hbarCUSP->SetMLSDDimension(MLSDSetDimenCmd->GetNew3VectorValue(newValue));
	}

  // -- Miki Lense
  if( command == MLDuctPUseCmd )
	{
	  hbarCUSP->UseMLDuctP(MLDuctPUseCmd->GetNewIntValue(newValue));
	}
  if (command == MLDuctPSetDimenCmd) 
	{
	  hbarCUSP->SetMLDuctPDimension(MLDuctPSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == MLEleSetDimenCmd) 
	{
	  hbarCUSP->SetMLEleDimension(MLEleSetDimenCmd->GetNew3VectorValue(newValue));
	}
  if (command == MLEleSubSetDimenCmd) 
	{
	  hbarCUSP->SetMLEleSubDimension(MLEleSubSetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == CuspBoxSetDimenCmd) 
	{
	  hbarCUSP->SetCuspBoxDimension(CuspBoxSetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == CuspT1SetDimenCmd) 
	{
	  hbarCUSP->SetCuspT1Dimension(CuspT1SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == CuspT2SetDimenCmd) 
	{
	  hbarCUSP->SetCuspT2Dimension(CuspT2SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == CuspSetPlaceCmd) 
	{
	  hbarCUSP->SetCuspPlacement(CuspSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if( command == CBFUseCmd )
    {
	  hbarCUSP->UseCBF(CBFUseCmd->GetNewIntValue(newValue));
	}
  
  if (command == CBFSetInRadCmd)
    {
	  hbarCUSP->SetCBFInRad(CBFSetInRadCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == CBFSetOutRadCmd)
    {
	  hbarCUSP->SetCBFOutRad(CBFSetOutRadCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == CBFSetLengthCmd)
    {
	  hbarCUSP->SetCBFLength(CBFSetLengthCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == CBFSetPlaceCmd) 
	{
	  hbarCUSP->SetCBFPlacement(CBFSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == GV1SetDimenCmd) 
	{
	  hbarCUSP->SetGV1Dimension(GV1SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Red0SetDimenCmd) 
	{
	  hbarCUSP->SetRed0Dimension(Red0SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == BellSetDimenCmd) 
	{
	  hbarCUSP->SetBellDimension(BellSetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Ch0SetDimenCmd) 
	{
	  hbarCUSP->SetCh0Dimension(Ch0SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Ch2SetDimenCmd) 
	{
	  hbarCUSP->SetCh2Dimension(Ch2SetDimenCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == GV1SetPlaceCmd) 
	{
	  hbarCUSP->SetGV1Placement(GV1SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Red0SetPlaceCmd) 
	{
	  hbarCUSP->SetRed0Placement(Red0SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == BellSetPlaceCmd) 
	{
	  hbarCUSP->SetBellPlacement(BellSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Ch0SetPlaceCmd) 
	{
	  hbarCUSP->SetCh0Placement(Ch0SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if (command == Ch2SetPlaceCmd) 
	{
	  hbarCUSP->SetCh2Placement(Ch2SetPlaceCmd->GetNew3VectorValue(newValue));
	}
  
  if( command == HDFUseCmd )
	{
	  hbarCUSP->UseHDF(HDFUseCmd->GetNewIntValue(newValue));
	}
  
  if (command == HDFSetInRadCmd)
	{
	  hbarCUSP->SetHDFInRad(HDFSetInRadCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == HDFSetOutRadCmd)
	{
	  hbarCUSP->SetHDFOutRad(HDFSetOutRadCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == HDFSetLengthCmd)
	{
	  hbarCUSP->SetHDFLength(HDFSetLengthCmd->GetNewDoubleValue(newValue));
	}
  
  if (command == HDFSetPlaceCmd) 
	{
	  hbarCUSP->SetHDFPlacement(HDFSetPlaceCmd->GetNew3VectorValue(newValue));
	}
  if (command == CuspElAsciiFileCmd)
    {
	  hbarCUSP->SetCuspElAsciiFile(newValue);
	}
  if (command == CuspMagAsciiFileCmd)
    {
	  hbarCUSP->SetCuspMagAsciiFile(newValue);
	}
}
