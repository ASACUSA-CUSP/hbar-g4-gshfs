#include "hbarAntiHydrogen.hh"

hbarAntiHydrogen* hbarAntiHydrogen::theInstance = 0;

hbarAntiHydrogen* hbarAntiHydrogen::Definition()
{
  if (theInstance !=0) 
    return theInstance;

  const G4String name = "antihydrogen";
  
  // Search in particle table for name.
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);

  if (anInstance ==0)
  {
    anInstance = new hbarAntiHydrogen(name);
  }
  
  theInstance = reinterpret_cast<hbarAntiHydrogen*>(anInstance);

  return theInstance;

}
hbarAntiHydrogen::hbarAntiHydrogen(const G4String& name)
  :hbarHydrogenLike(name)
{
  
}


