#include "hbarCavityConst.hh"

hbarCavityConst::hbarCavityConst(hbarMWSetup * toUse)
  :mwSetup(toUse), useCavityFieldmap(false)
{
  MWCavityInnerDiam = 0.317*m;
  MWCavityOuterDiam = 0.460*m;

  MWCavityInnerRingLength = 73.3*mm;
  MWCavityLength = 0.105*m;
  MWCavityInnerRingThickness = 0.004*m;
  MWCavityOuterRingThickness = 0.0299*m;

  MWCavityPlateHoleDiam = 0.1302*m;
  MWCavityPlateDiam = MWCavityOuterDiam-1*mm;

  MWCavityCenter = 0.0*m;
  MWCavityResThick = 0.004*m;
  MWCavityResWidth = 0.21005*m;
  MWCavityResSep = 0.104*m;

  HelmholtzCoilsInnerDiam = 0.439*m;
  HelmholtzCoilsOuterDiam = 0.454*m;
  HelmholtzCoilThickness = 0.016*m;
  HelmholtzCoilDistance = 0.212*m;

  SupportRingHHInnerDiam = HelmholtzCoilsInnerDiam-0.005*m;
  SupportRingHHMidDiam = HelmholtzCoilsInnerDiam;
  SupportRingHHOuterDiam = HelmholtzCoilsOuterDiam+0.006*m;
  SupportRingHHOuterThick = 0.004*m;

  ShieldingThickness = 0.001*m;
  InnerLayerShieldLength = 0.561*m;
  InnerLayerShieldHight = 0.529*m; // Update according drawing from 2014 (old value: 0.606)
  MiddleLayerShieldLength = 0.561*m;
  MiddleLayerShieldHight = 0.636*m;
  ShieldHoleDiam = 0.140*m;

  CavityWingsLength = 0.053*m;;
  CavityWingsWidth = 0.036*m;
  CavityWingsThickness = 0.004*m;
  
  UseCavityMiddleShielding = true;
  UseCavityInnerShielding = true;
  useCavityFieldmap = false;
  UseHelmholtzCoils = true;
  UseSupportRingsHH = true;
  

  cavityField = new hbarCavityField();

  cavityMess = new hbarCavityMess(this);

}

hbarCavityConst::~hbarCavityConst()
{
  delete cavityMess;
  if (mwSetup)
	delete mwSetup;
  if (cavityField)
	delete cavityField; // Added by Bernadette

}

void hbarCavityConst::MakeCavity(G4VPhysicalVolume * WorldPhy)
{
  // Microwave cavity
  if(useCavityFieldmap != 0)
	cavityField->Initialize(cavityFieldfilename.data()); // Added by Bernadette

  G4Tubs* CavityInnerRingSol = new G4Tubs("CavityInnerRing", MWCavityInnerDiam*0.5,
										  (MWCavityInnerDiam+MWCavityInnerRingThickness)*0.5,
										  MWCavityInnerRingLength*0.5, 0.0, 2.0*M_PI);

  G4Tubs* CavityOuterRingsSol = new G4Tubs("CavityOuterRing",
										   (MWCavityInnerDiam+MWCavityInnerRingThickness*0.5)*0.5,
										   MWCavityOuterDiam*0.5, MWCavityOuterRingThickness*0.5,
										   0.0, 2.0*M_PI);

  G4VSolid* CavitySoltemp = new G4UnionSolid("Cavitytemp", CavityInnerRingSol, CavityOuterRingsSol,
											 0, G4ThreeVector(0,0,MWCavityInnerRingLength*0.5));

  CavitySol = new G4UnionSolid("Cavity", CavitySoltemp, CavityOuterRingsSol, 0,
							   G4ThreeVector(0,0,-MWCavityInnerRingLength*0.5));

  CavityLog = new G4LogicalVolume(CavitySol, hbarDetConst::GetMaterial("Sts"), "Cavity");

  CavityPhy = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, MWCavityCenter),
								"Cavity", CavityLog, WorldPhy, false, 0);


  MWCavityPlateSol = new G4Tubs("CavityPlate", MWCavityPlateHoleDiam*0.5,
								MWCavityPlateDiam*0.5, MWCavityOuterRingThickness*0.5, 0.0, 2.0*M_PI);
  MWCavityPlateLog = new G4LogicalVolume(MWCavityPlateSol, hbarDetConst::GetMaterial("Sts"), "CavityPlate");
  MWCavityPlatePhys1 = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, MWCavityCenter+MWCavityInnerRingLength*0.5+MWCavityOuterRingThickness),
										 "CavityPlate1", MWCavityPlateLog, WorldPhy, false, 0);
  MWCavityPlatePhys2 = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, MWCavityCenter-MWCavityInnerRingLength*0.5-MWCavityOuterRingThickness),
										 "CavityPlate2", MWCavityPlateLog, WorldPhy, false, 0);

  G4RotationMatrix* rm = new G4RotationMatrix();
  rm->rotateY(90.0*deg);

  if(UseHelmholtzCoils)
	{
	  // Helmholtzcoils
	  HelmholtzCoilSol = new G4Tubs("HelmholtzCoil", HelmholtzCoilsInnerDiam*0.5,
									HelmholtzCoilsOuterDiam*0.5, HelmholtzCoilThickness*0.5 ,0.0, 2.0*M_PI);
	  HelmholtzCoilLog = new G4LogicalVolume(HelmholtzCoilSol, hbarDetConst::GetMaterial("Copper"), "HelmholtzCoil");

	  G4VisAttributes* bronzecolor = new G4VisAttributes(G4Colour(1.0, 0.62, 0.24));
	  HelmholtzCoilLog->SetVisAttributes(bronzecolor);

	  HelmholtzCoilPhys1 = new G4PVPlacement(rm, G4ThreeVector(HelmholtzCoilDistance*0.5+HelmholtzCoilThickness, 0.0, MWCavityCenter),
											 "HelmholtzCoil", HelmholtzCoilLog, WorldPhy, false, 0);
	  HelmholtzCoilPhys2 = new G4PVPlacement(rm, G4ThreeVector(-(HelmholtzCoilDistance*0.5+HelmholtzCoilThickness), 0.0, MWCavityCenter),
											 "HelmholtzCoil", HelmholtzCoilLog, WorldPhy, false, 0);
	}
  if(UseSupportRingsHH==1)
	{
	  //SupportRings
	  SupportRingHHSol = new G4Tubs("SupportRingHH", SupportRingHHInnerDiam*0.5, SupportRingHHMidDiam*0.5,
									HelmholtzCoilThickness*0.5, 0.0, 2.0*M_PI);
	  SupportRingHHLog = new G4LogicalVolume(SupportRingHHSol, hbarDetConst::GetMaterial("Epoxy"), "SupportRingHH");

	  G4VisAttributes* lightyellowcolor = new G4VisAttributes(G4Colour(0.8, 0.8, 0.0));
	  SupportRingHHLog->SetVisAttributes(lightyellowcolor);

	  SupportRingHHPhys1 = new G4PVPlacement(rm, G4ThreeVector(HelmholtzCoilDistance*0.5+HelmholtzCoilThickness, 0.0, MWCavityCenter),
											 "SupportRingHH", SupportRingHHLog, WorldPhy, false, 0);
	  SupportRingHHPhys2 = new G4PVPlacement(rm, G4ThreeVector(-(HelmholtzCoilDistance*0.5+HelmholtzCoilThickness), 0.0, MWCavityCenter),
											 "SupportRingHH", SupportRingHHLog, WorldPhy, false, 0);

	  SupportRingHHOuterSol = new G4Tubs("SupportRingHHOuter", SupportRingHHInnerDiam*0.5, SupportRingHHOuterDiam*0.5,
										 SupportRingHHOuterThick*0.5, 0.0, 2.0*M_PI);
	  SupportRingHHOuterLog = new G4LogicalVolume(SupportRingHHOuterSol, hbarDetConst::GetMaterial("Epoxy"), "SupportRingHHOuter");
	  SupportRingHHOuterLog->SetVisAttributes(lightyellowcolor);

	  SupportRingHHOuterPhys1 = new G4PVPlacement(rm, G4ThreeVector(HelmholtzCoilDistance*0.5+(HelmholtzCoilThickness)*0.5-SupportRingHHOuterThick*0.5, 0.0, MWCavityCenter),
												  "SupportRingHHOuter", SupportRingHHOuterLog, WorldPhy, false, 0);
	  SupportRingHHOuterPhys2 = new G4PVPlacement(rm, G4ThreeVector((HelmholtzCoilDistance*0.5+(HelmholtzCoilThickness)*1.5+SupportRingHHOuterThick*0.5), 0.0, MWCavityCenter),
												  "SupportRingHHOuter", SupportRingHHOuterLog, WorldPhy, false, 0);
	  SupportRingHHOuterPhys3 = new G4PVPlacement(rm, G4ThreeVector(-(HelmholtzCoilDistance*0.5+(HelmholtzCoilThickness)*0.5-SupportRingHHOuterThick*0.5), 0.0, MWCavityCenter),
												  "SupportRingHHOuter", SupportRingHHOuterLog, WorldPhy, false, 0);
	  SupportRingHHOuterPhys4 = new G4PVPlacement(rm, G4ThreeVector(-HelmholtzCoilDistance*0.5-(HelmholtzCoilThickness)*1.5-SupportRingHHOuterThick*0.5, 0.0, MWCavityCenter),
												  "SupportRingHHOuter", SupportRingHHOuterLog, WorldPhy, false, 0);
	}
  G4VisAttributes* greycolor = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  // Shielding
  if(UseCavityInnerShielding)
	{
	  // Inner Shielding
	  InnerInnerBox = new G4Box("InnerInnerBox", (InnerLayerShieldLength-ShieldingThickness)*0.5, (InnerLayerShieldLength-ShieldingThickness)*0.5,
								(InnerLayerShieldHight-ShieldingThickness)*0.5);
	  InnerOuterBox = new G4Box("InnerOuterBox", (InnerLayerShieldLength+ShieldingThickness)*0.5, (InnerLayerShieldLength+ShieldingThickness)*0.5,
								(InnerLayerShieldHight+ShieldingThickness)*0.5);
	  InnerLayerShieldBox = new G4SubtractionSolid("InnerLayerShieldBox", InnerOuterBox, InnerInnerBox, 0, G4ThreeVector(0,0,0));
	  ShieldHoleSolid = new G4Tubs("ShieldHole", 0.0, ShieldHoleDiam*0.5, InnerLayerShieldLength, 0.0, 2.0*M_PI);
	  InnerLayerShieldSol = new G4SubtractionSolid("InnerLayerShield", InnerLayerShieldBox, ShieldHoleSolid, 0, G4ThreeVector(0,0,0));
	  InnerLayerShieldLog = new G4LogicalVolume(InnerLayerShieldSol, hbarDetConst::GetMaterial("Mu-Metal"), "InnerLayerShield");
	  InnerLayerShieldLog->SetVisAttributes(greycolor);
	  InnerLayerShieldPhys = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, MWCavityCenter), "InnerLayerShield", InnerLayerShieldLog, WorldPhy, false, 0);
	}
  if(UseCavityMiddleShielding)
	{
	  // Middle Shielding
	  MiddleInnerBox = new G4Box("MiddleInnerBox", (MiddleLayerShieldLength-ShieldingThickness)*0.5, (MiddleLayerShieldLength-ShieldingThickness)*0.5,
								 (MiddleLayerShieldHight-ShieldingThickness)*0.5);
	  MiddleOuterBox = new G4Box("MiddleOuterBox", (MiddleLayerShieldLength+ShieldingThickness)*0.5, (MiddleLayerShieldLength+ShieldingThickness)*0.5,
								 (MiddleLayerShieldHight+ShieldingThickness)*0.5);

	  MiddleLayerShieldBox = new G4SubtractionSolid("MiddleLayerShieldBox", MiddleOuterBox, MiddleInnerBox, 0, G4ThreeVector(0,0,0));
	  ShieldHoleSolid = new G4Tubs("ShieldHole", 0.0, ShieldHoleDiam*0.5, MiddleLayerShieldLength, 0.0, 2.0*M_PI);
	  MiddleLayerShieldSol = new G4SubtractionSolid("MiddleLayerShield", MiddleLayerShieldBox, ShieldHoleSolid, 0, G4ThreeVector(0,0,0));
	  MiddleLayerShieldLog = new G4LogicalVolume(MiddleLayerShieldSol, hbarDetConst::GetMaterial("Mu-Metal"), "MiddleLayerShield");
	  MiddleLayerShieldLog->SetVisAttributes(greycolor);
	  MiddleLayerShieldPhys = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, MWCavityCenter), "MiddleLayerShield", MiddleLayerShieldLog, WorldPhy, false, 0);
	}
  // Strip resonators in the microwave cavity
  if ((MWCavityResThick > 0.0) && (MWCavityResSep > 0.0) && (MWCavityResWidth > 0.0))
	{
	  Res1Sol = new G4Box("Resonator1", MWCavityResWidth/2.0, MWCavityResThick/2.0, MWCavityLength/2.0-1*mm);
	  Res1Log = new G4LogicalVolume(Res1Sol, hbarDetConst::GetMaterial("Sts"), "Resonator1");

	  Res1Phy = new G4PVPlacement(0, G4ThreeVector(0.0, (MWCavityResSep/2.0 + MWCavityResThick/2.0-0.2*mm), MWCavityCenter),
								  "Resonator1", Res1Log, WorldPhy, false, 0);

	  Res2Sol = new G4Box("Resonator2", MWCavityResWidth/2.0, MWCavityResThick/2.0, MWCavityLength/2.0-1*mm);
	  Res2Log = new G4LogicalVolume(Res2Sol, hbarDetConst::GetMaterial("Sts"), "Resonator2");

	  Res2Phy = new G4PVPlacement(0, G4ThreeVector(0.0, -(MWCavityResSep/2.0 + MWCavityResThick/2.0-0.2*mm), MWCavityCenter),
								  "Resonator2", Res2Log, WorldPhy, false, 0);
	}
  CavityWingsSol = new G4Box("CavityWings", CavityWingsLength*0.5, CavityWingsThickness*0.5, CavityWingsWidth*0.5);
  CavityWingsLog = new G4LogicalVolume(CavityWingsSol, hbarDetConst::GetMaterial("Sts"), "CavityWings");
  CavityWingsLog->SetVisAttributes(new G4VisAttributes(G4Colour(0.0, 1.0, 0.0)));

  CavityWingsPhys1 = new G4PVPlacement(0, G4ThreeVector(0.5*MWCavityInnerDiam-CavityWingsLength*0.5-0.1*mm, 0.0, MWCavityCenter),
									   "CavityWings1", CavityWingsLog, WorldPhy, false, 0);

  CavityWingsPhys2 = new G4PVPlacement(0, G4ThreeVector(-0.5*MWCavityInnerDiam+CavityWingsLength*0.5+0.1*mm, 0.0, MWCavityCenter),
									   "CavityWings2", CavityWingsLog, WorldPhy, false, 0);
}
