#include "hbarCavityMess.hh"

hbarCavityMess::hbarCavityMess(hbarCavityConst * _hbarCavity)
  :hbarCavity(_hbarCavity)
{
  

  CavityLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCavityLength",this);
  CavityLengthCmd->SetGuidance("Set length of the microwave cavity (m)");
  CavityLengthCmd->SetParameterName("Size",false,false);
  CavityLengthCmd->SetDefaultUnit("m");
  CavityLengthCmd->SetRange("Size>=0.");
  CavityLengthCmd->AvailableForStates(G4State_Idle);

  CavityInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCavityInnerDiameter",this);
  CavityInnerDiamCmd->SetGuidance("Set inner diameter of the microwave cavity (m)");
  CavityInnerDiamCmd->SetParameterName("Size",false,false);
  CavityInnerDiamCmd->SetDefaultUnit("m");
  CavityInnerDiamCmd->SetRange("Size>=0.");
  CavityInnerDiamCmd->AvailableForStates(G4State_Idle);

  CavityOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCavityOuterDiameter",this);
  CavityOuterDiamCmd->SetGuidance("Set outer diameter of the microwave cavity (m)");
  CavityOuterDiamCmd->SetParameterName("Size",false,false);
  CavityOuterDiamCmd->SetDefaultUnit("m");
  CavityOuterDiamCmd->SetRange("Size>=0.");
  CavityOuterDiamCmd->AvailableForStates(G4State_Idle);

  CavityCenterCmd = new G4UIcmdWithADoubleAndUnit("/setup/setCavityCenter",this);
  CavityCenterCmd->SetGuidance("Set center position the microwave cavity (m)");
  CavityCenterCmd->SetParameterName("Size",false,false);
  CavityCenterCmd->SetDefaultUnit("m");
//  CavityCenterCmd->SetRange("Size>=0.");
  CavityCenterCmd->AvailableForStates(G4State_Idle);


  CavityResThickCmd = new G4UIcmdWithADoubleAndUnit("/setup/setResonatorThickness",this);
  CavityResThickCmd->SetGuidance("Set thickness of the resonator strip line plates (m)");
  CavityResThickCmd->SetParameterName("Size",false,false);
  CavityResThickCmd->SetDefaultUnit("m");
  CavityResThickCmd->SetRange("Size>=0.");
  CavityResThickCmd->AvailableForStates(G4State_Idle);

  CavityResSepCmd = new G4UIcmdWithADoubleAndUnit("/setup/setResonatorSeparation",this);
  CavityResSepCmd->SetGuidance("Set distance between the surfaces of the two resonator strip line plates (m)");
  CavityResSepCmd->SetParameterName("Size",false,false);
  CavityResSepCmd->SetDefaultUnit("m");
  CavityResSepCmd->SetRange("Size>=0.");
  CavityResSepCmd->AvailableForStates(G4State_Idle);

  CavityResWidthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setResonatorWidth",this);
  CavityResWidthCmd->SetGuidance("Set width of the two resonator strip line plates (m)");
  CavityResWidthCmd->SetParameterName("Size",false,false);
  CavityResWidthCmd->SetDefaultUnit("m");
  CavityResWidthCmd->SetRange("Size>=0.");
  CavityResWidthCmd->AvailableForStates(G4State_Idle);

  CavityFieldFileCmd = new G4UIcmdWithAString("/setup/setCavityFieldFile",this);
  CavityFieldFileCmd->SetGuidance("Set Cavity Fieldmap");
  CavityFieldFileCmd->SetParameterName("CavityFieldFile",false,false);
  CavityFieldFileCmd->AvailableForStates(G4State_Idle);
  
  UseCavityFieldMapCmd = new G4UIcmdWithABool("/setup/setUseCavityFieldmap",this);
  UseCavityFieldMapCmd->SetGuidance("Use Fieldmap for Cavity");
  UseCavityFieldMapCmd->SetParameterName("useflag",true,false);
  UseCavityFieldMapCmd->SetDefaultValue(true);
  UseCavityFieldMapCmd->AvailableForStates(G4State_Idle);
  
  HelmholtzCoilsInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setHelmholtzCoilsInnerDiam",this);
  HelmholtzCoilsInnerDiamCmd->SetGuidance("Set inner diameter of the helmholtzcoils");
  HelmholtzCoilsInnerDiamCmd->SetParameterName("Size",false,false);
  HelmholtzCoilsInnerDiamCmd->SetDefaultUnit("m");
  HelmholtzCoilsInnerDiamCmd->SetRange("Size>=0.");
  HelmholtzCoilsInnerDiamCmd->AvailableForStates(G4State_Idle);
  
  HelmholtzCoilsOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setHelmholtzCoilsOuterDiam",this);
  HelmholtzCoilsOuterDiamCmd->SetGuidance("Set Outer diameter of the helmholtzcoils");
  HelmholtzCoilsOuterDiamCmd->SetParameterName("Size",false,false);
  HelmholtzCoilsOuterDiamCmd->SetDefaultUnit("m");
  HelmholtzCoilsOuterDiamCmd->SetRange("Size>=0.");
  HelmholtzCoilsOuterDiamCmd->AvailableForStates(G4State_Idle);

  HelmholtzCoilThicknessCmd = new G4UIcmdWithADoubleAndUnit("/setup/setHelmholtzCoilThickness",this);
  HelmholtzCoilThicknessCmd->SetGuidance("Set thickness of the helmholtzcoils");
  HelmholtzCoilThicknessCmd->SetParameterName("Size",false,false);
  HelmholtzCoilThicknessCmd->SetDefaultUnit("m");
  HelmholtzCoilThicknessCmd->SetRange("Size>=0.");
  HelmholtzCoilThicknessCmd->AvailableForStates(G4State_Idle);

  HelmholtzCoilDistanceCmd = new G4UIcmdWithADoubleAndUnit("/setup/setHelmholtzCoilDistance",this);
  HelmholtzCoilDistanceCmd->SetGuidance("Set distance of the helmholtzcoils");
  HelmholtzCoilDistanceCmd->SetParameterName("Size",false,false);
  HelmholtzCoilDistanceCmd->SetDefaultUnit("m");
  HelmholtzCoilDistanceCmd->SetRange("Size>=0.");
  HelmholtzCoilDistanceCmd->AvailableForStates(G4State_Idle);

  SupportRingHHOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSupportRingHHOuterDiam",this);
  SupportRingHHOuterDiamCmd->SetGuidance("Set outer diameter of sopportrings of helmholtzcoils");
  SupportRingHHOuterDiamCmd->SetParameterName("Size",false,false);
  SupportRingHHOuterDiamCmd->SetDefaultUnit("m");
  SupportRingHHOuterDiamCmd->SetRange("Size>=0.");
  SupportRingHHOuterDiamCmd->AvailableForStates(G4State_Idle);

  SupportRingHHOuterThickCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSupportRingHHOuterThick",this);
  SupportRingHHOuterThickCmd->SetGuidance("Set thickness of sopportrings of helmholtzcoils");
  SupportRingHHOuterThickCmd->SetParameterName("Size",false,false);
  SupportRingHHOuterThickCmd->SetDefaultUnit("m");
  SupportRingHHOuterThickCmd->SetRange("Size>=0.");
  SupportRingHHOuterThickCmd->AvailableForStates(G4State_Idle);


  ShieldingThicknessCmd = new G4UIcmdWithADoubleAndUnit("/setup/setShieldingThickness",this);
  ShieldingThicknessCmd->SetGuidance("Set thickness of cavity shielding");
  ShieldingThicknessCmd->SetParameterName("Size",false,false);
  ShieldingThicknessCmd->SetDefaultUnit("m");
  ShieldingThicknessCmd->SetRange("Size>=0.");
  ShieldingThicknessCmd->AvailableForStates(G4State_Idle);

  InnerLayerShieldLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setInnerLayerShieldLength",this);
  InnerLayerShieldLengthCmd->SetGuidance("Set length of Inner layer of cavity shielding");
  InnerLayerShieldLengthCmd->SetParameterName("Size",false,false);
  InnerLayerShieldLengthCmd->SetDefaultUnit("m");
  InnerLayerShieldLengthCmd->SetRange("Size>=0.");
  InnerLayerShieldLengthCmd->AvailableForStates(G4State_Idle);

  InnerLayerShieldHightCmd = new G4UIcmdWithADoubleAndUnit("/setup/setInnerLayerShieldHight",this);
  InnerLayerShieldHightCmd->SetGuidance("Set Hight of Inner layer of cavity shielding");
  InnerLayerShieldHightCmd->SetParameterName("Size",false,false);
  InnerLayerShieldHightCmd->SetDefaultUnit("m");
  InnerLayerShieldHightCmd->SetRange("Size>=0.");
  InnerLayerShieldHightCmd->AvailableForStates(G4State_Idle);

  MiddleLayerShieldLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setMiddleLayerShieldLength",this);
  MiddleLayerShieldLengthCmd->SetGuidance("Set length of Middle layer of cavity shielding");
  MiddleLayerShieldLengthCmd->SetParameterName("Size",false,false);
  MiddleLayerShieldLengthCmd->SetDefaultUnit("m");
  MiddleLayerShieldLengthCmd->SetRange("Size>=0.");
  MiddleLayerShieldLengthCmd->AvailableForStates(G4State_Idle);

  MiddleLayerShieldHightCmd = new G4UIcmdWithADoubleAndUnit("/setup/setMiddleLayerShieldHight",this);
  MiddleLayerShieldHightCmd->SetGuidance("Set Hight of Middle layer of cavity shielding");
  MiddleLayerShieldHightCmd->SetParameterName("Size",false,false);
  MiddleLayerShieldHightCmd->SetDefaultUnit("m");
  MiddleLayerShieldHightCmd->SetRange("Size>=0.");
  MiddleLayerShieldHightCmd->AvailableForStates(G4State_Idle);

  ShieldHoleDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setShieldHoleDiam",this);
  ShieldHoleDiamCmd->SetGuidance("Set diameter of hole in cavity shielding");
  ShieldHoleDiamCmd->SetParameterName("Size",false,false);
  ShieldHoleDiamCmd->SetDefaultUnit("m");
  ShieldHoleDiamCmd->SetRange("Size>=0.");
  ShieldHoleDiamCmd->AvailableForStates(G4State_Idle);
  
  UseCavityInnerShieldingCmd = new G4UIcmdWithABool("/setup/setUseCavityInnerShielding",this);
  UseCavityInnerShieldingCmd->SetGuidance("Use Inner Shielding for Cavity");
  UseCavityInnerShieldingCmd->SetParameterName("useflag",true,false);
  UseCavityInnerShieldingCmd->SetDefaultValue(true);
  UseCavityInnerShieldingCmd->AvailableForStates(G4State_Idle);
  
  UseCavityMiddleShieldingCmd = new G4UIcmdWithABool("/setup/setUseCavityMiddleShielding",this);
  UseCavityMiddleShieldingCmd->SetGuidance("Use Middle Shielding for Cavity");
  UseCavityMiddleShieldingCmd->SetParameterName("useflag",true,false);
  UseCavityMiddleShieldingCmd->SetDefaultValue(true);
  UseCavityMiddleShieldingCmd->AvailableForStates(G4State_Idle);
  
  UseHelmholtzCoilsCmd = new G4UIcmdWithABool("/setup/setUseHelmholtzCoils",this);
  UseHelmholtzCoilsCmd->SetGuidance("Use Helmholtz Coils");
  UseHelmholtzCoilsCmd->SetParameterName("useflag",true,false);
  UseHelmholtzCoilsCmd->SetDefaultValue(true);
  UseHelmholtzCoilsCmd->AvailableForStates(G4State_Idle);
  
  UseSupportRingsHHCmd = new G4UIcmdWithABool("/setup/setUseSupportRingsHH",this);
  UseSupportRingsHHCmd->SetGuidance("Use Supportrings of Helmholtz Coils");
  UseSupportRingsHHCmd->SetParameterName("useflag",true,false);
  UseSupportRingsHHCmd->SetDefaultValue(true);
  UseSupportRingsHHCmd->AvailableForStates(G4State_Idle);

}


hbarCavityMess::~hbarCavityMess()
{
  delete CavityFieldFileCmd;
  delete UseCavityFieldMapCmd;
  delete HelmholtzCoilsInnerDiamCmd;
  delete HelmholtzCoilsOuterDiamCmd;
  delete SupportRingHHOuterDiamCmd;
  delete SupportRingHHOuterThickCmd;
  delete ShieldingThicknessCmd;
  delete InnerLayerShieldLengthCmd;
  delete InnerLayerShieldHightCmd;
  delete MiddleLayerShieldLengthCmd;
  delete MiddleLayerShieldHightCmd;
  delete ShieldHoleDiamCmd;
  delete UseCavityInnerShieldingCmd;
  delete UseCavityMiddleShieldingCmd;
  delete UseHelmholtzCoilsCmd;
  delete UseSupportRingsHHCmd;
  delete CavityLengthCmd; 
  delete CavityInnerDiamCmd;  
  delete CavityOuterDiamCmd;
  delete CavityCenterCmd;
  delete CavityResThickCmd; 
  delete CavityResSepCmd; 
  delete CavityResWidthCmd;
}


void hbarCavityMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if (command == CavityFieldFileCmd)
   {
	 hbarCavity->SetCavityFieldFile(newValue);
   }
  
    
  if (command == UseCavityFieldMapCmd)
   {
	 hbarCavity->SetUseCavityFieldMap(UseCavityFieldMapCmd->GetNewBoolValue(newValue));
   }
   

  if(command == HelmholtzCoilsInnerDiamCmd)
   {
	 hbarCavity->SetHelmholtzCoilsInnerDiam(HelmholtzCoilsInnerDiamCmd->GetNewDoubleValue(newValue));
   }
   
   if(command == HelmholtzCoilsOuterDiamCmd)
   {
	 hbarCavity->SetHelmholtzCoilsOuterDiam(HelmholtzCoilsOuterDiamCmd->GetNewDoubleValue(newValue));
   }
   
   if(command == HelmholtzCoilThicknessCmd)
   {
	 hbarCavity->SetHelmholtzCoilThickness(HelmholtzCoilThicknessCmd->GetNewDoubleValue(newValue));
   }
   
   if(command == HelmholtzCoilDistanceCmd)
   {
	 hbarCavity->SetHelmholtzCoilDistance(HelmholtzCoilDistanceCmd->GetNewDoubleValue(newValue));
   }
   
   if(command == SupportRingHHOuterDiamCmd)
   {
	 hbarCavity->SetSupportRingHHOuterDiam(SupportRingHHOuterDiamCmd->GetNewDoubleValue(newValue));
   }
   
   if(command == SupportRingHHOuterThickCmd)
   {
	 hbarCavity->SetSupportRingHHOuterThick(SupportRingHHOuterThickCmd->GetNewDoubleValue(newValue));
   }
   
   
    if(command == ShieldingThicknessCmd)
   {
	 hbarCavity->SetShieldingThickness(ShieldingThicknessCmd->GetNewDoubleValue(newValue));
   }
   
    if(command == InnerLayerShieldLengthCmd)
   {
	 hbarCavity->SetInnerLayerShieldLength(InnerLayerShieldLengthCmd->GetNewDoubleValue(newValue));
   }
   
    if(command == InnerLayerShieldHightCmd)
   {
	 hbarCavity->SetInnerLayerShieldHight(InnerLayerShieldHightCmd->GetNewDoubleValue(newValue));
   }
   
    if(command == MiddleLayerShieldLengthCmd)
   {
	 hbarCavity->SetMiddleLayerShieldLength(MiddleLayerShieldLengthCmd->GetNewDoubleValue(newValue));
   }
   
    if(command == MiddleLayerShieldHightCmd)
   {
	 hbarCavity->SetMiddleLayerShieldHight(MiddleLayerShieldHightCmd->GetNewDoubleValue(newValue));
   }
   
    if(command == ShieldHoleDiamCmd)
   {
	 hbarCavity->SetShieldHoleDiam(ShieldHoleDiamCmd->GetNewDoubleValue(newValue));
   }
   
	if(command == UseCavityInnerShieldingCmd)
	  {
		hbarCavity->SetUseCavityInnerShielding(UseCavityInnerShieldingCmd->GetNewBoolValue(newValue));
	  }
   
   if(command == UseCavityMiddleShieldingCmd)
   {
	 hbarCavity->SetUseCavityMiddleShielding(UseCavityMiddleShieldingCmd->GetNewBoolValue(newValue));
   }  
   
   if(command == UseHelmholtzCoilsCmd)
   {
	 hbarCavity->SetUseHelmholtzCoils(UseHelmholtzCoilsCmd->GetNewBoolValue(newValue));
   }  
   
   if(command == UseSupportRingsHHCmd)
   {
	 hbarCavity->SetUseSupportRingsHH(UseSupportRingsHHCmd->GetNewBoolValue(newValue));
   }  
   
 if( command == CavityLengthCmd )
   { 
	 hbarCavity->SetMWCavityLength(CavityLengthCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityInnerDiamCmd )
   { 
	 hbarCavity->SetMWCavityInnerDiameter(CavityInnerDiamCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityOuterDiamCmd )
   { 
	 hbarCavity->SetMWCavityOuterDiameter(CavityOuterDiamCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityCenterCmd )
   { 
	 hbarCavity->SetMWCavityCenter(CavityCenterCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityResThickCmd )
   { 
	 hbarCavity->SetMWCavityResonatorThickness(CavityResThickCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityResSepCmd )
   { 
	 hbarCavity->SetMWCavityResonatorSeparation(CavityResSepCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == CavityResWidthCmd )
   { 
	 hbarCavity->SetMWCavityResonatorWidth(CavityResWidthCmd->GetNewDoubleValue(newValue));
   }
 
}
