#include "hbarHydrogenDecayAtRest.hh"

hbarHydrogenDecayAtRest::hbarHydrogenDecayAtRest() : 
  hbarHydrogenLikeDecayAtRest("HydrogenDecayAtRest")
{
  
}
//! Destructor frees memory after particle is killed. Currently does nothing.
hbarHydrogenDecayAtRest::~hbarHydrogenDecayAtRest()
{

}
