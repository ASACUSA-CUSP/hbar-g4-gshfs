#include "hbarPhysicsListMess.hh"
#include "hbarPhysicsList.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarPhysicsListMess::hbarPhysicsListMess(hbarPhysicsList* plist)
:physList(plist)
{
  // physics lists
  ////////////////
  setElectromagneticCmd = new G4UIcmdWithAString("/process/setElectromagneticList",this);
  setElectromagneticCmd->SetGuidance("Set the physics list for electomagnetic processes");
  setElectromagneticCmd->SetGuidance("emstandard_opt4 - most accurate processes");
  setElectromagneticCmd->SetGuidance("livermore - livermore processes");
  setElectromagneticCmd->SetGuidance("livermore_polarized - livermore processes with polarized photon models");
  setElectromagneticCmd->SetGuidance("penelope - penelope 2008 processes");
  setElectromagneticCmd->SetGuidance("dnaphysics - Geant4-DNA physics models ");
  setElectromagneticCmd->SetParameterName("value",true);
  setElectromagneticCmd->SetDefaultValue("emstandard_opt4");
  setElectromagneticCmd->SetCandidates("emstandard_opt4 livermore livermore_polarized penelope dnaphysics");
  setElectromagneticCmd->AvailableForStates(G4State_Idle);

  setHadronicCmd = new G4UIcmdWithAString("/process/setHadronicList",this);
  setHadronicCmd->SetGuidance("Set the physics list for ineleastic hadronic processes");
  setHadronicCmd->SetGuidance("FTFP_BERT_TRV - Fritof with Bertini cascade for particles below 3 GeV");
  setHadronicCmd->SetGuidance("FTFP_BERT - Fritof with Bertini cascade for particles below 5 GeV");
  setHadronicCmd->SetGuidance("FTF_BIC - Fritof with binary cascade");
  setHadronicCmd->SetGuidance("QGSP_FTFP_BERT - quark gluon string model with fritof and  Bertini cascade - replaces old LEP list");
  setHadronicCmd->SetGuidance("QGSP_BERT - quark gluon string model with Bertini cascade");
  setHadronicCmd->SetGuidance("QGSP_BIC - quark gluon string model with binary cascade");
  setHadronicCmd->SetParameterName("value",true);
  setHadronicCmd->SetDefaultValue("FTFP_BERT_TRV");
  setHadronicCmd->SetCandidates("FTFP_BERT_TRV FTFP_BERT FTF_BIC QGSP_FTFP_BERT QGSP_BERT QGSP_BIC");
  setHadronicCmd->AvailableForStates(G4State_Idle);
  
  setUseSMIPbarAnnihilationCmd = new G4UIcmdWithAString("/process/setUseSMIPbarAnnihilation",this);
  setUseSMIPbarAnnihilationCmd->SetGuidance("Use the SMI code for pbar annihilation or use the Geant4 code (G4AntiProtonAbsorptionFritiof)");
  setUseSMIPbarAnnihilationCmd->SetParameterName("value",true);
  setUseSMIPbarAnnihilationCmd->SetDefaultValue("true");
  setUseSMIPbarAnnihilationCmd->SetCandidates("true false");
  setUseSMIPbarAnnihilationCmd->AvailableForStates(G4State_Idle);

  SetConstructOptPhotonsCmd = new G4UIcmdWithAString("/process/SetConstructOptPhotons",this);
  SetConstructOptPhotonsCmd->SetGuidance("Activate optical photons (for scintillator bars of Hodor)");
  SetConstructOptPhotonsCmd->SetParameterName("value",true);
  SetConstructOptPhotonsCmd->SetDefaultValue("on");
  SetConstructOptPhotonsCmd->SetCandidates("on off");
  SetConstructOptPhotonsCmd->AvailableForStates(G4State_Idle);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarPhysicsListMess::~hbarPhysicsListMess()
{
  delete setElectromagneticCmd;
  delete setHadronicCmd;
  delete setUseSMIPbarAnnihilationCmd;
  delete SetConstructOptPhotonsCmd;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void hbarPhysicsListMess::SetNewValue(G4UIcommand* command,
				      G4String newValue)
{  
    
  if( command == SetConstructOptPhotonsCmd )
   { physList->SetConstructOptPhotons(newValue);} 
  if( command == setElectromagneticCmd )
   { physList->SetElectromagneticList(newValue);}
  if( command == setHadronicCmd )
   { physList->SetHadronicList(newValue);}
  if( command == setUseSMIPbarAnnihilationCmd )
   { physList->SetUseSMICode(newValue);}


 

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
