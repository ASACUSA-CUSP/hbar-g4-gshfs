#include "hbarCPTConst.hh"

hbarCPTConst::hbarCPTConst()
  :useHbarCounter(false), 
  fSolidFoil (0), fLogicFoil (0), fPhysiFoil (0),
  fSolidTPix (0), fLogicTPix (0), fPhysiTPix (0),
  fSolidElectrode (0), fLogicElectrode (0), fPhysiElectrode (0)
{
  // general scintillator eg. the normalization counters
  Scintipt = -1;
  for (G4int i = 0; i < maxScN; i++)
	{
	  ScintiPos[i] = G4ThreeVector();
	  ScintiRot[i] = G4RotationMatrix();
	  useScinti[i] = false;
	  ScintiCoin[i] = 1;
	  NumOfLeadLayers[i] = 1;
	  LeadThick[i] = 0.0*mm;
	  NumOfScintiSides[i] = 4;
	}

  // The veto counter
  Vetopt = -1;
  for (G4int i = 0; i < maxVeto; i++)
	{
	  VetoPos[i] = G4ThreeVector();
	  VetoRot[i] = G4RotationMatrix();
	  useVeto[i] = false;
	  VetoOutRad[i] = 10*cm;
	  VetoInRad[i] = 2*cm;
	  VetoLength[i] = 2*cm;
	  Hododiv[i] = 4;
	}

  // The hodoscope
  Hodoscopept = -1;
  for (G4int i = 0; i < maxHodoscope; i++)
	{
	  HodoscopePos[i] = G4ThreeVector();
	  HodoscopeRot[i] = G4RotationMatrix();
	  useHodoscope[i] = false;
	  HodoscopeOutRad[i] = 10*cm;
	  HodoscopeInRad[i] = 2*cm;
	  HodoscopeLength[i] = 2*cm;
	  vetodiv[i] = 4;
	}


	//std::cout << "********************* " << maxHodoscope << endl;

  // the central hbar detector
  HbarPos = G4ThreeVector();
  HbarRot = G4RotationMatrix();
  HbarXseg = 4;
  HbarYseg = 4;
  HbarDetX = 3*cm;
  HbarDetY = 3*cm;
  HbarDetZ = 1*cm;
  CPTMessenger = new hbarCPTMess(this);

  MAPMT_SupplyVoltage = 0.0;
  PMTGain = 0.0;
  
  FoilMat = "CarbonCoatmaterial";

//useHodoscope[0] = 1;
//useHodoscope[1] = 1;

}


hbarCPTConst::~hbarCPTConst()
{
  delete CPTMessenger;
}


//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
void hbarCPTConst::MakeCPT(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD)
{

 // General rectangular scintillator
  for (G4int i = 0; i < maxScN; i++)
	{ // START scintillator loop
    if (useScinti[i])
	  { // START use if
		// Scintillators eg the normalization counters
		G4String detname = "Scinti" + numtostr(i+1);
		if (NumOfScintiSides[i] == 4)
		  {
			ScintiSol[i] = new G4Trd(detname,
									 ScintiThick[i]/2.0, ScintiThick[i]/2.0,
									 ScintiWidth[i]/2.0, ScintiWidthL[i]/2.0,
									 ScintiLength[i]/2.0);
		  }
		else if ((NumOfScintiSides[i] == 3) || (NumOfScintiSides[i] > 4))
		  {
			G4double sc_r_0 [] = {0.0, 0.0};
			G4double sc_r [] = {ScintiWidth[i]/2.0, ScintiWidth[i]/2.0};
			G4double sc_z [] = {-ScintiThick[i]/2.0, ScintiThick[i]/2.0};
			ScintiSol[i] = new G4Polyhedra(detname, 0.0, 2.0*M_PI,
										   NumOfScintiSides[i], 2,
										   sc_z, sc_r_0, sc_r);
		  }
		ScintiLog[i] = new G4LogicalVolume(ScintiSol[i], hbarDetConst::GetMaterial("Scintillator"), detname);
		ScintiPhy[i] = new G4PVPlacement(G4Transform3D(ScintiRot[i], ScintiPos[i]),
										 detname, ScintiLog[i], WorldPhy, false, 0);
		G4VisAttributes* bluecolor = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
		//bluecolor->SetForceWireframe(true);
		ScintiLog[i]->SetVisAttributes(bluecolor);
		// Define as sensitive detector
		ScintiLog[i]->SetSensitiveDetector(scintiSD);

		// Lead layers
		if ((LeadThick[i] > 0.0) && (NumOfLeadLayers[i] > 0))
		  {
			G4String leadname = "Lead" + numtostr(i+1);
			if (NumOfScintiSides[i] == 4)
			  {
				LeadSol[i] = new G4Trd(leadname,
									   LeadThick[i]/2.0, LeadThick[i]/2.0,
									   ScintiWidth[i]/2.0, ScintiWidthL[i]/2.0,
									   ScintiLength[i]/2.0);
			  }
			else if ((NumOfScintiSides[i] == 3) || (NumOfScintiSides[i] > 4))
			  {
				G4double sc_r_0 [] = {0.0, 0.0};
				G4double sc_r [] = {ScintiWidth[i]/2.0, ScintiWidth[i]/2.0};
				G4double sc_z [] = {-LeadThick[i]/2.0, LeadThick[i]/2.0};
				LeadSol[i] = new G4Polyhedra(leadname, 0.0, 2.0*M_PI,
											 NumOfScintiSides[i], 2,
											 sc_z, sc_r_0, sc_r);
			  }
			LeadLog[i] = new G4LogicalVolume(LeadSol[i], hbarDetConst::GetMaterial("Lead"), leadname);
			LeadParam[i] = new hbarDetParam(ScintiThick[i], NumOfLeadLayers[i], LeadThick[i]);
			if (LeadThick[i]*NumOfLeadLayers[i] > ScintiThick[i])
			  {
				G4cout << "***** WARNING! Lead layers are too thick! *****" << G4endl;
			  }
			LeadPhy[i] = new G4PVParameterised(leadname, LeadLog[i], ScintiPhy[i],
											   kUndefined, NumOfLeadLayers[i], LeadParam[i]);
		  }
		else
		  {
			G4cout << "***** WARNING! Lead layers are undefinied! *****" << G4endl;
		  }
	  }
	} // END scintillator loop

  // Veto Counter
  for (G4int i = 0; i < maxVeto; i++)
	{
	  if (useVeto[i])
		{
		  G4String tmp = "Veto" + numtostr(i+1);
		  G4String detname = tmp + "_det";
		  G4String detnamel = detname + "L";
		  G4String detnamep = detname + "P";
		  G4String detnamediv = detname + "_div";
		  G4String detnamephys = tmp;

		  G4double tube_dPhi = 2.* M_PI * rad;
		  VetoBaseSol[i] = new G4Tubs(detname,VetoInRad[i],
									  VetoOutRad[i],VetoLength[i]/2.,
									  0.,tube_dPhi);
		  VetoBaseLog[i] =
			new G4LogicalVolume(VetoBaseSol[i], hbarDetConst::GetMaterial("Scintillator"), detnamel, 0, 0, 0);
		  VetoBasePhy[i] =
			new G4PVPlacement(G4Transform3D(VetoRot[i], VetoPos[i]),
							  detname, VetoBaseLog[i], WorldPhy, false, 0);

		  G4double divided_tube_dPhi = tube_dPhi/vetodiv[i];
		  VetoSol[i] = new G4Tubs(detnamediv, VetoInRad[i],VetoOutRad[i],
								  VetoLength[i]/2,-divided_tube_dPhi/2.,
								  divided_tube_dPhi);
		  VetoLog[i] = new G4LogicalVolume(VetoSol[i],hbarDetConst::GetMaterial("Scintillator"),
										   detnamediv+"L",0,0,0);
		  VetoPhy[i] = new G4PVReplica(detnamephys, VetoLog[i],
									   VetoBaseLog[i], kPhi, vetodiv[i], divided_tube_dPhi);

		  G4VisAttributes* bluecolor = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
		  VetoLog[i]->SetVisAttributes(bluecolor);
		  VetoBaseLog[i]->SetVisAttributes(bluecolor);
		  VetoLog[i]->SetSensitiveDetector(scintiSD);
		}
	}
	
    if(hbarDetConst::GetUseHbar2014Det()) {
	    /*if(UseGDMLHodor==true) {
	        LoadGDMLHodoscope(WorldPhy, scintiSD);
	    }
	    else BuildDetailedHodoscope(WorldPhy, scintiSD); // not very detailed geometry of the octagonal hodoscope
	    	    */
	    // TODO BGO
	}
	else if (hbarDetConst::GetUseTimepixSetup()) {

	    //std::cout << "################################################################################################################## in" << std::endl;

	    if(UseGDMLHodor==true) {
	        //LoadGDMLHodoscope(WorldPhy, scintiSD); // no fibre detector possible - causes overlaps with the loaded geometry	        
	    }	    
	    else {
		//std::cout << "################################################################################################################## in" << std::endl;
			
	        BuildDetailedHodoscope(WorldPhy, scintiSD); 
	        BuildFibreDetector(WorldPhy, scintiSD);
	    }
	    //std::cout << "################################################################################################################## in" << std::endl;
	    BuildTimePix(WorldPhy,scintiSD);
	}
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
void hbarCPTConst::BuildTimePix(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {
    /*    //TODO
        G4double FoilZPos = 3.0*cm;
        G4double FoilWidth = 0.001*mm;
	    tpxfoil = new G4Box("carbonfoil", 2.0*cm*0.5, 2.0*cm*0.5, FoilWidth); // this is the timepix carbon foil
     
        tpxfoilLog = new G4LogicalVolume(tpxfoil, hbarDetConst::GetMaterial("CarbonCoatmaterial"), "carbonfoil", 0, 0, 0);
       //G4double maxStep = 0.0001 *mm;
	   //G4UserLimits* stepLimit = new G4UserLimits(maxStep);
	   //tpxLog->SetUserLimits(stepLimit); 
       
       tpxfoilPhy = new G4PVPlacement(0, G4ThreeVector(0, 0, HodoscopePos[0].z() - FoilZPos - 0.5*FoilWidth ), "carbonfoil", tpxfoilLog, WorldPhy, false, 0);
       tpxfoilLog->SetSensitiveDetector(scintiSD);
       //tpxLog->SetVisAttributes(orangecolor);
       */
    ////////////////////////////////////////////////////
     G4RotationMatrix* TpxDummyRot = new G4RotationMatrix();
      // TPix
      
      //HodoscopePos[0] = {0.0,0.0,0.0};
      fSolidTPix = 0;
      fLogicTPix = 0;
      
      // default parameter values of the target foil
      fFoilThick  = 2.E-3 * mm;
      fFoilSizeXY = 2. * cm;
      G4double FoilZPos = -2.68*cm;
      //SetFoilMaterial ("G4_C");

      // default parameter values of the TimePix detector
      fTPixPosZ     = 1. * cm; // distance of foil and tpx
      fTPixThick    = 500.E-3 * mm;
      fTPixNumPix   = 256;
      fTPixPixelXY  = 55.E-3 * mm;       
      fTPixdxcross  = 0.0 * mm;
      fTPixdycross  = 0.0 * mm;
      //SetTPixMaterial ("G4_Si");
      
      
      /*HodoscopeVacuumSol = new G4Tubs("vacuum_hodoscope_sol",0.*mm, 50.*mm, 1.0*0.5*m, 0., 2.0*M_PI);
                                
      HodoscopeVacuumLog = new G4LogicalVolume(HodoscopeVacuumSol, hbarDetConst::GetMaterial("Vac"), "vacuum_hodoscope", 0, 0, 0);
   
      //G4double maxStep = 0.1 *mm;
	  //G4UserLimits* stepLimit = new G4UserLimits(maxStep);
	  //HodoscopeVacuumLog->SetUserLimits(stepLimit); 
      
      HodoscopeVacuumPhys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0,FoilZPos - fFoilThick - 1.0*0.5*m)),
                                             "vacuum_hodoscope_phys", HodoscopeVacuumLog, WorldPhy, false, 0); */
	 
      /*HodoscopeVacuumSol = new G4Tubs("vacuum_hodoscope_sol",0.*mm, 50.*mm, 1.0*0.5*m, 0., 2.0*M_PI);
                                
      HodoscopeVacuumLog = new G4LogicalVolume(HodoscopeVacuumSol, hbarDetConst::GetMaterial("Vac"), "vacuum_hodoscope", 0, 0, 0);
   
      //G4double maxStep = 0.1 *mm;
	  //G4UserLimits* stepLimit = new G4UserLimits(maxStep);
	  //HodoscopeVacuumLog->SetUserLimits(stepLimit); 
      
      HodoscopeVacuumPhys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0,FoilZPos + fTPixPosZ - 1.0*0.5*m)),
                                             "vacuum_hodoscope_phys", HodoscopeVacuumLog, WorldPhy, false, 0); */
	
      
      
      // default parameter values of the Electrode
      fElectrodeThick = 500.E-6 * mm;
      //SetElectrodeMaterial ("G4_Al");
      fElectrodeSizeX = fTPixNumPix*fTPixPixelXY;
      fElectrodeSizeY = fTPixNumPix*fTPixPixelXY;
      
        // Foil
        fSolidFoil = 0;
        fLogicFoil = 0;
        fPhysiFoil = 0;

        fSolidFoil = new G4Box ("Foil",fFoilSizeXY/2., fFoilSizeXY/2., fFoilThick/2.);
        
        std::cout << "------------------------------------------------------------------------------------------------" << FoilMat << std::endl;

        fLogicFoil = new G4LogicalVolume (fSolidFoil,hbarDetConst::GetMaterial(FoilMat), "Foil"); // CarbonCoatmaterial
        
        fLogicFoil->SetSensitiveDetector(scintiSD); 


       // G4UserLimits* stepLimit = new G4UserLimits();  //...for step size
       // G4double maxStep = 0.000001 *mm;   //added for the step size (Angela)
       // stepLimit->SetMaxAllowedStep(maxStep);
        //fLogicFoil->SetUserLimits(stepLimit);  //...for step size 


        //fPhysiFoil = new G4PVPlacement (0, G4ThreeVector(0, 0, FoilZPos - 0.5*fFoilThick ),"Foil", fLogicFoil, WorldPhy, false, 0);
        fPhysiFoil = new G4PVPlacement (0, G4ThreeVector(0, 0, FoilZPos ),"Foil", fLogicFoil, WorldPhy, false, 0);
        //cout << fPhysiFoil->G4PVPlacement::CheckOverlaps() << endl;
        // new G4PVPlacement(0, G4ThreeVector(0, 0, HodoscopePos[0].z() - FoilZPos - 0.5*FoilWidth ), "carbonfoil", tpxfoilLog, WorldPhy, false, 0);
      if (fPhysiTPix) delete[] fPhysiTPix;
      // 4 TPix chips
      fPhysiTPix = new G4PVPlacement*[4*fTPixNumPix*fTPixNumPix];

      fSolidTPix = new G4Box ("TPix",fTPixPixelXY/2., fTPixPixelXY/2., fTPixThick/2.);

      fLogicTPix = new G4LogicalVolume (fSolidTPix,hbarDetConst::GetMaterial("Silicon"),"TPix");
        
      fLogicTPix->SetSensitiveDetector(scintiSD);

      // place replica of TimePix pixel
      G4double xref, yref;
      G4int ChipNum=0, PixelNum=0;
      G4String PixelName;
      G4ThreeVector PixelPos;
      char numch[12];
      
      G4double pixel_Xwidth = 0;
      G4double pixel_Ywidth = 0;
      
      G4double pixel_Xpos = 0;
      G4double pixel_Ypos = 0;
      
      int counter_ = 0;
      
      int ch_nr_helper = 0;
      
      /*for (G4int chip=0; chip<4; chip++) {
       
        ChipNum = chip*pow(10,(G4int)(2*log10(fTPixNumPix)+1));
        
        // reference position of chip
        xref = ((chip % 2)-1)*(fTPixNumPix*fTPixPixelXY+fTPixdxcross)+fTPixdxcross;
        yref = (G4int)(chip/2-1)*(fTPixNumPix*fTPixPixelXY+fTPixdycross)+fTPixdycross;
        
        //std::cout << "chip: " << ChipNum << std::endl;
        
        //std::cout << "xref: " << xref << std::endl; 
        //std::cout << "yref: " << yref << std::endl;
        pixel_Xwidth = fTPixPixelXY;
        pixel_Ywidth = fTPixPixelXY;
        
        
        for (G4int ii=0; ii<fTPixNumPix; ii++) {
          for (G4int jj=0; jj<fTPixNumPix; jj++) {
            
            // volume name = TPixnmmm
            // n   = chip number
            // mmm = pixel within chip
            
            ch_nr_helper = ChipNum+ii*fTPixNumPix+jj;
            sprintf(numch,"%6.6i",ChipNum+ii*fTPixNumPix+jj);
            //std::cout << numch << std::endl;
            
                            
            if (chip == 0 && ii*fTPixNumPix+jj != 0) {
                if((ii*fTPixNumPix+jj + 1) % 256 == 0) {
                    pixel_Xwidth = fTPixPixelXY;
                    pixel_Ywidth = 2.0*fTPixPixelXY;
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                } 
                else if(ii*fTPixNumPix+jj > 65279) {
                    pixel_Xwidth = 2.0*fTPixPixelXY;
                    pixel_Ywidth = fTPixPixelXY;
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY;                    
                }       
            }            
            else if (chip == 1 ) {
                if((ii*fTPixNumPix+jj + 1) % 256 == 0) {
                    pixel_Xwidth = fTPixPixelXY;
                    pixel_Ywidth = 2.0*fTPixPixelXY;     
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY +fTPixdxcross*0.5;                                 
                }
                else if( ii*fTPixNumPix+jj < 255) {
                    pixel_Xwidth = 2.0*fTPixPixelXY;
                    pixel_Ywidth = fTPixPixelXY;      
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY;                                 
                }        
            }
            else if (chip == 2 && ii*fTPixNumPix+jj != 0) {
                if((ii*fTPixNumPix+jj) % 256 == 0 ) {
                    pixel_Xwidth = fTPixPixelXY;
                    pixel_Ywidth = 2.0*fTPixPixelXY;
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY ;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY+fTPixdxcross*0.5;                                       
                }
                else if (ii*fTPixNumPix+jj > 65279) {
                    pixel_Xwidth = 2.0*fTPixPixelXY;
                    pixel_Ywidth = fTPixPixelXY;   
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY;                                        
                }        
            }
            else if (chip == 3) {
                if((ii*fTPixNumPix+jj) % 256 == 0 ) {
                    pixel_Xwidth = fTPixPixelXY;
                    pixel_Ywidth = 2.0*fTPixPixelXY;   
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY ;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY+fTPixdxcross*0.5;                                    
                }   
                else if (ii*fTPixNumPix+jj < 255) {
                    pixel_Xwidth = 2.0*fTPixPixelXY;
                    pixel_Ywidth = fTPixPixelXY;    
                    pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                    pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY;                                    
                }     
            }
            else{
                // this is the case for the square pixels (not on the cross)
                // position vector:
                pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY;
                pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY;

            }
            
            // four pixel in the center:
            if(ch_nr_helper == 65535 || ch_nr_helper == 100255 || ch_nr_helper == 265279 || ch_nr_helper== 300000) {
                pixel_Xwidth = 2.0*fTPixPixelXY;
                pixel_Ywidth = 2.0*fTPixPixelXY;   
                pixel_Xpos = xref + (ii+0.5)*fTPixPixelXY +fTPixdxcross*0.5;
                pixel_Ypos = yref + (jj+0.5)*fTPixPixelXY +fTPixdxcross*0.5;                  
            }  
            
            PixelName = G4String("TPix")+G4String(numch);
            
            PixelPos  = G4ThreeVector(pixel_Xpos, pixel_Ypos, FoilZPos + fTPixPosZ +(fTPixThick)/2.);        
            
            fPhysiTPix[PixelNum] = new G4PVPlacement (TpxDummyRot,PixelPos,PixelName,fLogicTPix,WorldPhy,false,0, true);
            PixelNum++;
            
            
          }
        }
      }*/
      
        for (G4int chip=0; chip<4; chip++) {
       
        ChipNum = chip*pow(10,(G4int)(2*log10(fTPixNumPix)+1));
        
        // reference position of chip
        xref = ((chip % 2)-1)*(fTPixNumPix*fTPixPixelXY+fTPixdxcross)+fTPixdxcross;
        yref = (G4int)(chip/2-1)*(fTPixNumPix*fTPixPixelXY+fTPixdycross)+fTPixdycross;
        
        //std::cout << "chip: " << ChipNum << std::endl;
        
        //std::cout << "xref: " << xref << std::endl; 
        //std::cout << "yref: " << yref << std::endl;
        pixel_Xwidth = fTPixPixelXY;
        pixel_Ywidth = fTPixPixelXY;
        
        
        for (G4int ii=0; ii<fTPixNumPix; ii++) {
          for (G4int jj=0; jj<fTPixNumPix; jj++) {
            
            // volume name = TPixnmmm
            // n   = chip number
            // mmm = pixel within chip
            
            ch_nr_helper = ChipNum+ii*fTPixNumPix+jj;
            sprintf(numch,"%6.6i",ChipNum+ii*fTPixNumPix+jj);
            //std::cout << numch << std::endl;
           
            PixelName = G4String("TPix")+G4String(numch);
            
            PixelPos  = G4ThreeVector(xref + (ii+0.5)*fTPixPixelXY, yref + (jj+0.5)*fTPixPixelXY, FoilZPos + fTPixPosZ +(fTPixThick)/2.);        
            
            fPhysiTPix[PixelNum] = new G4PVPlacement (TpxDummyRot,PixelPos,PixelName,fLogicTPix,WorldPhy,false,0);
            PixelNum++;
            
            
          }
        }
      }
      
      //std::cout << "counter: "<<  counter_ << std::endl;   
      
      // TimePix has a layer of 500 nm Al on top of surface
      fElectrodeSizeX = 2*fTPixNumPix*fTPixPixelXY;
      fElectrodeSizeY = 2*fTPixNumPix*fTPixPixelXY;
      
      fSolidElectrode = 0;
      fLogicElectrode = 0;
      fPhysiElectrode = 0;

      fSolidElectrode = new G4Box ("Electrode",fElectrodeSizeX/2., fElectrodeSizeY/2., fElectrodeThick/2.);

      fLogicElectrode = new G4LogicalVolume (fSolidElectrode,hbarDetConst::GetMaterial("Aluminium"), "Electrode");

      fPhysiElectrode = new G4PVPlacement (0,G4ThreeVector (0.,0., FoilZPos + fTPixPosZ+ fTPixThick + fElectrodeThick/2.),"Electrode",fLogicElectrode,WorldPhy,false,0);
      
      //cout << fPhysiElectrode->G4PVPlacement::CheckOverlaps() << endl;
      
      
      /////////////////////////////////////////////////////////////////
      // Timepix Flange
      // starting from the downstream side:
      G4Tubs* TpxFlangeHelperSol1 = new G4Tubs("TpxFlangeHelperSol1",100.*mm*0.5, 155.*mm*0.5, 8.0*mm*0.5, 0., 2.0*M_PI);       
      G4Tubs* TpxFlangeHelperSol2 = new G4Tubs("TpxFlangeHelperSol2", 115*mm*0.5, 155.*mm*0.5, 10.0*mm*0.5, 0., 2.0*M_PI);      
      G4Tubs* TpxFlangeHelperSol3 = new G4Tubs("TpxFlangeHelperSol3", 115*mm*0.5, 123*mm*0.5, 46.0*mm*0.5, 0., 2.0*M_PI);      
      G4Tubs* TpxFlangeHelperSol4 = new G4Tubs("TpxFlangeHelperSol4", 0*mm, 155.0*mm*0.5, 23.0*mm*0.5, 0., 2.0*M_PI);
      
      G4UnionSolid* TpxFlangeHelperUnSol1 = new G4UnionSolid("TpxFlangeHelperUnSol1", TpxFlangeHelperSol1, TpxFlangeHelperSol2, 0, G4ThreeVector(0, 0,8.0*mm*0.5 + 10.0*mm*0.5));
      G4UnionSolid* TpxFlangeHelperUnSol2 = new G4UnionSolid("TpxFlangeHelperUnSol2", TpxFlangeHelperUnSol1, TpxFlangeHelperSol3, 0, G4ThreeVector(0, 0,8.0*mm*0.5 + 10.0*mm + 46.0*mm*0.5 ));      
      TpxFlangeSolid = new G4UnionSolid("TpxFlangeSolid", TpxFlangeHelperUnSol2, TpxFlangeHelperSol4, 0, G4ThreeVector(0, 0,8.0*mm*0.5 + 10.0*mm + 46.0*mm + 23.0*mm*0.5 ));
      
      TpxFlangeLog = new G4LogicalVolume(TpxFlangeSolid, hbarDetConst::GetMaterial("Sts"), "TimepixFlange", 0, 0, 0);
      TpxFlangePhy = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0, FoilZPos  - 8.0*mm )), //(8.0*mm+10.0*mm+46.0*mm+23.0*mm)*0.5)
                                        "TpxFlangePhys", TpxFlangeLog, WorldPhy, false, 0);
      
      /////////////////////////////////////////////////////////////////
      // Aperature/Blende 
      G4Tubs* TpxAperatureHelperSol1 = new G4Tubs("TpxAperatureHelperSol1",0.*mm, 100.*mm*0.5, 8.0*mm*0.5, 0., 2.0*M_PI); 
      // cut out this part for the whole:
      G4Box* TpxAperatureHelperSol2 = new G4Box ("TpxAperatureHelperSol2",48.0*mm*0.5, 48.0*mm*0.5, 8.0*mm);      
      TpxAperatureSol = new G4SubtractionSolid("TpxAperatureLog", TpxAperatureHelperSol1, TpxAperatureHelperSol2);
      TpxAperatureLog = new G4LogicalVolume(TpxAperatureSol, hbarDetConst::GetMaterial("Sts"), "TimepixAperature", 0, 0, 0);      
      TpxAperaturePhy = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0, FoilZPos + 0.5*fFoilThick + 8.0*mm*0.5 )), //(8.0*mm+10.0*mm+46.0*mm+23.0*mm)*0.5)
                                        "TpxAperaturePhys", TpxAperatureLog, WorldPhy, false, 0);
      // copper plate inside aperature
      G4Box* TpxCopperApHelperSol1 = new G4Box ("TpxCopperApHelperSol1",48.0*mm*0.5, 48.0*mm*0.5, 0.8*mm*0.5);
      // cut this part out:
      G4Box* TpxCopperApHelperSol2 = new G4Box ("TpxCopperApHelperSol2",20.0*mm*0.5, 20.0*mm*0.5, 0.8*mm); 
      TpxCopperAperatureSol = new G4SubtractionSolid("TpxCopperAperatureLog", TpxCopperApHelperSol1, TpxCopperApHelperSol2);
      TpxCopperAperatureLog = new G4LogicalVolume(TpxCopperAperatureSol, hbarDetConst::GetMaterial("Copper"), "TpxCopperAperature", 0, 0, 0);      
      TpxCopperAperaturePhy = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0, FoilZPos + 0.5*fFoilThick )), //(8.0*mm+10.0*mm+46.0*mm+23.0*mm)*0.5)
                                        "TpxCopperAperaturePhys", TpxCopperAperatureLog, WorldPhy, false, 0);
      
      
      /////////////////////////////////////////////////////////////////
      // Beampipe       
        
      TpxBeampipeSol = new G4Tubs("TpxBeampipeSol",50.*mm, 52.*mm, 1.0*0.5*m, 0., 2.0*M_PI);                                
      TpxBeampipeLog = new G4LogicalVolume(TpxBeampipeSol, hbarDetConst::GetMaterial("Sts"), "TpxBeampipeLog", 0, 0, 0);
      
      TpxBeampipePhys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0,FoilZPos - 8.0*mm - 1.0*0.5*m)),
                                             "TpxBeampipePhys", TpxBeampipeLog, WorldPhy, false, 0); 
      
      
      
      
      

}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
void hbarCPTConst::BuildDetailedHodoscope(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {
  //Glue
  GlueThickness = 0.1 *mm;

  // Scintillator bar
  BarX[0] = 20 *mm;
  BarZ[0] = 270*mm;
  BarY[0] = 5  *mm;
  BarX[1] = 35 *mm;
  BarZ[1] = 470*mm;
  BarY[1] = 5  *mm;
  //
  BarRotation = new G4RotationMatrix();
  BarPosition = G4ThreeVector();

  // Lightguides
 // Lightguide_baseX = 20 *mm;
  Lightguide_baseX[0] = 20 *mm;
  Lightguide_baseY[0] = 5  *mm;
  Lightguide_topX[0]  = 8  *mm;
  Lightguide_topY[0]  = Lightguide_baseY[0];
  LightguideZ[0]      = 40 *mm;
  
  Lightguide_baseX[1] = 35 *mm;
  Lightguide_baseY[1] = 5  *mm;
  Lightguide_topX[1]  = 8  *mm;
  Lightguide_topY[1]  = Lightguide_baseY[1];
  LightguideZ[1]      = 80 *mm;
  //
  LightguideTopRotation = new G4RotationMatrix();
  LightguideTopRotation->rotateX(M_PI/2.);
  LightguideTopPosition[0] = G4ThreeVector(0,BarZ[0]/2. + LightguideZ[0]/2.,0);
  LightguideTopPosition[1] = G4ThreeVector(0,BarZ[1]/2. + LightguideZ[1]/2.,0);
  
  LightguideBottomRotation = new G4RotationMatrix();
  LightguideBottomRotation->rotateX(-M_PI/2.);
  LightguideBottomPosition[0] = G4ThreeVector(0,- BarZ[0]/2. - LightguideZ[0]/2.,0);
  LightguideBottomPosition[1] = G4ThreeVector(0,- BarZ[1]/2. - LightguideZ[1]/2.,0);
  // SiPM's
  SiPM_SensorAreaSideLength = 3     *mm;
  SiPM_SensorThickness      = 0.725 *mm;
  SiPM_GapBetweenSensors    = 0.5   *mm;
  //
  SiPMTopRotation = new G4RotationMatrix();
  SiPMTopRotation->rotateX(M_PI/2.);
  SiPMTopPosition[0] = G4ThreeVector(0,BarZ[0]/2. + LightguideZ[0] + SiPM_SensorThickness/2.,0);
  SiPMTopPosition[1] = G4ThreeVector(0,BarZ[1]/2. + LightguideZ[1] + SiPM_SensorThickness/2.,0);
  
  SiPMBottomRotation = new G4RotationMatrix();
  SiPMBottomRotation->rotateX(-M_PI/2.);
  SiPMBottomPosition[0] = G4ThreeVector(0,- BarZ[0]/2. - LightguideZ[0] - SiPM_SensorThickness/2.,0);
  SiPMBottomPosition[1] = G4ThreeVector(0,- BarZ[1]/2. - LightguideZ[1] - SiPM_SensorThickness/2.,0);

  // Wrapping
  AluminiumFoilThickness = 0.011 *mm;
  WrappingAirGap = 0.05 *mm;

 //****************************************************************************************// 
 // Scintillator bar
  scintiBar_sol[0] = new G4Box("scinti_bar_0",BarX[0]/2.,BarY[0]/2.,BarZ[0]/2.);
  
  scintiBar_sol[1] = new G4Box("scinti_bar_1",BarX[1]/2.,BarY[1]/2.,BarZ[1]/2.);
	std::string da_name = "scinti_bar_";

	for(int i=0; i<2; i++) {
			da_name =   "scinti_bar_" + numtostr(i);
			scintiBar_log[i] = new G4LogicalVolume(scintiBar_sol[i],hbarDetConst::GetMaterial("Scintillator"), da_name, 0, 0, 0);
			scintiBar_log[i]->SetSensitiveDetector(scintiSD);
	}
	
 //****************************************************************************************// 
  // Scintillator bar wrapping
  G4Box* BarWrapOut[2];
  G4Box* BarWrapIn[2];

  BarWrapOut[0] = new G4Box("BarWrapOut_0",BarX[0]/2. + WrappingAirGap + AluminiumFoilThickness,BarY[0]/2. + WrappingAirGap + AluminiumFoilThickness,BarZ[0]/2. + GlueThickness/2.0);
  BarWrapIn[0] = new G4Box("BarWrapIn_0",BarX[0]/2. + WrappingAirGap,BarY[0]/2.  + WrappingAirGap,BarZ[0]/2. + GlueThickness/2.0 );
  BarWrapOut[1] = new G4Box("BarWrapOut_1",BarX[1]/2. + WrappingAirGap + AluminiumFoilThickness,BarY[1]/2. + WrappingAirGap + AluminiumFoilThickness,BarZ[1]/2. + GlueThickness/2.0);
  BarWrapIn[1] = new G4Box("BarWrapIn_1",BarX[1]/2. + WrappingAirGap,BarY[1]/2.  + WrappingAirGap,BarZ[1]/2. + GlueThickness/2.0 );

  scintiBarWrapping_sol[0] = new G4SubtractionSolid("scinti_bar_wrapping_0",BarWrapOut[0],BarWrapIn[0]);
  scintiBarWrapping_sol[1] = new G4SubtractionSolid("scinti_bar_wrapping_1",BarWrapOut[1],BarWrapIn[1]);

	for(int i=0; i<2; i++) {
			da_name = "scinti_bar_wrapping_" + numtostr(i);
 			 scintiBarWrapping_log[i] = new G4LogicalVolume(scintiBarWrapping_sol[i],hbarDetConst::GetMaterial("Wrapping"),da_name,0, 0, 0);
	}

 //****************************************************************************************// 
  // Lightguides
  Lightguide_sol[0] = new G4Trd("scinti_lightguide_0",Lightguide_baseX[0]/2.,Lightguide_topX[0]/2.,Lightguide_baseY[0]/2.,Lightguide_topY[0]/2.,LightguideZ[0]/2.);
  Lightguide_sol[1] = new G4Trd("scinti_lightguide_1",Lightguide_baseX[1]/2.,Lightguide_topX[1]/2.,Lightguide_baseY[1]/2.,Lightguide_topY[1]/2.,LightguideZ[1]/2.);

	for(int i=0; i<2; i++) {
			da_name = "scinti_lightguide_" + numtostr(i);
 			Lightguide_log[i] = new G4LogicalVolume(Lightguide_sol[i],hbarDetConst::GetMaterial("Plexiglas"),da_name,0, 0, 0);
	}

 //****************************************************************************************// 
  // Lightguides Wrapping
 G4Trd * LightguideWrappingOut[2];
 G4Trd * LightguideWrappingIn[2];
 LightguideWrappingOut[0] = new G4Trd("LightguideWrappingOut_0",
					    Lightguide_baseX[0]/2. + WrappingAirGap + AluminiumFoilThickness,Lightguide_topX[0]/2. + WrappingAirGap + AluminiumFoilThickness,
					    Lightguide_baseY[0]/2. + WrappingAirGap + AluminiumFoilThickness,Lightguide_topY[0]/2. + WrappingAirGap + AluminiumFoilThickness,LightguideZ[0]/2.);
  LightguideWrappingIn[0] = new G4Trd("LightguideWrappingIn_0",
					    Lightguide_baseX[0]/2. + WrappingAirGap,Lightguide_topX[0]/2. + WrappingAirGap,
					    Lightguide_baseY[0]/2. + WrappingAirGap,Lightguide_topY[0]/2. + WrappingAirGap,LightguideZ[0]/2.);

 LightguideWrappingOut[1] = new G4Trd("LightguideWrappingOut_1",
					    Lightguide_baseX[1]/2. + WrappingAirGap + AluminiumFoilThickness,Lightguide_topX[1]/2. + WrappingAirGap + AluminiumFoilThickness,
					    Lightguide_baseY[1]/2. + WrappingAirGap + AluminiumFoilThickness,Lightguide_topY[1]/2. + WrappingAirGap + AluminiumFoilThickness,LightguideZ[1]/2.);
  LightguideWrappingIn[1] = new G4Trd("LightguideWrappingIn_1",
					    Lightguide_baseX[1]/2. + WrappingAirGap, Lightguide_topX[1]/2. + WrappingAirGap,Lightguide_baseY[1]/2. + WrappingAirGap,
					    Lightguide_topY[1]/2. + WrappingAirGap, LightguideZ[1]/2.);

  LightguideWrapping_sol[0] = new G4SubtractionSolid("scinti_lightguide_wrapping_0",LightguideWrappingOut[0],LightguideWrappingIn[0]);

  LightguideWrapping_sol[1] = new G4SubtractionSolid("scinti_lightguide_wrapping_1",LightguideWrappingOut[1],LightguideWrappingIn[1]);


    for(int i=0; i<2; i++) {
			 da_name = "scinti_lightguide_wrapping_" + numtostr(i);
			 LightguideWrapping_log[i] = new G4LogicalVolume(LightguideWrapping_sol[i], hbarDetConst::GetMaterial("Wrapping"),da_name,0, 0, 0);
		//}
	}
  //****************************************************************************************// 
  // Glue Bar LG
  G4VisAttributes * LightBlue   = new G4VisAttributes( G4Colour(150/255 , 204/255., 230/255.));
  GlueBarLG_sol[0] = new G4Box("glue_barlg_0",BarX[0]/2.,BarY[0]/2.,GlueThickness/2.);
  GlueBarLG_sol[1] = new G4Box("glue_barlg_1",BarX[1]/2.,BarY[1]/2.,GlueThickness/2.);

	for(int i=0; i<2; i++) {
		da_name =    "glue_barlg_" + numtostr(i);
		GlueBarLG_log[i] = new G4LogicalVolume(GlueBarLG_sol[i],hbarDetConst::GetMaterial("Epoxy"),da_name, 0, 0, 0);
		GlueBarLG_log[i]->SetVisAttributes(LightBlue);
	}
  
	//****************************************************************************************// 
  // Glue LG SiPM

  GlueLGSiPM_sol[0] = new G4Box("glue_barsipm_0",Lightguide_topX[0]/2.,Lightguide_topY[0]/2.,GlueThickness/2.);
  GlueLGSiPM_sol[1] = new G4Box("glue_barsipm_1",Lightguide_topX[1]/2.,Lightguide_topY[1]/2.,GlueThickness/2.);

	for(int i=0; i<2; i++) {
		da_name =   "glue_barsipm_" + numtostr(i);
		GlueLGSiPM_log[i] = new G4LogicalVolume(GlueLGSiPM_sol[i],hbarDetConst::GetMaterial("Epoxy"),da_name,0, 0, 0);
		GlueLGSiPM_log[i]->SetVisAttributes(LightBlue);
	}

 //****************************************************************************************//
  // SiPM's
  G4Box * Box1 = new G4Box("Box1_2",SiPM_SensorAreaSideLength + SiPM_GapBetweenSensors/2.,SiPM_SensorAreaSideLength/2.,SiPM_SensorThickness/2.);
  G4Box * Box2 = new G4Box("Box2_2",SiPM_GapBetweenSensors/2.,SiPM_SensorAreaSideLength/2. + 1*mm,SiPM_SensorThickness/2. + 1*mm);
  SiPM_sol = new G4SubtractionSolid("SiPM_2",Box1,Box2);

    da_name = "Up_SiPM_";
	for(int i=0; i<2; i++) {
		for(int j=0; j<32; j++) {
			da_name = "Up_SiPM_" + numtostr(i) + "_" + numtostr(j);
 			 HodorSiPMUpLog[i][j] = new G4LogicalVolume(SiPM_sol,hbarDetConst::GetMaterial("SiPM"),da_name,0, 0, 0);
		}
	}

    da_name = "Down_SiPM_";
	for(int i=0; i<2; i++) {
		for(int j=0; j<32; j++) {
			da_name = "Down_SiPM_" + numtostr(i) + "_" + numtostr(j);
 			 HodorSiPMDownLog[i][j] = new G4LogicalVolume(SiPM_sol,hbarDetConst::GetMaterial("SiPM"),da_name,0, 0, 0);
		}
	}

//---------------------------------------------------------------------------------------------------------------

	int nr_layer_Hodoscope = 2;
	G4ThreeVector HodoscopePos[nr_layer_Hodoscope];
	
	HodoscopePos[0] = {0,0,0.0};
	HodoscopePos[1] = {0,0,0.0};

    G4RotationMatrix* HodorRot = new G4RotationMatrix();
    G4ThreeVector HodorTrans(0,0,0.0);
    
    SetAssemblyBar();

    Hodor = new G4AssemblyVolume();

    for (int j = 0; j < nr_layer_Hodoscope; j++) // j = Hodosope Layer, usually 0 = inner, 1 = outer
        {
            //Mapping for numbering the Hodoscope Bars according to the DAQ Process as construction has different order
            int HodoBarArray[32] = {11,10,9,8,7,6,5,4,3,2,1,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12};

            int HodoBarNum   = 0; //Hodoscope Scintillator Number from 0 to 31 per Hodoscope Layer

            G4double HodoScintYPos[32]; //X and Y Position of each Szintillator Bar (CoM Point)
            G4double HodoScintXPos[32];

            G4double RadBar[nr_layer_Hodoscope]; // Distance from tipping point to Center-Point for angeled bars
            G4double AngleBar[nr_layer_Hodoscope]; // Angle of the Diagonal of a bar (connection tipping point to Center-Point)
            
            G4double BarXTemp = BarX[j] + 0.122*mm; // increased length in x due to aur gab and wrapping

            RadBar[j]   = sqrt((BarXTemp)*(BarXTemp) + (BarY[j])*(BarY[j]))/2.0; 
            
            //std::cout << "Raaaaaad" << RadBar[j] << std::endl;
            
            AngleBar[j] = atan(BarY[j]/BarXTemp);

            for (G4int i=0; i<4; i+=1) //Very Top Octagon side
            {   // 1st number = Hodoscope Layer; 2nd number = Scintillator Unit
                // ScintillatorUnit number is in consecutive order clock wise from the very top left bar
                
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum]) + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                HodoSiPMRot->rotateZ(0*CLHEP::deg);
                
                G4double iDouble   = i*1.; // "integer to float type"

                HodoScintYPos[HodoBarNum] = (BarXTemp*4.)/(sqrt(2.)) + BarXTemp*2. + BarY[j]/2.; //Vertical Pos. through Rot
                HodoScintXPos[HodoBarNum] = -(3./2.)*BarXTemp + iDouble * BarXTemp; //Horizontal Pos.
                //G4cout << HodoscopePos[0] << " " << HodoscopePos[0] << G4endl;
				G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());							  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);
				
				 TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++)  // Right-Top Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum]) + "_SiPM_";
				G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                HodoSiPMRot->rotateZ(-45*CLHEP::deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] =  (BarXTemp*4.)/(sqrt(2.)) + BarXTemp*2.
                                            + RadBar[j]*sin(AngleBar[j] - M_PI/4.) + iDouble * BarXTemp*sin( - M_PI/4.);

                HodoScintXPos[HodoBarNum] =  2.*BarXTemp + RadBar[j]*cos(AngleBar[j] - M_PI/4.)
                                            + iDouble * BarXTemp*cos( - M_PI/4.);

				G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);				
				// HodoSiPMRot->rotateZ(90*CLHEP::deg);
				 TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
					Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
					Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Very Right Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                HodoSiPMRot->rotateZ(90*CLHEP::deg);
                G4double iDouble   = i*1.; // to get rid of number type problems

                HodoScintYPos[HodoBarNum] = (3./2.)*BarXTemp - iDouble * BarXTemp;
                HodoScintXPos[HodoBarNum] = (BarXTemp*4.)/(sqrt(2.)) + BarXTemp*2. + BarY[j]/2.;

                G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);
				
			 	 TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
			     Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Right-Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                HodoSiPMRot->rotateZ(45*CLHEP::deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = - 2*BarXTemp - RadBar[j]*cos(M_PI/4.-AngleBar[j])
                                            - iDouble * BarXTemp*sin(M_PI/4.);

                HodoScintXPos[HodoBarNum] = (BarXTemp*4.)/(sqrt(2.)) + BarXTemp*2.
                                            - RadBar[j]*sin(- AngleBar[j] + M_PI/4.) - iDouble * BarXTemp*cos( - M_PI/4.);
                                            
                G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);
                
				 //HodoSiPMRot->rotateZ(-90*CLHEP::deg);
                 TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
				  Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
					Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i+=1) // Very Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                 HodoSiPMRot->rotateZ(0*CLHEP::deg);
                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = -(BarXTemp*4.)/(sqrt(2.)) - BarXTemp*2. - BarY[j]/2.;
                HodoScintXPos[HodoBarNum] = +(3./2.)*BarXTemp - iDouble * BarXTemp;

				G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);

				 TransW.setZ( HodoscopePos[0].z()-BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				 //HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
					Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				//HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Left-Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                 HodoSiPMRot->rotateZ(-45*CLHEP::deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] =  - (BarXTemp*4.)/(sqrt(2.)) - 2*BarXTemp
                                            - RadBar[j]*sin(AngleBar[j] - M_PI/4.) + iDouble * BarXTemp*sin( M_PI/4.);

                HodoScintXPos[HodoBarNum] =  - 2.*BarXTemp - RadBar[j]*cos(AngleBar[j] - M_PI/4.)
                                            - iDouble * BarXTemp*cos( - M_PI/4.);

				G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);

				 //HodoSiPMRot->rotateZ(90*CLHEP::deg);
			     TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
					Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				 TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Very Left Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
                HodoSiPMRot->rotateZ(90*CLHEP::deg);
                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = -(3./2.)*BarXTemp + iDouble * BarXTemp;
                HodoScintXPos[HodoBarNum] = -(BarXTemp*4.)/(sqrt(2.)) - BarXTemp*2. - BarY[j]/2.;

                G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);

				TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				//HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);
				// HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Left-Top Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "_" + numtostr(HodoBarArray[HodoBarNum])  + "_SiPM_";
                G4RotationMatrix* HodoSiPMRot = new G4RotationMatrix();
				 HodoSiPMRot->rotateZ(45*CLHEP::deg);
                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = + 2*BarXTemp + RadBar[j]*cos(M_PI/4.-AngleBar[j])
                                            + iDouble * BarXTemp*sin(M_PI/4.);

                HodoScintXPos[HodoBarNum] = -(BarXTemp*4.)/(sqrt(2.)) - BarXTemp*2.
                                            + RadBar[j]*sin(- AngleBar[j] + M_PI/4.) + iDouble * BarXTemp*cos( - M_PI/4.);

                G4ThreeVector TransW(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z());						  
				Hodor->AddPlacedAssembly(assemblyBar[j], TransW, HodoSiPMRot);
                //HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName, assemblyBar[j], WorldPhys, false, 0);
				//HodoSiPMRot->rotateZ(-90*CLHEP::deg);
				TransW.setZ(HodoscopePos[0].z() -BarZ[j]*0.5 - LightguideZ[j] - SiPM_SensorThickness*0.5 - 2*GlueThickness);
				Hodor->AddPlacedVolume(HodorSiPMUpLog[j][HodoBarNum],TransW,HodoSiPMRot);
				//HodorSiPMUpPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"up", HodorSiPMUpLog[j][HodoBarNum], WorldPhys, false, 0);
				TransW.setZ(HodoscopePos[0].z() +BarZ[j]*0.5 + LightguideZ[j] + SiPM_SensorThickness*0.5 + 2*GlueThickness);
				//HodorSiPMDownPhy[j][HodoBarNum] = new G4PVPlacement(HodoSiPMRot, TransW, HodoScintName+"down", HodorSiPMDownLog[j][HodoBarNum], WorldPhys, false, 0);
				Hodor->AddPlacedVolume(HodorSiPMDownLog[j][HodoBarNum],TransW,HodoSiPMRot);

                HodoBarNum++;
            }
        }
        
        Hodor->MakeImprint(WorldPhy->GetLogicalVolume(), HodorTrans, HodorRot);
        
        //cout << assemblyBar[i]->CheckOverlaps() << endl;
        
        
}

//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
void hbarCPTConst::SetAssemblyBar() {
	
	assemblyBar[0] = new G4AssemblyVolume();
	assemblyBar[1] = new G4AssemblyVolume();
	
	G4ThreeVector Ta;
    G4RotationMatrix* Ra1 = new G4RotationMatrix();
    Ra1->rotateX(180*CLHEP::deg); 
    G4RotationMatrix* Ra2 = new G4RotationMatrix();
    Ra2->rotateX(0*CLHEP::deg); 
    
	for(int i=0; i<2; i++) {
		// Place Bar & Bar-wrapping
		Ta.setX( 0. ); Ta.setY( 0. ); Ta.setZ( 0. );
		assemblyBar[i]->AddPlacedVolume(scintiBar_log[i], Ta,0 );
		assemblyBar[i]->AddPlacedVolume(scintiBarWrapping_log[i], Ta, 0);

		// Place Glue between Bar and LG
		Ta.setX( 0. ); Ta.setZ( BarZ[i]/2.0 + GlueThickness/2.0 ); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(GlueBarLG_log[i], Ta,0 );
		Ta.setX( 0. ); Ta.setZ( - BarZ[i]/2.0 - GlueThickness/2.0); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(GlueBarLG_log[i], Ta,0 );
		

		// Place Lightguide and LG-wrapping both sides
		Ta.setX( 0. ); Ta.setZ( BarZ[i]/2.0 + LightguideZ[i]/2.0 + GlueThickness ); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(Lightguide_log[i], Ta , Ra2);
		assemblyBar[i]->AddPlacedVolume(LightguideWrapping_log[i],Ta, Ra2);

		Ta.setX( 0. ); Ta.setZ(- BarZ[i]/2.0 -LightguideZ[i]/2.0 - GlueThickness ); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(Lightguide_log[i], Ta , Ra1);	
		assemblyBar[i]->AddPlacedVolume(LightguideWrapping_log[i],Ta, Ra1);

		// Place Glue between LG and SiPM both sides
		Ta.setX( 0. ); Ta.setZ( BarZ[i]/2.0 + LightguideZ[i] + GlueThickness + GlueThickness/2.0); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(GlueLGSiPM_log[i], Ta , Ra2);
		Ta.setX( 0. ); Ta.setZ(- BarZ[i]/2.0 -LightguideZ[i] - GlueThickness - GlueThickness/2.0); Ta.setY( 0. );
		assemblyBar[i]->AddPlacedVolume(GlueLGSiPM_log[i], Ta , Ra1);	
        

	}
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
/*void hbarCPTConst::LoadGDMLHodoscope(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {
	// Get Volumes from GDML files---------------------------------------------------------------------------------
    G4GDMLParser fParser;
	fParser.Read("./data_scintil/hodor_geometry.gdml", false); // read GDML file
	Hodor_log = fParser.GetVolume("Hodor_cyl_logical"); // get Tube with Hodor inside
	Hodor_log->SetMaterial(hbarDetConst::GetMaterial("Galactic")); // set material of tube

	G4ThreeVector HodorTrans(0, 0, HodoscopePos[0].z());
    G4RotationMatrix* HodorRot = new G4RotationMatrix();
	Hodor_phy = new G4PVPlacement(HodorRot, HodorTrans, "Hodor", Hodor_log, WorldPhy, false, 0); // place tube
	
	//cout << Hodor_phy->G4PVPlacement::CheckOverlaps() << endl;

	fParser.GetVolume("scinti_bar_0")->SetMaterial(hbarDetConst::GetMaterial("Scintillator"));
	fParser.GetVolume("scinti_bar_0")->SetSensitiveDetector(scintiSD);
	fParser.GetVolume("scinti_bar_1")->SetMaterial(hbarDetConst::GetMaterial("Scintillator"));
	fParser.GetVolume("scinti_bar_1")->SetSensitiveDetector(scintiSD);

	string scintiname;
	

	//->SetSensitiveDetector(scintiSD);
	//fParser.GetVolume(da_name_up)->SetSensitiveDetector(SiPM_SD_up[i][j]);

	fParser.GetVolume("scinti_lightguide_0")->SetMaterial(hbarDetConst::GetMaterial("Plexiglas"));
	fParser.GetVolume("scinti_lightguide_1")->SetMaterial(hbarDetConst::GetMaterial("Plexiglas"));

	fParser.GetVolume("glue_barlg_0")->SetMaterial(hbarDetConst::GetMaterial("Plexiglas"));
	fParser.GetVolume("glue_barlg_1")->SetMaterial(hbarDetConst::GetMaterial("Plexiglas"));

	fParser.GetVolume("glue_barsipm_0")->SetMaterial(hbarDetConst::GetMaterial("Epoxy_Glue"));
	fParser.GetVolume("glue_barsipm_1")->SetMaterial(hbarDetConst::GetMaterial("Epoxy_Glue"));

	DefineMaterialPropertiesHodor(); // set optical properties: Relative light output of scintillator, refractive indices etc...

 	std::string da_name_up = "Up_SiPM_";
	std::string da_name_down = "Down_SiPM_";

   for(int i = 0; i< 2; i++) {
		for(int j=0; j<32; j++) {
			da_name_up = "Up_SiPM_" + numtostr(i) + "_" + numtostr(j);
			da_name_down = "Down_SiPM_" + numtostr(i) + "_" + numtostr(j);
			fParser.GetVolume(da_name_up)->SetMaterial(hbarDetConst::GetMaterial("SiPM"));
			fParser.GetVolume(da_name_down)->SetMaterial(hbarDetConst::GetMaterial("SiPM"));
		}
	}

   // Make a reflecting wrapping-----------------------------------------------------------------------------------
    OptSurface = new G4OpticalSurface("wrappingSurface");
    BarWrappingSurface0 = new G4LogicalSkinSurface("bar_wrapping_surface_0",
								  fParser.GetVolume("scinti_bar_wrapping_0"),
								  OptSurface
								  );

    BarWrappingSurface1 = new G4LogicalSkinSurface("bar_wrapping_surface_1",
								 fParser.GetVolume("scinti_bar_wrapping_1"),
								  OptSurface
								  );

    LightguideWrappingSurface0 = new G4LogicalSkinSurface("lightguide_wrapping_surface_0",
  								  fParser.GetVolume("scinti_lightguide_wrapping_0"),
  								  OptSurface
  								  );

    LightguideWrappingSurface1 = new G4LogicalSkinSurface("lightguide_wrapping_surface_1",
  								  fParser.GetVolume("scinti_lightguide_wrapping_1"),
  								  OptSurface
  								  );

	OptSurface->SetType(dielectric_metal);
	OptSurface->SetFinish(polished);
	OptSurface->SetModel(glisur);

	G4MaterialPropertiesTable * OptSurface_MPT = new G4MaterialPropertiesTable();
	OptSurface_MPT->AddProperty("REALRINDEX", &(Al_real_PP[0]), &(Al_real[0]), Al_real_PP.size());
	OptSurface_MPT->AddProperty("IMAGINARYRINDEX", &(Al_imag_PP[0]), &(Al_imag[0]), Al_imag_PP.size());
	OptSurface->SetMaterialPropertiesTable(OptSurface_MPT);

	G4double maxStep = 0.5 *mm;
	G4UserLimits* stepLimit = new G4UserLimits(maxStep);
	Hodor_log->SetUserLimits(stepLimit);

	G4VisAttributes * Metal       = new G4VisAttributes( G4Colour(204/255., 204/255., 255/255.));
    G4VisAttributes * LightBlue   = new G4VisAttributes( G4Colour(150/255 , 204/255., 230/255.));
	G4VisAttributes * orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));


  // Set SiPM's sensitive --------------------------------------------------------------------------------------
	if(UseSensitiveSiPMs==true) {
		G4SDManager* SDman = G4SDManager::GetSDMpointer();
		G4String SDname;
		ReadResponseFunctionSiPM();

    	for(int i=0; i<2; i++) {
			for(int j=0; j<32; j++) {
				SiPM_SD_up[i][j] = new hbarSiPM_SD(SDname="SiPM_up_SD_" + numtostr(i) + "_" + numtostr(j), this);
				SiPM_SD_down[i][j] = new hbarSiPM_SD(SDname="SiPM_down_SD_" + numtostr(i) + "_" + numtostr(j), this);
				SDman->AddNewDetector(SiPM_SD_up[i][j]);
				SDman->AddNewDetector(SiPM_SD_down[i][j]);
				da_name_up = "Up_SiPM_" + numtostr(i) + "_" + numtostr(j);
				da_name_down = "Down_SiPM_" + numtostr(i) + "_" + numtostr(j);
				fParser.GetVolume(da_name_up)->SetSensitiveDetector(SiPM_SD_up[i][j]);
				fParser.GetVolume(da_name_down)->SetSensitiveDetector(SiPM_SD_down[i][j]);
				fParser.GetVolume(da_name_up)->SetVisAttributes(orangecolor);
				fParser.GetVolume(da_name_down)->SetVisAttributes(orangecolor);
				if(SaveWaveformsToDat)  {
					SiPM_SD_up[i][j]->Set_write_to_dat_files(true);
					SiPM_SD_down[i][j]->Set_write_to_dat_files(true);
				}
				if(WriteWaveformsToPDF) {
					SiPM_SD_up[i][j]->Set_drawhistos(true);
					SiPM_SD_down[i][j]->Set_drawhistos(true);
				}
			}
		}

	}
	// Set visible attributes--------------------------------------------------------------------------------------
    fParser.GetVolume("scinti_bar_0")->SetVisAttributes(LightBlue);
    fParser.GetVolume("scinti_bar_1")->SetVisAttributes(LightBlue);
    //fParser.GetVolume("scinti_lightguide_0")->SetVisAttributes(Metal);
    //fParser.GetVolume("scinti_lightguide_1")->SetVisAttributes(Metal);
	fParser.GetVolume("Hodor_cyl_logical")-> SetVisAttributes (G4VisAttributes::GetInvisible()); // make tube invisible
		for(int i=0; i<2; i++) {
			for(int j=0; j<32; j++) {
				da_name_up = "Up_SiPM_" + numtostr(i) + "_" + numtostr(j);
				da_name_down = "Down_SiPM_" + numtostr(i) + "_" + numtostr(j);
				fParser.GetVolume(da_name_up)->SetVisAttributes(orangecolor);
				fParser.GetVolume(da_name_down)->SetVisAttributes(orangecolor);
			}
		}				   
}*/
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
void hbarCPTConst::BuildOctHodoscope(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {

        G4VisAttributes* orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));

        for (int j = 0; j < maxHodoscope; j++) // j = Hodosope Layer, usually 0 = inner, 1 = outer
        {
          if (useHodoscope[j])
          {
			//std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ "<< j << std::endl;
            //Mapping for numbering the Hodoscope Bars according to the DAQ Process as construction has different order
            int HodoBarArray[32] = {11,10,9,8,7,6,5,4,3,2,1,0,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12};

            int HodoBarNum   = 0; //Hodoscope Scintillator Number from 0 to 31 per Hodoscope Layer

            G4double HodoScintYPos[32]; //X and Y Position of each Szintillator Bar (CoM Point)
            G4double HodoScintXPos[32];

            G4double RadBar[maxHodoscope]; // Distance from tipping point to Center-Point for angeled bars
            G4double AngleBar[maxHodoscope]; // Angle of the Diagonal of a bar (connection tipping point to Center-Point)

            RadBar[j]   = sqrt((HodoScintWidth[j])*(HodoScintWidth[j]) + (HodoScintHeight[j])*(HodoScintHeight[j]))/2.0;
            AngleBar[j] = atan(HodoScintHeight[j]/HodoScintWidth[j]);

            for (G4int i=0; i<4; i+=1) //Very Top Octagon side
            {   // 1st number = Hodoscope Layer; 2nd number = Scintillator Unit
                // ScintillatorUnit number is in consecutive order clock wise from the very top left bar
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the ScintiBars
                HodoScintRot->rotateZ(0*deg);
                G4double iDouble   = i*1.; // "integer to float type"

                HodoScintYPos[HodoBarNum] = (HodoScintWidth[j]*4.)/(sqrt(2.)) + HodoScintWidth[j]*2. + HodoScintHeight[j]/2.; //Vertical Pos. through Rot
                HodoScintXPos[HodoBarNum] = -(3./2.)*HodoScintWidth[j] + iDouble * HodoScintWidth[j]; //Horizontal Pos.

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++)  // Right-Top Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(45*deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] =  (HodoScintWidth[j]*4.)/(sqrt(2.)) + HodoScintWidth[j]*2.
                                            + RadBar[j]*sin(AngleBar[j] - M_PI/4.) + iDouble * HodoScintWidth[j]*sin( - M_PI/4.);

                HodoScintXPos[HodoBarNum] =  2.*HodoScintWidth[j] + RadBar[j]*cos(AngleBar[j] - M_PI/4.)
                                            + iDouble * HodoScintWidth[j]*cos( - M_PI/4.);

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],
                                                            HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Very Right Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(90*deg);
                G4double iDouble   = i*1.; // to get rid of number type problems

                HodoScintYPos[HodoBarNum] = (3./2.)*HodoScintWidth[j] - iDouble * HodoScintWidth[j];
                HodoScintXPos[HodoBarNum] = (HodoScintWidth[j]*4.)/(sqrt(2.)) + HodoScintWidth[j]*2. + HodoScintHeight[j]/2.;


                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Right-Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(-45*deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = - 2*HodoScintWidth[j] - RadBar[j]*cos(M_PI/4.-AngleBar[j])
                                            - iDouble * HodoScintWidth[j]*sin(M_PI/4.);

                HodoScintXPos[HodoBarNum] = (HodoScintWidth[j]*4.)/(sqrt(2.)) + HodoScintWidth[j]*2.
                                            - RadBar[j]*sin(- AngleBar[j] + M_PI/4.) - iDouble * HodoScintWidth[j]*cos( - M_PI/4.);

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],
                                                            HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i+=1) // Very Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(0*deg);
                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = -(HodoScintWidth[j]*4.)/(sqrt(2.)) - HodoScintWidth[j]*2. - HodoScintHeight[j]/2.;
                HodoScintXPos[HodoBarNum] = +(3./2.)*HodoScintWidth[j] - iDouble * HodoScintWidth[j];

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Left-Bottom Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(45*deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] =  - (HodoScintWidth[j]*4.)/(sqrt(2.)) - 2*HodoScintWidth[j]
                                            - RadBar[j]*sin(AngleBar[j] - M_PI/4.) + iDouble * HodoScintWidth[j]*sin( M_PI/4.);

                HodoScintXPos[HodoBarNum] =  - 2.*HodoScintWidth[j] - RadBar[j]*cos(AngleBar[j] - M_PI/4.)
                                            - iDouble * HodoScintWidth[j]*cos( - M_PI/4.);

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],
                                                            HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Very Left Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(90*deg);
                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = -(3./2.)*HodoScintWidth[j] + iDouble * HodoScintWidth[j];
                HodoScintXPos[HodoBarNum] = -(HodoScintWidth[j]*4.)/(sqrt(2.)) - HodoScintWidth[j]*2. - HodoScintHeight[j]/2.;


                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum], HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }

            for (G4int i=0; i<4; i++) // Left-Top Octagon side
            {
                G4String HodoScintName = "Hodoscope" + numtostr(j) + "/" + numtostr(HodoBarArray[HodoBarNum]);
                G4RotationMatrix* HodoScintRot = new G4RotationMatrix(); // Rotate the parts
                HodoScintRot->rotateZ(-45*deg);

                G4double iDouble   = i*1.;

                HodoScintYPos[HodoBarNum] = + 2*HodoScintWidth[j] + RadBar[j]*cos(M_PI/4.-AngleBar[j])
                                            + iDouble * HodoScintWidth[j]*sin(M_PI/4.);

                HodoScintXPos[HodoBarNum] = -(HodoScintWidth[j]*4.)/(sqrt(2.)) - HodoScintWidth[j]*2.
                                            + RadBar[j]*sin(- AngleBar[j] + M_PI/4.) + iDouble * HodoScintWidth[j]*cos( - M_PI/4.);

                G4Box* HodoInnBox = new G4Box("HodoScintUnit", HodoScintWidth[j]/2., HodoScintHeight[j]/2., HodoscopeLength[j]/2.);
                HodoBarLog[j][HodoBarNum] = new G4LogicalVolume(HodoInnBox, hbarDetConst::GetMaterial("Scintillator"), HodoScintName, 0, 0, 0);
                HodoBarPhy[j][HodoBarNum] = new G4PVPlacement(HodoScintRot, G4ThreeVector(HodoScintXPos[HodoBarNum], HodoScintYPos[HodoBarNum],
                                                            HodoscopePos[0].z()), HodoScintName, HodoBarLog[j][HodoBarNum], WorldPhy, false, 0);

                HodoBarLog[j][HodoBarNum]->SetVisAttributes(orangecolor);
                HodoBarLog[j][HodoBarNum]->SetSensitiveDetector(scintiSD);

                HodoBarNum++;
            }
          }
        }

	}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
	   // vacuum
       /*HodoscopeVacuumSol = new G4Tubs("vacuum_hodoscope_sol",0.*mm,
                                       50.*mm, 1.0*0.5*m, // 75.*mm length   
                                       0., 2.0*M_PI);
                                       
                              
                                       
      HodoscopeVacuumLog = new G4LogicalVolume(HodoscopeVacuumSol, hbarDetConst::GetMaterial("Vac"), "vacuum_hodoscope", 0, 0, 0); */
      
      
      //G4double maxStep = 0.1 *mm;
	  //G4UserLimits* stepLimit = new G4UserLimits(maxStep);
	  //HodoscopeVacuumLog->SetUserLimits(stepLimit); 
      
       //HodoscopeVacuumPhys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0,2.0*m - 0.5*m - 2.5005*mm )), "vacuum_hodoscope_phys", HodoscopeVacuumLog, WorldPhy, false, 0); 
	   //HodoscopeVacuumPhys = new G4PVPlacement(G4Transform3D(G4RotationMatrix(0,0,0), G4ThreeVector(0,0,2.0*m - 0.5*m - 0.00105*mm )), "vacuum_hodoscope_phys", HodoscopeVacuumLog, WorldPhy, false, 0); 
	
//----------------------------------- Octagonal Hodoscoupe END -------------------------------------//
	  // BGO start 1.9975
	  // TODO placement of vacuum in tube! check spectroscopy macro for the beampipe 
	    /*# Pipe inside Hodoscope
        /setup/setUseBeamPipe 18
        /setup/setBeamPipeStart 1.615
        /setup/setBeamPipeEnd 2.98 # 1.826
        /setup/setBeamPipeFrontInnerDiameter 0.10
        /setup/setBeamPipeFrontOuterDiameter 0.12
        /setup/setBeamPipeRearInnerDiameter 0.10
        /setup/setBeamPipeRearOuterDiameter 0.12*/
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X       
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X	
void hbarCPTConst::BuildFibreDetector(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {	
	InnerFibreRadius = 167.0*0.5; // mm
	OuterFibreRadius = 292.0*0.5; // mm
	FibreWidth = 4.0; // mm
	InnerFibreChannels = 63;
	OuterFibreChannels = 100;
	InnerFibreLength = 252.0;
	OuterFibreLength = 403.9;
	
	G4RotationMatrix* FibreRot = new G4RotationMatrix(); 
    //G4ThreeVector* FibrePos = new G4ThreeVector(0.0, 0.0,  - InnerFibreLength*0.5 + i*FibreWidth );
    //FibreRot->rotateZ(90*deg);
		
	InnerFibreSol = new G4Tubs("InnerFibreSol", InnerFibreRadius - FibreWidth*0.5, InnerFibreRadius + FibreWidth*0.5, FibreWidth*0.5, 0., 2.0*M_PI);	
	OuterFibreSol = new G4Tubs("OuterFibreSol", OuterFibreRadius - FibreWidth*0.5, OuterFibreRadius + FibreWidth*0.5, FibreWidth*0.5, 0., 2.0*M_PI);
	
    for(int i=0; i < InnerFibreChannels; i++) {    
        G4String InnerFibreName = "InnerFibre" + numtostr(i) + "-";
        InnerFibreLog[i] = new G4LogicalVolume(InnerFibreSol, hbarDetConst::GetMaterial("Scintillator"), InnerFibreName, 0, 0, 0);	
	    InnerFibrePhy[i] = new G4PVPlacement(FibreRot, G4ThreeVector(0.0, 0.0,  HodoscopePos[0].z() - InnerFibreLength*0.5 + i*FibreWidth ),InnerFibreName, InnerFibreLog[i], WorldPhy, false, 0);
	    InnerFibreLog[i]->SetSensitiveDetector(scintiSD);
	    //cout << InnerFibrePhy[i]->G4PVPlacement::CheckOverlaps() << endl;
	}
	
	for(int i=0; i < OuterFibreChannels; i++) {    
        G4String OuterFibreName = "OuterFibre" + numtostr(i) + "-";
        OuterFibreLog[i] = new G4LogicalVolume(OuterFibreSol, hbarDetConst::GetMaterial("Scintillator"), OuterFibreName, 0, 0, 0);	
        //G4ThreeVector* FibrePos = new G4ThreeVector(0.0, 0.0, - OuterFibreLength*0.5 + i*FibreWidth );
	    OuterFibrePhy[i] = new G4PVPlacement(FibreRot, G4ThreeVector(0.0, 0.0, HodoscopePos[0].z() - OuterFibreLength*0.5 + i*FibreWidth), OuterFibreName, OuterFibreLog[i], WorldPhy, false, 0);
	    OuterFibreLog[i]->SetSensitiveDetector(scintiSD);
	    //cout << OuterFibrePhy[i]->G4PVPlacement::CheckOverlaps() << endl;
	}
	
	
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X       
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
/*void hbarCPTConst::BuildGDMLBGO(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {    
	// Get Volumes from GDML files---------------------------------------------------------------------------------
    G4GDMLParser fParser;
	fParser.Read("./data_scintil/BGO_geometry.gdml", false); // read GDML file
	//fParser.Read("./data_scintil/nobgo.gdml", false); // read GDML file
	//fParser.Read("./data_scintil/bgo_geometry_onlybgo.gdml", false); // read GDML file
    BGOboxlog = fParser.GetVolume("BGO_box_logical"); // get Tube with Hodor inside
	BGOboxlog->SetMaterial(hbarDetConst::GetMaterial("Galactic")); // set material of tube
	//G4double BGO2014ZPos = 1.5*m;
	G4ThreeVector BGOTrans(0.0, 0.0, HodoscopePos[0].z());	
    G4RotationMatrix* BGORot = new G4RotationMatrix();
	BGOboxphy = new G4PVPlacement(BGORot, BGOTrans, "BGOphy", BGOboxlog, WorldPhy, false, 0); // place box

	// Set colours ------------------------------------------------------------------------------------------------	
	G4VisAttributes * Metal       = new G4VisAttributes( G4Colour(204/255., 204/255., 255/255.));
    G4VisAttributes * LightBlue   = new G4VisAttributes( G4Colour(150/255 , 204/255., 230/255.));
	G4VisAttributes * orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));
	G4VisAttributes * blackcolor = new G4VisAttributes(G4Colour(0.0,0.0,0.0));
	G4VisAttributes * whitecolor = new G4VisAttributes(G4Colour(1.0,1.0,1.0));

	fParser.GetVolume("BGO_box_logical")-> SetVisAttributes(G4VisAttributes::GetInvisible()); // make tube invisible

	// Set material (properties)-----------------------------------------------------------------------------------			
	fParser.GetVolume("BGO_log")->SetMaterial(hbarDetConst::GetMaterial("BGO"));

	fParser.GetVolume("Coat_log")->SetMaterial(hbarDetConst::GetMaterial("CarbonCoatmaterial"));
	fParser.GetVolume("Coat_log")->SetVisAttributes(blackcolor);

	coat_bgo_opsurf = new G4OpticalSurface("coat_bgo_opsurf");

	border_surf = new G4LogicalBorderSurface("border_surf", fParser.GetVolume("BGO_box_logical")->GetDaughter(0), 
															fParser.GetVolume("BGO_box_logical")->GetDaughter(1), coat_bgo_opsurf);
	//G4double sigma_alpha = 0.1;
    //	for(int i=0; i<300; i++) std::cout << "+++++++ " << fParser.GetVolume("BGO_box_logical")->GetDaughter(i)->GetName() << std::endl;

	coat_bgo_opsurf->SetType(dielectric_dielectric);
	coat_bgo_opsurf->SetModel(glisur);
	coat_bgo_opsurf->SetFinish(ground);
	//coat_bgo_opsurf->SetSigmaAlpha(sigma_alpha);

	borosilicate_opsurf = new G4OpticalSurface("borosilicate");
	borosilicate_opsurf->SetType(dielectric_dielectric);
	borosilicate_opsurf->SetModel(glisur);
	borosilicate_opsurf->SetFinish(polished);

	G4LogicalSkinSurface* boro_surf = new G4LogicalSkinSurface("boro_surf",fParser.GetVolume("Kovar_log"), borosilicate_opsurf);
	G4LogicalSkinSurface* boro2_surf= new G4LogicalSkinSurface("boro2_surf",fParser.GetVolume("Tempax_log"), borosilicate_opsurf);

	// G4BoundaryProcess with surfaces is only possible for volumnes
	// that have been positioned by using placement rather than replica or parameterised volumes.

	//new G4LogicalSkinSurface("coat_surf",fParser.GetVolume("Coat_log"), coat_opsurf);
	new G4LogicalSkinSurface("coat_surf",fParser.GetVolume("Coat_log"), coat_bgo_opsurf);

	fParser.GetVolume("BGO_Flange_log")->SetMaterial(hbarDetConst::GetMaterial("Sts"));
	fParser.GetVolume("BGO_Flange_log")-> SetVisAttributes(Metal);

	fParser.GetVolume("Kovar_log")->SetMaterial(hbarDetConst::GetMaterial("Borosilicate"));
	fParser.GetVolume("Kovar_log")->SetVisAttributes(whitecolor);
	fParser.GetVolume("Tempax_log")->SetMaterial(hbarDetConst::GetMaterial("Borosilicate"));
	fParser.GetVolume("Tempax_log")->SetVisAttributes(whitecolor);
	fParser.GetVolume("Cathodewindow_log")->SetMaterial(hbarDetConst::GetMaterial("Borosilicate"));
	fParser.GetVolume("Cathodewindow_log")->SetVisAttributes(whitecolor);

	DefineMaterialPropertiesBGO(); // set optical properties
	
 	std::string da_name_anode = "PMT_";

   for(int i = 0; i<4; i++) {
		for(int j=0; j<64; j++) {
			da_name_anode = "PMT_" + numtostr(i) + "_" + numtostr(j);
			fParser.GetVolume(da_name_anode)->SetMaterial(hbarDetConst::GetMaterial("PMTmaterial"));
		    fParser.GetVolume(da_name_anode)->SetVisAttributes(orangecolor);
		}
	}

  // Set BGO PMT's sensitive --------------------------------------------------------------------------------------
	if(UseSensitiveBGOPMTs==true) {

		//Photocathode surface properties
		G4SDManager* SDman = G4SDManager::GetSDMpointer();
		G4String SDname;

		PMTGain = CalculatePMTGain(MAPMT_SupplyVoltage);

		int count = 0;

    	for(int i=0; i<4; i++) {
			//count = 9;
			for(int j=0; j<64; j++) {
				BGOPMT_SD[i][j] = new hbarBGOPMT_SD(SDname= "BGOPMT_SD_" + numtostr(i) + "_" + numtostr(j), this);
				da_name_anode = "PMT_" + numtostr(i) + "_" + numtostr(j);
				SDman->AddNewDetector(BGOPMT_SD[i][j]);								
				new G4LogicalSkinSurface("photocath_surf",fParser.GetVolume(da_name_anode),photocath_opsurf);
				//new G4LogicalBorderSurface("border_surf_" + numtostr(i+5), fParser.GetVolume("BGO_box_logical")->GetDaughter(5+i), 
				//											fParser.GetVolume("BGO_box_logical")->GetDaughter((9+j+i*64)), photocath_opsurf);

				//std::cout << fParser.GetVolume("BGO_box_logical")->GetDaughter((9+j+i*64))->GetName() << std::endl;
				fParser.GetVolume(da_name_anode)->SetSensitiveDetector(BGOPMT_SD[i][j]);	
				//fParser.GetVolume(da_name_anode)->SetSensitiveDetector(scintiSD);
				//new G4LogicalSkinSurface("photocath_surf",fParser.GetVolume(da_name_anode),photocath_opsurf);
			}
		}
	}

	//std::cout << "hallo const " << std::endl;
    //UseSensitiveBGO = true;
	//G4SDManager* SDman = G4SDManager::GetSDMpointer();
	//BGODet_SD = new hbarDetectorSD("BGODet_SD");
   
	//SDman->AddNewDetector(BGODet_SD);
	fParser.GetVolume("BGO_log")->SetSensitiveDetector(scintiSD);
   }*/
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X       
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X   
void hbarCPTConst::BuildBGO(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) { 
      G4double BGO2014ZPos = 0;
      //--------------------------------------- BGO 2014 START -------------------------------------------//
        for (G4int i = 0; i < maxBGO2014; i++)
	    {
	      if (useBGO2014[i] && ((BGOScint2014End[i]-BGOScint2014Start[i]) > 0.0) && (BGOScint2014Diam[i] > 0.0) )
          {
               G4String BGO2014Name = "BGO2014" + numtostr(i);
               BGO2014ZPos = (BGOScint2014End[i]+ BGOScint2014Start[i])*.5;

               G4Tubs* BGO2014Scint = new G4Tubs("BGO2014", 0, BGOScint2014Diam[i]*.5, (BGOScint2014End[i]-BGOScint2014Start[i])*.5, 0.0, 2.0*M_PI); // FIXME this is for BGO
               //G4Box* BGO2014Scint = new G4Box("BGO2014", 2.0*cm*0.5, 2.0*cm*0.5, 0.001*mm); // this is the timepix carbon foil
               
               
               BGO2014Log[i] = new G4LogicalVolume(BGO2014Scint, hbarDetConst::GetMaterial("BGO"), BGO2014Name, 0, 0, 0);
               BGO2014Phy[i] = new G4PVPlacement(0, G4ThreeVector(0, 0, BGO2014ZPos), BGO2014Name, BGO2014Log[i], WorldPhy, false, 0);

               //BGO2014Log[i]->SetVisAttributes(orangecolor);
               BGO2014Log[i]->SetSensitiveDetector(scintiSD);
          }
	    }

        // carbon coating
       G4Tubs *Clayer = new G4Tubs("CLayer1", 0,
                              45.0*mm, 0.7*0.5*m*1e-6,   // always half length in Z
                                    0, 2.0*M_PI);
        G4LogicalVolume *innerC = new G4LogicalVolume(Clayer,  hbarDetConst::GetMaterial("CarbonCoatmaterial"), "innerLOG2");                            
        G4PVPlacement *innerphys2 = new G4PVPlacement(NULL,G4ThreeVector(0,0,BGO2014ZPos - 2.5*mm - 0.7/2.0*m*1e-6), // minus half length
                                               "carbon", innerC, WorldPhy, false, 0); 
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X       
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X   
void hbarCPTConst::BuildveryOldHodoscope(G4VPhysicalVolume * WorldPhy, hbarDetectorSD *scintiSD) {    
  // Hodoscope
    for (G4int i = 0; i < maxHodoscope; i++)
	  {
	    if (useHodoscope[i])
		  {
		    G4String tmp = "Hodoscope" + numtostr(i+1);
		    G4String detname = tmp + "_det";
		    G4String detnamel = detname + "L";
		    G4String detnamep = detname + "P";
		    G4String detnamediv = detname + "_div";
		    G4String detnamephys = tmp;

		    G4double tube_dPhi = 2.* M_PI * rad;
		    HodoscopeBaseSol[i] = new G4Tubs(detname, HodoscopeInRad[i], HodoscopeOutRad[i], HodoscopeLength[i]/2., 0.,tube_dPhi);
		    HodoscopeBaseLog[i] = new G4LogicalVolume(HodoscopeBaseSol[i], hbarDetConst::GetMaterial("Scintillator"), detnamel, 0, 0, 0);
		    HodoscopeBasePhy[i] = new G4PVPlacement(G4Transform3D(HodoscopeRot[i], HodoscopePos[i]),detname, HodoscopeBaseLog[i], WorldPhy, false, 0);

		    G4double divided_tube_dPhi = tube_dPhi/Hododiv[i];
		    HodoscopeSol[i] = new G4Tubs(detnamediv,HodoscopeInRad[i],HodoscopeOutRad[i],HodoscopeLength[i]/2,-divided_tube_dPhi/2.,divided_tube_dPhi);
		    HodoscopeLog[i] = new G4LogicalVolume(HodoscopeSol[i], hbarDetConst::GetMaterial("Scintillator"),detnamediv+"L",0,0,0);
		    HodoscopePhy[i] = new G4PVReplica(detnamephys, HodoscopeLog[i], HodoscopeBaseLog[i], kPhi, Hododiv[i], divided_tube_dPhi);

		    G4VisAttributes* orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));
		    HodoscopeLog[i]->SetVisAttributes(orangecolor);
		    HodoscopeBaseLog[i]->SetVisAttributes(orangecolor);
		    HodoscopeLog[i]->SetSensitiveDetector(scintiSD);
		}
	}
  
  if (useHbarCounter)
	{
	  HbarBaseSol = new G4Box("HbarDetectorBase_sol", HbarDetX, HbarDetY,
							  HbarDetZ);
	  HbarBaseLog = new G4LogicalVolume(HbarBaseSol, hbarDetConst::GetMaterial("Scintillator"),
										"HbarDetectorBase_log", 0, 0, 0);
	  HbarBasePhy = new G4PVPlacement(G4Transform3D(HbarRot, HbarPos),
									  "HbarDetectorBase", HbarBaseLog, WorldPhy,
									  false, 0);

	  HbarYSegSol = new G4Box("HbarDetectorYSeg_sol", HbarDetX,
							  HbarDetY/HbarYseg, HbarDetZ);
	  HbarYSegLog = new G4LogicalVolume(HbarYSegSol, hbarDetConst::GetMaterial("Scintillator"),
										"HbarDetectorYSeg_log", 0, 0, 0);
	  HbarYSegPhy = new G4PVDivision("HbarDetSeg",
									 HbarYSegLog, HbarBaseLog, kYAxis, HbarYseg, 0);
	  HbarSol = new G4Box("HbarDetectorSeg_sol",HbarDetX/HbarXseg,
						  HbarDetY/HbarYseg, HbarDetZ);
	  HbarLog = new G4LogicalVolume(HbarSol,hbarDetConst::GetMaterial("Scintillator"),"HbarDetectorSeg_log",
									0,0,0);
	  HbarPhy = new G4PVDivision("HbarDet",
								 HbarLog, HbarYSegLog, kXAxis, HbarXseg, 0);
	  G4VisAttributes* redcolor = new G4VisAttributes(G4Colour(1.0,0.0,0.0));

	  HbarBaseLog->SetVisAttributes(redcolor);
	  HbarYSegLog->SetVisAttributes(redcolor);
	  HbarLog->SetVisAttributes(redcolor);
	  HbarLog->SetSensitiveDetector(scintiSD);
	}
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X       
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X

//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
void hbarCPTConst::DefineMaterialPropertiesBGO() {
//--------- read data from files ------------

  const double e = 1.602177*1e-19;
  const double h = 6.626070*1e-34;
  const double c = 299792458;
  G4double col1=0.0, col2=0.0;
  double col3=0.0, col4=0.0;
  std::ifstream bgo_fRead, refbgo_fRead, pmt_fRead, pmtgain_fRead, boros_fRead;
  
 /* bgo_fRead.open("data_scintil/bgo_emission_spec.dat", std::ios::in);
  if(bgo_fRead.bad())
    {
      G4cout << "*** Data file containing the emission spectrum of BGO cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!bgo_fRead.eof())
    {
      bgo_fRead >> col1 >> col2;

      bgo_PP.push_back( (h*c/(col1*1e-9)/e) *eV ); // wave length to energy
	//	std::cout << h*c/(col1*1e-9)/e << std::endl;
      bgo_FAST.push_back( col2);
      //rindex_galactic.push_back(1.0);
     // rindex_air.push_back(1.00027717);
     // rindex_bgo.push_back(2.15); //1.835
	 // rindex_tempax.push_back(1.471);
	 // rindex_kovar.push_back(1.471);
	  abs_length_carboncoat.push_back(1.0*nm);
	  rindex_carboncoat.push_back(1.8619);
	  rindexc_carboncoat.push_back(0.72877);
	  abs_length_bgo.push_back(10.0*cm); //1/0.92;
    }
  bgo_PP.pop_back();
  bgo_FAST.pop_back();  
  abs_length_bgo.pop_back();
  //rindex_bgo.pop_back();
  //rindex_tempax.pop_back();
 // rindex_kovar.pop_back();
  abs_length_carboncoat.pop_back();
  rindex_carboncoat.pop_back();
  rindexc_carboncoat.pop_back();*/
 bgo_fRead.open("data_scintil/bgo_emissionspec_refindex.dat", std::ios::in);
  if(bgo_fRead.bad())
    {
      G4cout << "*** Data file containing the emission spectrum of BGO cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!bgo_fRead.eof())
    {
      bgo_fRead >> col1 >> col2 >> col3;

      bgo_PP.push_back( (h*c/(col1*1e-9)/e) *eV ); // wave length to energy
	//	std::cout << h*c/(col1*1e-9)/e << std::endl;
      bgo_FAST.push_back( col2);
      //rindex_galactic.push_back(1.0);
     // rindex_air.push_back(1.00027717);
     // rindex_bgo.push_back(2.15); //1.835
	 // rindex_tempax.push_back(1.471);
	 // rindex_kovar.push_back(1.471);
	  refbgo_i.push_back(col3);
	  refbgo_img.push_back(1.0e-5);
	  abs_length_carboncoat.push_back(1.0*nm);
	  rindex_carboncoat.push_back(1.8619);
	  rindexc_carboncoat.push_back(0.72877);
	  abs_length_bgo.push_back(10.0*cm); //1/0.92;
    }

  refbgo_i.pop_back();
  refbgo_img.pop_back();
  bgo_PP.pop_back();
  bgo_FAST.pop_back();  
  abs_length_bgo.pop_back();
  //rindex_bgo.pop_back();
  //rindex_tempax.pop_back();
  //rindex_kovar.pop_back();
  abs_length_carboncoat.pop_back();
  rindex_carboncoat.pop_back();
  rindexc_carboncoat.pop_back();
//----------------------------------------------------------------------------------------------------
  pmt_fRead.open("data_scintil/pmt_quant_eff.dat", std::ios::in);
  if(pmt_fRead.bad())
    {
      G4cout << "*** Data file containing the quantum efficiency of the BGO PMTs cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!pmt_fRead.eof())
    {
      pmt_fRead >> col1 >> col2;

      photocath_PP.push_back( (h*c/(col1*1e-9)/e) *eV ); // wave length to energy
      photocath_EFF.push_back(col2);
	  photocath_ReR.push_back(2.5); //paper, refractive index bialkali photocathode
	  photocath_ImR.push_back(1.5);
    }
  photocath_PP.pop_back();
  photocath_EFF.pop_back();
  photocath_ReR.pop_back();
  photocath_ImR.pop_back();
//----------------------------------------------------------------------------------------------------
   pmtgain_fRead.open("data_scintil/gain_nagata.dat", std::ios::in);
   if(pmtgain_fRead.bad())
    {
      G4cout << "*** Data file containing the gain of the BGO PMTs cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!pmtgain_fRead.eof())
    {
      pmtgain_fRead >> col1 >> col2;
      pmtgain_x.push_back(col1); // supply voltage (V)
      pmtgain_y.push_back(col2); // gain
    }
  pmtgain_x.pop_back();
  pmtgain_y.pop_back();
//----------------------------------------------------------------------------------------------------
 /* refbgo_fRead.open("data_scintil/bgo_refractive_index.txt", std::ios::in);
   if(refbgo_fRead.bad())
    {
      G4cout << "*** Data file containing refrective index of the BGO cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!refbgo_fRead.eof())
    {
      refbgo_fRead >> col1 >> col2;
      refbgo_wl.push_back((h*c/(col1*1e-9)/e) *eV); // wavelegnth to eV
      refbgo_i.push_back(col2); 
	  refbgo_img.push_back(1.0e-5);
    }
  refbgo_wl.pop_back();
  refbgo_i.pop_back();
  refbgo_img.pop_back();*/                 
//----------------------------------------------------------------------------------------------------
boros_fRead.open("data_scintil/refractiveindex_borosilicate.dat", std::ios::in);
   if(boros_fRead.bad())
    {
      G4cout << "*** Data file containing refractive index of Borosilicate cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!boros_fRead.eof())
    {
      boros_fRead >> col1 >> col3 >> col4;
	  //col1*=1.0e+3*col1;
      rindex_x_boros.push_back((h*c/(col1*1e-6)/e) *eV); // wavelegnth to eV
      rindex_tempax.push_back(col4); 
	  rindexc_tempax.push_back(col3);
	  boro_abslength.push_back(479.46*cm);
    }
  rindex_x_boros.pop_back();
  rindexc_tempax.pop_back();
  rindex_tempax.pop_back();
  boro_abslength.pop_back();
 //----------------------------------------------------------------------------------------------------

  G4cout << "BGO emission spectrum and PMTs quantum efficiency successfully read from data file." << G4endl;
  bgo_fRead.close();
  pmt_fRead.close();
  pmtgain_fRead.close();
  refbgo_fRead.close();
  boros_fRead.close();

  G4MaterialPropertiesTable* bgo_MPT = new G4MaterialPropertiesTable();
  bgo_MPT->AddProperty("FASTCOMPONENT", &(bgo_PP[0]), &(bgo_FAST[0]), bgo_PP.size());
  bgo_MPT->AddConstProperty("SCINTILLATIONYIELD", 8200.0/MeV); // photons per MeV
  bgo_MPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
  bgo_MPT->AddConstProperty("FASTTIMECONSTANT",  300.*ns);
  bgo_MPT->AddConstProperty("YIELDRATIO", 1.0);
  bgo_MPT->AddProperty("RINDEX", &(bgo_PP[0]), &(refbgo_i[0]), bgo_PP.size());
  //bgo_MPT->AddProperty("IMAGINARYRINDEX", &(bgo_PP[0]), &(refbgo_img[0]), bgo_PP.size());
  // no significant selft absorption of scintillation light
  bgo_MPT->AddProperty("ABSLENGTH", &(bgo_PP[0]), &(abs_length_bgo[0]), bgo_PP.size());
  hbarDetConst::GetMaterial("BGO")->SetMaterialPropertiesTable(bgo_MPT);

  G4MaterialPropertiesTable* Tempax_MPT = new G4MaterialPropertiesTable();
  Tempax_MPT->AddProperty("RINDEX", &(rindex_x_boros[0]), &(rindex_tempax[0]), rindex_x_boros.size());
  //Tempax_MPT->AddProperty("IMAGINARYRINDEX", &(rindex_x_boros[0]), &(rindexc_tempax[0]), rindex_x_boros.size());
  Tempax_MPT->AddProperty("ABSLENGTH", &(rindex_x_boros[0]), &(boro_abslength[0]), rindex_x_boros.size());
  hbarDetConst::GetMaterial("Borosilicate")->SetMaterialPropertiesTable(Tempax_MPT);

  borosilicate_opsurf->SetMaterialPropertiesTable(Tempax_MPT);

  G4MaterialPropertiesTable* carboncoat_MPT = new G4MaterialPropertiesTable();
  coat_bgo_opsurf->SetMaterialPropertiesTable(carboncoat_MPT);
  carboncoat_MPT->AddProperty("ABSLENGTH", &(bgo_PP[0]), &(abs_length_bgo[0]), bgo_PP.size());
  carboncoat_MPT->AddProperty("REALRINDEX", &(bgo_PP[0]), &(rindex_carboncoat[0]), bgo_PP.size());
  carboncoat_MPT->AddProperty("IMAGINARYRINDEX", &(bgo_PP[0]), &(rindexc_carboncoat[0]), bgo_PP.size());
  hbarDetConst::GetMaterial("CarbonCoatmaterial")->SetMaterialPropertiesTable(carboncoat_MPT);

  G4MaterialPropertiesTable* photocath_mt = new G4MaterialPropertiesTable();
  photocath_mt->AddProperty("EFFICIENCY",&(photocath_PP[0]),&(photocath_EFF[0]),photocath_PP.size());
  photocath_mt->AddProperty("REALRINDEX",&(photocath_PP[0]),&(photocath_ReR[0]),photocath_PP.size());
  photocath_mt->AddProperty("IMAGINARYRINDEX",&(photocath_PP[0]),&(photocath_ImR[0]),photocath_PP.size());
  photocath_opsurf = new G4OpticalSurface("photocath_opsurf",glisur,polished,dielectric_metal);
  photocath_opsurf->SetMaterialPropertiesTable(photocath_mt);
  G4cout << "*** Reading optical properties of Hodor done!" << G4endl;
  G4cout << "*** Reading optical properties of BGO done!" << G4endl;

}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
double hbarCPTConst::CalculatePMTGain(double volts)
{
	if (volts<=0.0) return 0.0;

	std::vector<double> gain_x = pmtgain_x;
	std::vector<double> gain_y = pmtgain_y;

	std::vector<double>::iterator upperwl = std::upper_bound(pmtgain_x.begin(), pmtgain_x.end(), volts);
	std::vector<double>::iterator lowerwl = upperwl - 1;

	double pmtgain_lower = pmtgain_y[(lowerwl - pmtgain_x.begin())];
	double pmtgain_upper = pmtgain_y[(upperwl - pmtgain_x.begin())];

	// linear interpolation
	double ret_pmtgain = pmtgain_lower + (pmtgain_upper - pmtgain_lower)*(volts - *lowerwl)/(*upperwl - *lowerwl);

	return ret_pmtgain;
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
void hbarCPTConst::DefineMaterialPropertiesHodor()
{
  //--------- read data from files ------------

  const double e = 1.602177*1e-19;
  const double h = 6.626070*1e-34;
  const double c = 299792458;
  G4double col1, col2;
  std::ifstream scinti_fRead;

  scinti_fRead.open("data_scintil/bc408_plastic_scintillator.dat", std::ios::in);
  if(scinti_fRead.bad())
    {
      G4cout << "*** Data file containing the relative light output of scintillator cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be enabled!" << G4endl;
      return;
    }
  while(!scinti_fRead.eof())
    {
      scinti_fRead >> col1 >> col2;

      Scinti_PP.push_back( (h*c/(col1*1e-9)/e) *eV );
      Scinti_FAST.push_back( col2/100. );
      rindex_galactic.push_back(1.0);
      rindex_air.push_back(1.00027717);
      rindex_scinti.push_back(1.58);
	  rindex_plexi.push_back(1.491);
	  rindex_glue.push_back(1.56);
      absorption.push_back(380*cm);
    }
  Scinti_PP.pop_back();
  Scinti_FAST.pop_back();
  rindex_galactic.pop_back();
  rindex_air.pop_back();
  rindex_scinti.pop_back();
  rindex_plexi.pop_back();
  rindex_glue.pop_back();
  absorption.pop_back();
  G4cout << "Relative light output of scintillator successfully read from data file." << G4endl;
  scinti_fRead.close();

  std::ifstream rindex_real_fRead;
  std::ifstream rindex_imag_fRead;
  rindex_real_fRead.open("data_scintil/Al_n_real.dat", std::ios::in);
  rindex_imag_fRead.open("data_scintil/Al_n_imag.dat", std::ios::in);
  if(rindex_real_fRead.bad() || rindex_imag_fRead.bad())
    {
      G4cout << "*** One of the data files containing the aluminium refractive index cannot be opened." << G4endl;
      G4cout << "*** Optical physics will not be fully enabled!" << G4endl;
      return;
    }
  while( !rindex_real_fRead.eof() && !rindex_imag_fRead.eof() )
    {
      rindex_real_fRead >> col1 >> col2;
      Al_real_PP.push_back(col1 *eV);
      Al_real.push_back(col2);

      rindex_imag_fRead >> col1 >> col2;
      Al_imag_PP.push_back(col1 *eV);
      Al_imag.push_back(col2);
    }
  Al_real_PP.pop_back();
  Al_real.pop_back();
  Al_imag_PP.pop_back();
  Al_imag.pop_back();

  //  rindex_fRead.close();
  rindex_real_fRead.close();
  rindex_imag_fRead.close();

  G4MaterialPropertiesTable* galactic_MPT = new G4MaterialPropertiesTable();
  galactic_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_galactic[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("Galactic")->SetMaterialPropertiesTable(galactic_MPT);

  G4MaterialPropertiesTable* air_MPT = new G4MaterialPropertiesTable();
  air_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_air[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("Air")->SetMaterialPropertiesTable(air_MPT);

  G4MaterialPropertiesTable* Scinti_MPT = new G4MaterialPropertiesTable();
  Scinti_MPT->AddProperty("FASTCOMPONENT", &(Scinti_PP[0]), &(Scinti_FAST[0]), Scinti_PP.size());
  Scinti_MPT->AddConstProperty("SCINTILLATIONYIELD", 10000./MeV);
  Scinti_MPT->AddConstProperty("RESOLUTIONSCALE", 1.0);
  Scinti_MPT->AddConstProperty("FASTTIMECONSTANT",  1.*ns);
  //  Scinti_MPT->AddConstProperty("YIELDRATIO", 0.64);
  Scinti_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_scinti[0]), Scinti_PP.size());
  Scinti_MPT->AddProperty("ABSLENGTH", &(Scinti_PP[0]), &(absorption[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("Scintillator")->SetMaterialPropertiesTable(Scinti_MPT);

  G4MaterialPropertiesTable* Lightguide_MPT = new G4MaterialPropertiesTable();
  Lightguide_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_plexi[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("Plexiglas")->SetMaterialPropertiesTable(Lightguide_MPT);

  G4MaterialPropertiesTable* Glue_MPT = new G4MaterialPropertiesTable();
  Glue_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_glue[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("Epoxy_Glue")->SetMaterialPropertiesTable(Glue_MPT);

  G4MaterialPropertiesTable* SiPMMaterial_MPT = new G4MaterialPropertiesTable();
  SiPMMaterial_MPT->AddProperty("RINDEX", &(Scinti_PP[0]), &(rindex_scinti[0]), Scinti_PP.size());
  hbarDetConst::GetMaterial("SiPM")->SetMaterialPropertiesTable(SiPMMaterial_MPT);
}
//X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X_X
void hbarCPTConst::ReadResponseFunctionSiPM() {
//read response fkt //////////////////////
	ifstream inFile("data_scintil/response_fkt_binwidth02ns_4096.dat");
	int numberoflines=4096;			
	int lcount = 0;
	double timetemp=0, sigtemp=0;
	const char* histo_name2 = "histo_resp";

	while(!inFile.eof() && lcount != numberoflines) {
		inFile >> timetemp >> sigtemp; // >> comptemp;
		responsefkt_x.push_back(timetemp);
		responsefkt_y.push_back(sigtemp); //V
		lcount++; 	
	}   

	histo_responsefkt = new TH1D(histo_name2, histo_name2, numberoflines, 0, numberoflines*0.2);
	for(int j=0; j<numberoflines; j++) {
		histo_responsefkt->SetBinContent(j, responsefkt_y[j]);
	}
}





void hbarCPTConst::UseCalorimeter(G4int num)
{

  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  Scintipt = num-1;
	  useScinti[Scintipt] = true;
	}
  else if (num == 0)
	{
	  AddCalorimeter();
	}
  else
	{
	  G4cout << "Calorimeter number " << num << " is out of range!" << G4endl;
	}
}

void hbarCPTConst::AddCalorimeter()
{
  if (Scintipt < maxScN-1)
	{
	  Scintipt++;
	  useScinti[Scintipt] = true;
	}
  else
	{
	  G4cout << "Cannot create calorimeter! There are too many of them." << G4endl;
	}
}

void hbarCPTConst::SetCalorimeterCenter(G4int num, G4double xval, G4double yval, G4double zval)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiPos[num-1] = G4ThreeVector(xval, yval, zval);
	}
}

void hbarCPTConst::SetCalorimeterAngle(G4int num, G4double phival, G4double thetaval, G4double psival)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiRot[num-1] = G4RotationMatrix(phival, thetaval, psival);
	}
}

void hbarCPTConst::SetCalorimeterThickness(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiThick[num-1] = val;
	}
}

void hbarCPTConst::SetCalorimeterLength(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiLength[num-1] = val;
	}
}

void hbarCPTConst::SetCalorimeterWidth(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiWidth[num-1] = val;
	}
}

void hbarCPTConst::SetCalorimeterLargerWidth(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiWidthL[num-1] = val;
	}
}

void hbarCPTConst::SetCalorimeterCoincidence(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  ScintiCoin[num-1] = val;
	}
}

void hbarCPTConst::SetLeadThickness(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  LeadThick[num-1] = val;
	}
}

void hbarCPTConst::SetNumberOfLeadLayers(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  NumOfLeadLayers[num-1] = val;
	}
}

void hbarCPTConst::SetNumberOfCalorimeterSides(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  NumOfScintiSides[num-1] = val;
	}
}


void hbarCPTConst::UseVeto(G4int num)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  Vetopt = num-1;
	  useVeto[Vetopt] = true;
	} else if (num == 0)
	{
	  AddVeto();
	} else {
    G4cout << "Veto number " << num << " is out of range!" << G4endl;
  }
}

void hbarCPTConst::AddVeto()
{
  if (Vetopt < maxScN-1)
	{
	  Vetopt++;
	  useVeto[Vetopt] = true;
	}
  else
	{
	  throw hbarException("Too many veto counters, cannot create more.");
	}
}

void hbarCPTConst::SetVetoCenter(G4int num, G4double xval, G4double yval, G4double zval)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  VetoPos[num-1] = G4ThreeVector(xval, yval, zval);
	}
}

void hbarCPTConst::SetVetoAngle(G4int num, G4double phival, G4double thetaval, G4double psival)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  VetoRot[num-1] = G4RotationMatrix(phival, thetaval, psival);
	}
}

void hbarCPTConst::SetVetoLength(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  VetoLength[num-1] = val;
	}
}

void hbarCPTConst::SetVetoInRad(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  VetoInRad[num-1] = val;
	}
}

void hbarCPTConst::SetVetoOutRad(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  VetoOutRad[num-1] = val;
	}
}

void hbarCPTConst::SetVetoSegment(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  vetodiv[num-1] = val;
	}
}

// Hodoscope
void hbarCPTConst::UseHodoscope(G4int num)
{

  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  Hodoscopept = num-1;
	  useHodoscope[Hodoscopept] = true;
	}
  else if (num == 0)
	{
	  AddHodoscope();
	}
  else
	{
	  char buffer[400];
	  sprintf(buffer, "Hodoscope number %d is out of range.",num);
	  throw hbarException(buffer);
	}
}

void hbarCPTConst::AddHodoscope()
{
  if (Hodoscopept < maxScN-1)
	{
	  Hodoscopept++;
	  useHodoscope[Hodoscopept] = true;
	}
  else
	{
	  throw hbarException("Cannot create Hodoscope! There are too many of them.");
	}
}

void hbarCPTConst::SetHodoscopeCenter(G4int num, G4double xval, G4double yval, G4double zval)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoscopePos[num-1] = G4ThreeVector(xval, yval, zval);
	}
}

void hbarCPTConst::SetHodoscopeAngle(G4int num, G4double phival, G4double thetaval, G4double psival)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoscopeRot[num-1] = G4RotationMatrix(phival, thetaval, psival);
	}
}

void hbarCPTConst::SetHodoscopeLength(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoscopeLength[num-1] = val;
	}
}

void hbarCPTConst::SetHodoscopeInRad(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoscopeInRad[num-1] = val;
	}
}

void hbarCPTConst::SetHodoscopeOutRad(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoscopeOutRad[num-1] = val;
	}
}

void hbarCPTConst::SetHodoscopeSegment(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  Hododiv[num-1] = val;
	}
}

void hbarCPTConst::SetHodoScintWidth(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoScintWidth[num-1] = val;
	}
}

void hbarCPTConst::SetHodoScintHeight(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxScN))
	{
	  HodoScintHeight[num-1] = val;
	}
}

void hbarCPTConst::UseBGO2014(G4int num)
{

  if ((num-1 >= 0) && (num-1 < maxBGO2014))
	{
	  BGO2014pt = num-1;
	  useBGO2014[BGO2014pt] = true;
	}
  else if (num == 0)
	{
	  AddBGO2014();
	}
  else
	{
	  char buffer[400] = "";
	  sprintf(buffer, "BGO2014 number %d is out of range.",num);
	  throw hbarException(buffer);
	}
}

void hbarCPTConst::AddBGO2014()
{
  if (BGO2014pt < maxBGO2014-1)
	{
	  BGO2014pt++;
	  useBGO2014[BGO2014pt] = true;
	}
  else
	{
	  throw hbarException("Cannot create BGO2014 Scintillator! There are too many of them.");
	}
}

void hbarCPTConst::SetBGOScint2014Start(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBGO2014))
	{
	  BGOScint2014Start[num-1] = val;
	}
}

void hbarCPTConst::SetBGOScint2014End(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBGO2014))
	{
	  BGOScint2014End[num-1] = val;
	}
}

void hbarCPTConst::SetBGOScint2014Diam(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBGO2014))
	{
	  BGOScint2014Diam[num-1] = val;
	}
}

void hbarCPTConst::SetHbarDetCenter(G4double xval, G4double yval, G4double zval)
{
  HbarPos = G4ThreeVector(xval, yval, zval);
}

void hbarCPTConst::SetHbarDetAngle(G4double phi, G4double theta, G4double psi)
{
  HbarRot = G4RotationMatrix(phi, theta, psi);
}

void hbarCPTConst::SetHbarDetSegment(G4int xsegval, G4int ysegval)
{
  HbarXseg = xsegval;
  HbarYseg = ysegval;
}

void hbarCPTConst::SetHbarDetDimension(G4double xlen, G4double ylen, G4double zlen)
{
  HbarDetX = xlen;
  HbarDetY = ylen;
  HbarDetZ = zlen;
}

void hbarCPTConst::SetFoilMaterial (G4String foilname) {

    FoilMat = foilname;
}


