#include "hbarCUSPField.hh"

#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"

#include "G4EqEMFieldWithSpin.hh"
#include "hbarEqSextupoleField.hh"
#include "G4ChordFinder.hh"
#include "G4PropagatorInField.hh"

#include "G4MagIntegratorStepper.hh"
#include "G4ClassicalRK4.hh"
#include "fieldmap.hh"

#include <string>
#include <map>
#include <iomanip>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarCUSPField::hbarCUSPField() : G4ElectroMagneticField()
{

  // G4cout << "In hbarCUSPField::hbarCUSPField constructor"  << G4endl;
  // G4cout << "Elfilename: " << Elfilename << G4endl;
  // G4cout << "Magfilename: " << Magfilename << G4endl;

  // fEmatrix = new Matrix<std::pair<double, double> >(1000,1000);
  // fBmatrix = new Matrix<std::pair<double, double> >(1001,1001);

  fEmatrix = NULL;
  fBmatrix = NULL;

  fZAxisOffset = 0.0*m;

  felectric_field = new std::vector<double>(3);
  fmagnetic_field = new std::vector<double>(3);

  // G4cout << "fEmatrix: " << fEmatrix << G4endl;
  // G4cout << "fEmatrix(500, 500): " << ((*fEmatrix)(500, 500)).first << G4endl;
  // G4cout << "fBmatrix(500, 500): " << ((*fBmatrix)(500, 500)).first << G4endl;

  //  fEquation = new G4EqEMFieldWithSpin(this);
  fEquation = new hbarEqSextupoleField(this);

  //  fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fieldMgr = new G4FieldManager(this);

  fieldMgr->SetDetectorField(this);

  fStepper = new G4ClassicalRK4(fEquation,12);

  G4double minStep           = 0.001*mm;

  fChordFinder = new G4ChordFinder((G4MagneticField*)this,minStep,fStepper);
 
  // Set accuracy parameters
  G4double deltaChord        = 0.1*mm;
  fChordFinder->SetDeltaChord( deltaChord );

  G4double deltaOneStep      = 0.01*mm; 
  fieldMgr->SetAccuraciesWithDeltaOneStep(deltaOneStep);

  G4double deltaIntersection = 0.1*mm; 
  fieldMgr->SetDeltaIntersection(deltaIntersection);

  G4TransportationManager* fTransportManager =
    G4TransportationManager::GetTransportationManager();

  fieldPropagator = fTransportManager->GetPropagatorInField();
  fieldPropagator->SetMaxLoopCount(5000);
  //  G4cout << "FieldPropagator max loop count: " << fieldPropagator->GetMaxLoopCount() << G4endl;

  G4double epsMin            = 2.5e-7*mm;
  G4double epsMax            = 0.05*mm;
 
  fieldPropagator->SetMinimumEpsilonStep(epsMin);
  fieldPropagator->SetMaximumEpsilonStep(epsMax);
 
  fieldMgr->SetChordFinder(fChordFinder);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

hbarCUSPField::~hbarCUSPField()
{
  if (fEquation)    delete fEquation;
  if (fStepper)     delete fStepper;
  if (fChordFinder) delete fChordFinder;
  if (fEmatrix) delete fEmatrix;
  if (fBmatrix) delete fBmatrix;
  if (fieldMgr) delete fieldMgr;

  if (felectric_field) delete felectric_field;
  if (fmagnetic_field) delete fmagnetic_field;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void hbarCUSPField::Initialize(const char * Elfilename, const char * Magfilename){

  G4cout << "Reading CUSP electric field map from ASCII file" << G4endl;
  ReadElectricFieldASCII(Elfilename);
  G4cout << "Reading CUSP magnetic field map from ASCII file" << G4endl;
  ReadMagneticFieldASCII(Magfilename);
  fEz_min = get_Ez_min();
  fEz_max = get_Ez_max();
  fEz_step = get_Ez_step();
  fEr_min = get_Er_min();
  fEr_max = get_Er_max();
  fEr_step = ger_Er_step();
  
  fBz_min = get_Bz_min();
  fBz_max = get_Bz_max();
  fBz_step = get_Bz_step();
  fBr_min = get_Br_min();
  fBr_max = get_Br_max();
  fBr_step = ger_Br_step();



}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void hbarCUSPField::GetFieldValue( const G4double Point[3],G4double* Bfield ) const
{
  //  G4cout << "In hbarCUSPField::GetFieldValue" << G4endl;
 
  double lenUnit = m;

  double x = Point[0]/lenUnit;
  double y = Point[1]/lenUnit;
  double z = (Point[2] - fZAxisOffset)/lenUnit;

  //  G4cout << "x,y,z: " << x << ", " << y << ", " << z << " m" << G4endl;
  //G4cout << "x,y,z: " << Point[0]/m << ", " << Point[1]/m << ", " << Point[2]/m << " m" << G4endl;

  std::vector<double> ef = getElectr(x,y,z);
  std::vector<double> bf = getMagn(x,y,z);

  Bfield[0] = bf[0]*tesla;      // Bfields xaxis
  Bfield[1] = bf[1]*tesla;      // Bfields yaxis
  Bfield[2] = bf[2]*tesla;      // Bfields zaxis

  Bfield[3] = ef[0]*(volt/m);   // Efields xaxis
  Bfield[4] = ef[1]*(volt/m);   // Efields yaxis
  Bfield[5] = ef[2]*(volt/m);   // Efields zaxis



  return;
}

std::vector<double> hbarCUSPField::getElectr(const double& _x,const double& _y,const double& _z) const{
  std::pair<double, double> E12,E22,E11,E21, Ezr;
  // z offset
  double __z = _z;
  //pool things
  double z_index,r_index;
  //  double z1,z2,r1,r2;
  int z1_index,z2_index,r1_index,r2_index;
  double r;

  //Interpolate potentials on r,z direction and calculate stuff
  r=sqrt(_x*_x+_y*_y);if(r == 0)r=1e-10; //otherwise division by zero!     
  //check of ze wel in of bound zitten he!
  //  G4cout << "Ez_min: " << fEz_min << "Er_max: " << fEr_max << ", Ez_max: " << fEz_max << G4endl;
  if(_x>fEr_max||_y>fEr_max||r>fEr_max ||__z>fEz_max||__z<(fEz_min+fEr_step)){
    //    G4cout<<"E particle out of bound: "<<_x<<" "<<_y<<" "<<__z<<G4endl;
    (*felectric_field)[0] = (*felectric_field)[1] = (*felectric_field)[2] = 0;
    return *felectric_field;
  }
  //scale both particles
  z_index= __z-fEz_min; 
  r_index= r-fEr_min;
  r1_index=fabs(floor(r_index/fEr_step));
  r2_index=r1_index+1;
  z1_index=floor(z_index/fEz_step);
  z2_index=z1_index+1;
  
  //matrix:first row(z), then column(r)
  
  E12=(*fEmatrix)(z2_index,r1_index);
  E22=(*fEmatrix)(z2_index,r2_index);
  E11=(*fEmatrix)(z1_index,r1_index);
  E21=(*fEmatrix)(z1_index,r2_index);

  (*felectric_field)[0] = E12.second*(_x/r);
  (*felectric_field)[1] = E12.second*(_y/r);
  (*felectric_field)[2] = E12.first;

  return *felectric_field;
}

std::vector<double> hbarCUSPField::getMagn(const double& _x,const double& _y,const double& _z) const{
  //Interpolate potentials on r,z direction and calculate stuff
  //Currently works only if r>0 and z>0.
  std::pair<double, double> B12,B22,B11,B21, Bzr;
  // z offset
  double __z = _z;
  //pool things
  double z_index,r_index;
  //  double z1,z2,r1,r2;
  int z1_index,z2_index,r1_index,r2_index;
  double r;
  r=sqrt(_x*_x+_y*_y);if(r == 0)r=1e-10; //otherwise division by zero!     
  //check of ze wel in of bound zitten he!
  if(_x>fBr_max||_y>fBr_max||r>fBr_max||__z>std::abs(fBz_max)||__z<(fBz_min+fBz_step)){

    //G4cout<<"B particle out of bound: "<<_x<<" "<<_y<<" "<<__z<<G4endl;

       // G4cout<<"B particle out of bound: "<<_x<<" "<<_y<<" "<<__z<<G4endl;

    (*fmagnetic_field)[0] = (*fmagnetic_field)[1] = (*fmagnetic_field)[2] = 0;
    return *fmagnetic_field;      
  }
  //scale both particles
  z_index= __z-fBz_min; 
  r_index= r-fBr_min;
  r1_index=fabs(floor(r_index/fBr_step));
  r2_index=r1_index+1;
  z1_index=floor(z_index/fBz_step);
  z2_index=z1_index+1;//ceil(z_index/fBz_step);

  B12=(*fBmatrix)(z2_index,r1_index);
  B22=(*fBmatrix)(z2_index,r2_index);
  B11=(*fBmatrix)(z1_index,r1_index);
  B21=(*fBmatrix)(z1_index,r2_index);
  

  (*fmagnetic_field)[0] = B12.second*(_x/r);
  (*fmagnetic_field)[1] = B12.second*(_y/r);
  (*fmagnetic_field)[2] = B12.first;


  return *fmagnetic_field;
}

void hbarCUSPField::ReadElectricFieldASCII(const char * filename){
  fEmatrix = ReadElectricFieldAscii(filename);
}

void hbarCUSPField::ReadMagneticFieldASCII(const char * filename){
  fBmatrix = ReadMagneticFieldAscii(filename);
}


// Bilinear interpolation 
// http://en.wikipedia.org/wiki/Bilinear_interpolation
std::vector<double> hbarCUSPField::interpolateBili(std::pair<double,double>& Q11, std::pair<double,double>& Q12, std::pair<double,double>& Q21, std::pair<double,double>& Q22, double& r1, double& r2, double& z1, double& z2, double& r, const double& z,const double& x, const double& y) const{
  // z - first
  // r - second

  double fieldz = (Q11.first*(r2-r)*(z2-z) +
		   Q21.first*(r-r1)*(z2-z) +
		   Q12.first*(r2-r)*(z-z1) +
		   Q22.first*(r-r1)*(z-z1)) * (1.0/((r2-r1)*(z2-z1)));

  double fieldr = (Q11.second*(r2-r)*(z2-z) +
		   Q21.second*(r-r1)*(z2-z) +
		   Q12.second*(r2-r)*(z-z1) +
		   Q22.second*(r-r1)*(z-z1)) * (1.0/((r2-r1)*(z2-z1)));
  
  double fieldx = fieldr*(x/r);
  double fieldy = fieldr*(y/r);

  std::vector<double> field;
  field.push_back(fieldx);
  field.push_back(fieldy);
  field.push_back(fieldz);
  return field;
}


void hbarCUSPField::CaluculateGradient(const G4double Point[3], G4double *grad){

  // gradient has to be normed to mm steps! therefore in the end divide by m
  const G4double delta = 0.1*mm;
  double lenUnit = m;
  const G4double deltaDist = delta/lenUnit;

  double x = Point[0]/lenUnit;
  double y = Point[1]/lenUnit;
  double z = (Point[2] - fZAxisOffset)/lenUnit;

  // mag gradient x direction
  std::vector<double> p1 = getMagn(x+deltaDist,y,z);
  std::vector<double> p2 = getMagn(x-deltaDist,y,z);
  grad[0] =  ( sqrt( p1[0]*tesla*p1[0]*tesla + p1[1]*tesla*p1[1]*tesla 
		  + p1[2]*tesla*p1[2]*tesla) 
	     -sqrt( p2[0]*tesla*p2[0]*tesla + p2[1]*tesla*p2[1]*tesla
		  + p2[2]*tesla*p2[2]*tesla) )/(2.0*deltaDist);
  
  // mag gradient y direction
  p1 = getMagn(x,y+deltaDist,z);
  p2 = getMagn(x,y-deltaDist,z);
  grad[1] =  ( sqrt(  p1[0]*tesla*p1[0]*tesla + p1[1]*tesla*p1[1]*tesla 
	 	   + p1[2]*tesla*p1[2]*tesla) 
	     -sqrt(  p2[0]*tesla*p2[0]*tesla + p2[1]*tesla*p2[1]*tesla
		   + p2[2]*tesla*p2[2]*tesla) )/(2.0*deltaDist); 
  
  // mag gradient z direction
  p1 = getMagn(x,y,z+deltaDist);
  p2 = getMagn(x,y,z-deltaDist);
  grad[2] = ( sqrt(  p1[0]*tesla*p1[0]*tesla + p1[1]*tesla*p1[1]*tesla 
		   + p1[2]*tesla*p1[2]*tesla) 
	     -sqrt(  p2[0]*tesla*p2[0]*tesla + p2[1]*tesla*p2[1]*tesla
		   + p2[2]*tesla*p2[2]*tesla) )/(2.0*deltaDist);

  grad[0] /= lenUnit;
  grad[1] /= lenUnit;
  grad[2] /= lenUnit;


  
}
