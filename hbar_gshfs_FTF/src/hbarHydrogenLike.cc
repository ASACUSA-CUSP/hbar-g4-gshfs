
#include "hbarHydrogenLike.hh"


//See the documentation for definition of G4ParticleDefinition constructor.
hbarHydrogenLike::hbarHydrogenLike(const G4String& aName)
  :G4ParticleDefinition(
			aName,  938.783*MeV,       0.0*MeV,  0.0, 
			0,               0,             0,         
			0,               0,             0,             
			"baryon",         0,            0,       0,
			true,            -1.0,          NULL,
			false,       "nucleon", 0, 99        ),
   rfacDecayableObject(&GetU01Random)
{
  decayEnabled = true;
  levelSplittingEnabled = true;
}

hbarHydrogenLike::~hbarHydrogenLike()
{
}

bool hbarHydrogenLike::GetSplittingEnabled()
{
  return levelSplittingEnabled;
}

void hbarHydrogenLike::SetSplittingEnabled(bool val)
{
  levelSplittingEnabled = val;
}

bool hbarHydrogenLike::GetDecayEnabled()
{
  return decayEnabled;
}

void hbarHydrogenLike::SetDecayEnabled(bool val)
{
  decayEnabled = val;
}

void hbarHydrogenLike::StepTime(double timeStep)
{
  if(decayEnabled)
    {
      DoStepTime(timeStep);
    }
}

G4double hbarHydrogenLike::GetMuPrefix() const
{
  if(IsWeakFieldRegime())
	{
	  G4double mj = GetTwoMJ()/2;
	  G4double gFactor = GetgFactor();
	  return mj * gFactor;
	}
  else if(IsStrongFieldRegime())
	{
	  G4double ml = GetML();
	  G4double twoms = GetTwoMS();
	  return (ml + twoms);
	}
  throw RLException("Internal error 77.");    
}

G4double hbarHydrogenLike::GetgFactor() const
{
  G4double GL = 1;
  G4double GS = 2.002319;
  G4double j = ((G4double)GetTwoJ())/2.;
  G4double l = GetL();
  G4double s = abs(((G4double)GetTwoMJ())/2.) - l;
  G4double gj = 
    GL*(j*(j+1)-s*(s+1)+l*(l+1))/(2*j*(j+1)) +
    GS*(j*(j-1)+s*(s+1)-l*(l+1))/(2*j*(j+1));
  return gj;
}

G4double hbarHydrogenLike::GetMu(G4double Field) 
{
  G4double BohrMagneton = 5.7884e-11*MeV/tesla; // The Bohr Magneton constant
  G4double mu = BohrMagneton;
  
  if (GetM() == 0 && GetN() == 1) //Apparently, we neglect hyperfine interaction for M>0 (and obviously if not in GS).
    {
	  G4double GS_HFS_split = 9.41172e-25*joule; // Groundstate hyperfinesplitting constant, calculated from the "21 cm line".

      //These formulas derived from eq. 47.9 in the Bethe-Salpeter book, the so-called Breit-Rabi formula.
	  //Better, though is the Rabi paper http://link.aps.org/doi/10.1103/PhysRev.49.324
	  //What we do is to calculate the _effective_ magnetic moment.
	  //Note that the Breit-Rabi equation is also valid for GetM() != 1, but it reduces to the
	  //Bohr magneton in those cases, so we don't need to invoke it.
      G4double x = 2.0*BohrMagneton*Field/GS_HFS_split;
	  mu = x/sqrt(sqr(x) + 1)*BohrMagneton;
  
	  //Effectively: Set the direction of the force acting on the atom due to the Zeeman effect (that's the method calling this one).
    }
  if(GetN() == 1)
	{
	  G4double direction = 1.0;
	  if (GetF() == 1 && ((GetM() == 0) || GetM() == -1))
		direction = -1.0;
	  return direction * mu;
	}


  if(levelSplittingEnabled)
	{
	  mu *= GetMuPrefix();
	}

  return mu;
}
