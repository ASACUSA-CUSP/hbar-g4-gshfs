#include "hbarRunAction.hh"
#include "hbarRunMess.hh"
#include "hbarDetConst.hh"
#include "hbarFieldSetup.hh"
#include "hbarSextupoleField.hh"
#include "hbarField.hh"
#include "hbarMWSetup.hh"
#include "numtostr.hh"
#include "hbarPrimGenAction.hh"
#include "hbarDetectorHit.hh"

#include "G4Run.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"

#include "TObjString.h"
#include "TH1.h"
#include "TROOT.h"

hbarRunAction::hbarRunAction(hbarDetConst* aDet) :
  angleDiffDist(NULL), angleDiffDistAll(NULL), veloDist(NULL), 
  veloDistDet(NULL), angleDistDet(NULL), cavityEntranceQuantumNumber(NULL), detectorCavityQuantumNumber(NULL), rootfile(NULL),
  HodorWaveformFileName("rootfiles/Hodor_File.root"), HodorFile(NULL), 
  BGOPMTFileName("rootfiles/BGOPMT_File.root"), BGOPMTFile(NULL),// BGOHitCounter(0),
  fileName("rootfiles/g4.root"), detConst(aDet), hitCounter(0),
  exitAngle(-9.9), entranceAngle(-9.9), velocity(-9.9), velocityZ(-9.9),
  posX_Cavity(0.0), posY_Cavity(0.0),
  enterTime_Cavity(0.0), exitTime_Cavity(0.0),
  isVerboseStepping(false), isVerboseDecay(false), isVerboseTypeTransitions(false), quantumNumberAtCavity(0), quantumNumberAtDetector(0),
  primGenAction(NULL)
{
//   theHit;
   runMessenger = new hbarRunMessenger(this);
}


void hbarRunAction::SetVerboseStepping(G4bool val)
{
  isVerboseStepping = val;
}

G4bool hbarRunAction::GetVerboseStepping()
{
  return isVerboseStepping;
}

G4bool hbarRunAction::GetVerboseTypeTransitions()
{
  return isVerboseTypeTransitions;
}

void hbarRunAction::SetVerboseTypeTransitions(G4bool val)
{
  isVerboseTypeTransitions = val;
}

void hbarRunAction::SetVerboseDecay(G4bool val)
{
  isVerboseDecay = val;
}

G4bool hbarRunAction::GetVerboseDecay()
{
  return isVerboseDecay;
}

hbarRunAction::~hbarRunAction()
{  
  delete runMessenger;  
}

void hbarRunAction::BeginOfRunAction(const G4Run* aRun)
{
  if (!primGenAction) 
	{
	  std::cerr << "No hbarPrimGenAction object is connected to the hbarRunAction object!" << std::endl
				<< "Use hbarRunAction::ConnectPrimGenAction()" << std::endl
				<< "Aborting run." << std::endl;
	  exit(-1);
	}
  ResetHitCounter();
 // BGOHitCounter = 0;

  primGenAction->ResetNumberOfParticles();
  if (primGenAction->IsResetTimer()) 
	primGenAction->ResetTimer();
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  
  // Output ROOT file
  rootfile = new TFile(fileName, "RECREATE");
  rootfile->cd();

  // Histogram of the difference between the spin angles of an Hbar atom at the exit of sextupole #1 and at the entrance of sextupole #2
  // Only those Hbar atoms are added which hit the dummy Hbar detector 
  angleDiffDist = new TH1I("AngleDiffHist", "Distribution of angle difference", 100, -2.0*M_PI, 2.0*M_PI);

  // Same as above, except that all Hbar atoms are added
  angleDiffDistAll = new TH1I("AngleDiffHistAll", "Distribution of angle difference - all", 100, -2.0*M_PI, 2.0*M_PI);

  // Histogram of the Hbar velocity distribution in the RF cavity
  // Only those Hbar atoms are added which hit the dummy Hbar detector 
  veloDist = new TH1I("VelocityDist", "Distribution of velocity in cavity", 800, 0.0, 4000.0);

  // Added by Clemens -- start
  veloDistDet = new TH1F("VelocityDistDet", "Distribution of velocity when reaching the detector", 800, 0.0, 4000.0); //all particles that reach the detector
  angleDistDet = new TH1F("AngleDiffHistDet", "Distribution of impact angle", 1000, -2.0*M_PI, 2.0*M_PI);; //all particles that reach the detector
  // Added by Clemens -- start

  cavityEntranceQuantumNumber = new TH1I("CavityEntranceQuantumNumber", "Principal quantum number at cavity entrance", 35.0,-4.0,30.0);
  detectorCavityQuantumNumber = new TH2I("DetectorCavityQuantumNumber", "Principal quantum number at the detector AND at the cavity", 35.0, -4.0, 30.0, 35.0, -4.0, 30.0);

  // Ntuple of the Hbar stopping (and thus annihilation) positions
  stopNt = new TNtuple("stopNt", "Hbar stopping distribution", "x:y:z");
  
  dummydetNt = new TNtuple("dummydetNt", "dummy detector hits", "x:y:z:v:theta:phi:n:j:mj:F:mF");
  
  
  exitAngle = 0.0;
  entranceAngle = 0.0;
  velocity = 0.0;
  velocityZ = 0.0;
  posX_Cavity = 0.0;
  posY_Cavity = 0.0;
  enterTime_Cavity = 0.0;
  exitTime_Cavity = 0.0;

  // ROOT tree with the hits in the sensitive detectors (i.e. scintillators)
  hitTree = new TTree("hitTree","Hits in the detectors");
  hitTree->Branch("event", &theHit.Event, "event/I:hit");
  hitTree->Branch("energy", &theHit.Edep, "Edep/D:Ekin");
  hitTree->Branch("time", &theHit.Time, "time/D");
  hitTree->Branch("position", &theHit.PosX, "x/D:y:z");
  hitTree->Branch("momentumdir", &theHit.MomX, "x/D:y:z"); // Added by Clemens
  hitTree->Branch("particle", theHit.Particle, "particle[50]/C");
  

  hitTree->Branch("PDG_code", &theHit.PDGcode, "PDG_code/I");
  hitTree->Branch("detector", theHit.Detector, "detector[50]/C");
  // added by tajima -- start
  hitTree->Branch("track_id",&theHit.track_id, "track_id/I");
  hitTree->Branch("parent_id",&theHit.parent_id, "parent_id/I");
  // added by tajima -- stop
  hitTree->SetDirectory(0);


  if(detConst->GetCPTConst()->GetUseSensitiveSiPMs()) {
		hbarCPTConst* localcptconst = detConst->GetCPTConst();
				
		HodorFile = new TFile(HodorWaveformFileName, "RECREATE");
		HodorFile->cd();
		HodorTree = new TTree("HodorTree", "Waveforms of SiPMs");

		G4String sipmname;
		G4String sipmname2;

		for(int i=0; i<2; i++) {
			for(int j=0;j<32; j++) {
				sipmname = localcptconst->Get_SiPM_SD_up(i,j)->GetSiPMName();
				sipmname2 = sipmname + "[1024]/D";
				HodorTree->Branch(sipmname, (localcptconst->Get_SiPM_SD_up(i,j)->GetHodorWaveform()), sipmname2);

				sipmname = localcptconst->Get_SiPM_SD_down(i,j)->GetSiPMName();
				sipmname2 = sipmname + "[1024]/D";
				HodorTree->Branch(sipmname, (localcptconst->Get_SiPM_SD_down(i,j)->GetHodorWaveform()), sipmname2);
			}
		}

		HodorTree->SetDirectory(0);
  }

  if(detConst->GetCPTConst()->GetUseSensitiveBGOPMTs()) {
		hbarCPTConst* localcptconst = detConst->GetCPTConst();
			//std::cout << "++++++++++++++++++++++++++++++++++ runaction create+connect " << std::endl;
				
		BGOPMTFile = new TFile(BGOPMTFileName, "RECREATE");
		BGOPMTFile->cd();
		BGOPMTTree = new TTree("BGOPMTTree", "BGOPMT Energies in eV");

		G4String pmtname;
		G4String pmtname2;

		//std::vector<double> pmtA, pmtB, pmtC, pmtD;

		for(int i=0; i<4; i++) {
			for(int j=0;j<64; j++) {
				pmtname = localcptconst->Get_BGOPMT_SD(i,j)->GetBGOPMTName();
				pmtname2 = pmtname + "/D";
				BGOPMTTree->Branch(pmtname, (localcptconst->Get_BGOPMT_SD(i,j)->GetTotalEnergy()), pmtname2);
			}
		}

		//std::cout << "********************************** " << pmtA.size() << " " << pmtB.size() << " " << pmtC.size() << " " << pmtD.size() << std::endl;


		
	/*	pmtname = "PMT_A";
		pmtname2 = pmtname + "[64]/D";
		BGOPMTTree->Branch(pmtname, &pmtA[0], pmtname2);
		pmtname = "PMT_B";
		pmtname2 = pmtname + "[64]/D";
		BGOPMTTree->Branch(pmtname, &pmtB[0], pmtname2);
		pmtname = "PMT_C";
		pmtname2 = pmtname + "[64]/D";
		BGOPMTTree->Branch(pmtname, &pmtC[0], pmtname2);
		pmtname = "PMT_D";
		pmtname2 = pmtname + "[64]/D";
		BGOPMTTree->Branch(pmtname, &pmtD[0], pmtname2);
		*/


		//BGOPMTTree->Branch("event", &theHit.Event, "event/I:hit");
		//BGOPMTTree->Branch("energy", &theHit.Edep, "Edep/D:Ekin");
		//BGOPMTTree->Branch("time", &theHit.Time, "time/D");
		//BGOPMTTree->Branch("position", &theHit.PosX, "x/D:y:z");

		BGOPMTTree->SetDirectory(0);
		


	}

  // Calculate the theoretical optimum power in the RF cavity to induce a perfect Pi-pulse
  detConst->GetCavityConst()->GetMWSetup()->CalculateOptimumPower();

  if (G4VVisManager::GetConcreteInstance())
    {
      G4UImanager* UI = G4UImanager::GetUIpointer(); 
      UI->ApplyCommand("/vis/scene/notifyHandlers");
    } 

}

void hbarRunAction::EndOfRunAction(const G4Run* )
{
  //G4cout << "Last particle's creation time: "
  //	 << primGenAction->GetProductionTime()/s << " seconds" << G4endl;
  G4cout << "Hbar hits in the dummy detector: " << hitCounter << G4endl;
 // if(detConst->GetCPTConst()->GetUseSensitiveBGO()) G4cout << "Hbar hits in the BGO detector: " << BGOHitCounter << G4endl;
  rootfile->cd();
  angleDiffDist->Write();
  angleDiffDistAll->Write();
  veloDist->Write();
  veloDistDet->Write(); // Added by Clemens
  cavityEntranceQuantumNumber->Write();
  detectorCavityQuantumNumber->Write();
  angleDistDet->Write(); // Added by Clemens
  stopNt->Write();
  hitTree->Write();

  //hitTree->Print();


  char buffer[50];
  int n = sprintf(buffer, "%f", detConst->GetWorldSizeXY()/m);
  (void)n;
  TObjString *tempstr = new TObjString(buffer);
  tempstr->Write("WorldSizeXY");
  delete tempstr;
  
  n = sprintf(buffer, "%f", detConst->GetWorldSizeZ()/m);
  tempstr = new TObjString(buffer);
  tempstr->Write("WorldSizeZ");
  delete tempstr;

  n = sprintf(buffer, "%f", detConst->GetSextupoleConst()->GetSextupoleLength(1)/m);
  tempstr = new TObjString(buffer);
  tempstr->Write("Sextupole1Length");
  delete tempstr;


  if(!primGenAction->GetUseStrongInitialState())
	{
	  QuantumNumbers * tempNumbers = new QuantumNumbers(primGenAction->GetSourceStateN(),
														primGenAction->GetSourceStateL(),
														primGenAction->GetSourceStateTwoJ(),
														primGenAction->GetSourceStateTwoMJ());
	  tempNumbers->Write("SourceState");
	  delete tempNumbers;
	}
  else
	{
	  StrongFieldQuantumNumbers * tempNumbers = new StrongFieldQuantumNumbers(primGenAction->GetSourceStateN(),
																			  primGenAction->GetSourceStateL(),
														primGenAction->GetSourceStateML(),
														primGenAction->GetSourceStateTwoMS());
	  tempNumbers->Write("SourceState");
	  delete tempNumbers;
	}

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleLength(2)/m, 3, true).c_str());
  tempstr->Write("Sextupole2Length");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleFrontInnerDiameter(1)/m, 3, true).c_str());
  tempstr->Write("Sextupole1FrontInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleRearInnerDiameter(1)/m, 3, true).c_str());
  tempstr->Write("Sextupole1RearInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleFrontOuterDiameter(1)/m, 3, true).c_str());
  tempstr->Write("Sextupole1FrontOuterDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleRearOuterDiameter(1)/m, 3, true).c_str());
  tempstr->Write("Sextupole1RearOuterDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleFrontInnerDiameter(2)/m, 3, true).c_str());
  tempstr->Write("Sextupole2FrontInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleRearInnerDiameter(2)/m, 3, true).c_str());
  tempstr->Write("Sextupole2RearInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleFrontOuterDiameter(2)/m, 3, true).c_str());
  tempstr->Write("Sextupole2FrontOuterDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetSextupoleConst()->GetSextupoleRearOuterDiameter(2)/m, 3, true).c_str());
  tempstr->Write("Sextupole2RearOuterDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWCavityLength()/m, 3, true).c_str());
  tempstr->Write("MWCavityLength");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWCavityInnerDiameter()/m, 3, true).c_str());
  tempstr->Write("MWCavityInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWCavityOuterDiameter()/m, 3, true).c_str());
  tempstr->Write("MWCavityOuterDiameter");
  delete tempstr;
  
  

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWCavityCenter()/m, 3, true).c_str());
  tempstr->Write("MWCavityCenter");
  delete tempstr;

  if(detConst->GetDDetConst() != NULL)
	{
	  tempstr = new TObjString(numtostr(detConst->GetDDetConst()->GetDetectorLength()/m, 3, true).c_str());
	  tempstr->Write("DetectorLength");
	  delete tempstr;
	  
	  tempstr = new TObjString(numtostr(detConst->GetDDetConst()->GetDetectorDiameter()/m, 3, true).c_str());
	  tempstr->Write("DetectorDiameter");
	  delete tempstr;
	  
	  tempstr = new TObjString(numtostr(detConst->GetDDetConst()->GetDetectorCenter()/m, 3, true).c_str());
	  tempstr->Write("DetectorCenter");
	  delete tempstr;
	}

  tempstr = new TObjString(numtostr(detConst->GetMagneticFieldSetup()->GetField()->GetFieldMaxValue(1)/tesla, 3, true).c_str());
  tempstr->Write("S1FieldMaxValue");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetMagneticFieldSetup()->GetField()->GetFieldMaxValue(2)/tesla, 3, true).c_str());
  tempstr->Write("S2FieldMaxValue");
  delete tempstr;

  tempstr = new TObjString((detConst->GetMagneticFieldSetup()->GetAllowMajoranaTransitions()).c_str());
  tempstr->Write("AllowMajoranaTransitions");
  delete tempstr;

  tempstr = new TObjString((detConst->GetMagneticFieldSetup()->GetUseFieldLinesAngleDifference()).c_str());
  tempstr->Write("UseFieldLinesAngleDifference");
  delete tempstr;

  tempstr = NULL;
  if (detConst->GetMagneticFieldSetup()->GetField()->IsFieldFlipped(2)) {
    tempstr = new TObjString("on");
  } else {
    tempstr = new TObjString("off");
  }
  tempstr->Write("S2Flipped");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWSetup()->GetFixedTransitionProbability(), 3, true).c_str());
  tempstr->Write("FixedTransitionProbability");
  delete tempstr;

  tempstr = new TObjString(detConst->GetCavityConst()->GetMWSetup()->GetUseFixedTransitionProbabilities().c_str());
  tempstr->Write("UseFixedTransitionProbabilities");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWSetup()->GetMWPower()/watt, 3, true).c_str());
  tempstr->Write("MWPower");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWSetup()->GetMWFrequency()/megahertz, 12, true).c_str());
  tempstr->Write("MWFrequency");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWSetup()->GetCavityQ(), 3, true).c_str());
  tempstr->Write("CavityQ");
  delete tempstr;

  tempstr = new TObjString(numtostr(detConst->GetCavityConst()->GetMWSetup()->GetCavityMagneticField()/tesla, 3, true).c_str());
  tempstr->Write("CavityMagneticField");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetNumberOfParticles()).c_str());
  tempstr->Write("NumberOfPrimaries");
  delete tempstr;

  tempstr = new TObjString(primGenAction->GetParticleName().c_str());
  tempstr->Write("PrimaryName");
  delete tempstr;

  tempstr = new TObjString(primGenAction->GetRndmEnergyFlag().c_str());
  tempstr->Write("RandomSourceEnergy");
  delete tempstr;

  tempstr = new TObjString(primGenAction->GetRndmStateFlag().c_str());
  tempstr->Write("RandomSourceState");
  delete tempstr;

  tempstr = new TObjString(primGenAction->GetRndmDirectionFlag().c_str());
  tempstr->Write("RandomSourceDirection");
  delete tempstr;

  tempstr = new TObjString(primGenAction->GetSliceFlag().c_str());
  tempstr->Write("SliceSource");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceStateF()).c_str());
  tempstr->Write("SourceState_F");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceStateM()).c_str());
  tempstr->Write("SourceState_M");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceCenter()/m, 3, true).c_str());
  tempstr->Write("SourceCenter");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceTemperature()/kelvin, 3, true).c_str());
  tempstr->Write("SourceTemperature");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetBeamKineticEnergy()/eV, 3, true).c_str());
  tempstr->Write("BeamKineticEnergy");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceFWHM_X()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_X");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceFWHM_Y()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_Y");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceFWHM_Z()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_Z");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceOffsetX()/m, 3, true).c_str());
  tempstr->Write("SourceOffset_X");
  delete tempstr;

  tempstr = new TObjString(numtostr(primGenAction->GetSourceOffsetY()/m, 3, true).c_str());
  tempstr->Write("SourceOffset_Y");
  delete tempstr;

  tempstr = new TObjString(numtostr(hitCounter).c_str());
  tempstr->Write("RealHits");
  delete tempstr;
  
  tempstr = new TObjString(numtostr(detConst->GetCPTConst()->GetHodoscopeCenter(0)/m, 3, true).c_str());
  tempstr->Write("HodoscopeCenter");
  delete tempstr;
  
  
  rootfile->Close();  

  if(detConst->GetCPTConst()->GetUseSensitiveBGOPMTs()) {
	//std::cout << "++++++++++++++++++++++++++++++++++ runaction write+close " << std::endl;
		BGOPMTFile->cd();
		BGOPMTTree->Write();
		BGOPMTFile->Close();
  }
  if(detConst->GetCPTConst()->GetUseSensitiveSiPMs()) {
		HodorFile->cd();
		HodorTree->Write();
		HodorFile->Close();
  }
  
  hbarBuildHitTree treecreator(detConst, fileName);
  treecreator.FillRealHitTree();
  treecreator.Fill_ND_TD_Tree();    // added by tajima
  //treecreator.FillNDHitTree();    // added by tajima
  //treecreator.FillTDHitTree();    // added by tajima



 /* std::ofstream fileOut(filenamedatBGO);
 // std::vector<std::vector<double> > edeptemp = detConst->GetCPTConst()->Get_BGODet_SD()->GetEdep_vector();
  //for(int i=0; i<edeptemp.size(); i++) std::cout <<  edeptemp[i][0] << "	" << edeptemp[i][1] << std::endl;
  int oldevnr = 0;
  double etot =0.0;
  if(edeptemp.size()>0) oldevnr = edeptemp[0][0];

 
  for(int i=0; i<edeptemp.size(); i++) {
     etot += edeptemp[i][1];
    if(oldevnr < edeptemp[i][0]) {
		oldevnr = edeptemp[i][0];
	    //fileOut <<  edeptemp[i][0] << "	" << edeptemp[i][1] << std::endl;
		fileOut << etot << std::endl;
		etot = 0.0;
   }
  }
  fileOut.close();*/
  
  if (G4VVisManager::GetConcreteInstance()) 
	{
	  G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/update");
	}
}

void hbarRunAction::SetFileName(G4String name)
{
   fileName = name;
}

void hbarRunAction::SetHodorWaveformFileName(G4String name)
{
   HodorWaveformFileName = name;
}

void hbarRunAction::SetBGOPMTFileName(G4String name)
{
   BGOPMTFileName = name;
}

void hbarRunAction::SetfilenamedatBGO(G4String name)
{
   filenamedatBGO = name;
}

void hbarRunAction::FillHit(hbarDetectorHit *aHit) 
{
   theHit.Event = aHit->GetEventNumber();
   theHit.Hit   = aHit->GetHitNumber();
   theHit.Edep  = aHit->GetEnergyDeposit()/MeV;
   theHit.Ekin  = aHit->GetKineticEnergy()/MeV;
   theHit.Time  = aHit->GetTime()/ns;
   theHit.PosX  = aHit->GetPosition().x()/m;
   theHit.PosY  = aHit->GetPosition().y()/m;
   theHit.PosZ  = aHit->GetPosition().z()/m;
   theHit.PDGcode = aHit->Get_PDGcode();
   //std::cout << theHit.PDG_code << " fill hit " << typeid(aHit->GetPDG_code()).name() << std::endl;
   //Added by Clemens
   theHit.MomX  = aHit->GetMomentumdir().x()/m;
   theHit.MomY  = aHit->GetMomentumdir().y()/m;
   theHit.MomZ  = aHit->GetMomentumdir().z()/m;
   
   G4String str1 = aHit->GetDetectorName();
   //std::cout << "################### " << str1 << std::endl;
   int i = 0;
   if(str1.contains("scinti_bar")) {
   
        theHit.Detector[0] = 'H';
        theHit.Detector[1] = 'o';
        theHit.Detector[2] = 'd';
        theHit.Detector[3] = 'o';
        theHit.Detector[4] = 'r';            
        
        int barnr = str1[10] - '0';
        if(str1(11) == '_') barnr = barnr -2 ;
        else barnr = barnr*10 + (str1[11]-'0') - 2;
        
        
        if(barnr < 32) {
            theHit.Detector[5] = '0';
            theHit.Detector[6] = '_';
            theHit.Detector[7] = '0';
            theHit.Detector[7] = barnr/10 + '0';
            theHit.Detector[8] = barnr%10 + '0';
            theHit.Detector[9] = '\0';
        }
        else {      
            theHit.Detector[5] = '1';
            theHit.Detector[6] = '_';  
            barnr = barnr - 32;          
            theHit.Detector[7] = barnr/10 + '0';
            theHit.Detector[8] = barnr%10 + '0';
            theHit.Detector[9] = '\0';
        
        }
     
        //std::cout << str1 << std::endl;
        //std::cout << barnr << std::endl;
        //std::cout << theHit.Detector << std::endl;
		/*theHit.Detector[0] = 'b';
		theHit.Detector[1] = 'a';
		theHit.Detector[2] = 'r';
		theHit.Detector[3] = str1(10);
		theHit.Detector[4] = str1(11);
		theHit.Detector[5] = '\0';*/

   }
   /*else if(str1.contains("Fibre")) {
    std::cout << "---------------------------------------------------------------------------------------------------------Fibre:" << str1 << std::endl;
   }*/
   else {
   do {
      theHit.Detector[i] = str1(i);
      i++;
   } while (str1(i-1) != '\0');
	}

   //if(aHit->GetDetectorName().contains("Veto"))
     //std::cout << "VETONAME:" << aHit->GetDetectorName() << std::endl;


   G4String str2 = aHit->GetParticleName();
   i = 0;
   do {
      theHit.Particle[i] = str2(i);
      i++;
   } while (str2(i-1) != '\0');
   // added by tajima
   
   
   
   //std::cout << "hbRRUN ACTION:" << aHit->GetPDG_code() << std::endl;
   
   theHit.track_id = aHit ->Gettrack_id();
   theHit.parent_id = aHit ->Getparent_id();


}
