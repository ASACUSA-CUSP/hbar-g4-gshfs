#include "hbarCUSPConst.hh"

hbarCUSPConst::hbarCUSPConst(hbarFieldSetup * fieldToUse)
  :magFieldSetup(fieldToUse),
   useMLSDUCT(false), useSGVDUCT(false), useMikiLense(false), useMLDUCT(false), useMLELE(false)
{
  CuspMREsuppt = -1;
  for (G4int i = 0; i < maxCuspMREsup; i++) 
	{
	  CuspMREsupRot[i] = G4RotationMatrix();
	}
  CuspCuStpt = -1;
  for (G4int i = 0; i < maxCuspCuSt; i++) 
	{
	  CuspCuStRot[i] = G4RotationMatrix();
	}
  for (G4int i =0; i < maxCuspPol; i++)
	{
	  CuspPolRot[i] = G4RotationMatrix();
	}


  CenterOfTheCusp = -1500.0*mm;
  CenterOfTheMikiLense = CenterOfTheCusp + 2473.8*mm;
  CuspMBHalfXLen[0]= 620.0 *0.5*mm; CuspMBHalfYLen[0]= 670.0 *0.5*mm; CuspMBHalfZLen[0]= 570.0 *0.5*mm;
  CuspMTInRad[0]=      0.0 *mm;     CuspMTOutRad[0]=   620.0 *0.5*mm; CuspMTHalfLen[0]=  570.0 *0.5*mm;
  CuspMHInRad[0]=      0.0 *mm,     CuspMHOutRad[0]=   160.0 *0.5*mm; CuspMHHalfLen[0]=  580.0 *0.5*mm;
  
  CuspMBHalfXLen[1]= 660.0 *0.5*mm; CuspMBHalfYLen[1]= 700.0 *0.5*mm; CuspMBHalfZLen[1]=  15.0 *0.5*mm;
  CuspMTInRad[1]=      0.0 *mm;     CuspMTOutRad[1]=   660.0 *0.5*mm; CuspMTHalfLen[1]=   15.0 *0.5*mm;
  CuspMHInRad[1]=      0.0 *mm,     CuspMHOutRad[1]=   160.0 *0.5*mm; CuspMHHalfLen[1]=   20.0 *0.5*mm;
  
  CuspMBHalfXLen[2]= 660.0 *0.5*mm; CuspMBHalfYLen[2]= 700.0 *0.5*mm; CuspMBHalfZLen[2]=  25.0 *0.5*mm;
  CuspMTInRad[2]=      0.0 *mm;     CuspMTOutRad[2]=   660.0 *0.5*mm; CuspMTHalfLen[2]=   25.0 *0.5*mm;
  CuspMHInRad[2]=      0.0 *mm,     CuspMHOutRad[2]=   232.0 *0.5*mm; CuspMHHalfLen[2]=   30.0 *0.5*mm;
  
  CuspMREInRad[0]= 89.0 *0.5*mm; CuspMREOutRad[0]= 92.0 *0.5*mm; CuspMREHalfLen[0]= 14.0 *0.5*mm; // CE
  
  CuspMREInRad[1]= 80.0 *0.5*mm; CuspMREOutRad[1]= 86.0 *0.5*mm; CuspMREHalfLen[1]= 12.0 *0.5*mm; // U1
  CuspMREInRad[2]= 80.0 *0.5*mm; CuspMREOutRad[2]= 86.0 *0.5*mm; CuspMREHalfLen[2]= 14.0 *0.5*mm; // U2
  CuspMREInRad[3]= 80.0 *0.5*mm; CuspMREOutRad[3]= 88.0 *0.5*mm; CuspMREHalfLen[3]= 30.0 *0.5*mm; // U3
  CuspMREInRad[4]= 80.0 *0.5*mm; CuspMREOutRad[4]= 88.0 *0.5*mm; CuspMREHalfLen[4]= 30.0 *0.5*mm; // U4
  CuspMREInRad[5]= 80.0 *0.5*mm; CuspMREOutRad[5]= 88.0 *0.5*mm; CuspMREHalfLen[5]= 30.0 *0.5*mm; // U5
  CuspMREInRad[6]= 80.0 *0.5*mm; CuspMREOutRad[6]= 88.0 *0.5*mm; CuspMREHalfLen[6]= 30.0 *0.5*mm; // U6
  CuspMREInRad[7]= 80.0 *0.5*mm; CuspMREOutRad[7]= 88.0 *0.5*mm; CuspMREHalfLen[7]= 30.0 *0.5*mm; // U7
  CuspMREInRad[8]= 80.0 *0.5*mm; CuspMREOutRad[8]= 88.0 *0.5*mm; CuspMREHalfLen[8]= 30.0 *0.5*mm; // U8
  CuspMREInRad[9]= 80.0 *0.5*mm; CuspMREOutRad[9]= 88.0 *0.5*mm; CuspMREHalfLen[9]= 72.0 *0.5*mm; // U9
  
  CuspMREInRad[10]= 80.0  *0.5*mm; CuspMREOutRad[10]=  86.0  *0.5*mm; CuspMREHalfLen[10]=  12.0 *0.5*mm; // D1
  CuspMREInRad[11]= 80.0  *0.5*mm; CuspMREOutRad[11]=  86.0  *0.5*mm; CuspMREHalfLen[11]=  14.0 *0.5*mm; // D2
  CuspMREInRad[12]= 80.0  *0.5*mm; CuspMREOutRad[12]=  88.0  *0.5*mm; CuspMREHalfLen[12]=  30.0 *0.5*mm; // D3
  CuspMREInRad[13]= 80.0  *0.5*mm; CuspMREOutRad[13]=  88.0  *0.5*mm; CuspMREHalfLen[13]=  30.0 *0.5*mm; // D4
  CuspMREInRad[14]= 80.0  *0.5*mm; CuspMREOutRad[14]=  88.0  *0.5*mm; CuspMREHalfLen[14]=  30.0 *0.5*mm; // D5
  CuspMREInRad[15]= 80.0  *0.5*mm; CuspMREOutRad[15]=  88.0  *0.5*mm; CuspMREHalfLen[15]=  30.0 *0.5*mm; // D6
  CuspMREInRad[16]= 80.0  *0.5*mm; CuspMREOutRad[16]=  88.0  *0.5*mm; CuspMREHalfLen[16]=  70.0 *0.5*mm; // D7
  
  CuspMREInRad[17]= 88.0  *0.5*mm; CuspMREOutRad[17]= 107.5  *0.5*mm; CuspMREHalfLen[17]=  30.0 *0.5*mm; // upstream edge
  CuspMREInRad[18]= 16.0  *0.5*mm; CuspMREOutRad[18]=  79.8  *0.5*mm; CuspMREHalfLen[18]=   2.0 *0.5*mm; 
  CuspMREInRad[19]= 79.8  *0.5*mm; CuspMREOutRad[19]=  79.81  *0.5*mm; CuspMREHalfLen[19]=   6.0 *0.5*mm;  
  CuspMREInRad[20]= 88.0  *0.5*mm; CuspMREOutRad[20]= 107.5  *0.5*mm; CuspMREHalfLen[20]=  20.0 *0.5*mm; // downstream edge
  CuspMREInRad[21]= 80.0  *0.5*mm; CuspMREOutRad[21]= 107.5  *0.5*mm; CuspMREHalfLen[21]=  10.0 *0.5*mm;  
  CuspMREInRad[22]= 80.0  *0.5*mm; CuspMREOutRad[22]=  93.09 *0.5*mm; CuspMREHalfLen[22]=  50.0 *0.5*mm;  
  CuspMREInRad[23]= 80.0  *0.5*mm; CuspMREOutRad[23]= 106.4  *0.5*mm; CuspMREHalfLen[23]=  10.0 *0.5*mm;  
  CuspMREInRad[24]= 95.71 *0.5*mm; CuspMREOutRad[24]= 106.4  *0.5*mm; CuspMREHalfLen[24]=  40.0 *0.5*mm;  
  
  CuspMREInRad[25]= 16.0  *0.5*mm; CuspMREOutRad[25]=  20.0  *0.5*mm; CuspMREHalfLen[25]=  18.0 *0.5*mm; // Enomoto aperture
  CuspMREInRad[26]= 15.6  *0.5*mm; CuspMREOutRad[26]=  60.0  *0.5*mm; CuspMREHalfLen[26]=   2.0 *0.5*mm;
  CuspMREInRad[27]= 40.0  *0.5*mm; CuspMREOutRad[27]=  60.0  *0.5*mm; CuspMREHalfLen[27]=   2.0 *0.5*mm;
  CuspMREInRad[28]= 40.0  *0.5*mm; CuspMREOutRad[28]=  44.0  *0.5*mm; CuspMREHalfLen[28]= 196.0 *0.5*mm;
  CuspMREInRad[29]= 40.0  *0.5*mm; CuspMREOutRad[29]=  60.0  *0.5*mm; CuspMREHalfLen[29]=   2.0 *0.5*mm;
  CuspMREInRad[30]= 60.0  *0.5*mm; CuspMREOutRad[30]= 107.8  *0.5*mm; CuspMREHalfLen[30]=  50.0 *0.5*mm;
  
  for(G4int i=0;i<4;++i)
	{
	  CuspMREsupHalfXLen[i]= 24.0 *0.5*mm; 
	  CuspMREsupHalfYLen[i]= 5.4 *0.5*mm;
	  CuspMREsupHalfZLen[i]=  20.0 *0.5*mm;
	  CuspMREsupHalfXLen[i+4]= 24.0 *0.5*mm; 
	  CuspMREsupHalfYLen[i+4]= 7.4 *0.5*mm; 
	  CuspMREsupHalfZLen[i+4]= 273.0 *0.5*mm;
	  CuspMREsupHalfXLen[i+8]= 24.0 *0.5*mm; 
	  CuspMREsupHalfYLen[i+8]= 7.4 *0.5*mm; 
	  CuspMREsupHalfZLen[i+8]= 209.0 *0.5*mm;
	}
  
  CuspUHVbInRad[0]= 109.9 *0.5*mm; CuspUHVbOutRad[0]= 114.3 *0.5*mm; CuspUHVbHalfLen[0]= 1255.0  *0.5*mm;
  CuspUHVbInRad[1]= 148.0 *0.5*mm; CuspUHVbOutRad[1]= 153.0 *0.5*mm; CuspUHVbHalfLen[1]=  346.65 *0.5*mm;
  CuspUHVbInRad[2]= 153.0 *0.5*mm; CuspUHVbOutRad[2]= 210.0 *0.5*mm; CuspUHVbHalfLen[2]=  121.0  *0.5*mm;
  CuspUHVbInRad[3]=  50.0 *0.5*mm; CuspUHVbOutRad[3]= 146.0 *0.5*mm; CuspUHVbHalfLen[3]=    1.0  *0.5*mm;
  
  CuspUHVcnFOutRad= 114.3 *0.5*mm; CuspUHVcnROutRad= 153.0 *0.5*mm; CuspUHVcnHalfLen= 19.35 *0.5*mm;
  CuspUHVcnFInRad= 108.9 *0.5*mm; CuspUHVcnRInRad= 148.0 *0.5*mm;
  
  CuspCuColInRad= 114.3 *0.5*mm; CuspCuColOutRad= 119.3 *0.5*mm; CuspCuColHalfLen= 597.5 *0.5*mm;
  
  for(G4int i=0; i<8; ++i)
	{
	  CuspCuStHalfXLen[i]=    15.0 *0.49*mm; CuspCuStHalfYLen[i]=     2.5  *0.4*mm; CuspCuStHalfZLen[i]=   370.0 *0.49*mm;
	  CuspCuStHalfXLen[i+8]=  15.0 *0.49*mm; CuspCuStHalfYLen[i+8]=   2.5  *0.4*mm; CuspCuStHalfZLen[i+8]= 370.0 *0.49*mm;
	  CuspCuStHalfXLen[i+16]= 10.0 *0.49*mm; CuspCuStHalfYLen[i+16]= 21.85 *0.4*mm; CuspCuStHalfZLen[i+16]=  2.5 *0.49*mm;
	}
  for(G4int i=0; i<2; ++i)
	{
	  CuspPolInRad[i*8]=    36.0 *0.5*mm; CuspPolOutRad[i*8]=    38.0 *0.5*mm; CuspPolHalfLen[i*8]=   207.0 *0.5*mm;
	  CuspPolInRad[i*8+1]=  36.0 *0.5*mm; CuspPolOutRad[i*8+1]=  68.0 *0.5*mm; CuspPolHalfLen[i*8+1]=   7.0 *0.5*mm;
	  CuspPolInRad[i*8+2]=  94.0 *0.5*mm; CuspPolOutRad[i*8+2]=  99.0 *0.5*mm; CuspPolHalfLen[i*8+2]= 455.5 *0.5*mm;
	  CuspPolInRad[i*8+3]=  94.0 *0.5*mm; CuspPolOutRad[i*8+3]= 125.0 *0.5*mm; CuspPolHalfLen[i*8+3]=   9.5 *0.5*mm;
	  CuspPolInRad[i*8+4]= 147.0 *0.5*mm; CuspPolOutRad[i*8+4]= 203.0 *0.5*mm; CuspPolHalfLen[i*8+4]=  22.0 *0.5*mm;
	  CuspPolInRad[i*8+5]= 147.0 *0.5*mm; CuspPolOutRad[i*8+5]= 203.0 *0.5*mm; CuspPolHalfLen[i*8+5]=  22.0 *0.5*mm;
	  CuspPolInRad[i*8+6]= 147.0 *0.5*mm; CuspPolOutRad[i*8+6]= 153.0 *0.5*mm; CuspPolHalfLen[i*8+6]= 456.0 *0.5*mm;
	  CuspPolInRad[i*8+7]= 147.0 *0.5*mm; CuspPolOutRad[i*8+7]= 203.0 *0.5*mm; CuspPolHalfLen[i*8+7]=  22.0 *0.5*mm;
	}


  double tmpCuspCuPlateHalfXLen[37] = {2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 15.0, 131.8, 72.15, 
									19.15, 19.15, 10.0, 10.0, 10.0, 2.5, 2.5, 16.5, 16.5, 2.5, 
									2.5, 2.5, 2.5, 2.5, 2.5, 2.5, 175.5, 91.5, 26.37, 26.37, 
									10.0, 10.0, 10.0, 2.5, 2.5, 16.5, 16.5};
  double tmpCuspCuPlateHalfYLen[37] = {19.15, 19.15, 69.65, 69.65, 106.4, 106.4, 124.3, 2.5, 2.5, 2.5, 
									2.5, 2.5, 2.5,  150.85, 150.85, 150.85, 150.85, 2.5, 2.5, 23.87, 
									86.5, 141.63, 165.5, 23.87, 86.5, 141.63, 2.5, 2.5, 2.5, 2.5, 
									2.5, 131.5, 131.5, 131.5, 131.5, 2.5, 2.5};
  double tmpCuspCuPlateHalfZLen[37] = {10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 
									10.0, 10.0, 102.5, 2.5, 2.5, 10.0, 10.0, 10.0, 10.0, 10.0, 
									10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 
									84.0, 2.5, 2.5, 10.0, 10.0, 10.0, 10.0};

  for(int i = 0; i<37; ++i)
	{

	  CuspCuPlateHalfXLen[i]=tmpCuspCuPlateHalfXLen[i]*.5*mm;
	  CuspCuPlateHalfYLen[i]=tmpCuspCuPlateHalfYLen[i]*.5*mm;
	  CuspCuPlateHalfZLen[i]=tmpCuspCuPlateHalfZLen[i]*.5*mm;
	}
  CuspRadShieldInRad= 131.0 *0.5*mm; CuspRadShieldOutRad= 134.0 *0.5*mm; CuspRadShieldHalfLen= 930.0 *0.5*mm;
  CuspOVCrInRad[0]= 0.0 *0.5*mm; CuspOVCrOutRad[0]= 359.5 *0.5*mm; CuspOVCrHalfLen[0]= 367.0 *0.5*mm;
  CuspOVCrInRad[1]= 0.0 *0.5*mm; CuspOVCrOutRad[1]= 153.0 *0.5*mm; CuspOVCrHalfLen[1]= 416.0 *0.5*mm;
  CuspOVCrInRad[2]= 0.0 *0.5*mm; CuspOVCrOutRad[2]= 153.0 *0.5*mm; CuspOVCrHalfLen[2]= 436.0 *0.5*mm;
  CuspOVCrInRad[3]= 0.0 *0.5*mm; CuspOVCrOutRad[3]= 354.0 *0.5*mm; CuspOVCrHalfLen[3]= 370.0 *0.5*mm;
  CuspOVCrInRad[4]= 0.0 *0.5*mm; CuspOVCrOutRad[4]= 147.0 *0.5*mm; CuspOVCrHalfLen[4]= 420.0 *0.5*mm;
  CuspOVCrInRad[5]= 0.0 *0.5*mm; CuspOVCrOutRad[5]= 147.0 *0.5*mm; CuspOVCrHalfLen[5]= 440.0 *0.5*mm;
  CuspOVCrInRad[6]= 0.0 *0.5*mm; CuspOVCrOutRad[6]= 360.0 *0.5*mm; CuspOVCrHalfLen[6]= 324.0 *0.5*mm;
  CuspOVCrInRad[7]= 0.0 *0.5*mm; CuspOVCrOutRad[7]= 153.0 *0.5*mm; CuspOVCrHalfLen[7]= 456.0 *0.5*mm;
  CuspOVCrInRad[8]= 0.0 *0.5*mm; CuspOVCrOutRad[8]= 354.0 *0.5*mm; CuspOVCrHalfLen[8]= 330.0 *0.5*mm;
  CuspOVCrInRad[9]= 0.0 *0.5*mm; CuspOVCrOutRad[9]= 147.0 *0.5*mm; CuspOVCrHalfLen[9]= 460.0 *0.5*mm;
  CuspOVCInRad[0]= 147.0 *0.5*mm; CuspOVCOutRad[0]= 153.0 *0.5*mm; CuspOVCHalfLen[0]= 746.0 *0.5*mm;
  CuspOVCInRad[1]= 147.0 *0.5*mm; CuspOVCOutRad[1]= 203.0 *0.5*mm; CuspOVCHalfLen[1]= 22.0 *0.5*mm;
  CuspOVCInRad[2]= 152.0 *0.5*mm; CuspOVCOutRad[2]= 354.0 *0.5*mm; CuspOVCHalfLen[2]= 25.0 *0.5*mm;
  CuspOVCInRad[4]= 354.0 *0.5*mm; CuspOVCOutRad[4]= 430.0 *0.5*mm; CuspOVCHalfLen[4]= 23.0 *0.5*mm;
  CuspOVCInRad[5]= 215.0 *0.5*mm; CuspOVCOutRad[5]= 430.0 *0.5*mm; CuspOVCHalfLen[5]= 25.0 *0.5*mm;
  CuspOVCInRad[6]= 147.0 *0.5*mm; CuspOVCOutRad[6]= 265.0 *0.5*mm; CuspOVCHalfLen[6]= 22.0 *0.5*mm;
  CuspOVCInRad[7]= 147.0 *0.5*mm; CuspOVCOutRad[7]= 153.0 *0.5*mm; CuspOVCHalfLen[7]=207.0 *0.5*mm;
  CuspOVCInRad[8]= 147.0 *0.5*mm; CuspOVCOutRad[8]= 203.0 *0.5*mm; CuspOVCHalfLen[8]= 16.0 *0.5*mm;
  CuspOVCInRad[9]= 152.0 *0.5*mm; CuspOVCOutRad[9]= 202.40 *0.5*mm; CuspOVCHalfLen[9]=69.78 *0.5*mm;
  CuspOVCInRad[10]=147.0 *0.5*mm; CuspOVCOutRad[10]=203.0 *0.5*mm; CuspOVCHalfLen[10]=22.0 *0.5*mm;
  CuspOVCInRad[11]=140.0 *0.5*mm; CuspOVCOutRad[11]=430.0 *0.5*mm; CuspOVCHalfLen[11]=26.0 *0.5*mm;
  CuspOVCInRad[12]=354.0 *0.5*mm; CuspOVCOutRad[12]=430.0 *0.5*mm; CuspOVCHalfLen[12]=23.0 *0.5*mm;
  CuspOVCInRad[14]=354.0 *0.5*mm; CuspOVCOutRad[14]=430.0 *0.5*mm; CuspOVCHalfLen[14]=23.0 *0.5*mm;
  CuspOVCInRad[15]= 80.0 *0.5*mm; CuspOVCOutRad[15]=430.0 *0.5*mm; CuspOVCHalfLen[15]=11.0 *0.5*mm;
  CuspOVCInRad[16]=109.0 *0.5*mm; CuspOVCOutRad[16]=430.0 *0.5*mm; CuspOVCHalfLen[16]=12.0 *0.5*mm;
  
  
  for(G4int i=0; i<2; ++i)
	{
	  CuspSUSbandInRad[i]=   119.5 *0.5*mm; CuspSUSbandOutRad[i]=   119.9 *0.5*mm; CuspSUSbandHalfLen[i]=   52.0 *0.5*mm;
	  CuspSUSbandInRad[i+2]= 119.5 *0.5*mm; CuspSUSbandOutRad[i+2]= 119.9 *0.5*mm; CuspSUSbandHalfLen[i+2]= 104.0 *0.5*mm;
	}

  CuspRadShieldInRad= 130.0 *0.5*mm; CuspRadShieldOutRad= 134.0 *0.5*mm; CuspRadShieldHalfLen= 920.0 *0.5*mm;
  
  cuspField = new hbarCUSPField();
  
  cuspMessenger = new hbarCUSPMess(this);
}


hbarCUSPConst::~hbarCUSPConst()
{
  delete cuspMessenger;
  if (cuspField) 
	delete cuspField;
}

void hbarCUSPConst::MakeCUSP(G4VPhysicalVolume * WorldPhy, hbarDetectorSD * scintiSD)
{
	  cuspField->Initialize(AsciiElfilename.data(), AsciiMagfilename.data());
	  cuspField->SetZOffset(CenterOfTheCusp);
	  if(magFieldSetup) 
		magFieldSetup->GetField()->SetCUSPField(cuspField);
	  
	  if(useCUSPprev != 0)
		{   // Added by tajima
		  //------------------------------ beam line 0  ///// 
		  G4Tubs* beamline0 = new G4Tubs("beamline0",innerRadiusOfTheKB0,
										 outerRadiusOfTheKB0,hightOfTheKB0,
										 0.0, 2.0*M_PI);
		  
		  beamline0_log = new G4LogicalVolume(beamline0,hbarDetConst::GetMaterial("Sts"),"beamline0",0,0,0);
		  
		  beamline0_phys = new G4PVPlacement(0, G4ThreeVector(beamline0Pos_x,beamline0Pos_y,beamline0Pos_z),
											 beamline0_log,"beamline0",WorldPhy->GetLogicalVolume(),false,0);
		  
		  //------------------------------ beam line 1 Cusp magnet box  ////
		  
		  G4Box* cuspm00 = new G4Box("cuspm00",cuspWidth, cuspThick, cuspLength);
		  G4Tubs* cuspm01 = new G4Tubs("cuspm01",innerRadiusOfCuspT1, outerRadiusOfCuspT1, hightOfCuspT1, 0.0, 2.0*M_PI);    
		  G4Tubs* cuspm02 = new G4Tubs("cuspm02",innerRadiusOfCuspT2, outerRadiusOfCuspT2, hightOfCuspT2, M_PI, M_PI);    
		  
		  G4SubtractionSolid* cuspm03 = new G4SubtractionSolid("cuspm03", cuspm00, cuspm02);
		  G4SubtractionSolid* cuspm0 = new G4SubtractionSolid("cuspm0", cuspm03, cuspm01);
		  
		  cuspm0_log = new G4LogicalVolume(cuspm0,hbarDetConst::GetMaterial("Sts"),"cuspm0",0,0,0);
		  cuspm0_phys = new G4PVPlacement(0, G4ThreeVector(cuspPos_x,cuspPos_y,cuspPos_z), cuspm0_log,"cuspm0",WorldPhy->GetLogicalVolume(),false,0);
		  
		  
		  //------------------------------ beam line 1 Cold Bore  
		  
		  G4Tubs* beamline1 = new G4Tubs("beamline1",innerRadiusOfTheKB1,
										 outerRadiusOfTheKB1,hightOfTheKB1,
										 0.0,2.0*M_PI);
		  beamline1_log = new G4LogicalVolume(beamline1,hbarDetConst::GetMaterial("Sts"),"beamline1",0,0,0);
		  
		  beamline1_phys = new G4PVPlacement(0,
											 G4ThreeVector(beamline1Pos_x,beamline1Pos_y,beamline1Pos_z),
											 beamline1_log,"beamline1",WorldPhy->GetLogicalVolume(),false,0);
		  //------------------------------ beam line 1 Cold Bore flange0  
		  
		  
		  for (G4int i = 0; i < maxCBF; i++) 
			{
			  //std::cout << "CB: started" << std::endl;
			  
			  G4String CBFname = "coldf" + numtostr(i);
			  
			  CBF[i]=new G4Tubs(CBFname,CBFInRad[i], CBFOutRad[i], CBFLength[i], 0.0, 2.0*M_PI);
			  CBFLog[i]=new G4LogicalVolume(CBF[i],hbarDetConst::GetMaterial("Sts"),CBFname,0,0,0);
			  CBFPhy[i]=new G4PVPlacement(0, G4ThreeVector(CBFPos_x[i],CBFPos_y[i],CBFPos_z[i]), CBFLog[i],CBFname,WorldPhy->GetLogicalVolume(),false,0);
			}
		  
		  //------------------------------ GV 
		  
		  G4Tubs* gv1 = new G4Tubs("gv1",innerRadiusOfTheGV1, outerRadiusOfTheGV1,hightOfTheGV1, 0.0,2.0*M_PI);
		  gv1_log = new G4LogicalVolume(gv1,hbarDetConst::GetMaterial("Sts"),"gv1",0,0,0);
		  gv1_phys = new G4PVPlacement(0, G4ThreeVector(gv1Pos_x,gv1Pos_y,gv1Pos_z),gv1_log,"gv1",WorldPhy->GetLogicalVolume(),false,0);
		  
		  //------------------------------ 152 reducing flange 
		  G4Tubs* red0 = new G4Tubs("red0",innerRadiusOfTheRed0, outerRadiusOfTheRed0,hightOfTheRed0, 0.0,2.0*M_PI);
		  red0_log = new G4LogicalVolume(red0,hbarDetConst::GetMaterial("Sts"),"red0",0,0,0);
		  red0_phys = new G4PVPlacement(0, G4ThreeVector(red0Pos_x,red0Pos_y,red0Pos_z), red0_log,"red0",WorldPhy->GetLogicalVolume(),false,0);  
		  
		  //------------------------------ 152 bellow 
		  G4Tubs* bell = new G4Tubs("bell", innerRadiusOfTheBell, outerRadiusOfTheBell,hightOfTheBell, 0.0,2.0*M_PI);
		  bell_log = new G4LogicalVolume(bell,hbarDetConst::GetMaterial("Aluminium"),"bell",0,0,0);
		  bell_phys = new G4PVPlacement(0, G4ThreeVector(bellPos_x,bellPos_y,bellPos_z), bell_log,"bell",WorldPhy->GetLogicalVolume(),false,0); 
		  
		  //------------------------------ 152 flange 
		  
		  for (G4int i = 0; i < maxHDF; i++) 
			{
			  G4String HDFname = "hdf" + numtostr(i);
			  
			  HDF[i]=new G4Tubs(HDFname,HDFInRad[i], HDFOutRad[i], HDFLength[i], 0.0, 2.0*M_PI);
			  HDFLog[i]=new G4LogicalVolume(HDF[i],hbarDetConst::GetMaterial("Aluminium"),HDFname,0,0,0);
			  HDFPhy[i]=new G4PVPlacement(0, G4ThreeVector(HDFPos_x[i],HDFPos_y[i],HDFPos_z[i]), HDFLog[i],HDFname,WorldPhy->GetLogicalVolume(),false,0);
			}
		  
		  //------------------------------ chamber 0 Air
		  G4Tubs* chambervac0 = new G4Tubs("chambervac0",innerRadiusOfCh0, outerRadiusOfCh0,hightOfCh0, 0.0,2.0*M_PI);
		  chambervac0_log = new G4LogicalVolume(chambervac0,hbarDetConst::GetMaterial("IsolVac"),"chambervac0",0,0,0);
		  chambervac0_phys = new G4PVPlacement(0, G4ThreeVector(ch0Pos_x,ch0Pos_y,ch0Pos_z), chambervac0_log,"chambervac0",WorldPhy->GetLogicalVolume(),false,0); 
		  
		  // //------------------------------ chamber 2
		  G4Tubs* chamber2 = new G4Tubs("chamber2",innerRadiusOfCh2, outerRadiusOfCh2,hightOfCh2, 0.0,2.0*M_PI);
		  chamber2_log = new G4LogicalVolume(chamber2,hbarDetConst::GetMaterial("Sts"),"chamber2",0,0,0);
		  chamber2_phys = new G4PVPlacement(0,
											G4ThreeVector(ch2Pos_x,ch2Pos_y,ch2Pos_z), 
											chamber2_log,"chamber2",WorldPhy->GetLogicalVolume(),false,0);
		}
	  
	  // Added by tajima -- start
	  // -- Cusp Main
	  if(useCUSPMain != 0)
		{    
		  
		  for (G4int i = 0; i < maxCuspMPart; i++) 
			{
			  G4String CuspMBoxName = "CuspMBox" + numtostr(i+1);
			  G4String CuspMTubName = "CuspMTub" + numtostr(i+1);
			  G4String CuspMHoleName = "CuspMHole" + numtostr(i+1);
			  G4String Cusp_MName = "Cusp_M" + numtostr(i+1);
			  CuspMBox[i] = new G4Box(CuspMBoxName,CuspMBHalfXLen[i],CuspMBHalfYLen[i],CuspMBHalfZLen[i]);
			  CuspMTub[i] = new G4Tubs(CuspMTubName,CuspMTInRad[i], CuspMTOutRad[i], CuspMTHalfLen[i],1.0*M_PI,1.0*M_PI);
			  CuspMHole[i] = new G4Tubs(CuspMHoleName,CuspMHInRad[i], CuspMHOutRad[i], CuspMHHalfLen[i],0.0,2.0*M_PI);
			  CuspMtransY[i] = -1.0*CuspMBHalfYLen[i];
			  CuspMaintransY[i] = CuspMtransY[i]-60.0*mm;
			  Cusp_M[i] = new G4UnionSolid(Cusp_MName,CuspMBox[i],CuspMTub[i],0,G4ThreeVector(0.0*mm,CuspMtransY[i],0.0*mm));
			}
		  G4SubtractionSolid * tempMain = new G4SubtractionSolid("CuspMainT",Cusp_M[0],CuspMHole[0],0,G4ThreeVector(0.0*mm,CuspMaintransY[0],0.0*mm));



		  ///Begin the awesome copy-pasting! 

		  for (G4int i = 0; i < maxCuspMgCoil; i++) 
			{
			  G4String CuspMgCoilName = "CuspMgCoilR" + numtostr(i+1);
			  CuspMgCoil[i]=new G4Tubs(CuspMgCoilName,CuspMgCoilInRad[i]-0.1*mm,CuspMgCoilOutRad[i]+0.1*mm,CuspMgCoilHalfWidth[i], 0.0, 2.0*M_PI);
			}
		  G4UnionSolid* CuspMgCoil00 = new G4UnionSolid("CuspMgCoil00R", CuspMgCoil[0], CuspMgCoil[1], 0,
														G4ThreeVector(0.0*mm, 0.0*mm, CuspMgCoilHalfWidth[0] + CuspMgCoilHalfWidth[1] + 15.0*mm));
		  G4UnionSolid* CuspMgCoil01 = new G4UnionSolid("CuspMgCoil01R", CuspMgCoil00, CuspMgCoil[3], 0,
														G4ThreeVector(0.0*mm, 0.0*mm, -CuspMgCoilHalfWidth[0] - CuspMgCoilHalfWidth[3] - 15.0*mm));
		  G4UnionSolid* CuspMgCoil02 = new G4UnionSolid("CuspMgCoil02R", CuspMgCoil01, CuspMgCoil[2], 0,
														G4ThreeVector(0.0*mm, 0.0*mm,
																	  CuspMgCoilHalfWidth[0] + 2.0*CuspMgCoilHalfWidth[1] + CuspMgCoilHalfWidth[2] + 15.0*mm + 21.0*mm));
		  G4UnionSolid* CuspMagCoil = new G4UnionSolid("CuspMagCoilR", CuspMgCoil02, CuspMgCoil[4], 0,
													   G4ThreeVector(0.0*mm, 0.0*mm,
																	 -CuspMgCoilHalfWidth[0] - 2.0*CuspMgCoilHalfWidth[3] - CuspMgCoilHalfWidth[4] - 15.0*mm - 21.0*mm));
		  





		  CuspMain[0] = new G4SubtractionSolid("CuspMain0",tempMain,CuspMagCoil,0,G4ThreeVector(0.0*mm,CuspMaintransY[0],0.0*mm));
		  CuspMainLog[0] = new G4LogicalVolume(CuspMain[0],hbarDetConst::GetMaterial("Galactic"),"CuspMain0",0,0,0);
		  CuspMainPhy[0] = new G4PVPlacement(0, G4ThreeVector(0.0*mm,-1.0*CuspMaintransY[0],CenterOfTheCusp),CuspMainLog[0],"CuspMain0",WorldPhy->GetLogicalVolume(),false,0);
		  
		  G4SubtractionSolid* CuspMain01 = new G4SubtractionSolid("CuspMain01",Cusp_M[1],CuspMHole[1],0,G4ThreeVector(0.0*mm,CuspMaintransY[1],0.0*mm));
		  G4SubtractionSolid* CuspMain02 = new G4SubtractionSolid("CuspMain02",Cusp_M[2],CuspMHole[2],0,G4ThreeVector(0.0*mm,CuspMaintransY[2],0.0*mm));
		  CuspMain[1] = new G4UnionSolid("CuspMain1",CuspMain01,CuspMain02,0,G4ThreeVector(0.0*mm,0.0*mm,-CuspMBHalfZLen[1]-CuspMBHalfZLen[2]));
		  CuspMain[2] = new G4UnionSolid("CuspMain2",CuspMain01,CuspMain02,0,G4ThreeVector(0.0*mm,0.0*mm,CuspMBHalfZLen[1]+CuspMBHalfZLen[2]));
		  CuspMainLog[1]=new G4LogicalVolume(CuspMain[1],hbarDetConst::GetMaterial("Sts"),"CuspMain1",0,0,0);
		  CuspMainLog[2]=new G4LogicalVolume(CuspMain[2],hbarDetConst::GetMaterial("Sts"),"CuspMain2",0,0,0);
		  CuspMainPhy[1]=new G4PVPlacement(0, G4ThreeVector(0.0*mm,-1.0*CuspMaintransY[1],CenterOfTheCusp-CuspMBHalfZLen[0]-CuspMBHalfZLen[1]),
										   CuspMainLog[1],"CuspMain1",WorldPhy->GetLogicalVolume(),false,0);
		  CuspMainPhy[2]=new G4PVPlacement(0, G4ThreeVector(0.0*mm,-1.0*CuspMaintransY[1],CenterOfTheCusp+CuspMBHalfZLen[0]+CuspMBHalfZLen[1]),
										   CuspMainLog[2],"CuspMain2",WorldPhy->GetLogicalVolume(),false,0);
		}
	  
	  // -- MRE
	  if(useCUSPMRE != 0)
		{
		  for (G4int i=0; i<maxCuspMRE; i++) 
			{
			  G4String CuspMREname = "CuspMRE" + numtostr(i+1);
			  if(i==4)
				{
				  G4Tubs* CuspMRE04=new G4Tubs("CuspMRE04",CuspMREInRad[i],CuspMREOutRad[i],CuspMREHalfLen[i],0.025,0.5*M_PI-0.05);
				  for(G4int j=0; j<3; j++) 
					{
					  G4String CuspMRESegname = "CuspMRESeg" + numtostr(j+1);
					  CuspMRESeg[j]=new G4Tubs(CuspMRESegname,CuspMREInRad[4],CuspMREOutRad[4],CuspMREHalfLen[4],0.025+0.5*M_PI*(j+1),0.5*M_PI-0.05);
					}
				  G4UnionSolid* CuspMRESeg01 = new G4UnionSolid("CuspMRESeg01",CuspMRE04,CuspMRESeg[0]);
				  G4UnionSolid* CuspMRESeg02 = new G4UnionSolid("CuspMRESeg02",CuspMRESeg01,CuspMRESeg[1]);
				  CuspMRE[4] = new G4UnionSolid(CuspMREname,CuspMRESeg02,CuspMRESeg[2]);
				}
			  else if(i==7)
				{
				  G4Tubs* CuspMRE07=new G4Tubs("CuspMRE07",CuspMREInRad[i],CuspMREOutRad[i],CuspMREHalfLen[i],0.025,0.5*M_PI-0.05);
				  for(G4int j=3; j<6; j++) 
					{
					  G4String CuspMRESegname = "CuspMRESeg" + numtostr(j+1);
					  CuspMRESeg[j]=new G4Tubs(CuspMRESegname,CuspMREInRad[7],CuspMREOutRad[7],CuspMREHalfLen[7],0.025+0.5*M_PI*(j+2),0.5*M_PI-0.05);
					}
				  G4UnionSolid* CuspMRESeg03 = new G4UnionSolid("CuspMRESeg03",CuspMRE07,CuspMRESeg[3]);
				  G4UnionSolid* CuspMRESeg04 = new G4UnionSolid("CuspMRESeg04",CuspMRESeg03,CuspMRESeg[4]);
				  CuspMRE[7] = new G4UnionSolid(CuspMREname,CuspMRESeg04,CuspMRESeg[5]);
				}
			  else
				{
				  CuspMRE[i]=new G4Tubs(CuspMREname, CuspMREInRad[i], CuspMREOutRad[i], CuspMREHalfLen[i], 0.0, 2.0*M_PI);
				}
			  
			  if(i==25)
				{
				  CuspMRELog[i]=new G4LogicalVolume(CuspMRE[i],hbarDetConst::GetMaterial("Sts"),CuspMREname,0,0,0);
				}
			  else     
				{
				  CuspMRELog[i]=new G4LogicalVolume(CuspMRE[i],hbarDetConst::GetMaterial("A7075_Alloy"),CuspMREname,0,0,0);
				}
			}
		  
		  G4double DBE = 2.0*mm;  //Distance Between Electrodes 

   
		  
		  CuspMREPosZ[0] = CenterOfTheCusp;
		  CuspMREPosZ[1] = CuspMREPosZ[0] - 11.0*mm;
		  CuspMREPosZ[2] = CuspMREPosZ[1] - CuspMREHalfLen[1] - CuspMREHalfLen[2];
		  for(G4int i=3;i< 10;i++)
			{
			  CuspMREPosZ[i]=CuspMREPosZ[i-1]-CuspMREHalfLen[i-1]-CuspMREHalfLen[i]-DBE;
			}
		  CuspMREPosZ[10] = CuspMREPosZ[0] + 11.0*mm;
		  CuspMREPosZ[11] = CuspMREPosZ[10] + CuspMREHalfLen[10]+CuspMREHalfLen[11];
		  for(G4int i=12;i<17;i++)
			{
			  CuspMREPosZ[i]=CuspMREPosZ[i-1]+CuspMREHalfLen[i-1]+CuspMREHalfLen[i]+DBE;
			}
		  CuspMREPosZ[17] = CenterOfTheCusp - 301.0*mm;
		  CuspMREPosZ[18] = CenterOfTheCusp - 228.0*mm;
		  CuspMREPosZ[19] = CenterOfTheCusp - 292.0*mm;
		  CuspMREPosZ[20] = CenterOfTheCusp + 234.0*mm;
		  for(G4int i=21;i<25;i++)
			{
			  CuspMREPosZ[i]=CuspMREPosZ[i-1]+CuspMREHalfLen[i-1]+CuspMREHalfLen[i];
			}
		  CuspMREPosZ[25] = CenterOfTheCusp - 346.0*mm;
		  for(G4int i=26;i<30;i++)
			{
			  CuspMREPosZ[i]=CuspMREPosZ[i-1]-CuspMREHalfLen[i-1]-CuspMREHalfLen[i];
			}
		  CuspMREPosZ[30] = CenterOfTheCusp - 532.0*mm;
		  
		  /*
		  for(int i = 0; i<33; ++i)
			{
			  G4cout << "CUSPMRE " << i << "\t FromZ=" <<  (CuspMREPosZ[i]-CuspMREHalfLen[i])*mm << "mm \tToZ=" << (CuspMREPosZ[i]+CuspMREHalfLen[i])*mm << "mm \tinnerRad=" << CuspMREInRad[i]*mm << "mm \touterRad=" << CuspMREOutRad[i]*mm << "mm." << G4endl;
			}

		  */

		  for (G4int i = 0; i < maxCuspMRE; i++) 
			{
			  G4String CuspMREname = "CuspMRE" + numtostr(i+1);
			  CuspMREPhy[i]=new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspMREPosZ[i]),CuspMRELog[i],CuspMREname,WorldPhy->GetLogicalVolume(),false,0);
			}
		  
		  G4double CuspMRESegsupHalfThick = 1.5*mm;
		  for(G4int i=0;i<4;i++)
			{
			  G4String CuspMRESegsupname = "CuspMRESegsup" + numtostr(i+1);
			  CuspMRESegsup[i] = new G4Tubs(CuspMRESegsupname,CuspMREOutRad[4],CuspMREOutRad[4]+CuspMRESegsupHalfThick,CuspMREHalfLen[4],-0.35+i*0.5*M_PI,0.7);
			}
		  for(G4int i=4;i<8;i++)
			{
			  G4String CuspMRESegsupname = "CuspMRESegsup" + numtostr(i+1);
			  CuspMRESegsup[i] = new G4Tubs(CuspMRESegsupname,CuspMREOutRad[7],CuspMREOutRad[7]+CuspMRESegsupHalfThick,CuspMREHalfLen[7],-0.35+(i-4)*0.5*M_PI,0.7);
			}
		  G4UnionSolid* CuspMRESegsup00 = new G4UnionSolid("CuspMRESegsup00",CuspMRESegsup[0],CuspMRESegsup[1]);
		  G4UnionSolid* CuspMRESegsup01 = new G4UnionSolid("CuspMRESegsup01",CuspMRESegsup00,CuspMRESegsup[2]);
		  G4UnionSolid* CuspMRESegsup02 = new G4UnionSolid("CuspMRESegsup02",CuspMRESegsup[4],CuspMRESegsup[5]);
		  G4UnionSolid* CuspMRESegsup03 = new G4UnionSolid("CuspMRESegsup03",CuspMRESegsup02,CuspMRESegsup[6]);
		  
		  G4UnionSolid* CuspMRESegsupA = new G4UnionSolid("CuspMRESegsupA",CuspMRESegsup01,CuspMRESegsup[3]);
		  G4UnionSolid* CuspMRESegsupB = new G4UnionSolid("CuspMRESegsupB",CuspMRESegsup03,CuspMRESegsup[7]);
		  CuspMRESegsupLog[0] = new G4LogicalVolume(CuspMRESegsupA,hbarDetConst::GetMaterial("Macerite"),"CuspMRESegsupA",0,0,0);
		  CuspMRESegsupLog[1] = new G4LogicalVolume(CuspMRESegsupB,hbarDetConst::GetMaterial("Macerite"),"CuspMRESegsupB",0,0,0);
		  CuspMRESegsupPhy[0] = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspMREPosZ[4]),CuspMRESegsupLog[0],"CuspMRESegsupA",WorldPhy->GetLogicalVolume(),false,0);
		  CuspMRESegsupPhy[1] = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspMREPosZ[7]),CuspMRESegsupLog[1],"CuspMRESegsupB",WorldPhy->GetLogicalVolume(),false,0);
		}

	  // -- MRE support
	  if(useCUSPMREsup != 0)
		{
		  for (G4int i = 0; i < maxCuspMREsup; i++) 
			{
			  G4String CuspMREsupName = "CuspMREsup" + numtostr(i+1);
			  CuspMREsup[i]=new G4Box(CuspMREsupName,CuspMREsupHalfXLen[i],CuspMREsupHalfYLen[i],CuspMREsupHalfZLen[i]);
			  CuspMREsupLog[i]=new G4LogicalVolume(CuspMREsup[i],hbarDetConst::GetMaterial("A7075_Alloy"),CuspMREsupName,0,0,0);
			  CuspMREsupRot[i].rotateZ((0.25+0.5*i)*M_PI);
			  if(i>=0 && i<4)
				{
				  CuspMREsupPos[i] = G4ThreeVector(0.0*mm, CuspMREOutRad[0]+CuspMREsupHalfYLen[i]+2.0*mm,CenterOfTheCusp);
				}  
			  else if(i>=4 && i<8)
				{
				  CuspMREsupPos[i] = G4ThreeVector(0.0*mm, CuspMREOutRad[3]+CuspMREsupHalfYLen[i]+2.0*mm,
												   CenterOfTheCusp-CuspMREsupHalfZLen[0]-CuspMREsupHalfZLen[i]);
				} 
			  else if(i>=8 && i<12)
				{
				  CuspMREsupPos[i] = G4ThreeVector(0.0*mm,CuspMREOutRad[3]+CuspMREsupHalfYLen[i]+2.0*mm,
												   CenterOfTheCusp+CuspMREsupHalfZLen[0]+CuspMREsupHalfZLen[i]);
				} 
			  CuspMREsupPos[i].rotateZ((0.25+0.5*i)*M_PI);
			  CuspMREsupPhy[i]=new G4PVPlacement(G4Transform3D(CuspMREsupRot[i],CuspMREsupPos[i]),
												 CuspMREsupLog[i],CuspMREsupName,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- UHV bore
	  if(useCUSPUHV != 0)
		{
		  for (G4int i = 0; i < maxCuspUHV; i++) 
			{
			  G4String CuspUHVname = "CuspUHV" + numtostr(i+1);
			  if(i>=0 && i<=2)
				{
				  CuspUHV[i] = new G4Tubs(CuspUHVname,CuspUHVbInRad[i], CuspUHVbOutRad[i], CuspUHVbHalfLen[i], 0.0, 2.0*M_PI);
				  CuspUHVLog[i] = new G4LogicalVolume(CuspUHV[i],hbarDetConst::GetMaterial("Sts"),CuspUHVname,0,0,0);
				}
			  else if(i==3)
				{
				  CuspUHV[i] = new G4Tubs(CuspUHVname,CuspUHVbInRad[i], CuspUHVbOutRad[i], CuspUHVbHalfLen[i], 0.0, 2.0*M_PI);
				  CuspUHVLog[i] = new G4LogicalVolume(CuspUHV[i],hbarDetConst::GetMaterial("Copper"),CuspUHVname,0,0,0);
				}
			  else if(i==4)
				{
				  CuspUHV[i] = new G4Cons(CuspUHVname,CuspUHVcnFInRad,CuspUHVcnFOutRad,
										  CuspUHVcnRInRad,CuspUHVcnROutRad,CuspUHVcnHalfLen,0.0, 2.0*M_PI);
				  CuspUHVLog[i] = new G4LogicalVolume(CuspUHV[i],hbarDetConst::GetMaterial("Sts"),CuspUHVname,0,0,0);
				}
			}
		  
		  CuspUHVPosZ[0] = CenterOfTheCusp - CuspUHVbHalfLen[0] + 465.0*mm + 2*mm;
		  CuspUHVPosZ[4] = CuspUHVPosZ[0] + CuspUHVbHalfLen[0] + CuspUHVcnHalfLen;
		  CuspUHVPosZ[1] = CuspUHVPosZ[4] + CuspUHVcnHalfLen + CuspUHVbHalfLen[1];
		  CuspUHVPosZ[2] = CenterOfTheCusp + 835.0*mm - CuspUHVbHalfLen[2];
		  CuspUHVPosZ[3] = CenterOfTheCusp + 618.0*mm;
		  
		  for (G4int i = 0; i < maxCuspUHV; i++) 
			{
			  G4String CuspUHVname = "CuspUHV" + numtostr(i+1);
			  CuspUHVPhy[i]=new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspUHVPosZ[i]),CuspUHVLog[i],CuspUHVname,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- copper collar
	  if(useCUSPCuCol != 0)
		{
		  G4Tubs* CuspCuCol = new G4Tubs("CuspCuCol",CuspCuColInRad,CuspCuColOutRad,CuspCuColHalfLen,0.0, 2.0*M_PI);
		  CuspCuColLog = new G4LogicalVolume(CuspCuCol,hbarDetConst::GetMaterial("Copper"),"CuspCuCol",0,0,0);
		  //CuspCuColLog->SetFieldManager(cuspField->fieldMgr, allLocal);
		  CuspCuColPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheCusp-1.25*mm),CuspCuColLog,"CuspCuCol",WorldPhy->GetLogicalVolume(),false,0);
		}
	  // -- copper strip
	  if(useCUSPCuSt != 0)
		{
		  for (G4int i = 0; i < maxCuspCuSt; i++) 
			{
			  G4String CuspCuStName = "CuspCuSt" + numtostr(i+1);
			  CuspCuSt[i]=new G4Box(CuspCuStName,CuspCuStHalfXLen[i],CuspCuStHalfYLen[i],CuspCuStHalfZLen[i]);
			  CuspCuStLog[i]=new G4LogicalVolume(CuspCuSt[i],hbarDetConst::GetMaterial("Copper"),CuspCuStName,0,0,0);
			  //CuspCuStLog[i]->SetFieldManager(cuspField->fieldMgr, allLocal);
			  CuspCuStRot[i].rotateZ(0.25*M_PI*i);
			  if(i>=0 && i<8)
				{
				  CuspCuStPos[i] = G4ThreeVector(0.0*mm, CuspCuColOutRad+CuspCuStHalfYLen[i]+0.5*mm, CenterOfTheCusp-280.0*mm);
				}
			  else if(i>=8 && i<16)
				{
				  CuspCuStPos[i] = G4ThreeVector(0.0*mm, CuspCuColOutRad+CuspCuStHalfYLen[i]+0.5*mm, CenterOfTheCusp+280.0*mm);
				}
			  else if(i>=16 && i<24)
				{
				  CuspCuStPos[i] = G4ThreeVector(0.0*mm, CuspCuColOutRad+CuspCuStHalfYLen[i]+0.5*mm, CenterOfTheCusp+463.75*mm);
				}
			  CuspCuStPos[i].rotateZ(0.25*M_PI*i);
			  CuspCuStPhy[i]=new G4PVPlacement(G4Transform3D(CuspCuStRot[i],CuspCuStPos[i]),
											   CuspCuStLog[i],CuspCuStName,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- poll
	  if(useCUSPPol != 0)
		{
		  for (G4int i = 0; i < maxCuspPol; i++) 
			{
			  G4String CuspPolname = "CuspPol" + numtostr(i+1);
			  CuspPol[i]=new G4Tubs(CuspPolname,CuspPolInRad[i], CuspPolOutRad[i], CuspPolHalfLen[i], 0.0, 2.0*M_PI);
			  if(i == 0 || i == 8)
				{ 
				  CuspPolLog[i]=new G4LogicalVolume(CuspPol[i],hbarDetConst::GetMaterial("Copper"),CuspPolname,0,0,0);
				}
			  else
				{ 
				  CuspPolLog[i]=new G4LogicalVolume(CuspPol[i],hbarDetConst::GetMaterial("Sts"),CuspPolname,0,0,0);
				}
			  
			  CuspPolRot[i].rotateX(0.5*M_PI);
			  
			  for(G4int j=0;j<8;j++)
				{ 
				  CuspPolPosZ[j] = CenterOfTheCusp+535.0*mm;
				}
			  for(G4int j=8;j<16;j++)
				{ 
				  CuspPolPosZ[j] = CenterOfTheCusp-556.0*mm;
				}
			  
			  if(i==0 || i==8)      
				{ 
				  CuspPolPosY[i] = 165.5*mm + CuspPolHalfLen[i];
				}
			  else if(i==1 || i==9) 
				{ 
				  CuspPolPosY[i] = CuspPolPosY[i-1] + CuspPolHalfLen[i-1] + CuspPolHalfLen[i];
				}
			  else if(i==2 || i==10)
				{ 
				  CuspPolPosY[i] = 151.0*mm + CuspPolHalfLen[i];
				}
			  else if(i==3 || i==11)
				{ 
				  CuspPolPosY[i] = CuspPolPosY[i-1] + CuspPolHalfLen[i-1] + CuspPolHalfLen[i];
				}
			  else if(i==4 || i==12)
				{ 
				  CuspPolPosY[i] = 228.0*mm + CuspPolHalfLen[i];
				}
			  else
				{ 
				  CuspPolPosY[i] = CuspPolPosY[i-1] + CuspPolHalfLen[i-1] + CuspPolHalfLen[i];
				}
			  
			  //CuspPolLog[i]->SetFieldManager(cuspField->fieldMgr, allLocal);
			  CuspPolPhy[i]=new G4PVPlacement(G4Transform3D(CuspPolRot[i],G4ThreeVector(0.0*mm,CuspPolPosY[i],CuspPolPosZ[i])),
											  CuspPolLog[i],CuspPolname,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- copper plate 
	  if(useCUSPCuPlate != 0)
		{
		  for (G4int i = 0; i < maxCuspCuPlate; i++) 
			{
			  G4String CuspCuPlateName = "CuspCuPlate" + numtostr(i+1);
			  CuspCuPlate[i]=new G4Box(CuspCuPlateName,CuspCuPlateHalfXLen[i],CuspCuPlateHalfYLen[i],CuspCuPlateHalfZLen[i]);
			  CuspCuPlateLog[i]=new G4LogicalVolume(CuspCuPlate[i],hbarDetConst::GetMaterial("Copper"),CuspCuPlateName,0,0,0);
			}
		  
		  CuspCuPlatePos[0]=  G4ThreeVector(  43.0 *mm,    52.575 *mm, CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[1]=  G4ThreeVector( -43.0 *mm,    52.575 *mm, CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[2]=  G4ThreeVector(  60.9 *mm,    27.325 *mm, CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[3]=  G4ThreeVector( -60.9 *mm,    27.325 *mm, CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[4]=  G4ThreeVector(  63.4 *mm,     8.95 *mm,  CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[5]=  G4ThreeVector( -63.4 *mm,     8.95 *mm,  CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[6]=  G4ThreeVector(  65.9 *mm,     0.0 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[7]=  G4ThreeVector(   0.0 *mm,    60.9 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[8]=  G4ThreeVector(   1.25 *mm,   63.4 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[9]=  G4ThreeVector(  28.575 *mm, -60.9 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[10]= G4ThreeVector(  52.575 *mm, -43.0 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[11]= G4ThreeVector( -52.575 *mm, -43.0 *mm,   CenterOfTheCusp -470.0 *mm);
		  CuspCuPlatePos[12]= G4ThreeVector(   0.0 *mm,    63.4 *mm,   CenterOfTheCusp -526.25 *mm);
		  CuspCuPlatePos[13]= G4ThreeVector(   0.0 *mm,   140.075 *mm, CenterOfTheCusp -535.75 *mm);
		  CuspCuPlatePos[14]= G4ThreeVector(   0.0 *mm,   140.075 *mm, CenterOfTheCusp -576.25 *mm);
		  CuspCuPlatePos[15]= G4ThreeVector(  20.25 *mm,  140.075 *mm, CenterOfTheCusp -556.0 *mm);
		  CuspCuPlatePos[16]= G4ThreeVector( -20.25 *mm,  140.075 *mm, CenterOfTheCusp -556.0 *mm);
		  CuspCuPlatePos[17]= G4ThreeVector( -13.25 *mm,   63.4 *mm,   CenterOfTheCusp -556.0 *mm);
		  CuspCuPlatePos[18]= G4ThreeVector(  13.25 *mm,   63.4 *mm,   CenterOfTheCusp -556.0 *mm);
		  CuspCuPlatePos[19]= G4ThreeVector(  58.88 *mm,   69.565 *mm, CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[20]= G4ThreeVector(  82.75 *mm,   38.25 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[21]= G4ThreeVector(  85.25 *mm,   10.685 *mm, CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[22]= G4ThreeVector(  87.75 *mm,   -1.25 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[23]= G4ThreeVector( -58.88 *mm,   69.565 *mm, CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[24]= G4ThreeVector( -82.75 *mm,   38.25 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[25]= G4ThreeVector( -85.25 *mm,   10.685 *mm, CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[26]= G4ThreeVector(   1.25 *mm,   82.75 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[27]= G4ThreeVector(  40.75 *mm,  -82.75 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[28]= G4ThreeVector(  70.815 *mm, -58.88 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[29]= G4ThreeVector( -70.815 *mm, -58.88 *mm,  CenterOfTheCusp +467.5 *mm);
		  CuspCuPlatePos[30]= G4ThreeVector(   0.0 *mm,    82.75 *mm,  CenterOfTheCusp +514.5 *mm);
		  CuspCuPlatePos[31]= G4ThreeVector(   0.0 *mm,   149.75 *mm,  CenterOfTheCusp +555.25 *mm);
		  CuspCuPlatePos[32]= G4ThreeVector(   0.0 *mm,   149.75 *mm,  CenterOfTheCusp +514.75 *mm);
		  CuspCuPlatePos[33]= G4ThreeVector(  20.25 *mm,  149.75 *mm,  CenterOfTheCusp +535.0 *mm);
		  CuspCuPlatePos[34]= G4ThreeVector( -20.25 *mm,  149.75 *mm,  CenterOfTheCusp +535.0 *mm);
		  CuspCuPlatePos[35]= G4ThreeVector(  13.25 *mm,   82.75 *mm,  CenterOfTheCusp +535.0 *mm);
		  CuspCuPlatePos[36]= G4ThreeVector( -13.25 *mm,   82.75 *mm,  CenterOfTheCusp +535.0 *mm);
		  
		  for (G4int i = 0; i < maxCuspCuPlate; i++) 
			{
			  G4String CuspCuPlateName = "CuspCuPlate" + numtostr(i+1);
			  //CuspCuPlateLog[i]->SetFieldManager(cuspField->fieldMgr, allLocal);
			  CuspCuPlatePhy[i]=new G4PVPlacement(0,CuspCuPlatePos[i],CuspCuPlateLog[i],CuspCuPlateName,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- thin SUS band 0.3mm
	  if(useCUSPSUSband != 0)
		{
		  for (G4int i = 0; i < maxCuspSUSband; i++) 
			{
			  G4String CuspSUSbandname = "CuspSUSband" + numtostr(i+1);
			  CuspSUSband[i]=new G4Tubs(CuspSUSbandname,CuspSUSbandInRad[i], CuspSUSbandOutRad[i], CuspSUSbandHalfLen[i], 0.0, 2.0*M_PI);
			  CuspSUSbandLog[i]=new G4LogicalVolume(CuspSUSband[i],hbarDetConst::GetMaterial("Sts"),CuspSUSbandname,0,0,0);
			  
			  if(i==0)     
				{ 
				  CuspSUSbandPosZ[i] = CenterOfTheCusp+199.04*mm;
				}
			  else if(i==1)
				{ 
				  CuspSUSbandPosZ[i] = CenterOfTheCusp-199.04*mm;
				}
			  else if(i==2)
				{ 
				  CuspSUSbandPosZ[i] = CenterOfTheCusp+86.54*mm;
				}
			  else if(i==3)
				{ 
				  CuspSUSbandPosZ[i] = CenterOfTheCusp-86.54*mm;
				}
			  
			  //CuspSUSbandLog[i]->SetFieldManager(cuspField->fieldMgr, allLocal);
			  CuspSUSbandPhy[i]=new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspSUSbandPosZ[i]),CuspSUSbandLog[i],CuspSUSbandname,WorldPhy->GetLogicalVolume(),false,0);
			}
		}
	  // -- copper cylinder (radiation shield)
	  if(useCUSPRadShield)
		{
		  G4Tubs* CuspRadShield = new G4Tubs("CuspRadShield",CuspRadShieldInRad,CuspRadShieldOutRad,CuspRadShieldHalfLen,0.0, 2.0*M_PI);
		  CuspRadShieldLog = new G4LogicalVolume(CuspRadShield,hbarDetConst::GetMaterial("Copper"),"CuspRadShield",0,0,0);
		  //CuspRadShieldLog->SetFieldManager(cuspField->fieldMgr, allLocal);
		  CuspRadShieldPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm, 0.0*mm, CenterOfTheCusp),CuspRadShieldLog,"CuspRadShield",WorldPhy->GetLogicalVolume(),false,0);
		}
	  // -- OVC
	  if(useCUSPOVC)
		{
		  for (G4int i = 0; i < maxCuspOVCr; i++)
			{
			  G4String CuspOVCrname = "CuspOVCr" + numtostr(i+1);

			  CuspOVCr[i]=new G4Tubs(CuspOVCrname,CuspOVCrInRad[i], CuspOVCrOutRad[i], CuspOVCrHalfLen[i], 0.0, 2.0*M_PI);
			}
		  
		  G4RotationMatrix* OVCRotX = new G4RotationMatrix();
		  G4RotationMatrix* OVCRotY = new G4RotationMatrix();
		  OVCRotX -> rotateX(0.5*M_PI);
		  OVCRotY -> rotateY(0.5*M_PI);
		  G4ThreeVector CuspOVCtrans0 = G4ThreeVector(0.0*mm, 0.0*mm, -1.0*CuspOVCrHalfLen[0] + 115.0*mm); 
		  G4ThreeVector CuspOVCtrans1 = G4ThreeVector(0.0*mm, 10.0*mm, -1.0*CuspOVCrHalfLen[0] + 115.0*mm); 
		  G4ThreeVector CuspOVCtrans2 = G4ThreeVector(0.0*mm, 0.0*mm, 50.0*mm); 
		  
		  for (G4int i = 0; i < maxCuspOVC; i++) 
			{
			  G4String CuspOVCname = "CuspOVC" + numtostr(i+1);
			  if(i==3)
				{
				  G4UnionSolid* CuspOVC00 = new G4UnionSolid("CuspOVC00",CuspOVCr[0],CuspOVCr[1],OVCRotY,CuspOVCtrans0);
				  G4UnionSolid* CuspOVC01 = new G4UnionSolid("CuspOVC01",CuspOVC00,CuspOVCr[2],OVCRotX,CuspOVCtrans1);
				  G4SubtractionSolid* CuspOVC02 = new G4SubtractionSolid("CuspOVC02",CuspOVC01,CuspOVCr[3]);
				  G4SubtractionSolid* CuspOVC03 = new G4SubtractionSolid("CuspOVC03",CuspOVC02,CuspOVCr[4],OVCRotY,CuspOVCtrans0);
				  CuspOVC[i] = new G4SubtractionSolid(CuspOVCname,CuspOVC03,CuspOVCr[5],OVCRotX,CuspOVCtrans1);
				}
			  
			  else if(i==13)
				{
				  G4UnionSolid* CuspOVC10 = new G4UnionSolid("CuspOVC10",CuspOVCr[6],CuspOVCr[7],OVCRotY,CuspOVCtrans2);
				  G4UnionSolid* CuspOVC11 = new G4UnionSolid("CuspOVC11",CuspOVC10,CuspOVCr[7],OVCRotX,CuspOVCtrans2);
				  G4SubtractionSolid* CuspOVC12 = new G4SubtractionSolid("CuspOVC12",CuspOVC11,CuspOVCr[8]);
				  G4SubtractionSolid* CuspOVC13 = new G4SubtractionSolid("CuspOVC13",CuspOVC12,CuspOVCr[9],OVCRotY,CuspOVCtrans2);
				  CuspOVC[i] = new G4SubtractionSolid(CuspOVCname,CuspOVC13,CuspOVCr[9],OVCRotX,CuspOVCtrans2);
				}
			  else
				{
				  CuspOVC[i]=new G4Tubs(CuspOVCname,CuspOVCInRad[i], CuspOVCOutRad[i], CuspOVCHalfLen[i], 0.0, 2.0*M_PI);
				}
			  
			  if(i==0)                                 
				{
				  CuspOVCPosZ[i] = CenterOfTheCusp;}
			  else if(i==1 || i==2 ||(i>=5 && i<=9))   
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] + CuspOVCHalfLen[i-1] + CuspOVCHalfLen[i];
				}
			  else if(i==3)                            
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] + CuspOVCHalfLen[i-1] + CuspOVCrHalfLen[0];
				}
			  else if(i==4)
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] + CuspOVCrHalfLen[0] + CuspOVCHalfLen[i];
				}
			  else if(i==10)
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[0] - CuspOVCHalfLen[0] - CuspOVCHalfLen[i];
				}
			  else if(i==13)
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] - CuspOVCHalfLen[i-1] - CuspOVCrHalfLen[6];
				}
			  else if(i==14)
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] - CuspOVCrHalfLen[6] - CuspOVCHalfLen[i];
				}
			  else if(i==11 || i==12 || i==15 || i==16)
				{
				  CuspOVCPosZ[i] = CuspOVCPosZ[i-1] - CuspOVCHalfLen[i-1] - CuspOVCHalfLen[i];
				}
			  
			  CuspOVCLog[i]=new G4LogicalVolume(CuspOVC[i],hbarDetConst::GetMaterial("Sts"),CuspOVCname,0,0,0);
			  //CuspOVCLog[i]->SetFieldManager(cuspField->fieldMgr, allLocal);
			  CuspOVCPhy[i]=new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CuspOVCPosZ[i]),CuspOVCLog[i],CuspOVCname,WorldPhy->GetLogicalVolume(),false,0);
			  
			}
		}
	  // -- magnet coil
	  if(useCUSPMgCoil != 0)
		{
		  for (G4int i = 0; i < maxCuspMgCoil; i++) 
			{
			  G4String CuspMgCoilName = "CuspMgCoil" + numtostr(i+1);
			  CuspMgCoil[i]=new G4Tubs(CuspMgCoilName,CuspMgCoilInRad[i],CuspMgCoilOutRad[i],CuspMgCoilHalfWidth[i], 0.0, 2.0*M_PI);
			}
		  G4UnionSolid* CuspMgCoil00 = new G4UnionSolid("CuspMgCoil00", CuspMgCoil[0], CuspMgCoil[1], 0,
														G4ThreeVector(0.0*mm, 0.0*mm, CuspMgCoilHalfWidth[0] + CuspMgCoilHalfWidth[1] + 15.0*mm));
		  G4UnionSolid* CuspMgCoil01 = new G4UnionSolid("CuspMgCoil01", CuspMgCoil00, CuspMgCoil[3], 0,
														G4ThreeVector(0.0*mm, 0.0*mm, -CuspMgCoilHalfWidth[0] - CuspMgCoilHalfWidth[3] - 15.0*mm));
		  G4UnionSolid* CuspMgCoil02 = new G4UnionSolid("CuspMgCoil02", CuspMgCoil01, CuspMgCoil[2], 0,
														G4ThreeVector(0.0*mm, 0.0*mm,
																	  CuspMgCoilHalfWidth[0] + 2.0*CuspMgCoilHalfWidth[1] + CuspMgCoilHalfWidth[2] + 15.0*mm + 21.0*mm));
		  G4UnionSolid* CuspMagCoil = new G4UnionSolid("CuspMagCoil", CuspMgCoil02, CuspMgCoil[4], 0,
													   G4ThreeVector(0.0*mm, 0.0*mm,
																	 -CuspMgCoilHalfWidth[0] - 2.0*CuspMgCoilHalfWidth[3] - CuspMgCoilHalfWidth[4] - 15.0*mm - 21.0*mm));
		  
		  CuspMagCoilLog = new G4LogicalVolume(CuspMagCoil,hbarDetConst::GetMaterial("NbTiCable"),"CuspMagCoil",0,0,0);
		  //CuspMagCoilLog->SetFieldManager(cuspField->fieldMgr, allLocal);
		  CuspMagCoilPhys = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheCusp),CuspMagCoilLog,"CuspMagCoil",WorldPhy->GetLogicalVolume(),false,0);
		}

	  // -- magnetic shield
	  if(useCUSPMgShield != 0){
		for (G4int i = 0; i < maxCuspMgSh; i++) 
		  {
			G4String CuspMgShName = "CuspMgSh" + numtostr(i+1);
			CuspMgSh[i]=new G4Tubs(CuspMgShName,CuspMgShInRad[i], CuspMgShOutRad[i], CuspMgShHalfLen[i], 0.0, 2.0*M_PI);
		  }
		for (G4int i = 0; i < maxCuspMgShCn; i++) 
		  {
			G4String CuspMgShCnName = "CuspMgShCn" + numtostr(i+1);
			CuspMgShCn[i]=new G4Cons(CuspMgShCnName,CuspMgShCnFInRad[i],CuspMgShCnFOutRad[i],
									 CuspMgShCnRInRad[i],CuspMgShCnROutRad[i],CuspMgShCnHalfLen[i],0.0, 2.0*M_PI);
		  }
		G4SubtractionSolid* CuspMgSh00 = new G4SubtractionSolid("CuspMgSh00",CuspMgSh[0],CuspMgShCn[0]);
		G4SubtractionSolid* CuspMgSh01 = new G4SubtractionSolid("CuspMgSh01",CuspMgSh[1],CuspMgShCn[1]);
		G4SubtractionSolid* CuspMgSh02 = new G4SubtractionSolid("CuspMgSh02",CuspMgSh[2],CuspMgShCn[2]);
		G4SubtractionSolid* CuspMgSh03 = new G4SubtractionSolid("CuspMgSh03",CuspMgSh01,CuspMgSh00);
		G4UnionSolid* CuspMgSh04 = new G4UnionSolid("CuspMgSh04",CuspMgSh03,CuspMgSh02,0,
													G4ThreeVector(0.0*mm, 0.0*mm, CuspMgShHalfLen[0]+CuspMgShHalfLen[2]));
		G4SubtractionSolid* CuspMgShield = new G4SubtractionSolid("CuspMgShield",CuspMgSh04,CuspMgShCn[3],0,
																  G4ThreeVector(0.0*mm, 0.0*mm, 0.5*CuspMgShHalfLen[0]+0.5*CuspMgShHalfLen[2]));
		CuspMgShieldLog = new G4LogicalVolume(CuspMgShield,hbarDetConst::GetMaterial("Iron"),"CuspMgShield",0,0,0);  
		//CuspMgShieldLog->SetFieldManager(cuspField->fieldMgr, allLocal);
		CuspMgShieldPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheCusp+360.0*mm),
											CuspMgShieldLog,"CuspMgShield",WorldPhy->GetLogicalVolume(),false,0);
	  }
	  // -- track detector plascin
	  if(useCUSPTPlsci != 0)
		{
		  for (G4int i = 0; i < maxCuspTPlsci; i++)
			{
			  G4String CuspTPlsciName = "CuspTPlsci" + numtostr(i+1);
			  CuspTPlsci[i]=new G4Box(CuspTPlsciName,CuspTPlsciHalfXLen[i],CuspTPlsciHalfYLen[i],CuspTPlsciHalfZLen[i]);
			  CuspTPlsciLog[i]=new G4LogicalVolume(CuspTPlsci[i],hbarDetConst::GetMaterial("Scintillator"),CuspTPlsciName,0,0,0);
			  CuspTPlsciPhy[i]=new G4PVPlacement(0,CuspTPlsciPos[i],CuspTPlsciLog[i],CuspTPlsciName,WorldPhy->GetLogicalVolume(),false,0);
			  G4VisAttributes* orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));
			  CuspTPlsciLog[i]->SetVisAttributes(orangecolor);
			  if(sensCuspTPLASCIN[i] != 0)
				{
				  CuspTPlsciLog[i]->SetSensitiveDetector(scintiSD);
				}
			}
		}

  //************************ END ************************************
  
  
  // -- duct between sextupole and GV
  if(useSGVDUCT != 0)
	{
	  G4Tubs* SGVD00 = new G4Tubs("SGVD00",SGVD0InRad,SGVD0OutRad,SGVD0HalfLen,0.0,2.0*M_PI);
	  G4Tubs* SGVD01 = new G4Tubs("SGVD01",SGVD1InRad,SGVD1OutRad,SGVD1HalfLen,0.0,2.0*M_PI);
	  G4Tubs* SGVD02 = new G4Tubs("SGVD02",SGVD1InRad,SGVD1OutRad,SGVD1HalfLen,0.0,2.0*M_PI);
	  G4UnionSolid* SGVD03 = new G4UnionSolid("SGVD03",SGVD00,SGVD01,0,G4ThreeVector(0.0*mm,0.0*mm,SGVD0HalfLen+SGVD1HalfLen));
	  G4UnionSolid* SGVD04 = new G4UnionSolid("SGVD04",SGVD03,SGVD02,0,G4ThreeVector(0.0*mm,0.0*mm,-SGVD0HalfLen-SGVD1HalfLen));
	  SGVDLog = new G4LogicalVolume(SGVD04,hbarDetConst::GetMaterial("Sts"),"SGVD04",0,0,0);
	  SGVDPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheSGVD),SGVDLog,"SGVD04",WorldPhy->GetLogicalVolume(),false,0);
	}
  
  // -- Miki Lense
  if(useMikiLense != 0)
	{
	  if(useMLDUCT != 0)
		{
		  G4RotationMatrix* MLDuctRotX = new G4RotationMatrix();
		  G4RotationMatrix* MLDuctRotY = new G4RotationMatrix();
		  MLDuctRotX -> rotateX(0.5*M_PI);
		  MLDuctRotY -> rotateY(0.5*M_PI);
		  for(G4int i=0; i<maxMLDuctP; i++)
			{
			  G4String MLDuctPName = "MLDuctP" + numtostr(i+1);
			  MLDuctP[i] = new G4Tubs(MLDuctPName,MLDuctPInRad[i],MLDuctPOutRad[i],MLDuctPHalfLen[i],0.0,2.0*M_PI);
			}
		  G4ThreeVector MLDucttrans0 = G4ThreeVector(0.0*mm,-MLDuctPHalfLen[1], 0.0*mm); 
		  G4ThreeVector MLDucttrans1 = G4ThreeVector(0.0*mm,-110.0*mm, 0.0*mm); 
		  G4ThreeVector MLDucttrans2 = G4ThreeVector(0.0*mm,0.0*mm,MLDuctPHalfLen[0]+MLDuctPHalfLen[7]);
		  G4ThreeVector MLDucttrans3 = G4ThreeVector(0.0*mm,0.0*mm,-MLDuctPHalfLen[0]-MLDuctPHalfLen[8]);
		  G4ThreeVector MLDucttrans4 = G4ThreeVector(0.0*mm,-2.0*MLDuctPHalfLen[1]-MLDuctPHalfLen[6], 0.0*mm); 
		  //    G4ThreeVector MLDucttrans5 = G4ThreeVector(MLDuctPHalfLen[2]+MLDuctPHalfLen[9],-110.0*mm, 0.0*mm); 
		  //    G4ThreeVector MLDucttrans6 = G4ThreeVector(-1.0*MLDuctPHalfLen[2]-MLDuctPHalfLen[10],-110.0*mm, 0.0*mm); 
		  
		  G4UnionSolid* MLDuct00 = new G4UnionSolid("MLDuct00",MLDuctP[0],MLDuctP[1],MLDuctRotX,MLDucttrans0);
		  G4UnionSolid* MLDuct01 = new G4UnionSolid("MLDuct01",MLDuct00,MLDuctP[2],MLDuctRotY,MLDucttrans1);
		  G4SubtractionSolid* MLDuct02 = new G4SubtractionSolid("MLDuct02",MLDuct01,MLDuctP[3]);
		  G4SubtractionSolid* MLDuct03 = new G4SubtractionSolid("MLDuct03",MLDuct02,MLDuctP[4],MLDuctRotX,MLDucttrans0);
		  G4SubtractionSolid* MLDuct04 = new G4SubtractionSolid("MLDuct04",MLDuct03,MLDuctP[5],MLDuctRotY,MLDucttrans1);
		  G4UnionSolid* MLDuct05 = new G4UnionSolid("MLDuct05",MLDuct04,MLDuctP[7],0,MLDucttrans2);
		  G4UnionSolid* MLDuct06 = new G4UnionSolid("MLDuct06",MLDuct05,MLDuctP[8],0,MLDucttrans3);
		  G4UnionSolid* MLDuct07 = new G4UnionSolid("MLDuct07",MLDuct06,MLDuctP[6],MLDuctRotX,MLDucttrans4);
		  //    G4UnionSolid* MLDuct08 = new G4UnionSolid("MLDuct08",MLDuct07,MLDuctP[9],MLDuctRotY,MLDucttrans5);
		  //    G4UnionSolid* MLDuct = new G4UnionSolid("MLDuct",MLDuct08,MLDuctP[10],MLDuctRotY,MLDucttrans6);
		  
		  MLDuctLog = new G4LogicalVolume(MLDuct07,hbarDetConst::GetMaterial("Sts"),"MLDuct07",0,0,0);
		  MLDuctPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm, CenterOfTheMikiLense),MLDuctLog,"MLDuct07",WorldPhy->GetLogicalVolume(),false,0);
		}
	  if(useMLELE !=0)
		{
		  G4double ML_DBE = 2.0*mm;  // distance between electrodes
		  for(G4int i=0; i<maxMLEle; i++)
			{
			  G4String MLEleName = "MLEle" + numtostr(i+1);
			  MLEle[i] = new G4Tubs(MLEleName,MLEleInRad,MLEleOutRad,MLEleHalfLen,0.0,2.0*M_PI);
			}
		  for(G4int i=0; i<maxMLEleSub; i++)
			{
			  G4String MLEleSubName = "MLEleSub" + numtostr(i+1);
			  MLEleSub[i] = new G4Tubs(MLEleSubName,MLEleSubInRad,MLEleSubOutRad,MLEleSubHalfLen,1.25*M_PI,0.5*M_PI);
			}
		  G4UnionSolid* MLEle01 = new G4UnionSolid("MLEle01",MLEle[0],MLEle[1],0,G4ThreeVector(0.0*mm,0.0*mm,ML_DBE+2.0*MLEleHalfLen));
		  G4UnionSolid* MLEle02 = new G4UnionSolid("MLEle02",MLEle01,MLEle[2],0,G4ThreeVector(0.0*mm,0.0*mm,-ML_DBE-2.0*MLEleHalfLen));
		  G4UnionSolid* MLEle03 = new G4UnionSolid("MLEle03",MLEle02,MLEle[3],0,G4ThreeVector(0.0*mm,0.0*mm,2.0*ML_DBE+4.0*MLEleHalfLen));
		  G4SubtractionSolid* MLEle04 = new G4SubtractionSolid("MLEle04",MLEle03,MLEleSub[0],0,G4ThreeVector(0.0*mm,0.0*mm,4.25*mm));
		  G4SubtractionSolid* MLEle05 = new G4SubtractionSolid("MLEle05",MLEle04,MLEleSub[1],0,
															   G4ThreeVector(0.0*mm,0.0*mm,2.0*MLEleHalfLen+ML_DBE-4.25*mm));
		  MLEleLog = new G4LogicalVolume(MLEle05,hbarDetConst::GetMaterial("Aluminium"),"MLEle05",0,0,0);
		  MLElePhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheMikiLense-0.5*ML_DBE-MLEleHalfLen),MLEleLog,"MLEle05",WorldPhy->GetLogicalVolume(),false,0);
		  
		  G4Tubs* MLElesup00 = new G4Tubs("MLElesup00",43.0*mm,MLDuctPOutRad[3],4.0*MLEleHalfLen+1.5*ML_DBE,220.0/180.0*M_PI,100.0/180.0*M_PI);
		  G4Tubs* MLElesup01 = new G4Tubs("MLElesup01",41.0*mm,MLDuctPOutRad[3]+2.0*mm,30.0*mm+0.5*ML_DBE,220.0/180.0*M_PI,100.0/180.0*M_PI);
		  G4SubtractionSolid* MLElesup02 = new G4SubtractionSolid("MLElesup02",MLElesup00,MLElesup01);
		  MLElesupLog = new G4LogicalVolume(MLElesup02,hbarDetConst::GetMaterial("Macerite"),"MLElesup02",0,0,0);
		  MLElesupPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheMikiLense),MLElesupLog,"MLElesup02",WorldPhy->GetLogicalVolume(),false,0);
		}
	}  
  // --  duct between MLduct and sextupole
  if(useMLSDUCT != 0)
	{
	  G4int MLSDpr = 0;
	  G4RotationMatrix* MLSDRotY = new G4RotationMatrix();
	  MLSDRotY -> rotateY(0.5*M_PI);
	  for(G4int i=0; i<maxMLSD; i++)
		{
		  G4String MLSDName = "MLSD" + numtostr(i+1);
		  if(i>=0 && i<2)
			{MLSDpr = 0;}
		  else if(i>=2 && i<6)
			{MLSDpr = 1;}
		  else if(i>=6 && i<8)
			{MLSDpr = 2;}
		  MLSD[i] = new G4Tubs(MLSDName,MLSDInRad[MLSDpr],MLSDOutRad[MLSDpr],MLSDHalfLen[MLSDpr],0.0,2.0*M_PI);
		}
	  G4UnionSolid* MLSextD00 = new G4UnionSolid("MLSextD00",MLSD[0],MLSD[1],MLSDRotY,G4ThreeVector(0,0,0));
	  G4UnionSolid* MLSextD01 = new G4UnionSolid("MLSextD01",MLSextD00,MLSD[2],0,G4ThreeVector(0.0*mm,0.0*mm,MLSDHalfLen[0]+MLSDHalfLen[1]));
	  G4UnionSolid* MLSextD02 = new G4UnionSolid("MLSextD02",MLSextD01,MLSD[3],0,G4ThreeVector(0.0*mm,0.0*mm,-MLSDHalfLen[0]-MLSDHalfLen[1]));
	  G4UnionSolid* MLSextD03 = new G4UnionSolid("MLSextD03",MLSextD02,MLSD[4],MLSDRotY,
												 G4ThreeVector(MLSDHalfLen[0]+MLSDHalfLen[1],0.0*mm,0.0*mm));
	  G4UnionSolid* MLSextD04 = new G4UnionSolid("MLSextD04",MLSextD03,MLSD[5],MLSDRotY,
												 G4ThreeVector(-MLSDHalfLen[0]-MLSDHalfLen[1],0.0*mm,0.0*mm));
	  G4SubtractionSolid* MLSextD05 = new G4SubtractionSolid("MLSextD05",MLSextD04,MLSD[6]);
	  G4SubtractionSolid* MLSextD06 = new G4SubtractionSolid("MLSextD06",MLSextD05,MLSD[7],MLSDRotY,G4ThreeVector(0,0,0));
	  MLSDLog = new G4LogicalVolume(MLSextD06,hbarDetConst::GetMaterial("Sts"),"MLSextD06",0,0,0);
	  MLSDPhy = new G4PVPlacement(0,G4ThreeVector(0.0*mm,0.0*mm,CenterOfTheMLSD),MLSDLog,"MLSextD06",WorldPhy->GetLogicalVolume(),false,0);
	}
}

void hbarCUSPConst::SetBeamline0Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfTheKB0=inr;
  outerRadiusOfTheKB0=our;
  hightOfTheKB0=hi;
}

void hbarCUSPConst::SetBeamline0Placement(G4double x, G4double y, G4double z)
{
  beamline0Pos_x=x;
  beamline0Pos_y=y;
  beamline0Pos_z=z;
}

void hbarCUSPConst::SetBeamline1Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfTheKB1=inr;
  outerRadiusOfTheKB1=our;
  hightOfTheKB1=hi;
}

void hbarCUSPConst::SetBeamline1Placement(G4double x, G4double y, G4double z)
{
  beamline1Pos_x=x;
  beamline1Pos_y=y;
  beamline1Pos_z=z;
}

void hbarCUSPConst::SetCuspBoxDimension(G4double wi, G4double thi, G4double le)
{
  cuspWidth=wi;
  cuspThick=thi;
  cuspLength=le;
}

void hbarCUSPConst::SetCuspT1Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfCuspT1=inr;
  outerRadiusOfCuspT1=our;
  hightOfCuspT1=hi;
}

void hbarCUSPConst::SetCuspT2Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfCuspT2=inr;
  outerRadiusOfCuspT2=our;
  hightOfCuspT2=hi;
}

void hbarCUSPConst::SetCuspPlacement(G4double x, G4double y, G4double z)
{
  cuspPos_x=x;
  cuspPos_y=y;
  cuspPos_z=z;
}

void hbarCUSPConst::UseCBF(G4int num)
{
  
  if ((num >= 0) && (num < maxCBF)) 
	{
	  CBFpt = num;
	  useCBF[CBFpt] = true;
	} 
  else if (num == 0) 
	{
	  AddCBF();
	} 
  else 
	{
	  char temp[400];
	  sprintf(temp,"CBF number %d is out of range!", num);
	  throw hbarException(temp);
	}
}

void hbarCUSPConst::AddCBF()
{
  if (CBFpt < maxCBF) 
	{
	  CBFpt++;
	  useCBF[CBFpt] = true;
	} 
  else 
	{
	  throw hbarException("Cannot create CBF! There are too many of them.");
	}
}

void hbarCUSPConst::SetCBFInRad(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxCBF)) 
	{
	  CBFInRad[num] = val;
	}
}

void hbarCUSPConst::SetCBFOutRad(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxCBF)) 
	{
	  CBFOutRad[num] = val;
	}
}

void hbarCUSPConst::SetCBFLength(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxCBF)) 
	{
	  CBFLength[num] = val;
	}
}

void hbarCUSPConst::SetCBFPlacement(G4int num, G4double x, G4double y, G4double z)
{
  
  if ((num >= 0) && (num < maxCBF)) 
	{
	  CBFPos_x[num]=x;
	  CBFPos_y[num]=y;
	  CBFPos_z[num]=z;
	}
}

void hbarCUSPConst::SetGV1Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfTheGV1=inr;
  outerRadiusOfTheGV1=our;
  hightOfTheGV1=hi;
}

void hbarCUSPConst::SetRed0Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfTheRed0=inr;
  outerRadiusOfTheRed0=our;
  hightOfTheRed0=hi;
}

void hbarCUSPConst::SetBellDimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfTheBell=inr;
  outerRadiusOfTheBell=our;
  hightOfTheBell=hi;
}

void hbarCUSPConst::UseHDF(G4int num)
{
  
  if ((num >= 0) && (num < maxHDF)) 
	{
	  HDFpt = num;
	  useHDF[HDFpt] = true;
	} 
  else if (num == 0) 
	{
	  AddHDF();
	} 
  else 
	{
	  char temp[400];
	  sprintf(temp,"HDF number %d is out of range!", num);
	  throw hbarException(temp);
	}
}

void hbarCUSPConst::AddHDF()
{
  if (HDFpt < maxHDF) 
	{
	  HDFpt++;
	  useHDF[HDFpt] = true;
	} 
  else 
	{
	  throw hbarException("Cannot create HDF! There are too many of them.");
	}
}


void hbarCUSPConst::SetHDFInRad(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxHDF)) 
	{
	  HDFInRad[num] = val;
	}
}

void hbarCUSPConst::SetHDFOutRad(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxHDF)) 
	{
	  HDFOutRad[num] = val;
	}
}

void hbarCUSPConst::SetHDFLength(G4int num, G4double val)
{
  if ((num >= 0) && (num < maxHDF)) 
	{
	  HDFLength[num] = val;
	}
}

void hbarCUSPConst::SetHDFPlacement(G4int num, G4double x, G4double y, G4double z)
{
  
  if ((num >= 0) && (num < maxHDF)) 
	{
	  HDFPos_x[num]=x;
	  HDFPos_y[num]=y;
	  HDFPos_z[num]=z;
	}
}

void hbarCUSPConst::SetCh0Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfCh0=inr;
  outerRadiusOfCh0=our;
  hightOfCh0=hi;
}
void hbarCUSPConst::SetCh2Dimension(G4double inr, G4double our, G4double hi)
{
  innerRadiusOfCh2=inr;
  outerRadiusOfCh2=our;
  hightOfCh2=hi;
}
void hbarCUSPConst::SetGV1Placement(G4double x, G4double y, G4double z)
{
  gv1Pos_x=x;
  gv1Pos_y=y;
  gv1Pos_z=z;
}
void hbarCUSPConst::SetRed0Placement(G4double x, G4double y, G4double z)
{
  red0Pos_x=x;
  red0Pos_y=y;
  red0Pos_z=z;
}

void hbarCUSPConst::SetBellPlacement(G4double x, G4double y, G4double z)
{
  bellPos_x=x;
  bellPos_y=y;
  bellPos_z=z;
}

void hbarCUSPConst::SetCh0Placement(G4double x, G4double y, G4double z)
{
  ch0Pos_x=x;
  ch0Pos_y=y;
  ch0Pos_z=z;
}

void hbarCUSPConst::SetCh2Placement(G4double x, G4double y, G4double z)
{
  ch2Pos_x=x;
  ch2Pos_y=y;
  ch2Pos_z=z;
}

void hbarCUSPConst::UseCuspMPart(G4int num)
{
  if ((num>=0) && (num<maxCuspMPart))
	{
	  CuspMPartpt = num;
	  useCuspMPart[CuspMPartpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspMPart();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer,"CuspMPart number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspMPart()
{
  if (CuspMPartpt < maxCuspMPart) 
	{
	  CuspMPartpt++;
	  useCuspMPart[CuspMPartpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspMPart! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspMBDimension(G4int num, G4double xlen, G4double ylen, G4double zlen)
{
  if ((num >= 0) && (num < maxCuspMPart))
	{ 
	  CuspMBHalfXLen[num]=0.5*xlen; CuspMBHalfYLen[num]=0.5*ylen; CuspMBHalfZLen[num]=0.5*zlen;
	}
}

void hbarCUSPConst::SetCuspMTDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspMPart))
	{ 
	  CuspMTInRad[num]=0.5*ind; CuspMTOutRad[num]=0.5*oud; CuspMTHalfLen[num]=0.5*len;
	}
}

void hbarCUSPConst::SetCuspMHDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspMPart))
	{ 
	  CuspMHInRad[num]=0.5*ind; CuspMHOutRad[num]=0.5*oud; CuspMHHalfLen[num]=0.5*len;
	}
}

// -- MRE
void hbarCUSPConst::UseCuspMRE(G4int num)
{
  if ((num >= 0) && (num < maxCuspMRE))
	{
	  CuspMREpt = num;
	  useCuspMRE[CuspMREpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspMRE();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspMRE number %d is out of range!", num);
	  throw new hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspMRE()
{
  if (CuspMREpt < maxCuspMRE) 
	{
	  CuspMREpt++;
	  useCuspMRE[CuspMREpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspMRE! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspMREDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspMRE))
	{ 
	  CuspMREInRad[num]=0.5*ind; CuspMREOutRad[num]=0.5*oud; CuspMREHalfLen[num]=0.5*len;
	}
}

void hbarCUSPConst::SetCuspMREPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspMRE))
	{
	  CuspMREPos[num]=G4ThreeVector(x,y,z);
	}
}

void hbarCUSPConst::UseCuspMREsup(G4int num)
{
  if ((num >= 0) && (num < maxCuspMREsup))
	{
	  CuspMREsuppt = num;
	  useCuspMREsup[CuspMREsuppt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspMREsup();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspMREsup number %d is out of range", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspMREsup()
{
  if (CuspMREsuppt<maxCuspMREsup)
	{
	  CuspMREsuppt++;
	  useCuspMREsup[CuspMREsuppt] = true;
	}
  else 
	{
	  throw hbarException("Cannot create CuspMREsup! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspMREsupDimension(G4int num, G4double xlen, G4double ylen, G4double zlen)
{
  for(G4int i=4*num;i<(4*num+4);++i)
	{
	  CuspMREsupHalfXLen[i]=0.5*xlen; CuspMREsupHalfYLen[i]=0.5*ylen; CuspMREsupHalfZLen[i]=0.5*zlen;
	}
}

void hbarCUSPConst::SetCuspMREsupPlacement(G4int num,G4double x, G4double y, G4double z)
{
  for(G4int i=4*num; i<(4*num+4); ++i)
	{CuspMREsupPos[i]=G4ThreeVector(x,y,z);}
}

// -- UHV bore
void hbarCUSPConst::UseCuspUHV(G4int num)
{
  if ((num>=0) && (num<maxCuspUHV))
	{
	  CuspUHVpt = num;
	  useCuspUHV[CuspUHVpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspUHV();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspUHV number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspUHV()
{
  if (CuspUHVpt < maxCuspUHV) 
	{
	  CuspUHVpt++;
	  useCuspUHV[CuspUHVpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspUHV! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspUHVbDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < (maxCuspUHVb)))
	{
	  CuspUHVbInRad[num]=0.5*ind; CuspUHVbOutRad[num]=0.5*oud; CuspUHVbHalfLen[num]=0.5*len;
	}
}

void hbarCUSPConst::SetCuspUHVcnDimension(G4double foud, G4double roud, G4double len)
{
  CuspUHVcnFOutRad=0.5*foud; CuspUHVcnROutRad=0.5*roud; CuspUHVcnHalfLen=0.5*len;
}

void hbarCUSPConst::SetCuspUHVcnFrontInnerRadius(G4double find)
{
  CuspUHVcnFInRad=0.5*find;
}

void hbarCUSPConst::SetCuspUHVcnRearInnerRadius(G4double rind)
{
  CuspUHVcnRInRad=0.5*rind;
}

void hbarCUSPConst::SetCuspUHVPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspUHV))
	{
	  CuspUHVPos[num]=G4ThreeVector(x,y,z);
	}
}

// -- copper collar
void hbarCUSPConst::SetCuspCuColDimension(G4double ind, G4double oud, G4double len)
{
  CuspCuColInRad=0.5*ind; 
  CuspCuColOutRad=0.5*oud; 
  CuspCuColHalfLen=0.5*len;
}

void hbarCUSPConst::SetCuspCuColPlacement(G4double x, G4double y, G4double z)
{
  CuspCuColPos=G4ThreeVector(x,y,z);
} 

// -- copper strip
void hbarCUSPConst::UseCuspCuSt(G4int num)
{
  if ((num>=0)&&(num<maxCuspCuSt))
	{
	  CuspCuStpt = num;
	  useCuspCuSt[CuspCuStpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspCuSt();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspCuSt number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspCuSt()
{
  if (CuspCuStpt<maxCuspCuSt)
	{
	  CuspCuStpt++;
	  useCuspCuSt[CuspCuStpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspCuSt! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspCuStDimension(G4int num, G4double xlen, G4double ylen, G4double zlen)
{
  for(G4int i=8*num; i<(8*num+8); ++i)
	{
	  CuspCuStHalfXLen[i]=0.5*xlen; 
	  CuspCuStHalfYLen[i]=0.5*ylen; 
	  CuspCuStHalfZLen[i]=0.5*zlen;
	}
}

// -- poll
void hbarCUSPConst::UseCuspPol(G4int num)
{
  if ((num >= 0) && (num < maxCuspPol))
	{
	  CuspPolpt = num;
	  useCuspPol[CuspPolpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspPol();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspPol number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspPol()
{
  if (CuspPolpt<maxCuspPol)
	{
	  CuspPolpt++;
	  useCuspPol[CuspPolpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspPol! There are too many of them.");
	}
}
void hbarCUSPConst::SetCuspPolDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspPol))
	{
	  CuspPolInRad[num]=0.5*ind; 
	  CuspPolOutRad[num]=0.5*oud; 
	  CuspPolHalfLen[num]=0.5*len;
	  CuspPolInRad[num+8]=0.5*ind; 
	  CuspPolOutRad[num+8]=0.5*oud; 
	  CuspPolHalfLen[num+8]=0.5*len;
	}
}
void hbarCUSPConst::SetCuspPolPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspPol)) 
	{
	  CuspPolPos[num]=G4ThreeVector(x,y,z);
	}
}
// -- copper plate
void hbarCUSPConst::UseCuspCuPlate(G4int num)
{
  if ((num>=0) && (num<maxCuspCuPlate))
	{
	  CuspCuPlatept = num;
	  useCuspCuPlate[CuspCuPlatept] = true;
	}
  else if (num == 0) 
	{
	  AddCuspCuPlate();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspCuPlate number %d is out of range!", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspCuPlate()
{
  if (CuspCuPlatept<maxCuspCuPlate)
	{
	  CuspCuPlatept++;
	  useCuspCuPlate[CuspCuPlatept] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspCuPlate! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspCuPlateDimension(G4int num, G4double xlen, G4double ylen, G4double zlen)
{
  if ((num >= 0) && (num < maxCuspCuPlate))
	{
	  CuspCuPlateHalfXLen[num]=0.5*xlen; 
	  CuspCuPlateHalfYLen[num]=0.5*ylen; 
	  CuspCuPlateHalfZLen[num]=0.5*zlen;
	}
}

void hbarCUSPConst::SetCuspCuPlatePlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspCuPlate)) 
	{
	  CuspCuPlatePos[num]=G4ThreeVector(x,y,CenterOfTheCusp+z);
	}
}

// -- thin SUS band 0.3mm
void hbarCUSPConst::UseCuspSUSband(G4int num)
{
  if ((num >= 0) && (num < maxCuspSUSband))
	{
	  CuspSUSbandpt = num;
	  useCuspSUSband[CuspSUSbandpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspSUSband();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspSUSband number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddCuspSUSband()
{
  if (CuspSUSbandpt < maxCuspSUSband) 
	{
	  CuspSUSbandpt++;
	  useCuspSUSband[CuspSUSbandpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspSUSband! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspSUSbandDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  for(G4int i=2*num; i<(2*num+2); ++i)
	{
	  CuspSUSbandInRad[i]=0.5*ind; 
	  CuspSUSbandOutRad[i]=0.5*oud; 
	  CuspSUSbandHalfLen[i]=0.5*len;
	}
}

void hbarCUSPConst::SetCuspSUSbandPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspSUSband)) 
	{
	  CuspSUSbandPos[num]=G4ThreeVector(x,y,z);
	}
}

// -- copper cylinder (radiation shield)
void hbarCUSPConst::SetCuspRadShieldDimension(G4double ind, G4double oud, G4double len)
{
  CuspRadShieldInRad=0.5*ind; 
  CuspRadShieldOutRad=0.5*oud; 
  CuspRadShieldHalfLen=0.5*len;
}

void hbarCUSPConst::SetCuspRadShieldPlacement(G4double x, G4double y, G4double z)
{
  CuspRadShieldPos = G4ThreeVector(x,y,z);
}
// -- OVC 
void hbarCUSPConst::UseCuspOVC(G4int num)
{
  if ((num >= 0) && (num < maxCuspOVC))
	{
	  CuspOVCpt = num;
	  useCuspOVC[CuspOVCpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspOVC();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspOVC number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspOVC()
{
  if (CuspOVCpt < maxCuspOVC)
	{
	  CuspOVCpt++;
	  useCuspOVC[CuspOVCpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspOVC! There are too many of them.");
	}
}

void hbarCUSPConst::UseCuspOVCr(G4int num)
{
  if ((num >= 0) && (num < maxCuspOVCr))
	{
	  CuspOVCrpt = num;
	  useCuspOVCr[CuspOVCrpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspOVCr();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "CuspOVCr number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspOVCr()
{
  if (CuspOVCrpt < maxCuspOVCr)
	{
	  CuspOVCrpt++;
	  useCuspOVCr[CuspOVCrpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create CuspOVCr! There are too many of them.");
	}
}
void hbarCUSPConst::SetCuspOVCDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspOVC))
	{
	  CuspOVCInRad[num]=0.5*ind; 
	  CuspOVCOutRad[num]=0.5*oud; 
	  CuspOVCHalfLen[num]=0.5*len;
	}
}
void hbarCUSPConst::SetCuspOVCrDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspOVCr))
	{
	  CuspOVCrInRad[num]=0.5*ind; 
	  CuspOVCrOutRad[num]=0.5*oud; 
	  CuspOVCrHalfLen[num]=0.5*len;
	}
}
void hbarCUSPConst::SetCuspOVCPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspOVC)) 
	{
	  CuspOVCPos[num]=G4ThreeVector(x,y,z);
	}
}
// -- magnet coil
void hbarCUSPConst::UseCuspMgCoil(G4int num)
{
  if ((num >= 0) && (num < maxCuspMgCoil))
	{
	  CuspMgCoilpt = num;
	  useCuspMgCoil[CuspMgCoilpt] = true;
	}
  else if (num == 0) 
	{ 
	  AddCuspMgCoil();
	}
  else 
	{ 
	  char buffer[400];
	  sprintf(buffer, "CuspMgCoil number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspMgCoil()
{
  if (CuspMgCoilpt < maxCuspMgCoil) 
	{
	  CuspMgCoilpt++;
	  useCuspMgCoil[CuspMgCoilpt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create CuspMgCoil! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspMgCoilDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if (num==0)
	{
	  CuspMgCoilInRad[num]=0.5*ind; 
	  CuspMgCoilOutRad[num]=0.5*oud; 
	  CuspMgCoilHalfWidth[num]=0.5*len;
	}
  else if(num!=0)
	{
	  CuspMgCoilInRad[num]=0.5*ind; 
	  CuspMgCoilOutRad[num]=0.5*oud; 
	  CuspMgCoilHalfWidth[num]=0.5*len;
	  CuspMgCoilInRad[num+2]=0.5*ind; 
	  CuspMgCoilOutRad[num+2]=0.5*oud; 
	  CuspMgCoilHalfWidth[num+2]=0.5*len;
	}
}
void hbarCUSPConst::SetCuspMgCoilPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspMgCoil)) 
	{ 
	  CuspMgCoilPos[num]=G4ThreeVector(x,y,z);
	}
}
// -- magnetic shield
void hbarCUSPConst::UseCuspMgSh(G4int num)
{
  if ((num >= 0) && (num < maxCuspMgSh))
	{
	  CuspMgShpt = num;
	  useCuspMgSh[CuspMgShpt] = true;
	}
  else if (num == 0) 
	{
	  AddCuspMgSh();
	}
  else 
	{ 
	  char buffer[400];
	  sprintf(buffer, "CuspMgSh number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspMgSh()
{
  if (CuspMgShpt < maxCuspMgSh)
	{
	  CuspMgShpt++;
	  useCuspMgSh[CuspMgShpt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create CuspMgSh! There are too many of them.");
	}
}
void hbarCUSPConst::SetCuspMgShDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxCuspMgSh))
	{
	  CuspMgShInRad[num]=0.5*ind; 
	  CuspMgShOutRad[num]=0.5*oud; 
	  CuspMgShHalfLen[num]=0.5*len;
	}
}
void hbarCUSPConst::UseCuspMgShCn(G4int num)
{
  if ((num >= 0) && (num < maxCuspMgShCn))
	{
	  CuspMgShCnpt = num;
	  useCuspMgShCn[CuspMgShCnpt] = true;
	}
  else if (num == 0) 
	{ 
	  AddCuspMgShCn();
	}
  else 
	{ 
	  char buffer[400];
	  sprintf(buffer, "CuspMgShCn number %d is out of range", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspMgShCn()
{
  if (CuspMgShCnpt < maxCuspMgShCn)
	{
	  CuspMgShCnpt++;
	  useCuspMgShCn[CuspMgShCnpt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create CuspMgShCn! There are too many of them.");
	}
}
void hbarCUSPConst::SetCuspMgShCnDimension(G4int num,G4double foud, G4double roud, G4double len)
{
  if ((num >= 0) && (num < maxCuspMgShCn))
	{
	  CuspMgShCnFOutRad[num]=0.5*foud; 
	  CuspMgShCnROutRad[num]=0.5*roud; 
	  CuspMgShCnHalfLen[num]=0.5*len;
	}
}
void hbarCUSPConst::SetCuspMgShCnFrontInnerRadius(G4int num,G4double find)
{
  if ((num >= 0) && (num < maxCuspMgShCn))
	{ 
	  CuspMgShCnFInRad[num] = 0.5*find; 
	}
}
void hbarCUSPConst::SetCuspMgShCnRearInnerRadius(G4int num,G4double rind)
{
  if ((num >= 0) && (num < maxCuspMgShCn))
	{ 
	  CuspMgShCnRInRad[num] = 0.5*rind; 
	}
}
// -- cusp track detector 
void hbarCUSPConst::UseCuspTPlsci(G4int num)
{
  if ((num >= 0) && (num < maxCuspTPlsci))
	{
	  CuspTPlscipt = num;
	  useCuspTPlsci[CuspTPlscipt] = true;
	}
  else if (num == 0) 
	{ 
	  AddCuspTPlsci();
	}
  else 
	{ 
	  char buffer[400];
	  sprintf(buffer, "CuspTPlsci number %d is out of range", num);
	  throw hbarException(buffer);
	}
}
void hbarCUSPConst::AddCuspTPlsci()
{
  if (CuspTPlscipt < maxCuspTPlsci)
	{
	  CuspTPlscipt++;
	  useCuspTPlsci[CuspTPlscipt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create CuspTPlsci! There are too many of them.");
	}
}

void hbarCUSPConst::SetCuspTPlsciDimension(G4int num, G4double xlen, G4double ylen, G4double zlen)
{
  if ((num >= 0) && (num < maxCuspTPlsci))
	{
	  CuspTPlsciHalfXLen[num]=0.5*xlen; 
	  CuspTPlsciHalfYLen[num]=0.5*ylen; 
	  CuspTPlsciHalfZLen[num]=0.5*zlen;
	}
}

void hbarCUSPConst::SetCuspTPlsciPlacement(G4int num, G4double x, G4double y, G4double z)
{
  if ((num >= 0) && (num < maxCuspTPlsci))
	{
	  CuspTPlsciPos[num] = G4ThreeVector(x,y,CenterOfTheCusp+z);
	}
}

// -- duct between sextupole and GV
void hbarCUSPConst::SetSGVD0Dimension(G4double ind, G4double oud, G4double len)
{
  SGVD0InRad=0.5*ind; 
  SGVD0OutRad=0.5*oud; 
  SGVD0HalfLen=0.5*len;
}

void hbarCUSPConst::SetSGVD1Dimension(G4double ind, G4double oud, G4double len)
{
  SGVD1InRad=0.5*ind; 
  SGVD1OutRad=0.5*oud; 
  SGVD1HalfLen=0.5*len;
}

// -- duct between MLduct and sextupole
void hbarCUSPConst::UseMLSD(G4int num)
{
  if ((num >= 0) && (num < maxMLSD))
	{
	  MLSDpt = num;
	  useMLSD[MLSDpt] = true;
	}
  else if (num == 0) 
	{ 
	  AddMLSD();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "MLSD number %d is out of range.", num);
	  throw hbarException(buffer); 
	}
}

void hbarCUSPConst::AddMLSD()
{
  if (MLSDpt < maxMLSD)
	{
	  MLSDpt++;
	  useMLSD[MLSDpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create MLSD! There are too many of them.");
	}
}

void hbarCUSPConst::SetMLSDDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxMLSD))
	{
	  MLSDInRad[num]=0.5*ind; 
	  MLSDOutRad[num]=0.5*oud; 
	  MLSDHalfLen[num]=0.5*len;
	}
}

// -- Miki Lense 
void hbarCUSPConst::UseMLDuctP(G4int num)
{
  if ((num >= 0) && (num < maxMLDuctP))
	{
	  MLDuctPpt = num;
	  useMLDuctP[MLDuctPpt] = true;
	}
  else if (num == 0) 
	{ 
	  AddMLDuctP();
	}
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "MLDuctP number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarCUSPConst::AddMLDuctP()
{
  if (MLDuctPpt < maxMLDuctP)
	{
	  MLDuctPpt++;
	  useMLDuctP[MLDuctPpt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create MLDuctP! There are too many of them.");
	}
}

void hbarCUSPConst::SetMLDuctPDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxMLDuctP))
	{
	  MLDuctPInRad[num]=0.5*ind; 
	  MLDuctPOutRad[num]=0.5*oud; 
	  MLDuctPHalfLen[num]=0.5*len;
	  if(num==7)
		{
		  MLDuctPInRad[num+1]=0.5*ind; 
		  MLDuctPOutRad[num+1]=0.5*oud; 
		  MLDuctPHalfLen[num+1]=0.5*len;
		}
	}
}

void hbarCUSPConst::SetMLEleDimension(G4double ind, G4double oud, G4double len)
{
  MLEleInRad=0.5*ind; 
  MLEleOutRad=0.5*oud; 
  MLEleHalfLen=0.5*len;
}
void hbarCUSPConst::SetMLEleSubDimension(G4double ind, G4double oud, G4double len)
{
  MLEleSubInRad=0.5*ind; 
  MLEleSubOutRad=0.5*oud; 
  MLEleSubHalfLen=0.5*len;
}

void hbarCUSPConst::SetCenterOfTheSGVD(G4double val)
{
  CenterOfTheSGVD = CenterOfTheCusp+val;
}

void hbarCUSPConst::SetCenterOfTheMLSD(G4double val)
{
  CenterOfTheMLSD = CenterOfTheCusp+val;
}

void hbarCUSPConst::SetCenterOfTheMikiLense(G4double val)
{
  CenterOfTheMikiLense = CenterOfTheCusp+val;
}

