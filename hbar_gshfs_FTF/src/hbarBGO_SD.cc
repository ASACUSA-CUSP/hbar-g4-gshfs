// USER //
#include "hbarBGO_SD.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

//FFT
#include "TH1.h"
#include "TVirtualFFT.h"


// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

hbarBGO_SD::hbarBGO_SD(G4String SDname)
  : G4VSensitiveDetector(SDname), BGOhitCollection(0)
{
  BGOName = SDname;
  G4String collName = SDname + "collection";
  collectionName.insert(SDname);
  BGOhitCollection = NULL;
  //localcptconst = cptconst;
  TotalEnergy = 0.0;
  bgohit = false;
  //std::cout << "#####################################hallo sd const " << std::endl;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
hbarBGO_SD::~hbarBGO_SD()
{
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

void hbarBGO_SD::Initialize(G4HCofThisEvent* HCE)
{
  //G4cout << "create new HitCollection " << GetName() << " " << collectionName[0] << G4endl;
  BGOhitCollection = new hbarDetectorHitsCollection(GetName(), collectionName[0]);

  static G4int HCID = -1;
  //if (HCID<0) { HCID = GetCollectionID(0); }
  if (HCID<0)
    { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(BGOhitCollection); }
  HCE->AddHitsCollection(HCID, BGOhitCollection);

 /*static int HCID = -1;
  scintiCollection = new hbarDetectorHitsCollection
                      (SensitiveDetectorName,collectionName[0]);

  if (HCID < 0) {
     HCID = GetCollectionID(0);
  }
  HCE->AddHitsCollection(HCID, scintiCollection);
*/

}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
G4bool hbarBGO_SD::ProcessHits(G4Step * aStep, G4TouchableHistory* ROhist)
{
  G4String part_name = track->GetParticleDefinition()->GetParticleName();

  if(part_name == "opticalphoton") {  
  G4double edep = aStep->GetTotalEnergyDeposit();
  hbarDetectorHit* newHit = new hbarDetectorHit();

  newHit->SetEnergyDeposit( edep );
  newHit->SetTime(((aStep->GetPreStepPoint()->GetGlobalTime() + aStep->GetPostStepPoint()->GetGlobalTime())/2.0)/ns);
  newHit->SetKineticEnergy(aStep->GetPreStepPoint()->GetKineticEnergy() );
  newHit->SetPosition( aStep->GetPreStepPoint()->GetPosition() );
  newHit->SetMomentumdir(aStep->GetPreStepPoint()->GetMomentumDirection());
  newHit->SetParticleName( aStep->GetTrack()->GetDefinition()->GetParticleName() );


  BGOhitCollection->insert( newHit );

  }
  if (aStep->GetTotalEnergyDeposit() <= 0.) return false;

  return true;






/*
  G4TouchableHandle touchable = step->GetPreStepPoint()->GetTouchableHandle();
  track = step->GetTrack();
  int evtnumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4String volume_name = track->GetVolume()->GetName();
  G4String part_name = track->GetParticleDefinition()->GetParticleName();
  
 // G4cout << track->GetLogicalVolumeAtVertex()->GetName()   << "      " << track->GetParticleDefinition()->GetParticleName() << G4endl;
  //G4cout << volume_name   << "      " << track->GetParticleDefinition()->GetParticleName() << G4endl;
  double Etot = step->GetTotalEnergyDeposit(); 
  TotalEnergy = 0.0;

  // for not optical photons:
if (step->GetTotalEnergyDeposit() <= 0.) return false;
  else {
		bgohit = true;
  		vector<double> row;
 		 row.push_back(evtnumber);
  		row.push_back(Etot/MeV);
 		 Edep_vector.push_back(row);


 		 return true;
	}


// for optical photons:
if(part_name == "opticalphoton") {  

		track->SetTrackStatus(fKillTrackAndSecondaries);
		vector<double> row;
		row.push_back(evtnumber);
  		row.push_back(Etot/MeV);
		Edep_vector.push_back(row);
	}

  if (step->GetTotalEnergyDeposit() <= 0.) return false;

  return true;
*/
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
void hbarBGO_SD::EndOfEvent(G4HCofThisEvent* HE)
{
  int evtnumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  std::string strev;
  std::stringstream out;
  out << evtnumber;
  strev = out.str();
//  for(int i=0; i<Edep_vector.size(); i++) TotalEnergy += Edep_vector[i]/MeV; 
// G4cout << "***Event: " << evtnumber << " ... in '" << GetName() << ", Total Energy " << TotalEnergy/MeV  << G4endl; //"	Total Charge (C): " << TotalCharge << "	ADC: " << ADCNumber << G4endl;
 
 //G4cout << "EndOfEvent method of SD '" << GetName() << "' called. " << GetNumberOfEvent() << G4endl;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...


