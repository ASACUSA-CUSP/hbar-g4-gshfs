#include "hbarHydrogen.hh"

hbarHydrogen* hbarHydrogen::theInstance = 0;

hbarHydrogen* hbarHydrogen::Definition()
{
  if (theInstance !=0) 
    return theInstance;

  const G4String name = "hydrogen";
  
  // Search in particle table for name.
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);

  if (anInstance ==0)
  {
    anInstance = new hbarHydrogen(name);
  }
  
  theInstance = reinterpret_cast<hbarHydrogen*>(anInstance);

  return theInstance;

}
hbarHydrogen::hbarHydrogen(const G4String& name)
  :hbarHydrogenLike(name)
{
  
}


