#include "hbarCavityField.hh"

//#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4EqEMFieldWithSpin.hh"
#include "G4ChordFinder.hh"
#include "G4PropagatorInField.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4ClassicalRK4.hh"
#include <string>

//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
hbarCavityField::hbarCavityField() : G4ElectroMagneticField() {
	fieldMgr = new G4FieldManager(this);	
	////////// Balint told me in his mail, that i will need this: 
	//fEquation = new hbarEqSextupoleField(this);
	fMinStep     = 0.1*mm ; // minimal step of 10 mm is default
	fStepperType = 4 ;      // ClassicalRK4 is default stepper
	fMinMiss     = 0.1*mm ; // minimal miss distance
	allowMajorana = "off";
	useAngleDiff = "off";
	meanB_val = 0.0;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
hbarCavityField::~hbarCavityField() {
	if(fieldMgr) delete fieldMgr;
	//if(fEquation) delete fEquation;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void hbarCavityField::Initialize(const char* filename) {
	// Read file
	G4cout << "Reading cavity field map from ASCII file " << filename << G4endl;
	std::ifstream inFile(filename); 
	if (!inFile) std::cout << "Error in opening file: " << filename << std::endl; 
	numberoflines=0;
	// count number of lines in file
	numberoflines = std::count(std::istreambuf_iterator<char>(inFile), 
	std::istreambuf_iterator<char>(), '\n');
	inFile.close();
	numberoflines--;
	
	MagField = new double*[numberoflines];
	for(int i=0; i<numberoflines; i++) MagField[i] = new double[columns];

	ReadFileToMatrix(MagField, filename);

	SearchTree = std::tr1::shared_ptr<KDNode>(new KDNode(MagField,numberoflines,0));
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void hbarCavityField::ReadFileToMatrix(double** MagField, const char* filename) {
	std::ifstream inFile(filename); 
	if (!inFile) std::cout << "Error in opening file: " << filename << std::endl; 
	
	double x,y,z,Bx=0,By=0,Bz=0;
	int lcount=0;
	double dump=0;
	
	inFile.clear(); // clear eof flag
	inFile.seekg(1, std::ios::beg); // 0 means: go back to firstline, 1 means: go to second line

	std::cout << "file name: " << filename << std::endl;             
	std::cout << "# of lines: "<< numberoflines << std::endl;

	while(!inFile.eof() && lcount != numberoflines) {
		inFile >> x >> y >> z >> dump >> Bx >> By >> Bz; // >> dump >> dump >> dump;
		//Michi Heiss fieldmap: Bx is in this case the mean value
		//inFile >> x >> y >> z >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> Bx >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump >> dump;
		//std::cout << "x "<< x << " y " << y " z " << z << " B " << Bx << std::endl;
		
		//skipcolumn(inFile, 2);
		MagField[lcount][0] = x*100;
		MagField[lcount][1] = y*100;
		MagField[lcount][2] = z*100;
		MagField[lcount][3] = Bx;
		MagField[lcount][4] = By;
		MagField[lcount][5] = Bz;

		meanB_val += sqrt(Bx*Bx + By*By + Bz*Bz);
				
		lcount++; 
	}
	meanB_val /=numberoflines;
	std::cout << "file: " << filename <<" read. mean value: " << meanB_val << std::endl; 
    inFile.close();
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> hbarCavityField::GetSearchTree(){
	return SearchTree;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
int hbarCavityField::Getnumberoflines() {
	return numberoflines;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
void hbarCavityField::GetFieldValue(const G4double* Point, G4double* Bfield) const {
	//field is really field[6]: Bx,By,Bz,Ex,Ey,Ez.
	//point[] is in global coordinates: x,y,z,t.
	double lenUnit = mm;
	
	double* point = new double[3];
	point[0] = Point[0]/lenUnit;
	point[1] = Point[1]/lenUnit;
	point[2] = Point[2]/lenUnit;
	//z = (Point[2] - fZAxisOffset)/lenUnit;

	// set magnetic field:
	double* Bf = new double[3]; 	
	double neighbours[neighboursnumber][columns];	
	
	std::tr1::shared_ptr<KDNode> tempST(new KDNode());
	tempST = SearchTree;
	std::tr1::shared_ptr<KDNode> tempNN0(new KDNode());
	/*std::tr1::shared_ptr<KDNode> tempNN1(new KDNode());
	std::tr1::shared_ptr<KDNode> tempNN2(new KDNode());
	std::tr1::shared_ptr<KDNode> tempNN3(new KDNode());*/

		tempNN0 = Nearest_Neighbour(tempST, point);
	//	tempNN0->Setchecked(true);
		for(int j=0;j<columns;j++) neighbours[0][j] = tempNN0->GetPoint(j);
/*		
	//	std::cout << "********* gefunden:	" << neighbours[0][0] << "	" << neighbours[0][1] << "	" << neighbours[0][2] << std::endl;

		tempNN1 = Nearest_Neighbour(tempST, point);
		tempNN1->Setchecked(true);
		for(int j=0;j<columns;j++) neighbours[1][j] = tempNN1->GetPoint(j);
		
	//	std::cout << "********* gefunden:	" << neighbours[1][0] << "	" << neighbours[1][1] << "	" << neighbours[1][2] << std::endl;

		tempNN2 = Nearest_Neighbour(tempST, point);
		tempNN2->Setchecked(true);
		for(int j=0;j<columns;j++) neighbours[2][j] = tempNN2->GetPoint(j);
		
	//	std::cout << "********* gefunden:	" << neighbours[2][0] << "	" << neighbours[2][1] << "	" << neighbours[2][2] << std::endl;
		
		tempNN3 = Nearest_Neighbour(tempST, point);
		for(int j=0;j<columns;j++) neighbours[3][j] = tempNN3->GetPoint(j);
	
	//	std::cout << "********* gefunden:	" << neighbours[3][0] << "	" << neighbours[3][1] << "	" << neighbours[3][2] << std::endl;
	
		tempNN0->Setchecked(false);
		tempNN1->Setchecked(false);
		tempNN2->Setchecked(false);
	*/
	//Bf = Find_Field(point,neighbours,neighboursnumber);
	

	Bfield[0] = neighbours[0][3]*tesla;
	Bfield[1] = neighbours[0][4]*tesla;
	Bfield[2] = neighbours[0][5]*tesla;
	// set magnetic field
	//Bfield[0] = Bf[0]*tesla;
	//Bfield[1] = Bf[1]*tesla;
	//Bfield[2] = Bf[2]*tesla;
	
	// set electric field to zero:
	Bfield[3] = 0.0*(volt/m);
	Bfield[4] = 0.0*(volt/m);
	Bfield[5] = 0.0*(volt/m);
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
double* hbarCavityField::Find_Field(double* point, double neighbours[][columns], int numberofneighbours) const {
	double* dist = new double[numberofneighbours];
	for(int i=0;i<numberofneighbours;i++) dist[i]=0.0;	
	double sum = 0;
	double* B = new double[3];
	for(int i=0;i<3;i++) B[i]=0.0;
	for(int j=0; j<numberofneighbours; j++) {
		dist[j] = 1/Distance(point,neighbours[j]);
		sum += dist[j];
	}	
	for(int i=0; i<3; i++) for(int j=0; j<numberofneighbours; j++) B[i] += neighbours[j][3+i]*(dist[j])/sum;
	return B;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> hbarCavityField::Nearest_Neighbour(std::tr1::shared_ptr<KDNode> tree, double* value) const {	
	std::tr1::shared_ptr<KDNode> approx =std::tr1::shared_ptr<KDNode>(new KDNode());
	approx = Approx_Neighbour(tree,value); // calculate approximate neighbour
	return Check_Sphere(tree,approx,value); // return node after sphere is checked
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> hbarCavityField::Approx_Neighbour(std::tr1::shared_ptr<KDNode> current, double* x) const {
	if(current->GetRightChild().get()==NULL) return current; // if end of tree is reached, return node
	if(current->GetRightChild()->Getchecked() && current->GetLeftChild()->Getchecked()) current->Setchecked(true); // if both children are checked, set node.checked 1

	//if point[Axis] of current node is smaller and the right child is not checked, then search branch of right child:
	if(current->GetPoint(current->GetAxis())<x[current->GetAxis()]&&(!current->GetRightChild()->Getchecked())) return Approx_Neighbour(current->GetRightChild(),x);//if left child is checked, search branch of right child: 									 		                           
	else if(current->GetLeftChild()->Getchecked()) return Approx_Neighbour(current->GetRightChild(),x);
	//else, search branch of left child:
	else return Approx_Neighbour(current->GetLeftChild(),x);
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
std::tr1::shared_ptr<KDNode> hbarCavityField::Check_Sphere(std::tr1::shared_ptr<KDNode> node, std::tr1::shared_ptr<KDNode> approx, double* x) const {
	double dist = Distance(approx->GetPoint(),x);		 // calculate distance from point saved in approx and x, the point we are looking for

	if(node->GetLeftChild()==NULL)  {
		if(Distance(node->GetPoint(),x)<dist) approx = node;
		return approx;       //return approx at leaf 
	}
	int axis = node->GetAxis();
	if(fabs(node->GetPoint(axis)-x[axis])<dist) {   //check if hyperplane intersects hypersphere
		std::tr1::shared_ptr<KDNode> nright(Check_Sphere(node->GetRightChild(),approx,x));		// if yes, then search branches for intersection
		std::tr1::shared_ptr<KDNode> nleft(Check_Sphere(node->GetLeftChild(),approx,x));	

		if(nright->Getchecked()&&nleft->Getchecked()) node->Setchecked(true);

		if(nright->Getchecked()) return nleft;
		if(nleft->Getchecked()) return nright;
		
		if(Distance(nright->GetPoint(),x)<Distance(nleft->GetPoint(),x)) return nright; // if distance between nright and x is smaller
		else return nleft;																
	}
	else {
		// if there is no intersection, check whether component point[Axis] of current node is larger or smaller than x[Axis]...
		if((node->GetRightChild()->Getchecked())&&(node->GetLeftChild()->Getchecked())) node->Setchecked(true);
		if(node->GetPoint(axis)<x[axis]&&!(node->GetRightChild()->Getchecked())) return Check_Sphere(node->GetRightChild(),approx,x); 
		else if(node->GetLeftChild()->Getchecked()) return Check_Sphere(node->GetRightChild(),approx,x);
		else return Check_Sphere(node->GetLeftChild(),approx,x); //... and search corresponding branch
	}
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
double hbarCavityField::Distance(double* x, double* y) const {
	double distance=0;
	for(int i=0; i<DIM; i++) distance += (x[i]-y[i])*(x[i]-y[i]);
	distance = sqrt(distance);
	return distance;
}
//XOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXOXO//
