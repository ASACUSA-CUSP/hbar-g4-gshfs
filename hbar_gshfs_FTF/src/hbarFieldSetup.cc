#include "hbarFieldSetup.hh"
#include "hbarDetConst.hh"
#include "hbarEqSextupoleField.hh"
#include "hbarFieldMess.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" // Added by Clemens
#include "G4MagIntegratorDriver.hh"

#include "G4UniformMagField.hh"
#include "G4MagneticField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4ChordFinder.hh"
#include "G4PropagatorInField.hh"

#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"
#include "Randomize.hh"

//////////////////////////////////////////////////////////////////////////
//
//  Constructors:

hbarFieldSetup::hbarFieldSetup(hbarDetConst *aDet)
: hbarDetector(aDet), fFieldManager(0),
fChordFinder(0),
fStepper(0), fFieldMessenger(0)
{
//////  fLocalMagneticField = new hbarField();
  fMagneticField = new hbarField(hbarDetector);
/////  fS1MagneticField = new hbarField(aDet->GetSextupole1MagFieldSolid());
/////  fS2MagneticField = new hbarField(aDet->GetSextupole2MagFieldSolid());
//  fMagneticField = new G4UniformMagField(
//                       G4ThreeVector(1.0*tesla,
//                                     0.0*tesla,              // 0.5*tesla,
//                                     1.0*tesla       ));


  fFieldMessenger = new hbarFieldMess(this) ;

//  fEquation = new G4Mag_UsualEqRhs(fMagneticField);
  fEquation = new hbarEqSextupoleField(fMagneticField);
/////  fS1Equation = new hbarEqSextupoleField(fS1MagneticField);
/////  fS2Equation = new hbarEqSextupoleField(fS2MagneticField);
//  fLocalEquation = new G4Mag_UsualEqRhs(fLocalMagneticField);

  fMinStep     = 0.1*mm ; // minimal step of 10 mm is default
  fStepperType = 4 ;      // ClassicalRK4 is default stepper
  fMinMiss     = 0.1*mm ; // minimal miss distance
  allowMajorana = "strong";
  useAngleDiff = "off";

  //fFieldManager = GetGlobalFieldManager();
  fFieldManager = new G4FieldManager(fMagneticField);
  /*if(hbarDetector->IsUseSextupole(1))
    hbarDetector->GetSextupoleLogicalVolume(1)->
    SetFieldManager(fFieldManager,true);*/

  /*for(int i=1; i<hbarDetector->GetMaximumSextupoleNumber(); i++){
    if(hbarDetector->IsUseSextupole(i))
      hbarDetector->GetSextupoleLogicalVolume(i)->
	SetFieldManager(fFieldManager,true);
	}*/
//  fFieldManager = GetGlobalFieldManager();
/////  fS1FieldManager = new G4FieldManager();
/////  fS2FieldManager = new G4FieldManager();

////  UpdateField();

  G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetMaxLoopCount(200);

}

/////////////////////////////////////////////////////////////////////////////////

////hbarFieldSetup::hbarFieldSetup(G4ThreeVector fieldVector)
////{    
////  fMagneticField = new G4UniformMagField(fieldVector);
////  GetGlobalFieldManager()->CreateChordFinder(fMagneticField);
////}

////////////////////////////////////////////////////////////////////////////////

hbarFieldSetup::~hbarFieldSetup()
{
  if(fMagneticField) delete fMagneticField;
  //if(fChordFinder)        delete fChordFinder;
  if(fStepper)            delete fStepper;
/*
  if(fS1MagneticField)    delete fS1MagneticField;
  if(fS2MagneticField)    delete fS2MagneticField;
  if(fS1ChordFinder)      delete fS1ChordFinder;
  if(fS2ChordFinder)      delete fS2ChordFinder;
  if(fS1Stepper)          delete fS1Stepper;
  if(fS2Stepper)          delete fS2Stepper;
*/
  if(fFieldMessenger)     delete fFieldMessenger;
  if(fEquation)           delete fEquation;
  if(fFieldManager)       delete fFieldManager;
}

/////////////////////////////////////////////////////////////////////////////
//
// Update field
//

void hbarFieldSetup::UpdateField()
{
  SetStepper();
  G4cout<<"The minimal step is equal to "<<fMinStep/mm<<" mm"<<G4endl ;

///  fLocalFieldManager->SetDetectorField(fLocalMagneticField );

  if (fChordFinder) delete fChordFinder;
/////  if (fS1ChordFinder) delete fS1ChordFinder;
/////  if (fS2ChordFinder) delete fS2ChordFinder;
///  if(fLocalChordFinder) delete fLocalChordFinder;

  fIntgrDriver = new G4MagInt_Driver(fMinStep,fStepper,fStepper->GetNumberOfVariables());

  fChordFinder = new G4ChordFinder(fIntgrDriver);

/*****
  fS1IntgrDriver = new G4MagInt_Driver(fMinStep,
                                       fS1Stepper,
                                       fS1Stepper->GetNumberOfVariables() );
  fS2IntgrDriver = new G4MagInt_Driver(fMinStep,
                                       fS2Stepper,
                                       fS2Stepper->GetNumberOfVariables() );

  fS1ChordFinder = new G4ChordFinder(fS1IntgrDriver);
  fS2ChordFinder = new G4ChordFinder(fS2IntgrDriver);
*****/

//  fS1ChordFinder = new G4ChordFinder(fS1MagneticField, fMinStep, fS1Stepper);
//  fS2ChordFinder = new G4ChordFinder(fS2MagneticField, fMinStep, fS2Stepper);
///  fLocalChordFinder = new G4ChordFinder( fLocalMagneticField, 
///                                         fMinStep,fLocalStepper);

/////  fS1ChordFinder->SetDeltaChord(fMinMiss);
/////  fS2ChordFinder->SetDeltaChord(fMinMiss);
  fChordFinder->SetDeltaChord(fMinMiss);

  fFieldManager->SetDetectorField(fMagneticField);
  fFieldManager->SetChordFinder(fChordFinder);

/*****
  fS1FieldManager->SetDetectorField(fS1MagneticField);
  fS2FieldManager->SetDetectorField(fS2MagneticField);
  fS1FieldManager->SetChordFinder(fS1ChordFinder);
  fS2FieldManager->SetChordFinder(fS2ChordFinder);
*****/

///  fLocalFieldManager->SetChordFinder( fLocalChordFinder );
///  if (fFieldManager == fLocalFieldManager) G4cout << "Field managers are the same!" << G4endl;
//  G4TransportationManager::GetTransportationManager()->SetFieldManager(fFieldManager);

  fMagneticField->Update();
//  fS1MagneticField->Update();
//  fS2MagneticField->Update();

}

/////////////////////////////////////////////////////////////////////////////
//
// Set stepper according to the stepper type
//

void hbarFieldSetup::SetStepper()
{

  if (fStepper) delete fStepper;
/////  if(fS1Stepper) delete fS1Stepper;
/////  if(fS2Stepper) delete fS2Stepper;

  switch ( fStepperType ) 
  {
    case 0:  
/////      fS1Stepper = new G4ExplicitEuler( fS1Equation );
/////      fS2Stepper = new G4ExplicitEuler( fS2Equation );
      fStepper = new G4ExplicitEuler( fEquation, 8 );
      G4cout<<"G4ExplicitEuler is called"<<G4endl;
      break;
    case 1:  
/////      fS1Stepper = new G4ImplicitEuler( fS1Equation );
/////      fS2Stepper = new G4ImplicitEuler( fS2Equation );
      fStepper = new G4ImplicitEuler( fEquation, 8 );
      G4cout<<"G4ImplicitEuler is called"<<G4endl;
      break;
    case 2:  
/////      fS1Stepper = new G4SimpleRunge( fS1Equation );
/////      fS2Stepper = new G4SimpleRunge( fS2Equation );
      fStepper = new G4SimpleRunge( fEquation, 8 ); 
      G4cout<<"G4SimpleRunge is called"<<G4endl;
      break;
    case 3:  
/////      fS1Stepper = new G4SimpleHeum( fS1Equation );
/////      fS2Stepper = new G4SimpleHeum( fS2Equation );
      fStepper = new G4SimpleHeum( fEquation, 8 );
      G4cout<<"G4SimpleHeum is called"<<G4endl;
      break;
    case 4:  
/////      fS1Stepper = new G4ClassicalRK4( fS1Equation );
/////      fS2Stepper = new G4ClassicalRK4( fS2Equation );
      fStepper = new G4ClassicalRK4( fEquation, 9 );
      G4cout<<"G4ClassicalRK4 (default) is called"<<G4endl;
      break;
/****
    case 5:  
      fStepper = new G4HelixExplicitEuler( fEquation ); 
///      fLocalStepper = new G4HelixExplicitEuler( fLocalEquation ); 
      G4cout<<"G4HelixExplicitEuler is called"<<G4endl;     
      break;
    case 6:  
      fStepper = new G4HelixImplicitEuler( fEquation ); 
///      fLocalStepper = new G4HelixImplicitEuler( fLocalEquation ); 
      G4cout<<"G4HelixImplicitEuler is called"<<G4endl;     
      break;
    case 7:  
      fStepper = new G4HelixSimpleRunge( fEquation );   
///      fLocalStepper = new G4HelixSimpleRunge( fLocalEquation );   
      G4cout<<"G4HelixSimpleRunge is called"<<G4endl;     
      break;
******/
    case 5:  
/////      fS1Stepper = new G4CashKarpRKF45( fS1Equation );
/////      fS2Stepper = new G4CashKarpRKF45( fS2Equation );
      fStepper = new G4CashKarpRKF45( fEquation, 8 );
      G4cout<<"G4CashKarpRKF45 is called"<<G4endl;
      break;
/****
    case 6:  
      fStepper = new G4RKG3_Stepper( fEquation );       
///      fLocalStepper = new G4RKG3_Stepper( fLocalEquation );
      G4cout<<"G4RKG3_Stepper is called"<<G4endl;
      break;
*****/
/////    default: fS1Stepper = 0; fS2Stepper = 0;
    default: fStepper = 0;
  }

}

/////////////////////////////////////////////////////////////////////////////
//
// 
//

void hbarFieldSetup::SetFieldMaxValue(G4double fieldValue)
{
  G4cout << "SETTING MAX FIELD VALUE TO " << fieldValue / tesla << G4endl;
   fMagneticField->SetFieldMaxValue(fieldValue);
/////   fS1MagneticField->SetFieldMaxValue(fieldValue);
/////   fS2MagneticField->SetFieldMaxValue(fieldValue);
////  if(fMagneticField) delete fMagneticField;
////  fMagneticField = new hbarField(hbarDetector, fieldValue);
//  fMagneticField = new G4UniformMagField(
//                       G4ThreeVector(0.0*tesla,
//                                     0.0*tesla,
//                                     fieldValue ));
  UpdateField();
}

/////////////////////////////////////////////////////////////////////////////
//
// 
//

void hbarFieldSetup::SetZeroField(G4double fieldValue)
{
   fMagneticField->SetZeroField(fieldValue);
/////   fS1MagneticField->SetFieldMaxValue(fieldValue);
/////   fS2MagneticField->SetFieldMaxValue(fieldValue);
////  if(fMagneticField) delete fMagneticField;
////  fMagneticField = new hbarField(hbarDetector, fieldValue);
//  fMagneticField = new G4UniformMagField(
//                       G4ThreeVector(0.0*tesla,
//                                     0.0*tesla,
//                                     fieldValue ));
  UpdateField();
}

/*********
///////////////////////////////////////////////////////////////////////////////
//
// Set the value of the Global Field
//

void hbarFieldSetup::SetFieldValue(G4ThreeVector fieldVector)
{
  // Find the Field Manager for the global field
  G4FieldManager* fieldMgr= GetGlobalFieldManager();
    
  if(fMagneticField) delete fMagneticField;

  if(fieldVector != G4ThreeVector(0.,0.,0.))
  { 
    fMagneticField = new  G4UniformMagField(fieldVector);
  }
  else 
  {
    // If the new field's value is Zero, then it is best to
    //  insure that it is not used for propagation.
    fMagneticField = 0; 
  }
  fieldMgr->SetDetectorField(fMagneticField);

  // UpdateField();
}
*********/

////////////////////////////////////////////////////////////////////////////////
//
//  Utility method
template<class T>
void hbarFieldSetup::MakeMajoranaTransition_templ(T aHbar, G4double anglediff)
{
  if (allowMajorana == "off") 
	return;

  if(aHbar->GetN() != 1) ///Only make the Majorana transition in ground state.
	return;
  
  if (useAngleDiff == "off") 
	{
      anglediff = 0.0;
	}

   if (allowMajorana == "weak") 
	 {
	   G4double flatrndmnum = G4UniformRand();
	   G4double m1prob = 0.0;
	   G4double m0prob = 0.0;
	   G4double mm1prob = 0.0;
	   if ((aHbar->GetF() == 0) && (aHbar->GetM() == 0)) 
		 {
		   return;
		 } 
	   else if ((aHbar->GetF() == 1) && (aHbar->GetM() == 1)) 
		 {
		   m1prob  = sqr(0.5*(1.0 + cos(anglediff)));
		   m0prob  = 0.5*sqr(sin(anglediff));
		   mm1prob = sqr(0.5*(1.0 - cos(anglediff)));
		 } else if ((aHbar->GetF() == 1) && (aHbar->GetM() == 0)) 
		 {
		   m1prob  = 0.5*sqr(sin(anglediff));
		   m0prob  = sqr(cos(anglediff));
		   mm1prob = 0.5*sqr(sin(anglediff));
		 } 
	   else if ((aHbar->GetF() == 1) && (aHbar->GetM() == -1 ) )
		 {
		   m1prob  = sqr(0.5*(1.0 - cos(anglediff)));
		   m0prob  = 0.5*sqr(sin(anglediff));
		   mm1prob = sqr(0.5*(1.0 + cos(anglediff)));
		 }
	   
	   if ((m1prob+m0prob+mm1prob > 1.001) || (m1prob+m0prob+mm1prob < 0.999)) 
		 {
		   G4cerr << "The sum of Majorana probabilities is " << m1prob+m0prob+mm1prob
				  << "!" << G4endl;
		 }

      if (flatrndmnum < m1prob) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(1);
		} 
	  else if (flatrndmnum < m1prob+m0prob) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(0);
		} 
	  else if (flatrndmnum <= m1prob+m0prob+mm1prob) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(-1);
		}
	  
	 } 
   else if (allowMajorana == "strong") 
	 {
	   G4double flatrndmnumJ = G4UniformRand();
	   G4double flatrndmnumI = G4UniformRand();
	   G4double J12prob = 0.0;
	   G4double I12prob = 0.0;
	   G4double halfangle = anglediff/2.0;
	   if ((aHbar->GetF() == 0) && (aHbar->GetM() == 0)) 
		 {
		   J12prob  = sqr(sin(halfangle));
		   I12prob = sqr(cos(halfangle));
      } 
	   else if ((aHbar->GetF() == 1) && (aHbar->GetM() == 1)) 
		 {
		   J12prob  = sqr(cos(halfangle));
		   I12prob = sqr(cos(halfangle));
      } 
	   else if ((aHbar->GetF() == 1) && (aHbar->GetM() == 0)) 
		 {
		   J12prob  = sqr(cos(halfangle));
		   I12prob = sqr(sin(halfangle));
		 } 
	   else if ((aHbar->GetF() == 1) && (aHbar->GetM() == -1 ) )
		 {
		   J12prob  = sqr(sin(halfangle));
		   I12prob = sqr(sin(halfangle));
		 }
	   
	   G4int j = 0;
	   if (flatrndmnumJ < J12prob) 
		 {
		   j = 1;
		 } 
	   else 
		 {
		   j = -1;
		 }
	   
	   G4int i = 0;
      if (flatrndmnumI < I12prob) 
		{
		  i = 1;
		} 
	  else 
		{
		  i = -1;
		}
	  
      if ((j == 1) && (i == 1)) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(1);         
		} 
	  else if ((j == 1) && (i == -1)) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(0);         
      } 
	  else if ((j == -1) && (i == -1)) 
		{
		  aHbar->SetF(1);
		  aHbar->SetM(-1);         
      } 
	  else if ((j == -1) && (i == 1)) 
		{
		  aHbar->SetF(0);
		  aHbar->SetM(0);         
		} 
	  else 
		{
		  G4cerr << "Invalid Majorana transition!" << G4endl;      
		}
	 }
}

void hbarFieldSetup::MakeMajoranaTransition(hbarHydrogenLike* aHbar, G4double anglediff) 
{
  MakeMajoranaTransition_templ<hbarHydrogenLike*>(aHbar,anglediff);
}

void hbarFieldSetup::SetFlipMagnet(G4int num, G4String val)
{
  if (val == "on") 
	fMagneticField->SetFieldFlip(num, true);
  else 
	fMagneticField->SetFieldFlip(num, false);
  UpdateField();
}

void hbarFieldSetup::SetFlipMagnet(G4String val)
{
  if (val == "on") 
	fMagneticField->SetFieldFlip(true);
  else 
	fMagneticField->SetFieldFlip(false);
  UpdateField();
}

void hbarFieldSetup::SetFieldHalfPoleNumber(G4int val)
{
   fMagneticField->SetFieldHalfPoleNumber(val);
   UpdateField();
}

void hbarFieldSetup::SetMinimumStepLength(G4double val)
{
   if (val > 0.0) 
	 {
	   fMinStep = val;
	   UpdateField();
	 } 
   else 
	 {
	   G4cout << "Error: Minimum step length has to be larger than zero!" << G4endl;
	 }
}

