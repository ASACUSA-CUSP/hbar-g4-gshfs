#include "hbarPrimGenMess.hh"

#include "hbarPrimGenAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithoutParameter.hh"

hbarPrimGenMess::hbarPrimGenMess(hbarPrimGenAction* hbarGun)
:hbarAction(hbarGun)
{ 
  RndmDirCmd = new G4UIcmdWithAString("/gun/setRandomDirection",this);
  RndmDirCmd->SetGuidance("Shoot the particles into random directions.");
  RndmDirCmd->SetGuidance("The direction can be random even if the particle energy is fixed.");
  RndmDirCmd->SetGuidance("  Choice : on(into 4 Pi, default), off, aim (into a cone)");
  RndmDirCmd->SetParameterName("choice",true);
  RndmDirCmd->SetDefaultValue("on");
  RndmDirCmd->SetCandidates("on off aim");
  RndmDirCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  RndmEnergyCmd = new G4UIcmdWithAString("/gun/setRandomEnergy",this);
  RndmEnergyCmd->SetGuidance("Shoot the particles with random energy.");
  RndmEnergyCmd->SetGuidance("  Choice : on(default), off");
  RndmEnergyCmd->SetParameterName("choice",true);
  RndmEnergyCmd->SetDefaultValue("on");
  RndmEnergyCmd->SetCandidates("on off");
  RndmEnergyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  RndmVelCmd = new G4UIcmdWithADouble("/gun/setMaximumFlatRandomVelocity",this);
  RndmVelCmd->SetGuidance("Set the maximum velocity and generates a flat velocity distribution between zero and this velocity.");
  RndmVelCmd->SetParameterName("Velocity",false,false);
  RndmVelCmd->SetRange("Velocity>=0.0");
  RndmVelCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  RndmStateCmd = new G4UIcmdWithAString("/gun/setRandomState",this);
  RndmStateCmd->SetGuidance("Shoot the particles in random states (F,M).");
  RndmStateCmd->SetGuidance("  Choice : on, follow(default), off");
  RndmStateCmd->SetParameterName("choice",true);
  RndmStateCmd->SetDefaultValue("follow");
  RndmStateCmd->SetCandidates("on follow off");
  RndmStateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  RndmRydbergCmd = new G4UIcmdWithAString("/gun/setRandomRydberg",this);
  RndmRydbergCmd->SetGuidance("Randomize the initial n, l, j, mj, p states.");
  RndmRydbergCmd->SetGuidance("  Choice : on, off(default)");
  RndmRydbergCmd->SetParameterName("choice",true);
  RndmRydbergCmd->SetDefaultValue("off");
  RndmRydbergCmd->SetCandidates("on off");
  RndmRydbergCmd->AvailableForStates(G4State_PreInit,G4State_Idle);



  EnableQuantumTransitionCmd = new G4UIcmdWithAString("/gun/setQuantumTransitions",this);
  EnableQuantumTransitionCmd->SetGuidance("Enable transition between different (n, l, j, m_j).");
  EnableQuantumTransitionCmd->SetGuidance("  Choice : on, off(default)");
  EnableQuantumTransitionCmd->SetParameterName("choice",true);
  EnableQuantumTransitionCmd->SetDefaultValue("off");
  EnableQuantumTransitionCmd->SetCandidates("on off");
  EnableQuantumTransitionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  PreloadQuantumStatesCmd = new G4UIcmdWithAString("/gun/preloadQuantumStates",this);
  PreloadQuantumStatesCmd->SetGuidance("Preload quantum states (trade RAM for time).");
  PreloadQuantumStatesCmd->SetGuidance("  Choice : on (default), off");
  PreloadQuantumStatesCmd->SetParameterName("choice",true);
  PreloadQuantumStatesCmd->SetDefaultValue("off");
  PreloadQuantumStatesCmd->SetCandidates("on off");
  PreloadQuantumStatesCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  EnableFieldRegimeTranslationsCmd = new G4UIcmdWithAString("/gun/enableFieldRegimeTranslations",this);
  EnableFieldRegimeTranslationsCmd->SetGuidance("Enable field regime translations.");
  EnableFieldRegimeTranslationsCmd->SetGuidance("  Choice : on(default), off");
  EnableFieldRegimeTranslationsCmd->SetParameterName("choice",true);
  EnableFieldRegimeTranslationsCmd->SetDefaultValue("off");
  EnableFieldRegimeTranslationsCmd->SetCandidates("on off");
  EnableFieldRegimeTranslationsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);



  UseStrongInitialStateCmd = new G4UIcmdWithAString("/gun/setUseStrongInitialState",this);
  UseStrongInitialStateCmd->SetGuidance("Use strong field state as initial description.");
  UseStrongInitialStateCmd->SetGuidance("  Choice : on, off(default)");
  UseStrongInitialStateCmd->SetParameterName("choice",true);
  UseStrongInitialStateCmd->SetDefaultValue("off");
  UseStrongInitialStateCmd->SetCandidates("on off");
  UseStrongInitialStateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  EnableQuantumEnergyForceCmd = new G4UIcmdWithAString("/gun/setQuantumEnergyForce",this);
  EnableQuantumEnergyForceCmd->SetGuidance("Enable different force dependent on quantum state.");
  EnableQuantumEnergyForceCmd->SetGuidance("  Choice : on, off(default)");
  EnableQuantumEnergyForceCmd->SetParameterName("choice",true);
  EnableQuantumEnergyForceCmd->SetDefaultValue("off");
  EnableQuantumEnergyForceCmd->SetCandidates("on off");
  EnableQuantumEnergyForceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  QuantumStateFileCmd = new G4UIcmdWithAString("/gun/setQuantumStateFile",this);
  QuantumStateFileCmd->SetGuidance("File where the quantum states is stored.");
  QuantumStateFileCmd->SetGuidance("Filename");
  QuantumStateFileCmd->SetParameterName("filename",true);
  QuantumStateFileCmd->SetDefaultValue("");
  QuantumStateFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  SourceStateNCmd = new G4UIcmdWithAnInteger("/gun/setStateN",this);
  SourceStateNCmd->SetGuidance("Set state N of the source");
  SourceStateNCmd->SetParameterName("state",false,false);
  SourceStateNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateLCmd = new G4UIcmdWithAnInteger("/gun/setStateL",this);
  SourceStateLCmd->SetGuidance("Set state L of the source");
  SourceStateLCmd->SetParameterName("state",false,false);
  SourceStateLCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateTwoJCmd = new G4UIcmdWithAnInteger("/gun/setStateTwoJ",this);
  SourceStateTwoJCmd->SetGuidance("Set state J of the source");
  SourceStateTwoJCmd->SetParameterName("state",false,false);
  SourceStateTwoJCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateTwoMJCmd = new G4UIcmdWithAnInteger("/gun/setStateTwoMJ",this);
  SourceStateTwoMJCmd->SetGuidance("Set state MJ of the source");
  SourceStateTwoMJCmd->SetParameterName("state",false,false);
  SourceStateTwoMJCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateMLCmd = new G4UIcmdWithAnInteger("/gun/setStateML",this);
  SourceStateMLCmd->SetGuidance("Set state ML of the source");
  SourceStateMLCmd->SetParameterName("state",false,false);
  SourceStateMLCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateTwoMSCmd = new G4UIcmdWithAnInteger("/gun/setStateTwoMS",this);
  SourceStateTwoMSCmd->SetGuidance("Set state MS of the source");
  SourceStateTwoMSCmd->SetParameterName("state",false,false);
  SourceStateTwoMSCmd->AvailableForStates(G4State_PreInit,G4State_Idle);



  LowerHysteresisCmd = new G4UIcmdWithADouble("/gun/setLowerHysteresis",this);
  LowerHysteresisCmd->SetGuidance("Set lower hysteresis level for the strong-weak transitions in hbar.");
  LowerHysteresisCmd->SetParameterName("hysteresis",false,false);
  LowerHysteresisCmd->SetRange("hysteresis>=0.0");
  LowerHysteresisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  UpperHysteresisCmd = new G4UIcmdWithADouble("/gun/setUpperHysteresis",this);
  UpperHysteresisCmd->SetGuidance("Set upper hysteresis level for the strong-weak transitions in hbar.");
  UpperHysteresisCmd->SetParameterName("hysteresis",false,false);
  UpperHysteresisCmd->SetRange("hysteresis>=0.0");
  UpperHysteresisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);




  

  SourceStateMaxNCmd = new G4UIcmdWithAnInteger("/gun/setStateMaxN",this);
  SourceStateMaxNCmd->SetGuidance("Set state MaxN of the source");
  SourceStateMaxNCmd->SetParameterName("state",false,false);
  SourceStateMaxNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateMaxLCmd = new G4UIcmdWithAnInteger("/gun/setStateMaxL",this);
  SourceStateMaxLCmd->SetGuidance("Set state MaxL of the source");
  SourceStateMaxLCmd->SetParameterName("state",false,false);
  SourceStateMaxLCmd->AvailableForStates(G4State_PreInit,G4State_Idle);






  SliceCmd = new G4UIcmdWithAString("/gun/setSlice",this);
  SliceCmd->SetGuidance("Only shoot the particles in a narrow slice in Z.");
  SliceCmd->SetGuidance("This essentially produces a two dimensional beam");
  SliceCmd->SetGuidance("which looks nice when creating a figure.");
  SliceCmd->SetGuidance("  Choice : on, off(default)");
  SliceCmd->SetParameterName("choice",true);
  SliceCmd->SetDefaultValue("off");
  SliceCmd->SetCandidates("on off");
  SliceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceCenterCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceCenter",this);
  SourceCenterCmd->SetGuidance("Set source center (x,y,z).");
  SourceCenterCmd->SetParameterName("Size",false,false);
  SourceCenterCmd->SetDefaultUnit("m");
//  SourceCenterCmd->SetRange("Size>=0.");
  SourceCenterCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  KinECmd = new G4UIcmdWithADoubleAndUnit("/gun/setBeamEnergy",this);
  KinECmd->SetGuidance("Set kinetic energy of the beam (eV).");
  KinECmd->SetGuidance("This command has no effect if the source energy is set to random");
  KinECmd->SetGuidance("because in that case the particle energy is obtained randomly from the source temperature.");
  KinECmd->SetParameterName("Energy",false,false);
  KinECmd->SetDefaultUnit("eV");
  KinECmd->SetRange("Energy>=0.");
  KinECmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceTempCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceTemperature",this);
  SourceTempCmd->SetGuidance("Set temperature of the source (K).");
  SourceTempCmd->SetGuidance("This command has no effect if the source energy is not set to random");
  SourceTempCmd->SetGuidance("because in that case the particle energy is obtained from the source energy.");
  SourceTempCmd->SetParameterName("Temperature",false,false);
  SourceTempCmd->SetDefaultUnit("K");
  SourceTempCmd->SetRange("Temperature>=0.");
  SourceTempCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  PartCmd = new G4UIcmdWithAString("/gun/setParticle",this);
  PartCmd->SetGuidance("Select particle to shoot.");
  PartCmd->SetGuidance("  Choice : pbar(default), antiproton, anti_proton, p, proton, neutron, n, anti_neutron, antineutron, anti_hydrogen, antihydrogen, hbar, Hbar, mu+, mu-, muon+, muon-, pi+, pi-, pi0, pion+, pion-, pion0, cosmic, e-, e+, electron, positron, hydrogen, k+, k-, k0"); // modified by Clemens
  PartCmd->SetParameterName("choice",true);
  PartCmd->SetDefaultValue("anti_proton");
  PartCmd->SetCandidates("pbar antiproton anti_proton p proton neutron n anti_neutron antineutron anti_hydrogen antihydrogen hbar Hbar mu+ mu- muon+ muon- pi+ pi- pi0 pion+ pion- pion0 cosmic e- e+ electron positron hydrogen k+ k- k0"); // modified by Clemens
  PartCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceSizeXCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceFWHM_X",this);
  SourceSizeXCmd->SetGuidance("Set FWHM of the source in X");
  SourceSizeXCmd->SetParameterName("Size",false,false);
  SourceSizeXCmd->SetDefaultUnit("m");
  SourceSizeXCmd->SetRange("Size>=0.");
  SourceSizeXCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceSizeYCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceFWHM_Y",this);
  SourceSizeYCmd->SetGuidance("Set FWHM of the source in Y");
  SourceSizeYCmd->SetParameterName("Size",false,false);
  SourceSizeYCmd->SetDefaultUnit("m");
  SourceSizeYCmd->SetRange("Size>=0.");
  SourceSizeYCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceSizeZCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceFWHM_Z",this);
  SourceSizeZCmd->SetGuidance("Set FWHM of the source in Z (beam direction)");
  SourceSizeZCmd->SetParameterName("Size",false,false);
  SourceSizeZCmd->SetDefaultUnit("m");
  SourceSizeZCmd->SetRange("Size>=0.");
  SourceSizeZCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceSizeCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceFWHM",this);
  SourceSizeCmd->SetGuidance("Set FWHM of the source in all three directions");
  SourceSizeCmd->SetParameterName("Size",false,false);
  SourceSizeCmd->SetDefaultUnit("m");
  SourceSizeCmd->SetRange("Size>=0.");
  SourceSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceOpenAngleCmd = new G4UIcmdWithADouble("/gun/setSourceOpeningAngle",this);
  SourceOpenAngleCmd->SetGuidance("Set opening angle of the cone (in degrees)");
  SourceOpenAngleCmd->SetGuidance("into which the aprticles are shot in case the direction is set to 'aim'");
  SourceOpenAngleCmd->SetParameterName("Angle",false,false);
  SourceOpenAngleCmd->SetRange("Angle>=0.");
  SourceOpenAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  SourceOffsetXCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceOffsetX",this);
  SourceOffsetXCmd->SetGuidance("Set offset of the source from the beam axis in X");
  SourceOffsetXCmd->SetParameterName("Distance",false,false);
  SourceOffsetXCmd->SetDefaultUnit("m");
  SourceOffsetXCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceOffsetYCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceOffsetY",this);
  SourceOffsetYCmd->SetGuidance("Set offset of the source from the beam axis in Y");
  SourceOffsetYCmd->SetParameterName("Distance",false,false);
  SourceOffsetYCmd->SetDefaultUnit("m");
  SourceOffsetYCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceMaxRadCmd = new G4UIcmdWithADoubleAndUnit("/gun/setSourceMaximumRadius",this);
  SourceMaxRadCmd->SetGuidance("Set maximum diameter of the source in X-Y");
  SourceMaxRadCmd->SetParameterName("Size",false,false);
  SourceMaxRadCmd->SetDefaultUnit("m");
  SourceMaxRadCmd->SetRange("Size>0.");
  SourceMaxRadCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateFCmd = new G4UIcmdWithAnInteger("/gun/setStateF",this);
  SourceStateFCmd->SetGuidance("Set state F of the source");
  SourceStateFCmd->SetParameterName("state",false,false);
  SourceStateFCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateMCmd = new G4UIcmdWithAnInteger("/gun/setStateM",this);
  SourceStateMCmd->SetGuidance("Set state M of the source");
  SourceStateMCmd->SetParameterName("state",false,false);
  SourceStateMCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SourceStateARatioCmd = new G4UIcmdWithADouble("/gun/setStateARatio",this);
  SourceStateARatioCmd->SetGuidance("Set the production ratio of state (0,0)");
  SourceStateARatioCmd->SetParameterName("Ratio",false,false);
  SourceStateARatioCmd->SetRange("Ratio>=0.");
  SourceStateARatioCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  SourceStateBRatioCmd = new G4UIcmdWithADouble("/gun/setStateBRatio",this);
  SourceStateBRatioCmd->SetGuidance("Set the production ratio of state (1,1)");
  SourceStateBRatioCmd->SetParameterName("Ratio",false,false);
  SourceStateBRatioCmd->SetRange("Ratio>=0.");
  SourceStateBRatioCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  SourceStateCRatioCmd = new G4UIcmdWithADouble("/gun/setStateCRatio",this);
  SourceStateCRatioCmd->SetGuidance("Set the production ratio of state (1,0)");
  SourceStateCRatioCmd->SetParameterName("Ratio",false,false);
  SourceStateCRatioCmd->SetRange("Ratio>=0.");
  SourceStateCRatioCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  SourceStateDRatioCmd = new G4UIcmdWithADouble("/gun/setStateDRatio",this);
  SourceStateDRatioCmd->SetGuidance("Set the production ratio of state (1,-1)");
  SourceStateDRatioCmd->SetParameterName("Ratio",false,false);
  SourceStateDRatioCmd->SetRange("Ratio>=0.");
  SourceStateDRatioCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  DirCmd = new G4UIcmdWith3Vector("/gun/setBeamDirection",this);
  DirCmd->SetGuidance("Set direction of the beam (only relevant when the random direction is off)");
  DirCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  ProdRateCmd = new G4UIcmdWithADoubleAndUnit("/gun/setProductionRate",this);
  ProdRateCmd->SetGuidance("Set production rate (1/sec) of the (Hbar) particle");
  ProdRateCmd->SetGuidance("0 (zero) means all particles are created at the same time");
  ProdRateCmd->SetParameterName("Rate",false,false);
  ProdRateCmd->SetDefaultUnit("hertz");
  ProdRateCmd->SetRange("Rate>=0.");
  ProdRateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  AlwaysRTCmd = new G4UIcmdWithABool("/gun/setAlwaysResetProductionTime",this);
  AlwaysRTCmd->SetGuidance("Whether to reset the particle production time in the beginning of each run");
  AlwaysRTCmd->SetDefaultValue(true);
  AlwaysRTCmd->AvailableForStates(G4State_Idle);

  ResetTimeCmd = new G4UIcmdWithoutParameter("/gun/resetProductionTime",this);
  ResetTimeCmd->SetGuidance("Reset particle production time now");
  ResetTimeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  // Added by Clemens -- start
  useAEgISCmd  = new G4UIcmdWithABool("/gun/useAEgISData",this);
  useAEgISCmd->SetGuidance("Set whether to use AEgIS velocity distribution or not");
  useAEgISCmd->SetDefaultValue(false);
  useAEgISCmd->AvailableForStates(G4State_Idle);
  useAEgISCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  CRYFileCmd = new G4UIcmdWithAString("/CRY/file",this);
  CRYFileCmd->SetGuidance("This reads the CRY definition from a file");
  CRYFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  CRYInputCmd = new G4UIcmdWithAString("/CRY/input",this);
  CRYInputCmd->SetGuidance("CRY input lines");
  CRYInputCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  CRYUpdateCmd = new G4UIcmdWithoutParameter("/CRY/update",this);
  CRYUpdateCmd->SetGuidance("Update CRY definition.");
  CRYUpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  CRYUpdateCmd->SetGuidance("if you changed the CRY definition.");
  CRYUpdateCmd->AvailableForStates(G4State_Idle);

  CRYDispacementCmd = new G4UIcmdWith3Vector("/CRY/setDisplacement",this);
  CRYDispacementCmd->SetGuidance("Set the Displacement of CRY particle generation");
  CRYDispacementCmd->SetGuidance("Default: Particles are created around 0,0,0 (Geant coordinates)");
  CRYDispacementCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  useCRYCmd = new G4UIcmdWithABool("/gun/useCRY",this);
  useCRYCmd->SetGuidance("Use CRY for production of cosmics");
  useCRYCmd->SetDefaultValue(false);
  useCRYCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  MessInput = new std::string;

  SpectrumCmd = new G4UIcmdWithAString("/gun/setSpectrumFilename",this);
  SpectrumCmd->SetGuidance("Select the root file that contains the spectrum of the hbar velocity distribution in  z direction");
  SpectrumCmd->SetParameterName("filename",true);
  SpectrumCmd->SetDefaultValue("aegis_spectrum.root");
  SpectrumCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  useParticleMixtureCmd = new G4UIcmdWithABool("/gun/setUseParticleMixture", this);
  useParticleMixtureCmd->SetGuidance("Switch the simulation of background particles on or off");
  useParticleMixtureCmd->SetDefaultValue(false);
  useParticleMixtureCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  addBGParticleCmd = new G4UIcmdWithAString("/gun/addBackgroundParticle",this);
  addBGParticleCmd->SetGuidance("Add background particle to shoot.");
  addBGParticleCmd->SetGuidance("  Choice : pbar(default), antiproton, anti_proton, p, proton, neutron, n, anti_neutron, antineutron, anti_hydrogen, antihydrogen, hbar, Hbar, mu+, mu-, muon+, muon-, pi+, pi-, pi0, pion+, pion-, pion0, cosmic, e-, e+, electron, positron, hydrogen"); // modified by Clemens
  addBGParticleCmd->SetParameterName("choice",true);
  addBGParticleCmd->SetDefaultValue("anti_proton");
  addBGParticleCmd->SetCandidates("pbar antiproton anti_proton p proton neutron n anti_neutron antineutron anti_hydrogen antihydrogen hbar Hbar mu+ mu- muon+ muon- pi+ pi- pi0 pion+ pion- pion0 cosmic e- e+ electron positron hydrogen"); // modified by Clemens
  addBGParticleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  addBGParticleNumCmd = new G4UIcmdWithAnInteger("/gun/addBackgroundParticleNum",this);
  addBGParticleNumCmd->SetGuidance("Set number of background particles of given type to shoot per primary particle.");
  addBGParticleNumCmd->SetGuidance("The number of generated background particles per event is the sum of all background particle numbers, the sequence of particle emission is random (including the primary particle).");
  addBGParticleNumCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
// Added by Clemens -- stop

}

hbarPrimGenMess::~hbarPrimGenMess()
{

  delete RndmDirCmd; 
  delete RndmEnergyCmd; 
  delete RndmVelCmd; 
  delete RndmStateCmd; 
  delete RndmRydbergCmd;
  
  delete LowerHysteresisCmd;
  delete UpperHysteresisCmd;

  ///By Rikard
  delete EnableQuantumTransitionCmd;
  delete PreloadQuantumStatesCmd;
  delete EnableFieldRegimeTranslationsCmd;
  delete UseStrongInitialStateCmd;
  delete EnableQuantumEnergyForceCmd; 
  delete QuantumStateFileCmd;
  delete SourceStateNCmd; 
  delete SourceStateLCmd;
  delete SourceStateTwoJCmd; 
  delete SourceStateTwoMSCmd;
  delete SourceStateMLCmd;
  
  delete SourceStateMaxNCmd;
  delete SourceStateMaxLCmd; 
  delete SourceStateTwoMJCmd;
  ///until here

  delete KinECmd; delete PartCmd;
  delete SourceCenterCmd; delete SourceTempCmd;
  delete SourceSizeXCmd; delete SourceSizeYCmd; delete SourceSizeZCmd;
  delete SourceSizeCmd; delete SourceOpenAngleCmd;
  delete SourceOffsetXCmd; delete SourceOffsetYCmd;
  delete SourceMaxRadCmd;
  delete SliceCmd;
  delete SourceStateFCmd; delete SourceStateMCmd;
  delete SourceStateARatioCmd; delete SourceStateBRatioCmd;
  delete SourceStateCRatioCmd; delete SourceStateDRatioCmd;
  delete DirCmd;
  delete ProdRateCmd;
  delete AlwaysRTCmd;
  delete ResetTimeCmd;

  // Added by Clemens -- start
  delete CRYFileCmd;
  delete CRYInputCmd;
  delete CRYUpdateCmd;
  delete CRYDispacementCmd;
  delete useCRYCmd;
  delete MessInput;

  delete useAEgISCmd; 
  delete SpectrumCmd;

  delete useParticleMixtureCmd;
  delete addBGParticleCmd;
  delete addBGParticleNumCmd;
  // Added by Clemens -- stop

}

void hbarPrimGenMess::SetNewValue(G4UIcommand * command,G4String newValue)
{ 
  if( command == RndmDirCmd )
   { hbarAction->SetRndmDirectionFlag(newValue);}

  if( command == RndmEnergyCmd )
   { hbarAction->SetRndmEnergyFlag(newValue);}

  if( command == RndmVelCmd )
   { hbarAction->SetRndmSourceVelocity(RndmVelCmd->GetNewDoubleValue(newValue));}

  if( command == RndmStateCmd )
   { hbarAction->SetRndmStateFlag(newValue);}

  if( command == RndmRydbergCmd)
	{ hbarAction->SetRndmRydbergFlag(newValue == "on"); }

  if( command == EnableQuantumTransitionCmd )
    { hbarAction->SetQuantumTransitionFlag(newValue); }

  if( command == PreloadQuantumStatesCmd )
    { hbarAction->SetPreloadQuantumStatesFlag(newValue); }

  if( command ==   EnableFieldRegimeTranslationsCmd )
	{hbarAction->SetEnableFieldRegimeTranslations(newValue == "on"); }

  if( command == UseStrongInitialStateCmd )
	{ hbarAction->SetUseStrongInitialState(newValue == "on"); }

  if ( command == EnableQuantumEnergyForceCmd)
    { hbarAction->SetQuantumEnergyForceFlag(newValue); }

  if( command == QuantumStateFileCmd)
    { hbarAction->SetQuantumStateFile(newValue); }

  if( command == SourceStateNCmd )
   { hbarAction->SetSourceStateN(SourceStateNCmd->GetNewIntValue(newValue));}

  if( command == SourceStateLCmd )
   { hbarAction->SetSourceStateL(SourceStateLCmd->GetNewIntValue(newValue));}

  if( command == SourceStateTwoJCmd )
   { hbarAction->SetSourceStateTwoJ(SourceStateTwoJCmd->GetNewIntValue(newValue));}

  if( command == SourceStateTwoMJCmd )
   { hbarAction->SetSourceStateTwoMJ(SourceStateTwoMJCmd->GetNewIntValue(newValue));}

  if( command == SourceStateMLCmd )
   { hbarAction->SetSourceStateML(SourceStateMLCmd->GetNewIntValue(newValue));}

  if( command == SourceStateTwoMSCmd )
   { hbarAction->SetSourceStateTwoMS(SourceStateTwoMSCmd->GetNewIntValue(newValue));}
  


  if( command == SourceStateMaxNCmd )
    { hbarAction->SetSourceStateMaxN(SourceStateMaxNCmd->GetNewIntValue(newValue));}

  if( command == SourceStateMaxLCmd )
   { hbarAction->SetSourceStateMaxL(SourceStateMaxLCmd->GetNewIntValue(newValue));}


  if( command == LowerHysteresisCmd)
	{ hbarAction->SetLowerHysteresis(LowerHysteresisCmd->GetNewDoubleValue(newValue)); }

  if( command == UpperHysteresisCmd)
	{ hbarAction->SetUpperHysteresis(UpperHysteresisCmd->GetNewDoubleValue(newValue)); }




  if( command == SliceCmd )
   { hbarAction->SetSliceFlag(newValue);}

  if( command == SourceCenterCmd )
   { hbarAction->SetSourceCenter(SourceCenterCmd->GetNewDoubleValue(newValue));}

  if( command == KinECmd )
   { hbarAction->SetBeamKinE(KinECmd->GetNewDoubleValue(newValue));}

  if( command == SourceTempCmd )
   { hbarAction->SetSourceTemperature(SourceTempCmd->GetNewDoubleValue(newValue));}

  if( command == PartCmd )
   { hbarAction->SetParticle(newValue);}

  if( command == SourceSizeXCmd )
   { hbarAction->SetSourceFWHM_X(SourceSizeXCmd->GetNewDoubleValue(newValue));}

  if( command == SourceSizeYCmd )
   { hbarAction->SetSourceFWHM_Y(SourceSizeYCmd->GetNewDoubleValue(newValue));}

  if( command == SourceSizeZCmd )
   { hbarAction->SetSourceFWHM_Z(SourceSizeZCmd->GetNewDoubleValue(newValue));}

  if( command == SourceSizeCmd )
   { hbarAction->SetSourceFWHM(SourceSizeCmd->GetNewDoubleValue(newValue));}

  if( command == SourceOpenAngleCmd )
   { hbarAction->SetSourceOpeningAngle(SourceOpenAngleCmd->GetNewDoubleValue(newValue));}

  if( command == SourceOffsetXCmd )
   { hbarAction->SetSourceOffsetX(SourceOffsetXCmd->GetNewDoubleValue(newValue));}

  if( command == SourceOffsetYCmd )
   { hbarAction->SetSourceOffsetY(SourceOffsetYCmd->GetNewDoubleValue(newValue));}

  if( command == SourceMaxRadCmd )
   { hbarAction->SetSourceMaximumRadius(SourceMaxRadCmd->GetNewDoubleValue(newValue));}

  if( command == SourceStateFCmd )
   { hbarAction->SetSourceStateF(SourceStateFCmd->GetNewIntValue(newValue));}

  if( command == SourceStateMCmd )
   { hbarAction->SetSourceStateM(SourceStateMCmd->GetNewIntValue(newValue));}

  if( command == SourceStateARatioCmd )
   { hbarAction->SetSourceStateARatio(SourceStateARatioCmd->GetNewDoubleValue(newValue));}

  if( command == SourceStateBRatioCmd )
   { hbarAction->SetSourceStateBRatio(SourceStateBRatioCmd->GetNewDoubleValue(newValue));}

  if( command == SourceStateCRatioCmd )
   { hbarAction->SetSourceStateCRatio(SourceStateCRatioCmd->GetNewDoubleValue(newValue));}

  if( command == SourceStateDRatioCmd )
   { hbarAction->SetSourceStateDRatio(SourceStateDRatioCmd->GetNewDoubleValue(newValue));}

  if( command == DirCmd )
   { hbarAction->SetBeamDirection(DirCmd->GetNew3VectorValue(newValue));}

  if( command == ProdRateCmd )
   { hbarAction->SetProductionRate(ProdRateCmd->GetNewDoubleValue(newValue));}

  if( command == AlwaysRTCmd )
   { hbarAction->SetIsResetTimer(AlwaysRTCmd->GetNewBoolValue(newValue));}

  if( command == ResetTimeCmd )
   { hbarAction->ResetTimer(); }

  // Added by Clemens -- start

  if( command == CRYFileCmd ) hbarAction->CRYFromFile(newValue);

  if( command == useAEgISCmd ) hbarAction->SetuseAEgIS(useAEgISCmd->GetNewBoolValue(newValue)); 

  if( command == SpectrumCmd ) hbarAction->SetSpectrumFilename(newValue);

  if( command == CRYInputCmd ) {
    (*MessInput).append(newValue);
    (*MessInput).append(" "); }

  if( command == CRYUpdateCmd ){
    hbarAction->UpdateCRY(MessInput); 
    *MessInput = ""; }
    
  if( command == CRYDispacementCmd ) 
    hbarAction->SetCRYDisplacement(CRYDispacementCmd->GetNew3VectorValue(newValue));

  if( command == useCRYCmd ) 
    hbarAction->SetUseCRY(useCRYCmd->GetNewBoolValue(newValue));

  if( command == useParticleMixtureCmd )
    hbarAction->SetuseParticleMixture(useParticleMixtureCmd->GetNewBoolValue(newValue));
  
  if( command == addBGParticleCmd ) hbarAction->addBGParticle(newValue);

  if( command == addBGParticleNumCmd )
    hbarAction->addBGParticleNum(addBGParticleNumCmd->GetNewIntValue(newValue));
  // Added by Clemens -- stop
}

