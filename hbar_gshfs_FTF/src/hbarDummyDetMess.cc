#include "hbarDummyDetMess.hh"

hbarDummyDetMess::hbarDummyDetMess(hbarDummyDetectorConst * _hbarDummyDet)
  : hbarDummyDet(_hbarDummyDet)
{
  DetLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setDetectorLength",this);
  DetLengthCmd->SetGuidance("Set length of the detector (m)");
  DetLengthCmd->SetParameterName("Size",false,false);
  DetLengthCmd->SetDefaultUnit("m");
  DetLengthCmd->SetRange("Size>0.");
  DetLengthCmd->AvailableForStates(G4State_Idle);

  DetDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setDetectorDiameter",this);
  DetDiamCmd->SetGuidance("Set diameter of the detector (m)");
  DetDiamCmd->SetParameterName("Size",false,false);
  DetDiamCmd->SetDefaultUnit("m");
  DetDiamCmd->SetRange("Size>=0.");
  DetDiamCmd->AvailableForStates(G4State_Idle);

  DetCenterCmd = new G4UIcmdWithADoubleAndUnit("/setup/setDetectorCenter",this);
  DetCenterCmd->SetGuidance("Set center of the detector (m)");
  DetCenterCmd->SetParameterName("Size",false,false);
  DetCenterCmd->SetDefaultUnit("m");
  //DetCenterCmd->SetRange("Size>=0.");
  DetCenterCmd->AvailableForStates(G4State_Idle);
}


hbarDummyDetMess::~hbarDummyDetMess()
{
  delete DetLengthCmd; 
  delete DetDiamCmd; 
  delete DetCenterCmd;
}


void hbarDummyDetMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
 if( command == DetLengthCmd )
   { 
	 hbarDummyDet->SetDetectorLength(DetLengthCmd->GetNewDoubleValue(newValue));
   }
 
 if( command == DetDiamCmd )
   { 
	 hbarDummyDet->SetDetectorDiameter(DetDiamCmd->GetNewDoubleValue(newValue));
   }
 
  if( command == DetCenterCmd )
    { 
	  hbarDummyDet->SetDetectorCenter(DetCenterCmd->GetNewDoubleValue(newValue));
	}
}
