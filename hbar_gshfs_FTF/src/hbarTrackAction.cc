#include "hbarTrackAction.hh"

hbarTrackAction::hbarTrackAction()
 :storeFlag("all")
{
  trackLengthPt = -1;
  trackMessenger = new hbarTrackActionMess(this);
}

hbarTrackAction::~hbarTrackAction()
{
  delete trackMessenger;
}

void hbarTrackAction::PreUserTrackingAction(const G4Track* aTrack)
{
  trackLengthPt = -1;
  for (G4int i = 0; i < 10; i++) {
    trackLength[i] = 0.0;
  }

  if ((storeFlag == "primary") && (aTrack->GetParentID()==0)) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if (storeFlag == "allwithneutrino") 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
  } 
  else if ((storeFlag == "all") && (!aTrack->GetDefinition()->GetParticleName().contains("nu") )) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if ((storeFlag == "pbar") && (aTrack->GetDefinition()->GetParticleName() == "anti_proton")) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if ((storeFlag == "hbar") && (aTrack->GetDefinition()->GetParticleName() == "antihydrogen")) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if ((storeFlag == "hbar") && (aTrack->GetDefinition()->GetParticleName() == "hydrogen")) 
	{ 
      fpTrackingManager->SetStoreTrajectory(true);
      fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if ((storeFlag == "charged") && (aTrack->GetDefinition()->GetPDGCharge() != 0.)) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else if ((storeFlag == "heavy") && (aTrack->GetDefinition()->GetParticleName() != "gamma")
	     && (aTrack->GetDefinition()->GetParticleName() != "e+")
	     && (aTrack->GetDefinition()->GetParticleName() != "e-")
	     && (!aTrack->GetDefinition()->GetParticleName().contains("nu") )) 
	{
	  fpTrackingManager->SetStoreTrajectory(true);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	} 
  else 
	{
	  fpTrackingManager->SetStoreTrajectory(false);
	  fpTrackingManager->SetTrajectory(new hbarTrajectory(aTrack));
	}
  (const_cast<G4Track*>(aTrack))->SetUserInformation(new hbarUserTrackInformation());
}

void hbarTrackAction::PostUserTrackingAction(const G4Track* aTrack)
{
  (void)aTrack; //Avoid compiler warning //Rikard
}

void hbarTrackAction::SetTrackLength(G4double val) {
  if (trackLengthPt == 9) trackLengthPt = 0;
  else trackLengthPt++;
  trackLength[trackLengthPt] = val;
}
