#include "hbarDummyDetectorConst.hh"

hbarDummyDetectorConst::hbarDummyDetectorConst(G4UserLimits * _alim)
  :DetSol(NULL), DetLog(NULL), DetPhy(NULL),alim(_alim)
{
  DetLength = 0.1*m;
  DetDiam = 0.2*m;
  DetCenter = 2.5*m;
  DummyDetMess = new hbarDummyDetMess(this);
}

hbarDummyDetectorConst::~hbarDummyDetectorConst()
{
  if(DetSol)
	delete DetSol;
  if(DetLog)
	delete DetLog;
  if(DetPhy)
	delete DetPhy;
  if(DummyDetMess)
	delete DummyDetMess;
}

G4VPhysicalVolume * hbarDummyDetectorConst::Construct()
{
  //std::cout << "myDummyDetectorConst-------------------- " << std::endl;
  ///Assuming that the stuff is deleted by G4 system before this is called...
  DetSol = new G4Tubs("DetP", 0, DetDiam/2.0, DetLength/2.0, 0.0, 2.0*M_PI);
  DetLog = new G4LogicalVolume(DetSol, hbarDetConst::GetMaterial("Sts"), "DetP");
  DetLog->SetUserLimits(alim);
  DetPhy = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, DetCenter), "DetP", DetLog, NULL, false, 0);
  G4VisAttributes* redcolor = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
  DetLog->SetVisAttributes(redcolor);
  return DetPhy;
}
