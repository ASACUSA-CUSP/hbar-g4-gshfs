#include "hbarSextupoleMess.hh"

hbarSextupoleMess::hbarSextupoleMess(hbarSextupoleConst * _hbarSextupole)
  :hbarSextupole(_hbarSextupole)
{
  SxPartUseCmd = new G4UIcmdWithAnInteger("/setup/setSxPart",this);
  SxPartUseCmd->SetGuidance("Switch SxPart to number X");
  SxPartUseCmd->SetParameterName("Count",true,false);
  SxPartUseCmd->SetDefaultValue(0);
  SxPartUseCmd->SetRange("Count>=0");
  SxPartUseCmd->AvailableForStates(G4State_Idle);

  SxPartSetDimenCmd = new G4UIcmdWith3VectorAndUnit("/setup/setSxPartDimension",this);
  SxPartSetDimenCmd->SetGuidance("Set dimensions of the SxPart ");
  SxPartSetDimenCmd->SetParameterName("ind", "oud", "len",false,false);
  SxPartSetDimenCmd->SetDefaultUnit("mm");
  SxPartSetDimenCmd->AvailableForStates(G4State_Idle);
 

  SextStartCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleStart",this);
  SextStartCmd->SetGuidance("Set start (upstream) position of the current sextupole (m)");
  SextStartCmd->SetParameterName("Size",false,false);
  SextStartCmd->SetDefaultUnit("m");
  SextStartCmd->AvailableForStates(G4State_Idle);

  SextEndCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleEnd",this);
  SextEndCmd->SetGuidance("Set end (downstream) position of the current sextupole (m)");
  SextEndCmd->SetParameterName("Size",false,false);
  SextEndCmd->SetDefaultUnit("m");
  SextEndCmd->AvailableForStates(G4State_Idle);

  SextOffsetXCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleOffsetX",this);
  SextOffsetXCmd->SetGuidance("Set X offset of the current sextupole (m)");
  SextOffsetXCmd->SetParameterName("Size",false,false);
  SextOffsetXCmd->SetDefaultUnit("m");
  SextOffsetXCmd->AvailableForStates(G4State_Idle);

  SextOffsetYCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleOffsetY",this);
  SextOffsetYCmd->SetGuidance("Set Y offset of the current sextupole (m)");
  SextOffsetYCmd->SetParameterName("Size",false,false);
  SextOffsetYCmd->SetDefaultUnit("m");
  SextOffsetYCmd->AvailableForStates(G4State_Idle);

  SextInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleInnerDiameter",this);
  SextInnerDiamCmd->SetGuidance("Set inner diameter of the current sextupole (m)");
  SextInnerDiamCmd->SetParameterName("Size",false,false);
  SextInnerDiamCmd->SetDefaultUnit("m");
  SextInnerDiamCmd->SetRange("Size>0.");
  SextInnerDiamCmd->AvailableForStates(G4State_Idle);

  SextFrontInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleFrontInnerDiameter",this);
  SextFrontInnerDiamCmd->SetGuidance("Set front inner diameter of the current sextupole (m)");
  SextFrontInnerDiamCmd->SetParameterName("Size",false,false);
  SextFrontInnerDiamCmd->SetDefaultUnit("m");
  SextFrontInnerDiamCmd->SetRange("Size>0.");
  SextFrontInnerDiamCmd->AvailableForStates(G4State_Idle);

  SextRearInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleRearInnerDiameter",this);
  SextRearInnerDiamCmd->SetGuidance("Set rear inner diameter of the current sextupole (m)");
  SextRearInnerDiamCmd->SetParameterName("Size",false,false);
  SextRearInnerDiamCmd->SetDefaultUnit("m");
  SextRearInnerDiamCmd->SetRange("Size>0.");
  SextRearInnerDiamCmd->AvailableForStates(G4State_Idle);

  SextOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleOuterDiameter",this);
  SextOuterDiamCmd->SetGuidance("Set outer diameter of the current sextupole (m)");
  SextOuterDiamCmd->SetParameterName("Size",false,false);
  SextOuterDiamCmd->SetDefaultUnit("m");
  SextOuterDiamCmd->SetRange("Size>0.");
  SextOuterDiamCmd->AvailableForStates(G4State_Idle);

  SextFrontOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleFrontOuterDiameter",this);
  SextFrontOuterDiamCmd->SetGuidance("Set front outer diameter of the current sextupole (m)");
  SextFrontOuterDiamCmd->SetParameterName("Size",false,false);
  SextFrontOuterDiamCmd->SetDefaultUnit("m");
  SextFrontOuterDiamCmd->SetRange("Size>0.");
  SextFrontOuterDiamCmd->AvailableForStates(G4State_Idle);

  SextRearOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setSextupoleRearOuterDiameter",this);
  SextRearOuterDiamCmd->SetGuidance("Set outer diameter of the current sextupole (m)");
  SextRearOuterDiamCmd->SetParameterName("Size",false,false);
  SextRearOuterDiamCmd->SetDefaultUnit("m");
  SextRearOuterDiamCmd->SetRange("Size>0.");
  SextRearOuterDiamCmd->AvailableForStates(G4State_Idle);

  SextRotXCmd = new G4UIcmdWithADouble("/setup/setSextupoleRotationX",this);
  SextRotXCmd->SetGuidance("Set X rotation of the current sextupole field (degree)");
  SextRotXCmd->SetParameterName("Size",false,false);
  SextRotXCmd->AvailableForStates(G4State_Idle);

  SextRotYCmd = new G4UIcmdWithADouble("/setup/setSextupoleRotationY",this);
  SextRotYCmd->SetGuidance("Set Y rotation of the current sextupole field (degree)");
  SextRotYCmd->SetParameterName("Size",false,false);
  SextRotYCmd->AvailableForStates(G4State_Idle);

  SextRotMag = new G4UIcmdWithABool("/setup/setSextupoleRotateMagnet",this);
  SextRotMag->SetGuidance("Whether the sextupole geometry is rotated with the field axis or nor");
  SextRotMag->SetDefaultValue(false);
  SextRotMag->AvailableForStates(G4State_Idle);

 
  UseSextupolePartsCmd = new G4UIcmdWithAnInteger("/setup/setUseSextupoleParts",this);
  UseSextupolePartsCmd->SetGuidance("Swich SextupoleParts on or off");
  UseSextupolePartsCmd->SetParameterName("useflag",true,false);
  UseSextupolePartsCmd->SetDefaultValue(1);
  UseSextupolePartsCmd->SetRange("useflag>=0");
  UseSextupolePartsCmd->AvailableForStates(G4State_Idle);

  SextUseCmd = new G4UIcmdWithAnInteger("/setup/setUseSextupole",this);
  SextUseCmd->SetGuidance("Switch to sextupole number X");
  SextUseCmd->SetParameterName("Count",true,false);
  SextUseCmd->SetDefaultValue(0);
  SextUseCmd->SetRange("Count>=0");
  SextUseCmd->AvailableForStates(G4State_Idle);

}


hbarSextupoleMess::~hbarSextupoleMess()
{
  delete SxPartUseCmd; 
  delete SxPartSetDimenCmd; 
  delete UseSextupolePartsCmd; 
  delete SextStartCmd; 
  delete SextEndCmd;
  delete SextInnerDiamCmd; 
  delete SextFrontInnerDiamCmd; 
  delete SextRearInnerDiamCmd;
  delete SextOuterDiamCmd; 
  delete SextFrontOuterDiamCmd; 
  delete SextRearOuterDiamCmd;
  delete SextRotXCmd; 
  delete SextRotYCmd; 
  delete SextRotMag;
  delete SextOffsetXCmd; 
  delete SextOffsetYCmd;
  delete SextUseCmd; 
}

void hbarSextupoleMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == SextUseCmd )
   { 
	 hbarSextupole->UseSextupole(SextUseCmd->GetNewIntValue(newValue));
   }

  if( command == UseSextupolePartsCmd)
   { 
	 hbarSextupole->SetUseSextupoleParts(UseSextupolePartsCmd->GetNewIntValue(newValue));
   }

  if( command == SxPartUseCmd )
   { 
	 hbarSextupole->UseSxPart(SxPartUseCmd->GetNewIntValue(newValue));
   }
  if (command == SxPartSetDimenCmd) 
   { 
	 hbarSextupole->SetSxPartDimension(SxPartSetDimenCmd->GetNew3VectorValue(newValue));
   }


  if( command == SextStartCmd )
   { 
	 hbarSextupole->SetSextupoleStart(SextStartCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextEndCmd )
   { 
	 hbarSextupole->SetSextupoleEnd(SextEndCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextOffsetXCmd )
   { 
	 hbarSextupole->SetSextupoleOffsetX(SextOffsetXCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextOffsetYCmd )
   { 
	 hbarSextupole->SetSextupoleOffsetY(SextOffsetYCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextInnerDiamCmd )
   { 
	 hbarSextupole->SetSextupoleInnerDiameter(SextInnerDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextFrontInnerDiamCmd )
   { 
	 hbarSextupole->SetSextupoleFrontInnerDiameter(SextFrontInnerDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextRearInnerDiamCmd )
   { 
	 hbarSextupole->SetSextupoleRearInnerDiameter(SextRearInnerDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextOuterDiamCmd )
   { 
	 hbarSextupole->SetSextupoleOuterDiameter(SextOuterDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextFrontOuterDiamCmd )
   { 
	 hbarSextupole->SetSextupoleFrontOuterDiameter(SextFrontOuterDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextRearOuterDiamCmd )
   { 
	 hbarSextupole->SetSextupoleRearOuterDiameter(SextRearOuterDiamCmd->GetNewDoubleValue(newValue));
   }

  if( command == SextRotXCmd )
  { 
	 hbarSextupole->SetSextupoleRotationX(SextRotXCmd->GetNewDoubleValue(newValue));
   } 
  
  if( command == SextRotYCmd )
  { 
	 hbarSextupole->SetSextupoleRotationY(SextRotYCmd->GetNewDoubleValue(newValue));
   } 

  if( command == SextRotMag )
  { 
	 hbarSextupole->SetSextupoleRotateMagnet(SextRotMag->GetNewBoolValue(newValue));
   } 
}
