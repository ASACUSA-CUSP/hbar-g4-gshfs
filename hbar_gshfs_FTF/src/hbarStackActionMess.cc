#include "hbarStackActionMess.hh"
#include "hbarStackAction.hh"
#include "G4UIcmdWithAString.hh"
#include "G4ios.hh"

hbarStackActionMess::hbarStackActionMess(hbarStackAction * aStaAct)
:stackAction(aStaAct)
{
  // To select which particles to track in the simulation
  // primary: only the primary particle is tracked; all the secondaries are killed immediately
  // charged: only charged particles are tracked
  // pbar: only antiprotons are tracked
  // hbar: only antihydrogen atoms are tracked
  // heavy: only heavy particles are tracked, i.e. electrons and photons are not
  // fermion: only fermions are tracked
  // allbute+e-: all particles are tracked, except for electrons and positrons
  // all: all particles are tracked, except for neutrinos
  // allwithneutrino: all particles are tracked
  ProcessCmd = new G4UIcmdWithAString("/tracking/processTracks",this);
  ProcessCmd->SetGuidance("Choose which tracks to process.");
  ProcessCmd->SetGuidance("  Choice : primary(default), charged, pbar, hbar, heavy, fermion, allbute+e-, all, allwithneutrino");
  ProcessCmd->SetParameterName("track",true);
  ProcessCmd->SetDefaultValue ("primary");
  ProcessCmd->SetCandidates("primary charged pbar hbar heavy fermion allbute+e- allwithneutrino all");
  ProcessCmd->AvailableForStates(G4State_Idle);
}

hbarStackActionMess::~hbarStackActionMess()
{
  delete ProcessCmd;
}

void hbarStackActionMess::SetNewValue(G4UIcommand * command,G4String newValue)
{
  if( command==ProcessCmd )
  { stackAction->SetProcessFlag(newValue); }
}

