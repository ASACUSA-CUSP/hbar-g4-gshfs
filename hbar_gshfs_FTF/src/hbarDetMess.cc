#include "hbarDetMess.hh"

hbarDetMess::hbarDetMess(hbarDetConst * hbarDet)
:hbarDetector(hbarDet)
{
  hbardetDir = new G4UIdirectory("/setup/");
  hbardetDir->SetGuidance("Setup properties control.");

  UseCUSPCmd = new G4UIcmdWithAnInteger("/setup/setUseCUSP",this);
  UseCUSPCmd->SetGuidance("Swich CUSP on or off");
  UseCUSPCmd->SetParameterName("bool",true,false);
  UseCUSPCmd->SetDefaultValue(0);
  UseCUSPCmd->SetRange("bool>=0");
  UseCUSPCmd->AvailableForStates(G4State_Idle);


  WorldSizeXYCmd = new G4UIcmdWithADoubleAndUnit("/setup/setWorldSizeXY",this);
  WorldSizeXYCmd->SetGuidance("Set world size in X-Y direction (m)");
  WorldSizeXYCmd->SetParameterName("Size",false,false);
  WorldSizeXYCmd->SetDefaultUnit("m");
  WorldSizeXYCmd->SetRange("Size>=0.");
  WorldSizeXYCmd->AvailableForStates(G4State_Idle);

  WorldSizeZCmd = new G4UIcmdWithADoubleAndUnit("/setup/setWorldSizeZ",this);
  WorldSizeZCmd->SetGuidance("Set world size in Z direction (m)");
  WorldSizeZCmd->SetParameterName("Size",false,false);
  WorldSizeZCmd->SetDefaultUnit("m");
  WorldSizeZCmd->SetRange("Size>=0.");
  WorldSizeZCmd->AvailableForStates(G4State_Idle);

  UpdateCmd = new G4UIcmdWithoutParameter("/setup/update",this);
  UpdateCmd->SetGuidance("Update setup geometry.");
  UpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" ");
  UpdateCmd->SetGuidance("If you changed geometrical value(s).");
  UpdateCmd->AvailableForStates(G4State_Idle);


  UseNagataDetectorCmd = new G4UIcmdWithAnInteger("/setup/setUseNagataDetector",this);
  UseNagataDetectorCmd->SetGuidance("Swich NagataDetector on or off");
  UseNagataDetectorCmd->SetParameterName("useflag",true,false);
  UseNagataDetectorCmd->SetDefaultValue(0);
  UseNagataDetectorCmd->SetRange("useflag>=0");
  UseNagataDetectorCmd->AvailableForStates(G4State_Idle);

  UseCAVITYCmd = new G4UIcmdWithAnInteger("/setup/setUseCAVITY",this);
  UseCAVITYCmd->SetGuidance("Swich CAVITY on or off");
  UseCAVITYCmd->SetParameterName("useflag",true,false);
  UseCAVITYCmd->SetDefaultValue(1);
  UseCAVITYCmd->SetRange("useflag>=0");
  UseCAVITYCmd->AvailableForStates(G4State_Idle);

  WriteGDMLCmd = new G4UIcmdWithAnInteger("/setup/WriteGDML",this);
  WriteGDMLCmd->SetGuidance("Write geometrie to GDML");
  WriteGDMLCmd->SetParameterName("useflag",true,false);
  WriteGDMLCmd->SetDefaultValue(0);
  WriteGDMLCmd->SetRange("useflag>=0");
  WriteGDMLCmd->AvailableForStates(G4State_Idle);

  HbarUseCmd = new G4UIcmdWithAnInteger("/setup/detector/setUseHbar",this);
  HbarUseCmd->SetGuidance("Place Hbar Counter");
  HbarUseCmd->SetParameterName("Count",true,false);
  HbarUseCmd->SetDefaultValue(0);
  HbarUseCmd->SetRange("Count>=0");
  HbarUseCmd->AvailableForStates(G4State_Idle);

  HbarUse2014Cmd = new G4UIcmdWithAnInteger("/setup/detector/setUseHbar2014",this);
  HbarUse2014Cmd->SetGuidance("Load the CPT of 2014 - octagonal Hodoscope + BGO");
  HbarUse2014Cmd->SetParameterName("useflag",true,false);
  HbarUse2014Cmd->SetDefaultValue(0);
  HbarUse2014Cmd->SetRange("useflag>=0");
  HbarUse2014Cmd->AvailableForStates(G4State_Idle);

  UseCPTCmd = new G4UIcmdWithAnInteger("/setup/detector/UseCPTCmd",this);
  UseCPTCmd->SetGuidance("Load the Charged Pion Tracker");
  UseCPTCmd->SetParameterName("useflag",true,false);
  UseCPTCmd->SetDefaultValue(0);
  UseCPTCmd->SetRange("useflag>=0");
  UseCPTCmd->AvailableForStates(G4State_Idle);
}


hbarDetMess::~hbarDetMess()
{
  delete WorldSizeXYCmd;
  delete WorldSizeZCmd;
  delete UseNagataDetectorCmd;
  delete UseCAVITYCmd;
  delete UseCUSPCmd;
  delete hbardetDir;
  delete HbarUseCmd;

  delete UpdateCmd;
  delete WriteGDMLCmd;
  delete HbarUse2014Cmd;
  delete UseCPTCmd;

}

void hbarDetMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == UseNagataDetectorCmd)
   {
	 hbarDetector->SetUseNagataDetector(UseNagataDetectorCmd->GetNewIntValue(newValue));
   }

  if( command == UseCAVITYCmd)
   {
	 hbarDetector->SetUseCAVITY(UseCAVITYCmd->GetNewIntValue(newValue));
   }

  if (command == UseCUSPCmd)
	{
	  hbarDetector->SetUseCUSP(UseCUSPCmd->GetNewIntValue(newValue));
	}

  if( command == WorldSizeXYCmd )
    {
	  hbarDetector->SetWorldSizeXY(WorldSizeXYCmd->GetNewDoubleValue(newValue));
	}

  if( command == WorldSizeZCmd )
    {
	  hbarDetector->SetWorldSizeZ(WorldSizeZCmd->GetNewDoubleValue(newValue));
	}

  if( command == UpdateCmd )
    {
	  hbarDetector->UpdateGeometry();
	}

  if( command == WriteGDMLCmd )
    {
	  hbarDetector->SetWriteGDML(WriteGDMLCmd->GetNewIntValue(newValue));
	}

  if (command == HbarUseCmd)
	{
	  hbarDetector->SetUseHbarDet(HbarUseCmd->GetNewIntValue(newValue));
	}

  if (command == HbarUse2014Cmd)
	{
	  hbarDetector->SetUseHbar2014Det(HbarUse2014Cmd->GetNewIntValue(newValue));
	}

  if (command == UseCPTCmd)
	{
	  hbarDetector->SetUseCPT(UseCPTCmd->GetNewIntValue(newValue));
	}

}
