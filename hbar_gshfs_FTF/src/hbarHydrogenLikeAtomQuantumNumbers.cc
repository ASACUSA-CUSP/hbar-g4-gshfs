#include "hbarHydrogenLikeAtomQuantumNumbers.hh"

void hbarHydrogenLikeAtomQuantumNumbers::SetF(G4int val)
{
  F = val;
}

void hbarHydrogenLikeAtomQuantumNumbers::SetM(G4int val)
{
  M = val;
}

hbarHydrogenLikeAtomQuantumNumbers::hbarHydrogenLikeAtomQuantumNumbers(G4int _F, G4int _M)
  :F(_F), M(_M)

{

}


hbarHydrogenLikeAtomQuantumNumbers::hbarHydrogenLikeAtomQuantumNumbers(const hbarHydrogenLikeAtomQuantumNumbers& toCopy)
{
  F=toCopy.F;
  M=toCopy.M;
}
