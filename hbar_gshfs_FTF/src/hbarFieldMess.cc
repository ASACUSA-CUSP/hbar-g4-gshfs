#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

#include "hbarFieldMess.hh"
#include "hbarFieldSetup.hh"

hbarFieldMess::hbarFieldMess(hbarFieldSetup *hbarFS)
:hbarFSetup(hbarFS)
{ 
  hbarfieldDir = new G4UIdirectory("/field/");
  hbarfieldDir->SetGuidance("Sextupole magnetic field properties control.");

  // Whether to use sextupole #N at all.
  // If set to false, the sextupole will not be present in the geometry.
  SextUseCmd = new G4UIcmdWithAnInteger("/field/setUseSextupole",this);
  SextUseCmd->SetGuidance("Switch to sextupole number X");
  SextUseCmd->SetParameterName("Count",true,false);
  SextUseCmd->SetDefaultValue(0);
  SextUseCmd->SetRange("Count>=0");
  SextUseCmd->AvailableForStates(G4State_Idle);

  MaxValueCmd = new G4UIcmdWithADoubleAndUnit("/field/setMaximumValue",this);
  MaxValueCmd->SetGuidance("Set maximum value of the magnetic field of the current sextupole (tesla)");
  MaxValueCmd->SetParameterName("Strength",false,false);
  MaxValueCmd->SetDefaultUnit("tesla");
  MaxValueCmd->SetRange("Strength>=0.");
  MaxValueCmd->AvailableForStates(G4State_Idle);

  ZeroValueCmd = new G4UIcmdWithADoubleAndUnit("/field/setZeroFieldValue",this);
  ZeroValueCmd->SetGuidance("Set homogeneous field component along Z (tesla)");
  ZeroValueCmd->SetParameterName("ZStrength",false,false);
  ZeroValueCmd->SetDefaultUnit("tesla");
  ZeroValueCmd->SetRange("ZStrength>=0.");
  ZeroValueCmd->AvailableForStates(G4State_Idle);

  // Whether spontaneous Majorana spin-flips should happen at the entrance of sextupole #2.
  // If the Hbar atoms are guided by a >1 G magnetic field between the two sextupoles,
  // (or between the cusp trap and the sextupole) then these spin flips should no occur.
  AllowMajCmd = new G4UIcmdWithAString("/field/setAllowMajoranaTransitions",this);
  AllowMajCmd->SetGuidance("Allow Majorana spin flips at the entrance of sextupole #2");
  AllowMajCmd->SetGuidance("  Choice : off(default), weak, strong");
  AllowMajCmd->SetGuidance("    weak:   transitions in weak field: only (F,M) = (1,1) <-> (1,-1) are allowed");
  AllowMajCmd->SetGuidance("    strong: transitions in strong field: all transitions are allowed");
  AllowMajCmd->SetParameterName("value",true);
  AllowMajCmd->SetDefaultValue("strong");
  AllowMajCmd->SetCandidates("off weak strong");
  AllowMajCmd->AvailableForStates(G4State_Idle);

  // When an Hbar atoms leaves sextupole #1, its spin is (anti)parallel to the field lines.
  // When it enters sextupole #2, its spin can be at an angle to the field lines,
  // thus Majorana spin-flips can occur there. The probability of the spin flip
  // depends on the angle difference between the spin and the field line.
  // This command tells whether to use this angle difference when calculating
  // the spin flip probability.
  // Setting it to "off" essentially turns off Majorana spin-flips.
  UseAngleDiffCmd = new G4UIcmdWithAString("/field/setUseAngleDifference",this);
  UseAngleDiffCmd->SetGuidance("Use difference between the angle of field lines");
  UseAngleDiffCmd->SetGuidance("when calculating the Majorana transition probabilities");
  UseAngleDiffCmd->SetGuidance("  Choice : off, on(default)");
  UseAngleDiffCmd->SetParameterName("value",true);
  UseAngleDiffCmd->SetDefaultValue("on");
  UseAngleDiffCmd->SetCandidates("off on");
  UseAngleDiffCmd->AvailableForStates(G4State_Idle);

  FlipCmd = new G4UIcmdWithAString("/field/setFlip",this);
  FlipCmd->SetGuidance("Flip current sextupole by 180 degrees");
  FlipCmd->SetGuidance("  Choice : off, on(default)");
  FlipCmd->SetParameterName("value",true);
  FlipCmd->SetDefaultValue("on");
  FlipCmd->SetCandidates("off on");
  FlipCmd->AvailableForStates(G4State_Idle);

  MagnetHalfPoleNumCmd = new G4UIcmdWithAnInteger("/field/setMagnetHalfPoleNumber",this);
  MagnetHalfPoleNumCmd->SetGuidance("Set the half of magnet pole number (i.e. 3 for a sextupole)");
  MagnetHalfPoleNumCmd->SetParameterName("Pole",true,false);
  MagnetHalfPoleNumCmd->SetDefaultValue(3);
  MagnetHalfPoleNumCmd->SetRange("Pole>0");
  MagnetHalfPoleNumCmd->AvailableForStates(G4State_Idle);

  MinStepLengthCmd = new G4UIcmdWithADoubleAndUnit("/field/setMinimumStepLength",this);
  MinStepLengthCmd->SetGuidance("Set minimum step length in the magnetic field");
  MinStepLengthCmd->SetParameterName("Length",false,false);
  MinStepLengthCmd->SetDefaultUnit("m");
  MinStepLengthCmd->SetRange("Length>0.");
  MinStepLengthCmd->AvailableForStates(G4State_Idle);

}

hbarFieldMess::~hbarFieldMess()
{
  delete SextUseCmd; delete ZeroValueCmd;
  delete MaxValueCmd; delete AllowMajCmd; delete UseAngleDiffCmd;
  delete FlipCmd; delete MagnetHalfPoleNumCmd;
  delete hbarfieldDir; delete MinStepLengthCmd;
}

void hbarFieldMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == SextUseCmd )
   { hbarFSetup->CurrentSextupole(SextUseCmd->GetNewIntValue(newValue));}

  if( command == MaxValueCmd )
   { hbarFSetup->SetFieldMaxValue(MaxValueCmd->GetNewDoubleValue(newValue));}

  if( command == ZeroValueCmd )
   { hbarFSetup->SetZeroField(ZeroValueCmd->GetNewDoubleValue(newValue));}

  if( command == AllowMajCmd )
   { hbarFSetup->SetAllowMajoranaTransitions(newValue);}

  if( command == UseAngleDiffCmd )
   { hbarFSetup->SetUseFieldLinesAngleDifference(newValue);}

  if( command == FlipCmd )
   { hbarFSetup->SetFlipMagnet(newValue);}

  if( command == MagnetHalfPoleNumCmd )
   { hbarFSetup->SetFieldHalfPoleNumber(MagnetHalfPoleNumCmd->GetNewIntValue(newValue));}

  if( command == MinStepLengthCmd )
   { hbarFSetup->SetMinimumStepLength(MinStepLengthCmd->GetNewDoubleValue(newValue));}

}


