#include "hbarSteppingVerbose.hh"

#include "G4SteppingManager.hh"
#include "G4UnitsTable.hh"
#include "TH1.h"
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"

hbarSteppingVerbose::hbarSteppingVerbose()
{
  SPfile = new TFile("./rootfiles/2ndaries.root","recreate");
  SPTree = new TTree("SPTree","secondary particles");
  SPTree->Branch("Secondaries",Secondaries,"Secondaries[20]/C");
}

hbarSteppingVerbose::~hbarSteppingVerbose()
{
  SPfile->cd();
  SPTree->Write();
  SPfile->Close();
}

void hbarSteppingVerbose::StepInfo()
{
  CopyState();
  G4int prec = G4cout.precision(3);
  G4int tN2ndariesTot = fN2ndariesAtRestDoIt + fN2ndariesAlongStepDoIt + fN2ndariesPostStepDoIt;
  if(tN2ndariesTot>0){
    G4cout.precision(4);
    for(size_t lp1=(*fSecondary).size()-tN2ndariesTot;lp1<(*fSecondary).size();lp1++){
      G4String spName = (*fSecondary)[lp1]->GetDefinition()->GetParticleName();
      for(G4int j=0;j<20;j++){
        Secondaries[j]=spName(j);
      }
      SPTree->Fill();
    }
  }
  G4cout.precision(prec);
}

void hbarSteppingVerbose::TrackingStarted()
{

 CopyState();
  G4int prec = G4cout.precision(3);
  if( verboseLevel > 0 ){
    G4cout << std::setw( 5) << "Step#"      << " "
           << std::setw( 6) << "X"          << "    "
           << std::setw( 6) << "Y"          << "    "  
           << std::setw( 6) << "Z"          << "    "
           << std::setw( 9) << "KineE"      << " "
           << std::setw( 9) << "dEStep"     << " "  
           << std::setw(10) << "StepLeng"  
           << std::setw(10) << "TrakLeng"
           << std::setw(10) << "Volume"     << "  "
           << std::setw(10) << "Process"    << G4endl;             

    G4cout << std::setw(5) << fTrack->GetCurrentStepNumber() << " "
        << std::setw(6) << G4BestUnit(fTrack->GetPosition().x(),"Length")
        << std::setw(6) << G4BestUnit(fTrack->GetPosition().y(),"Length")
        << std::setw(6) << G4BestUnit(fTrack->GetPosition().z(),"Length")
        << std::setw(6) << G4BestUnit(fTrack->GetKineticEnergy(),"Energy")
        << std::setw(6) << G4BestUnit(fStep->GetTotalEnergyDeposit(),"Energy")
        << std::setw(6) << G4BestUnit(fStep->GetStepLength(),"Length")
        << std::setw(6) << G4BestUnit(fTrack->GetTrackLength(),"Length")
        << std::setw(10) << fTrack->GetVolume()->GetName()
        << "   initStep" << G4endl;        
  }
  G4cout.precision(prec);
}

