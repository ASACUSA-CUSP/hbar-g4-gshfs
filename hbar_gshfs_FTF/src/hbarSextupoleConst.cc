#include "hbarSextupoleConst.hh"

hbarSextupoleConst::hbarSextupoleConst(hbarFieldSetup * fieldToUse)
  :magFieldSetup(fieldToUse), allLocal(false), useNagataDetector(false)
{
  // intitialize sextupole parameters
  Sextpt = -1;
  for (G4int i = 0; i < maxSN; i++) 
	{
	  useSext[i] = false;
	  SextupoleStart[i] = -1.0*m;
	  SextupoleEnd[i] = -0.6*m;
	  SextupoleOffsetX[i] = 0.0;
	  SextupoleOffsetY[i] = 0.0;
	  SextupoleFrontOuterDiam[i] = 0.6*m;
	  SextupoleRearOuterDiam[i] = 0.6*m;
	  SextupoleFrontInnerDiam[i] = 0.3*m;
	  SextupoleRearInnerDiam[i] = 0.3*m;
	  SextupoleRotationY[i] = 0;
	  SextupoleRotationX[i] = 0;
	  SextupoleRotateMagnet[i] = false;
	  useSextupoleParts[i] = false;
	}
  SextupoleMess = new hbarSextupoleMess(this);
}

hbarSextupoleConst::~hbarSextupoleConst()
{
  delete SextupoleMess;
}

void hbarSextupoleConst::MakeSextupole(G4VPhysicalVolume * WorldPhy,  G4UserLimits* alim)
{
  for (G4int i = 0; i < maxSN; i++) 
	{
	  if (useSext[i]) 
		{
		  G4String sextname = "Sext" + numtostr(i+1);
		  G4RotationMatrix* sextrot = new G4RotationMatrix();
		  
		  if(SextupoleRotateMagnet[i])
			{ // rotate sextupole geometry
			  sextrot->rotateX((-SextupoleRotationX[i])*(M_PI/180));
			  sextrot->rotateY((-SextupoleRotationY[i])*(M_PI/180));}
		  
		  SextSol[i] = new G4Cons(sextname,
								  SextupoleFrontInnerDiam[i]/2.0, SextupoleFrontOuterDiam[i]/2.0,
								  SextupoleRearInnerDiam[i]/2.0, SextupoleRearOuterDiam[i]/2.0,
								  (SextupoleEnd[i]-SextupoleStart[i])/2.0, 0.0, 2.0*M_PI);
		  SextLog[i] = new G4LogicalVolume(SextSol[i], hbarDetConst::GetMaterial("Sts"), sextname);
		  SextLog[i]->SetUserLimits(alim);
		  
		  SextPhy[i] = new G4PVPlacement(sextrot, G4ThreeVector(SextupoleOffsetX[i], SextupoleOffsetY[i], 
																(SextupoleEnd[i]+SextupoleStart[i])/2.0), 
										 sextname, SextLog[i], WorldPhy, false, 0);
		  
		  // assign magnetic field
		  SextLog[i]->SetFieldManager(magFieldSetup->GetFieldManager(),allLocal);
		  G4VisAttributes* colour = new G4VisAttributes(G4Colour(0.0,0.9,0.95));
		  
		  SextLog[i]->SetUserLimits(alim);
		  SextLog[i]->SetVisAttributes(colour); 
		  
		  
		  // -- sextupole parts (superconducting sextupole)
		  if(useSextupoleParts[i] != 0)
			{
			  G4int SxPartpr = 0;
			  for(G4int j=0;j<maxSxPart;j++)
				{
				  G4String SxPartName = "SxPart"+ numtostr(j);
				  switch(j)
					{
					case 0:
					case 1:
					  SxPartpr=0;
					  break;
					case 2:
					case 3:
					  SxPartpr = 1;
					  break;
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					  SxPartpr = j-2;
					  break;
					case 11:
					case 12:
					  SxPartpr = 9;
					  break;
					case 13:
					case 14:
					  SxPartpr = 10;
					  break;
					}
				  SxPart[j] = new G4Tubs(SxPartName,SxPartInRad[SxPartpr],SxPartOutRad[SxPartpr],SxPartHalfLen[SxPartpr],0.0,2.0*M_PI); 
				}
			  G4RotationMatrix* SxPartRotX = new G4RotationMatrix();
			  G4RotationMatrix* SxPartRotY = new G4RotationMatrix();
			  SxPartRotX -> rotateX(0.5*M_PI);
			  SxPartRotY -> rotateY(-0.5*M_PI);
			  G4UnionSolid* Sext00 = new G4UnionSolid("Sext00",SxPart[0],SxPart[2],0,G4ThreeVector(0,0,SxPartHalfLen[0]+SxPartHalfLen[1]));
			  G4UnionSolid* Sext01 = new G4UnionSolid("Sext01",SxPart[1],SxPart[3],0,G4ThreeVector(0,0,-SxPartHalfLen[0]-SxPartHalfLen[1]));
			  G4UnionSolid* Sext02 = new G4UnionSolid("Sext02",SxPart[4],SxPart[5],SxPartRotX,G4ThreeVector(0,SxPartHalfLen[3],-SxPartHalfLen[2]+213.73*mm));
			  G4UnionSolid* Sext03 = new G4UnionSolid("Sext03",Sext02,SxPart[6],SxPartRotY,G4ThreeVector(0,2.0*SxPartHalfLen[3]-180.0*mm,-SxPartHalfLen[2]+213.73*mm));
			  G4SubtractionSolid* Sext04 = new G4SubtractionSolid("Sext04",Sext03,SxPart[7],0,G4ThreeVector(0,0,0));
			  G4SubtractionSolid* Sext05 = new G4SubtractionSolid("Sext05",Sext04,SxPart[8],SxPartRotX,G4ThreeVector(0,SxPartHalfLen[3],-SxPartHalfLen[2]+213.73*mm));
			  G4SubtractionSolid* Sext06 = new G4SubtractionSolid("Sext06",Sext05,SxPart[9],SxPartRotY,G4ThreeVector(0,2.0*SxPartHalfLen[3]-180.0*mm,-SxPartHalfLen[2]+213.73*mm));
			  G4UnionSolid* Sext07 = new G4UnionSolid("Sext07",Sext06,Sext00,SxPartRotY,
													  G4ThreeVector(SxPartHalfLen[4]+SxPartHalfLen[0],2.0*SxPartHalfLen[3]-180.0*mm,-SxPartHalfLen[2]+213.73*mm));
			  G4UnionSolid* Sext08 = new G4UnionSolid("Sext08",Sext07,Sext01,SxPartRotY,
													  G4ThreeVector(-SxPartHalfLen[4]-SxPartHalfLen[1],2.0*SxPartHalfLen[3]-180.0*mm,-SxPartHalfLen[2]+213.73*mm));
			  G4UnionSolid* Sext09 = new G4UnionSolid("Sext09",Sext08,SxPart[10],SxPartRotX,
													  G4ThreeVector(0,2.0*SxPartHalfLen[3]+SxPartHalfLen[8],-SxPartHalfLen[2]+213.73*mm));
			  G4UnionSolid* Sext10 = new G4UnionSolid("Sext10",SxPart[11],SxPart[13],0,G4ThreeVector(0.0*mm,0.0*mm,SxPartHalfLen[9]+SxPartHalfLen[10]));
			  G4UnionSolid* Sext11 = new G4UnionSolid("Sext11",SxPart[12],SxPart[14],0,G4ThreeVector(0.0*mm,0.0*mm,-SxPartHalfLen[9]-SxPartHalfLen[10]));
			  
			  SxPartLog[0] = new G4LogicalVolume(Sext09,hbarDetConst::GetMaterial("Sts"),"Sext08",0,0,0);
			  SxPartLog[1] = new G4LogicalVolume(Sext10,hbarDetConst::GetMaterial("Sts"),"Sext10",0,0,0);
			  SxPartLog[2] = new G4LogicalVolume(Sext11,hbarDetConst::GetMaterial("Sts"),"Sext11",0,0,0);
			  
			  // central part
			  G4double Sext09PosZ = (SextupoleEnd[i]+SextupoleStart[i])/2.0;
			  
			  // right flange
			  G4double Sext10PosZ = Sext09PosZ + SxPartHalfLen[6];
			  
			  // left flange
			  G4double Sext11PosZ = Sext09PosZ - SxPartHalfLen[6];

			  /*  FIXME. But the nagata detector is not used anyway...?
			  if(useNagataDetector != 0)
				{
				  Sext09PosZ = CenterOfTheSGVD+SGVD0HalfLen+2.0*SGVD1HalfLen+247.51*mm-213.73*mm+SxPartHalfLen[2]; 
				  Sext10PosZ = CenterOfTheMLSD-115.0*mm-2.0*SxPartHalfLen[10]-SxPartHalfLen[9]; 
				  Sext11PosZ = CenterOfTheSGVD+SGVD0HalfLen+2.0*SGVD1HalfLen+2.0*SxPartHalfLen[10]+SxPartHalfLen[9];
				}

			  */
			  
			  SxPartPhy[0] = new G4PVPlacement(0,G4ThreeVector(0,0,Sext09PosZ),SxPartLog[0],"Sext09",WorldPhy->GetLogicalVolume(),false,0);
			  SxPartPhy[1] = new G4PVPlacement(0,G4ThreeVector(0,0,Sext10PosZ),SxPartLog[1],"Sext10",WorldPhy->GetLogicalVolume(),false,0);
			  SxPartPhy[2] = new G4PVPlacement(0,G4ThreeVector(0,0,Sext11PosZ),SxPartLog[2],"Sext11",WorldPhy->GetLogicalVolume(),false,0);
			}
		} 
	}
}

void hbarSextupoleConst::UseSxPart(G4int num)
{
  if ((num >= 0) && (num < maxSxPart))
	{
	  SxPartpt = num;
	  useSxPart[SxPartpt] = true;
	}
  else if (num == 0) 
	{ 
	  AddSxPart();
	}
  else 
	{ 
	  G4cout << "SxPart number " << num << " is out of range!" << G4endl;
	}
}
void hbarSextupoleConst::AddSxPart()
{
  if (SxPartpt < maxSxPart)
	{
	  SxPartpt++;
	  useSxPart[SxPartpt] = true;
	}
  else
	{ 
	  throw hbarException("Cannot create SxPart! There are too many of them.");
	}
}

void hbarSextupoleConst::SetSxPartDimension(G4int num, G4double ind, G4double oud, G4double len)
{
  if ((num >= 0) && (num < maxSxPart))
	{
	  SxPartInRad[num]=0.5*ind; 
	  SxPartOutRad[num]=0.5*oud; 
	  SxPartHalfLen[num]=0.5*len;
	}
}


void hbarSextupoleConst::UseSextupole(G4int num)
{

  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  Sextpt = num-1;
	  useSext[Sextpt] = true;
	} 
  else if (num == 0) 
	{
	  AddSextupole();
	} 
  else 
	{
	  char buffer[400];
	  sprintf(buffer, "Sextupole number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarSextupoleConst::AddSextupole()
{
  if (Sextpt < maxSN-1) 
	{
	  Sextpt++;
	  useSext[Sextpt] = true;
	} 
  else 
	{
	  throw hbarException("Cannot create sextupole! There are too many of them.");
	}
}

void hbarSextupoleConst::SetSextupoleFrontInnerDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleFrontInnerDiam[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleRearInnerDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleRearInnerDiam[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleRotationX(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleRotationX[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleRotationY(G4int num, G4double val)
{
  if ((num-1 > 0) && (num-1 < maxSN)) 
	{
	  SextupoleRotationY[num-1] = val;
	}
}

void hbarSextupoleConst::SetSextupoleFrontOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleFrontOuterDiam[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleRearOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleRearOuterDiam[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleStart(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleStart[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleEnd(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleEnd[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleOffsetX(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleOffsetX[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}

void hbarSextupoleConst::SetSextupoleOffsetY(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxSN)) 
	{
	  SextupoleOffsetY[num-1] = val;
	}
  else
	{
	  throw hbarException("Arguments invalid.");
	}
}
