#include "hbarMWSetup.hh"

hbarMWSetup::hbarMWSetup(hbarDetConst *aDet)
  : nu_0(1420405751.7667*hertz),
    BohrMagneton(5.78838175555e-11*MeV/tesla),
    NuclearMagneton(3.1524512326e-14*MeV/tesla),
    GFactorElectron(2.0023193043622),
    GFactorProton(5.585694713),
    hbarDet(aDet)
{
  useFixedProbs = "off";
  nu_used = nu_0;
  P11 = 3.83170597020751;
  P01 = 2.40482555769577;
  cavity_Q = 100.0;
  MW_power = 1.0*watt;
  MW_mode = "TM110";
  magn_permeab = 1.256637e-6*henry/meter;
  J1 = new TF1("Bessel1", "TMath::BesselJ1(x)", 0.0, 100.0);
  J0 = new TF1("Bessel0", "TMath::BesselJ0(x)", 0.0, 100.0);
  //  JprimeSqr_P11 = sqr(J1->Derivative(P11));
  J1primeP11 = -0.40276;
  J1P01 = 0.519147;
  fieldAngle = 90.0;
  randPartNum = false;
  BinCavity = 0.0*tesla;
  DeltaBinCavity = 0.0*tesla;

  scanStart = nu_0;
  scanEnd = nu_0;
  scanSteps = 1;
  scanFileName = "rootfiles/g4_scan.root";
  scanFile = NULL;
  scanGraph = NULL;
  scanInitGraph = NULL;
  scanNtuples = NULL;

  fMWMessenger = new hbarMWMess(this);
  runAction = NULL;
  useCavity = false;
}


hbarMWSetup::~hbarMWSetup()
{
  if (fMWMessenger) 
	delete fMWMessenger;
  delete J1; 
  J1 = NULL;
  delete J0; 
  J0 = NULL;
}

G4double hbarMWSetup::GetCavityCenter() 
{
  return hbarDet->GetCavityConst()->GetMWCavityCenter();
}

G4double hbarMWSetup::GetCavityInnerDiameter() 
{
  return hbarDet->GetCavityConst()->GetMWCavityInnerDiameter();
}

G4double hbarMWSetup::GetCavityOuterDiameter() 
{
  return hbarDet->GetCavityConst()->GetMWCavityOuterDiameter();
}

G4double hbarMWSetup::GetCavityLength() 
{
  return hbarDet->GetCavityConst()->GetMWCavityLength();
}

G4double hbarMWSetup::GetCavityResonatorSeparation() 
{
  return hbarDet->GetCavityConst()->GetMWCavityResonatorSeparation();
}

void hbarMWSetup::MakeMWTransition(hbarHydrogenLike *aHbar, const G4Step* aStep, G4double xx, G4double yy, G4double T) 
{
  //G4cout << aHbar->GetN() << " " << aHbar->GetF() << " " << aHbar->GetM() << G4endl;

  if(aHbar->GetN() > 1) ///No transition if n>1
	return;
  if(useCavity == true) { // Swich to turn of the cavity effect
    //   G4cout << "Time in cavity: " << T/(0.001*second) << G4endl;
	
    if (useFixedProbs == "on") 
	  {
		G4double transprob = G4UniformRand();   // uniform random number in [0,1]
		// G4cout << "State: " << aHbar->GetFM() << " -> ";
		if (aHbar->GetF() == 0 && aHbar->GetM() == 0) 
		  {
			if (transprob < fTransProb) {  // (0,0) -> (1,-1)
			  aHbar->SetF(1);
			  aHbar->SetM(-1);
			}
		  } 
		else if (aHbar->GetF()==1 && aHbar->GetM()==1) 
		  {
			if (transprob < fTransProb) 
			  {  // (1,1) -> (1,0)
				aHbar->SetF(1);
				aHbar->SetM(0);
			  }
		  } 
		else if (aHbar->GetF()==1 && aHbar->GetM()==0) 
		  {
			if (transprob < fTransProb) 
			  {  // (1,0) -> (1,1)
				aHbar->SetF(1);
				aHbar->SetM(1);
			  }
		  } 
		else if (aHbar->GetF()==1 && aHbar->GetM()==-1 )
		  {
			if (transprob < fTransProb) {  // (1,-1) -> (0,0)
			  aHbar->SetF(0);
			  aHbar->SetM(0);
			}
		  } 
		else 
		  {
			G4cout << "Invalid state in hbarMWSetup::MakeMWTransition()! F=" << aHbar->GetF() << "M="<< aHbar->GetM() << G4endl;
		  }
		return;
	  }
    
    const G4int states_num = 4;
	
    G4double rho_real[states_num][states_num]; // real and imag. parts of final density matrix rho
    G4double rho_imag[states_num][states_num];

    G4double rho_k_real[states_num][states_num][6];
    G4double rho_k_imag[states_num][states_num][6];
    G4double rho_t_real[states_num][states_num][6];
    G4double rho_t_imag[states_num][states_num][6];
    G4double rho_real4_temp[states_num][states_num]; // solution of fourth order
    G4double rho_imag4_temp[states_num][states_num];
    G4double rho_real5_temp[states_num][states_num]; // solution of fifth order
    G4double rho_imag5_temp[states_num][states_num];

    for (int i = 0; i < states_num; i++) 
	  {  // initializing rhos
		for (int j = 0; j < states_num; j++) 
		  {
			rho_real[i][j] = 0.0;
			rho_imag[i][j] = 0.0;
			rho_real4_temp[i][j] = 0.0;
			rho_imag4_temp[i][j] = 0.0;
			rho_real5_temp[i][j] = 0.0;
			rho_imag5_temp[i][j] = 0.0;
		  }
	  }
	
	G4double TotalBinCavity = 0.0*tesla;
	G4double E[states_num];
	G4double x;
	G4double gJplusgI =  GFactorElectron - GFactorProton*NuclearMagneton/BohrMagneton;
	G4double omega_0[states_num][states_num];
	G4double omega = (nu_used*2.0*M_PI)/hertz;
	
    G4double delta_omega[4][4];
    double timestep = 1.0e-7;
    G4double* Point;
	Point = new G4double[4];
	CLHEP::Hep3Vector vector1;
	G4double* BBfield;
	BBfield = new G4double[6];
    	
	if(hbarDet->GetCavityConst()->GetUseCavityFieldMap()==0) 
	  {
		G4double bshift = 0.5;
		if (DeltaBinCavity*0.5 > BinCavity) 
		  {
			// the variation of the static magnetic field is larger than the field itself
			// therefore it has to be reduced, otherwise a negative static field magnitude might be created
			if (DeltaBinCavity > 0.0) 
			  { // this should always be true if the previous if() is true
				bshift = BinCavity/DeltaBinCavity;
			  }
		  }
		G4double BinCavityDev = DeltaBinCavity*(G4UniformRand() - bshift);   // random magnetic field deviation
		TotalBinCavity = BinCavity + BinCavityDev;   // final (random) total static magnetic field in the cavity
		x = (GFactorElectron + GFactorProton*NuclearMagneton/BohrMagneton)*BohrMagneton*TotalBinCavity/(nu_0*h_Planck);  // B/B0	
		// Breit Rabi formulae
		E[1] =  0.25*nu_0*h_Planck - 0.5*gJplusgI*BohrMagneton*TotalBinCavity;   // (1,1)
		E[2] = -0.25*nu_0*h_Planck + 0.5*nu_0*h_Planck*sqrt(1 + sqr(x));         // (1,0)
		E[3] =  0.25*nu_0*h_Planck + 0.5*gJplusgI*BohrMagneton*TotalBinCavity;   // (1,-1)
		E[0] = -0.25*nu_0*h_Planck - 0.5*nu_0*h_Planck*sqrt(1 + sqr(x));         // (0,0)
		
		for (G4int i = 0; i < states_num; i++) 
		  {
			for (G4int j = 0; j < states_num; j++) 
			  {
				omega_0[i][j] = (TMath::Abs(E[i] - E[j])/hbar_Planck)/hertz;
			  }
		  }
	  }	
    
    CLHEP::Hep2Vector vect(xx, yy);
    G4double r = vect.r();
    G4double phi = vect.phi();
    G4double Bfield = GetMWFieldB(r, phi); // amplitude of osc. bfield ?

    G4double sigmafactor = TMath::Abs(TMath::Cos(fieldAngle*(2.0*M_PI)/360.0));
    G4double pifactor = TMath::Abs(TMath::Sin(fieldAngle*(2.0*M_PI)/360.0));
    G4double Rabifreq[4][4];

    Rabifreq[0][0] = 0.0;
    Rabifreq[1][1] = 0.0;
    Rabifreq[2][2] = 0.0;
    Rabifreq[3][3] = 0.0;
    Rabifreq[0][1] = Rabifreq[1][0] = pifactor*(sqrt(2.0)*BohrMagneton*Bfield/hbar_Planck)/hertz;
    Rabifreq[0][3] = Rabifreq[3][0] = pifactor*(sqrt(2.0)*BohrMagneton*Bfield/hbar_Planck)/hertz;
    Rabifreq[0][2] = Rabifreq[2][0] = sigmafactor*(BohrMagneton*Bfield/hbar_Planck)/hertz;
    Rabifreq[2][3] = Rabifreq[3][2] = pifactor*(sqrt(2.0)*BohrMagneton*Bfield/hbar_Planck)/hertz;
    Rabifreq[1][2] = Rabifreq[2][1] = pifactor*(sqrt(2.0)*BohrMagneton*Bfield/hbar_Planck)/hertz;
    Rabifreq[1][3] = Rabifreq[3][1] = 0.0;
  
	if(hbarDet->GetCavityConst()->GetUseCavityFieldMap()==0) 
	  {	  
		for (G4int i = 0; i < 4; i++) 
		  {
			for (G4int j = 0; j < 4; j++) 
			  {
				delta_omega[i][j] = TMath::Abs(omega_0[i][j] - omega);
				if (delta_omega[i][j] > 30.0*Rabifreq[i][j]) 
				  {
					Rabifreq[i][j] = 0.0;
				  }
			  }
		  }
		for (G4int i = 0; i < 4; i++) 
		  {
			for (G4int j = 0; j < 4; j++) 
			  {
				if (!((i == 0) || (j == 0))) 
				  {
					delta_omega[i][j] = TMath::Abs(delta_omega[0][j] - delta_omega[0][i]);
				  }
			  }
		  }
		if (Rabifreq[0][1] > 0.0) 
		  timestep = TMath::Min(timestep, 0.05/Rabifreq[0][1]);
		if ((delta_omega[0][1] > 0.0) && (Rabifreq[0][1] > 0.0)) 
		  timestep = TMath::Min(timestep, 0.05/delta_omega[0][1]);
		if (Rabifreq[0][2] > 0.0) 
		  timestep = TMath::Min(timestep, 0.05/Rabifreq[0][2]);
		if ((delta_omega[0][2] > 0.0) && (Rabifreq[0][2] > 0.0)) 
		  timestep = TMath::Min(timestep, 0.05/delta_omega[0][2]);
		if (Rabifreq[0][3] > 0.0) 
		  timestep = TMath::Min(timestep, 0.05/Rabifreq[0][3]);
		if ((delta_omega[0][3] > 0.0) && (Rabifreq[0][3] > 0.0)) 
		  timestep = TMath::Min(timestep, 0.05/delta_omega[0][3]);
	  }
	
    double SECURITY = 0.9; 			// safety factor
    double delta1 = 0.0;	       	// difference between 4th and 5th order solution
    int stepcounter = 0;	       	// stepcounter
    double eps = 0.000000001;		// level of accuracy
    double err = 0.0;		       	// error estimation
    double errold = 0.0;     		// error estimation of previous step
    double nexttimestep = 0.0;     	// temporary variable for calc. of next timestep
    
    double rtol= eps;	       		// relative tolerance
    double atol = eps;       		// absolute tolerance
    double scale = 0;	       		// scaling factor 
    double timefactor = 0;			// factor to in/decrease timestep
    double mintimefactor = 0.2;   	// minimal timefactor
    double maxtimefactor = 10.0;   	// maximal timefactor
    bool reject = false;	       	// if timestep was rejected, this is set to true
    
    double beta = 0.08;				
    double alpha = 0.2 - 0.75*beta;	
    double tic = 0.0; 	       	// time in cavity

    // phase of osc. Bfield along beamline
    double phase = 0.0; 
    double (*Zfactor)(double) = GetOne;
    if ((MW_mode == "Cos") || (MW_mode == "cos")) 
	  Zfactor = &TMath::Cos;
    else if ((MW_mode == "Sin") || (MW_mode == "sin")) 
	  Zfactor = &TMath::Sin;
    
    if (aHbar->GetF() == 0 && aHbar->GetM()==0) 
	  {
		rho_real[0][0] = 1.0;
	  } 
	else if (aHbar->GetF() == 1 && aHbar->GetM()==1) 
	  {
		rho_real[1][1] = 1.0;
	  } 
	else if (aHbar->GetF() == 1 && aHbar->GetM()==0) 
	  {
		rho_real[2][2] = 1.0;
	  } 
	else if (aHbar->GetF()==1 && aHbar->GetM()==-1)
	  {
		rho_real[3][3] = 1.0;
	  } 
	else 
	  {
		G4cout << "Invalid state in hbarMWSetup::MakeMWTransition()! F=" << aHbar->GetF() << "M="<< aHbar->GetM() << G4endl;
		return;
	  }
     
    //std::cout << hbarDet->GetCavityConst()->GetUseCavityFieldMap() << std::endl;
    while (tic <= T/s) 
	  {  
		//************************************************************************************************************//
		if(timestep==0) 
		  std::cout << "timestep is zero! " << std::endl;	
		
		if(hbarDet->GetCavityConst()->GetUseCavityFieldMap()!=0) 
		  {
			vector1 = aStep->GetTrack()->GetPosition();	// Get current position of particle
			Point[0] = vector1.x();
			Point[1] = vector1.y();
			Point[2] = vector1.z();	
	
			hbarDet->GetCavityConst()->GetcavityFieldmap()->GetFieldValue(Point,BBfield); // get Bfield at current position
			
			TotalBinCavity = sqrt((BBfield[0])*(BBfield[0])+(BBfield[1])*(BBfield[1])+(BBfield[2])*(BBfield[2])); // calculate abs. value of Bfield
			//std::cout << BBfield[0] << " " << BBfield[1] << std::endl;
			x = (GFactorElectron + GFactorProton*NuclearMagneton/BohrMagneton)*BohrMagneton*TotalBinCavity/(nu_0*h_Planck);  // B/B0
				
			// Breit Rabi formulae
			E[1] =  0.25*nu_0*h_Planck - 0.5*gJplusgI*BohrMagneton*TotalBinCavity;   // (1,1)
			E[2] = -0.25*nu_0*h_Planck + 0.5*nu_0*h_Planck*sqrt(1 + sqr(x));         // (1,0)
			E[3] =  0.25*nu_0*h_Planck + 0.5*gJplusgI*BohrMagneton*TotalBinCavity;   // (1,-1)
			E[0] = -0.25*nu_0*h_Planck - 0.5*nu_0*h_Planck*sqrt(1 + sqr(x));         // (0,0)
				
				
			for (G4int i = 0; i < states_num; i++) 
			  {
				for (G4int j = 0; j < states_num; j++) 
				  {
					omega_0[i][j] = (TMath::Abs(E[i] - E[j])/hbar_Planck)/hertz;
				  }
			  }	
			
			for (G4int i = 0; i < 4; i++) 
			  {
				for (G4int j = 0; j < 4; j++) 
				  {
					delta_omega[i][j] = TMath::Abs(omega_0[i][j] - omega);
					if (delta_omega[i][j] > 30.0*Rabifreq[i][j]) 
					  {
						Rabifreq[i][j] = 0.0;
					  }
				  }
			  }
			for (G4int i = 0; i < 4; i++) 
			  {
				for (G4int j = 0; j < 4; j++) 
				  {
					if (!((i == 0) || (j == 0))) 
					  {
						delta_omega[i][j] = TMath::Abs(delta_omega[0][j] - delta_omega[0][i]);
					  }
				  }
			  }				
		  }
		//*************************************************************************************************************//
	
		err = 0.0; // reset error
		
		for (int i = 0; i < 4; i++)	
		  {	// initialize
			for (int j = 0; j < 4; j++) 
			  {
				for (int k = 0; k < 6; k++) 
				  {
					rho_k_real[i][j][k] = 0.0;
					rho_t_real[i][j][k] = 0.0;
					rho_k_imag[i][j][k] = 0.0;
					rho_t_imag[i][j][k] = 0.0;
				  }
			  }
		  }
		
		// Runge-Kutta Fehlberg RKF45
		for (int rk = 0; rk < 6; rk++) 
		  {
			
			for (int i = 0; i < states_num; i++) 
			  {
				for (int j = 0; j < states_num; j++) 
				  {
					if (rk == 0) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j];
						rho_t_imag[i][j][rk] = rho_imag[i][j];
					  } 
					else if (rk == 1) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j] + rho_k_real[i][j][0]/5.0;
						rho_t_imag[i][j][rk] = rho_imag[i][j] + rho_k_imag[i][j][0]/5.0;
					  } 
					else if (rk == 2) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j] + rho_k_real[i][j][0]*3.0/40.0 + rho_k_real[i][j][1]*9.0/40.0;
						rho_t_imag[i][j][rk] = rho_imag[i][j] + rho_k_imag[i][j][0]*3.0/40.0 + rho_k_imag[i][j][1]*9.0/40.0;
					  } 
					else if (rk == 3) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j] + rho_k_real[i][j][0]*3.0/10.0 - rho_k_real[i][j][1]*9.0/10.0 + rho_k_real[i][j][2]*6.0/5.0;
						rho_t_imag[i][j][rk] = rho_imag[i][j] + rho_k_imag[i][j][0]*3.0/10.0 - rho_k_imag[i][j][1]*9.0/10.0 + rho_k_imag[i][j][2]*6.0/5.0;
					  } 
					else if (rk == 4) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j] - rho_k_real[i][j][0]*11.0/54.0 + rho_k_real[i][j][1]*5.0/2.0 - rho_k_real[i][j][2]*70.0/27.0 + rho_k_real[i][j][3]*35.0/27.0;
						rho_t_imag[i][j][rk] = rho_imag[i][j] - rho_k_imag[i][j][0]*11.0/54.0 + rho_k_imag[i][j][1]*5.0/2.0 - rho_k_imag[i][j][2]*70.0/27.0 + rho_k_imag[i][j][3]*35.0/27.0;
					  } 
					else if (rk == 5) 
					  {
						rho_t_real[i][j][rk] = rho_real[i][j] + rho_k_real[i][j][0]*1631.0/55296.0 + rho_k_real[i][j][1]*175.0/512.0 + rho_k_real[i][j][2]*575.0/13824.0 + rho_k_real[i][j][3]*44275.0/110592.0 + 253.0/4096.0*rho_k_real[i][j][4];
						rho_t_imag[i][j][rk] = rho_imag[i][j] + rho_k_imag[i][j][0]*1631.0/55296.0 + rho_k_imag[i][j][1]*175.0/512.0 + rho_k_imag[i][j][2]*575.0/13824.0 + rho_k_imag[i][j][3]*44275.0/110592.0 + 253.0/4096.0*rho_k_imag[i][j][4];
					  }
				  }
			  }
			
			phase = M_PI*(tic/(T/s) - 0.5);
			
			for (int i = 0; i < states_num; i++) 
			  {
				for (int j = 0; j < states_num; j++) 
				  {
					if (i != j) 
					  {
						rho_k_real[i][i][rk] += Zfactor(phase)*Rabifreq[i][j]/2.0*(rho_t_imag[j][i][rk] - rho_t_imag[i][j][rk]);
						rho_k_imag[i][i][rk] += -Zfactor(phase)*Rabifreq[i][j]/2.0*(rho_t_real[j][i][rk] - rho_t_real[i][j][rk]);
						
						if (j > i) 
						  {
							rho_k_real[i][j][rk] += -Zfactor(phase)*Rabifreq[i][j]/2.0*(rho_t_imag[i][i][rk] - rho_t_imag[j][j][rk])
							  + delta_omega[i][j]*rho_t_imag[i][j][rk]; 
							rho_k_imag[i][j][rk] += Zfactor(phase)*Rabifreq[i][j]/2.0*(rho_t_real[i][i][rk] - rho_t_real[j][j][rk])
							  - delta_omega[i][j]*rho_t_real[i][j][rk];
							
							for (int ll = 0; ll < states_num; ll++) 
							  {		  
								if((i != ll) && (j != ll)) 
								  {
									rho_k_real[i][j][rk] += Zfactor(phase)*Rabifreq[i][ll]/2.0*rho_t_imag[ll][j][rk]
									  - Zfactor(phase)*Rabifreq[ll][j]/2.0*rho_t_imag[i][ll][rk];
									rho_k_imag[i][j][rk] += -Zfactor(phase)*Rabifreq[i][ll]/2.0*rho_t_real[ll][j][rk]
									  + Zfactor(phase)*Rabifreq[ll][j]/2.0*rho_t_real[i][ll][rk];
								  }
							  }
						  } 
						else 
						  {
							rho_k_real[i][j][rk] = rho_k_real[j][i][rk];
							rho_k_imag[i][j][rk] = -rho_k_imag[j][i][rk];
						  }
					  }
				  }
			  }
			
			for (int i = 0; i < states_num; i++) 
			  {
				for (int j = 0; j < states_num; j++) 
				  {
					rho_k_real[i][j][rk] *= timestep;
					rho_k_imag[i][j][rk] *= timestep;	    
				  }
			  }
			
		  }//loop rk end
      
		// Calculate final value: 5th order
		for (int i = 0; i < states_num; i++) 
		  {
			for (int j = 0; j < states_num; j++) 
			  {
				rho_real5_temp[i][j] = rho_real[i][j] + rho_k_real[i][j][0]*37.0/378.0 + rho_k_real[i][j][2]*250.0/621.0
				  + rho_k_real[i][j][3]*125.0/594.0 + rho_k_real[i][j][5]*512.0/1771.0;
				rho_imag5_temp[i][j] = rho_imag[i][j] + rho_k_imag[i][j][0]*37.0/378.0 + rho_k_imag[i][j][2]*250.0/621.0
				  + rho_k_imag[i][j][3]*125.0/594.0 + rho_k_imag[i][j][5]*512.0/1771.0;
			  }
		  }
		
      // Calculate final value: 4th order
    for (int i = 0; i < states_num; i++) 
	  {
		for (int j = 0; j < states_num; j++) 
		  {
			rho_real4_temp[i][j] = rho_real[i][j] + rho_k_real[i][j][0]*2825.0/27648.0 + rho_k_real[i][j][2]*18575.0/48384.0
			  + rho_k_real[i][j][3]*13525.0/55296.0 + rho_k_real[i][j][4]*277.0/14336.0 + rho_k_real[i][j][5]/4.0;
			rho_imag4_temp[i][j] = rho_imag[i][j] + rho_k_imag[i][j][0]*2825.0/27648.0 + rho_k_imag[i][j][2]*18575.0/48384.0
			  + rho_k_imag[i][j][3]*13525.0/55296.0 + rho_k_imag[i][j][4]*277.0/14336.0 + rho_k_imag[i][j][5]/4.0;
		  }
	  }
    (void)rho_imag4_temp; //Remove unused variable warning //Rikard.
      
	for (int i = 0; i < states_num; i++) 
	  {
		for (int j = 0; j < states_num; j++) 
		  {
			delta1 = rho_real4_temp[i][j] - rho_real5_temp[i][j];
			scale = atol + rtol*fmax(fabs(rho_real[i][j]),fabs(rho_real5_temp[i][j]));	 
			err += delta1*delta1/(scale*scale); 
		  }
	  }
      
    err=sqrt(err/(16.0));
    if(err<=1.0) 
	  {
		if(err==0) 
		  timefactor = maxtimefactor;
		else 
		  {
			timefactor = SECURITY*pow(err,-alpha)*pow(errold,beta);
			
			if (timefactor<mintimefactor) 
			  timefactor=mintimefactor;
			if (timefactor>maxtimefactor) 
			  timefactor=maxtimefactor;
		  }
		
		if (reject) 
		  nexttimestep=timestep*fmin(timefactor,1.0);
		else 
		  nexttimestep = timestep*timefactor;
		
		errold=fmax(err,1.0e-4);
		reject=false;
		stepcounter++;
		tic+=timestep;
		timestep = nexttimestep;
		
		for (int i = 0; i < states_num; i++) 
		  {
		  for (int j = 0; j < states_num; j++) 
			{
			  rho_real[i][j] = rho_real5_temp[i][j];
			  rho_imag[i][j] = rho_imag5_temp[i][j];
			}
		  }
		
      } 
	else 
	  {
		timefactor=fmax(SECURITY*pow(err,-alpha),mintimefactor);
		timestep *= timefactor;
		reject=true;
      }
	
	  } // end of while loop
	
	
    double transprob = 0;
    transprob = G4UniformRand();   // uniform random number in [0,1]
	
    if (transprob < rho_real[0][0]) 
	  {   // Jump to (0,0)
		aHbar->SetF(0);
		aHbar->SetM(0);
	  } 
	else if (transprob < rho_real[0][0] + rho_real[1][1]) 
	  {   // Jump to (1,1)
		aHbar->SetF(1);
		aHbar->SetM(1);
	  } 
	else if (transprob < rho_real[0][0] + rho_real[1][1] + rho_real[2][2]) 
	  {   // Jump to (1,0)
		aHbar->SetF(1);
		aHbar->SetM(0);
	  } 
	else if (transprob < rho_real[0][0] + rho_real[1][1] + rho_real[2][2] + rho_real[3][3]) 
	  {   // Jump to (1,-1)
		aHbar->SetF(1);
		aHbar->SetM(-1);
	  } 
	else 
	  {
		G4cout << "Calculation of transition probability in hbarMWSetup::MakeMWTransition() is screwed up!" << G4endl;
	  }    
  }
}
G4double hbarMWSetup::GetMWFieldB(G4double r, G4double phi) 
{
  // Calculates the amplitude of the oscillating magnetic field
  // at a given point (r, phi) in the cavity
  G4double B = 0.0;
  if (MW_mode == "TM110") 
	{
	  B = sqrt(sqr(GetMWFieldB_r(r, phi)) + sqr(GetMWFieldB_phi(r, phi)));
	} 
  else if (MW_mode == "TM010") 
	{
	  B = GetMWFieldB_phi(r, phi);
	} 
  else 
	{
	  B = GetMWFieldB0();
	}
  return B;
}

G4double hbarMWSetup::GetMWFieldB0() 
{
  // Calculates the amplitude of the oscillating magnetic field
  // at the center of the cavity
  G4double B0 = 0.0;
  if (MW_mode == "TM110") 
	{
	  B0 = sqrt(2.0*magn_permeab*cavity_Q*MW_power
				/(sqr(M_PI)*sqr(GetCavityInnerDiameter()/2.0)*GetCavityLength()
				  *nu_used*sqr(J1primeP11)));
	} 
  else if (MW_mode == "TM010") 
	{
	  B0 = sqrt(magn_permeab*cavity_Q*MW_power
				/(sqr(M_PI)*sqr(GetCavityInnerDiameter()/2.0)*GetCavityLength()
				  *nu_used*sqr(J1P01)));
	} 
  else 
	{
	  G4double aperture = GetCavityResonatorSeparation();
	  if (aperture <= 0) return 0.0;
	  B0 = sqrt((MW_power*cavity_Q*100.0*ohm)/sqr(c_light*aperture));
	}
  return B0;
}

G4double hbarMWSetup::GetMWFieldB_r(G4double r, G4double phi) {
  G4double B_r = 0.0;
  if (r == 0) 
	{ }
  else 
	{
	  if (MW_mode == "TM110") 
		{
		  G4double k = nu_used/c_light;
		  B_r = GetMWFieldB0()*J1->Eval(k*r)/(k*r)*sin(phi);
		} 
	  else if (MW_mode == "TM010") 
		{

		}
	}
  return B_r;
}

G4double hbarMWSetup::GetMWFieldB_phi(G4double r, G4double phi) 
{
  G4double k = nu_used/c_light;
  G4double B_phi = 0.0;
  if (MW_mode == "TM110") 
	{
	  B_phi = GetMWFieldB0()*J1->Derivative(k*r)*cos(phi);
	} 
  else if (MW_mode == "TM010") 
	{
	  B_phi = GetMWFieldB0()*J1->Eval(k*r);
	}
  return B_phi;
}

G4double hbarMWSetup::CalculateOptimumPower() 
{
  G4double power = 0.0;
  G4double v = sqrt(2.0*8.31*joule/kelvin*(runAction->GetPrimaryGeneratorAction()->GetSourceTemperature())/(0.001*kilogram));
  G4cout << "The most probable velocity for " << runAction->GetPrimaryGeneratorAction()->GetSourceTemperature()/kelvin
		 << " K is " << v/(m/s) << " m/s." << G4endl;
  //   G4double dist = TMath::Abs(hbarDet->GetCavityConst()->GetSextupole2Start() - hbarDet->GetCavityConst()->GetSextupole1End());
  G4double dist = TMath::Abs(hbarDet->GetCavityConst()->GetMWCavityLength());
  G4cout << "Average time in cavity: " << (dist/v)/millisecond << " ms." << G4endl;
  //   G4double b = (M_PI/2.0)/(dist/v);
  G4double b = (M_PI*2.0)/(dist/v);
  G4cout << "The optimum b for the Sigma_1 transition is " << b/kilohertz << " kilo(1/s), " << G4endl;
  //   G4double B = 2.0*hbar_Planck*b/(sqrt(2.0)*BohrMagneton);
  G4double B = hbar_Planck*b/(BohrMagneton);   // For Pi_1: hbar_Planck*b/(sqrt(2.0)*BohrMagneton)
  //   G4cout << "which needs B: " << B/tesla << " tesla." << G4endl;
  if (MW_mode == "TM110") 
	{
	  power = sqr(B)*(sqr(M_PI)*sqr(GetCavityInnerDiameter()/2.0)*GetCavityLength()
					  *nu_used*sqr(J1primeP11))/(2.0*magn_permeab*cavity_Q);
	} 
  else if (MW_mode == "TM010") 
	{
	  power = sqr(B)*(sqr(M_PI)*sqr(GetCavityInnerDiameter()/2.0)*GetCavityLength()
					  *nu_used*sqr(J1P01))/(magn_permeab*cavity_Q);
	} 
  else 
	{
	  power = sqr(c_light*GetCavityResonatorSeparation()*B)/(100*ohm*cavity_Q);
	}
  G4cout << "which needs " << B/tesla << " tesla at the center of the cavity," << G4endl
		 << "which in turn needs " << power/watt << " watt." << G4endl;
  G4double actBfield = GetMWFieldB(0.0, 0.0);
  G4cout << "Actual field in the cavity: " << actBfield/tesla << " tesla." << G4endl;
  return power;
}

void hbarMWSetup::Scan(G4int nevents) 
{
  if (runAction == NULL) 
	{
	  G4cerr << "Cannot start scan because no hbarRunAction object is connected" << G4endl
			 << "to the hbarMWSetup object." << G4endl;
	  return;
	}
  scanGraph = new TGraphErrors(scanSteps);
  scanGraph->SetMarkerStyle(20);
  scanGraph->SetDrawOption("AP");
  scanGraph->SetTitle("MC microwave scan");
  scanGraph->GetXaxis()->CenterTitle();
  scanGraph->GetXaxis()->SetTitle("microwave frequency (GHz)");
  scanGraph->GetYaxis()->CenterTitle();
  scanGraph->GetYaxis()->SetTitle("normalized number of events");

  scanInitGraph = new TGraphErrors(scanSteps);
  scanInitGraph->SetMarkerStyle(20);
  scanInitGraph->SetDrawOption("AP");
  scanInitGraph->SetTitle("MC event numbers");
  scanInitGraph->GetXaxis()->CenterTitle();
  scanInitGraph->GetXaxis()->SetTitle("microwave frequency (GHz)");
  scanInitGraph->GetYaxis()->CenterTitle();
  scanInitGraph->GetYaxis()->SetTitle("particles shot");

  scanNtuples = new TList();

 /*  B_field_value = 0.0000163122; 

   if(hbarDet->GetCavityConst()->GetUseCavityFieldMap()!=0) B_field_value = hbarDet->GetCavityConst()->GetcavityFieldmap()->Get_meanB_val();

   double B0 = 2.0*M_PI*nu_0/((GFactorElectron - GFactorProton*NuclearMagneton/BohrMagneton)*BohrMagneton);
   double B_over_B0 = B_field_value/B0;

  double nu_trans = 0.0;
  int transition = 1;
  if(transition == 1) nu_trans = nu_0*sqrt(1.0 + B_over_B0*B_over_B0);

  if(transition == 2) {
	nu_trans = 0.5*nu_0
	 - 0.5*(GFactorElectron + GFactorProton*NuclearMagneton/BohrMagneton)*B_field_value*BohrMagneton/(h_Planck)
	 + 0.5*nu_0*sqrt(1.0 + B_over_B0*B_over_B0);
  }

 // std::cout << "============================================== " << std::setprecision(10) << nu_trans << " " << scanStart << std::endl;

  scanStart = nu_trans - 0.000025;
  scanEnd = nu_trans + 0.000025;

 // std::cout << "============================================== " << std::setprecision(10) << nu_trans << " " << scanStart << std::endl;
*/
  G4double steplength = TMath::Abs(scanEnd - scanStart)/G4double(scanSteps);
  G4double nu_used_backup = nu_used;

  for (G4int s = 0; s <= scanSteps; s++) 
	{
	  
	  nu_used = scanStart + G4double(s)*steplength;
	  G4cout << "Scan step number: " << s+1 << "/" << scanSteps+1 << G4endl;
	  if (randPartNum) nevents = (int)( CLHEP::RandGauss::shoot(double(nevents), TMath::Sqrt(double(nevents))) );
	  
	  G4RunManager::GetRunManager()->BeamOn(nevents);
	  scanGraph->SetPoint(s, nu_used/(megahertz), runAction->GetHitCounter()/G4double(nevents));
	  scanGraph->SetPointError(s, 0.0, sqrt(runAction->GetHitCounter())/G4double(nevents));
	  scanInitGraph->SetPoint(s, nu_used/(megahertz), G4double(nevents));
	  scanInitGraph->SetPointError(s, 0.0, 0.0);
	  if (runAction->GetHitTree()->CloneTree() == NULL) std::cout << "NULL tree!" << std::endl;
	  TTree *scantree = runAction->GetHitTree()->CloneTree();
	  scantree->SetName(numtostr(nu_used/megahertz, 12, true).c_str());
	  scanNtuples->Add(scantree);
	}

  //  std::cout << "============================================== " << std::setprecision(10) << nu_trans << " " << scanStart << " "<< scanEnd << " " << h_Planck << std::endl;

  
  nu_used = nu_used_backup;
  scanFile = new TFile(scanFileName, "RECREATE");
  scanFile->cd(); // Added by Clemens
  scanGraph->Write("MW_scan");
  scanInitGraph->Write("MW_init");
  scanNtuples->Write("ScanTrees", 1);

  TObjString *tempstr = new TObjString(numtostr(scanStart/megahertz, 12, true).c_str());
  tempstr->Write("ScanFrequency_Start");
  delete tempstr;

  tempstr = new TObjString(numtostr(scanEnd/megahertz, 12, true).c_str());
  tempstr->Write("ScanFrequency_End");
  delete tempstr;

  tempstr = new TObjString(numtostr(scanSteps).c_str());
  tempstr->Write("ScanSteps");
  delete tempstr;


  tempstr = new TObjString(numtostr(hbarDet->GetWorldSizeXY()/m, 3, true).c_str());
  tempstr->Write("WorldSizeXY");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetWorldSizeZ()/m, 3, true).c_str());
  tempstr->Write("WorldSizeZ");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetCavityConst()->GetMWCavityLength()/m, 3, true).c_str());
  tempstr->Write("MWCavityLength");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetCavityConst()->GetMWCavityInnerDiameter()/m, 3, true).c_str());
  tempstr->Write("MWCavityInnerDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetCavityConst()->GetMWCavityOuterDiameter()/m, 3, true).c_str());
  tempstr->Write("MWCavityOuterDiameter");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetCavityConst()->GetMWCavityCenter()/m, 3, true).c_str());
  tempstr->Write("MWCavityCenter");
  delete tempstr;

  if(hbarDet->GetDDetConst() != NULL)
	{
	  tempstr = new TObjString(numtostr(hbarDet->GetDDetConst()->GetDetectorLength()/m, 3, true).c_str());
	  tempstr->Write("DetectorLength");
	  delete tempstr;
	  
	  tempstr = new TObjString(numtostr(hbarDet->GetDDetConst()->GetDetectorCenter()/m, 3, true).c_str());
	  tempstr->Write("DetectorCenter");
	  delete tempstr;
	  
	  tempstr = new TObjString(numtostr(hbarDet->GetDDetConst()->GetDetectorDiameter()/m, 3, true).c_str());
	  tempstr->Write("DetectorDiameter");
	  delete tempstr;
	}

  tempstr = new TObjString(numtostr(hbarDet->GetMagneticFieldSetup()->GetField()->GetFieldMaxValue(1)/tesla, 3, true).c_str());
  tempstr->Write("S1FieldMaxValue");
  delete tempstr;

  tempstr = new TObjString(numtostr(hbarDet->GetMagneticFieldSetup()->GetField()->GetFieldMaxValue(2)/tesla, 3, true).c_str());
  tempstr->Write("S2FieldMaxValue");
  delete tempstr;

  tempstr = new TObjString((hbarDet->GetMagneticFieldSetup()->GetAllowMajoranaTransitions()).c_str());
  tempstr->Write("AllowMajoranaTransitions");
  delete tempstr;

  tempstr = new TObjString((hbarDet->GetMagneticFieldSetup()->GetUseFieldLinesAngleDifference()).c_str());
  tempstr->Write("UseFieldLinesAngleDifference");
  delete tempstr;

  tempstr = NULL;
  if (hbarDet->GetMagneticFieldSetup()->GetField()->IsFieldFlipped(2)) 
	{
	  tempstr = new TObjString("on");
	} 
  else 
	{
	  tempstr = new TObjString("off");
	}
  
  tempstr->Write("S2Flipped");
  delete tempstr;

  tempstr = new TObjString(numtostr(GetFixedTransitionProbability(), 3, true).c_str());
  tempstr->Write("FixedTransitionProbability");
  delete tempstr;

  tempstr = new TObjString(GetUseFixedTransitionProbabilities().c_str());
  tempstr->Write("UseFixedTransitionProbabilities");
  delete tempstr;

  tempstr = new TObjString(numtostr(GetMWPower()/watt, 3, true).c_str());
  tempstr->Write("MWPower");
  delete tempstr;

  tempstr = new TObjString(numtostr(GetMWFrequency()/megahertz, 12, true).c_str());
  tempstr->Write("MWFrequency");
  delete tempstr;

  tempstr = new TObjString(numtostr(GetCavityQ(), 3, true).c_str());
  tempstr->Write("CavityQ");
  delete tempstr;

  tempstr = new TObjString(numtostr(GetCavityMagneticField()/tesla, 3, true).c_str());
  tempstr->Write("CavityMagneticField");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetNumberOfParticles()).c_str());
  tempstr->Write("NumberOfPrimaries");
  delete tempstr;

  tempstr = new TObjString(runAction->GetPrimaryGeneratorAction()->GetParticleName().c_str());
  tempstr->Write("PrimaryName");
  delete tempstr;

  tempstr = new TObjString(runAction->GetPrimaryGeneratorAction()->GetRndmEnergyFlag().c_str());
  tempstr->Write("RandomSourceEnergy");
  delete tempstr;

  tempstr = new TObjString(runAction->GetPrimaryGeneratorAction()->GetRndmStateFlag().c_str());
  tempstr->Write("RandomSourceState");
  delete tempstr;

  tempstr = new TObjString(runAction->GetPrimaryGeneratorAction()->GetRndmDirectionFlag().c_str());
  tempstr->Write("RandomSourceDirection");
  delete tempstr;

  tempstr = new TObjString(runAction->GetPrimaryGeneratorAction()->GetSliceFlag().c_str());
  tempstr->Write("SliceSource");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceStateF()).c_str());
  tempstr->Write("SourceState_F");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceStateM()).c_str());
  tempstr->Write("SourceState_M");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceCenter()/m, 3, true).c_str());
  tempstr->Write("SourceCenter");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceTemperature()/kelvin, 3, true).c_str());
  tempstr->Write("SourceTemperature");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetBeamKineticEnergy()/eV, 3, true).c_str());
  tempstr->Write("BeamKineticEnergy");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceFWHM_X()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_X");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceFWHM_Y()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_Y");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceFWHM_Z()/m, 3, true).c_str());
  tempstr->Write("SourceFWHM_Z");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceOffsetX()/m, 3, true).c_str());
  tempstr->Write("SourceOffset_X");
  delete tempstr;

  tempstr = new TObjString(numtostr(runAction->GetPrimaryGeneratorAction()->GetSourceOffsetY()/m, 3, true).c_str());
  tempstr->Write("SourceOffset_Y");
  delete tempstr;

  //scanFile->Write();
  scanFile->Close();
  G4cout << "MW scan ROOT file " << scanFileName << " written for " << scanSteps+1
	 << " points." << G4endl;
}
