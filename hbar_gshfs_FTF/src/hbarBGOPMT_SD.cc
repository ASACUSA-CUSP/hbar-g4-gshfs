// USER //
#include "hbarBGOPMT_SD.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <cstdlib>

//FFT
#include "TH1.h"
#include "TVirtualFFT.h"


// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

hbarBGOPMT_SD::hbarBGOPMT_SD(G4String SDname, hbarCPTConst* cptconst)
  : G4VSensitiveDetector(SDname), hitCollection(0)
{
  BGOPMTName = SDname;
  collectionName.insert(SDname);
  PMT_energyData.clear();
  localcptconst = cptconst;
  TotalCharge = 0.0;
  TotalEnergy = 0.0;
  ADCNumber = 0;
  write_to_dat_files = false;

  /*for(int i=0; i<64; i++) {
     PMT0_energyData.push_back(0.0);
     PMT1_energyData.push_back(0.0);
     PMT2_energyData.push_back(0.0);
     PMT3_energyData.push_back(0.0);
  }*/


}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
hbarBGOPMT_SD::~hbarBGOPMT_SD()
{
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...

void hbarBGOPMT_SD::Initialize(G4HCofThisEvent* HCE)
{
  //G4cout << "create new HitCollection " << GetName() << " " << collectionName[0] << G4endl;
  hitCollection = new hbarDetectorHitsCollection(GetName(), collectionName[0]);

  static G4int HCID = -1;
  //if (HCID<0) { HCID = GetCollectionID(0); }
  if (HCID<0)
    { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(hitCollection); }
  HCE->AddHitsCollection(HCID, hitCollection);

  hitCounter = 0;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
G4bool hbarBGOPMT_SD::ProcessHits(G4Step * step, G4TouchableHistory* ROhist)
{
	return ProcessHits_constStep(step,ROhist);
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
G4bool hbarBGOPMT_SD::ProcessHits_constStep(const G4Step* step, G4TouchableHistory* ROhist)
{
  G4TouchableHandle touchable = step->GetPreStepPoint()->GetTouchableHandle();
  track = step->GetTrack();
  G4String volume_name = track->GetVolume()->GetName();
  G4String part_name = track->GetParticleDefinition()->GetParticleName();
  G4StepPoint* thePostPoint = step->GetPostStepPoint();

 // G4cout << track->GetLogicalVolumeAtVertex()->GetName()   << "      " << track->GetParticleDefinition()->GetParticleName() << G4endl;
  
  G4VPhysicalVolume* thePostPV = thePostPoint->GetPhysicalVolume();
 //std::cout << thePostPV->GetName() << std::endl;
 // double wavelength = 1.2398/Etot*1.0e3; // wavelength in nm
  //double pmtgain = localcptconst->Get_PMTGain(); // # of e- at anode as a result of a single e- emitted at cathode
  //double ret_qe = CalculateQE_PMT(wavelength); // Quantum efficiency: q.e. = #e- / #photons
  //double rand_var =  G4UniformRand();   // uniform random number in [0,1]
  //bool ret_bool = false;
  //if(rand_var < ret_qe) ret_bool = true; // roll the dice

  double Etot = step->GetPreStepPoint()->GetTotalEnergy()/CLHEP::MeV; 

  if (step->GetTotalEnergyDeposit() <= 0.) return false;

  G4OpBoundaryProcessStatus boundaryStatus=Undefined;
  G4OpBoundaryProcess* boundary=NULL;


  //find the boundary process only once
  if(!boundary) {
    G4ProcessManager* pm = step->GetTrack()->GetDefinition()->GetProcessManager();
    G4int nprocesses = pm->GetProcessListLength();
    G4ProcessVector* pv = pm->GetProcessList();
    G4int i;
    for( i=0;i<nprocesses;i++) {
		//	std::cout << "############# " << (*pv)[i]->GetProcessName() << std::endl;
      if((*pv)[i]->GetProcessName()=="OpBoundary") {
        boundary = (G4OpBoundaryProcess*)(*pv)[i];
        break;
      }
    }
  }

  if(boundary!=NULL) boundaryStatus=boundary->GetStatus();

  G4ParticleDefinition* particleType = step->GetTrack()->GetDefinition();
//std::cout << "#################### " << particleType->GetParticleName() << std::endl;
  if(particleType==G4OpticalPhoton::OpticalPhotonDefinition()) {
    G4VisAttributes * redcolor = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
	//std::cout << "#################### " << thePostPV->GetName() << std::endl;
      //if(thePostPV->GetName()=="expHall")
      //Kill photons entering expHall from something other than Slab
      //theTrack->SetTrackStatus(fStopAndKill);

    //   double Etot = step->GetTotalEnergyDeposit()/eV;
	//std::cout << "####################### " << boundaryStatus <<  std::endl;
  if(thePostPoint->GetStepStatus()==fGeomBoundary) {
    
      //fExpectedNextStatus=Undefined;
      switch(boundaryStatus) {
      case Absorption: break;
      case Detection:
      {
		 if(thePostPV->GetName().contains("PMT")) {
			PMT_energyData.push_back(Etot);
			//track->GetVolume()->GetLogicalVolume()->SetVisAttributes(redcolor);
			/*int number = (int)(thePostPV->GetName()[16]-'0');
			char name2 = thePostPV->GetName()[18];
            char name3 = thePostPV->GetName()[19];
			int pmt = (int)(name2-'0');
			if(name3 != '_') pmt = pmt*10 + (int)(name3-'0');	
			//std::cout << thePostPV->GetName() << " " <<  pmt << std::endl;
			//PMT
			if(number==0) PMT0_energyData[pmt] += Etot;
			if(number==1) PMT1_energyData[pmt] += Etot;
			if(number==2) PMT2_energyData[pmt] += Etot;
			if(number==3) PMT3_energyData[pmt] += Etot;*/

			hitCounter++;
		}
		//track->SetTrackStatus(fKillTrackAndSecondaries);
        break;
      }
     // case FresnelReflection:
     // case TotalInternalReflection:
     // case LambertianReflection:
     // case LobeReflection:
     // case SpikeReflection:
    //  case BackScattering:
        //trackInformation->IncReflections();
        //fExpectedNextStatus=StepTooSmall;
       // break;
      default: break;
		
      }
  }
}
 // std::cout << "stat " << boundaryStatus << std::endl;   // only numbers

 /* G4VisAttributes * redcolor = new G4VisAttributes(G4Colour(1.0,0.0,0.0));

	
	if(step->GetPreStepPoint()->GetStepStatus() == fGeomBoundary && part_name == "opticalphoton") {  
	//G4cout << volume_name   << "      " << track->GetParticleDefinition()->GetParticleName() << G4endl;
		//if(ret_bool) {
   		 track->SetTrackStatus(fKillTrackAndSecondaries);
   		 hitCounter++;
		 track->GetVolume()->GetLogicalVolume()->SetVisAttributes(redcolor);
		 PMT_timeData.push_back(step->GetPreStepPoint()->GetGlobalTime()/ns);
		 PMT_energyData.push_back(Etot);
		 //PMT_chargeData.push_back(ret_qe*pmtgain);
    	//}
	}
*/
  return true;
}

// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
void hbarBGOPMT_SD::EndOfEvent(G4HCofThisEvent* HE)
{
  int evtnumber = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  std::string strev;
  std::stringstream out;
  out << evtnumber;
  strev = out.str();
  const double e = 1.602177*1e-19;
  TotalEnergy = 0.0;
  //for(int i=0; i<PMT_chargeData.size(); i++) TotalCharge += PMT_chargeData[i]*e;
  for(int i=0; i<PMT_energyData.size(); i++) TotalEnergy += PMT_energyData[i];

  if(write_to_dat_files==true) {
  	G4String fileName = "data_scintil/hits/" + GetName() + "_" + strev + ".dat";
  	std::ofstream fileOut(fileName);
  	for(int i=0; i<PMT_energyData.size(); i++) fileOut << PMT_energyData[i] << std::endl;
  }


  /*double tot0=0.0, tot1=0.0, tot2=0.0, tot3=0.0;
  for(int i=0; i<PMT0_energyData.size(); i++) {
		tot0 += PMT0_energyData[i];
        tot1 += PMT1_energyData[i];
		tot2 += PMT2_energyData[i];
		tot3 += PMT3_energyData[i];		
  }  
*/
  //std::cout << "****************** " << tot0+tot1+tot2+tot3 << " " << TotalEnergy << std::endl;

  G4cout << "***Event: " << evtnumber << " ... Number of tracks in '" << GetName() << "':	" << hitCounter << G4endl; //"	Total Charge (C): " << TotalCharge << "	ADC: " << ADCNumber << G4endl;
  PMT_energyData.clear();

  /*std::fill(PMT0_energyData.begin(), PMT0_energyData.end(), 0.0);
  std::fill(PMT1_energyData.begin(), PMT1_energyData.end(), 0.0);
  std::fill(PMT2_energyData.begin(), PMT2_energyData.end(), 0.0);
  std::fill(PMT3_energyData.begin(), PMT3_energyData.end(), 0.0);*/
  //PMT_chargeData.clear();
 // PMT_timeData.clear();
 //G4cout << "EndOfEvent method of SD '" << GetName() << "' called. " << GetNumberOfEvent() << G4endl;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...
double hbarBGOPMT_SD::CalculateQE_PMT(double wl)
{
	std::vector<double> qe_x = localcptconst->Get_photocath_PP();
	std::vector<double> qe_y = localcptconst->Get_photocath_EFF();

	std::vector<double>::iterator upperwl = std::upper_bound(qe_x.begin(), qe_x.end(), wl);
	std::vector<double>::iterator lowerwl = upperwl - 1;

	double qe_lower = qe_y[(lowerwl - qe_x.begin())];
	double qe_upper = qe_y[(upperwl - qe_x.begin())];

	// linear interpolation
	double ret_qe = qe_lower + (qe_upper - qe_lower)*(wl - *lowerwl)/(*upperwl - *lowerwl);

	return ret_qe;
}
// ...ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo......ooOO0OOoo...


