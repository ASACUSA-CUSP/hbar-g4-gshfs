#include "hbarBuildRealRootTree.hh"

hbarBuildHitTree::hbarBuildHitTree(hbarDetConst* dconst, TString filename){
  // open detector constructon
  detConst = dconst;

  // open root file
  rootfile = new TFile(filename,"update");
  rootfile->cd();

  // get HitTree
  hittree = (TTree *)rootfile->Get("hitTree");

  // associate the branches
  hittree->SetBranchAddress("event", &(theHit.Event));
  hittree->SetBranchAddress("energy", &(theHit.Edep));
  hittree->SetBranchAddress("time", &(theHit.Time));
  hittree->SetBranchAddress("position", &(theHit.PosX));
  hittree->SetBranchAddress("momentumdir", &(theHit.MomX));
  hittree->SetBranchAddress("particle", theHit.Particle);
  hittree->SetBranchAddress("detector", theHit.Detector);
  hittree->SetBranchAddress("PDG_code", &(theHit.PDGcode));
  // tajima
  hittree->SetBranchAddress("track_id",&(theHit.track_id));
  hittree->SetBranchAddress("parent_id",&(theHit.parent_id));
  construct_ND_TD_Tree();


  // create TTree for the real Hits, we call is just "T"
  constructTTree();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void hbarBuildHitTree::constructTTree(){
  // ROOT tree that holds the events as they would be seen with our detector
  retval = new TTree("T", "Realistic hits in the detectors");

  // now we need to access the detector geometry to get the active detectors
  // for this we use: Veto counters, Hodoscopes and the Hbar detector
  char buffer[128];
  for(int j=0; j<detConst->GetCPTConst()->GetHbarSegemtsX(); j++){
    for(int k=0; k<detConst->GetCPTConst()->GetHbarSegemtsY(); k++){
      sprintf(buffer,"HbarDet%i_Q", j* detConst->GetCPTConst()->GetHbarSegemtsY() + k);
      retval->Branch(buffer, &(realrootHit.HbarDet_Q[j* detConst->GetCPTConst()->GetHbarSegemtsY() + k]),
		     "HbarDet_Q/D");
      sprintf(buffer,"HbarDet%i_T", j* detConst->GetCPTConst()->GetHbarSegemtsY() + k);
      retval->Branch(buffer, &(realrootHit.HbarDet_T[j* detConst->GetCPTConst()->GetHbarSegemtsY() + k]),
		     "HbarDet_T/D");
    }
  }

  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfHodoscopes(); i++){
    for(int j=0; j<detConst->GetCPTConst()->GetHodoscopeSegments(i); j++){

      // The idea behind having two charges and timings for the hodoscope is
      // that we want to be as close as possible to te real produced data,
      // therefore I calculate the Qs and Ts from the hit position backwards.

      sprintf(buffer,"Hodoscope%i_%i_Q1", i, j);
      retval->Branch(buffer, &(realrootHit.Hodoscope_Q1[i][j]), "Hodoscope_Q1/D");
      sprintf(buffer,"Hodoscope%i_%i_T1", i, j);
      retval->Branch(buffer, &(realrootHit.Hodoscope_T1[i][j]), "Hodoscope_T1/D");

      sprintf(buffer,"Hodoscope%i_%i_Q2", i, j);
      retval->Branch(buffer, &(realrootHit.Hodoscope_Q2[i][j]), "Hodoscope_Q2/D");
      sprintf(buffer,"Hodoscope%i_%i_T2", i, j);
      retval->Branch(buffer, &(realrootHit.Hodoscope_T2[i][j]), "Hodoscope_T2/D");
    }
  }

  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfVetos(); i++){
    for(int j=0; j<detConst->GetCPTConst()->GetVetoSegments(i); j++){
      sprintf(buffer,"Veto%i_%i_Q", i, j);
      retval->Branch(buffer, &(realrootHit.Veto_Q[i][j]), "Veto_Q/D");
      sprintf(buffer,"Veto%i_%i_T", i, j);
      retval->Branch(buffer, &(realrootHit.Veto_T[i][j]), "Veto_T/D");
    }
  }

  retval->Branch("BGO_Edep",&(realrootHit.BGO_Edep), "BGO_Edep/D");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// added by tajima -- start
void hbarBuildHitTree::construct_ND_TD_Tree(){
  NDretval = new TTree("T_ND", "Hits in the Nagata detector");
  NDretval->Branch("nd_event",&nd_event,"nd_event/I");
  NDretval->Branch("nd_Edep",&nd_Edep,"nd_Edep/D");
  NDretval->Branch("nd_detector",nd_detector,"nd_detector[20]/C");
  NDretval->Branch("nd_particle",nd_particle,"nd_particle[20]/C");

  TDretval = new TTree("T_TD", "Hits in the track detector");
  TDretval->Branch("td_event",&td_event,"td_event/I");
  TDretval->Branch("td_Edep",&td_Edep,"td_Edep/D");
  TDretval->Branch("td_detector",td_detector,"td_detector[20]/C");
  TDretval->Branch("td_particle",td_particle,"td_particle[20]/C");
  TDretval->Branch("td_trackID",&td_trackID,"td_trackID/I");
  TDretval->Branch("td_parentID",&td_parentID,"td_parentID/I");
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void hbarBuildHitTree::Fill_ND_TD_Tree(){
  G4String ntdstr;
  sumND_Edep = 0;
  sumTD_Edep = 0;
  G4int num = hittree->GetEntries();
  for(int i=0;i<num;i++){
    hittree->GetEntry(i);
    ntdstr=theHit.Detector[0];
    for(int j=1;j<20;j++){ntdstr += theHit.Detector[j];}
    if(ntdstr.contains("NDBGO") || ntdstr.contains("NDplsciFront") || ntdstr.contains("NDplsciBack") ||
       ntdstr.contains("NDplsciRight") || ntdstr.contains("NDplsciLeft") || ntdstr.contains("NDplsciTop")){
      for(int j=0;j<20;j++){curNDdet[j] = theHit.Detector[j];}
      for(int j=0;j<20;j++){curNDpar[j] = theHit.Particle[j];}
      curND_Tid = theHit.track_id;
      curND_Pid = theHit.parent_id;
      curND_eve = theHit.Event;

      if(i!=0){
        sameflag=0;
        for(int j=0;j<20;j++){
          if(preNDdet[j]!=curNDdet[j]){sameflag+=1;}
        }
        if(sameflag==0 && (preND_Tid==curND_Tid) && (preND_Pid==curND_Pid)){
          sumND_Edep += theHit.Edep;
        }
        else{
          nd_Edep = sumND_Edep;
          for(int j=0;j<20;j++){nd_detector[j] = preNDdet[j];}
          for(int j=0;j<20;j++){nd_particle[j] = preNDpar[j];}
          nd_event = preND_eve;
          NDretval->Fill();
          sumND_Edep = theHit.Edep;
        }
      }

      if(i==(num-1)){
        nd_Edep = sumND_Edep;
        for(int j=0;j<20;j++){nd_detector[j] = curNDdet[j];}
        for(int j=0;j<20;j++){nd_particle[j] = curNDpar[j];}
        nd_event = curND_eve;
        NDretval->Fill();
      }
      else{
        preND_Pid = curND_Pid;
        preND_Tid = curND_Tid;
        preND_eve = curND_eve;
        for(int j=0;j<20;j++){preNDdet[j] = curNDdet[j];}
        for(int j=0;j<20;j++){preNDpar[j] = curNDpar[j];}
        if(i==0){sumND_Edep = theHit.Edep;}
      }
    }
    else if(ntdstr.contains("CuspTPlsci0") || ntdstr.contains("CuspTPlsci1")){
      for(int j=0;j<20;j++){curTDdet[j] = theHit.Detector[j];}
      for(int j=0;j<20;j++){curTDpar[j] = theHit.Particle[j];}
      curTD_Tid = theHit.track_id;
      curTD_Pid = theHit.parent_id;
      curTD_eve = theHit.Event;

      if(i!=0){
        sameflag=0;
        for(int j=0;j<20;j++){
          if(preTDdet[j]!=curTDdet[j]){sameflag+=1;}
        }
        if(sameflag==0 && (preTD_Tid==curTD_Tid) && (preTD_Pid==curTD_Pid)){
          sumTD_Edep += theHit.Edep;
        }
        else{
          td_Edep = sumTD_Edep;
          for(int j=0;j<20;j++){td_detector[j] = preTDdet[j];}
          for(int j=0;j<20;j++){td_particle[j] = preTDpar[j];}
          td_event = preTD_eve;
          td_trackID = preTD_Tid;
          td_parentID = preTD_Pid;
          TDretval->Fill();
          sumTD_Edep = theHit.Edep;
        }
      }

      if(i==(num-1)){
        td_Edep = sumTD_Edep;
        for(int j=0;j<20;j++){td_detector[j] = curTDdet[j];}
        for(int j=0;j<20;j++){td_particle[j] = curTDpar[j];}
        td_event = curTD_eve;
        td_trackID = curTD_Tid;
        td_parentID = curTD_Pid;
        TDretval->Fill();
      }
      else{
        preTD_Pid = curTD_Pid;
        preTD_Tid = curTD_Tid;
        preTD_eve = curTD_eve;
        for(int j=0;j<20;j++){preTDdet[j] = curTDdet[j];}
        for(int j=0;j<20;j++){preTDpar[j] = curTDpar[j];}
        if(i==0){sumTD_Edep = theHit.Edep;}
      }
    }
  }
}
// added by tajima -- stop


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
hbarBuildHitTree::~hbarBuildHitTree(){
  retval->Write();
  NDretval->Write();   // added by tajima
  TDretval->Write();   // added by tajima
  rootfile->Close();
  rootfile = NULL;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void hbarBuildHitTree::FillRealHitTree() {
  // Step 1: identify full events in the hittree
  long long int entries = hittree->GetEntriesFast();

  //std::cout << "1" << std::endl;
  int oldnum = 0;

  // Central detector buffer
  Double_t *hbarcharge  = new Double_t [detConst->GetCPTConst()->GetHbarSegemtsX()*detConst->GetCPTConst()->GetHbarSegemtsY()];
  Double_t *hbartime    = new Double_t [detConst->GetCPTConst()->GetHbarSegemtsX()*detConst->GetCPTConst()->GetHbarSegemtsY()];

  //bgo buffer
  Double_t bgoedep = 0.0;

  // Hodoscope buffer
  Double_t **hodocharge = new Double_t*[detConst->GetCPTConst()->GetNumberOfHodoscopes()];
  Double_t **hodotime   = new Double_t*[detConst->GetCPTConst()->GetNumberOfHodoscopes()];
  Double_t **hit_z_val  = new Double_t*[detConst->GetCPTConst()->GetNumberOfHodoscopes()];

  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfHodoscopes(); i++){
    // allocating memory for the buffers
    hodocharge[i] = new Double_t[detConst->GetCPTConst()->GetHodoscopeSegments(i)];
    hodotime[i]   = new Double_t[detConst->GetCPTConst()->GetHodoscopeSegments(i)];
    hit_z_val[i]  = new Double_t[detConst->GetCPTConst()->GetHodoscopeSegments(i)];

    // initilaizing the buffers with 0
    for(int j=0; j<detConst->GetCPTConst()->GetHodoscopeSegments(i); j++){
      hodocharge[i][j] = 0;
      hodotime  [i][j] = 0;
      hit_z_val [i][j] = 0;}
  }

  // Veto counter buffer
  Double_t **vetocharge = new Double_t*[detConst->GetCPTConst()->GetNumberOfVetos()];
  Double_t **vetotime   = new Double_t*[detConst->GetCPTConst()->GetNumberOfVetos()];

  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfVetos(); i++){
    vetocharge[i] = new Double_t[detConst->GetCPTConst()->GetVetoSegments(i)];
    vetotime[i]   = new Double_t[detConst->GetCPTConst()->GetVetoSegments(i)];

    for(int j=0; j<detConst->GetCPTConst()->GetVetoSegments(i); j++){
      vetocharge[i][j] = 0;
      vetotime  [i][j] = 0;}
  }
  //std::cout << "3" << std::endl;


  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfHodoscopes(); i++){

  }

  for(long long int i=0; i<entries; i++){ // hit loop
    //std::cout << "Eventnummer: " << i << "/" << entries<< std::endl;
    hittree->GetEvent(i);

    // now the single events are stored in theHit
    // the hits are stores sequential in the TTree
    // therefore we don't need to loop twice (hopefully)

    if(oldnum != (int)theHit.Event && i > 0){
      realrootHit.isHit = theHit.Hit;

      // Loop over Hbar detector
      for(int hbar=0; hbar<detConst->GetCPTConst()->GetHbarSegemtsX()*detConst->GetCPTConst()->GetHbarSegemtsY()
	    ; hbar++){
	//std::cout << "TEST" << "charge:" << hbarcharge[hbar] << " time:" <<  hbartime[hbar] << std::endl;
	realrootHit.HbarDet_Q[hbar] = hbarcharge[hbar];

	hbarcharge[hbar] = 0;
	realrootHit.HbarDet_T[hbar] = hbartime[hbar];

	hbartime[hbar] = 0;
      }

      // Loop over Hodoscopes
      for(int hodo=0; hodo<detConst->GetCPTConst()->GetNumberOfHodoscopes(); hodo++){
	// calculate ratios from hit position
	// Loop over segments
	for(int seg=0; seg<detConst->GetCPTConst()->GetHodoscopeSegments(hodo); seg++){
	  if(hit_z_val[hodo][seg] > 0){
	    G4double relpos = 1000*(hit_z_val[hodo][seg])-
	      detConst->GetCPTConst()->GetHodoscopeCenter(hodo) +
	      (detConst->GetCPTConst()->GetHodoscopeLength(hodo) / 2);
	    G4double ratio = relpos/detConst->GetCPTConst()->GetHodoscopeLength(hodo);
	    /*std::cout << "ratio= " << ratio
		      << "charge=" << hodocharge[hodo][seg]
		      << "r*c = "  << ratio * hodocharge[hodo][seg]
		      << std::endl;*/
	    realrootHit.Hodoscope_Q1[hodo][seg] = ratio * hodocharge[hodo][seg];
	    realrootHit.Hodoscope_Q2[hodo][seg] =
	      hodocharge[hodo][seg]-realrootHit.Hodoscope_Q1[hodo][seg];
	    hodocharge[hodo][seg] = 0;
	  }
	  else {
	    realrootHit.Hodoscope_Q1[hodo][seg] = 0;
	    realrootHit.Hodoscope_Q2[hodo][seg] = 0;

	  }
	  realrootHit.Hodoscope_T1[hodo][seg] = hodotime[hodo][seg];
	  realrootHit.Hodoscope_T2[hodo][seg] = hodotime[hodo][seg];

	}
	}

      // Loop over Veto counters
      for(int veto=0; veto<detConst->GetCPTConst()->GetNumberOfVetos(); veto++){
	for(int seg=0; seg<detConst->GetCPTConst()->GetVetoSegments(veto); seg++){
	  realrootHit.Veto_Q[veto][seg] = vetocharge[veto][seg];
	  realrootHit.Veto_T[veto][seg] = vetotime[veto][seg];
	  vetocharge[veto][seg] = 0;
	}
      }

      // now we fill the TTree if the Trigger is hit
      bool good=false;
      for(int num=0; num<detConst->GetCPTConst()->GetHbarSegemtsX()*detConst->GetCPTConst()->GetHbarSegemtsY(); num++){
	if (realrootHit.HbarDet_Q[num] > 0) good = true;
      }
      if(good)
	retval->Fill();
      }

    // Get number of HbarDet element
    TString buffer;
    if (TString(theHit.Detector).Contains("HbarDet")) {
      buffer = TString(theHit.Detector);
      buffer.Remove(0,8);
      int x = buffer.SubString(0,buffer.First("-")).String().Atoi() - 1;
      buffer.Remove(0,buffer.First("-")+1);
      int y = buffer.SubString(0,buffer.Sizeof()).String().Atoi() - 1;
      //std::cout << "x: " << x << " y: " << y << " elem: "
      //	<< x*detConst->GetCPTConst()->GetHbarSegemtsY() + y  <<std::endl;
      //if(theHit.Edep>0)
      //std::cout << "Edep hbar= " << theHit.Edep  <<  " " << theHit.Particle << std::endl;

      hbarcharge[x*detConst->GetCPTConst()->GetHbarSegemtsY() + y] += theHit.Edep;
      hbartime[x*detConst->GetCPTConst()->GetHbarSegemtsY() + y] = theHit.Time;
    }

    // Get number of Hodoscope element
    if (TString(theHit.Detector).Contains("Hodoscope")){
      buffer = TString(theHit.Detector);
      buffer.Remove(0,9);
      int num;
      int seg;

      /* name after "-" not valid for octagonal Hodoscope as different labeled:
      HodoscopeX/Y with X=Hodoscopelayer and Y=HodoscopeSegment */
      if(hbarDetConst::GetUseHbar2014Det()) //Switchover between Round Hodoscope and octagonal Hodoscope, because different Detectorlabels used
      {
        buffer.Remove(buffer.First("-"),buffer.Sizeof()); //after "-" not interesting for octagonal Hodoscope as x/y x..
        num = buffer.SubString(0,buffer.First("/")).String().Atoi(); //Which Hodoscope: 0 for inner, 1 for outer
        buffer.Remove(0,buffer.First("/")+1);
        seg = buffer.SubString(0,buffer.Sizeof()).String().Atoi(); //Which Hodoscope Scintillator: 0 to 31 per Hodoscope
      }

      else
      {
        num = buffer.SubString(0,buffer.First("-")).String().Atoi() - 1;
        buffer.Remove(0,buffer.First("-")+1);
        seg = buffer.SubString(0,buffer.Sizeof()).String().Atoi() - 1;
      }


      //if(theHit.Edep>0)
      //std::cout << "Edep hodo= " << theHit.Edep <<  " " << theHit.Particle <<std::endl;

      hodocharge[num][seg] += theHit.Edep;
      hodotime[num][seg] = theHit.Time;

      hit_z_val[num][seg] = theHit.PosZ;
    }

    // Get number of Veto element
    if (TString(theHit.Detector).Contains("Veto")){
      buffer = TString(theHit.Detector);
      //std::cout << buffer << std::endl;
      buffer.Remove(0,4);
      //std::cout << buffer << std::endl;
      int num = buffer.SubString(0,buffer.First("-")).String().Atoi() - 1;
      buffer.Remove(0,buffer.First("-")+1);
      //std::cout << buffer << std::endl;
      int seg = buffer.SubString(0,buffer.Sizeof()).String().Atoi() - 1;
      //std::cout << "veto num: " << num << " seg: " << seg << std::endl;

      vetocharge[num][seg] += theHit.Edep;
      vetotime[num][seg] = theHit.Time;
      //std::cout << buffer << std::endl;
      }

    oldnum = (int)theHit.Event;
    // hbarcharge[0] = theHit.PosZ;
    // realrootHit.HbarDet_Q[0] = hbarcharge[0];
    // std::cout << theHit.PosZ << std::endl;
    // retval->Fill();
  }


  // cleaning the memory
    for(int i=0; i<detConst->GetCPTConst()->GetNumberOfHodoscopes(); i++){
    delete [] hodocharge[i];
    delete [] hodotime[i];
    delete [] hit_z_val[i];
    }
    for(int i=0; i<detConst->GetCPTConst()->GetNumberOfVetos(); i++){
    delete [] vetocharge[i];
    delete [] vetotime[i];
    }
  delete [] hodocharge;
  delete [] vetocharge;
  delete [] hbarcharge;
  delete [] hodotime;
  delete [] hit_z_val;
  delete [] vetotime;


  /*for(int i=0; i<detConst->GetCPTConst()->GetNumberOfHodoscopes(); i++)
    delete [] hodotime[i];
  for(int i=0; i<detConst->GetCPTConst()->GetNumberOfVetos(); i++)
    delete [] vetotime[i];
  delete [] hodotime;
  delete [] vetotime;
  delete [] hbartime;*/
}
