#include "hbarBeamlineConst.hh"

hbarBeamlineConst::hbarBeamlineConst()
{
  BPpt = -1;
  for (G4int i = 0; i < maxBPN; i++)
	{
	  useBP[i] = false;
	  visBP[i] = true;
	  useLS[i] = false;
	}

  useConcreteBlock = false;

  for (G4int i=0; i<maxCF100Cross; i++)
    useCF100Cross[i] = false;
  for (G4int i = 0; i < maxLSN; i++)
	{
	  NumOfLSSides[i] = 0;
	}

   for (G4int i=0; i<maxGV; i++)
    useGV[i] = false;

   for (G4int i=0; i<maxFI; i++)
     useFI[i]=false; 

  beamlineMessenger = new hbarBeamlineMess(this);
  CFExt = false;
}

hbarBeamlineConst::~hbarBeamlineConst()
{
  delete beamlineMessenger;
  //if (ExtFIField) delete ExtFIField;
}


void hbarBeamlineConst::MakeBeamline(G4VPhysicalVolume * WorldPhy)
{

  // added by tajima
  /*  if(useCUSP && useNagataDetector)
	{
	  BPStart[4] = CuspOVCPosZ[9]+CuspOVCHalfLen[9];
	  }*/ //Comment by Rikard: This should be dynamic anyway. But useNagataDetector is hard-coded to be false, so I won't bother right now.

  for (G4int i = 0; i < maxBPN; i++)
	{
	  if (useBP[i] && ((BPEnd[i]-BPStart[i]) > 0.0) && (BPFrontInnerDiam[i] < BPFrontOuterDiam[i]) && (BPRearInnerDiam[i] < BPRearOuterDiam[i])
		  )
		{
		  G4String bpname = "BP" + numtostr(i+1);
		  BPSol[i] = new G4Cons(bpname,
								BPFrontInnerDiam[i]/2.0, BPFrontOuterDiam[i]/2.0,
								BPRearInnerDiam[i]/2.0, BPRearOuterDiam[i]/2.0,
								(BPEnd[i]-BPStart[i])/2.0, 0.0, 2.0*M_PI);
		  BPLog[i] = new G4LogicalVolume(BPSol[i], hbarDetConst::GetMaterial("Sts"), bpname);
		  //         BPLog[i]->SetUserLimits(alim);
		  if (!visBP[i]) BPLog[i]->SetVisAttributes(G4VisAttributes::Invisible);
		  BPPhy[i] = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, (BPEnd[i]+BPStart[i])/2.0),
									   bpname, BPLog[i], WorldPhy, false, 0);
		}
	}

  // Lead shields
  for (G4int i = 0; i < maxLSN; i++)
	{
	  if (useLS[i])
		{
		  G4String lsname = "LS" + numtostr(i+1);
		  if (NumOfLSSides[i] == 0)
			{
			  LSSol[i] = new G4Cons(lsname,
									LSFrontInnerDiam[i]/2.0, LSFrontOuterDiam[i]/2.0,
									LSRearInnerDiam[i]/2.0, LSRearOuterDiam[i]/2.0,
									(LSEnd[i]-LSStart[i])/2.0, 0.0, 2.0*M_PI);
			}
		  else if (NumOfLSSides[i] >= 3)
			{
			  G4double sc_r_0 [] = {LSFrontInnerDiam[i]/2.0, LSRearInnerDiam[i]/2.0};
			  G4double sc_r [] = {LSFrontOuterDiam[i]/2.0, LSRearOuterDiam[i]/2.0};
			  G4double sc_z [] = {-(LSEnd[i]-LSStart[i])/2.0, (LSEnd[i]-LSStart[i])/2.0};
			  LSSol[i] = new G4Polyhedra(lsname, 0.0, 2.0*M_PI,
										 NumOfLSSides[i], 2,
										 sc_z, sc_r_0, sc_r);
			}
		  LSLog[i] = new G4LogicalVolume(LSSol[i], hbarDetConst::GetMaterial("Lead"), lsname);

		  LSPhy[i] = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, (LSEnd[i]+LSStart[i])/2.0),
									   lsname, LSLog[i], WorldPhy, false, 0);

		}
	}

    // CF 100
    G4double cf100flangediam = 152.5*mm/2;
    G4double cf100innerdiam = 100*mm/2;
    G4double cf100flangethick = 21.5*mm;
    G4double cf100wallthick = 2*mm;
    G4double cf100crosscentralshererad = (162/2) *mm - cf100wallthick;
    G4double cf100crosspipelen = 135*mm;


    G4String name = "CF100Cross";
    G4Sphere *CrossSphereOuterSol = new G4Sphere(name + "SphereSol",0,
				       cf100crosscentralshererad + cf100wallthick,
				       0, 2.0*M_PI, 0, 1.0*M_PI);
    G4Sphere *CrossSphereInnerSol = new G4Sphere(name + "SphereSol",0,
				       cf100crosscentralshererad,
				       0, 2.0*M_PI, 0, 1.0*M_PI);

    G4Tubs *CrossTubeOuterSol = new G4Tubs(name + "SphereTubSol1", 0,
				     cf100innerdiam+cf100wallthick, cf100crosspipelen,
				     0, 2*M_PI);
    G4Tubs *CrossTubeInnerSol = new G4Tubs(name + "SphereTubSol1", 0,
				     cf100innerdiam, cf100crosspipelen+1*cm,
				     0, 2*M_PI);
    G4Tubs *CF100CrossFlangeSol = new G4Tubs(name + "SphereTubSol2", cf100innerdiam,
				     cf100flangediam, cf100flangethick/2,
				     0, 2*M_PI);

    G4RotationMatrix yRot;
    yRot.rotateY(M_PI/2.*rad);

    G4UnionSolid *rawstepo1 = new G4UnionSolid("cf100crossrawstepo1", CrossSphereOuterSol,
					      CrossTubeOuterSol);
    G4UnionSolid *rawstepo2 = new G4UnionSolid("cf100crossrawstepo2", rawstepo1,
					      CrossTubeOuterSol,&yRot,G4ThreeVector(0,0,0));
    G4UnionSolid *rawstepi1 = new G4UnionSolid("cf100crossrawstepi1", CrossSphereInnerSol,
					      CrossTubeInnerSol);
    G4UnionSolid *rawstepi2 = new G4UnionSolid("cf100crossrawstepi2", rawstepi1,
					      CrossTubeInnerSol,&yRot,G4ThreeVector(0,0,0));
    G4SubtractionSolid *base = new G4SubtractionSolid("cf10base",rawstepo2,rawstepi2);
    G4LogicalVolume *CF100FlangeLog = new G4LogicalVolume(CF100CrossFlangeSol, hbarDetConst::GetMaterial("Sts"), name+"LOGf1");
    G4LogicalVolume *CF100CrossLog = new G4LogicalVolume(base, hbarDetConst::GetMaterial("Sts"), name+"LOG");

    for (G4int i=0; i<maxCF100Cross; i++)
	  {
		if (!useCF100Cross[i])
		  continue;

		G4RotationMatrix *rot = new G4RotationMatrix;
		G4ThreeVector offset1 = G4ThreeVector(0,0,-( cf100crosspipelen - cf100flangethick/2));
		G4ThreeVector offset2 = G4ThreeVector(-( cf100crosspipelen - cf100flangethick/2),0,0);

		*rot = yRot * CF100CrossRotation[i];
		name +=  numtostr(i+1);

		offset1 = CF100CrossRotation[i] * offset1;
		offset2 = CF100CrossRotation[i] * offset2;


		CF100CrossBody[i] = new G4PVPlacement(&CF100CrossRotation[i],
											  CF100CrossPlacement[i],
											  name, CF100CrossLog, WorldPhy,
											  false, 0);


		CF100CrossFl1[i] = new G4PVPlacement(&CF100CrossRotation[i],
											 CF100CrossPlacement[i]+offset1,
											 name, CF100FlangeLog, WorldPhy,
											 false, 0);
		CF100CrossFl2[i] = new G4PVPlacement(&CF100CrossRotation[i],
											 CF100CrossPlacement[i]-offset1,
											 name, CF100FlangeLog, WorldPhy,
											 false, 0);
		CF100CrossFl3[i] = new G4PVPlacement(rot,
											 CF100CrossPlacement[i]-offset2,
											 name, CF100FlangeLog, WorldPhy,
											 false, 0);

		CF100CrossFl4[i] = new G4PVPlacement(rot,
											 CF100CrossPlacement[i]+offset2,
											 name, CF100FlangeLog, WorldPhy,
											 false, 0);

		// CF100 gate valve

	  }

  // ---------------------------------- GATE VALVE CONSTUCTION START ----------------------------------//

  G4Tubs*             GVTube[maxGV];   // Temporary Array-Pointer used for the Gate Valve Construction
  G4Tubs*             GVBigTube[maxGV];
  G4Box*              GVBox[maxGV];
  G4Box*              GVTopBox[maxGV];
  G4UnionSolid*       GVTopParts[maxGV];
  G4SubtractionSolid* GVBody[maxGV];
  G4double            GVTopBoxDisp[maxGV];

  G4double epsilon = 2.*um; //Epsilon used to impose safety margin in G4SubtractionSolid

  for (G4int i = 0; i < maxGV; i++)
	{
	  if (useGV[i] && ((GVEnd[i]-GVStart[i]) > 0.0) && (GVInnerDim[i] < GVOuterDim[i]) )
		{
		  G4String GVPipeName = "GateValvePipe" + numtostr(i+1);
          G4String GVnameTop = "GateValveTopParts" + numtostr(i+1);

          // Creating the Basis Pipe of the Gate Valve (Visible in all Drawings)
          GVTube[i] = new G4Tubs("GVTube", GVInnerDim[i]/2.0, GVOuterDim[i]/2.0, (GVEnd[i]-GVStart[i])/2.0, 0.0, 2.0*M_PI);
          GVPipeLog[i] = new G4LogicalVolume(GVTube[i], hbarDetConst::GetMaterial("Sts"), GVPipeName);
          GVPipePhy[i] = new G4PVPlacement(0, G4ThreeVector(0.0, 0.0, (GVEnd[i]+GVStart[i])/2.0), GVPipeName, GVPipeLog[i], WorldPhy, false, 0);

          // Creating the top parts of the Gate Valve (not visible in some OpenGL viewer <- famous boolean processor "limitation")
          GVBox[i] = new G4Box("GVBox", GVOuterDim[i]/2, GVBonnetHeight[i]/2, (GVEnd[i]-GVStart[i])/2 );
		  GVBigTube[i] = new G4Tubs("GVBigTube", 0, GVOuterDim[i]/2.0, (GVEnd[i]-GVStart[i]+epsilon)/2, 0.0, 2.0*M_PI); //cutting this out of the box
          GVTopBox[i] = new G4Box("GVTopBox", GVTopBoxLength[i]/2, GVTopBoxHeight[i]/2, GVTopBoxWidth[i]/2); // Top Motor Box

          GVTopBoxDisp[i] = GVBonnetHeight[i]/2 + GVTopBoxHeight[i]/2;

          GVBody[i] = new G4SubtractionSolid("GVBody",GVBox[i],GVBigTube[i], 0, G4ThreeVector(0, -GVBonnetHeight[i]/2, 0));
          GVTopParts[i] = new G4UnionSolid("GVTopParts", GVBody[i], GVTopBox[i], 0, G4ThreeVector(0, GVTopBoxDisp[i], 0));

          GVTopLog[i] = new G4LogicalVolume(GVTopParts[i], hbarDetConst::GetMaterial("Sts"), GVnameTop); // Top Parts of Gate Valve
          GVTopPhy[i] = new G4PVPlacement(0, G4ThreeVector(0.0, GVBonnetHeight[i]/2, (GVEnd[i]+GVStart[i])/2.0), GVnameTop, GVTopLog[i], WorldPhy, false, 0);

		}
	}  //------------------------------- GATE VALVE CONSTRUCTION END ----------------------------------------------//

	   //------------------------------- CROSS FLANGE EXTERNALS START ---------------------------------------------//

  if (CFExt == 1){
    G4double FlangeClosureThick  = 20*mm;
    G4double FlangeClosureRad    = 152.5*mm/2.;
    G4double FlangeClosure1XPos  = 155*mm;     //Half Size of the CrossFlange 145mm
    G4double FlangeClosure2XPos  = -155*mm;    //Half Size of the CrossFlange
    G4double VacPumpRad          = 132/2.*mm;  //Dimensions of Vaccuum Pump
    G4double VacPumpLength       = 200/2.*mm;
    G4double VacPumpXPos         = 265*mm;     //Half Size of the CrossFlange

    G4RotationMatrix* RotCFClosure = new G4RotationMatrix(); // Rotate the parts
    RotCFClosure->rotateY(90*deg);
    //Cross flange closures
    G4Tubs* CFClosureTub = new G4Tubs("CrossFlangeClosure", 0, FlangeClosureRad, FlangeClosureThick, 0.0, 2.0*M_PI);
    G4LogicalVolume* CFClosure1Log = new G4LogicalVolume(CFClosureTub, hbarDetConst::GetMaterial("Sts"), "CrossFlangeClosure1");
    CFClosurePhy = new G4PVPlacement(RotCFClosure, G4ThreeVector(FlangeClosure1XPos, 0.0, CF100CrossPlacement[0].z()), "CrossFlangeClosure1", CFClosure1Log, WorldPhy, false, 0);
    G4LogicalVolume* CFClosure2Log = new G4LogicalVolume(CFClosureTub, hbarDetConst::GetMaterial("Sts"), "CrossFlangeClosure2");
    CFClosure2Phy = new G4PVPlacement(RotCFClosure, G4ThreeVector(FlangeClosure2XPos, 0.0, CF100CrossPlacement[0].z()), "CrossFlangeClosure2", CFClosure2Log, WorldPhy, false, 0);

    //Create vaccuum pump
    G4Tubs* CFVacPump = new G4Tubs("CrossFlangeClosure", 0, VacPumpRad, VacPumpLength, 0.0, 2.0*M_PI);
    G4LogicalVolume* CFVacPumpLog = new G4LogicalVolume(CFVacPump, hbarDetConst::GetMaterial("Sts"), "VaccuumPump at CF");
    CFVacPumpPhy = new G4PVPlacement(RotCFClosure, G4ThreeVector(VacPumpXPos, 0.0, CF100CrossPlacement[0].z()), "VaccuumPump at CF", CFVacPumpLog, WorldPhy, false, 0);
  }   //--------------------------------- CROSS FLANGE EXTERNALS END -----------------------------------------------//

      //------------------------------------ FIELD IONIZER START ---------------------------------------------------//

    G4double FILeftFlOutDiam      = 203*mm;
    G4double FILeftInnDiam        = 155*mm;
    G4double FILeftFlLength       = 20*mm;
    G4double FILeftOutDiam        = 159*mm;
    G4double FILeftLength         = 97*mm;
    G4double FIMiddleLength       = 8*mm;
    G4double FIRightInnerDiam     = 104*mm;
    G4double FIRightZylOutDiam    = 108*mm;
    G4double FIRightLength        = 62*mm;
    G4double FIRightFlOutDiam     = 152*mm;
    G4double FIRightFlLength      = 20*mm;
    G4double FITopBotDist         = 244*mm;
    G4double FITopBotInnDiam      = 40*mm;
    G4double FITopBotOutDiam      = 43*mm;
    G4double FITopBotFlDiam       = 70*mm;
    G4double FITopBotFlThick      = 15*mm;
    G4double FITopBotMidPos       = 90*mm;

    G4Tubs*  FILeftFl;
    G4Tubs*  FILeftZyl;
    G4Tubs*  FIRightZyl;
    G4Tubs*  FIRightFl;
    G4Tubs*  FIMidZyl;
    G4Tubs*  FIZylTemp;
    G4Tubs*  FITopBotZyl;
    G4Tubs*  FIFlange;
    G4Tubs*  FIFlangeCl;
    G4UnionSolid* FILeftUnion;
    G4UnionSolid* FIRightUnion;
    G4UnionSolid* FIFlUnion;
    G4UnionSolid* FIFlUnionCl;
    G4SubtractionSolid* FITop;

    G4RotationMatrix* RotMatrixY = new G4RotationMatrix(); // Rotate the ScintiBars
    RotMatrixY->rotateY(90*deg);

    G4RotationMatrix* RotMatrixXZ = new G4RotationMatrix();
    RotMatrixXZ->rotateX(90*deg);
    RotMatrixXZ->rotateZ(90*deg);

    G4RotationMatrix* RotMatrixX = new G4RotationMatrix();
    RotMatrixX->rotateX(90*deg);
    
    /*bool useExtFIfield = 1; // TODO make an external flag for this 
    
    if(useExtFIfield) {  
    std::cout << "created HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << FIStart[0] << std::endl;  
        ExtFIField = new hbarExtFIField(); 
        ExtFIField->SetExtFIcenter(FIStart[0])   ;
    }*/

    for (G4int i = 0; i < maxFI; i++)
	{
      if (useFI[i])
      {
          G4String FILName = "FieldIonizerLeft"   + numtostr(i+1);
          G4String FIMName = "FieldIonizerMiddle" + numtostr(i+1);
          G4String FIRName = "FieldIonizerRight"  + numtostr(i+1);
          G4String FITopBotName = "FieldIonizerTopBot"  + numtostr(i+1);
          G4String FIFlClName = "FieldIonizerTopBotClosures" + numtostr(i+1);

          FILeftFl     = new G4Tubs("LeftFlange", FILeftInnDiam*.5, FILeftFlOutDiam*.5, FILeftFlLength*.5, .0, 2.*M_PI); //Left Half of Field Ionizer
          FILeftZyl    = new G4Tubs("LeftZyl", FILeftInnDiam*.5, FILeftOutDiam*.5, FILeftLength*.5, .0, 2.*M_PI);
          FILeftUnion  = new G4UnionSolid("FILeftParts", FILeftFl, FILeftZyl, 0, G4ThreeVector(0, 0, (FILeftLength+FILeftFlLength)*.5));
          G4double FILeftzPos = FIStart[i] + FILeftFlLength*.5; //Calculating the Center-point for the G4PVPlacement
          FILeftLog[i]  = new G4LogicalVolume(FILeftUnion, hbarDetConst::GetMaterial("Sts"), FILName);
          FILeftPhy[i]  = new G4PVPlacement(0, G4ThreeVector(.0,.0, FILeftzPos), FILName, FILeftLog[i], WorldPhy, false, 0);

          G4double FIMidPos  = FIStart[i] + FILeftFlLength + FILeftLength + FIMiddleLength*0.5;   //Mid Part (Connecting Zyl between Left and Right Part)
          FIMidZyl     = new G4Tubs("LeftFlange", FIRightInnerDiam*.5, FILeftOutDiam*.5, FIMiddleLength*.5, .0, 2.*M_PI);
          FIMidZylLog[i]  = new G4LogicalVolume(FIMidZyl, hbarDetConst::GetMaterial("Sts"), FIMName);
          FIMidZylPhy[i]  = new G4PVPlacement(0, G4ThreeVector(.0,.0, FIMidPos), FIMName, FIMidZylLog[i], WorldPhy, false, 0);


          G4double FIRightzPos  = FIStart[i] + FILeftFlLength + FILeftLength + FIRightLength*0.5 + FIMiddleLength;      //Right Half of Field Ionizer
          FIRightZyl   = new G4Tubs("RightZyl", FIRightInnerDiam*.5, FIRightZylOutDiam*.5, FIRightLength*.5, .0, 2.*M_PI);
          FIRightFl    = new G4Tubs("RightFlange", FIRightInnerDiam*.5, FIRightFlOutDiam*.5, FIRightFlLength*.5, .0, 2.*M_PI);
          FIRightUnion = new G4UnionSolid("FIRightParts", FIRightZyl, FIRightFl, 0, G4ThreeVector(0, 0,FIRightLength*0.5));
          FIRightLog[i]  = new G4LogicalVolume(FIRightUnion, hbarDetConst::GetMaterial("Sts"), FIRName);
          FIRightPhy[i]  = new G4PVPlacement(0, G4ThreeVector(.0,.0, FIRightzPos), FIRName, FIRightLog[i], WorldPhy, false, 0);

          FITopBotZyl    = new G4Tubs("ZylCut", FITopBotInnDiam*.5, FITopBotOutDiam*.5, FITopBotDist*.5, .0, 2.*M_PI);   //Top and Bottom Piping of FI
          FIZylTemp  = new G4Tubs("ZylTopBot", .0, FILeftOutDiam*.5, FITopBotOutDiam*2., .0, 2.*M_PI);
          FITop = new G4SubtractionSolid("GVBody",FITopBotZyl,FIZylTemp, RotMatrixY, G4ThreeVector(0, 0, 0));
          G4double FITopBotZPos = FITopBotMidPos + FIStart[i];
          FITopLog[i]  = new G4LogicalVolume(FITop, hbarDetConst::GetMaterial("Sts"), FITopBotName);
          FITopPhy[i]  = new G4PVPlacement(RotMatrixXZ, G4ThreeVector(.0,.0, FITopBotZPos), FITopBotName, FITopLog[i], WorldPhy, false, 0);

          G4double FITopBotFlYPos = FITopBotDist*.5 - FITopBotFlThick*.5;                                                  //Flange at Top and Bottom Piping
          FIFlange    = new G4Tubs("FIFlange", FITopBotOutDiam*.5, FITopBotFlDiam*.5, FITopBotFlThick*.5, .0, 2.*M_PI);
          FIFlUnion   = new G4UnionSolid("FIFlange", FIFlange, FIFlange, 0, G4ThreeVector(0, 0,-FITopBotFlYPos*2.));
          FIFlLog[i]  = new G4LogicalVolume(FIFlUnion, hbarDetConst::GetMaterial("Sts"), FITopBotName);
          FIFlPhy[i]  = new G4PVPlacement(RotMatrixX, G4ThreeVector(.0,FITopBotFlYPos, FITopBotZPos), FITopBotName, FIFlLog[i], WorldPhy, false, 0);

          G4double FITopBotFlClYPos = FITopBotDist*.5 + FITopBotFlThick*.5;
          FIFlangeCl    = new G4Tubs("FIFlangeClosure", 0, FITopBotFlDiam*.5, FITopBotFlThick*.5, .0, 2.*M_PI); //Flange at Top and Bottom Piping
          FIFlUnionCl   = new G4UnionSolid("FIFlangeClosure", FIFlangeCl, FIFlangeCl, 0, G4ThreeVector(0, 0,-FITopBotFlClYPos*2.));
          FIFlClLog[i]  = new G4LogicalVolume(FIFlUnionCl, hbarDetConst::GetMaterial("Sts"), FIFlClName);
          FIFlClPhy[i]  = new G4PVPlacement(RotMatrixX, G4ThreeVector(.0,FITopBotFlClYPos, FITopBotZPos), FIFlClName, FIFlClLog[i], WorldPhy, false, 0);


      }
	}
      //------------------------------------ FIELD IONIZER END ---------------------------------------------------//

}

void hbarBeamlineConst::ConstructConcreteBlock(G4VPhysicalVolume * WorldPhy)
{
  ///Parameter passing is lame.
  G4double WorldSizeXY = static_cast<G4Box*>(WorldPhy->GetLogicalVolume()->GetSolid())->GetXHalfLength()*2;
  G4double WorldSizeZ = static_cast<G4Box*>(WorldPhy->GetLogicalVolume()->GetSolid())->GetXHalfLength()*2;

  G4double blockthick = WorldSizeXY/2.0 - BEAMLINE_HEIGHT*m; // 1.2 m is the height of the beam line
  if (blockthick > 0.0)
	{
	  ConcreteSol = new G4Box("Concrete", WorldSizeXY/2.0, blockthick/2.0, WorldSizeZ/2.0);
	  ConcreteLog = new G4LogicalVolume(ConcreteSol, hbarDetConst::GetMaterial("Concrete"), "Concrete");

	  ConcretePhy = new G4PVPlacement(0, G4ThreeVector(0.0, -(WorldSizeXY/2.0 - blockthick/2.0), 0.0),
									  "Concrete", ConcreteLog, WorldPhy, false, 0);
	}
}


void hbarBeamlineConst::UseBeamPipe(G4int num)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  BPpt = num-1;
	  useBP[BPpt] = true;
	}
  else if (num == 0)
	{
	  AddBeamPipe();
	}
  else
	{
	  char buffer[400];
	  sprintf(buffer, "Beam pipe number %d is out of range", num);
	  throw hbarException(buffer);
	}
}

void hbarBeamlineConst::AddBeamPipe()
{
  if (BPpt < maxBPN-1)
	{
	  BPpt++;
	  useBP[BPpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create beam pipe! There are too many of them.");
	}
}

void hbarBeamlineConst::SetBeamPipeFrontInnerDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN)) {
    BPFrontInnerDiam[num-1] = val;
  }
}

void hbarBeamlineConst::SetBeamPipeRearInnerDiameter(G4int num, G4double val)
{
  if ((num-1 > 0) && (num-1 < maxBPN))
	{
	  BPRearInnerDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetBeamPipeFrontOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  BPFrontOuterDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetBeamPipeRearOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  BPRearOuterDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetBeamPipeStart(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  BPStart[num-1] = val;
	}
}

void hbarBeamlineConst::SetBeamPipeEnd(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  BPEnd[num-1] = val;
	}
}

void hbarBeamlineConst::SetBeamPipeVisible(G4int num, G4bool val)
{
  if ((num-1 >= 0) && (num-1 < maxBPN))
	{
	  visBP[num-1] = val;
	}
}

void hbarBeamlineConst::UseLeadShield(G4int num)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSpt = num-1;
	  useLS[LSpt] = true;
	}
  else if (num == 0)
	{
	  AddLeadShield();
  }
  else
	{
	  char buffer[400];
	  sprintf(buffer, "Lead shield number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarBeamlineConst::AddLeadShield()
{
  if (LSpt < maxLSN-1)
	{
	  LSpt++;
	  useLS[LSpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create lead shield! There are too many of them.");
	}
}

void hbarBeamlineConst::SetLeadShieldFrontInnerDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSFrontInnerDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetLeadShieldRearInnerDiameter(G4int num, G4double val)
{
  if ((num-1 > 0) && (num-1 < maxLSN))
	{
	  LSRearInnerDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetLeadShieldFrontOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSFrontOuterDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetLeadShieldRearOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSRearOuterDiam[num-1] = val;
	}
}

void hbarBeamlineConst::SetLeadShieldStart(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSStart[num-1] = val;
	}
}

void hbarBeamlineConst::SetLeadShieldEnd(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  LSEnd[num-1] = val;
	}
}

void hbarBeamlineConst::SetNumberOfLeadShieldSides(G4int num, G4int val)
{
  if ((num-1 >= 0) && (num-1 < maxLSN))
	{
	  NumOfLSSides[num-1] = val;
	}
}

void hbarBeamlineConst::UseGateValve(G4int num)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVpt = num-1;
	  useGV[GVpt] = true;
	}
  else if (num == 0)
	{
	  AddGateValve();
  }
  else
	{
	  char buffer[400];
	  sprintf(buffer, "Gate Valve number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarBeamlineConst::AddGateValve()
{
  if (GVpt < maxGV-1)
	{
	  GVpt++;
	  useGV[GVpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create Gate Valve! There are too many of them.");
	}
}

void hbarBeamlineConst::SetGateValveStart(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVStart[num-1] = val;
	}
}

void hbarBeamlineConst::SetGateValveEnd(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVEnd[num-1] = val;
	}
}

void hbarBeamlineConst::SetGateValveInnerDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVInnerDim[num-1] = val;
	}
}

void hbarBeamlineConst::SetGateValveOuterDiameter(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVOuterDim[num-1] = val;
	}
}

void hbarBeamlineConst::SetBonnetHight(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVBonnetHeight[num-1] = val;
	}
}

void hbarBeamlineConst::SetTopBoxLength(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVTopBoxLength[num-1] = val;
	}
}

void hbarBeamlineConst::SetTopBoxWidth(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVTopBoxWidth[num-1] = val;
	}
}

void hbarBeamlineConst::SetTopBoxHeight(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxGV))
	{
	  GVTopBoxHeight[num-1] = val;
	}
}

void hbarBeamlineConst::UseFieldIonizer(G4int num)
{
  if ((num-1 >= 0) && (num-1 < maxFI))
	{
	  FIpt = num-1;
	  useFI[FIpt] = true;
	}
  else if (num == 0)
	{
	  AddFieldIonizer();
  }
  else
	{
	  char buffer[400];
	  sprintf(buffer, "Field Ionizer number %d is out of range.", num);
	  throw hbarException(buffer);
	}
}

void hbarBeamlineConst::AddFieldIonizer()
{
  if (FIpt < maxFI-1)
	{
	  FIpt++;
	  useFI[FIpt] = true;
	}
  else
	{
	  throw hbarException("Cannot create Field Ionizer! There are too many of them.");
	}
}

void hbarBeamlineConst::SetFieldIonizerStart(G4int num, G4double val)
{
  if ((num-1 >= 0) && (num-1 < maxFI))
	{
	  FIStart[num-1] = val;
	}
}
