#include "hbarBGOMess.hh"

hbarBGOMess::hbarBGOMess(hbarBGOConst * _hbarBGO)
  :hbarBGO(_hbarBGO)
{

  hbarBGODir = new G4UIdirectory("/setup/BGO/");
  hbarBGODir->SetGuidance("BGO properties control.");


  TargetRinRoutLenCmd = new G4UIcmdWith3VectorAndUnit("/setup/BGO/targetRinRoutLen",this);
  TargetRinRoutLenCmd->SetGuidance("Set dimensions of the detector volume.");
  TargetRinRoutLenCmd->SetParameterName("ind", "oud", "len",false, false);
  TargetRinRoutLenCmd->SetDefaultUnit("mm");
  TargetRinRoutLenCmd->AvailableForStates(G4State_Idle);

  FlangeRinRoutLenCmd = new G4UIcmdWith3VectorAndUnit("/setup/BGO/flangeRinRoutLen",this);
  FlangeRinRoutLenCmd->SetGuidance("Set dimensions of the flange.");
  FlangeRinRoutLenCmd->SetParameterName("ind", "oud", "len",false,false);
  FlangeRinRoutLenCmd->SetDefaultUnit("mm");
  FlangeRinRoutLenCmd->AvailableForStates(G4State_Idle);



  CoverNumberCmd = new G4UIcmdWithAnInteger("/setup/BGO/coverNumber",this);
  CoverNumberCmd->SetGuidance("Switch to cover number X for the following cover commands.");
  CoverNumberCmd->SetParameterName("Count",true,false);
  CoverNumberCmd->SetDefaultValue(0);
  CoverNumberCmd->SetRange("Count>=0");
  CoverNumberCmd->AvailableForStates(G4State_Idle);

  CoverRinRoutLenCmd = new G4UIcmdWith3VectorAndUnit("/setup/BGO/coverRinRoutLen",this);
  CoverRinRoutLenCmd->SetGuidance("Set dimensions of the cover.");
  CoverRinRoutLenCmd->SetParameterName("ind", "oud", "len",false,false);
  CoverRinRoutLenCmd->SetDefaultUnit("mm");
  CoverRinRoutLenCmd->AvailableForStates(G4State_Idle);


  ScintillatorNumberCmd = new G4UIcmdWithAnInteger("/setup/BGO/scintillatorNumber",this);
  ScintillatorNumberCmd->SetGuidance("Switch to scintillator X.");
  ScintillatorNumberCmd->SetParameterName("Count",true,false);
  ScintillatorNumberCmd->SetDefaultValue(0);
  ScintillatorNumberCmd->SetRange("Count>=0");
  ScintillatorNumberCmd->AvailableForStates(G4State_Idle);

  ScintillatorBoxDimensionsXYZ = new G4UIcmdWith3VectorAndUnit("/setup/BGO/scintillatorBoxDimensionsXYZ",this);
  ScintillatorBoxDimensionsXYZ->SetGuidance("Set dimensions of the scintillator box.");
  ScintillatorBoxDimensionsXYZ->SetParameterName("xlen", "ylen", "zlen",false,false);
  ScintillatorBoxDimensionsXYZ->SetDefaultUnit("mm");
  ScintillatorBoxDimensionsXYZ->AvailableForStates(G4State_Idle);


  ScintillatorTrapezoidX1Cmd = new G4UIcmdWithADoubleAndUnit("/setup/BGO/scintillatorTrapezoidX1",this);
  ScintillatorTrapezoidX1Cmd->SetGuidance("Set trapezoid X1 ");
  ScintillatorTrapezoidX1Cmd->SetParameterName("len",false,false);
  ScintillatorTrapezoidX1Cmd->SetDefaultUnit("mm");
  ScintillatorTrapezoidX1Cmd->AvailableForStates(G4State_Idle);

  ScintillatorTrapezoidX2Cmd = new G4UIcmdWithADoubleAndUnit("/setup/BGO/scintillatorTrapezoidX2",this);
  ScintillatorTrapezoidX2Cmd->SetGuidance("Set trapezoid X2 ");
  ScintillatorTrapezoidX2Cmd->SetParameterName("len",false,false);
  ScintillatorTrapezoidX2Cmd->SetDefaultUnit("mm");
  ScintillatorTrapezoidX2Cmd->AvailableForStates(G4State_Idle);

  ScintillatorTrapezoidY1Cmd = new G4UIcmdWithADoubleAndUnit("/setup/BGO/scintillatorTrapezoidY1",this);
  ScintillatorTrapezoidY1Cmd->SetGuidance("Set trapezoid Y1 ");
  ScintillatorTrapezoidY1Cmd->SetParameterName("len",false,false);
  ScintillatorTrapezoidY1Cmd->SetDefaultUnit("mm");
  ScintillatorTrapezoidY1Cmd->AvailableForStates(G4State_Idle);

  ScintillatorTrapezoidY2Cmd = new G4UIcmdWithADoubleAndUnit("/setup/BGO/scintillatorTrapezoidY2",this);
  ScintillatorTrapezoidY2Cmd->SetGuidance("Set trapezoid Y2 ");
  ScintillatorTrapezoidY2Cmd->SetParameterName("len",false,false);
  ScintillatorTrapezoidY2Cmd->SetDefaultUnit("mm");
  ScintillatorTrapezoidY2Cmd->AvailableForStates(G4State_Idle);

  ScintillatorTrapezoidZCmd = new G4UIcmdWithADoubleAndUnit("/setup/BGO/scintillatorTrapezoidZ",this);
  ScintillatorTrapezoidZCmd->SetGuidance("Set trapezoid Z ");
  ScintillatorTrapezoidZCmd->SetParameterName("len",false,false);
  ScintillatorTrapezoidZCmd->SetDefaultUnit("mm");
  ScintillatorTrapezoidZCmd->AvailableForStates(G4State_Idle);


  EnableScintillatorCmd = new G4UIcmdWithAnInteger("/setup/BGO/enableScintillator",this);
  EnableScintillatorCmd->SetGuidance("Swich scintillator on or off");
  EnableScintillatorCmd->SetParameterName("num",true,false);
  EnableScintillatorCmd->SetDefaultValue(0);
  EnableScintillatorCmd->SetRange("num>=0");
  EnableScintillatorCmd->AvailableForStates(G4State_Idle);

  EnableSensitiveScintillatorCmd = new G4UIcmdWithAnInteger("/setup/BGO/enableSensitiveScintillator",this);
  EnableSensitiveScintillatorCmd->SetGuidance("Set NDPLASCIN sensitive");
  EnableSensitiveScintillatorCmd->SetParameterName("num",true,false);
  EnableSensitiveScintillatorCmd->SetDefaultValue(0);
  EnableSensitiveScintillatorCmd->SetRange("num>=0");
  EnableSensitiveScintillatorCmd->AvailableForStates(G4State_Idle);

  SupportBoxNumberCmd = new G4UIcmdWithAnInteger("/setup/BGO/supportBoxNumber",this);
  SupportBoxNumberCmd->SetGuidance("Switch to support box number X");
  SupportBoxNumberCmd->SetParameterName("Count",true,false);
  SupportBoxNumberCmd->SetDefaultValue(0);
  SupportBoxNumberCmd->SetRange("Count>=0");
  SupportBoxNumberCmd->AvailableForStates(G4State_Idle);

  SupportBoxDimensionXYZCmd = new G4UIcmdWith3VectorAndUnit("/setup/BGO/supportBoxDimensionXYZ",this);
  SupportBoxDimensionXYZCmd->SetGuidance("Set dimensions of the support box ");
  SupportBoxDimensionXYZCmd->SetParameterName("xlen", "ylen", "zlen",false,false);
  SupportBoxDimensionXYZCmd->SetDefaultUnit("mm");
  SupportBoxDimensionXYZCmd->AvailableForStates(G4State_Idle);

  UseSupportBoxCmd = new G4UIcmdWithAnInteger("/setup/BGO/useSupportBox",this);
  UseSupportBoxCmd->SetGuidance("Swich support box on or off");
  UseSupportBoxCmd->SetParameterName("useflag",true,false);
  UseSupportBoxCmd->SetDefaultValue(0);
  UseSupportBoxCmd->SetRange("useflag>=0");
  UseSupportBoxCmd->AvailableForStates(G4State_Idle);

  SupportBoxBarDistance = new G4UIcmdWith3VectorAndUnit("/setup/BGO/supportBoxBarDistance",this);
  SupportBoxBarDistance->SetGuidance("Set DBbar of the NDsupBox ");
  SupportBoxBarDistance->SetParameterName("db0", "db1", "db2",false,false);
  SupportBoxBarDistance->SetDefaultUnit("mm");
  SupportBoxBarDistance->AvailableForStates(G4State_Idle);

  HydroTargetInsteadOfNormalCmd = new G4UIcmdWithAnInteger("/setup/BGO/hydrogenTargetInsteadOfNormal",this);
  HydroTargetInsteadOfNormalCmd->SetGuidance("NagataDetectorHydroTarget on or off");
  HydroTargetInsteadOfNormalCmd->SetParameterName("useflag",true,false);
  HydroTargetInsteadOfNormalCmd->SetDefaultValue(0);
  HydroTargetInsteadOfNormalCmd->SetRange("useflag>=0");
  HydroTargetInsteadOfNormalCmd->AvailableForStates(G4State_Idle);

}

hbarBGOMess::~hbarBGOMess()
{
  delete HydroTargetInsteadOfNormalCmd; 
  delete TargetRinRoutLenCmd; 
  delete FlangeRinRoutLenCmd; 

  delete CoverNumberCmd; 
  delete CoverRinRoutLenCmd;

  delete ScintillatorNumberCmd; 
  delete ScintillatorBoxDimensionsXYZ; 

  delete ScintillatorTrapezoidX1Cmd; 
  delete ScintillatorTrapezoidX2Cmd; 
  delete ScintillatorTrapezoidY1Cmd; 
  delete ScintillatorTrapezoidY2Cmd; 
  delete ScintillatorTrapezoidZCmd; 
  delete EnableScintillatorCmd; 
  delete EnableSensitiveScintillatorCmd;

  delete SupportBoxNumberCmd; 
  delete SupportBoxDimensionXYZCmd; 
  delete SupportBoxBarDistance;
  delete UseSupportBoxCmd; 

  delete hbarBGODir;
}


void hbarBGOMess::SetNewValue(G4UIcommand* command, G4String newValue)
{
  if( command == HydroTargetInsteadOfNormalCmd)
	{ 
	  hbarBGO->SetUseHydrogenTargetInsteadOfNormal(HydroTargetInsteadOfNormalCmd->GetNewIntValue(newValue));
	}

  if (command == TargetRinRoutLenCmd) 
	{ 
	  hbarBGO->SetTargetRinRoutLen(TargetRinRoutLenCmd->GetNew3VectorValue(newValue));
	}
  if (command == FlangeRinRoutLenCmd) 
	{ 
	  hbarBGO->SetFlangeRinRoutLen(FlangeRinRoutLenCmd->GetNew3VectorValue(newValue));
	}
  if( command == CoverNumberCmd )
	{ 
	  hbarBGO->UseCoverNumber(CoverNumberCmd->GetNewIntValue(newValue));
	}
  if (command == CoverRinRoutLenCmd) 
	{ 
	  hbarBGO->SetCoverRinRoutLen(CoverRinRoutLenCmd->GetNew3VectorValue(newValue));
	}
  
  if( command == ScintillatorNumberCmd )
	{ 
	  hbarBGO->UseScintillatorNumber(ScintillatorNumberCmd->GetNewIntValue(newValue));
	}
  if (command == ScintillatorBoxDimensionsXYZ) 
	{ 
	  hbarBGO->SetScintillatorBoxDimensionsXYZ(ScintillatorBoxDimensionsXYZ->GetNew3VectorValue(newValue));
	}

  if (command == ScintillatorTrapezoidX1Cmd) 
	{ 
	  hbarBGO->SetScintillatorTrapezoidX1(ScintillatorTrapezoidX1Cmd->GetNewDoubleValue(newValue));
	}

  if (command == ScintillatorTrapezoidX2Cmd) 
	{ 
	  hbarBGO->SetScintillatorTrapezoidX2(ScintillatorTrapezoidX2Cmd->GetNewDoubleValue(newValue));
	}
  if (command == ScintillatorTrapezoidY1Cmd) 
	{ 
	  hbarBGO->SetScintillatorTrapezoidY1(ScintillatorTrapezoidY1Cmd->GetNewDoubleValue(newValue));
	}
  if (command == ScintillatorTrapezoidY2Cmd) 
	{ 
	  hbarBGO->SetScintillatorTrapezoidY2(ScintillatorTrapezoidY2Cmd->GetNewDoubleValue(newValue));
	}
  if (command == ScintillatorTrapezoidZCmd) 
	{ 
	  hbarBGO->SetScintillatorTrapezoidZ(ScintillatorTrapezoidZCmd->GetNewDoubleValue(newValue));
	}

  if( command == EnableScintillatorCmd)
	{ 
	  hbarBGO->EnableScintillator(EnableScintillatorCmd->GetNewIntValue(newValue));
	}
  if( command == EnableSensitiveScintillatorCmd)
	{ 
	  hbarBGO->EnableSensitiveScintillator(EnableSensitiveScintillatorCmd->GetNewIntValue(newValue));
	}

  if( command == UseSupportBoxCmd)
	{ 
	  hbarBGO->EnableSupportBox(UseSupportBoxCmd->GetNewIntValue(newValue));
	}
  if( command == SupportBoxNumberCmd )
	{ 
	  hbarBGO->UseSupportBoxNumber(SupportBoxNumberCmd->GetNewIntValue(newValue));
	}
  if (command == SupportBoxDimensionXYZCmd) 
	{ 
	  hbarBGO->SetSupportBoxDimensionXYZ(SupportBoxDimensionXYZCmd->GetNew3VectorValue(newValue));
	}
  if (command == SupportBoxBarDistance) 
	{ 
	  hbarBGO->SetSupportBoxDistanceBetweenBars(SupportBoxBarDistance->GetNew3VectorValue(newValue));
	}
}
