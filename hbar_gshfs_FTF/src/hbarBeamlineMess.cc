#include "hbarBeamlineMess.hh"

hbarBeamlineMess::hbarBeamlineMess(hbarBeamlineConst * _hbarBeamline)
  :hbarBeamline(_hbarBeamline)
{
  UseConcreBlockCmd = new G4UIcmdWithAnInteger("/setup/setUseConcreBlock",this);
  UseConcreBlockCmd->SetGuidance("Swich Concrete Block on or off");
  UseConcreBlockCmd->SetParameterName("useflag",true,false);
  UseConcreBlockCmd->SetDefaultValue(0);
  UseConcreBlockCmd->SetRange("useflag>=0");
  UseConcreBlockCmd->AvailableForStates(G4State_Idle);

  BPUseCmd = new G4UIcmdWithAnInteger("/setup/setUseBeamPipe",this);
  BPUseCmd->SetGuidance("Switch to beam pipe number X");
  BPUseCmd->SetParameterName("Count",true,false);
  BPUseCmd->SetDefaultValue(0);
  BPUseCmd->SetRange("Count>=0");
  BPUseCmd->AvailableForStates(G4State_Idle);

  BPStartCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeStart",this);
  BPStartCmd->SetGuidance("Set start (upstream) position of the current beam pipe (m)");
  BPStartCmd->SetParameterName("Size",false,false);
  BPStartCmd->SetDefaultUnit("m");
  BPStartCmd->AvailableForStates(G4State_Idle);

  BPEndCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeEnd",this);
  BPEndCmd->SetGuidance("Set end (downstream) position of the current beam pipe (m)");
  BPEndCmd->SetParameterName("Size",false,false);
  BPEndCmd->SetDefaultUnit("m");
  BPEndCmd->AvailableForStates(G4State_Idle);

  BPInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeInnerDiameter",this);
  BPInnerDiamCmd->SetGuidance("Set inner diameter of the current beam pipe (m)");
  BPInnerDiamCmd->SetParameterName("Size",false,false);
  BPInnerDiamCmd->SetDefaultUnit("m");
  BPInnerDiamCmd->SetRange("Size>=0.");
  BPInnerDiamCmd->AvailableForStates(G4State_Idle);

  BPFrontInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeFrontInnerDiameter",this);
  BPFrontInnerDiamCmd->SetGuidance("Set front inner diameter of the current beam pipe (m)");
  BPFrontInnerDiamCmd->SetParameterName("Size",false,false);
  BPFrontInnerDiamCmd->SetDefaultUnit("m");
  BPFrontInnerDiamCmd->SetRange("Size>=0.");
  BPFrontInnerDiamCmd->AvailableForStates(G4State_Idle);

  BPRearInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeRearInnerDiameter",this);
  BPRearInnerDiamCmd->SetGuidance("Set rear inner diameter of the current beam pipe (m)");
  BPRearInnerDiamCmd->SetParameterName("Size",false,false);
  BPRearInnerDiamCmd->SetDefaultUnit("m");
  BPRearInnerDiamCmd->SetRange("Size>=0.");
  BPRearInnerDiamCmd->AvailableForStates(G4State_Idle);

  BPOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeOuterDiameter",this);
  BPOuterDiamCmd->SetGuidance("Set outer diameter of the current beam pipe (m)");
  BPOuterDiamCmd->SetParameterName("Size",false,false);
  BPOuterDiamCmd->SetDefaultUnit("m");
  BPOuterDiamCmd->SetRange("Size>=0.");
  BPOuterDiamCmd->AvailableForStates(G4State_Idle);

  BPFrontOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeFrontOuterDiameter",this);
  BPFrontOuterDiamCmd->SetGuidance("Set front outer diameter of the current beam pipe (m)");
  BPFrontOuterDiamCmd->SetParameterName("Size",false,false);
  BPFrontOuterDiamCmd->SetDefaultUnit("m");
  BPFrontOuterDiamCmd->SetRange("Size>=0.");
  BPFrontOuterDiamCmd->AvailableForStates(G4State_Idle);

  BPRearOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setBeamPipeRearOuterDiameter",this);
  BPRearOuterDiamCmd->SetGuidance("Set outer diameter of the current beam pipe (m)");
  BPRearOuterDiamCmd->SetParameterName("Size",false,false);
  BPRearOuterDiamCmd->SetDefaultUnit("m");
  BPRearOuterDiamCmd->SetRange("Size>=0.");
  BPRearOuterDiamCmd->AvailableForStates(G4State_Idle);

  BPVisCmd = new G4UIcmdWithABool("/setup/setBeamPipeVisible",this);
  BPVisCmd->SetGuidance("Whether the current beam pipe is visible or not");
  BPVisCmd->SetDefaultValue(true);
  BPVisCmd->AvailableForStates(G4State_Idle);


  LSUseCmd = new G4UIcmdWithAnInteger("/setup/setUseLeadShield",this);
  LSUseCmd->SetGuidance("Switch to lead shield number X");
  LSUseCmd->SetParameterName("Count",true,false);
  LSUseCmd->SetDefaultValue(0);
  LSUseCmd->SetRange("Count>=0");
  LSUseCmd->AvailableForStates(G4State_Idle);

  LSStartCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldStart",this);
  LSStartCmd->SetGuidance("Set start (upstream) position of the current lead shield (m)");
  LSStartCmd->SetParameterName("Size",false,false);
  LSStartCmd->SetDefaultUnit("m");
  LSStartCmd->AvailableForStates(G4State_Idle);

  LSEndCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldEnd",this);
  LSEndCmd->SetGuidance("Set end (downstream) position of the current lead shield (m)");
  LSEndCmd->SetParameterName("Size",false,false);
  LSEndCmd->SetDefaultUnit("m");
  LSEndCmd->AvailableForStates(G4State_Idle);

  LSInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldInnerDiameter",this);
  LSInnerDiamCmd->SetGuidance("Set inner diameter of the current lead shield (m)");
  LSInnerDiamCmd->SetParameterName("Size",false,false);
  LSInnerDiamCmd->SetDefaultUnit("m");
  LSInnerDiamCmd->SetRange("Size>=0.");
  LSInnerDiamCmd->AvailableForStates(G4State_Idle);

  LSFrontInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldFrontInnerDiameter",this);
  LSFrontInnerDiamCmd->SetGuidance("Set front inner diameter of the current lead shield (m)");
  LSFrontInnerDiamCmd->SetParameterName("Size",false,false);
  LSFrontInnerDiamCmd->SetDefaultUnit("m");
  LSFrontInnerDiamCmd->SetRange("Size>=0.");
  LSFrontInnerDiamCmd->AvailableForStates(G4State_Idle);

  LSRearInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldRearInnerDiameter",this);
  LSRearInnerDiamCmd->SetGuidance("Set rear inner diameter of the current lead shield (m)");
  LSRearInnerDiamCmd->SetParameterName("Size",false,false);
  LSRearInnerDiamCmd->SetDefaultUnit("m");
  LSRearInnerDiamCmd->SetRange("Size>=0.");
  LSRearInnerDiamCmd->AvailableForStates(G4State_Idle);

  LSOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldOuterDiameter",this);
  LSOuterDiamCmd->SetGuidance("Set outer diameter of the current lead shield (m)");
  LSOuterDiamCmd->SetParameterName("Size",false,false);
  LSOuterDiamCmd->SetDefaultUnit("m");
  LSOuterDiamCmd->SetRange("Size>=0.");
  LSOuterDiamCmd->AvailableForStates(G4State_Idle);

  LSFrontOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldFrontOuterDiameter",this);
  LSFrontOuterDiamCmd->SetGuidance("Set front outer diameter of the current lead shield (m)");
  LSFrontOuterDiamCmd->SetParameterName("Size",false,false);
  LSFrontOuterDiamCmd->SetDefaultUnit("m");
  LSFrontOuterDiamCmd->SetRange("Size>=0.");
  LSFrontOuterDiamCmd->AvailableForStates(G4State_Idle);

  LSRearOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setLeadShieldRearOuterDiameter",this);
  LSRearOuterDiamCmd->SetGuidance("Set outer diameter of the current lead shield (m)");
  LSRearOuterDiamCmd->SetParameterName("Size",false,false);
  LSRearOuterDiamCmd->SetDefaultUnit("m");
  LSRearOuterDiamCmd->SetRange("Size>=0.");
  LSRearOuterDiamCmd->AvailableForStates(G4State_Idle);

  LSSidesNumCmd = new G4UIcmdWithAnInteger("/setup/setNumberOfLeadShieldSides",this);
  LSSidesNumCmd->SetGuidance("Set number of sides (circle, rectangle, hexagon etc.) of the current lead shield");
  LSSidesNumCmd->SetParameterName("Count",true,false);
  LSSidesNumCmd->SetDefaultValue(0);
  LSSidesNumCmd->SetRange("Count>=0");
  LSSidesNumCmd->AvailableForStates(G4State_Idle);

  CF100CrossUseCmd = new G4UIcmdWithAnInteger("/setup/setUseCF100Cross",this);
  CF100CrossUseCmd->SetGuidance("Whether the current CF100 cross is in use");
  CF100CrossUseCmd->SetDefaultValue(0);
  CF100CrossUseCmd->AvailableForStates(G4State_Idle);

  CF100CrossPlaceCmd = new G4UIcmdWith3VectorAndUnit("/setup/setCF100CrossCenter",this);
  CF100CrossPlaceCmd->SetGuidance("Set position of the center of the current CF100 vacuum cross (m)");
  CF100CrossPlaceCmd->SetParameterName("Px", "Py", "Pz",false,false);
  CF100CrossPlaceCmd->SetDefaultUnit("m");
  CF100CrossPlaceCmd->AvailableForStates(G4State_Idle);

  CF100CrossRotCmd = new G4UIcmdWith3VectorAndUnit("/setup/setCF100CrossRotation",this);
  CF100CrossRotCmd->SetGuidance("Set rotation of the current CF100 vacuum cross (euler angle)");
  CF100CrossRotCmd->SetParameterName("a", "b", "c",false,false);
  CF100CrossRotCmd->SetDefaultUnit("rad");
  CF100CrossRotCmd->AvailableForStates(G4State_Idle);

  GVUseCmd = new G4UIcmdWithAnInteger("/setup/setUseGateValve",this);
  GVUseCmd->SetGuidance("Switch Gate Valve X");
  GVUseCmd->SetParameterName("Count",true,false);
  GVUseCmd->SetDefaultValue(0);
  GVUseCmd->SetRange("Count>=0");
  GVUseCmd->AvailableForStates(G4State_Idle);

  GVStartCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveStart",this);
  GVStartCmd->SetGuidance("Set start (upstream) position of the current gate valve (m)");
  GVStartCmd->SetParameterName("Size",false,false);
  GVStartCmd->SetDefaultUnit("m");
  GVStartCmd->AvailableForStates(G4State_Idle);

  GVEndCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveEnd",this);
  GVEndCmd->SetGuidance("Set end (downstream) position of the current gate valve (m)");
  GVEndCmd->SetParameterName("Size",false,false);
  GVEndCmd->SetDefaultUnit("m");
  GVEndCmd->AvailableForStates(G4State_Idle);

  GVOuterDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveOuterDiam",this);
  GVOuterDiamCmd->SetGuidance("Set outer diameter of the current gate valve (m)");
  GVOuterDiamCmd->SetParameterName("Size",false,false);
  GVOuterDiamCmd->SetDefaultUnit("m");
  GVOuterDiamCmd->AvailableForStates(G4State_Idle);

  GVInnerDiamCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveInnerDiam",this);
  GVInnerDiamCmd->SetGuidance("Set inner diameter position of the current gate valve (m)");
  GVInnerDiamCmd->SetParameterName("Size",false,false);
  GVInnerDiamCmd->SetDefaultUnit("m");
  GVInnerDiamCmd->AvailableForStates(G4State_Idle);

  GVBonnetHightCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveBonnetHight",this);
  GVBonnetHightCmd->SetGuidance("Set the bonnet hight of current gate valve (m)");
  GVBonnetHightCmd->SetParameterName("Size",false,false);
  GVBonnetHightCmd->SetDefaultUnit("m");
  GVBonnetHightCmd->AvailableForStates(G4State_Idle);

  GVTopBoxLengthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveTopBoxLength",this);
  GVTopBoxLengthCmd->SetGuidance("Set the TopBox Length of current gate valve (m)");
  GVTopBoxLengthCmd->SetParameterName("Size",false,false);
  GVTopBoxLengthCmd->SetDefaultUnit("m");
  GVTopBoxLengthCmd->AvailableForStates(G4State_Idle);

  GVTopBoxWidthCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveTopBoxWidth",this);
  GVTopBoxWidthCmd->SetGuidance("Set the TopBox Width of current gate valve (m)");
  GVTopBoxWidthCmd->SetParameterName("Size",false,false);
  GVTopBoxWidthCmd->SetDefaultUnit("m");
  GVTopBoxWidthCmd->AvailableForStates(G4State_Idle);

  GVTopBoxHeightCmd = new G4UIcmdWithADoubleAndUnit("/setup/setGateValveTopBoxHeight",this);
  GVTopBoxHeightCmd->SetGuidance("Set the TopBox Height of current gate valve (m)");
  GVTopBoxHeightCmd->SetParameterName("Size",false,false);
  GVTopBoxHeightCmd->SetDefaultUnit("m");
  GVTopBoxHeightCmd->AvailableForStates(G4State_Idle);

  CFExtUseCmd = new G4UIcmdWithABool("/setup/setUseCrossFlangeExternals",this);
  CFExtUseCmd->SetGuidance("Whether the externals part on Cross Flange for 2014 Setup is used");
  CFExtUseCmd->SetDefaultValue(true);
  CFExtUseCmd->AvailableForStates(G4State_Idle);

  FIUseCmd = new G4UIcmdWithAnInteger("/setup/setUseFieldIonizer",this);
  FIUseCmd->SetGuidance("Switch to field ionizer number X");
  FIUseCmd->SetParameterName("Count",true,false);
  FIUseCmd->SetDefaultValue(0);
  FIUseCmd->SetRange("Count>=0");
  FIUseCmd->AvailableForStates(G4State_Idle);

  FIStartCmd = new G4UIcmdWithADoubleAndUnit("/setup/setFieldIonizerStart",this);
  FIStartCmd->SetGuidance("Set start (upstream) position of the current beam pipe (m)");
  FIStartCmd->SetParameterName("Size",false,false);
  FIStartCmd->SetDefaultUnit("m");
  FIStartCmd->AvailableForStates(G4State_Idle);

}


hbarBeamlineMess::~hbarBeamlineMess()
{
  delete UseConcreBlockCmd;
  delete BPUseCmd;
  delete BPStartCmd;
  delete BPEndCmd;
  delete BPInnerDiamCmd;
  delete BPFrontInnerDiamCmd;
  delete BPRearInnerDiamCmd;
  delete BPOuterDiamCmd;
  delete BPFrontOuterDiamCmd;
  delete BPRearOuterDiamCmd;
  delete BPVisCmd;
  delete CF100CrossUseCmd;
  delete CF100CrossPlaceCmd;
  delete CF100CrossRotCmd;
  delete LSUseCmd;
  delete LSStartCmd;
  delete LSEndCmd;
  delete LSInnerDiamCmd;
  delete LSFrontInnerDiamCmd;
  delete LSRearInnerDiamCmd;
  delete LSOuterDiamCmd;
  delete LSFrontOuterDiamCmd;
  delete LSRearOuterDiamCmd;
  delete LSSidesNumCmd;
  delete GVUseCmd;
  delete GVStartCmd;
  delete GVEndCmd;
  delete GVOuterDiamCmd;
  delete GVInnerDiamCmd;
  delete GVBonnetHightCmd;
  delete GVTopBoxLengthCmd;
  delete GVTopBoxWidthCmd;
  delete GVTopBoxHeightCmd;
  delete CFExtUseCmd;
  delete FIUseCmd;
  delete FIStartCmd;
}


void hbarBeamlineMess::SetNewValue(G4UIcommand* command, G4String newValue)
{

  if( command == UseConcreBlockCmd)
	{
	  hbarBeamline->SetUseConcreteBlock(UseConcreBlockCmd->GetNewIntValue(newValue));
	}

  if( command == BPUseCmd )
	{
	  hbarBeamline->UseBeamPipe(BPUseCmd->GetNewIntValue(newValue));
	}

  if( command == BPStartCmd )
	{
	  hbarBeamline->SetBeamPipeStart(BPStartCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPEndCmd )
	{
	  hbarBeamline->SetBeamPipeEnd(BPEndCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPInnerDiamCmd )
	{
	  hbarBeamline->SetBeamPipeInnerDiameter(BPInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPFrontInnerDiamCmd )
	{
	  hbarBeamline->SetBeamPipeFrontInnerDiameter(BPFrontInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPRearInnerDiamCmd )
	{
	  hbarBeamline->SetBeamPipeRearInnerDiameter(BPRearInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPOuterDiamCmd )
	{
	  hbarBeamline->SetBeamPipeOuterDiameter(BPOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPFrontOuterDiamCmd )
	{
	  hbarBeamline->SetBeamPipeFrontOuterDiameter(BPFrontOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPRearOuterDiamCmd )
	{
	  hbarBeamline->SetBeamPipeRearOuterDiameter(BPRearOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == BPVisCmd )
	{
	  hbarBeamline->SetBeamPipeVisible(BPVisCmd->GetNewBoolValue(newValue));
	}

  if( command == CF100CrossUseCmd )
	{
	  hbarBeamline->SetUseCF100Cross(CF100CrossUseCmd->GetNewIntValue(newValue));
	}
  if( command == CF100CrossPlaceCmd )
	{
	  hbarBeamline->SetCF100CrossPlacement(CF100CrossPlaceCmd->GetNew3VectorValue(newValue));
	}
  if( command == CF100CrossRotCmd )
	{
	  hbarBeamline->SetCF100CrossRotation(CF100CrossPlaceCmd->GetNew3VectorValue(newValue));
	}

  if( command == LSUseCmd )
	{
	  hbarBeamline->UseLeadShield(LSUseCmd->GetNewIntValue(newValue));
	}

  if( command == LSStartCmd )
	{
	  hbarBeamline->SetLeadShieldStart(LSStartCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSEndCmd )
	{
	  hbarBeamline->SetLeadShieldEnd(LSEndCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSInnerDiamCmd )
	{
	  hbarBeamline->SetLeadShieldInnerDiameter(LSInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSFrontInnerDiamCmd )
	{
	  hbarBeamline->SetLeadShieldFrontInnerDiameter(LSFrontInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSRearInnerDiamCmd )
	{
	  hbarBeamline->SetLeadShieldRearInnerDiameter(LSRearInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSOuterDiamCmd )
	{
	  hbarBeamline->SetLeadShieldOuterDiameter(LSOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSFrontOuterDiamCmd )
	{
	  hbarBeamline->SetLeadShieldFrontOuterDiameter(LSFrontOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSRearOuterDiamCmd )
	{
	  hbarBeamline->SetLeadShieldRearOuterDiameter(LSRearOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == LSSidesNumCmd )
	{
	  hbarBeamline->SetNumberOfLeadShieldSides(LSSidesNumCmd->GetNewIntValue(newValue));
	}

  if( command == GVUseCmd )
	{
	  hbarBeamline->UseGateValve(GVUseCmd->GetNewIntValue(newValue));
	}

  if( command == GVStartCmd )
	{
	  hbarBeamline->SetGateValveStart(GVStartCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVEndCmd )
	{
	  hbarBeamline->SetGateValveEnd(GVEndCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVOuterDiamCmd )
	{
	  hbarBeamline->SetGateValveOuterDiameter(GVOuterDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVInnerDiamCmd )
	{
	  hbarBeamline->SetGateValveInnerDiameter(GVInnerDiamCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVBonnetHightCmd )
	{
	  hbarBeamline->SetBonnetHight(GVBonnetHightCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVTopBoxLengthCmd )
	{
	  hbarBeamline->SetTopBoxLength(GVTopBoxLengthCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVTopBoxWidthCmd )
	{
	  hbarBeamline->SetTopBoxWidth(GVTopBoxWidthCmd->GetNewDoubleValue(newValue));
	}

  if( command == GVTopBoxHeightCmd )
	{
	  hbarBeamline->SetTopBoxHeight(GVTopBoxHeightCmd->GetNewDoubleValue(newValue));
	}

  if( command == CFExtUseCmd )
	{
	  hbarBeamline->SetCrossFlangeExtUse(CFExtUseCmd->GetNewBoolValue(newValue));
	}

  if( command == FIUseCmd )
	{
	  hbarBeamline->UseFieldIonizer(FIUseCmd->GetNewIntValue(newValue));
	}

  if( command == FIStartCmd )
	{
	  hbarBeamline->SetFieldIonizerStart(FIStartCmd->GetNewDoubleValue(newValue));
	}
}
