#include "hbarTrackDictionaryEntry.hh"

hbarTrackDictionaryEntry::hbarTrackDictionaryEntry(G4int _N, G4int _F, G4int _M)
  :N(_N), F(_F), M(_M)
{
  
}

hbarTrackDictionaryEntry::~hbarTrackDictionaryEntry()
{
  
}

bool hbarTrackDictionaryEntry::operator == (const hbarTrackDictionaryEntry &b) const
{
  return b.N == N && b.F == F && b.M == M;
}

bool hbarTrackDictionaryEntry::operator != (const hbarTrackDictionaryEntry &b) const
{
  return !(b.N == N && b.F == F && b.M == M);
}
