#include "hbarDetectorSD.hh"
#include "hbarDetectorHit.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "numtostr.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "TObject.h"
#include "hbarDetConst.hh"

hbarDetectorSD::hbarDetectorSD(G4String name)
:G4VSensitiveDetector(name)
{
  G4String HCname;
  collectionName.insert(HCname="scintiCollection");
  scintiCollection = NULL;
}

hbarDetectorSD::~hbarDetectorSD()
{
}

void hbarDetectorSD::Initialize(G4HCofThisEvent *HCE)
{
  static int HCID = -1;
  scintiCollection = new hbarDetectorHitsCollection
                      (SensitiveDetectorName,collectionName[0]);

  if (HCID < 0) {
     HCID = GetCollectionID(0);
  }
  HCE->AddHitsCollection(HCID, scintiCollection);
}

G4bool hbarDetectorSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{

  G4String particlename = aStep->GetTrack()->GetDefinition()->GetParticleName();
  
  
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.0) return false;
	// || particlename=="opticalphoton") return false;
	
  G4int pdg = aStep->GetTrack()->GetDefinition()->GetPDGEncoding();
  //std::cout << aStep->GetTrack()->GetDefinition()->GetParticleName() << " " << pdg << std::endl;

  hbarDetectorHit* newHit = new hbarDetectorHit();
  newHit->SetEnergyDeposit( edep );
  newHit->SetTime(((aStep->GetPreStepPoint()->GetGlobalTime() + aStep->GetPostStepPoint()->GetGlobalTime())/2.0)/ns);
  newHit->SetKineticEnergy(aStep->GetPreStepPoint()->GetKineticEnergy() );
  newHit->SetPosition( aStep->GetPreStepPoint()->GetPosition() );
  newHit->SetMomentumdir(aStep->GetPreStepPoint()->GetMomentumDirection());
  newHit->SetParticleName( particlename );  
  newHit->SetPDGcode(pdg);
  
  newHit->Settrack_id(aStep->GetTrack()->GetTrackID());
  newHit->Setparent_id(aStep->GetTrack()->GetParentID());

  //std::cout << aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() << std::endl;

  // Modified by Clemens
  if ((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("Veto")){
    G4TouchableHistory* theTouchable = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
    G4int copyNo = theTouchable->GetVolume()->GetCopyNo();
    newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()
                + "-" +  numtostr(copyNo+1));}

  // Modified by Stefan, if octagonal Hodoscope: no copy number needed
  else if ((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("Hodoscope") && hbarDetConst::GetUseHbar2014Det()){
    newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() + "-" ); }

  else if ((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("Hodoscope") && !hbarDetConst::GetUseHbar2014Det()){
    G4TouchableHistory* theTouchable = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
    G4int copyNo = theTouchable->GetVolume()->GetCopyNo();
    newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()
			     + "-" +  numtostr(copyNo+1) );}

  else if ((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("HbarDet")){
    G4TouchableHistory* theTouchable = (G4TouchableHistory*)(aStep->GetPreStepPoint()->GetTouchable());
    G4int xcopy = theTouchable->GetVolume()->GetCopyNo();
    G4int ycopy = theTouchable->GetVolume(1)->GetCopyNo();
    newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() + "-" +  numtostr(ycopy+1) + "-" + numtostr(xcopy+1));
  }
  else if((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("BGO_log")) { // added by Berni
		//if(aStep->GetTrack()->GetDefinition()->GetParticleName() != "opticalphoton")
		newHit->SetDetectorName("BGO_log");	 
  }
  else if((aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()).contains("scinti_bar")) {
		//std::cout << "************* " << aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() << std::endl;
        newHit->SetDetectorName(aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName());
        //G4VisAttributes* orangecolor = new G4VisAttributes(G4Colour(1.0,0.56,0.0));
        //aStep->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->SetVisAttributes(orangecolor);
  }
  else{
    newHit->SetDetectorName( aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() ); }

  scintiCollection->insert( newHit );

  return true;
}

void hbarDetectorSD::EndOfEvent(G4HCofThisEvent*)
{
}

void hbarDetectorSD::clear()
{
}

void hbarDetectorSD::DrawAll()
{
}

void hbarDetectorSD::PrintAll()
{
}
