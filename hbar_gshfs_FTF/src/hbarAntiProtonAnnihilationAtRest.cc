#include "hbarAntiProtonAnnihilationAtRest.hh"
#include "hbarGeneralPhaseSpaceDecay.hh"
#include "hbarAntiProtonMess.hh"

//#include "G4HEVector.hh" commented out because of compile error by SV, discussed with Clemens Sauerzopf
#include "G4DynamicParticle.hh"
#include "G4DecayProducts.hh"
#include "G4ParticleTypes.hh"
#include "G4Poisson.hh"
#include "Randomize.hh" 
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "TMath.h"

  G4bool hbarAntiProtonAnnihilationAtRest::ApplyPionAbsorption = true;
  G4String hbarAntiProtonAnnihilationAtRest::DebugFileName = "";

// constructor

hbarAntiProtonAnnihilationAtRest::hbarAntiProtonAnnihilationAtRest(const G4String& processName)
  : G4VRestProcess (processName, fHadronic),       // initialization
  globalTime(0.0), targetZ(0.0), targetA(0.0), targetN(0.0),
  secondaryProducts(NULL),
/***
  massPionMinus(G4PionMinus::PionMinus()->GetPDGMass()/GeV),
  massProton(G4Proton::Proton()->GetPDGMass()/GeV),
  massPionZero(G4PionZero::PionZero()->GetPDGMass()/GeV),
  massAntiProton(G4AntiProton::AntiProton()->GetPDGMass()/GeV),
  massPionPlus(G4PionPlus::PionPlus()->GetPDGMass()/GeV),
  massGamma(G4Gamma::Gamma()->GetPDGMass()/GeV),
***/
  pdefAntiProton(G4AntiProton::AntiProton()),
  pdefNeutron(G4Neutron::Neutron())
/*****
  pdefGamma(G4Gamma::Gamma())
  pdefPionPlus(G4PionPlus::PionPlus()),
  pdefPionZero(G4PionZero::PionZero()),
  pdefPionMinus(G4PionMinus::PionMinus()),
  pdefProton(G4Proton::Proton()),
  pdefDeuteron(G4Deuteron::Deuteron()),
  pdefTriton(G4Triton::Triton()),
  pdefAlpha(G4Alpha::Alpha())
******/
{
  // Hadron at Rest
  SetProcessSubType(151);

  if (verboseLevel>0) {
    G4cout << GetProcessName() << " is created "<< G4endl;
  }

/*******
  G4ParticleDefinition *piplus  = G4PionPlus::PionPlusDefinition();
  G4ParticleDefinition *piminus = G4PionMinus::PionMinusDefinition();
  G4ParticleDefinition *pinull  = G4PionZero::PionZeroDefinition();
  G4ParticleDefinition *kplus   = G4KaonPlus::KaonPlusDefinition();
  G4ParticleDefinition *kminus  = G4KaonMinus::KaonMinusDefinition();
  G4ParticleDefinition *knull   = G4KaonZero::KaonZeroDefinition();

  // pionic channels [NIM.A.496.102(2003)]
  channels.push_back(new DecayChannel(0.069, pinull, pinull));
  channels.push_back(new DecayChannel(4.1,   pinull, pinull, pinull));
  channels.push_back(new DecayChannel(0.307, piplus, piminus));
  channels.push_back(new DecayChannel(5.82,  piplus, piminus, pinull));
  channels.push_back(new DecayChannel(9.3,   piplus, piminus, pinull, pinull));
  channels.push_back(new DecayChannel(23.3,  piplus, piminus, pinull, pinull, pinull));
  channels.push_back(new DecayChannel(2.8,   piplus, piminus, pinull, pinull, pinull, pinull));
  channels.push_back(new DecayChannel(6.9,   piplus, piplus, piminus, piminus));
  channels.push_back(new DecayChannel(19.6,  piplus, piplus, piminus, piminus, pinull));
  channels.push_back(new DecayChannel(16.6,  piplus, piplus, piminus, piminus, pinull, pinull));
  channels.push_back(new DecayChannel(4.2,   piplus, piplus, piminus, piminus, pinull, pinull, pinull));
  channels.push_back(new DecayChannel(2.1,   piplus, piplus, piplus, piminus, piminus, piminus));
  channels.push_back(new DecayChannel(1.85,  piplus, piplus, piplus, piminus, piminus, piminus, pinull));
  channels.push_back(new DecayChannel(0.3,   piplus, piplus, piplus, piminus, piminus, piminus, pinull, pinull));

  // kaonic channels [NIM.A.496.102(2003)]
  channels.push_back(new DecayChannel(0.237, kplus, kminus, knull));
  channels.push_back(new DecayChannel(0.237, kplus, kminus, pinull));
  channels.push_back(new DecayChannel(0.23 , kplus, knull,  piminus));
  channels.push_back(new DecayChannel(0.23 , kminus,knull,  piplus));
  channels.push_back(new DecayChannel(0.23 , kplus, knull,  piminus, pinull));
  channels.push_back(new DecayChannel(0.23 , kminus,knull,  piplus, pinull));
  channels.push_back(new DecayChannel(0.099, kplus, kminus));

  sumOfProbabilities = 0;
  for(unsigned int i=0; i<channels.size(); ++i)
  {
      sumOfProbabilities += channels[i]->GetProbability();
  }
******/

  // pionic pbar-p channels [NIM.A.496.102(2003)]
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.069, 2, "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 4.1,   3, "pi0", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.307, 2, "pi+", "pi-"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 5.82,  3, "pi+", "pi-", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 9.3,   4, "pi+", "pi-", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 23.3,  5, "pi+", "pi-", "pi0", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 2.8,   6, "pi+", "pi-", "pi0", "pi0", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 6.9,   4, "pi+", "pi+", "pi-", "pi-"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 19.6,  5, "pi+", "pi+", "pi-", "pi-", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 16.6,  6, "pi+", "pi+", "pi-", "pi-", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 4.2,   7, "pi+", "pi+", "pi-", "pi-", "pi0", "pi0", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 2.1,   6, "pi+", "pi+", "pi+", "pi-", "pi-", "pi-"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 1.85,  7, "pi+", "pi+", "pi+", "pi-", "pi-", "pi-", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.3,   8, "pi+", "pi+", "pi+", "pi-", "pi-", "pi-", "pi0", "pi0"));


  // kaonic pbar-pchannels [NIM.A.496.102(2003)]
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.237, 3, "kaon+", "kaon-", "kaon0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.237, 3, "kaon+", "kaon-", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.23 , 3, "kaon+", "kaon0", "pi-"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.23 , 3, "kaon-", "kaon0", "pi+"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.235 , 4, "kaon+", "kaon0", "pi-", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.235 , 4, "kaon-", "kaon0", "pi+", "pi0"));
  channelsProton.push_back(new hbarGeneralPhaseSpaceDecay(2.0*pdefAntiProton->GetPDGMass(), 0.099, 2, "kaon+", "kaon-"));


  sumOfProbabilitiesProton = 0;
  for(unsigned int i = 0; i < channelsProton.size(); i++) {
      sumOfProbabilitiesProton += channelsProton[i]->GetBR();   // branching ratio
  }
//  std::cerr << "sumOfProbabilities: " << sumOfProbabilities << std::endl;

  // pionic pbar-n channels [NIM.A.496.102(2003)]
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 0.8,   2, "pi-", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 15.6,  3, "pi-", "pi0", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 3.4,   3, "pi-", "pi-", "pi+"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 17.0,  4, "pi-", "pi-", "pi+", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 39.3,  5, "pi-", "pi-", "pi+", "pi0", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 4.2,   5, "pi-", "pi-", "pi-", "pi+", "pi+"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 15.1,  6, "pi-", "pi-", "pi-", "pi+", "pi+", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 4.1,   7, "pi-", "pi-", "pi-", "pi+", "pi+", "pi0", "pi0"));
  channelsNeutron.push_back(new hbarGeneralPhaseSpaceDecay(pdefAntiProton->GetPDGMass()+pdefNeutron->GetPDGMass(), 0.39,  8, "pi-", "pi-", "pi-", "pi-", "pi+", "pi+", "pi+", "pi0"));

  sumOfProbabilitiesNeutron = 0;
  for(unsigned int i = 0; i < channelsNeutron.size(); i++) {
      sumOfProbabilitiesNeutron += channelsNeutron[i]->GetBR();   // branching ratio
  }

  // The length, over which the pion survival probability decreases
  // to 1/e in the nucleus
  pionAbsorptionLength = 2.0*fermi;   // 14*fermi;

  pionMomentumDegradation = 40*perCent/fermi;

  pbarMess = new hbarAntiProtonMess(this);
}

// destructor

hbarAntiProtonAnnihilationAtRest::~hbarAntiProtonAnnihilationAtRest()
{
////  for(unsigned int i=0; i<channels.size(); ++i) delete channels[i];
   delete pbarMess;
}


// methods.............................................................................


// Warning - this method may be optimized away if made "inline"
G4int hbarAntiProtonAnnihilationAtRest::GetNumberOfSecondaries()
{
  return ( nofSecondaries );
}

void hbarAntiProtonAnnihilationAtRest::SetApplyPionAbsorption(G4String value)
{
   if (value == "on") ApplyPionAbsorption = true;
   else ApplyPionAbsorption = false;
}
/***
// Warning - this method may be optimized away if made "inline"
G4GHEKinematicsVector* hbarAntiProtonAnnihilationAtRest::GetSecondaryKinematics()
{
  return ( &secondaryKinematics[0] );
}
***/

G4double hbarAntiProtonAnnihilationAtRest::AtRestGetPhysicalInteractionLength(
				   const G4Track& track,
				   G4ForceCondition* condition
				   )
{
  // beggining of tracking 
  ResetNumberOfInteractionLengthLeft();

  // condition is set to "Not Forced"
  *condition = NotForced;

  // get mean life time
  currentInteractionLength = GetMeanLifeTime(track, condition);

  if ((currentInteractionLength <0.0) || (verboseLevel>2)){
    G4cout << "AsacusaAntiProtonAnnihilationAtRestProcess::AtRestGetPhysicalInteractionLength ";
    G4cout << "[ " << GetProcessName() << "]" <<G4endl;
    track.GetDynamicParticle()->DumpInfo();
    G4cout << " in Material  " << track.GetMaterial()->GetName() <<G4endl;
    G4cout << "MeanLifeTime = " << currentInteractionLength/ns << "[ns]" <<G4endl;
  }

  return theNumberOfInteractionLengthLeft * currentInteractionLength;

}

G4VParticleChange* hbarAntiProtonAnnihilationAtRest::AtRestDoIt(
                                            const G4Track& track,
					    const G4Step& 
					    )
//
// Handles AntiProtons at rest; a AntiProton can either create secondaries or
// do nothing (in which case it should be sent back to decay-handling
// section
//
{
    //   Initialize ParticleChange
    //     all members of G4VParticleChange are set to equal to 
    //     corresponding member in G4Track

    aParticleChange.Initialize(track);

    //   Store some global quantities that depend on current material and particle

    globalTime = track.GetGlobalTime();  // /s;
    G4Material * aMaterial = track.GetMaterial();
    const G4int numberOfElements = aMaterial->GetNumberOfElements();
    const G4ElementVector* theElementVector = aMaterial->GetElementVector();

    // Determine on which nucleus (atom) the antiproton annihilates in a mixture
//    const G4double* theAtomicNumberDensity = aMaterial->GetAtomicNumDensityVector();
    G4double normalization = 0.0;
    for (G4int i1 = 0; i1 < numberOfElements; i1++) {
    	 // Anntiproton capture probability goes with Z^(2/3)
    	 // J.S. Cohen, Rep. Prog. Phys. 67 (2004) 1769–1819
    	 // (see last line on p. 1793)
         // remark by Clemens: in principal this is only true for high energies,
         //                    but I don't think the error changes anything for
         //                    our simulation.
       normalization += pow(G4double((*theElementVector)[i1]->GetZ()), 0.6666);
//        normalization += theAtomicNumberDensity[i1] ; // change when nucleon specific
//                                                      // probabilities are included.
    }

    // Finally select annihilation target nucleus
    G4double runningSum = 0.0;
    G4double random = G4UniformRand()*normalization;
    for (G4int i2 = 0; i2 < numberOfElements; i2++) {
        runningSum += pow(G4double((*theElementVector)[i2]->GetZ()), 0.6666);
//        runningSum += theAtomicNumberDensity[i2]; // change when nucleon specific
                                                  // probabilities are included.
        if (random <= runningSum) {
            targetZ = G4double((*theElementVector)[i2]->GetZ());
            targetA = G4double((*theElementVector)[i2]->GetN());   // GEANT4 stupidly uses N (instead of A) 
                                                                   // as total number of nucleons
                                                                   // averaged over all isotopes
            targetN = TMath::Abs(targetA - targetZ);
       }
    }
    if (random > runningSum) {
        targetZ = G4double((*theElementVector)[numberOfElements-1]->GetZ());
        targetA = G4double((*theElementVector)[numberOfElements-1]->GetN());
        targetN = TMath::Abs(targetA - targetZ);
    }

    if (verboseLevel>1)
    {
	G4cout << "AsacusaAntiProtonAnnihilationAtRest::AtRestDoIt is invoked " <<G4endl;
    }

    G4ParticleMomentum momentum;

    G4ThreeVector   position = track.GetPosition();

    // This function contains the in nucleus Pion absorption and scattering
    // and the dependence on Neutron and Proton annihilation
    GenerateSecondaries(); 

    aParticleChange.SetNumberOfSecondaries( nofSecondaries ); 

    for ( G4int isec = 0; isec < nofSecondaries; isec++ )
    {
	G4DynamicParticle* aNewParticle = new G4DynamicParticle;

//	aNewParticle->SetDefinition( secondaryKinematics[isec].GetParticleDef() );
//	aNewParticle->SetMomentum( secondaryKinematics[isec].GetMomentum() /* *GeV */ );
       aNewParticle->SetDefinition( (*secondaryProducts)[isec]->GetDefinition() );
       aNewParticle->SetMomentum( (*secondaryProducts)[isec]->GetMomentum() );
	
       
	
       //G4Track* aNewTrack = new G4Track( aNewParticle, localtime /* *s*/, position );
       //aParticleChange.AddSecondary( aNewTrack );
       aParticleChange.AddSecondary( aNewParticle, position);
    }

    aParticleChange.ProposeLocalEnergyDeposit( 0.0 );

    aParticleChange.ProposeTrackStatus(fStopAndKill); // Kill the incident AntiProton

    //   clear InteractionLengthLeft

    ResetNumberOfInteractionLengthLeft();

//    G4cout << "antiproton annihilated" << G4endl;

    return &aParticleChange;

}

inline double sq(double a) { return a*a; }

void hbarAntiProtonAnnihilationAtRest::GenerateSecondaries() {
   // Select whether to annihilate on proton or neutron
   // Also select decay channel
   hbarGeneralPhaseSpaceDecay *the_channel = 0;

   // nuclear radius is linear dependent on (N-Z)/A, (A. Trzcinska et. al. Phys.Rev.LEtt 87,8 2001)
   // linear regression data for deltaRnp is taken from (W. J. Swiatecki et. al. PRC 71,047301 (2005))
   // value for r0 is also from the second paper
   G4double deltaRnp = (-0.03 + 0.9 * (targetN-targetZ)/targetA )*fermi;
   G4double nucleonradius = (1.14*fermi * pow(targetA,1.0/3.0))/targetA;

   G4double protonProb  =  targetZ* nucleonradius * nucleonradius             ;  // 1st-order approximation
   G4double neutronProb = (targetN*(nucleonradius)*(nucleonradius) + deltaRnp);  // 1st-order approximation
   G4double randPN = G4UniformRand() * (protonProb + neutronProb);
   G4double running_probability_sum = 0;
   if (randPN <= protonProb) {
      G4double rand = G4UniformRand() * sumOfProbabilitiesProton;
         for (unsigned int i = 0; i < channelsProton.size(); i++) {
            running_probability_sum += channelsProton[i]->GetBR();
            if (running_probability_sum >= rand) {
            the_channel = channelsProton[i];
            break;
         }
      }
   } else {
      G4double rand = G4UniformRand() * sumOfProbabilitiesNeutron;
         for (unsigned int i = 0; i < channelsNeutron.size(); i++) {
            running_probability_sum += channelsNeutron[i]->GetBR();
            if (running_probability_sum >= rand) {
            the_channel = channelsNeutron[i];
            break;
         }
      }
   }

/*****
   // Select the annihilation channel
   hbarGeneralPhaseSpaceDecay *the_channel = 0;
   G4double rand = G4UniformRand() * sumOfProbabilities;
   G4double running_probability_sum = 0;
   for (unsigned int i = 0; i < channels.size(); i++) {
      running_probability_sum += channels[i]->GetBR();
      if (running_probability_sum >= rand) {
         the_channel = channels[i];
         break;
      }
   }
***/

   if (the_channel == 0) {
      std::cerr << "This should never occur: the_channel = 0" << std::endl;
      exit(1);
   }

/*****
    // Calculate their momenta according to a phase space distribution
    G4GHEKinematicsVector the_kinematics[MAX_SECONDARIES];
    for(G4int i = 0; i < the_channel->GetNumberOfDaughters(); i++) {
        the_kinematics[i].SetZero();
        the_kinematics[i].SetParticleDef(the_channel->GetSecondary(i));
        the_kinematics[i].SetMass(the_channel->GetSecondary(i)->GetPDGMass());
        the_kinematics[i].SetCharge(the_channel->GetSecondary(i)->GetPDGCharge());
    }
*****/

   G4DecayProducts* secondaryPreProducts = the_channel->DecayIt();
//    if(!NBodyPhaseSpace(2*G4Proton::ProtonDefinition()->GetPDGMass(),
//			the_channel->GetNumberOfSecondaries(),
//			the_kinematics))
   nofSecondaries = 0;
   if (!secondaryPreProducts) {
      std::cerr << "Could not generate n-body phase space" << std::endl;
      return;
   }
   nofSecondaries = the_channel->GetNumberOfDaughters();
   if (nofSecondaries != secondaryPreProducts->entries()) {
       std::cerr << "Problem with the number of products" << std::endl;
   }

   // Write pion properties to file before pion absorption and energy loss
   if (DebugFileName != "") {
      G4int n_chargedB = 0;
      for (G4int i=0; i < secondaryPreProducts->entries(); i++) {
          if ((*secondaryPreProducts)[i]->GetCharge() != 0.0) {
            n_chargedB++;
         }
      }

      std::ofstream file((DebugFileName+"bef").c_str(), std::ios::app);
      //file << "# Before " << secondaryPreProducts->entries() << " " << n_chargedB << std::endl;
      for (G4int i=0; i < secondaryPreProducts->entries(); i++) {
         const double px = (*secondaryPreProducts)[i]->GetMomentum().x()/MeV;
         const double py = (*secondaryPreProducts)[i]->GetMomentum().y()/MeV;
         const double pz = (*secondaryPreProducts)[i]->GetMomentum().z()/MeV;

         file << px << " " << py << " " << pz << " " << sqrt(px*px+py*py+pz*pz) << " "
              << (*secondaryPreProducts)[i]->GetKineticEnergy()/MeV << std::endl;
      }
      file.close();
   }

   delete secondaryProducts;
   secondaryProducts = new G4DecayProducts();

   // Take into account the effect of the nucleus:
   // pion energy loss due to (inelastic) scattering, and absorption.
   // Uses a very rough quasi-free scattering method
   // which does not take into account angular deflection
   //
   // Papers used:
   // J. Hüfner and M. Thies, PRC 20 (1979) 273
   // J. Cugnon and J. Vandermeulen, NPA 445 (1985) 717
   // J. Cugnon et al., NPA 470 (1987) 558
   // J. Cugnon et al., PRC 63 (2001) 027310

   // the radius of the target nucleus
   const G4double radius = targetZ*nucleonradius + targetN*(nucleonradius) + deltaRnp; 

   // generate a random direction
   G4double xx = 0.0, yy = 0.0, zz = 0.0;
   if (targetA > 4 && ApplyPionAbsorption) {
      G4double CosTheta = 1.0 - 2.0*G4UniformRand();
      G4double SinTheta = sqrt(1.0 - CosTheta*CosTheta);
      G4double Phi = twopi*G4UniformRand();
      xx = cos(Phi)*SinTheta;
      yy = sin(Phi)*SinTheta;
      zz = CosTheta;
   }


   nofSecondaries = 0;
   for (G4int i = 0; i < the_channel->GetNumberOfDaughters(); i++) {
      G4bool survived = true;
      if (targetA > 4 && ApplyPionAbsorption) {
         const G4ThreeVector p = (*secondaryPreProducts)[i]->GetMomentum(); // the_kinematics[i].GetMomentum();
         G4double Ekin = (*secondaryPreProducts)[i]->GetKineticEnergy();
         const G4double cos_alpha = (xx*p.x() + yy*p.y() + zz*p.z())/sqrt(sq(p.x())+sq(p.y())+sq(p.z()));
         const G4double sin_alpha = sqrt(1.0 - cos_alpha*cos_alpha);

	 // delta = 1.35 +/- 0.1 fm (J. Cugnon et al., PRC 63 (2001) 027310)
         const G4double delta = 1.35*fermi;    // 2.3

         if ((cos_alpha > 0) && ((delta+radius)*sin_alpha < radius)) {
            // The pion hits the nucleus
            G4double length = 2.0*sqrt(radius*radius - (delta+radius)*(delta+radius)*sin_alpha*sin_alpha);
            G4double MFP_abs = 0.0;    // pion absorption mean free path
            G4double MFP_sc = 0.0;     // pion scattering mean free path
            G4double length_abs = 0.0;
            G4double length_sc = 0.0;

            // loop until pion leaves the nucleus or is absorbed
	    // this is based on: J. Hüfner and M. Thies, PRC 20 (1979) 273,
	    // applying and optical potential calculation
            while (length > 0.0) {
               MFP_abs = 1.0/(0.1+7000.0/(8000.0 + (Ekin/MeV-180.0)*(Ekin/MeV-180.0)))*fermi;
               MFP_sc = 0.5*1.0/(Ekin/MeV*30.0/(10000.0 + (Ekin/MeV-160.0)*(Ekin/MeV-160.0)))*fermi;
               length_abs = -MFP_abs*log(G4UniformRand());
               length_sc = -MFP_sc*log(G4UniformRand());
               if ((length_abs > length) && (length_sc > length)) {
                  // no interaction; pion leaves the nucleus
                  break;
               } else if (length_abs < length_sc) {
                  // pion absorption
                  survived = false;
                  break;
               } else {
                  // pion scattering
                  // pion does not change direction
                  Ekin -= Ekin*G4UniformRand(); // very rough
                  length -= length_sc;
               }
            }

            (*secondaryPreProducts)[i]->SetKineticEnergy(Ekin);
            //the_kinematics[i].SetMomentumAndUpdate(p/degr);
            //the_kinematics[i].SetKineticEnergyAndUpdate(the_kinematics[i].GetKineticEnergy()/degr);
         }
      }
      if (survived) {
         secondaryProducts->PushProducts((*secondaryPreProducts)[i]);
      // (*secondaryProducts)[nofSecondaries] = (*secondaryPreProducts)[i];
      //secondaryKinematics[nofSecondaries] = the_kinematics[i];
         nofSecondaries++;
      }
   }

   if (nofSecondaries != secondaryProducts->entries()) {
       std::cerr << "Problem #2 with the number of products" << std::endl;
   }

   // Write pion properties to file after pion absorption and energy loss
   
   if (DebugFileName != "") {
      G4int n_chargedA = 0;
      for (G4int i=0; i < secondaryProducts->entries(); i++) {
          if ((*secondaryProducts)[i]->GetCharge() != 0.0) {
            n_chargedA++;
         }
      }

      std::ofstream file((DebugFileName+"after").c_str(), std::ios::app);
      //file << "# After " << secondaryProducts->entries() << " " << n_chargedA << std::endl;
      for (G4int i=0; i < secondaryProducts->entries(); i++) {
         const double px = (*secondaryProducts)[i]->GetMomentum().x()/MeV;
         const double py = (*secondaryProducts)[i]->GetMomentum().y()/MeV;
         const double pz = (*secondaryProducts)[i]->GetMomentum().z()/MeV;

         file << px << " " << py << " " << pz << " " << sqrt(px*px+py*py+pz*pz) << " "
              << (*secondaryProducts)[i]->GetKineticEnergy()/MeV << std::endl;
      }
      file << std::endl;
      file.close();
   }

//    nofSecondaries = 0;
} // GenerateSecondaries


G4int hbarAntiProtonAnnihilationAtRest::NFac(G4int n)
{
  G4int ret_val;

  static G4int i, m;

  // *** NVE 16-MAR-1988 CERN GENEVA ***
  // ORIGIN : H.FESEFELDT (27-OCT-1983)

  ret_val = 1;
  m = n;
  if (m > 1) {
    if (m > 10) {
      m = 10;
    }
    for (i = 2; i <= m; ++i) {
      ret_val *= i;
    }
  }
  return ret_val;

} // NFac


void hbarAntiProtonAnnihilationAtRest::Normal(G4float *ran)
{
  static G4int i;

  // *** NVE 14-APR-1988 CERN GENEVA ***
  // ORIGIN : H.FESEFELDT (27-OCT-1983)

  *ran = G4float(-6.);
  for (i = 1; i <= 12; ++i) {
    *ran += G4UniformRand();
  }

} // Normal


G4bool hbarAntiProtonAnnihilationAtRest::IsApplicable(
				 const G4ParticleDefinition& particle
				 )
{
   return ( &particle == pdefAntiProton );
}

