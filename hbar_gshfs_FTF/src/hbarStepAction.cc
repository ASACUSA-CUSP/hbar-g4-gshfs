#include "hbarStepAction.hh"
#include "hbarDetConst.hh"
#include "hbarRunAction.hh"
#include "hbarTrackAction.hh"
#include "hbarMWSetup.hh"
#include "hbarAntiHydrogen.hh"
#include "hbarHydrogen.hh" // Added by Clemens
#include "hbarFieldSetup.hh"

#include "G4Step.hh"
#include "G4StepPoint.hh"
#include "G4DecayTable.hh"
#include "G4RunManager.hh"
#include "Randomize.hh"

#include "CLHEP/Vector/TwoVector.h"

hbarStepAction::hbarStepAction(hbarRunAction *anAct):
  runAction(anAct)
{
   detConst = runAction->GetDetector();
   Field = detConst->GetMagneticFieldSetup()->GetField();
   
}

hbarStepAction::~hbarStepAction()
{ }

void hbarStepAction::UserSteppingAction(const G4Step* aStep)
{
   // Check whether the particle is stuck somewhere
   // To do so, calculate the square of distance from the origin
   // and compare it with the value 10 steps ago
   G4double TL = aStep->GetTrack()->GetTrackLength();
   ((hbarTrackAction*)(G4RunManager::GetRunManager()->GetUserTrackingAction()))->SetTrackLength(TL);
   
   
   // Helga code:
   /*G4Track * track= aStep->GetTrack();
  string name=track->GetDefinition()->GetParticleName();
  if (track->GetKineticEnergy()<1*eV && name=="anti_proton"){
    G4EventManager::GetEventManager()->AbortCurrentEvent();
  }*/
   
   if (aStep->GetTrack()->GetCurrentStepNumber() > 10) 
	 {
	    //G4cout << aStep->GetTrack()->GetCurrentStepNumber() << G4endl;
      if (std::abs( ((hbarTrackAction*)(G4RunManager::GetRunManager()->GetUserTrackingAction()))->GetTrackLengthInPast() - TL ) < 0.000001*mm) 
		{
		  // particle did not move in the last 10 steps
		  // (possible due to a bug in GEANT4.8.2 which did not yet exist in GEANT4.7.1)
		  // so kill it
		  //G4cout << aStep->GetTrack()->GetCurrentStepNumber() << G4endl;
		  G4cerr << " *** Particle is stuck in event #" << G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID() << ". Killing it. ***" << G4endl;		 		  
		  aStep->GetTrack()->SetTrackStatus(fStopAndKill);
		} 
	 }
   
   G4ParticleDefinition *partDef = aStep->GetTrack()->GetDefinition();
   
   
   //if (partDef->GetPDGEncoding()>10000 && partDef->GetParticleName().length() < 5) std::cout << partDef->GetParticleName() << " " << partDef->GetParticleName().length()<< std::endl;
   
   
   G4double *gradmu = Field->GetGradMu();
   G4double *mu     = Field->GetMu();

   if (partDef == hbarAntiHydrogen::Definition() || partDef == hbarHydrogen::Definition()) 
	 {
	   ParticleDependentUserStepAction(aStep);
	 }
   else
	 {	
	   gradmu[0] = 0;
	   gradmu[1] = 0;
	   gradmu[2] = 0;
	   *mu       = 0;
	 }
}

void hbarStepAction::ParticleDependentUserStepAction(const G4Step* aStep)
{

  // variable definitions
  G4double preZ  = aStep->GetPreStepPoint()->GetPosition().z();   // beam direction
  G4double postZ = aStep->GetPostStepPoint()->GetPosition().z();  // beam direction
  G4double postX = aStep->GetPostStepPoint()->GetPosition().x();
  G4double postY = aStep->GetPostStepPoint()->GetPosition().y();
  G4double postR = sqrt(sqr(postX) + sqr(postY));

  hbarHydrogenLike * myHbarHydrogenLike = dynamic_cast<hbarHydrogenLike *>(aStep->GetTrack()->GetDefinition());
  
  if(myHbarHydrogenLike == NULL)
	{
	  throw hbarException("The particle was null.");
	}

  G4int preN = myHbarHydrogenLike->GetN();
  ///Store in a dictionary in order to color correctly.
  (static_cast<hbarUserTrackInformation*>(aStep->GetTrack()->GetUserInformation()))->
	DictionaryInsert(aStep,hbarTrackDictionaryEntry(preN,myHbarHydrogenLike->GetF(), myHbarHydrogenLike->GetM()));
  
  
  
  GeneralQuantumNumbers * prior = myHbarHydrogenLike->GetQuantumNumbers()->CreateCopy();

  myHbarHydrogenLike->StepTime(aStep->GetDeltaTime()/s); ///do the decay if we should.

  if(!prior->Equals(myHbarHydrogenLike->GetQuantumNumbers()))
	{
	  if(runAction->GetVerboseDecay())
		{
		  G4cout << "Decay from " << prior->ToString() << " to " << myHbarHydrogenLike->GetQuantumNumbers()->ToString() << " at z=" << postZ/cm << "cm, r=" << postR/cm << "cm." << G4endl;
		  
		}
	  if(myHbarHydrogenLike->GetN() == 1)
		{
		  ///Randomize ground state.
		  runAction->GetPrimaryGeneratorAction()->PrepareGroundState(myHbarHydrogenLike);
		}
	}
  if(runAction->GetVerboseStepping())
	{ 
	  G4cout << "Particle now at z=" << postZ/cm << "cm, r=" << postR/cm << "cm " << "velocity " << aStep->GetPreStepPoint()->GetVelocity()/(m/s) << "m/s Current state: " << myHbarHydrogenLike->GetQuantumNumbers()->ToString() << G4endl;

	}



  
  G4double cavityentrancepos = detConst->GetCavityConst()->GetMWSetup()->GetCavityCenter() - detConst->GetCavityConst()->GetMWSetup()->GetCavityLength()/2.0;


  if ((preZ < cavityentrancepos) && (postZ >= cavityentrancepos) && (postR <= detConst->GetCavityConst()->GetMWSetup()->GetCavityInnerDiameter()/2.0))
	{
	  runAction->SetQuantumNumberAtCavity(preN);
	}
  


  G4double cavityexitpos = detConst->GetCavityConst()->GetMWSetup()->GetCavityCenter() + detConst->GetCavityConst()->GetMWSetup()->GetCavityLength()/2.0;
  G4double cavitycenterpos = detConst->GetCavityConst()->GetMWSetup()->GetCavityCenter();


  if(detConst->GetDDetConst() != NULL)
	{
	  // start of the dummy detector
	  G4double ddectstart = detConst->GetDDetConst()->GetDetectorStart();
	  
	  // log velocity distribution directly in front of dummy det
	  
	  if (preZ > ddectstart - 15 && postZ <= ddectstart)
		{
		  G4double velocity = aStep->GetPreStepPoint()->GetVelocity();
		  runAction->SetVelocityDet(velocity);
		  runAction->SetVelocityZDet(velocity*aStep->GetPreStepPoint()->
									 GetMomentumDirection().z());
		  // Angular distribution around Z direction
		  G4ThreeVector posdiff = aStep->GetPreStepPoint()->GetPosition() -
			aStep->GetPostStepPoint()->GetPosition();
		  runAction->SetImpactAngleDet(posdiff.theta());
	  }
	}



  //  Cavity spin flip part
  
  // log time of entrance to the cavity
  if ((preZ < cavityentrancepos) && (postZ >= cavityentrancepos)
      && (postR <= detConst->GetCavityConst()->GetMWSetup()->GetCavityInnerDiameter()/2.0)) 
	{
	  G4double entrancetime = aStep->GetPostStepPoint()->GetGlobalTime();
	  runAction->SetEntranceTime_Cavity(entrancetime);
	}
  
  // log position (x,y) when passing through the center of the cavity
  if ((preZ < cavitycenterpos) && (postZ >= cavitycenterpos)
      && (postR <= detConst->GetCavityConst()->GetMWSetup()->GetCavityInnerDiameter()/2.0)) 
	{
	  runAction->SetPositionX_Cavity(postX);
	  runAction->SetPositionY_Cavity(postY);
	  G4double velocity = aStep->GetPreStepPoint()->GetVelocity();
	  runAction->SetVelocity(velocity);
	  runAction->SetVelocityZ(velocity*aStep->GetPreStepPoint()->GetMomentumDirection().z());
	}
  // log time of exit from the cavity and
  // do the spin flip
  if ((preZ < cavityexitpos) && (postZ >= cavityexitpos)
      && (postR <= detConst->GetCavityConst()->GetMWSetup()->GetCavityInnerDiameter()/2.0)) 
	{
	  G4double exittime = aStep->GetPostStepPoint()->GetGlobalTime();
	  runAction->SetExitTime_Cavity(exittime);
	  G4double timeincavity = detConst->GetCavityConst()->GetMWSetup()->GetCavityLength()/runAction->GetVelocityZ();
	  G4double cx = runAction->GetPositionX_Cavity();
	  G4double cy = runAction->GetPositionY_Cavity();
	  detConst->GetCavityConst()->GetMWSetup()->MakeMWTransition(myHbarHydrogenLike, aStep, cx, cy, timeincavity);
  }
  
  //  Majorana spin flip part
  
  // log angle of field line at the exit of sextupole #1-#n
  for (G4int sn = 1; sn <= detConst->GetMaximumSextupoleNumber(); sn++) 
	{
	  // but only if they are in use
	  if (detConst->GetSextupoleConst()->IsUseSextupole(sn)) {
		G4double sext1pos = detConst->GetSextupoleConst()->GetSextupoleEnd(sn);
		// the atom just exited the magnet
		if ((preZ < sext1pos) && (postZ >= sext1pos)
			&& (postR <= detConst->GetSextupoleConst()->GetSextupoleRearInnerDiameter(sn)/2.0)) {
		  G4double angle = runAction->GetDetector()->GetMagneticFieldSetup()->GetField()->GetFieldAngle(postX, postY);
		  // log the exit angle
		  runAction->SetExitAngle(angle);
		}
	  }
	}
  
  // log angle of field line at the entrance of sextupole #2-#n
  // !!!! do not log for magnet #1 !!!!!
  for (G4int sn = 2; sn <= detConst->GetMaximumSextupoleNumber(); sn++) 
	{
	  // but only if they are in use
	  if (detConst->GetSextupoleConst()->IsUseSextupole(sn)) {
		G4double sext2pos = detConst->GetSextupoleConst()->GetSextupoleStart(sn);
		// the atom just entered the magnet
		if ((preZ < sext2pos) && (postZ >= sext2pos)
			&& (postR <= detConst->GetSextupoleConst()->GetSextupoleFrontInnerDiameter(sn)/2.0)) {
		  G4double angle = runAction->GetDetector()->GetMagneticFieldSetup()->GetField()->GetFieldAngle(postX, postY);
		  runAction->SetEntranceAngle(angle);
		  G4double anglediff = runAction->GetEntranceAngle() - runAction->GetExitAngle();
		  // log the entrance angle
		  runAction->GetAngleDifferenceHistogramAll()->Fill(anglediff);
		  // do the Majorana spin flip
		  detConst->GetMagneticFieldSetup()->MakeMajoranaTransition(myHbarHydrogenLike, anglediff);
		}
	  }
	}
  
  // calculate magnetic moment of particle
  // calculate mu and grad(mu)
  G4double f[24];
  G4double *gradmu = Field->GetGradMu();
  G4double *mu     = Field->GetMu();
  const  double P[3] = {postX, postY, postZ};
  Field->GetFieldValue(P,f);
  G4double fieldValue = std::sqrt(f[0]*f[0] + f[1]*f[1] + f[2]*f[2]);
  G4double deltaX = 0.1*mm;
  for (G4int i = 0; i < 3; i++) 
	{
	  gradmu[i] = (myHbarHydrogenLike->GetMu(fieldValue + f[i+6] * deltaX) - myHbarHydrogenLike->GetMu(fieldValue - f[i+6]*deltaX))/(2.0*deltaX);
	}
  
  *mu = myHbarHydrogenLike->GetMu(fieldValue/tesla);
  bool trigger = myHbarHydrogenLike->SetField(fieldValue/tesla);
  if(runAction->GetVerboseTypeTransitions() && trigger)
	cout << "A transition to " << (myHbarHydrogenLike->IsWeakFieldRegime()?"WEAK":"STRONG") << " fields was triggered by B=" << fieldValue/tesla << " T, current state is " << myHbarHydrogenLike->GetQuantumNumbers()->ToString() << "." << endl;
  

}
