#include "hbarEventActionMess.hh"
#include "hbarEventAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "globals.hh"

hbarEventActionMess::hbarEventActionMess(hbarEventAction* EvAct)
:eventAction(EvAct)
{ 
  DrawCmd = new G4UIcmdWithAString("/event/drawTracks",this);
  DrawCmd->SetGuidance("Draw the tracks in the event");
  DrawCmd->SetGuidance("  Choice : none, charged(default), pbar, hbar, inS2, hit(s), realhit(s), all");
  DrawCmd->SetParameterName("choice",true);
  DrawCmd->SetDefaultValue("charged");
  DrawCmd->SetCandidates("none charged pbar hbar inS2 hit hits realhit realhits all");
  DrawCmd->AvailableForStates(G4State_Idle);

  PrintCmd = new G4UIcmdWithAnInteger("/event/printModulo",this);
  PrintCmd->SetGuidance("Print events modulo n");
  PrintCmd->SetParameterName("EventNb",false);
  PrintCmd->SetRange("EventNb>0");
  PrintCmd->AvailableForStates(G4State_Idle);     

  PosInfoCmd = new G4UIcmdWithAString("/event/printPosInfo",this);
  PosInfoCmd->SetGuidance("Print stopping position of particles");
  PosInfoCmd->SetGuidance("  Choice : on, off(default)");
  PosInfoCmd->SetParameterName("choice",true);
  PosInfoCmd->SetDefaultValue("off");
  PosInfoCmd->SetCandidates("on off");
  PosInfoCmd->AvailableForStates(G4State_Idle);

  EventInfoCmd = new G4UIcmdWithAString("/event/printEventInfo",this);
  EventInfoCmd->SetGuidance("Print event information");
  EventInfoCmd->SetGuidance("  Choice : on, off(default)");
  EventInfoCmd->SetParameterName("choice",true);
  EventInfoCmd->SetDefaultValue("off");
  EventInfoCmd->SetCandidates("on off");
  EventInfoCmd->AvailableForStates(G4State_Idle);


  triggerDir = new G4UIdirectory("/trigger/");
  triggerDir->SetGuidance("Set triggers i.e. coincidence & anticoicidence multiplicities and levels");

  CoinMultiCmd = new G4UIcmdWithAnInteger("/trigger/setCoincidenceMultiplicity",this);
  CoinMultiCmd->SetGuidance("Set the coincidence number for the detectors");
  CoinMultiCmd->SetParameterName("Count",true,false);
  CoinMultiCmd->SetDefaultValue(1);
  CoinMultiCmd->SetRange("Count>=0");
  CoinMultiCmd->AvailableForStates(G4State_Idle);

  AntiCoinMultiCmd = new G4UIcmdWithAnInteger("/trigger/setAntiCoincidenceMultiplicity",this);
  AntiCoinMultiCmd->SetGuidance("Set the anticoincidence number for the detectors");
  AntiCoinMultiCmd->SetParameterName("Count",true,false);
  AntiCoinMultiCmd->SetDefaultValue(1);
  AntiCoinMultiCmd->SetRange("Count>=0");
  AntiCoinMultiCmd->AvailableForStates(G4State_Idle);

  TriggerLevelCmd = new G4UIcmdWithADoubleAndUnit("/trigger/setTriggerLevel",this);
  TriggerLevelCmd->SetGuidance("Set trigger level in the detectors (MeV)");
  TriggerLevelCmd->SetParameterName("Energy",false,false);
  TriggerLevelCmd->SetDefaultUnit("MeV");
  TriggerLevelCmd->SetRange("Energy>=0.");
  TriggerLevelCmd->AvailableForStates(G4State_Idle);

}

hbarEventActionMess::~hbarEventActionMess()
{
  delete DrawCmd;
  delete PrintCmd;
  delete PosInfoCmd; delete EventInfoCmd;
  delete CoinMultiCmd; delete AntiCoinMultiCmd; delete TriggerLevelCmd;
  delete triggerDir;
}

void hbarEventActionMess::SetNewValue(G4UIcommand * command,G4String newValue)
{ 
  if(command == DrawCmd)
    {eventAction->SetDrawFlag(newValue);}

  if(command == PrintCmd)
    {eventAction->SetPrintModulo(PrintCmd->GetNewIntValue(newValue));}

  if(command == PosInfoCmd)
    {eventAction->SetPosFlag(newValue);}

  if(command == EventInfoCmd)
    {eventAction->SetEventFlag(newValue);}

  if(command == CoinMultiCmd)
    {eventAction->SetCoincidenceMultiplicity(CoinMultiCmd->GetNewIntValue(newValue));}

  if(command == AntiCoinMultiCmd)
    {eventAction->SetAntiCoincidenceMultiplicity(AntiCoinMultiCmd->GetNewIntValue(newValue));}

  if(command == TriggerLevelCmd)
    {eventAction->SetTriggerLevel(TriggerLevelCmd->GetNewDoubleValue(newValue));}

}

