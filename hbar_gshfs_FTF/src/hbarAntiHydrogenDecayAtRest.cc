#include "hbarAntiHydrogenDecayAtRest.hh"

hbarAntiHydrogenDecayAtRest::hbarAntiHydrogenDecayAtRest() : 
  hbarHydrogenLikeDecayAtRest("AntiHydrogenDecayAtRest")
{
  
}
//! Destructor frees memory after particle is killed. Currently does nothing.
hbarAntiHydrogenDecayAtRest::~hbarAntiHydrogenDecayAtRest()
{

}

void hbarAntiHydrogenDecayAtRest::AtRestDoItAdditional(const G4Track& track)
{
  G4ThreeVector position = track.GetPosition();

  G4DynamicParticle* aNewParticle1 = new G4DynamicParticle;
  aNewParticle1->SetDefinition(G4AntiProton::AntiProton());
  aNewParticle1->SetMomentum(G4ThreeVector(0.0 * eV, 0.0 * eV, 0.0 * eV));
  //aParticleChange.AddSecondary( aNewTrack1 );
  aParticleChange.AddSecondary( aNewParticle1, position);
  
  G4DynamicParticle* aNewParticle2 = new G4DynamicParticle;
  aNewParticle2->SetDefinition(G4Positron::Positron());
  aNewParticle2->SetMomentum(G4ThreeVector(0.0 * eV, 0.0 * eV, 0.0 * eV));
 // aParticleChange.AddSecondary( aNewTrack2 );
  aParticleChange.AddSecondary(aNewParticle2, position);
}
