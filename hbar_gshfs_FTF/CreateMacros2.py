#!/usr/bin/python
"""
This script will recursively parse Geant4 macros into partitioned files suitable for parallell execution.
For usage, run the script with the -h flag.
In order to use it effectively for your purposes, you will most probably need to edit it by hand, since it is purpose-written for generating specific data sets in specific ways.

For questions, refer to Rikard Lundmark.
"""
import sys, argparse, subprocess, os, errno
from multiprocessing import Pool

temperatures = [10, 50] #[3, 5, 10, 30, 50, 70, 100]
SextupoleB = [0.616103, 3.08052]
qnumbers = range(1, 28)
positions = [-165, -173] #range(-180, -159)
fieldSeek = ["sHFS", "wHFS", "sLFS", "wLFS"]
nEvents = 2000
nRepeatRuns = 20
BFieldmap = ["Brz_fieldmap.txt", "B_fieldmap_doubleCUSP_500x500.txt"]
lowerHysteresis = [0.1, 0.3, 0.5, 0.9]
upperHysteresis = [10.0, 3.0, 5.0, 1.1] 
runIndex = range(0, nRepeatRuns)


def mkdir_p(path):
    """Create directory if does not exist."""
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


def isForbidden(line):
    """Certain lines should NOT be copied from the macro file. These are excluded by this function."""

    starters = ["/file/setFileName","/gun/setSourceTemperature","/gun/setSourceTemperature","/gun/setStateN","/gun/setStateL","/gun/setStateTwoJ","/gun/setStateTwoMJ","/gun/setStateML", "/gun/setStateMS", "/gun/setUseStrongInitialState", "/run/beamOn", "/event/printModulo", "/vis/", "/gun/setSourceCenter", "/setup/cusp/setCuspMagAsciiFile", "/field/setUseSextupole", "/field/setMaximumValue", "/gun/setLowerHysteresis", "/gun/setUpperHysteresis"] 
    for s in starters:
        if line.startswith(s):
            return True
    return False

#Recursively get the contents of the macro file(s)
def getEssentials(file):
    lines = []
    with open(file,"r") as f:
        for line in f:
            line=line.rstrip('\n')
            if line.startswith("/control/execute"):
                lines.extend(getEssentials(line.replace("/control/execute ","")))
            elif not line.strip().startswith("#") and len(line.strip()) > 3:
                if not isForbidden(line):
                    lines.append(line)
    return lines

def paramToName(temp, n, pos, fmap, fseek, lH, uH, runIndex):
    """Some functions pertaining to file names."""
    toReturn = "t" + str(temp) + "n" + str(n) + "p" + str(pos) + "m" + str(BFieldmap.index(fmap)) + fseek + "lH" + str(lH) + "uH" + str(uH) + "run" + str(runIndex)
    return toReturn.replace('.','.')

def paramToName(temp, n, pos, fmap, fseek, lH, uH, runIndex = ""):
    """Some functions pertaining to file names."""
    if runIndex != "":
        toReturn = "t" + str(temp) + "n" + str(n) + "p" + str(pos) + "m" + str(BFieldmap.index(fmap)) + fseek + "lH" + str(lH) + "uH" + str(uH) + "run" + str(runIndex)
    else:
        toReturn = "t" + str(temp) + "n" + str(n) + "p" + str(pos) + "m" + str(BFieldmap.index(fmap)) + fseek + "lH" + str(lH) + "uH" + str(uH)
    return toReturn.replace('.','.')

def paramToOutputName(macrodir, temp, n, pos, fmap, fseek, lH, uH):
    """Some functions pertaining to file names."""
    return macrodir + "/" + paramToName(temp, n, pos, fmap, fseek, lH, uH) + ".mac"

def paramToRootName(rootdir, temp, n, pos, fmap, fseek, lH, uH, runIndex):
    """Some functions pertaining to file names."""
    return rootdir + "/" + paramToName(temp, n, pos, fmap, fseek, lH, uH, runIndex) + ".root"

def postProcessor(lineList, macrodir, outdir):
    """Do this with the lines obtained from the macro file."""
    for temp in temperatures:
        for n in qnumbers:
            for pos in positions:
                for fmap in BFieldmap:
                    for fseek in fieldSeek:
                        for lH, uH in zip(lowerHysteresis, upperHysteresis):
                            if BFieldmap.index(fmap) != positions.index(pos):
                                continue
                            with open(paramToOutputName(macrodir, temp,n, pos, fmap, fseek, lH, uH), "w") as f:
                                for line in lineList:
                                    f.write(line + "\n")
                                    if line.startswith("/setup/cusp/setCuspElAsciiFile"):
                                        f.write("/setup/cusp/setCuspMagAsciiFile " + fmap + "\n")
                                f.write("\n")
                                f.write("/gun/setSourceCenter " + str(pos*0.01) + " m\n")
                                f.write("/gun/setSourceTemperature " + str(temp) + " K\n")
                                f.write("/gun/setLowerHysteresis " + str(lH) + "\n")
                                f.write("/gun/setUpperHysteresis " + str(uH) + "\n")
                                f.write("/gun/setStateN " + str(n) + "\n")
                                f.write("/gun/setStateL " + str(n-1) + "\n")
                                f.write("/gun/setStateTwoJ " + str(2*n-1) + "\n")
                                f.write("/gun/setUseStrongInitialState on" + "\n");
                                if fseek == "sHFS":
                                    f.write("/gun/setStateTwoMJ " + str(2*n-1) + "\n")
                                    f.write("/gun/setStateML " + str(n-1) + "\n")
                                    f.write("/gun/setStateTwoMS 1" + "\n");
                                elif fseek == "wHFS":
                                    f.write("/gun/setStateTwoMJ " + str(1) + "\n")
                                    f.write("/gun/setStateML 0" + "\n")
                                    f.write("/gun/setStateTwoMS 1" + "\n");
                                elif fseek == "sLFS":
                                    f.write("/gun/setStateTwoMJ " + str(-2*n+1) + "\n")
                                    f.write("/gun/setStateML " + str(-n+1) + "\n")
                                    f.write("/gun/setStateTwoMS -1" + "\n");
                                elif fseek == "wLFS":
                                    f.write("/gun/setStateTwoMJ " + str(-1) + "\n")
                                    f.write("/gun/setStateML 0"  + "\n")
                                    f.write("/gun/setStateTwoMS -1" + "\n");
                                else:
                                    sys.stdout.write("Internal error!")
                                    exit(12)
                                for sextNr in range(1,3):
                                    f.write("/field/setUseSextupole " + str(sextNr) + "\n")
                                    f.write("/field/setMaximumValue " + str(SextupoleB[temperatures.index(temp)]) + "\n")
                                    f.write("/event/printModulo 1000\n")
                                for idx in runIndex:
                                    f.write("\n")
                                    f.write("/file/setFileName " + paramToRootName(outdir, temp, n, pos, fmap, fseek, lH, uH, idx) + "\n")
                                    f.write("/run/beamOn " + str(nEvents) + "\n\n");


def g4Run(macrofile):
    """Run the G4 simulation on a specific macro file."""
    sys.stdout.write("Running " + macrofile + "\n")
    #with open("/dev/null", "w") as f:
    with open("/dev/null", "w", 0) as f:
        subprocess.call(['./hbar_gshfs', macrofile], stdout=f, stderr=f)

def main():
    """Main method."""
    parser = argparse.ArgumentParser(description='Generate scriptfiles for the hbar_gshfs simulation.', epilog='For any questions, contact Rikard')
    parser.add_argument('infile',metavar='infile',type=str, help='Base (main) macro file to build new macro files.')
    parser.add_argument('macrodir',metavar='macrodir',type=str, help='Directory to store processed macro files in.')
    parser.add_argument('outdir',metavar='outdir',type=str, help='Root file output directory.')
    parser.add_argument('-run', metavar='',type=bool, nargs='?', const=True, default=False, help="Specify this flag to actually launch the G4 processes in parallell.")
    parser.add_argument('-rm',metavar='',type=bool, nargs='?', const=True, default=False, help="Specify this flag to remove the macro files again after completion (usually used with the -run flag).")

    args = parser.parse_args()
    lines = getEssentials(args.infile)
    postProcessor(lines, args.macrodir, args.outdir)
    
    if args.run:
        mkdir_p(args.outdir)
        pool=Pool(10)
        for n in reversed(qnumbers):
            for pos in positions:
                for temp in temperatures:
                    for fmap in BFieldmap:
                        for fseek in fieldSeek:
                            for lH, uH in zip(lowerHysteresis, upperHysteresis):
                                if BFieldmap.index(fmap) != positions.index(pos):
                                    continue
                            
                                pool.apply_async(g4Run,args=(paramToOutputName(args.macrodir,temp, n, pos, fmap, fseek, lH, uH),))
        pool.close();
        pool.join();

    if args.rm:
        for n in qnumbers:
            for pos in positions:
                for temp in temperatures:
                    for fmap in BFieldmap:
                        for fseek in fieldSeek:
                            for lH, uH in zip(lowerHysteresis, upperHysteresis):
                                if BFieldmap.index(fmap) != positions.index(pos):
                                    continue
                                os.remove(paramToOutputName(args.macrodir,temp, n, pos, fmap, fseek, lH, uH))

if __name__ == "__main__":
    """Launch."""
    main()

