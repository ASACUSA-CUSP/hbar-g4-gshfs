//compile with: g++ `root-config --cflags --glibs` -lMinuit2 bgo_energy_dep.cc 

#include <iostream>
#include <iomanip>
#include <math.h>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <cstdlib>
#include <fstream>
#include<vector>

using namespace std;

int main() {

	vector<double> ref_rl, ref_comp;
	vector<double> wl_1, wl_2;

	ifstream inFile2("bgo_emission_spec.dat");
	ifstream inFile1("bgo_refractive_index.txt");
	
	int numberoflines=0, lcount=0;
	double tempen=0, evt=0, etot=0.0, oldevt =1;			

	//file read in =================================================================
	// count number of lines in file
	numberoflines = std::count(std::istreambuf_iterator<char>(inFile1), 
	std::istreambuf_iterator<char>(), '\n');
	
	inFile1.clear(); // clear eof flag
	inFile1.seekg(0, std::ios::beg); // go back to firstline	
		
	while(!inFile1.eof() && lcount != numberoflines) {
		inFile1 >> tempen >> etot;
		ref_rl.push_back(etot);
		wl_1.push_back(tempen);
		lcount++;
	}

	//file read in =================================================================
	// count number of lines in file
	lcount = 0;
	numberoflines = std::count(std::istreambuf_iterator<char>(inFile2), 
	std::istreambuf_iterator<char>(), '\n');
	
	inFile2.clear(); // clear eof flag
	inFile2.seekg(0, std::ios::beg); // go back to firstline	
		
	while(!inFile2.eof() && lcount != numberoflines) {
		inFile2 >> tempen >> etot;
		ref_comp.push_back(etot);
		wl_2.push_back(tempen);
		lcount++;
	}
	//===============================================================================
	
	ofstream fileOut("bgo_emissionspec_refindex.dat");
	double volts = 0.0;
	for(int i=0; i<ref_comp.size(); i++) {
		volts = wl_2[i];

		std::vector<double>::iterator upperwl = std::upper_bound(wl_1.begin(), wl_1.end(), volts);
		std::vector<double>::iterator lowerwl = upperwl - 1;

		double ref_lower = ref_rl[(lowerwl - wl_1.begin())];
		double ref_upper = ref_rl[(upperwl - wl_1.begin())];

		// linear interpolation
		double ret_val = ref_lower + (ref_upper - ref_lower)*(volts - *lowerwl)/(*upperwl - *lowerwl);

		fileOut << volts << "	" << ref_comp[i] << "	" << ret_val << endl;
	}

return 0;

}
