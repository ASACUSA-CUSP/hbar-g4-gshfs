#!/bin/bash
function Main()
{
	CheckRequirements
	SetupMakefile
	PromptAndConfigureHbarGshfs
	WarnOldDatabase
	DownloadDatabase
	FinalizeSetup
}


function FinalizeSetup()
{
	echo "Setup complete. If you experienced any problems, contact Rikard."
}

#Used to issue warning about old database version based on file modification date.
function WarnOldDatabase()
{
	if [ -f "hstates.root" ]
	then
		REFTIME=1388098092
		CURRTIME=`stat -c %Y hstates.root | cut -d ' ' -f1`
		if [ "$REFTIME" -gt "$CURRTIME" ]
		then
			echo "WARNING: hstates.root is OUTDATED. DO NOT use it!"
		fi
	fi
}


function PromptAndConfigureHbarGshfs()
{
	read -p "Link to radiative decay code (hbar_cfac_ftf)? (y/n)" -n 1 -r
	echo -en "\n"
	if [[ $REPLY =~ ^[yY]$ ]]
	then
		read -p "Enter the absolute path to the hbar_cfac_ftf/DataTool directory:" NOWPATH
		while [ ! -f "$NOWPATH/setup.sh" ]
		do
			read -p "The path seems incorrect, please try again:" NOWPATH
		done
		chmod a+w Makefile
		sed -i -e "s,RFAC:=.*,RFAC:=$NOWPATH,g" Makefile
		chmod a-w Makefile
	fi
}

function DownloadDatabase()
{
	read -p "Download decay database? (y/n)" -n 1 -r
	echo -en "\n"
	CanDownload=1
	if [[ $REPLY =~ ^[yY]$ ]]
	then
		if [ -f "hstates.root" ]
		then
			read -p "File hstates.root exists, overwrite? (y/n)" -n 1 -r
			echo -en "\n"
			if [[ $REPLY =~ ^[yY]$ ]]
			then
				rm -f "hstates.root"
			else
				CanDownload=0
			fi
		fi
		if [ "$CanDownload" -eq "1" ]
		then
			wget "https://smilx0.smi.oeaw.ac.at/decay/hstates.root"
		fi
	fi
}

function SetupMakefile()
{
	CanOverwrite=1
	if [ -f "Makefile" ]
	then
		read -p "Overwrite current makefile? (y/n)" -n 1 -r
		echo -en "\n"
		if [[ $REPLY =~ ^[yY]$ ]]
		then
			chmod a+w Makefile
		else
			CanOverwrite=0
		fi
	fi

	if [ "$CanOverwrite" -eq "1" ]
	then
		cp Makefile.in Makefile
        #To prevent accidental modification of makefile instead of makefile.in
		chmod a-w Makefile 
	fi
}


function CheckRequirements()
{
	#Check if we have a makefile in the current directory. If not, we are obviously not in the project directory.
	if [ ! -f "Makefile.in" ]
	then
		echo "Fatal error: Makefile.in not found."
		exit 1
	fi

}

Main
