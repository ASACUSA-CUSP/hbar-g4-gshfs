# Readme

## Download and install

```bash
git clone https://gitlab.cern.ch/ASACUSA-CUSP/hbar-g4-gshfs.git

source <ROOT-INSTALL-PATH>/root/bin/thisroot.sh
source <GEANT4-INSTALL-PATH>/bin/geant4.sh
source <GEANT4-INSTALL-PATH>/share/Geant4-10.7.1/geant4make/geant4make.sh

./autoinstall.sh
```
