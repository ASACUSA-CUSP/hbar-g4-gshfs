#!/usr/bin/env bash

WDIR=$(pwd)
GSHFS_DIR=hbar_gshfs_FTF
CFAC_FTF_DIR=hbar_cfac_ftf/DataTool

### STEP 0

# - check if the 2 directories exist
if [ ! -d $WDIR/$GSHFS_DIR ] || [ ! -d $WDIR/$CFAC_FTF_DIR ] ; then
    echo "Error: there's a problem with one or more directory location."
    echo "Exit."
    exit 1
fi



### STEP 1
# - move to hbar gshfs dir
# - delete Makefile
# - run setup.sh downloading data if not present
# - rsync with the fields folder under EOS if not present 

cd $WDIR/$GSHFS_DIR
# download hstates.root if not present
[ ! -f "hstates.root" ] && wget http://asacusa.web.cern.ch/material/hstates.root

# rsync with the EOS fields folder (ask for the CERN usename)
# This part should run only if the "fields" folder under $GSHFS_DIR does not exist yet
if [ ! -d "fields" ] ; then
	echo "The field files have to be downloaded from the CERN EOS space, rsync will be used."
	echo "Insert your CERN username (you will be prompted for the password later by rsync):"
	read CERN_USERNAME
	rsync -avz $CERN_USERNAME@lxplus.cern.ch:/eos/experiment/asacusa/hbar-g4-gshfs_files/fields . 
	echo "rsync done."
fi 

# create a fake Makefile if not present (just to make setup.sh ask for deletion)
touch Makefile
{ echo "yy${WDIR}/${CFAC_FTF_DIR}" ; echo "n" ; } | ./setup.sh

### STEP 2
# - mote to the other dir
# - run setup writing a new makefile (answer 2)

cd $WDIR/$CFAC_FTF_DIR
echo "I am in "$WDIR/$CFAC_FTF_DIR" that should be this: "$(pwd)
echo "2y${WDIR}/${GSHFS_DIR}" | ./setup.sh
make

### STEP 3
# - move back to the first dir
# - run again the setup script
# - compile

cd $WDIR/$GSHFS_DIR
{ echo "yy${WDIR}/${CFAC_FTF_DIR}" ; echo "n" ; } | ./setup.sh

make

echo $0"  ==> done. "

